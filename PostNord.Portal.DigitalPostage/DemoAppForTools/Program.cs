﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PostNord.Portal.Tools.Labels;
using System;
using System.Drawing;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Xml.Serialization;

namespace DemoAppForTools
{
    class Program
    {
        static void CreateLabels()
        {
            string templateFile = @"template.xml";

            string imageFile = @"c:\temp\husky.png";

            byte[] data;

            using(StreamReader reader = new StreamReader(imageFile))
            {
                int length = (int)reader.BaseStream.Length;
                data = new byte[length];
                reader.BaseStream.Read(data, 0, length);
            }

            System.Drawing.Image imgObj = null;
            using (MemoryStream mem = new MemoryStream(data))
            {
                imgObj = System.Drawing.Image.FromStream(mem);
            }


            using (StreamReader reader = new StreamReader(templateFile))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(LabelTemplate));
                LabelTemplate template = (LabelTemplate)serializer.Deserialize(reader);

                string filename = "Labels.pdf";// Path.GetTempFileName();

                LabelCreator.ProductInfo[] products = new LabelCreator.ProductInfo[]
                {
                    new LabelCreator.ProductInfo{
                        Code = "A 2 5 L\n3 7 6 4\n3 1 8 2",
                        ExpiryDate = new DateTime(2014,07,05),
                        Price = 100,
                        OverlayText = "A"
                    }
                    ,
                    new LabelCreator.ProductInfo{
                        Code = "A 2 5 L\n3 7 6 4\n3 1 8 2",
                        ExpiryDate = new DateTime(2014,07,09),
                        Price = 200,
                        OverlayText = "B"
                    }
                    ,
                    new LabelCreator.ProductInfo{
                        Code = "A 2 5 L\n3 7 6 4\n3 1 8 2",
                        ExpiryDate = new DateTime(2014,07,09),
                        Price = 300,
                        OverlayText = "B"
                    }
                };

                LabelCreator generator = new LabelCreator(template, imgObj);
                generator.CreateLabelSheets(filename, products);
            }
        }


        static FileStream GetReceipt()
        {
            string path = @"C:\test.htm";
            string outputPath = @"C:\test.pdf";

            string html = File.ReadAllText(path);

            var sr = new StringReader(html);
            var pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            var htmlparser = new HTMLWorker(pdfDoc);

            FileStream receiptStream = File.OpenWrite(outputPath);
            PdfWriter.GetInstance(pdfDoc, receiptStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();

            return receiptStream;

        }


        static byte[] CreatePdf(string html, iTextSharp.text.Rectangle pageSize, float marginLeft, float marginRight, float marginTop, float marginBottom)
        {

            //Basic PDF setup
            using (var msOutput = new MemoryStream())
            {
                using (var document = new Document(PageSize.A4, 30, 30, 30, 30))
                {
                    using (var writer = PdfWriter.GetInstance(document, msOutput))
                    {

                        //Open our document for writing
                        document.Open();

                        //Bind a reader to our text
                        using (TextReader reader = new StringReader(html))
                        {
                            //Parse the HTML and write it to the document
                            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, reader);
                        }

                        //Close the main document
                        document.Close();
                    }

                    //Return our raw bytes
                    return msOutput.ToArray();
                }
            }
        }


        static void CreateReceipt()
        {
            string path = @"C:\test.htm";
            string outputPath = @"C:\test.pdf";

            string html = File.ReadAllText(path);

            var sr = new StringReader(html);
            var pdfDoc = new Document (PageSize.A4, 10f, 10f, 10f, 0f);
            var htmlparser = new HTMLWorker(pdfDoc);

            FileStream receiptStream = File.OpenWrite(outputPath);
            PdfWriter.GetInstance(pdfDoc, receiptStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();

            return;



            iTextSharp.text.Document document = new iTextSharp.text.Document();
            iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, receiptStream);
            document.Open();

            using (MemoryStream htmlStream = new MemoryStream(System.Text.Encoding.Default.GetBytes(html)))
            {
                iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, new StreamReader(htmlStream));
            }


            receiptStream.Close();
            receiptStream.Dispose();

            byte[] data = new byte[receiptStream.Length];
            receiptStream.Seek(0, SeekOrigin.Begin);
            receiptStream.Read(data, 0, (int)receiptStream.Length);

            //document.Close();

        
        
        }


        static void SendMail(byte[] pdf)
        {

            SmtpClient mailClient = new SmtpClient("127.0.0.1");
            MailMessage message = new MailMessage("no-reply@postdanmark.dk", "MikaelLMortensen@gmail.com");
            message.IsBodyHtml = true;
            message.Body = "<html><body><h1>Hello world</h1></body></html>";
            message.Subject = "Hello";


            string path = @"C:\test.htm";
            string outputPath = @"C:\test.pdf";

            string html = File.ReadAllText(path);

            var sr = new StringReader(html);
            var pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            var htmlparser = new HTMLWorker(pdfDoc);

            FileStream receiptStream = new FileStream(outputPath, FileMode.OpenOrCreate);
            PdfWriter.GetInstance(pdfDoc, receiptStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();

            using (MemoryStream pdfStream = new MemoryStream(pdf)) {
                Attachment att = new Attachment(pdfStream, "test.pdf", "application/pdf");
                message.Attachments.Add(att);
                mailClient.Send(message);
            }
            
            Console.WriteLine("Mail sent!");

        }

        static void Main(string[] args)
        {
            try
            {
                string path = @"C:\test.htm";
                string outputPath = @"C:\test.pdf";

                string html = File.ReadAllText(path);

                byte[] outputPdf = CreatePdf(html, PageSize.A4, 10f, 10f, 10f, 0f);
                System.IO.File.WriteAllBytes(outputPath, outputPdf);
//                SendMail(outputPdf);

                Console.WriteLine("Mail with PDF sent! Size: {0}", outputPdf.Length);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            //SendMail();
            //CreateReceipt();
          //  CreateLabels();
        }
    }
}
