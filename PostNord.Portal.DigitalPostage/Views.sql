﻿GO
/****** Object:  View [dbo].[ViewOrdersSubOrdersCodes]    Script Date: 01/30/2014 14:48:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewOrdersSubOrdersCodes]
AS
SELECT     dbo.Orders.Id, dbo.Orders.Email, dbo.Orders.ReceiptNumber, dbo.Orders.CaptureId, dbo.Orders.LabelImage, dbo.Orders.ImageSize, dbo.Orders.SalesDate, 
                      dbo.Orders.ExpireDate, dbo.Orders.OrderSum, dbo.SubOrders.Id AS SubOrderId, dbo.SubOrders.ProductId, dbo.SubOrders.Weight, dbo.SubOrders.Priority, 
                      dbo.SubOrders.Destination, dbo.SubOrders.Amount, dbo.SubOrders.Price, dbo.SubOrders.SubOrderSum, dbo.Codes.Id AS CodesId, dbo.Codes.PostCode, 
                      dbo.Codes.CodeId, dbo.Codes.Status, dbo.Codes.UsedDate, dbo.Codes.RefundedDate, dbo.Codes.RefundId, dbo.Orders.CreditSum, dbo.Orders.Saldo, 
                      dbo.Orders.OrderStatus, dbo.SubOrders.SubOrderCreditSum, dbo.SubOrders.SubOrderSaldo, dbo.Codes.IssueDate
FROM         dbo.SubOrders INNER JOIN
                      dbo.Orders ON dbo.SubOrders.OrderId = dbo.Orders.Id INNER JOIN
                      dbo.Codes ON dbo.SubOrders.Id = dbo.Codes.SubOrderId
GO
