﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PostNord.Portal.DigitalPostage;
using System.Collections.Generic;
using PostNord.Portal.DigitalPostage.Bll;
using System.IO;
using PostNord.Portal.DigitalPostage.Helpers;


namespace UnitTests
{
    [TestClass]
    public class Codes
    {
        DalSubOrders dalSubOrders = null;
        public PostNord.Portal.DigitalPostage.Bll.Codes CreatedCode = null;

        [TestInitialize]
        public void Initialize()
        {
            //UnitTests.Initialize.InitConfig();

            //dalSubOrders = new DalSubOrders();

            //dalSubOrders.Initialize();
            //dalSubOrders.CreateSubOrder();

            //dalSubOrders.TestOrder.Status = PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Paid;
            //dalSubOrders.TestOrder.Save();
            //foreach(SubOrders sub in dalSubOrders.TestOrder.SubOrders)
            //{
            //   sub.CreateCodes();
            //}
        }

        [TestCleanup]
        public void CleanUp()
        {
            //if (dalSubOrders.TestOrder.Id > 0)
            //{
            //    dalSubOrders.TestOrder.Delete();
            //}
        }


        /// <summary>
        /// Gets the postage number. 
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>
        /// The postage week number. (1-366)
        /// </returns>
        /// <remarks>
        /// The Postage number is defined as number of weeks since 1-1-2014. Every 366 weeks (7 years) it is recycled.
        /// </remarks>
        static int GetPostageNumber(DateTime date)
        {
            int MAX_WEEK_COUNT = 366;
            DateTime startDate = new DateTime(2014, 1, 1);

            int weekCount = WeekCount(startDate, date);
            while (weekCount > MAX_WEEK_COUNT)
            {
                weekCount = weekCount - MAX_WEEK_COUNT;
            }
            return weekCount;
        }


        /// <summary>
        /// Get number of weeks between 2 dates.
        /// </summary>
        /// <param name="periodStart">The period start.</param>
        /// <param name="periodEnd">The period end.</param>
        /// <returns>Number of calendar weeks between 2 days</returns>
        static int WeekCount(DateTime periodStart, DateTime periodEnd)
        {
            const DayOfWeek FIRST_DAY_OF_WEEK = DayOfWeek.Monday;
            const DayOfWeek LAST_DAY_OF_WEEK = DayOfWeek.Sunday;
            const int DAYS_IN_WEEK = 7;

            DateTime firstDayOfWeekBeforeStartDate;
            int daysBetweenStartDateAndPreviousFirstDayOfWeek = (int)periodStart.DayOfWeek - (int)FIRST_DAY_OF_WEEK;
            if (daysBetweenStartDateAndPreviousFirstDayOfWeek >= 0)
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-daysBetweenStartDateAndPreviousFirstDayOfWeek);
            }
            else
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-(daysBetweenStartDateAndPreviousFirstDayOfWeek + DAYS_IN_WEEK));
            }

            DateTime lastDayOfWeekAfterEndDate;
            int daysBetweenEndDateAndFollowingLastDayOfWeek = (int)LAST_DAY_OF_WEEK - (int)periodEnd.DayOfWeek;
            if (daysBetweenEndDateAndFollowingLastDayOfWeek >= 0)
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek);
            }
            else
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek + DAYS_IN_WEEK);
            }

            int calendarWeeks = 1 + (int)((lastDayOfWeekAfterEndDate - firstDayOfWeekBeforeStartDate).TotalDays / DAYS_IN_WEEK);

            return calendarWeeks;
        }


        [TestMethod]
        public void TestWeekNumber()
        {
            DateTime d = new DateTime(2015, 2, 7);

            d = new DateTime(2015, 06, 12);
            Console.WriteLine("7/2 2014: {0}", GetPostageNumber(d));
            Console.WriteLine("Current week: {0}", GetPostageNumber(DateTime.Today));
            Console.WriteLine("180 dage week: {0}", GetPostageNumber(DateTime.Today.AddDays(180)));
        }


        /// <summary>
        /// Get the danish week number from given date.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <returns>
        /// The Calendar Week number in da-DK
        /// </returns>
        static int WeekNumber(DateTime fromDate)
        {
            // Get jan 1st of the year
            DateTime startOfYear = new DateTime(fromDate.Year, 1, 1);
            // Get dec 31st of the year
            DateTime endOfYear = new DateTime(fromDate.Year, 12, 31);

            // ISO 8601 weeks start with Monday 
            // The first week of a year includes the first Thursday 
            // DayOfWeek returns 0 for sunday up to 6 for saterday
            int[] iso8601Correction = { 6, 7, 8, 9, 10, 4, 5 };
            int nds = fromDate.Subtract(startOfYear).Days + iso8601Correction[(int)startOfYear.DayOfWeek];
            int wk = nds / 7;
            switch (wk)
            {
                case 0:
                    // Return weeknumber of dec 31st of the previous year
                    return WeekNumber(startOfYear.AddDays(-1));
                case 53:
                    // If dec 31st falls before thursday it is week 01 of next year
                    if (endOfYear.DayOfWeek < DayOfWeek.Thursday)
                        return 1;
                    else
                        return wk;
                default: return wk;
            }
        }


/*
        [TestMethod]
        public void TestCode()
        {
            UnitTests.Initialize.InitConfig();
            float val = 8;
            int num = 210;
            DateTime expDate = new DateTime(2014, 10, 1);

            PostNord.Portal.DigitalPostage.Bll.Codes c = new PostNord.Portal.DigitalPostage.Bll.Codes();
            string code = c.GetCode(expDate, val, num);

            Assert.IsTrue(code == "LNF2-N0NF-THK0", "Koden er ikke korrekt");

            string scrambled = ScrambleCode(code);

            Assert.IsTrue(scrambled == "3CAB7508A0C261F4AE23750B45A3AE343C08B2E0", "Scrambled er ikke korrekt");
        }


        [TestMethod]
        public void TestCodeSimple()
        {
            string code = "RPFH-F57F-HF76";

            string scrambled = ScrambleCode(code);

            Console.WriteLine("Code: {0}", code);
            Console.WriteLine("Scrambled: {0}", scrambled);

            code = "LNF2-N0NF-THK0";

            scrambled = ScrambleCode(code);

            Assert.IsTrue(scrambled == "3CAB7508A0C261F4AE23750B45A3AE343C08B2E0", "Scrambled er ikke korrekt");
        }
*/

        /// <summary>
        /// Scrambles the code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns>Scrambled Code</returns>
        static private string ScrambleCode(string code)
        {
            if (code.Length == 0)
            {
                throw new IndexOutOfRangeException("Code is empty");
            }
            code = code.Replace("-", "").Replace(" ", "");
            string res = HelperClass.GetHashString(code);
            return res;
        }


        [TestMethod]
        public void TestLoadTestFile()
        {

            long ORDER_NUM = 588;

            UnitTests.Initialize.InitConfig();
            PostNord.Portal.DigitalPostage.Helpers.ConfigSettings.Values.Add("TestConfig", "0");
            PostNord.Portal.DigitalPostage.Bll.SubOrders subOrder = new SubOrders(ORDER_NUM);

            string FILE_NAME = "CodeTest.txt";
            var lines = File.ReadAllLines(FILE_NAME);

            // Delete code
            while (subOrder.Codes.Count > 0)
            {
                PostNord.Portal.DigitalPostage.Bll.Codes c = subOrder.Codes[0];
                c.Delete();
            }

/*
            for (int i = 2; i < lines.Length; i++)
            {

                string[] codeArr = lines[i].Split('\t');
                if (codeArr.Length < 11)
                {
                    Console.WriteLine("Skipping: {0}", i);
                }
                string[] dateArr = codeArr[0].Split('-');
                DateTime date = new DateTime(int.Parse(dateArr[2]), int.Parse(dateArr[1]), int.Parse(dateArr[0]));
                double price = float.Parse(codeArr[1].Replace(",","."));
                uint serial = uint.Parse(codeArr[2]);

                uint weekValue = uint.Parse(codeArr[4]);
                uint amountValue = uint.Parse(codeArr[5]);
                uint serialTest = uint.Parse(codeArr[6]);

                string code = string.Format("{0}-{1}-{2}", codeArr[8], codeArr[9], codeArr[10]);

                TestCode(date, price, serial, amountValue, weekValue, serialTest, code);

                PostNord.Portal.DigitalPostage.Bll.Codes co = new PostNord.Portal.DigitalPostage.Bll.Codes(ORDER_NUM, price, date.AddDays(-190), date, (int)serial);
//                       PostNord.Portal.DigitalPostage.Dal.Codes.CodesInsertRow("", ORDER_NUM, (int)serial, 1, price, date.AddDays(-190), date);
            }
 */
        }

/*
        [TestMethod]
        public void TestCodeGeneration()
        {
            uint day = 10;
            float valueFloat = 8;
            uint valueInt = 16;
            uint number = 1000;

            PostNord.Portal.Code code = new PostNord.Portal.Code(string.Format("{0} {1} {2}", day, valueInt, number));

            Assert.IsTrue(code.Day == day,"Day is wrong");
            Assert.IsTrue(code.Franking == valueInt,"Value as int is wrong");
            Assert.IsTrue(code.FrankingValue == valueFloat,"Value as float is wrong");
            Assert.IsTrue(code.Number == number,"Value as float is wrong");

            string postageCode = PostNord.Portal.CodeWrapper.GetCode(day, valueInt, number);

            uint dayTest;
            float valueFloatTest;
            uint valueIntTest;
            uint numberTest;
            PostNord.Portal.CodeWrapper.GetCodeDetails(postageCode, out dayTest, out valueIntTest, out valueFloatTest, out numberTest);

            Assert.IsTrue(dayTest == day, "Day is wrong");
            Assert.IsTrue(valueFloatTest == valueFloat, "Value as float is wrong");
            Assert.IsTrue(valueIntTest == valueInt, "Value as int is wrong");
            Assert.IsTrue(numberTest == number, "Number is wrong");
        
        }


        /// <summary>
        /// Tests the codes.
        /// </summary>
        [TestMethod]
        public void TestCodes()
        {
            Assert.IsTrue(CreatedCode.Id > 0, "ID was not set");

            PostNord.Portal.DigitalPostage.Bll.Codes loadedCode = new PostNord.Portal.DigitalPostage.Bll.Codes(CreatedCode.Id);
            Assert.IsFalse(CreatedCode.Id == 0, "Code was not loaded");
            Assert.IsTrue(CreatedCode.Status == PostNord.Portal.DigitalPostage.Bll.Codes.CodeStatusValues.Disabled, "Status was not 'Unused'");

            Assert.IsTrue(CreatedCode.Id == loadedCode.Id, "ID was wrong");
            Assert.IsTrue(CreatedCode.SubOrderId == loadedCode.SubOrderId, "SubOrderId was wrong");
            Assert.IsTrue(CreatedCode.CodeId == loadedCode.CodeId, "CodeId was wrong");
            Assert.IsTrue(CreatedCode.PostCode.Length == 14, "Code Length is wrong. Expected: 14, got: {0}", CreatedCode.PostCode.Length);
            Assert.IsTrue(CreatedCode.PostCode == loadedCode.PostCode, "PostCode was wrong");
            Assert.IsTrue(CreatedCode.Price == loadedCode.Price, "Price was wrong");
            Assert.IsTrue(CreatedCode.IssueDate.ToShortDateString() == loadedCode.IssueDate.ToShortDateString(), "IssueDate was wrong");
            Assert.IsTrue(CreatedCode.ExpireDate.ToShortDateString() == loadedCode.ExpireDate.ToShortDateString(), "ExpireDate was wrong");
            Assert.IsTrue(CreatedCode.Status == loadedCode.Status, "Status was wrong");

            Assert.IsTrue(dalSubOrders.SubOrder.Codes.Count == 5, "Number of Generated Codes is wrong Expected: 5, got: {0}", dalSubOrders.SubOrder.Codes.Count);
            Assert.IsTrue(dalSubOrders.SubOrder.Codes[4].Id == loadedCode.Id, "Retrieved Code Id is wrong");

            dalSubOrders.SubOrder.SetCodesAsUnused(); // Change Status to Unused
            loadedCode = new PostNord.Portal.DigitalPostage.Bll.Codes(CreatedCode.Id);
            Assert.IsTrue(loadedCode.Status == PostNord.Portal.DigitalPostage.Bll.Codes.CodeStatusValues.Unused, "Status was not 'Unused'");

            loadedCode.SetCodeAsUsed("Employee");

            loadedCode = new PostNord.Portal.DigitalPostage.Bll.Codes(CreatedCode.Id);
            Assert.IsTrue(loadedCode.Status == PostNord.Portal.DigitalPostage.Bll.Codes.CodeStatusValues.Used, "Status was not 'Used'");
            Assert.IsTrue(loadedCode.UsedDate.Value.ToShortDateString() == DateTime.Now.ToShortDateString(), "Used Date was not updated");

            loadedCode = new PostNord.Portal.DigitalPostage.Bll.Codes(CreatedCode.PostCode);
            Assert.IsTrue(CreatedCode.PostCode == loadedCode.PostCode, "PostCode was wrong for loaded code");

            List<PostNord.Portal.DigitalPostage.Bll.Orders> orders = PostNord.Portal.DigitalPostage.Bll.Orders.GetOrdersFromCode(CreatedCode.IssueDate.AddDays(-10), CreatedCode.IssueDate.AddDays(10), loadedCode.PostCode);
            Assert.IsTrue(orders.Count == 1, "Order not found from code");
        }
 */
    }
}
