﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class NetAxeptWS
    {
        string MerchantId = "12001082";
        string Token = "4m=Q-No6";
        string TransActionId = "e9aca59c668a420fa5a63de91f847c5f";


        [TestMethod]
        public void Bla()
        {


        }

        [TestMethod]
        public void TestQueryCall()
        {
            using (NetAxept.Netaxept saleClient = new NetAxept.Netaxept())
            {
                saleClient.Url = "https://test.epayment.nets.eu/Netaxept.svc";
                int netsTimeOut = 10000;
                saleClient.Timeout = netsTimeOut;

                NetAxept.QueryRequest qReq = new NetAxept.QueryRequest
                {
                    TransactionId = this.TransActionId
                };
                NetAxept.PaymentInfo paymentInfo = (NetAxept.PaymentInfo) saleClient.Query(this.MerchantId, this.Token, qReq);

                Assert.IsTrue(paymentInfo.CardInformation.Issuer == "Visa", "Not Visa: " + paymentInfo.CardInformation.Issuer);

            }
        }
    }
}
