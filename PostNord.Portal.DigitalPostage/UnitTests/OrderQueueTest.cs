﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PostNord.Portal.DigitalPostage.Bll;

namespace UnitTests
{
    [TestClass]
    public class OrderQueueTest
    {

        PostNord.Portal.DigitalPostage.Bll.Orders newOrder = null;
        List<PostNord.Portal.DigitalPostage.Bll.Orders> orders = null;
        long id = 0;

        [TestInitialize]
        public void Initialize()
        {
            UnitTests.Initialize.InitConfig();
            var dos = new DalOrders();
            orders = PostNord.Portal.DigitalPostage.Bll.Orders.GetOrders(DateTime.Now.AddDays(-30), DateTime.Now);
        }

        [TestCleanup]
        public void CleanUp()
        {
            if (newOrder != null)
                newOrder.Delete();
        }


        [TestMethod]
        public void TestQueue()
        {
            int max = 5;

            if (orders.Count < max)
            {
                Console.WriteLine("Too few orders. Max: {0} Order count: {1}", max, orders.Count);
                Assert.Fail();
                return;
            }

            long id; 

            for (int i = 0; i < max; i++)
            {
                id = orders[i].Id;
                OrderQueue.Add(id, DateTime.Now.AddSeconds(i));
                Console.WriteLine("Adding Debit order: {0}", id);
            }

            var queue = OrderQueue.GetOrderQueue();
            OrderQueue popped = null;
            Console.WriteLine("Debit Queue length: {0}", queue.Count);

            Assert.IsTrue(queue.Count == max, "Queue length differs.");
            for (int i = max - 1; i >= 0; i--)
            {
                popped = OrderQueue.GetQueueNextEntry();
                Assert.IsTrue(queue[i].OrderId == popped.OrderId, "Order id differs");
                Assert.IsTrue(queue[i].RefundId == 0, "Refund id is not 0");
                Assert.IsTrue(queue[i].Action == "Debit", "Action not Debit");
                Console.WriteLine("Debit Popping order: {0}", popped.OrderId);
            }

            popped = OrderQueue.GetQueueNextEntry();
            Assert.IsTrue(popped == null, "Queue is not empty");

            for (int i = 0; i < max; i++)
            {
                id = orders[i].Id;
                OrderQueue.Add(id, id+1, DateTime.Now.AddSeconds(i));
                Console.WriteLine("Adding Credit order: {0}", id);
            }

            queue = OrderQueue.GetOrderQueue();
            Console.WriteLine("Queue length: {0}", queue.Count);

            Assert.IsTrue(queue.Count == max, "Credit Queue length differs.");
            max--;
            for (int i = max; i >= 0; i--)
            {
                popped = OrderQueue.GetQueueNextEntry();
                Assert.IsTrue(queue[i].OrderId == popped.OrderId, "Credit Order id differs");
                Assert.IsTrue(queue[i].RefundId == popped.OrderId + 1, "RefundId is not correct");
                Assert.IsTrue(queue[i].Action == "Credit", "Action not Credit");
                Console.WriteLine("Popping order: {0}", popped.OrderId);
            }
        }
    }
}
