﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PostNord.Portal.DigitalPostage.Bll;

namespace UnitTests
{
    /// <summary>
    /// Summary description for DalProducts
    /// </summary>
    [TestClass]
    public class DalProducts
    {
        string name = "Test Products Name";

        public DalProducts()
        {
            UnitTests.Initialize.InitConfig();

            //
            // TODO: Add constructor logic here
            //
        }

        [TestMethod]
        public void TestProducts()
        {
            PostNord.Portal.DigitalPostage.Bll.Products products = new PostNord.Portal.DigitalPostage.Bll.Products(name);
            Assert.IsTrue(products.Id > 0, "ID was NOT set");

            Assert.IsTrue(Products.GetProductId(name) == products.Id, "Name does not return correct Id");

            products.Delete();

            Assert.IsTrue(products.Id == 0, "ID is not zero");
        
        }
    }
}
