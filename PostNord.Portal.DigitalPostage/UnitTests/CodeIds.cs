﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Data.SqlClient;
using System.Data;

namespace UnitTests
{
    /// <summary>
    /// Summary description for CodeIds
    /// </summary>
    [TestClass]
    public class CodeIds
    {
        public CodeIds()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        [TestInitialize]
        public void Initialize()
        {
            UnitTests.Initialize.InitIntSecurity();
        }
/*
        [TestMethod]
        public void TestCodeIds()
        {

            DateTime testTime = DateTime.Now;

            PostNord.Portal.DigitalPostage.Bll.CodeIds codeId = new PostNord.Portal.DigitalPostage.Bll.CodeIds();

            int code1 = codeId.GenerateCode();
            int code2 = codeId.GenerateCode();
            Assert.IsTrue(code1 > 0 && code2 > 0, "Generated Code is 0");
            Assert.IsTrue(code1 != code2, "Generated Code is equal");

            List<PostNord.Portal.DigitalPostage.Bll.CodeIds> codeIds = PostNord.Portal.DigitalPostage.Bll.CodeIds.CodeIdsSelectDate(DateTime.Today, DateTime.Today);

            Assert.IsTrue(codeIds.Count == 1, "Wrong Number of CodeIds returned: Expected:1 Found: {0}", codeIds.Count);
            Assert.IsTrue(codeIds[0].CodeId == code2, "Loaded code is wrong. Expected:{0} Found: {1}", code2, codeIds[0].CodeId);
        }

        [TestMethod]
        public void TestCodeRacing()
        {

            DateTime testTime = DateTime.Now;

            int startCode = 0;
            int delayTime = 1000;

            PostNord.Portal.DigitalPostage.Bll.CodeIds code1 = new PostNord.Portal.DigitalPostage.Bll.CodeIds();
            startCode = code1.GenerateCode();
            code1.DelayTime = delayTime;

            PostNord.Portal.DigitalPostage.Bll.CodeIds code2 = new PostNord.Portal.DigitalPostage.Bll.CodeIds();

            Thread code1Thread = new Thread(new ThreadStart(code1.GenerateCodeTest));
            code1Thread.Start();
            while (!code1Thread.IsAlive) ;

            Thread code2Thread = new Thread(new ThreadStart(code2.GenerateCodeTest));
            code2Thread.Start();
            while (!code2Thread.IsAlive);

            System.Threading.Thread.Sleep(delayTime * 2 + 500);

            Assert.IsFalse(code1Thread.IsAlive, "code1 Thread is still alive");
            Assert.IsFalse(code2Thread.IsAlive, "code2 Thread is still alive");

            int endCode = code2.GenerateCode();

            int expectedCode = startCode + 3;

            Assert.IsTrue(endCode == expectedCode, "RacingCondition found. StartCode:{0} Expected CodeId: {1} Found: {2}", startCode, expectedCode, endCode);

        }
*/

        /// <summary>
        /// Tests the database connection.
        /// </summary>
        [TestMethod]
        public void TestDbConnection()
        {
            string connectionString = PostNord.Portal.DigitalPostage.Helpers.HelperClass.GetConnectionString();

            using (SqlConnection cn = new SqlConnection(connectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [DigitalPostage].[dbo].[Orders]", cn))
                {
                    object res = cmd.ExecuteScalar();
                    int numberOfOrders = int.Parse(res.ToString());
                    Assert.IsTrue(numberOfOrders > 0, "Ingen ordre fundet!");
                }
            }
        }
/*
        /// <summary>
        /// Tests the next code generation.
        /// </summary>
        [TestMethod]
        public void TestNextCodeGeneration()
        {

            int startcode;
            int nextcode;
            int codeCount = 5;

            double price = 1;

            startcode = PostNord.Portal.DigitalPostage.Dal.NextCode.GetCurrentCode(price);

            // if no code has been generated create the first one
            if (startcode == -1)
            {
                startcode = PostNord.Portal.DigitalPostage.Dal.NextCode.GetNextCode(price, 1);
            }

            nextcode = PostNord.Portal.DigitalPostage.Dal.NextCode.GetNextCode(price, codeCount);
            Assert.IsTrue(nextcode == startcode + codeCount, "Generated code is wrong: Expected: {0} Got: {1}", (startcode + codeCount), nextcode);

            // Change MAX code to force roll over
            PostNord.Portal.DigitalPostage.Dal.NextCode.MAX_CODE = nextcode + 1;

            // Test roll over
            nextcode = PostNord.Portal.DigitalPostage.Dal.NextCode.GetNextCode(price, codeCount);
            Assert.IsTrue(nextcode == codeCount + 1, "Code roll over is wrong: Expected: {0} Got: {1}", codeCount, nextcode);

            // Reset Code to start value
            nextcode = PostNord.Portal.DigitalPostage.Dal.NextCode.NextKeyResetRow(price, startcode);
            Assert.IsTrue(nextcode == startcode, "Code set to start value failed. Expected: {0} got {1}", nextcode, startcode);

        }

*/
    }
}
