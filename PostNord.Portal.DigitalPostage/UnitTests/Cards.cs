﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PostNord.Portal.DigitalPostage.Bll;

namespace UnitTests
{
    [TestClass]
    public class TestCards
    {

        string email = "test@mail.dk";
        string key = "Unique";
        long orderId = 12345;
        Guid cardId = Guid.Empty;

        [TestInitialize]
        public void Initialize()
        {
            UnitTests.Initialize.InitConfig();
        }

        [TestMethod]
        public void CreateCard()
        {
            Cards c = new Cards(orderId, email, key);
            c.Save();
            cardId = c.Id;
            Assert.IsTrue(c.Id != Guid.Empty, "Id is empty");
            Assert.IsTrue(c.Email == email, "Email is wrong");
            Assert.IsTrue(c.PanHash != string.Empty, "Panhash is empty");
            Assert.IsTrue(c.PanHash == key, "Panhash is not set");
            Assert.IsTrue(c.LastUsed.ToShortDateString() == DateTime.Today.ToShortDateString(), "LastUsed er ikke sat");
            Assert.IsTrue(c.Created.ToShortDateString() == DateTime.Today.ToShortDateString(), "Created er ikke sat");

            c = new Cards(c.Id);
            Assert.IsTrue(c.Email == email, "Email is wrong");
            Assert.IsTrue(c.PanHash != string.Empty, "Panhash is empty");
            Assert.IsTrue(c.PanHash == key, "Panhash is not set");
            Assert.IsTrue(c.LastUsed.ToShortDateString() == DateTime.Today.ToShortDateString(), "LastUsed er ikke sat");
            Assert.IsTrue(c.Created.ToShortDateString() == DateTime.Today.ToShortDateString(), "Created er ikke sat");

            c = new Cards(c.OrderId);
            Assert.IsTrue(c.Email == email, "Email is wrong");
            Assert.IsTrue(c.PanHash != string.Empty, "Panhash is empty");
            Assert.IsTrue(c.PanHash == key, "Panhash is not set");
            Assert.IsTrue(c.LastUsed.Date.ToShortDateString() == DateTime.Today.ToShortDateString(), "LastUsed er ikke sat");
            Assert.IsTrue(c.Created.Date.ToShortDateString() == DateTime.Today.ToShortDateString(), "Created er ikke sat");

            Console.WriteLine("All tests are passed!");

        }

        [TestCleanup]
        public void TearDown()
        {
            Cards c = new Cards(cardId);
            c.Delete();    
        }
    }
}
