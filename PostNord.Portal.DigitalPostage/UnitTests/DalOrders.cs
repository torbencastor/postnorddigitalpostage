﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class DalOrders
    {

        long id = 0;
        string email = "test@testing.dk";
        string receiptNumber = "RECEIPT_NR"; 
        string captureId = "CAPTUREID"; 
        byte[] labelImage = {}; 
        DateTime salesDate = DateTime.Now; 
        DateTime expireDate = DateTime.Now.AddDays(365);
        string log1 = "ErrorLog1";
        string log2 = "ErrorLog2";

        PostNord.Portal.DigitalPostage.Bll.Orders newOrder = null;

        public PostNord.Portal.DigitalPostage.Bll.Orders TestOrder()
        {
            PostNord.Portal.DigitalPostage.Bll.Orders testOrder = new PostNord.Portal.DigitalPostage.Bll.Orders();
            testOrder = new PostNord.Portal.DigitalPostage.Bll.Orders();
            testOrder.Email = email;
            testOrder.ReceiptNumber = receiptNumber;
            testOrder.CaptureId = captureId;
            testOrder.SalesDate = salesDate;
            testOrder.ExpireDate = expireDate;
            return testOrder;
        }

        [TestInitialize]
        public void Initialize()
        {
            UnitTests.Initialize.InitConfig();

            newOrder = TestOrder();
            newOrder.Save();
            this.id = newOrder.Id;
            Assert.IsTrue(newOrder.Id > 0, "Id is ZERO!");
        }

        [TestCleanup]
        public void CleanUp()
        {
            if (id > 0)
            {
                PostNord.Portal.DigitalPostage.Bll.Orders delOrder = new PostNord.Portal.DigitalPostage.Bll.Orders(id);
                delOrder.Delete();
            }
        }

        [TestMethod]
        public void CreateOrder()
        {

            if (newOrder.Id > 0)
            {
                PostNord.Portal.DigitalPostage.Bll.Orders order = new PostNord.Portal.DigitalPostage.Bll.Orders(newOrder.Id);

                Assert.IsTrue(order != null, "Order is NULL");
                Assert.IsTrue(order.Id == newOrder.Id, "Ids differ");
                Assert.IsTrue(order.Email == newOrder.Email, "Emails differ");
                Assert.IsTrue(order.ReceiptNumber == newOrder.ReceiptNumber, "Receipts differ");
                Assert.IsTrue(order.CaptureId == newOrder.CaptureId, "CaptureId differ");
                Assert.IsTrue(order.ImageSize == newOrder.ImageSize, "Initial ImageSize differ");
                Assert.IsTrue(order.SalesDate.ToString() == newOrder.SalesDate.ToString(), "SalesDate differ");
                Assert.IsTrue(order.ExpireDate.ToString() == newOrder.ExpireDate.ToString(), "ExpireDate differ");
                Assert.IsTrue(order.OrderSum == newOrder.OrderSum, "OrderSum differ");
                Assert.IsTrue(order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Initialized, "Initial Status is wrong");
                Assert.IsTrue(order.Saldo == 0, "Saldo is not 0");

                byte[] image = {1,2,3,4,5,6,7,8,9,10};

                order.SetImage(image);
                order = new PostNord.Portal.DigitalPostage.Bll.Orders(newOrder.Id);
                Assert.IsTrue(order.ImageSize == image.Length, "Image Size is not correct");

                order.AddLog(log1);
                DateTime logDate = salesDate.AddDays(-2);
                order.AddLog(log2, PostNord.Portal.DigitalPostage.Bll.OrderLogs.SeverityValues.Error, logDate);

                Assert.IsTrue(order.Logs.Count == 2, "OrderLog Count is wrong");

                Assert.IsTrue(order.Logs[0].Text == log2, "OrderLog Value is wrong");
                Assert.IsTrue(order.Logs[0].Severity == PostNord.Portal.DigitalPostage.Bll.OrderLogs.SeverityValues.Error, "OrderLog Direction is wrong");
                Assert.IsTrue(order.Logs[0].Timestamp.ToShortDateString() == logDate.ToShortDateString(), "OrderLog Value is wrong");
                Assert.IsTrue(order.Logs[1].Text == log1, "OrderLog Value is wrong");

                List<PostNord.Portal.DigitalPostage.Bll.Orders> orderList = PostNord.Portal.DigitalPostage.Bll.Orders.GetOrders(newOrder.SalesDate.Date, newOrder.SalesDate.AddDays(1).Date);
                Assert.IsTrue(orderList.Count > 0, "GetOrders does not return orders correctly");
                PostNord.Portal.DigitalPostage.Bll.Orders testOrder = orderList.FirstOrDefault(s => s.Id == newOrder.Id);
                Assert.IsTrue(testOrder != null, "GetOrders does not return specific order");
                Assert.IsTrue(testOrder.Id == newOrder.Id, "GetOrders does not return specific order Id");

            } 
        }
        
        [TestMethod]
        public void UpdateOrder()
        {
            PostNord.Portal.DigitalPostage.Bll.Orders otherOrder = new PostNord.Portal.DigitalPostage.Bll.Orders(id);

            otherOrder.Email += "1";
            otherOrder.ReceiptNumber += "1";
            otherOrder.CaptureId += "1";
            otherOrder.SalesDate = salesDate.AddDays(1);
            otherOrder.ExpireDate = expireDate.AddDays(1);
            otherOrder.Save();
            otherOrder = new PostNord.Portal.DigitalPostage.Bll.Orders(id);


            Assert.IsTrue(otherOrder.Id == newOrder.Id, "Ids does not differ");
            Assert.IsFalse(otherOrder.Email == newOrder.Email, "Emails does not differ");
            Assert.IsFalse(otherOrder.ReceiptNumber == newOrder.ReceiptNumber, "Receipts does not differ");
            Assert.IsFalse(otherOrder.CaptureId == newOrder.CaptureId, "CaptureId does not differ");
            Assert.IsFalse(otherOrder.SalesDate.ToString() == newOrder.SalesDate.ToString(), "SalesDate does not differ");
            Assert.IsFalse(otherOrder.ExpireDate.ToString() == newOrder.ExpireDate.ToString(), "ExpireDate does not differ");
        }
        
        [TestMethod]
        public void DeleteOrder()
        {
            PostNord.Portal.DigitalPostage.Bll.Orders delOrder = new PostNord.Portal.DigitalPostage.Bll.Orders(id);
            delOrder.Delete();
            delOrder = new PostNord.Portal.DigitalPostage.Bll.Orders(id);
            Assert.IsTrue(delOrder.Id == 0, "Order is not deleted");
            id = 0;
        }


    }
}
