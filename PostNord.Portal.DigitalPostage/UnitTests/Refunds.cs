﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using PostNord.Portal.DigitalPostage.Bll;

namespace UnitTests
{
    [TestClass]
    public class Refunds
    {
        public PostNord.Portal.DigitalPostage.Bll.Orders TestOrder = null;
        public PostNord.Portal.DigitalPostage.Bll.SubOrders SubOrder = null;
        
        [TestInitialize]
        public void Initialize()
        {
            UnitTests.Initialize.InitConfig();

            DalOrders dalOrders = new DalOrders();

            this.TestOrder = dalOrders.TestOrder();
            this.TestOrder.Save();
            Assert.IsTrue(this.TestOrder.Id > 0, "Order Id is ZERO!");

            this.SubOrder = DalSubOrders.TestSubOrder(this.TestOrder.Id,50,5,8);
            this.SubOrder.Save();
            Assert.IsTrue(SubOrder.Id > 0, "SubOrder Id is ZERO!");

        }

        [TestCleanup]
        public void CleanUp()
        {
            if (TestOrder.Id > 0)
            {
                TestOrder.Delete();
            }
        }

        [TestMethod]
        public void TestRefunds()
        {

            PostNord.Portal.DigitalPostage.Bll.Refunds newRefunds = new PostNord.Portal.DigitalPostage.Bll.Refunds();
            newRefunds.OrderNr = this.TestOrder.Id;
            newRefunds.Name = "Test Testersen";
            newRefunds.Address = "TestAdresse";
            newRefunds.PostNumber = 2620;
            newRefunds.Phone = "12346789";
            newRefunds.City = "Testerby";
            newRefunds.Message = "This is a test";
            newRefunds.Employee = "Employee";
            newRefunds.Timestamp = DateTime.Now;

            newRefunds.Save();

            Assert.IsTrue(newRefunds.Id > 0, "Refunds Id is not set");

            PostNord.Portal.DigitalPostage.Bll.Refunds testRefunds = new PostNord.Portal.DigitalPostage.Bll.Refunds(newRefunds.Id);

            Assert.IsTrue(newRefunds.Id == testRefunds.Id, "loaded Refunds Id does not match");
            Assert.IsTrue(newRefunds.Name == testRefunds.Name, "loaded Refunds Name does not match");
            Assert.IsTrue(newRefunds.Address == testRefunds.Address, "loaded Refunds Address does not match");
            Assert.IsTrue(newRefunds.PostNumber == testRefunds.PostNumber, "loaded Refunds PostNumber does not match");
            Assert.IsTrue(newRefunds.City == testRefunds.City, "loaded Refunds City does not match");
            Assert.IsTrue(newRefunds.Phone == testRefunds.Phone, "loaded Refunds Phone does not match");
            Assert.IsTrue(newRefunds.Message == testRefunds.Message, "loaded Refunds Message does not match");
            Assert.IsTrue(newRefunds.Employee == testRefunds.Employee, "loaded Refunds Employee does not match");
            Assert.IsTrue(newRefunds.Timestamp.ToShortDateString() == testRefunds.Timestamp.ToShortDateString(), "loaded Timestamps does not match");

            List<PostNord.Portal.DigitalPostage.Bll.Refunds> refundList = PostNord.Portal.DigitalPostage.Bll.Refunds.GetRefunds();
            Assert.IsTrue(refundList.Count > 0, "GetRefunds() does not return data correctly");
            testRefunds = refundList.FirstOrDefault(s => s.Id == newRefunds.Id);
            Assert.IsNotNull(testRefunds, "GetRefunds() does not return correct data");

            Assert.IsTrue(testRefunds.Id == newRefunds.Id, "GetRefunds() does not find data");

            refundList = PostNord.Portal.DigitalPostage.Bll.Refunds.GetRefunds(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(1));
            Assert.IsTrue(refundList.Count > 0, "GetRefunds(fromDate, toDate) does not return data correctly");

            testRefunds = refundList.FirstOrDefault(s => s.Id == newRefunds.Id);
            Assert.IsNotNull(testRefunds, "GetRefunds(fromDate, toDate) does not return correct data");
            Assert.IsTrue(testRefunds.Id == newRefunds.Id, "GetRefunds(fromDate, toDate) does not find data");

        
            
        }
    }
}
