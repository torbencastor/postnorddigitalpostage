﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    /// <summary>
    /// Summary description for DalSubOrders
    /// </summary>
    [TestClass]
    public class DalSubOrders
    {

        public PostNord.Portal.DigitalPostage.Bll.Orders TestOrder = null;
        public PostNord.Portal.DigitalPostage.Bll.SubOrders SubOrder = null;

        [TestInitialize]
        public void Initialize()
        {
            //UnitTests.Initialize.InitConfig();

            //DalOrders dalOrders = new DalOrders();

            //TestOrder = dalOrders.TestOrder();
            //TestOrder.Save();
            //Assert.IsTrue(TestOrder.Id > 0, "Order Id is ZERO!");
        }

        [TestCleanup]
        public void CleanUp()
        {
            if (TestOrder!= null && TestOrder.Id > 0)
            {
                TestOrder.Delete();
            }
        }

        public static PostNord.Portal.DigitalPostage.Bll.SubOrders TestSubOrder(long orderId, short weight, int amount, double price)
        {
            PostNord.Portal.DigitalPostage.Bll.SubOrders sub = new PostNord.Portal.DigitalPostage.Bll.SubOrders(orderId,"Indland" ,weight, "A", "Danmark", amount, price, 888,155);
            return sub;
        }
        
        [TestMethod]
        public void CreateSubOrder()
        {
            if (TestOrder.Id > 0)
            {
                SubOrder = TestSubOrder(TestOrder.Id, 50, 5, 8);
                SubOrder.Save();
                Assert.IsTrue(SubOrder.Id != 0, "SubOrder Id is not set");

                PostNord.Portal.DigitalPostage.Bll.SubOrders subOrderControl = PostNord.Portal.DigitalPostage.Bll.SubOrders.GetSubOrders(SubOrder.Id);
                Assert.IsTrue(subOrderControl != null, "Loaded subOrder is null");

                Assert.IsTrue(SubOrder.Id == subOrderControl.Id, "Ids differ");
                Assert.IsTrue(SubOrder.OrderId == subOrderControl.OrderId, "OrderId differ");
                Assert.IsTrue(SubOrder.Price == subOrderControl.Price, "Price differ");
                Assert.IsTrue(SubOrder.Amount == subOrderControl.Amount, "Amount differ");
                Assert.IsTrue(SubOrder.Priority == subOrderControl.Priority, "Priority differ");
                Assert.IsTrue(SubOrder.ProductId == subOrderControl.ProductId, "ProductId differ");
                Assert.IsTrue(SubOrder.SubOrderSum == subOrderControl.SubOrderSum, "SubOrderSum differ");
            }
        }


        [TestMethod]
        public void CreateMultipleOrders()
        {
            if (TestOrder.Id > 0)
            {
                double orderSum = 0;

                SubOrder = TestSubOrder(this.TestOrder.Id, 50, 15, 8);
                SubOrder.Save();

                orderSum += SubOrder.SubOrderSum;

                Assert.IsTrue(TestOrder.OrderSum == orderSum, "OrderSum differs. Expected: {0} Found: {1}", orderSum, TestOrder.OrderSum);

                SubOrder = TestSubOrder(this.TestOrder.Id, 100, 17, 10);
                SubOrder.Save();
                orderSum += SubOrder.SubOrderSum;
                Assert.IsTrue(TestOrder.OrderSum == orderSum, "OrderSum differs. Expected: {0} Found: {1}", orderSum, TestOrder.OrderSum);

                SubOrder = TestSubOrder(this.TestOrder.Id, 150, 23, 15);
                SubOrder.Save();
                orderSum += SubOrder.SubOrderSum;
                Assert.IsTrue(TestOrder.OrderSum == orderSum, "OrderSum differs. Expected: {0} Found: {1}", orderSum, TestOrder.OrderSum);

                TestOrder = new PostNord.Portal.DigitalPostage.Bll.Orders(SubOrder.OrderId);
                Assert.IsTrue(TestOrder.OrderSum == orderSum, "OrderSum is not correct Expected:{0} Found:{1}", orderSum, TestOrder.OrderSum);

                // Generate codes
                Assert.IsTrue(SubOrder.Codes.Count == 0, "Code Count is not Correct Expected: {0} Found: {1}", SubOrder.Amount, 0);
                TestOrder.Status = PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Paid;
                TestOrder.Save();
                foreach(PostNord.Portal.DigitalPostage.Bll.SubOrders sub in TestOrder.SubOrders)
                {
                    sub.CreateCodes();
                }

                // Test if codes are created
                Assert.IsTrue(SubOrder.Codes.Count == SubOrder.Amount, "Code Count is not Correct Expected: {0} Found: {1}", SubOrder.Amount, SubOrder.Codes.Count);

                // Do not delete order
                // TestOrder = null;
            }
        }
    }
}
