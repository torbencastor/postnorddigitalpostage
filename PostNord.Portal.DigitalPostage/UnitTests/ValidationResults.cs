﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    /// <summary>
    /// Summary description for ValidationErrors
    /// </summary>
    [TestClass]
    public class ValidationResults
    {
        PostNord.Portal.DigitalPostage.Bll.ValidationResults vaErrNew = null;

        [TestInitialize]
        public void Initialize()
        {
            UnitTests.Initialize.InitConfig();

            vaErrNew = new PostNord.Portal.DigitalPostage.Bll.ValidationResults();

            vaErrNew.CodeText = "ABCDEFG";
            vaErrNew.Employee = "Test Testersen";
            vaErrNew.ErrorCode = PostNord.Portal.DigitalPostage.Bll.ValidationResults.ValidationValues.OK;
            vaErrNew.CodeId = 20;
            vaErrNew.Timestamp = DateTime.Now;
            vaErrNew.Save();

            Assert.IsTrue(vaErrNew.Id > 0, "Id is not set");
        }

        [TestCleanup]
        public void CleanUp()
        {
            if (vaErrNew != null && vaErrNew.Id > 0)
            {
                vaErrNew.Delete();
            }
        }

        [TestMethod]
        public void TestValidationErrors()
        {
            PostNord.Portal.DigitalPostage.Bll.ValidationResults vaErrTest = new PostNord.Portal.DigitalPostage.Bll.ValidationResults(vaErrNew.Id);

            Assert.IsTrue(vaErrTest.Id == vaErrNew.Id, "Loaded Id is not correct");
            Assert.IsTrue(vaErrTest.CodeId == vaErrNew.CodeId, "Loaded CodeId is not correct");
            Assert.IsTrue(vaErrTest.CodeText == vaErrNew.CodeText, "Loaded CodeText is not correct");
            Assert.IsTrue(vaErrTest.Employee == vaErrNew.Employee, "Loaded Employee is not correct");
            Assert.IsTrue(vaErrTest.ErrorCode == vaErrNew.ErrorCode, "Loaded ErrorCode is not correct");
            Assert.IsTrue(vaErrTest.Timestamp.ToShortDateString() == vaErrNew.Timestamp.ToShortDateString(), "Loaded Timestamp is not correct");

            List<PostNord.Portal.DigitalPostage.Bll.ValidationResults> errList = PostNord.Portal.DigitalPostage.Bll.ValidationResults.GetValidationResults(DateTime.Now.AddDays(-1), DateTime.Now);
            Assert.IsTrue(errList.Count > 0, "GetValidationErrors does not return data correctly");

            vaErrTest = errList.First(s => s.Id == vaErrNew.Id);
            Assert.IsTrue(errList.Count > 0, "GetValidationErrors does not find data");

            string testCode = "1234567890AB";
            PostNord.Portal.DigitalPostage.Bll.ValidationResults.ValidationValues valVal = PostNord.Portal.DigitalPostage.Bll.ValidationResults.ValidateCode(testCode, vaErrTest.Employee);

            Assert.IsTrue(valVal == PostNord.Portal.DigitalPostage.Bll.ValidationResults.ValidationValues.DoesNotExist, "ErrorCode returned is not correct");
            errList = PostNord.Portal.DigitalPostage.Bll.ValidationResults.GetValidationResults(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(1));
            Assert.IsTrue(errList.Count > 1, "GetValidationErrors does not return data correctly for manual code");
            vaErrTest = errList.First(s => s.CodeText == testCode);
            Assert.IsTrue(vaErrTest.CodeText == testCode, "GetValidationErrors does not find Correct for manual code");

        }
    }
}
