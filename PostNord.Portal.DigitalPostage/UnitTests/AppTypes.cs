﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PostNord.Portal.DigitalPostage.Bll;

namespace UnitTests
{
    [TestClass]
    public class AppTypesTest
    {

        string platform = "Android";
        int version = 5;

        [TestInitialize]
        public void Initialize()
        {
            UnitTests.Initialize.InitConfig();
        }

        [TestMethod]
        public void CreateAppType()
        {
            AppTypes c = new AppTypes(platform, version);

            Assert.IsTrue(c.Id > 0, "Id is not set");
            Assert.IsTrue(c.Version == version, "Version is wrong");
            Assert.IsTrue(c.Platform == platform, "PlatForm is wrong");

            int id = c.Id;

            c = new AppTypes(c.Id);
            Assert.IsTrue(c.Id > 0, "Id is not set after load");
            Assert.IsTrue(c.Id == id, "Id is not correct after load");
            Assert.IsTrue(c.Version == version, "Version is wrong after load");
            Assert.IsTrue(c.Platform == platform, "PlatForm is wrong after load");

            c = new AppTypes(platform, version);
            Assert.IsTrue(c.Id > 0, "Id is not set after load 2");
            Assert.IsTrue(c.Id == id, "Id is not correct after load 2");
            Assert.IsTrue(c.Version == version, "Version is wrong after load 2");
            Assert.IsTrue(c.Platform == platform, "PlatForm is wrong after load 2");

        }
    }
}
