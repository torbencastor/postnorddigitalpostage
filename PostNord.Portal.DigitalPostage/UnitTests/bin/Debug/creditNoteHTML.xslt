﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes"/>
  <xsl:decimal-format name="danish" decimal-separator="," grouping-separator="." />

  <xsl:template match="Orders">

    <html lang="dk" xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"  />
        <style type="text/css">
          body,p,b,td,li,span {
          font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif;
          font-size:14px;
          }
        </style>
      </head>
      <body style="">
        <p>Du har forsøgt at bestille Mobilporto. Desværre er der opstået en teknisk fejl, der betyder, at du har fået trukket penge fra din konto, men ikke modtaget en portokode. Det er vi rigtig kede af.</p>
        <p>Vi har refunderet beløbet hos MobilePay.</p>
        <p>
          <table>
            <tr>
              <td>
                <b>Ordre nr. :</b>
              </td>
              <td style="color:#cc0000; padding-left:20px;">
                <xsl:value-of select="Id"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Tilbagebetalt beløb :</b>
              </td>
              <td style="padding-left:20px;">
                <xsl:call-template name="FormatPrice">
                  <xsl:with-param name="Price" select="OrderSum" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td>
                <b>Salgsdato :</b>
              </td>
              <td style="padding-left:20px;">
                <xsl:call-template name="FormatDate">
                  <xsl:with-param name="Date" select="SalesDate" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td>
                <b>Tilbagebetalingsdato :</b>
              </td>
              <td style="padding-left:20px;">
                <xsl:call-template name="FormatDate">
                  <xsl:with-param name="Date" select="ExpireDate" />
                </xsl:call-template>
              </td>
            </tr>
          </table>
        </p>

        <p>Beløbet vil blive refunderet til det samme kreditkort, som du har anvendt i MobilePay.</p>
        <p>
          <br/>
          <span style="font-size:14px;">
            <br/>
            <br/>
            <br/>
            <br/>
            Med venlig hilsen
            <br/>
            <br/>
          </span>
        </p>
        <p>
          Post Danmark A/S<br/>
          Tietgensgade 37<br/>
          1566 København V<br/>
          CVR: 26663903
        </p>
        <p>
          IBAN DK2530000001035886<br/>
          SWIFT-BIC DABADKKK
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date"/>

    <xsl:value-of select="concat(
                      substring($Date, 9, 2),
                      '-',
                      substring($Date, 6, 2),
                      '-',
                      substring($Date, 1, 4)
                      )"/>
  </xsl:template>

  <xsl:template name="FormatPrice">
    <xsl:param name="Price" />
    DKK <xsl:value-of select='format-number($Price, "#,00", "danish")'/>
  </xsl:template>


</xsl:stylesheet>
