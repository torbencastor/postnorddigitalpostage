﻿using iTextSharp.text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using PN.SoapLibrary.WSSecurity;
using PostNord.Portal.DigitalPostage.Bll;
using PostNord.Portal.DigitalPostage.Helpers;
using PostNord.Portal.DigitalPostage.MobilePay;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace UnitTests
{
    class Program
    {
        static string _xsltMail = string.Empty;
        static string _xsltPdf = string.Empty;

        static string RepayMobilePay(PostNord.Portal.DigitalPostage.Bll.Orders order, out string transactionId)
        {
            string result = transactionId = string.Empty;

            PnConfig configuration = new PnConfig()
            {
                ClientContentEncryptionPassword = ConfigSettings.GetText("ClientContentEncryptionPassword"),
                ClientContentEncryptionPath = ConfigSettings.GetText("ClientContentEncryptionPath"),
                ClientSignaturePassword = ConfigSettings.GetText("ClientSignaturePassword"),
                ClientSignaturePath = ConfigSettings.GetText("ClientSignaturePath"),
                DbContentEncryptionPath = ConfigSettings.GetText("DBContentEncryptionPath"),
                DbSignaturePath = ConfigSettings.GetText("DBSignaturePath")
            };

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            try
            {
                var dbOutput = Proxy.CallRefund(configuration, clientId, merchantId, order.MobilePayOrderNr, (decimal) order.OrderSum);
                transactionId = dbOutput.TransactionId;
                if (dbOutput.ReturnCode != "00") {
                    string error = string.Format("MobilePay Refund Error: Error: {0} ({1}) Reason: {2} ({3}) ", dbOutput.ReturnCode, PostNord.Portal.DigitalPostage.Helpers.MobilePay.V2Returncode(dbOutput.ReturnCode), dbOutput.ReasonCode, PostNord.Portal.DigitalPostage.Helpers.MobilePay.V2Reasoncode(dbOutput.ReasonCode));
                    order.AddLogError(error);
                    return error;
                }
            }
            catch (Exception ex)
            {
                return string.Format("MobilePay WS Error: {0}", ex.Message);
            }
            return string.Empty;
        }


        static string RecheckMobilePay(PostNord.Portal.DigitalPostage.Bll.Orders order, out string transactionId)
        {
            string result = transactionId = string.Empty;

            PnConfig configuration = new PnConfig()
            {
                ClientContentEncryptionPassword = ConfigSettings.GetText("ClientContentEncryptionPassword"),
                ClientContentEncryptionPath = ConfigSettings.GetText("ClientContentEncryptionPath"),
                ClientSignaturePassword = ConfigSettings.GetText("ClientSignaturePassword"),
                ClientSignaturePath = ConfigSettings.GetText("ClientSignaturePath"),
                DbContentEncryptionPath = ConfigSettings.GetText("DBContentEncryptionPath"),
                DbSignaturePath = ConfigSettings.GetText("DBSignaturePath")
            };

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            try
            {
                var dbOutput = Proxy.CallGetStatus(configuration, clientId, merchantId, order.MobilePayOrderNr);

                // if Return Code != 0 accept that WS is not used
                if (dbOutput.ReturnCode != PostNord.Portal.DigitalPostage.Helpers.MobilePay.RETURNCODE_OK)
                {
                    string errorDesc = string.Format("MobilePay Error! Error code: {0} ({1}) Reason code:{2} ({3})", dbOutput.ReturnCode, PostNord.Portal.DigitalPostage.Helpers.MobilePay.V2Returncode(dbOutput.ReturnCode), dbOutput.ReasonCode, PostNord.Portal.DigitalPostage.Helpers.MobilePay.V2Reasoncode(dbOutput.ReasonCode));
                    return errorDesc;
                }
                else
                {
                    if (dbOutput.LatestPaymentStatus == PostNord.Portal.DigitalPostage.Helpers.MobilePay.PAYMENTSTATUS_REFUNDED)
                    {
                        return "Order refunded.";
                    }
                    else if (dbOutput.LatestPaymentStatus != PostNord.Portal.DigitalPostage.Helpers.MobilePay.PAYMENTSTATUS_CAPTURED)
                    {
                        return "Order not captured.";
                    }
                    else if ((double)dbOutput.OriginalAmount != order.OrderSum)
                    {
                        return string.Format("Amount differs: Expected:{0:0.00} Got: {1:0.00}", order.OrderSum, dbOutput.OriginalAmount);
                    }
                    transactionId = dbOutput.OriginalTransactionId;
                    return string.Format("Order paid. TransactionId : {0}", dbOutput.OriginalTransactionId);
                }
            }
            catch (Exception ex)
            {
                return string.Format("MobilePay WS Error: {0}", ex.Message);
            }
        }


        private string GetXslt(string fileName)
        {
            string html = string.Empty;
            string xlst = System.IO.File.ReadAllText(fileName);
            return html;
        }

        /// <summary>
        /// Removes the XML tags and namespaces from html
        /// </summary>
        /// <param name="html">Raw HTML</param>
        /// <returns>Cleaned up HTML</returns>
        private static string RemoveXml(string html)
        {
            string pattern = "^<\\?xml.*\\?>";
            Regex reg = new Regex(pattern);
            return reg.Replace(html, "<!DOCTYPE html>" + Environment.NewLine);
        }

        private static string GetHtml(string xslt, Orders order)
        {
            string html;

            XslCompiledTransform xsl = new XslCompiledTransform();
            xsl.Load(new XmlTextReader(new StringReader(xslt)));

            string xml = order.Xml();
            order.ExpireDate = DateTime.Now;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            using (MemoryStream xmlStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml)))
            {
                XPathDocument xPathDoc = new XPathDocument(new StreamReader(xmlStream));
                StringBuilder htmlBuilder = new StringBuilder();

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Encoding = Encoding.UTF8;
                xmlSettings.OmitXmlDeclaration = true;
                xmlSettings.ConformanceLevel = ConformanceLevel.Document;

                XmlWriter xmlWriter = XmlWriter.Create(htmlBuilder, xmlSettings);
                xsl.Transform(xPathDoc, xmlWriter);
                html = htmlBuilder.ToString();
            }

            return RemoveXml(html);
        }


        /// <summary>
        /// Send refund mail to customer.
        /// </summary>
        /// <param name="order">The order.</param>
        private static void SendMail(Orders order)
        {
            string mailTitle = "Refundering vedr. køb af portokode";

            string bodyHtml = GetHtml(_xsltMail, order);
            SmtpClient mailClient = new SmtpClient(ConfigSettings.GetText("SMTPServer"));
            MailMessage message = new MailMessage(ConfigSettings.GetText("ReplyMailAddress"), order.Email);
            message.IsBodyHtml = true;
            message.Body = bodyHtml;
            message.Subject = mailTitle;
            message.SubjectEncoding = Encoding.UTF8;
            message.BodyEncoding = Encoding.UTF8;

            try
            {
                mailClient.Send(message);
            }
            catch (Exception ex)
            {
                order.AddLogError(string.Format("Error sending refundjob mail: {0}", ex.Message));
            }
        }


        static void LoopOrders(string[] args)
        {

            string creditnotaFile = "creditNoteHTML.xslt";

            Console.WriteLine("*** Looping orders ***");
            Console.WriteLine("Arguments <url> <daycount> <refund> <recheck>");
            Console.WriteLine("Example http://digitalpostage.dk 5 false true");
            string fileName = string.Format("Refunds_{0:yyyy-MM-dd_hhmmss}.csv", DateTime.Now);

            try
            {
                _xsltMail = System.IO.File.ReadAllText(creditnotaFile, Encoding.UTF8);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error loading credit note file: {0} ({1})", creditnotaFile, ex.Message);
                return;
            }

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            Console.WriteLine("Saving to file: {0}", fileName);

            string url = "http://dev";
            int dayCount = 100;
            bool refund = false;
            bool recheck = false;

            if (args.Length > 0) {
                url = args[0];
            }

            if (args.Length > 1) {
                int.TryParse(args[1], out dayCount);
            }

            if (args.Length > 2)
            {
                bool.TryParse(args[2], out refund);
                if (refund)
                {
                    Console.WriteLine("REFUNDING ENABLED!");
                }
            }

            if (args.Length > 3)
            {
                bool.TryParse(args[3], out recheck);
                if (recheck)
                {
                    Console.WriteLine("Checking all orders!");
                }
            }

            Initialize.LoadConfig(url);

            DateTime firstOrderDate = DateTime.Now.AddDays(-1*dayCount);
            firstOrderDate = firstOrderDate.AddHours(firstOrderDate.Hour * -1).AddMinutes(firstOrderDate.Minute * -1).AddSeconds(firstOrderDate.Second * -1);
            DateTime lastOrderDate = DateTime.Now;

            var orders = PostNord.Portal.DigitalPostage.Bll.Orders.GetOrders(firstOrderDate, lastOrderDate);
            Console.WriteLine("Total Number of orders: {0}", orders.Count);
            int mpOrderCount = orders.Count(order => order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentMobilePay);
            Console.WriteLine("Number of ReadyForPaymentMobilePay orders: {0}", mpOrderCount);
            int mpOrderNotProcessedCount = orders.Count(order => order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentMobilePay && order.CaptureId != "0");
            Console.WriteLine("Number of ReadyForPaymentMobilePay orders not processed: {0}", mpOrderNotProcessedCount);
            int paidCount = 0;
            int count = 0;
            string transactionId = string.Empty;
            string fileUrl = string.Empty;

            foreach(var order in orders)
            {
                bool refundOrder = false;

                // If MobilPay & Status is ReadyForPaymentMobilePay
                if (order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentMobilePay)
                {
                    if (order.SentToCint)
                    {
                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): SKIPPED - CINT", order.Id, order.SalesDate, order.Status);
                        continue;
                    }

                    // We have not looked up this order yet
                    if (order.CaptureId == string.Empty || recheck)
                    {
                        string res = RecheckMobilePay(order, out transactionId);
                        if (res.Contains("paid"))
                        {
                            paidCount++;
                            Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): {3} PAID", order.Id, order.SalesDate, order.Status, res);
                            refundOrder = true;
                        }
                        else if (res.Contains("refunded"))
                        {
                            Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): {3} REFUNDED", order.Id, order.SalesDate, order.Status, res);
                            order.Status = PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Refunded;
                            order.CaptureId = transactionId;
                            order.Save();
                        }
                        else
                        {
                            // Order is not paid!
                            order.CaptureId = "0";
                            order.Save();
                        }
                    }
                    count++;

                    //If order.captureId != 0 && order.captureId != "Refunded", order has been paid and should be refunded

                    if (refund)
                    {
                        if (refundOrder)
                        {
                            order.AddLogDebug("Scheduled MobilePay Refund");
                            string res = string.Empty;
                            res = RepayMobilePay(order, out transactionId);

                            if (res != string.Empty)
                            {
                                order.AddLogError(string.Format("Error refunding Manual MobilePay : {0}", res));
                            }
                            else
                            {
                                File.AppendAllText(fileName, string.Format("{0};{1:dd:MM:yyyy HH:mm:ss};{2};{3}{4}", order.Id, order.SalesDate, order.Email, order.OrderSum.ToString(CultureInfo.GetCultureInfo("da-DK").NumberFormat), Environment.NewLine), Encoding.UTF8);
                                order.CaptureId = transactionId;
                                order.Status = PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Refunded;
                                order.Save();
                                try
                                {
                                    SendMail(order);
                                }
                                catch (Exception ex)
                                {
                                    order.AddLogError(string.Format("Error sending mail : {0}", ex.Message));
                                }
                            }
                        }
                    }
                }
            }

            if (refund)
            {
                if (File.Exists(fileName))
                {
                    try
                    {
                        Console.WriteLine("Uploading log file");
                        using (SPSite site = new SPSite(url))
                        {
                            using (SPWeb web = site.OpenWeb("/admin"))
                            {

                                SPFolder myLibrary = web.Folders["Documents"];

                                // Prepare to upload
                                Boolean replaceExistingFiles = false;
                                FileStream fileStream = File.OpenRead(fileName);

                                //string uploadFilename = string.Format("Refunds_{0:yyyy-MM-dd_hhmmss}.csv", DateTime.Now);

                                // Upload document
                                SPFile spfile = myLibrary.Files.Add(fileName, fileStream, replaceExistingFiles);
                                spfile.CheckIn("Uploaded by Refund program", SPCheckinType.MajorCheckIn);

                                // Commit 
                                myLibrary.Update();

                                fileUrl = SPUtility.ConcatUrls(url, spfile.Url);
                                Console.WriteLine("logfile uploaded to: {0}", fileUrl);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error uploading log file: {0}", ex.Message);
                    }
                }
                else
                {
                    Console.WriteLine("File: {0} not found. File is NOT uploadet", fileName);
                }
            }

            Console.WriteLine("Number of orders with status ReadyForPaymentMobilePay: {0} Paid: {1}", count, paidCount);
            SendStatusMail(count, paidCount, fileUrl, dayCount, firstOrderDate, lastOrderDate);
        }

        /// <summary>
        /// Sends the status mail.
        /// </summary>
        /// <param name="numberOfMPOrders">The number of mp orders.</param>
        /// <param name="numberOfOrdersRefunded">The number of orders refunded.</param>
        /// <param name="url">The URL.</param>
        static void SendStatusMail(int numberOfMPOrders, int numberOfOrdersRefunded, string url, int dayCount, DateTime firstOrderDate, DateTime lastOrderDate)
        {
            string filename = "EmailNotifications.txt";
            string[] emails = null;
            try
            {
                emails = File.ReadAllLines(filename);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading file: '{0}' - {1}", filename, ex.Message);
                Console.WriteLine("No email sent.");
                return;
            }

            string mailTitle = "Refund job complete";

            string bodyHtml = string.Format("<p>Automatic refund job complete.</p><p>Number of orders processed:{0}<br/>Number of refunds made:{1}</br></p>", numberOfMPOrders, numberOfOrdersRefunded);
            bodyHtml += string.Format("<p>Order period: {0:dd:MM:yyyy HH:mm:ss} - {1:dd:MM:yyyy HH:mm:ss} ({2} days)</p>", firstOrderDate, lastOrderDate, dayCount);
            if (numberOfOrdersRefunded > 0)
            {
                bodyHtml += string.Format("<p>Log file uploaded to: <br/>{0}</p>", url);
            }
            else
            {
                bodyHtml += string.Format("<p>Log file not added</p>");
            }

            SmtpClient mailClient = new SmtpClient(ConfigSettings.GetText("SMTPServer"));

            foreach(string s in emails)
            {
                if (s.Contains('@'))
                {
                    try
                    {
                        Console.WriteLine("Sending mail to: {0}", s);
                        MailMessage message = new MailMessage(ConfigSettings.GetText("ReplyMailAddress"), s.Trim());
                        message.IsBodyHtml = true;
                        message.Body = bodyHtml;
                        message.Subject = mailTitle;
                        message.SubjectEncoding = Encoding.UTF8;
                        message.BodyEncoding = Encoding.UTF8;
                        mailClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error sending mail to : '{0}' - {1} ", s.Trim(), ex.Message);
                    }
                }
            }
        }



        static void TestV2(string[] args)
        {

            string url = "http://dev";
            Initialize.LoadConfig(url);

            PnConfig configuration = new PnConfig()
            {
                ClientContentEncryptionPassword = ConfigSettings.GetText("ClientContentEncryptionPassword"),
                ClientContentEncryptionPath = ConfigSettings.GetText("ClientContentEncryptionPath"),
                ClientSignaturePassword = ConfigSettings.GetText("ClientSignaturePassword"),
                ClientSignaturePath = ConfigSettings.GetText("ClientSignaturePath"),
                DbContentEncryptionPath = ConfigSettings.GetText("DBContentEncryptionPath"),
                DbSignaturePath = ConfigSettings.GetText("DBSignaturePath")
            };

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            string orderNr = "000001621237";
            //string requestNr = "1234";

            if (args.Length > 0)
            {
                orderNr = args[0];
            }

            Console.WriteLine("Testing order: {0}", orderNr);

            try
            {
                DB.MobilePay.GetStatus.Output dbOutput = Proxy.CallGetStatusSoapService(configuration, clientId, merchantId, orderNr);
                DateTime time = DateTime.MinValue;
                string price = string.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C} kr.", dbOutput.Amount);
                Console.WriteLine("V1 Amount: {0} TransactionId: {1} Captured: {2} Code: {3}", price, dbOutput.TransactionId, dbOutput.Captured, dbOutput.ReturnCode);
                Console.WriteLine();

                //DB.MobilePayV02.GetStatus.dacGetStatusOutput dbV2Output = Proxy.CallGetStatus(configuration, clientId, merchantId, orderNr, true);
                //Console.WriteLine("V2 Amount (test): {0} Captured: {1} TransactionId: {2}{8}Code: {3} ({4}) Reason:{5} ({6}) Count:{7}", dbV2Output.OriginalAmount, dbV2Output.LatestPaymentStatus, dbV2Output.OriginalTransactionId, dbV2Output.ReturnCode, MobilePay.V2Returncode(dbV2Output.ReturnCode), dbV2Output.ReasonCode, MobilePay.V2Reasoncode(dbV2Output.ReasonCode), dbV2Output.NumOfTransactions, Environment.NewLine);
                //if (dbV2Output.transactions != null && dbV2Output.transactions.Length > 0)
                //{
                //    Console.WriteLine("Transactions:");
                //    foreach (var transaction in dbV2Output.transactions)
                //    {
                //        Console.WriteLine("Amount: {0} Status: {1}, TimeStamp: {2}", transaction.Amount, transaction.PaymentStatus, transaction.TimeStamp);
                //    }
                //}
                //Console.WriteLine();

                DB.MobilePayV02.GetStatus.dacGetStatusOutput dbV2Output = Proxy.CallGetStatus(configuration, clientId, merchantId, orderNr, false);
                price = string.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C} kr.", dbV2Output.OriginalAmount);
                Console.WriteLine("V2 Amount: {0} Captured: '{1}' TransactionId: {2}{8}   Code: {3} ({4}) Reason:{5} ({6}) Count:{7}", price, dbV2Output.LatestPaymentStatus, dbV2Output.OriginalTransactionId, dbV2Output.ReturnCode, MobilePay.V2Returncode(dbV2Output.ReturnCode), dbV2Output.ReasonCode, MobilePay.V2Reasoncode(dbV2Output.ReasonCode), dbV2Output.NumOfTransactions, Environment.NewLine);
                if (dbV2Output.transactions != null && dbV2Output.transactions.Length > 0)
                {
                    Console.WriteLine("Transactions:");
                    foreach (var transaction in dbV2Output.transactions)
                    {
                        time = SPUtility.CreateDateTimeFromISO8601DateTimeString(transaction.TimeStamp);
                        price = string.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C} kr.", transaction.Amount);
                        Console.WriteLine("Amount: {0} Status: {1}, TimeStamp: {2:dd-MM-yyyy hh:mm:ss}", price, transaction.PaymentStatus, time);
                    }
                }
                Console.WriteLine();

             //   return;

                var captureOutput = Proxy.CallCapture(configuration, clientId, merchantId, orderNr, 10, true);
                Console.WriteLine("Capture (test): Remainder: {0} Code: {1} ({2}) Reason:{3} ({4})", captureOutput.RemainderAmount, captureOutput.ReturnCode, MobilePay.V2Returncode(captureOutput.ReturnCode), captureOutput.ReasonCode, MobilePay.V2Reasoncode(captureOutput.ReasonCode));
                return;
                Console.WriteLine();

                captureOutput = Proxy.CallCapture(configuration, clientId, merchantId, orderNr, 10, false);
                Console.WriteLine("Capture: Remainder: {0} Code: {1} ({2}) Reason:{3} ({4})", captureOutput.RemainderAmount, captureOutput.ReturnCode, MobilePay.V2Returncode(captureOutput.ReturnCode), captureOutput.ReasonCode, MobilePay.V2Reasoncode(captureOutput.ReasonCode));

                Console.WriteLine();

                var refundOutput = Proxy.CallRefund(configuration, clientId, merchantId, orderNr, 10, true);
                Console.WriteLine("Refund (test): Remainder: {0} Code: {1} ({2}) Reason:{3} ({4})", refundOutput.RemainderAmount, refundOutput.ReturnCode, MobilePay.V2Returncode(refundOutput.ReturnCode), refundOutput.ReasonCode, MobilePay.V2Reasoncode(refundOutput.ReasonCode));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }

        }



        static void Main(string[] args)
        {

            try
            {
                TestV2(args);

                //LoopOrders(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }
            Console.ReadLine();
            
        }
    }
}

