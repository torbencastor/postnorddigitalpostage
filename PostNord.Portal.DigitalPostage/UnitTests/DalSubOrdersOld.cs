﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    /// <summary>
    /// Summary description for DalSubOrders
    /// </summary>
    [TestClass]
    public class DalSubOrders
    {

        PostNord.Portal.DigitalPostage.Bll.Orders testOrder = null;

        [TestInitialize]
        public void Initialize()
        {
            DalOrders dalOrders = new DalOrders();

            testOrder = dalOrders.TestOrder();
            testOrder.Save();
            Assert.IsTrue(testOrder.Id > 0, "Order Id is ZERO!");
        }

        [TestCleanup]
        public void CleanUp()
        {
            if (testOrder.Id > 0)
            {
                testOrder.Delete();
            }
        }

        public PostNord.Portal.DigitalPostage.Bll.SubOrders TestSubOrder()
        {
            PostNord.Portal.DigitalPostage.Bll.SubOrders subOrder = new PostNord.Portal.DigitalPostage.Bll.SubOrders();
            subOrder = new PostNord.Portal.DigitalPostage.Bll.SubOrders();

            subOrder.OrderId = testOrder.Id;
            subOrder.Destination = "Danmark";
            subOrder.Weight = 50;
            subOrder.Price = 8;
            subOrder.Amount = 5;
            subOrder.Priority = "A";
            subOrder.ProductId = 1;
            subOrder.SubOrderSum = subOrder.Price * subOrder.Amount;

            return subOrder;
        }
        [TestMethod]
        public void CreateSubOrder()
        {
            if (testOrder.Id > 0)
            {
                PostNord.Portal.DigitalPostage.Bll.SubOrders subOrder = TestSubOrder();
                subOrder.Save();
                Assert.IsTrue(subOrder.Id != 0, "SubOrder Id is not set");

                PostNord.Portal.DigitalPostage.Bll.SubOrders subOrderControl = PostNord.Portal.DigitalPostage.Bll.SubOrders.SubOrdersSelectRow(subOrder.Id);
                Assert.IsTrue(subOrderControl != null, "Loaded subOrder is null");

                Assert.IsTrue(subOrder.Id == subOrderControl.Id, "Ids differ");
                Assert.IsTrue(subOrder.OrderId == subOrderControl.OrderId, "OrderId differ");
                Assert.IsTrue(subOrder.Price == subOrderControl.Price, "Price differ");
                Assert.IsTrue(subOrder.Amount == subOrderControl.Amount, "Amount differ");
                Assert.IsTrue(subOrder.Priority == subOrderControl.Priority, "Priority differ");
                Assert.IsTrue(subOrder.ProductId == subOrderControl.ProductId, "ProductId differ");
                Assert.IsTrue(subOrder.SubOrderSum == subOrderControl.SubOrderSum, "SubOrderSum differ");
            }
        }

    }
}
