﻿/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Refunds ADD
	Employee nvarchar(50) NULL
GO
ALTER TABLE dbo.Refunds SET (LOCK_ESCALATION = TABLE)
GO
COMMIT




GO
/****** Object:  StoredProcedure [dbo].[RefundsInsertRow]    Script Date: 03/17/2014 09:36:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: This SP Inserts value to Refunds table
-- ==========================================================================================


ALTER Procedure [dbo].[RefundsInsertRow]
@OrderNr bigint,
@Timestamp datetime,
@Name nvarchar(50),
@Address nvarchar(50),
@PostNumber int,
@City nvarchar(50),
@Phone nvarchar(50) = NULL,
@Message nvarchar(1000) = NULL,
@CreditSum float,
@Employee nvarchar(50)
AS
BEGIN

    INSERT INTO Refunds
        ([OrderNr], [Timestamp], [Name], [Address], [PostNumber], [City], [Phone], [Message], [CreditSum], [Employee])
    VALUES
        (@OrderNr, @Timestamp, @Name, @Address, @PostNumber, @City, @Phone, @Message, @CreditSum, @Employee)
    SELECT SCOPE_IDENTITY()
END

GO



ALTER Procedure [dbo].[RefundsUpdateRow]
@Id bigint,
@OrderNr bigint,
@Timestamp datetime,
@Name nvarchar(50),
@Address nvarchar(50),
@PostNumber int,
@City nvarchar(50),
@Phone nvarchar(50) = NULL,
@Message nvarchar(1000) = NULL,
@CreditSum float,
@Employee nvarchar(50)
AS
BEGIN
UPDATE Refunds SET [OrderNr] = @OrderNr, [Timestamp] = @Timestamp, [Name] = @Name, [Address] = @Address, [PostNumber] = @PostNumber, [City] = @City, [Phone] = @Phone, [Message] = @Message, [CreditSum] = @CreditSum, [Employee] = @Employee 
WHERE [Id] = @Id
END

GO
ALTER Procedure [dbo].[ReportingViewReimbursement]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT    dbo.Refunds.OrderNr, dbo.Refunds.Timestamp, dbo.Orders.Email, 
		  dbo.Orders.SalesDate, dbo.Orders.ExpireDate, dbo.Orders.OrderSum, 
          dbo.Refunds.CreditSum, dbo.Orders.Saldo, dbo.Refunds.Name, 
          dbo.Refunds.Address, dbo.Refunds.PostNumber, dbo.Refunds.City, 
          dbo.Refunds.Phone, dbo.Refunds.Message, dbo.Refunds.Employee
FROM         dbo.Refunds INNER JOIN
                      dbo.Orders ON dbo.Refunds.OrderNr = dbo.Orders.Id
WHERE dbo.Refunds.Timestamp>=@FromDate AND dbo.Refunds.Timestamp<=@ToDate AND dbo.Orders.OrderStatus = '2'

ORDER BY dbo.Refunds.CreditSum DESC

END

GO



