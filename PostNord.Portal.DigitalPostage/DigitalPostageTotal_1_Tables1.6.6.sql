﻿
GO
/****** Object:  Table [dbo].[NextKey]    Script Date: 04/07/2014 14:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NextKey](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CodeId] [int] NOT NULL,
	[Price] [float] NOT NULL,
 CONSTRAINT [PK_NextKey] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 04/07/2014 14:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 04/07/2014 14:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[ReceiptNumber] [nvarchar](50) NOT NULL,
	[CaptureId] [nvarchar](50) NOT NULL,
	[LabelImage] [varbinary](max) NULL,
	[ImageSize] [int] NOT NULL,
	[SalesDate] [datetime] NOT NULL,
	[ExpireDate] [datetime] NOT NULL,
	[OrderSum] [float] NOT NULL,
	[CreditSum] [float] NOT NULL,
	[Saldo] [float] NOT NULL,
	[OrderStatus] [smallint] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ValidationStatus]    Script Date: 04/07/2014 14:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidationStatus](
	[Id] [int] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ValidationStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ValidationResults]    Script Date: 04/07/2014 14:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidationResults](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CodeId] [bigint] NULL,
	[CodeText] [nvarchar](15) NULL,
	[Employee] [nvarchar](50) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[ErrorCode] [int] NOT NULL,
 CONSTRAINT [PK_ValidationErrors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubOrders]    Script Date: 04/07/2014 14:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubOrders](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Weight] [smallint] NOT NULL,
	[Priority] [nvarchar](50) NOT NULL,
	[Destination] [nvarchar](50) NOT NULL,
	[Amount] [int] NOT NULL,
	[Price] [float] NULL,
	[SubOrderSum] [float] NOT NULL,
	[SubOrderCreditSum] [float] NOT NULL,
	[SubOrderSaldo] [float] NOT NULL,
 CONSTRAINT [PK_SubOrders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Refunds]    Script Date: 04/07/2014 14:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Refunds](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderNr] [bigint] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](50) NOT NULL,
	[PostNumber] [int] NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NULL,
	[Message] [nvarchar](1000) NULL,
	[CreditSum] [float] NOT NULL,
	[Employee] [nvarchar](50) NULL,
 CONSTRAINT [PK_Refunds] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderLogs]    Script Date: 04/07/2014 14:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Severity] [smallint] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_OrderLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Codes]    Script Date: 04/07/2014 14:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Codes](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PostCode] [nvarchar](50) NOT NULL,
	[SubOrderId] [bigint] NOT NULL,
	[CodeId] [int] NOT NULL,
	[Status] [smallint] NOT NULL,
	[UsedDate] [datetime] NULL,
	[Price] [float] NOT NULL,
	[IssueDate] [datetime] NOT NULL,
	[ExpireDate] [datetime] NOT NULL,
	[RefundedDate] [datetime] NULL,
	[RefundId] [bigint] NULL,
 CONSTRAINT [PK_Codes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Codes_Refunds]    Script Date: 04/07/2014 14:31:07 ******/
ALTER TABLE [dbo].[Codes]  WITH CHECK ADD  CONSTRAINT [FK_Codes_Refunds] FOREIGN KEY([RefundId])
REFERENCES [dbo].[Refunds] ([Id])
GO
ALTER TABLE [dbo].[Codes] CHECK CONSTRAINT [FK_Codes_Refunds]
GO
/****** Object:  ForeignKey [FK_Codes_SubOrders]    Script Date: 04/07/2014 14:31:07 ******/
ALTER TABLE [dbo].[Codes]  WITH CHECK ADD  CONSTRAINT [FK_Codes_SubOrders] FOREIGN KEY([SubOrderId])
REFERENCES [dbo].[SubOrders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Codes] CHECK CONSTRAINT [FK_Codes_SubOrders]
GO
/****** Object:  ForeignKey [FK_OrderLogs_Orders]    Script Date: 04/07/2014 14:31:07 ******/
ALTER TABLE [dbo].[OrderLogs]  WITH CHECK ADD  CONSTRAINT [FK_OrderLogs_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderLogs] CHECK CONSTRAINT [FK_OrderLogs_Orders]
GO
/****** Object:  ForeignKey [FK_Refunds_Orders]    Script Date: 04/07/2014 14:31:07 ******/
ALTER TABLE [dbo].[Refunds]  WITH CHECK ADD  CONSTRAINT [FK_Refunds_Orders] FOREIGN KEY([OrderNr])
REFERENCES [dbo].[Orders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Refunds] CHECK CONSTRAINT [FK_Refunds_Orders]
GO
/****** Object:  ForeignKey [FK_SubOrders_Orders]    Script Date: 04/07/2014 14:31:07 ******/
ALTER TABLE [dbo].[SubOrders]  WITH CHECK ADD  CONSTRAINT [FK_SubOrders_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubOrders] CHECK CONSTRAINT [FK_SubOrders_Orders]
GO
/****** Object:  ForeignKey [FK_SubOrders_Products]    Script Date: 04/07/2014 14:31:07 ******/
ALTER TABLE [dbo].[SubOrders]  WITH CHECK ADD  CONSTRAINT [FK_SubOrders_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[SubOrders] CHECK CONSTRAINT [FK_SubOrders_Products]
GO
/****** Object:  ForeignKey [FK_ValidationResults_ValidationStat]    Script Date: 04/07/2014 14:31:07 ******/
ALTER TABLE [dbo].[ValidationResults]  WITH CHECK ADD  CONSTRAINT [FK_ValidationResults_ValidationStat] FOREIGN KEY([ErrorCode])
REFERENCES [dbo].[ValidationStatus] ([Id])
GO
ALTER TABLE [dbo].[ValidationResults] CHECK CONSTRAINT [FK_ValidationResults_ValidationStat]
GO




INSERT INTO [dbo].[ValidationStatus] VALUES (1,'OK');
INSERT INTO [dbo].[ValidationStatus] VALUES (2,'Used');
INSERT INTO [dbo].[ValidationStatus] VALUES (4,'Expired');
INSERT INTO [dbo].[ValidationStatus] VALUES (8,'Disabled');
INSERT INTO [dbo].[ValidationStatus] VALUES (16,'Refunded');
INSERT INTO [dbo].[ValidationStatus] VALUES (32,'Does not exist');

GO

