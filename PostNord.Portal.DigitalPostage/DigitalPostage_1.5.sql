﻿GO
/****** Object:  StoredProcedure [dbo].[CodeIdsUpdateRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsUpdateRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP updates CodeIds table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[CodeIdsUpdateRow]
@Id bigint
AS
BEGIN

UPDATE CodeIds WITH (ROWLOCK) SET [CodeId] = [CodeId] + 1
OUTPUT INSERTED.CodeId
WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSetRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSetRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP updates CodeIds table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[CodeIdsSetRow]
@Id bigint,
@CodeId int
AS
BEGIN

UPDATE CodeIds WITH (ROWLOCK) SET [CodeId] = @CodeId
OUTPUT INSERTED.CodeId
WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSelectRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP select a specify row from CodeIds
-- ==========================================================================================


Create Procedure [dbo].[CodeIdsSelectRow]
@CodeId int,
@CodeDate date
AS
BEGIN

SELECT * FROM CodeIds WHERE [CodeId]=@CodeId and [CodeDate]=@CodeDate

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSelectDateRange]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSelectDateRange
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP select a specify row from CodeIds
-- ==========================================================================================


Create Procedure [dbo].[CodeIdsSelectDateRange]
@FromDate date,
@ToDate date
AS
BEGIN

SELECT * FROM CodeIds WHERE [CodeDate]>=@FromDate AND [CodeDate]<=@ToDate

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSelectDate]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSelectDate
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP select a specify row from CodeIds
-- ==========================================================================================


CREATE Procedure [dbo].[CodeIdsSelectDate]
@CodeDate date
AS
BEGIN

SELECT * FROM CodeIds WITH (HOLDLOCK) WHERE [CodeDate]=@CodeDate

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSelectAll]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSelectAll
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: Select all rows form CodeIds
-- ==========================================================================================



Create Procedure [dbo].[CodeIdsSelectAll]
AS
BEGIN

SELECT * FROM CodeIds

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsInsertRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsInsertRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP Inserts value to CodeIds table
-- ==========================================================================================


Create Procedure [dbo].[CodeIdsInsertRow]
@CodeId int,
@CodeDate date
AS
BEGIN

    INSERT INTO CodeIds
        ([CodeId], [CodeDate])
    VALUES
        (@CodeId, @CodeDate)
END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsDeleteRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsDeleteRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP delete specify row from CodeIds table
-- ==========================================================================================


CREATE Procedure [dbo].[CodeIdsDeleteRow]
@Id bigint
AS
BEGIN

DELETE CodeIds WHERE [Id] = @Id

END
GO
/****** Object:  Table [dbo].[NextKey]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NextKey](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CodeId] [int] NOT NULL,
	[Price] [float] NOT NULL,
 CONSTRAINT [PK_NextKey] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[ReceiptNumber] [nvarchar](50) NOT NULL,
	[CaptureId] [nvarchar](50) NOT NULL,
	[LabelImage] [varbinary](max) NULL,
	[ImageSize] [int] NOT NULL,
	[SalesDate] [datetime] NOT NULL,
	[ExpireDate] [datetime] NOT NULL,
	[OrderSum] [float] NOT NULL,
	[CreditSum] [float] NOT NULL,
	[Saldo] [float] NOT NULL,
	[OrderStatus] [smallint] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Products]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ValidationStatus]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidationStatus](
	[Id] [int] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ValidationStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectDate]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/10/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Orders table
-- ==========================================================================================
CREATE Procedure [dbo].[OrdersSelectDate]
@FromDate datetime,
@ToDate datetime
AS
BEGIN

SELECT * FROM Orders WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate ORDER BY SalesDate ASC

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectCaptureId]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectCaptureId
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/21/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Orders table
-- ==========================================================================================
CREATE Procedure [dbo].[OrdersSelectCaptureId]
@CaptureId nvarchar(50)

AS
BEGIN

SELECT * FROM Orders WHERE [CaptureId] LIKE @CaptureId ORDER BY SalesDate ASC

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectAll]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: Select all rows form Orders
-- ==========================================================================================



Create Procedure [dbo].[OrdersSelectAll]
AS
BEGIN

SELECT * FROM Orders

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersInsertRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: This SP Inserts value to Orders table
-- ==========================================================================================


CREATE Procedure [dbo].[OrdersInsertRow]
@Email nvarchar(50),
@ReceiptNumber nvarchar(50),
@CaptureId nvarchar(50),
@SalesDate datetime,
@ExpireDate datetime,
@OrderSum float,
@CreditSum float,
@Saldo float,
@OrderStatus smallint
AS
BEGIN

    INSERT INTO Orders
        ([Email], [ReceiptNumber], [CaptureId], [ImageSize], [SalesDate], [ExpireDate], [OrderSum], [CreditSum], [Saldo], [OrderStatus])
    VALUES
        (@Email, @ReceiptNumber, @CaptureId, 0, @SalesDate, @ExpireDate, @OrderSum, @CreditSum, @Saldo, @OrderStatus)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[OrdersDeleteRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: This SP delete specify row from Orders table
-- ==========================================================================================


Create Procedure [dbo].[OrdersDeleteRow]
@Id bigint
AS
BEGIN

DELETE Orders WHERE [Id] = @Id

END
GO
/****** Object:  Table [dbo].[SubOrders]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubOrders](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Weight] [smallint] NOT NULL,
	[Priority] [nvarchar](50) NOT NULL,
	[Destination] [nvarchar](50) NOT NULL,
	[Amount] [int] NOT NULL,
	[Price] [float] NULL,
	[SubOrderSum] [float] NOT NULL,
	[SubOrderCreditSum] [float] NOT NULL,
	[SubOrderSaldo] [float] NOT NULL,
 CONSTRAINT [PK_SubOrders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ValidationResults]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidationResults](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CodeId] [bigint] NULL,
	[CodeText] [nvarchar](15) NULL,
	[Employee] [nvarchar](50) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[ErrorCode] [int] NOT NULL,
 CONSTRAINT [PK_ValidationErrors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Refunds]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Refunds](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderNr] [bigint] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](50) NOT NULL,
	[PostNumber] [int] NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NULL,
	[Message] [nvarchar](1000) NULL,
	[CreditSum] [float] NOT NULL,
	[Employee] [nvarchar](50) NULL,
 CONSTRAINT [PK_Refunds] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[ProductsUpdateRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: This SP updates Products table rows.
-- ==========================================================================================


Create Procedure [dbo].[ProductsUpdateRow]
@Id bigint,
@Name nvarchar(50)
AS
BEGIN

UPDATE Products SET [Name] = @Name WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductsSelectRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: This SP select a specify row from Products
-- ==========================================================================================


Create Procedure [dbo].[ProductsSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM Products WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductsSelectAll]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: Select all rows form Products
-- ==========================================================================================



Create Procedure [dbo].[ProductsSelectAll]
AS
BEGIN

SELECT * FROM Products

END
GO
/****** Object:  StoredProcedure [dbo].[ProductsInsertRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: This SP Inserts value to Products table
-- ==========================================================================================


CREATE Procedure [dbo].[ProductsInsertRow]
@Name nvarchar(50)
AS
BEGIN

    INSERT INTO Products
        ([Name])
    VALUES
        (@Name)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[ProductsDeleteRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: This SP delete specify row from Products table
-- ==========================================================================================


Create Procedure [dbo].[ProductsDeleteRow]
@Id bigint
AS
BEGIN

DELETE Products WHERE [Id] = @Id

END
GO
/****** Object:  Table [dbo].[OrderLogs]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Severity] [smallint] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_OrderLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[NextKeyUpdateRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[NextKeyUpdateRow]
@Price float,
@CodeId int
AS
BEGIN

UPDATE NextKey SET [CodeId] = @CodeId OUTPUT INSERTED.CodeId 
WHERE [Price] = @Price

END

Grant EXECUTE On [NextKeySelectRow] To [DigitalPostageTest]
Grant EXECUTE On [NextKeyUpdateKey] To [DigitalPostageTest]
Grant EXECUTE On [NextKeyInsertRow] To [DigitalPostageTest]
Grant EXECUTE On [NextKeyUpdateRow] To [DigitalPostageTest]
GO
/****** Object:  StoredProcedure [dbo].[NextKeyUpdateKey]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: NextKeyUpdateRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 3/9/2014 5:53:45 AM
-- Description: This SP updates NextKey table rows.
-- ==========================================================================================


Create Procedure [dbo].[NextKeyUpdateKey]
@Price float,
@CodeCount int

AS
BEGIN

UPDATE NextKey SET [CodeId] = [CodeId] + @CodeCount 
OUTPUT INSERTED.CodeId 
WHERE [Price] = @Price

END
GO
/****** Object:  StoredProcedure [dbo].[NextKeySelectRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: NextKeySelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 3/9/2014 5:53:45 AM
-- Description: This SP select a specify row from NextKey
-- ==========================================================================================


Create Procedure [dbo].[NextKeySelectRow]
@Price float
AS
BEGIN

SELECT CodeId FROM NextKey 
WHERE [Price] = @Price

END
GO
/****** Object:  StoredProcedure [dbo].[NextKeyInsertRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: NextKeyInsertRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 3/9/2014 5:53:45 AM
-- Description: This SP Inserts value to NextKey table
-- ==========================================================================================


Create Procedure [dbo].[NextKeyInsertRow]
@Price float,
@CodeId int
AS
BEGIN

    INSERT INTO NextKey
        ([CodeId], [Price])
	OUTPUT INSERTED.CodeId 
    VALUES
        (@CodeId, @Price)
END
GO
/****** Object:  StoredProcedure [dbo].[OrdersUpdateRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: This SP updates Orders table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[OrdersUpdateRow]
@Id bigint,
@Email nvarchar(50),
@ReceiptNumber nvarchar(50),
@CaptureId nvarchar(50),
@SalesDate datetime,
@ExpireDate datetime,
@OrderSum float,
@CreditSum float,
@Saldo float,
@OrderStatus smallint

AS
BEGIN

UPDATE Orders SET [Email] = @Email, [ReceiptNumber] = @ReceiptNumber, [CaptureId] = @CaptureId, [SalesDate] = @SalesDate, [ExpireDate] = @ExpireDate, [OrderSum] = @OrderSum, [CreditSum] = @CreditSum, [Saldo] = @Saldo, [OrderStatus] = @OrderStatus WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersUpdateImage]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Entity Name: OrdersUpdateImage
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 06-01-2013 15:12:34
-- Description: This SP updates Orders table rows.
-- =============================================
CREATE Procedure [dbo].[OrdersUpdateImage]
@Id bigint,
@LabelImage varbinary(max),
@ImageSize int
AS
BEGIN

UPDATE Orders SET [LabelImage] = @LabelImage, [ImageSize] = @ImageSize WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: This SP select a specify row from Orders
-- ==========================================================================================


Create Procedure [dbo].[OrdersSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM Orders WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectDateEmail]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectDateEmail
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/10/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Orders table
-- ==========================================================================================
CREATE Procedure [dbo].[OrdersSelectDateEmail]
@FromDate datetime,
@ToDate datetime,
@Email nvarchar(50)

AS
BEGIN

SELECT * FROM Orders WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND [Email] LIKE @Email ORDER BY SalesDate ASC

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsUpdateRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: This SP updates OrderLogs table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[OrderLogsUpdateRow]
@Id bigint,
@OrderId bigint,
@Timestamp datetime,
@Severity smallint,
@Text nvarchar(MAX)
AS
BEGIN

UPDATE OrderLogs SET [OrderId] = @OrderId, [Timestamp] = @Timestamp, [Severity] = @Severity, [Text] = @Text WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsSelectRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: This SP select a specify row from OrderLogs
-- ==========================================================================================


Create Procedure [dbo].[OrderLogsSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM OrderLogs WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsSelectForOrder]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: Select all rows form OrderLogs matching
-- ==========================================================================================


CREATE Procedure [dbo].[OrderLogsSelectForOrder]
@OrderId bigint
AS
BEGIN

SELECT * FROM OrderLogs WHERE [OrderId] = @OrderId ORDER BY [Timestamp] ASC

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsSelectAll]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: Select all rows form OrderLogs
-- ==========================================================================================



Create Procedure [dbo].[OrderLogsSelectAll]
AS
BEGIN

SELECT * FROM OrderLogs

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsInsertRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: This SP Inserts value to OrderLogs table
-- ==========================================================================================


CREATE Procedure [dbo].[OrderLogsInsertRow]
@OrderId bigint,
@Timestamp datetime,
@Severity smallint,
@Text nvarchar(MAX)
AS
BEGIN

    INSERT INTO OrderLogs
        ([OrderId], [Timestamp], [Severity], [Text])
    VALUES
        (@OrderId, @Timestamp, @Severity, @Text)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsDeleteRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: This SP delete specify row from OrderLogs table
-- ==========================================================================================


Create Procedure [dbo].[OrderLogsDeleteRow]
@Id bigint
AS
BEGIN

DELETE OrderLogs WHERE [Id] = @Id

END
GO
/****** Object:  Table [dbo].[Codes]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Codes](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PostCode] [nvarchar](50) NOT NULL,
	[SubOrderId] [bigint] NOT NULL,
	[CodeId] [int] NOT NULL,
	[Status] [smallint] NOT NULL,
	[UsedDate] [datetime] NULL,
	[Price] [float] NOT NULL,
	[IssueDate] [datetime] NOT NULL,
	[ExpireDate] [datetime] NOT NULL,
	[RefundedDate] [datetime] NULL,
	[RefundId] [bigint] NULL,
 CONSTRAINT [PK_Codes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewReimbursement]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ReportingViewReimbursement]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT    dbo.Refunds.OrderNr, dbo.Refunds.Timestamp, dbo.Orders.Email, 
		  dbo.Orders.SalesDate, dbo.Orders.ExpireDate, dbo.Orders.OrderSum, 
          dbo.Refunds.CreditSum, dbo.Orders.Saldo, dbo.Refunds.Name, 
          dbo.Refunds.Address, dbo.Refunds.PostNumber, dbo.Refunds.City, 
          dbo.Refunds.Phone, dbo.Refunds.Message, dbo.Refunds.Employee
FROM         dbo.Refunds INNER JOIN
                      dbo.Orders ON dbo.Refunds.OrderNr = dbo.Orders.Id
WHERE dbo.Refunds.Timestamp>=@FromDate AND dbo.Refunds.Timestamp<=@ToDate AND dbo.Orders.OrderStatus = '2'

ORDER BY dbo.Refunds.CreditSum DESC

END
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewByCustomers]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ReportingViewByCustomers]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT  SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
		SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
		SUM(dbo.SubOrders.SubOrderSaldo) AS Saldo, 
		SUM(dbo.SubOrders.Amount) AS Amount, 
		dbo.Orders.Email
FROM    dbo.Orders INNER JOIN dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2'                     

GROUP BY dbo.Orders.Email
ORDER BY OrderSum DESC

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsUpdateRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: This SP updates Refunds table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[RefundsUpdateRow]
@Id bigint,
@OrderNr bigint,
@Timestamp datetime,
@Name nvarchar(50),
@Address nvarchar(50),
@PostNumber int,
@City nvarchar(50),
@Phone nvarchar(50) = NULL,
@Message nvarchar(1000) = NULL,
@CreditSum float,
@Employee nvarchar(50)
AS
BEGIN
UPDATE Refunds SET [OrderNr] = @OrderNr, [Timestamp] = @Timestamp, [Name] = @Name, [Address] = @Address, [PostNumber] = @PostNumber, [City] = @City, [Phone] = @Phone, [Message] = @Message, [CreditSum] = @CreditSum, [Employee] = @Employee 
WHERE [Id] = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[RefundsSelectRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: This SP select a specify row from Refunds
-- ==========================================================================================


Create Procedure [dbo].[RefundsSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM Refunds WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsSelectOrderId]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectOrderId
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: Select all rows form Refunds matching orderNr
-- ==========================================================================================



Create Procedure [dbo].[RefundsSelectOrderId]
@OrderNr bigint
AS
BEGIN

SELECT * FROM Refunds WHERE OrderNr=@OrderNr ORDER BY Timestamp ASC

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsSelectDate]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/3/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Refunds table
-- ==========================================================================================
CREATE Procedure [dbo].[RefundsSelectDate]
@FromDate datetime,
@ToDate datetime
AS
BEGIN

SELECT * FROM Refunds WHERE [Timestamp]>=@FromDate AND [Timestamp]<=@ToDate ORDER BY Timestamp ASC

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsSelectAll]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: Select all rows form Refunds
-- ==========================================================================================



CREATE Procedure [dbo].[RefundsSelectAll]
AS
BEGIN

SELECT * FROM Refunds ORDER BY Timestamp ASC

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsInsertRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: This SP Inserts value to Refunds table
-- ==========================================================================================


CREATE Procedure [dbo].[RefundsInsertRow]
@OrderNr bigint,
@Timestamp datetime,
@Name nvarchar(50),
@Address nvarchar(50),
@PostNumber int,
@City nvarchar(50),
@Phone nvarchar(50) = NULL,
@Message nvarchar(1000) = NULL,
@CreditSum float,
@Employee nvarchar(50)
AS
BEGIN

    INSERT INTO Refunds
        ([OrderNr], [Timestamp], [Name], [Address], [PostNumber], [City], [Phone], [Message], [CreditSum], [Employee])
    VALUES
        (@OrderNr, @Timestamp, @Name, @Address, @PostNumber, @City, @Phone, @Message, @CreditSum, @Employee)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[RefundsDeleteRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: This SP delete specify row from Refunds table
-- ==========================================================================================


Create Procedure [dbo].[RefundsDeleteRow]
@Id bigint
AS
BEGIN

DELETE Refunds WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersUpdateRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: This SP updates SubOrders table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[SubOrdersUpdateRow]
@Id bigint,
@OrderId bigint,
@ProductId bigint,
@Weight smallint,
@Priority nvarchar(50),
@Destination nvarchar(50),
@Amount int,
@Price float,
@SubOrderSum float,
@SubOrderCreditSum float,
@SubOrderSaldo float
AS
BEGIN

UPDATE SubOrders SET [OrderId] = @OrderId, [ProductId] = @ProductId, [Weight] = @Weight, [Priority] = @Priority, [Destination] = @Destination, [Amount] = @Amount, [Price] = @Price, [SubOrderSum] = @SubOrderSum, [SubOrderCreditSum] = @SubOrderCreditSum, [SubOrderSaldo] = @SubOrderSaldo WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersSelectRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: This SP select a specify row from SubOrders
-- ==========================================================================================


Create Procedure [dbo].[SubOrdersSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM SubOrders WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersSelectFromOrderId]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[SubOrdersSelectFromOrderId]
@OrderId bigint

AS
BEGIN

SELECT * FROM SubOrders WHERE [OrderId] = @OrderId

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersSelectAll]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: Select all rows form SubOrders
-- ==========================================================================================



Create Procedure [dbo].[SubOrdersSelectAll]
AS
BEGIN

SELECT * FROM SubOrders

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersInsertRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: This SP Inserts value to SubOrders table
-- ==========================================================================================


CREATE Procedure [dbo].[SubOrdersInsertRow]
@OrderId bigint,
@ProductId bigint,
@Weight smallint,
@Priority nvarchar(50),
@Destination nvarchar(50),
@Amount int,
@Price float,
@SubOrderSum float,
@SubOrderCreditSum float,
@SubOrderSaldo float
AS
BEGIN

    INSERT INTO SubOrders
        ([OrderId], [ProductId], [Weight], [Priority], [Destination], [Amount], [Price], [SubOrderSum], [SubOrderCreditSum], SubOrderSaldo)
    VALUES
        (@OrderId, @ProductId, @Weight, @Priority, @Destination, @Amount, @Price, @SubOrderSum, @SubOrderCreditSum, @SubOrderSaldo)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersDeleteRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: This SP delete specify row from SubOrders table
-- ==========================================================================================


Create Procedure [dbo].[SubOrdersDeleteRow]
@Id bigint
AS
BEGIN

DELETE SubOrders WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsUpdateRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsUpdateRow
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: This SP updates ValidationResults table rows.
-- ==========================================================================================


Create Procedure [dbo].[ValidationResultsUpdateRow]
@Id bigint,
@CodeId bigint,
@CodeText nvarchar(15) = NULL,
@Employee nvarchar(50),
@Timestamp datetime,
@ErrorCode int
AS
BEGIN

UPDATE ValidationResults SET [CodeId] = @CodeId, [CodeText] = @CodeText, [Employee] = @Employee, [Timestamp] = @Timestamp, [ErrorCode] = @ErrorCode WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsSelectRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsSelectRow
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: This SP select a specify row from ValidationResults
-- ==========================================================================================


Create Procedure [dbo].[ValidationResultsSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM ValidationResults WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsSelectDateAndCode]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsSelectDateAndCode
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/3/2014 7:11:27 AM
-- Description: Select ValidationResults from daterange and CodeId
-- ==========================================================================================

CREATE Procedure [dbo].[ValidationResultsSelectDateAndCode]
@FromDate datetime,
@ToDate datetime,
@CodeId bigint
AS
BEGIN

SELECT * FROM ValidationResults WHERE [CodeId]=@CodeId AND [Timestamp]>=@FromDate AND [Timestamp]<=@ToDate ORDER BY Timestamp ASC

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsSelectDate]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsSelectDate
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/3/2014 7:11:27 AM
-- Description: This SP Inserts value to ValidationErrors table
-- ==========================================================================================

CREATE Procedure [dbo].[ValidationResultsSelectDate]
@FromDate datetime,
@ToDate datetime
AS
BEGIN

SELECT * FROM ValidationResults WHERE [Timestamp]>=@FromDate AND [Timestamp]<=@ToDate ORDER BY Timestamp ASC

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsSelectAll]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsSelectAll
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: Select all rows form ValidationResults
-- ==========================================================================================



Create Procedure [dbo].[ValidationResultsSelectAll]
AS
BEGIN

SELECT * FROM ValidationResults

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsInsertRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsInsertRow
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: This SP Inserts value to ValidationResults table
-- ==========================================================================================


Create Procedure [dbo].[ValidationResultsInsertRow]
@CodeId bigint,
@CodeText nvarchar(15) = NULL,
@Employee nvarchar(50),
@Timestamp datetime,
@ErrorCode int
AS
BEGIN

    INSERT INTO ValidationResults
        ([CodeId], [CodeText], [Employee], [Timestamp], [ErrorCode])
    VALUES
        (@CodeId, @CodeText, @Employee, @Timestamp, @ErrorCode)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsDeleteRow]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsDeleteRow
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: This SP delete specify row from ValidationResults table
-- ==========================================================================================


Create Procedure [dbo].[ValidationResultsDeleteRow]
@Id bigint
AS
BEGIN

DELETE ValidationResults WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewReportSalePerProduct]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ReportingViewReportSalePerProduct
-- Author:  Mikael Lindberg Mortensen
-- Create date: 2/7/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from ViewReportGroupedOnSalesDate
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewReportSalePerProduct]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     TOP (100) PERCENT dbo.Products.Name AS Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority, dbo.SubOrders.Price, SUM(dbo.SubOrders.Amount) 
                      AS SubAmount, SUM(dbo.SubOrders.SubOrderSum) AS SubSum, SUM(dbo.SubOrders.SubOrderCreditSum) AS SubCredit, SUM(dbo.SubOrders.SubOrderSaldo) 
                      AS SubSaldo
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2' 
GROUP BY dbo.SubOrders.Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority, dbo.SubOrders.Price, dbo.Products.Name
ORDER BY dbo.SubOrders.Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority

END
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewReportGroupedOnSalesDate]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ReportingViewReportGroupedOnSalesDate
-- Author:  Mikael Lindberg Mortensen
-- Create date: 2/7/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from ViewReportGroupedOnSalesDate
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewReportGroupedOnSalesDate]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120) AS OrderDate, dbo.Orders.SalesDate, dbo.Products.Name AS ProcuctName, 
                      SUM(dbo.SubOrders.Amount) AS SubAmount, SUM(dbo.SubOrders.SubOrderSum) AS SubSum, 
                      SUM(dbo.SubOrders.SubOrderCreditSum) AS SubCredit, SUM(dbo.SubOrders.SubOrderSaldo) AS SubSaldo
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2'                  
GROUP BY dbo.Products.Name, CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120), dbo.Orders.SalesDate
ORDER BY dbo.Orders.SalesDate, ProcuctName

END
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewReportSum]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ReportingViewReportSum
-- Author:  Mikael Lindberg Mortensen
-- Create date: 2/7/2014 7:11:27 AM
-- Description: This SP sums up all orders in period
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewReportSum]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, SUM(dbo.SubOrders.SubOrderSaldo) AS SaldoSum, 
                      SUM(dbo.SubOrders.Amount) AS AmountSum
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2'                     

END
GO
/****** Object:  View [dbo].[ViewValidationResults]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewValidationResults]
AS
SELECT     dbo.ValidationResults.CodeText, dbo.ValidationResults.Timestamp, dbo.ValidationResults.Employee, dbo.ValidationStatus.Status, 
                      dbo.ValidationResults.ErrorCode
FROM         dbo.ValidationResults INNER JOIN
                      dbo.ValidationStatus ON dbo.ValidationResults.ErrorCode = dbo.ValidationStatus.Id
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ValidationResults"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 233
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ValidationStatus"
            Begin Extent = 
               Top = 6
               Left = 227
               Bottom = 107
               Right = 378
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1545
         Width = 1995
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewValidationResults'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewValidationResults'
GO
/****** Object:  View [dbo].[ViewReportGroupedOnSalesDate]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewReportGroupedOnSalesDate]
AS
SELECT     TOP (100) PERCENT CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120) AS OrderDate, dbo.Orders.SalesDate, dbo.Products.Name AS ProcuctName, 
                      dbo.SubOrders.Weight, dbo.SubOrders.Priority, SUM(dbo.SubOrders.Amount) AS SubAmount, SUM(dbo.SubOrders.SubOrderSum) AS SubSum, 
                      SUM(dbo.SubOrders.SubOrderCreditSum) AS SubCredit, SUM(dbo.SubOrders.SubOrderSaldo) AS SubSaldo
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
GROUP BY dbo.Products.Name, dbo.SubOrders.Weight, dbo.SubOrders.Priority, CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120), dbo.Orders.SalesDate
ORDER BY dbo.Orders.SalesDate, ProcuctName, dbo.SubOrders.Weight, dbo.SubOrders.Priority
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[27] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Orders"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 261
               Right = 191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Products"
            Begin Extent = 
               Top = 103
               Left = 605
               Bottom = 199
               Right = 756
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SubOrders"
            Begin Extent = 
               Top = 8
               Left = 359
               Bottom = 239
               Right = 534
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1995
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 5205
         Alias = 1440
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewReportGroupedOnSalesDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewReportGroupedOnSalesDate'
GO
/****** Object:  View [dbo].[ViewReimbursements]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewReimbursements]
AS
SELECT     TOP (100) PERCENT dbo.Refunds.OrderNr, dbo.Refunds.Timestamp, dbo.Orders.Email, dbo.Orders.SalesDate, dbo.Orders.ExpireDate, dbo.Orders.OrderSum, 
                      dbo.Refunds.CreditSum, dbo.Orders.Saldo, dbo.Refunds.Name, dbo.Refunds.Address, dbo.Refunds.PostNumber, dbo.Refunds.City, dbo.Refunds.Phone, 
                      dbo.Refunds.Message
FROM         dbo.Refunds INNER JOIN
                      dbo.Orders ON dbo.Refunds.OrderNr = dbo.Orders.Id
ORDER BY dbo.Refunds.CreditSum DESC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Refunds"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 223
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Orders"
            Begin Extent = 
               Top = 15
               Left = 237
               Bottom = 259
               Right = 390
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 975
         Width = 2190
         Width = 1500
         Width = 1995
         Width = 1995
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewReimbursements'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewReimbursements'
GO
/****** Object:  View [dbo].[ViewOrdersSubOrdersCodes]    Script Date: 03/18/2014 14:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewOrdersSubOrdersCodes]
AS
SELECT     dbo.Orders.Id, dbo.Orders.Email, dbo.Orders.ReceiptNumber, dbo.Orders.CaptureId, dbo.Orders.LabelImage, dbo.Orders.ImageSize, dbo.Orders.SalesDate, 
                      dbo.Orders.ExpireDate, dbo.Orders.OrderSum, dbo.SubOrders.Id AS SubOrderId, dbo.SubOrders.ProductId, dbo.SubOrders.Weight, dbo.SubOrders.Priority, 
                      dbo.SubOrders.Destination, dbo.SubOrders.Amount, dbo.SubOrders.Price, dbo.SubOrders.SubOrderSum, dbo.Codes.Id AS CodesId, dbo.Codes.PostCode, 
                      dbo.Codes.CodeId, dbo.Codes.Status, dbo.Codes.UsedDate, dbo.Codes.RefundedDate, dbo.Codes.RefundId, dbo.Orders.CreditSum, dbo.Orders.Saldo, 
                      dbo.Orders.OrderStatus, dbo.SubOrders.SubOrderCreditSum, dbo.SubOrders.SubOrderSaldo, dbo.Codes.IssueDate
FROM         dbo.SubOrders INNER JOIN
                      dbo.Orders ON dbo.SubOrders.OrderId = dbo.Orders.Id INNER JOIN
                      dbo.Codes ON dbo.SubOrders.Id = dbo.Codes.SubOrderId
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SubOrders"
            Begin Extent = 
               Top = 20
               Left = 381
               Bottom = 251
               Right = 532
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Orders"
            Begin Extent = 
               Top = 22
               Left = 61
               Bottom = 236
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Codes"
            Begin Extent = 
               Top = 22
               Left = 638
               Bottom = 238
               Right = 789
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 31
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 4380
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1635
         Table = 1170
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewOrdersSubOrdersCodes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewOrdersSubOrdersCodes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewOrdersSubOrdersCodes'
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewReportSalePerProductReimbursement]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ReportingViewReportSalePerProductReimbursement
-- Author:  Mikael Lindberg Mortensen
-- Create date: 2/7/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from ViewReportGroupedOnSalesDate
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewReportSalePerProductReimbursement]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     TOP (100) PERCENT dbo.Products.Name AS Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority, COUNT(dbo.Refunds.Id) AS SubAmount, 
                      SUM(dbo.SubOrders.Price) AS SubSum, dbo.SubOrders.Price, dbo.SubOrders.ProductId
FROM         dbo.Refunds INNER JOIN
                      dbo.Codes ON dbo.Refunds.Id = dbo.Codes.RefundId INNER JOIN
                      dbo.SubOrders ON dbo.Codes.SubOrderId = dbo.SubOrders.Id INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
WHERE dbo.Codes.RefundedDate>=@FromDate AND dbo.Codes.RefundedDate<=@ToDate 
GROUP BY dbo.SubOrders.Weight, dbo.SubOrders.Priority, dbo.Products.Name, dbo.SubOrders.Price, dbo.Products.Id, dbo.SubOrders.ProductId
ORDER BY Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority


END
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewReimbursementsPerCustomers]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ReportingViewReimbursementsPerCustomers]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT COUNT(dbo.Codes.Id) AS Amount, SUM(dbo.Codes.Price) AS CreditSum, dbo.Orders.Email
FROM         dbo.Refunds INNER JOIN 
				dbo.Orders ON dbo.Refunds.OrderNr = dbo.Orders.Id INNER JOIN
                      dbo.Codes ON dbo.Refunds.Id = dbo.Codes.RefundId

WHERE dbo.Refunds.Timestamp >=@FromDate AND dbo.Refunds.Timestamp <=@ToDate
GROUP BY dbo.Orders.Email
ORDER BY CreditSum DESC

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersChangeCodeStatus]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersChangeCodeStatus
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 28-01-2014 13:12:34
-- Description: This SP Changes the status of codes for a given SubOrder
-- ==========================================================================================

CREATE Procedure [dbo].[SubOrdersChangeCodeStatus]
@SubOrderId bigint,
@OldStatus smallint,
@NewStatus smallint

AS
BEGIN
UPDATE Codes SET [Status] = @NewStatus WHERE [SubOrderId]=@SubOrderId AND [Status] = @OldStatus;
END
GO
/****** Object:  StoredProcedure [dbo].[SubOrderGetCreditCodeCount]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SubOrderGetCreditCodeCount] 
@SubOrderId bigint
AS
BEGIN
	SELECT COUNT(Id) FROM [dbo].[Codes] WHERE SubOrderId=@SubOrderId AND RefundId > 0;
END
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewValidationResultSums]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ReportingViewValidationResultSums]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     Status, COUNT(ErrorCode) AS ErrorCount
FROM       dbo.ViewValidationResults
WHERE Timestamp>=@FromDate AND Timestamp<=@ToDate 
GROUP BY ErrorCode, Status



END
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewValidationResults]    Script Date: 03/18/2014 14:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ReportingViewValidationResults]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT [CodeText]
      ,[Timestamp]
      ,[Employee]
      ,[Status]
FROM [dbo].[ViewValidationResults]
WHERE	Timestamp>=@FromDate AND Timestamp<=@ToDate 


END
GO
/****** Object:  StoredProcedure [dbo].[CodesUpdateUsedDate]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[CodesUpdateUsedDate]
@Id bigint,
@Status smallint,
@UsedDate datetime
AS
BEGIN

UPDATE Codes SET [Status] = @Status, [UsedDate] = @UsedDate WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodesUpdateRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create IssueDate: 17-12-2013 15:11:56
-- Description: This SP updates Codes table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[CodesUpdateRow]
@Id bigint,
@PostCode nchar(15),
@SubOrderId bigint,
@CodeId int,
@Status smallint,
@IssueDate datetime,
@UsedDate datetime,
@Price float,
@ExpireDate date,
@RefundedDate datetime,
@RefundId bigint
AS
BEGIN

UPDATE Codes SET [PostCode] = @PostCode, [SubOrderId] = @SubOrderId,  [CodeId]=@CodeId ,[Status] = @Status, [IssueDate]=@IssueDate, [UsedDate] = @UsedDate, [Price] = @Price, [ExpireDate] = @ExpireDate, [RefundedDate] = @RefundedDate, [RefundId] = @RefundId WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodesUpdateRefund]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[CodesUpdateRefund]
@Id bigint,
@Status smallint,
@RefundedDate datetime,
@RefundId bigint
AS
BEGIN

UPDATE Codes SET [Status] = @Status, [RefundedDate] = @RefundedDate, [RefundId] = @RefundId WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodesSelectRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: This SP select a specify row from Codes
-- ==========================================================================================


Create Procedure [dbo].[CodesSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM Codes WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodesSelectForSubOrder]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesSelectForSubOrder
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: Select rows from Codes that matches SubOrder
-- ==========================================================================================



Create Procedure [dbo].[CodesSelectForSubOrder]
@SubOrderId as bigint
AS
BEGIN

SELECT * FROM Codes WHERE SubOrderId=@SubOrderId

END
GO
/****** Object:  StoredProcedure [dbo].[CodesSelectCode]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: This SP updates Codes table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[CodesSelectCode]
@PostCode nchar(50)
AS
BEGIN

SELECT * FROM Codes WHERE [PostCode]=@PostCode

END
GO
/****** Object:  StoredProcedure [dbo].[CodesSelectAll]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: Select all rows form Codes
-- ==========================================================================================



Create Procedure [dbo].[CodesSelectAll]
AS
BEGIN

SELECT * FROM Codes

END
GO
/****** Object:  StoredProcedure [dbo].[CodesInsertRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[CodesInsertRow]
@PostCode nchar(50),
@SubOrderId bigint,
@CodeId int,
@Status smallint,
@Price float,
@IssueDate datetime,
@ExpireDate date
AS
BEGIN
    INSERT INTO Codes
        ([PostCode], [SubOrderId], [CodeId], [Status], [Price],[IssueDate], [ExpireDate])
    VALUES
        (@PostCode, @SubOrderId, @CodeId, @Status, @Price, @IssueDate, @ExpireDate)
	SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[CodesDeleteRow]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: This SP delete specify row from Codes table
-- ==========================================================================================


Create Procedure [dbo].[CodesDeleteRow]
@Id bigint
AS
BEGIN

DELETE Codes WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectDateCode]    Script Date: 03/18/2014 14:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectDateCode
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/10/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Orders table that matches code
-- ==========================================================================================
CREATE Procedure [dbo].[OrdersSelectDateCode]
@FromDate datetime,
@ToDate datetime,
@PostCode nvarchar(50)

AS
BEGIN

SELECT * FROM ViewOrdersSubOrdersCodes 
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND [PostCode] = @PostCode ORDER BY SalesDate ASC

END
GO
/****** Object:  ForeignKey [FK_Codes_Refunds]    Script Date: 03/18/2014 14:52:01 ******/
ALTER TABLE [dbo].[Codes]  WITH CHECK ADD  CONSTRAINT [FK_Codes_Refunds] FOREIGN KEY([RefundId])
REFERENCES [dbo].[Refunds] ([Id])
GO
ALTER TABLE [dbo].[Codes] CHECK CONSTRAINT [FK_Codes_Refunds]
GO
/****** Object:  ForeignKey [FK_Codes_SubOrders]    Script Date: 03/18/2014 14:52:01 ******/
ALTER TABLE [dbo].[Codes]  WITH CHECK ADD  CONSTRAINT [FK_Codes_SubOrders] FOREIGN KEY([SubOrderId])
REFERENCES [dbo].[SubOrders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Codes] CHECK CONSTRAINT [FK_Codes_SubOrders]
GO
/****** Object:  ForeignKey [FK_OrderLogs_Orders]    Script Date: 03/18/2014 14:52:01 ******/
ALTER TABLE [dbo].[OrderLogs]  WITH CHECK ADD  CONSTRAINT [FK_OrderLogs_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderLogs] CHECK CONSTRAINT [FK_OrderLogs_Orders]
GO
/****** Object:  ForeignKey [FK_Refunds_Orders]    Script Date: 03/18/2014 14:52:01 ******/
ALTER TABLE [dbo].[Refunds]  WITH CHECK ADD  CONSTRAINT [FK_Refunds_Orders] FOREIGN KEY([OrderNr])
REFERENCES [dbo].[Orders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Refunds] CHECK CONSTRAINT [FK_Refunds_Orders]
GO
/****** Object:  ForeignKey [FK_SubOrders_Orders]    Script Date: 03/18/2014 14:52:01 ******/
ALTER TABLE [dbo].[SubOrders]  WITH CHECK ADD  CONSTRAINT [FK_SubOrders_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubOrders] CHECK CONSTRAINT [FK_SubOrders_Orders]
GO
/****** Object:  ForeignKey [FK_SubOrders_Products]    Script Date: 03/18/2014 14:52:01 ******/
ALTER TABLE [dbo].[SubOrders]  WITH CHECK ADD  CONSTRAINT [FK_SubOrders_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[SubOrders] CHECK CONSTRAINT [FK_SubOrders_Products]
GO
/****** Object:  ForeignKey [FK_ValidationResults_ValidationStat]    Script Date: 03/18/2014 14:52:01 ******/
ALTER TABLE [dbo].[ValidationResults]  WITH CHECK ADD  CONSTRAINT [FK_ValidationResults_ValidationStat] FOREIGN KEY([ErrorCode])
REFERENCES [dbo].[ValidationStatus] ([Id])
GO
ALTER TABLE [dbo].[ValidationResults] CHECK CONSTRAINT [FK_ValidationResults_ValidationStat]
GO




INSERT INTO [dbo].[ValidationStatus] VALUES (1,'OK');
INSERT INTO [dbo].[ValidationStatus] VALUES (2,'Used');
INSERT INTO [dbo].[ValidationStatus] VALUES (4,'Expired');
INSERT INTO [dbo].[ValidationStatus] VALUES (8,'Disabled');
INSERT INTO [dbo].[ValidationStatus] VALUES (16,'Refunded');
INSERT INTO [dbo].[ValidationStatus] VALUES (32,'Does not exist');

GO

