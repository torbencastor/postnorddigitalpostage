﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DigitalPostageWSUnitTest.DigitalPostageWS;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Data.SqlClient;
using System.Data;


namespace DigitalPostageWSUnitTest
{

    [TestClass]
    public class WebServiceTest
    {
        const int APP_CLIENT_VERSION = 5;
        const string APP_CLIENT_PLATFORM = "Android";
        DigitalPostageWS.DigitalPostage dpWs = null;

        private string Username
        {
            get
            {
                return DigitalPostageWSUnitTest.Properties.Settings.Default.Username;
            }
        }

        private string Password
        {
            get
            {
                return DigitalPostageWSUnitTest.Properties.Settings.Default.Password;
            }
        }

        private string Domain
        {
            get
            {
                return DigitalPostageWSUnitTest.Properties.Settings.Default.Domain;
            }
        }

        private string Token
        {
            get
            {
                return string.Format("{0};{1};{2}", this.Domain, this.Username, this.Password);
            }
        }

        private string WsUrl
        {
            get
            {
                return DigitalPostageWSUnitTest.Properties.Settings.Default.DigitalPostageWSUnitTest_DigitalPostageWS_DigitalPostage;
            }
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [TestInitialize()]
        public void Initialize()
        {
            dpWs = new DigitalPostageWS.DigitalPostage();
            dpWs.Url = this.WsUrl;
            //System.Net.ICredentials credentials = new System.Net.NetworkCredential(this.Username, this.Password, this.Domain);
            //dpWs.UseDefaultCredentials = false;
            //dpWs.Credentials = credentials;
        }



        /// <summary>
        /// Tests the platform version.
        /// </summary>
        [TestMethod]
        public void TestPlatformVersion()
        {
            // Wrong platform
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, "Not IOS");
            Assert.IsTrue(productsInfo.ErrorCode == "PLATFORM_NOT_SUPPORTED", "Wrong platform allowed");

            // Wrong version
            productsInfo = dpWs.GetProducts(-1, APP_CLIENT_PLATFORM);
            Assert.IsTrue(productsInfo.ErrorCode == "VERSION_NOT_SUPPORTED", "Wrong version allowed");

            // Wrong version  and platform
            productsInfo = dpWs.GetProducts(-1, "NOT IOS");
            Assert.IsTrue(productsInfo.ErrorCode == "PLATFORM_NOT_SUPPORTED", "Wrong platform and version allowed");

            // Shop closed (disabled)
            productsInfo = dpWs.GetProducts(1, "DisabledPlatform");
            Assert.IsTrue(productsInfo.ErrorCode == "WEB_SHOP_IS_CLOSED", "Wrong platform and version allowed, but disabled");
        }

        /// <summary>
        /// Tests the Web Method GetProducts
        /// </summary>
        [TestMethod]
        public void TestGetProducts()
        {
            try
            {

            
            // Normal call
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            Assert.IsTrue(productsInfo.ErrorCode == "", string.Format("Error found: {0}. {1}", productsInfo.ErrorCode, productsInfo.ErrorDescription));
            Assert.IsTrue(productsInfo.Categories.Length > 0, "No Categories found");
            Assert.IsTrue(productsInfo.Categories[0].Products.Length > 0, "No Products found in first category");
            Assert.IsTrue(productsInfo.Categories[0].Id > 0, "First category is missing Id");

            productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, "DisabledPlatform");
            Assert.IsTrue(productsInfo.ErrorCode == "WEB_SHOP_IS_CLOSED", "WEB_SHOP_IS_CLOSED not found");
            Assert.IsTrue(productsInfo.ErrorDescription != string.Empty, "WEB_SHOP_IS_CLOSED Message not found");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [TestMethod]
        public void TestCache()
        {
            string text = dpWs.GetText("TEMPLATE_APP_INVOICE_BESTREGARDS");
            Assert.IsTrue(text == "Med venlig hilsen");
            Console.WriteLine("Text: {0}", text);
        }


        /// <summary>
        /// Tests the Tests the Web Method CreateOrder.
        /// </summary>
        [TestMethod]
        public void TestCreateOrder()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            Assert.IsTrue(productsInfo.ErrorCode == "", string.Format("GetProducts error: {0} Description: {1}", productsInfo.ErrorCode, productsInfo.ErrorDescription));

            string email = "torben.naess@innofactor.com";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Normal call
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Assert.IsTrue(order.Id > 0, "OrderId is not set");
            Assert.IsTrue(order.Products.Length == 1, "Number of products are wrong");
            Assert.IsTrue(order.Products[0].Amount == 1, "Amount is wrong");
            Assert.IsTrue(order.Products[0].Priority == "B", "Priority is wrong");
            Assert.IsTrue(order.Products[0].Price == prod.BPrice, "Price not correct");
            Assert.IsTrue(order.Products[0].ProductSum == order.Products[0].Price, "OrderSum is wrong");
            Assert.IsTrue(order.Email == email, "Email is wrong");
            Assert.IsTrue(order.Saldo == order.Products[0].ProductSum, "Order saldo is wrong");

            Assert.IsTrue(order.PaymentMethods.Nets == true, "NETS Payment disabled");
            Assert.IsTrue(order.PaymentMethods.MobilePay == true, "MP Payment disabled");

            Assert.IsTrue(Library.FromUnixTime(order.SalesDate).Date == DateTime.Today, "SalesDate is wrong");
            Assert.IsTrue(Library.FromUnixTime(order.ExpireDate) >= DateTime.Today.AddDays(Library.VALIDITY_PERIODE).ToUniversalTime(), "ExpireDate is wrong");

            //Test errors
            order = dpWs.CreateOrder(this.Token + "AAA", APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, "", "APP", prod.Id, 1, "B");
            Assert.IsTrue(order.ErrorCode == "ACCESS_DENIED", "Access denied error");
            
            // No Email
            order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, "", "APP", prod.Id, 1, "B");
            Assert.IsTrue(order.ErrorCode == "EMAIL_NOT_VALID", "No Email verification error");

            // Wrong Email
            order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, "invalid @ dk.k", "APP", prod.Id, 1, "B");
            Assert.IsTrue(order.ErrorCode == "EMAIL_NOT_VALID", "Email verification error");

            // Wrong Priority
            order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "C");
            Assert.IsTrue(order.ErrorCode == "PRIORITY_OUT_OF_RANGE", "Wrong priority allowed");

            // Wrong product Id
            order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", 1000000, 1, "B");
            Assert.IsTrue(order.ErrorCode == "PRODUCT_NOT_FOUND", "Wrong product allowed");

            // Access denied
            order = dpWs.CreateOrder(this.Token + "Error", APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", 1000000, 1, "B");
            Assert.IsTrue(order.ErrorCode == "ACCESS_DENIED", "Access denied failed");

        }



/*
        [TestMethod]
        public void TestMobilePayCaptureWithoutSignature()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "test@post.dk";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Normal call
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Assert.IsTrue(order.Id > 0, "OrderId is not set");

            Console.WriteLine("OrderId: {0}", order.Id);

            var placeOrderStatus = dpWs.PlaceOrder(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "MobilePay", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, string.Format("MobilePay error: {0}. Description: {1}", placeOrderStatus.ErrorCode, placeOrderStatus.ErrorDescription));
            Assert.IsTrue(placeOrderStatus.OrderId == order.Id, "MobilePay: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId == string.Empty, "MobilePay: TransactionId is not Empty");

            var orderStatus = dpWs.GetOrderStatus(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "", "");

            Assert.IsTrue(orderStatus.Status == "READYFORPAYMENTMOBILEPAY" || orderStatus.Status == "PAIDINMOBILEPAY", "READYFORPAYMENTMOBILEPAY not returned. Returned: " + orderStatus.Status);

            //orderStatus = dpWs.GetOrderStatus(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "TestPaidInMobilePay", "");
            //Assert.IsTrue(orderStatus.Status == "PAIDINMOBILEPAY", "PAIDINMOBILEPAY not returned. Returned: " + orderStatus.Status);

            orderStatus = dpWs.Capture(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, false, "", "");

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, string.Format("Capture error: {0} - {1}", orderStatus.ErrorCode, orderStatus.ErrorDescription));

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "No Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price == prod.BPrice, "Price is not Correct");

            Assert.IsTrue(orderStatus.Status == "PAID", "Status is not PAID: " + orderStatus.Status);

        }



        [TestMethod]
        public void TestMobilePay()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "test@post.dk";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Normal call
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Assert.IsTrue(order.Id > 0, "OrderId is not set");

            Console.WriteLine("OrderId: {0}", order.Id);

            var placeOrderStatus = dpWs.PlaceOrder(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "MobilePay", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, string.Format("MobilePay error: {0}. Description: {1}", placeOrderStatus.ErrorCode, placeOrderStatus.ErrorDescription));
            Assert.IsTrue(placeOrderStatus.OrderId == order.Id, "MobilePay: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId == string.Empty, "MobilePay: TransactionId is not Empty");

            string signature = @"MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0BBwGggCSABEo2ZmFhNjg0Zi1jNDVjLTQxNWUtYjc5ZS1iZjA1MGY0NTI1MDAjQVBQREswMDAwMDAwMDAwIzEyMzQ1Njc4OTAxMjM0NTY3ODkwIwAAAAAAAKCCBAUwggQBMIIC6aADAgECAgUCEdB6ETANBgkqhkiG9w0BAQsFADCBmDEQMA4GA1UEAxMHREJHUk9PVDELMAkGA1UEBhMCREsxEzARBgNVBAcTCkNvcGVuaGFnZW4xEDAOBgNVBAgTB0Rlbm1hcmsxGjAYBgNVBAoTEURhbnNrZSBCYW5rIEdyb3VwMRowGAYDVQQLExFEYW5za2UgQmFuayBHcm91cDEYMBYGA1UEBRMPNjExMjYyMjgxMTEwMDAyMB4XDTE0MDUxMjAwMDAwMFoXDTE2MDUxMjAwMDAwMFowgZYxDjAMBgNVBAMTBU1TSUdOMQswCQYDVQQGEwJESzETMBEGA1UEBxMKQ29wZW5oYWdlbjEQMA4GA1UECBMHRGVubWFyazEaMBgGA1UEChMRRGFuc2tlIEJhbmsgR3JvdXAxGjAYBgNVBAsTEURhbnNrZSBCYW5rIEdyb3VwMRgwFgYDVQQFEw82MTEyNjIyODg4MTAwMDEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCtJiF4u/CL39avAbSnJhOzj4sQg2dksv/iRoJOtISIosP2exSaxes4tRycCWBU1Owrbr8ksSXpPSu2UzLCMQ4+qcsbYkEeSpXjQak5JccVWZdXFoUTq72f7LSnnuK1fo8kTyybv8Z5MDn6daDd9z/KJg+J4x3z9O9A/5L553+ZsNsd70VlpcEVOWXPkNf9w/ze2ztcosbzuZYp+YSI36fjjsn0UmBDaYk9ajuk1o0odda3qq0BqrhUlU3G29jD73L+ZWhmRpUEtPfjY+7uRcpl7Bo7IoscXWk1943Gj4ATBZ/hW60boSQStgQlDxglyi6yGyPE0MTp3ngqXO8HX7LfAgMBAAGjUjBQMB8GA1UdIwQYMBaAFIT65b/ekUlm38WKUsOzt7MgHMdtMB0GA1UdDgQWBBSZb6E7shVfskN4t9sqioYjiJmfFTAOBgNVHQ8BAf8EBAMCBsAwDQYJKoZIhvcNAQELBQADggEBAEHGaztr1YjWJsGi0nJBakoTliwRPcsBiQYhFIIhPMPpbByg9P4KxHrSYI4oyz+QoC6s3htqrn98rUNUP05jvizRDVZ1CGY0vjB3Xn1adJMWS3pB5HPOLyooBfGR8AUK1BX61Xc0ggVfYdiEKkuopjsFAfYpWA2tF+fSA4tzbWCc1ZXf7bNtgbvqWg2x2S+gWsYZScgYn1ssUB/ho0pT9Ys0MXhdfMAMLPA0KO/OKoskpC/5E3clSNijfju5TyM7rTxQMoZYFcPUDCyHqL1SInAh/cGuqv3StPeT0ymCyB00QsyfFkQKhH4QFmBnTRCqAsDrX95coWHu77aBZFfsn90xggHOMIIBygIBATCBojCBmDEQMA4GA1UEAxMHREJHUk9PVDELMAkGA1UEBhMCREsxEzARBgNVBAcTCkNvcGVuaGFnZW4xEDAOBgNVBAgTB0Rlbm1hcmsxGjAYBgNVBAoTEURhbnNrZSBCYW5rIEdyb3VwMRowGAYDVQQLExFEYW5za2UgQmFuayBHcm91cDEYMBYGA1UEBRMPNjExMjYyMjgxMTEwMDAyAgUCEdB6ETANBglghkgBZQMEAgEFADANBgkqhkiG9w0BAQEFAASCAQBfWggguR8RvEjg7jTrytlKR7/6ZyQV/UGBn/CnG536MhC7ilo/WRIEmfQORljFAUPhW75jQBJs5XUhnz3YXcepjDUiUxAkuHMih5bsZ8MRqrfQM72ZykwmXTegTEUkZuIST715N6+qAcdS8Wiw7viuDwzEbZhSmOK6tbtIJi+ff9GyHYpu3QSi+JRq0bavCWgzBm83CCm2CP3LDT7PEHQzgp1hssPYwwzI8dZLZl/SnKoIvXWxJTeS/mpffLvqlflQ9Kt1kNXgbRNVTHveJoMQf/MeILUUnVfWw46dPSzyTNhOwYrnN+MmYiXkwdxmjdcXL4R985756ETWfTlHDk0aAAAAAAAA";
            signature = @"MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0BBwGggCSABDczMzMzNSNBUFBESzAwMDAwMDAwMDAjMTIzNDU2Nzg5MDEyMzQ1Njc4OTAjNy4wMCNES0sjREsjAAAAAAAAoIIEBTCCBAEwggLpoAMCAQICBQIR0HoRMA0GCSqGSIb3DQEBCwUAMIGYMRAwDgYDVQQDEwdEQkdST09UMQswCQYDVQQGEwJESzETMBEGA1UEBxMKQ29wZW5oYWdlbjEQMA4GA1UECBMHRGVubWFyazEaMBgGA1UEChMRRGFuc2tlIEJhbmsgR3JvdXAxGjAYBgNVBAsTEURhbnNrZSBCYW5rIEdyb3VwMRgwFgYDVQQFEw82MTEyNjIyODExMTAwMDIwHhcNMTQwNTEyMDAwMDAwWhcNMTYwNTEyMDAwMDAwWjCBljEOMAwGA1UEAxMFTVNJR04xCzAJBgNVBAYTAkRLMRMwEQYDVQQHEwpDb3BlbmhhZ2VuMRAwDgYDVQQIEwdEZW5tYXJrMRowGAYDVQQKExFEYW5za2UgQmFuayBHcm91cDEaMBgGA1UECxMRRGFuc2tlIEJhbmsgR3JvdXAxGDAWBgNVBAUTDzYxMTI2MjI4ODgxMDAwMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAK0mIXi78Ivf1q8BtKcmE7OPixCDZ2Sy/+JGgk60hIiiw/Z7FJrF6zi1HJwJYFTU7CtuvySxJek9K7ZTMsIxDj6pyxtiQR5KleNBqTklxxVZl1cWhROrvZ/stKee4rV+jyRPLJu/xnkwOfp1oN33P8omD4njHfP070D/kvnnf5mw2x3vRWWlwRU5Zc+Q1/3D/N7bO1yixvO5lin5hIjfp+OOyfRSYENpiT1qO6TWjSh11reqrQGquFSVTcbb2MPvcv5laGZGlQS09+Nj7u5FymXsGjsiixxdaTX3jcaPgBMFn+FbrRuhJBK2BCUPGCXKLrIbI8TQxOneeCpc7wdfst8CAwEAAaNSMFAwHwYDVR0jBBgwFoAUhPrlv96RSWbfxYpSw7O3syAcx20wHQYDVR0OBBYEFJlvoTuyFV+yQ3i32yqKhiOImZ8VMA4GA1UdDwEB/wQEAwIGwDANBgkqhkiG9w0BAQsFAAOCAQEAQcZrO2vViNYmwaLSckFqShOWLBE9ywGJBiEUgiE8w+lsHKD0/grEetJgjijLP5CgLqzeG2quf3ytQ1Q/TmO+LNENVnUIZjS+MHdefVp0kxZLekHkc84vKigF8ZHwBQrUFfrVdzSCBV9h2IQqS6imOwUB9ilYDa0X59IDi3NtYJzVld/ts22Bu+paDbHZL6BaxhlJyBifWyxQH+GjSlP1izQxeF18wAws8DQo784qiySkL/kTdyVI2KN+O7lPIzutPFAyhlgVw9QMLIeovVIicCH9wa6q/dK095PTKYLIHTRCzJ8WRAqEfhAWYGdNEKoCwOtf3lyhYe7vtoFkV+yf3TGCAc4wggHKAgEBMIGiMIGYMRAwDgYDVQQDEwdEQkdST09UMQswCQYDVQQGEwJESzETMBEGA1UEBxMKQ29wZW5oYWdlbjEQMA4GA1UECBMHRGVubWFyazEaMBgGA1UEChMRRGFuc2tlIEJhbmsgR3JvdXAxGjAYBgNVBAsTEURhbnNrZSBCYW5rIEdyb3VwMRgwFgYDVQQFEw82MTEyNjIyODExMTAwMDICBQIR0HoRMA0GCWCGSAFlAwQCAQUAMA0GCSqGSIb3DQEBAQUABIIBAAl4WwAirtCW58XVjTThDf04PNyJcCPw7uipLnYJS1OKUVOnofOMqq8zsa5i1TRZo8HTrxUCUzLDahkUT0+9aD6xojKZGWoR64/rap/I9p+FGOkYqLKKCo+4+yFMHi38+1ZYXpWdqknIfxQ4hdFrL8+6HyK++LlPFrnaABc7P/fmCbgmgcmyyuS8uzYBKmgwZ6RhrfyYDLrTrJ/UDoaG4ucIu+Jtj5SL9icaZfOj6Y683TAgXjcJDD+LBeXEGsg7ZI36N8vgwi2n8p3q7YezVsPCx7PBUBdoYYLK6DPmWHvkiWxp/nhWO1xNnv75I5qgJbsYqafD8H9sWFNEd0OnK3YAAAAAAAA=";

            var orderStatus = dpWs.GetOrderStatus(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "TestNotPaidInMobilePay", "");
            Assert.IsTrue(orderStatus.Status == "READYFORPAYMENTMOBILEPAY", "READYFORPAYMENTMOBILEPAY not returned. Returned: " + orderStatus.Status);

            orderStatus = dpWs.GetOrderStatus(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "TestPaidInMobilePay", "");
            Assert.IsTrue(orderStatus.Status == "PAIDINMOBILEPAY", "PAIDINMOBILEPAY not returned. Returned: " + orderStatus.Status);

            Console.WriteLine("Signature length: {0}", signature.Length);

            orderStatus = dpWs.Capture(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, false, "903506140", signature);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, string.Format("Capture error: {0} - {1}", orderStatus.ErrorCode, orderStatus.ErrorDescription));

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "No Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price == prod.BPrice, "Price is not Correct");

            Assert.IsTrue(orderStatus.Status == "PAID", "Status is not PAID: " + orderStatus.Status);

            order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "A");
            placeOrderStatus = dpWs.PlaceOrder(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "MobilePay", "");
            orderStatus = dpWs.Capture(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, false, "", "");

            Assert.IsTrue(orderStatus.Status == "PAID", "Order is not Paid, when calling with empty transactionId and signature.");
        }

        [TestMethod]
        public void TestTimeOut()
        {
            try
            {
                dpWs.TestTimeOut();
            }
            catch (TimeoutException ex)
            {
                Console.WriteLine("TimeoutException. {0}", ex.Message);
            }
            catch (WebException ex)
            {
                if (ex.Message == "The operation has timed out")
                {
                    Console.WriteLine("TimeOut!!");
                }

                Console.WriteLine("Web Exception. Type: {0}. Message:{1}", ex.GetType().Name, ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine("Inner Exception. Type: {0}. Message:{1}", ex.InnerException.GetType().Name, ex.InnerException.Message);
                }
                Console.WriteLine(ex);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Other Exception. Type: {0}. Message:{1}", ex.GetType().Name,  ex.Message);
            }
        }
*/

        /// <summary>
        /// Tests the Tests the Web Method CreateOrder.
        /// </summary>
        [TestMethod]
        public void TestAddProduct()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "test@post.dk";

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Normal call
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");

            prod = productsInfo.Categories[0].Products[1];
            order = dpWs.AddProduct(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 2, "B");

            Assert.IsTrue(order.Id > 0, "OrderId is not set");
            Assert.IsTrue(order.Products.Length == 2, "Number of products are wrong");
            Assert.IsTrue(order.Products[1].Amount == 2, "Amount is wrong");
            Assert.IsTrue(order.Products[1].Priority == "B", "Priority is wrong");
            Assert.IsTrue(order.Products[1].Price == prod.BPrice, "Price not correct");
            Assert.IsTrue(order.Products[1].ProductSum == (order.Products[1].Amount * prod.BPrice), "ProductSum is wrong");
            Assert.IsTrue(order.Email == email, "Email is wrong");
            Assert.IsTrue(order.Saldo == order.Products[0].ProductSum + order.Products[1].ProductSum, "Order saldo is wrong");

            Assert.IsTrue(Library.FromUnixTime(order.SalesDate).Date == DateTime.Today, "SalesDate is wrong");
            Assert.IsTrue(Library.FromUnixTime(order.ExpireDate) >= DateTime.Today.AddDays(Library.VALIDITY_PERIODE).ToUniversalTime(), "ExpireDate is wrong");

            //Test errors

            // Wrong Priority
            order = dpWs.AddProduct(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "C");
            Assert.IsTrue(order.ErrorCode == "PRIORITY_OUT_OF_RANGE", "Wrong priority allowed");

            // Wrong product Id
            order = dpWs.AddProduct(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", 1000000, 1, "B");
            Assert.IsTrue(order.ErrorCode == "PRODUCT_NOT_FOUND", "Wrong product allowed");

        }

        /// <summary>
        /// Tests the Web Method PlaceOrder.
        /// </summary>
        [TestMethod]
        public void TestPlaceOrderNetAxcept()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "test@post.dk";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Create order
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Console.WriteLine("Order created: {0}", order.Id);

            Assert.IsTrue(order.Id > 0, "OrderId is not set!");
            long orderId = order.Id;

            // Wrong type of payment
            var placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "OtherPayment", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == "PAYMENTMETHOD_INVALID", "Invalid payment allowed");

            // Wrong OrderId
            placeOrderStatus = dpWs.PlaceOrder(this.Token, 0, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxept", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == "ORDER_NOT_FOUND", "Invalid OrderId allowed");

            // Hashcode is missing
            placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxceptCardReuse", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == "HASHCODE_MISSING", "Missing hash code allowed");

            // Normal call
            placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxcept", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, string.Format("NetAxcept error: {0}. Description: {1}", placeOrderStatus.ErrorCode, placeOrderStatus.ErrorDescription));
            Assert.IsTrue(placeOrderStatus.OrderId == orderId, "NetAxcept: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId != string.Empty, "NetAxcept: TransactionId not set");

            Console.WriteLine("Transaction Id: {0}", placeOrderStatus.TransactionId);

            // Wrong OrderId
            placeOrderStatus = dpWs.PlaceOrder(this.Token, 0, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxept", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == "ORDER_NOT_FOUND", "Invalid OrderId allowed");

            // Wrong type of payment
            placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "OtherPayment", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == "PAYMENTMETHOD_INVALID", "Invalid payment allowed");

            // Hashcode is missing
            placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxceptCardReuse", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == "HASHCODE_MISSING", "Missing hash code allowed");


        }

        /// <summary>
        /// Tests the Web Method PlaceOrder.
        /// </summary>
        [TestMethod]
        public void TestCaptureOrderNetAxceptSendMail()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "MikaelLMortensen@gmail.com";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Create order
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Console.WriteLine("Order created: {0}", order.Id);

            Assert.IsTrue(order.Id > 0, "OrderId is not set!");
            long orderId = order.Id;

            // Normal call
            var placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxcept", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, "NetAxcept error: " + placeOrderStatus.ErrorCode);
            Assert.IsTrue(placeOrderStatus.OrderId == orderId, "NetAxcept: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId != string.Empty, "NetAxcept: TransactionId not set");

            Console.WriteLine("Transaction Id: {0}", placeOrderStatus.TransactionId);
            Console.WriteLine("TerminalUrl: {0}", placeOrderStatus.TerminalUrl);

            using (TestPayment tp = new TestPayment())
            {
                tp.Payment(placeOrderStatus.TerminalUrl, false);
                Assert.IsTrue(tp.Result == "OK", "Payment not OK");
            }

            var orderStatus = dpWs.Capture(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, order.Email, true, placeOrderStatus.TransactionId, string.Empty);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, "Capture error: " + orderStatus.ErrorCode);
            Assert.IsTrue(orderStatus.Id == order.Id, "Capture failed. Id is 0");

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "No Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price == prod.BPrice, "Price is not Correct");

            Assert.IsTrue(orderStatus.Status == "PAID", "Status is not PAID: " + orderStatus.Status);

            Assert.IsTrue(Library.FromUnixTime(orderStatus.SalesDate).Date == Library.FromUnixTime(order.SalesDate).Date, string.Format("Salesdate does not match {0} != {1}", Library.FromUnixTime(orderStatus.SalesDate), Library.FromUnixTime(order.SalesDate)));
            Assert.IsTrue(Library.FromUnixTime(orderStatus.ExpireDate).Date == Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE), string.Format("Expire does not match {0} != {1}", Library.FromUnixTime(orderStatus.ExpireDate), Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE)));

            orderStatus = TestGetOrderStatus(order, placeOrderStatus.TransactionId);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, "Order Status error: " + placeOrderStatus.ErrorCode);
            Assert.IsTrue(orderStatus.Id == order.Id, "Order Status failed. Id is 0");

            Assert.IsTrue(Library.FromUnixTime(orderStatus.SalesDate).Date == Library.FromUnixTime(order.SalesDate).Date, string.Format("Salesdate does not match {0} != {1}", Library.FromUnixTime(orderStatus.SalesDate), Library.FromUnixTime(order.SalesDate)));
            Assert.IsTrue(Library.FromUnixTime(orderStatus.ExpireDate).Date == Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE), string.Format("Expire does not match {0} != {1}", Library.FromUnixTime(orderStatus.ExpireDate), Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE)));

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "Order status: no Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Order status: code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price > 0, "Order status: value is not set");

            Assert.IsTrue(orderStatus.Status == "PAID", "Order status, Status has wrong value: " + orderStatus.Status);

        }


        /// <summary>
        /// Tests the Web Method PlaceOrder.
        /// </summary>
        [TestMethod]
        public void TestCaptureOrderNetAxcept()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "MikaelLMortensen@gmail.com";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Create order
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Console.WriteLine("Order created: {0}", order.Id);

            Assert.IsTrue(order.Id > 0, "OrderId is not set!");
            long orderId = order.Id;

            // Normal call
            var placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxcept", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, "NetAxcept error: " + placeOrderStatus.ErrorCode);
            Assert.IsTrue(placeOrderStatus.OrderId == orderId, "NetAxcept: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId != string.Empty, "NetAxcept: TransactionId not set");

            Console.WriteLine("Transaction Id: {0}", placeOrderStatus.TransactionId);
            Console.WriteLine("TerminalUrl: {0}", placeOrderStatus.TerminalUrl);

            using (TestPayment tp = new TestPayment())
            {
                tp.Payment(placeOrderStatus.TerminalUrl, false);
                Assert.IsTrue(tp.Result == "OK", "Payment not OK. Result: " + tp.Result);
            }

            var orderStatus = TestNetAxceptCapture(order, placeOrderStatus.TransactionId);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, "Capture error: " + orderStatus.ErrorCode);
            Assert.IsTrue(orderStatus.Id == order.Id, "Capture failed. Id is 0");

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "No Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price == prod.BPrice, "Price is not Correct");

            Assert.IsTrue(orderStatus.Status == "PAID", "Status is not PAID: " + orderStatus.Status);

            Assert.IsTrue(Library.FromUnixTime(orderStatus.SalesDate).Date == Library.FromUnixTime(order.SalesDate).Date, string.Format("Salesdate does not match {0} != {1}", Library.FromUnixTime(orderStatus.SalesDate), Library.FromUnixTime(order.SalesDate)));
            Assert.IsTrue(Library.FromUnixTime(orderStatus.ExpireDate).Date == Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE), string.Format("Expire does not match {0} != {1}", Library.FromUnixTime(orderStatus.ExpireDate), Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE)));

            orderStatus = TestGetOrderStatus(order, placeOrderStatus.TransactionId);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, "Order Status error: " + placeOrderStatus.ErrorCode);
            Assert.IsTrue(orderStatus.Id == order.Id, "Order Status failed. Id is 0");

            Assert.IsTrue(Library.FromUnixTime(orderStatus.SalesDate).Date == Library.FromUnixTime(order.SalesDate).Date, string.Format("Salesdate does not match {0} != {1}", Library.FromUnixTime(orderStatus.SalesDate), Library.FromUnixTime(order.SalesDate)));
            Assert.IsTrue(Library.FromUnixTime(orderStatus.ExpireDate).Date == Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE), string.Format("Expire does not match {0} != {1}", Library.FromUnixTime(orderStatus.ExpireDate), Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE)));

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "Order status: no Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Order status: code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price > 0, "Order status: value is not set");

            Assert.IsTrue(orderStatus.Status == "PAID", "Order status, Status has wrong value: " + orderStatus.Status);

        }


        /// <summary>
        /// Tests the Web Method PlaceOrder.
        /// </summary>
        [TestMethod]
        public void TestDoubeOrderCaptureOrderNetAxcept()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "MikaelLMortensen@gmail.com";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Create order
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Console.WriteLine("Order created: {0}", order.Id);

            Assert.IsTrue(order.Id > 0, "OrderId is not set!");
            long orderId = order.Id;

            // Normal call
            var placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxcept", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, "NetAxcept error: " + placeOrderStatus.ErrorCode);
            Assert.IsTrue(placeOrderStatus.OrderId == orderId, "NetAxcept: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId != string.Empty, "NetAxcept: TransactionId not set");

            Console.WriteLine("Transaction Id: {0}", placeOrderStatus.TransactionId);
            Console.WriteLine("TerminalUrl: {0}", placeOrderStatus.TerminalUrl);

            using (TestPayment tp = new TestPayment())
            {
                tp.Payment(placeOrderStatus.TerminalUrl, false);
                Assert.IsTrue(tp.Result == "OK", "Payment not OK. Result: " + tp.Result);
            }

            var orderStatus = TestNetAxceptCapture(order, placeOrderStatus.TransactionId);

            /*   SQL TO Reset order to enable another payment
             * 
             *    UPDATE [DigitalPostage].[dbo].[Orders] 
             *    SET AppType=15, OrderStatus=1, SentToCint=NULL, CaptureId='' 
             *    WHERE Id = <Order.Id>
             */

            placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxcept", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, "NetAxcept error: " + placeOrderStatus.ErrorCode);
            Assert.IsTrue(placeOrderStatus.OrderId == orderId, "NetAxcept: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId != string.Empty, "NetAxcept: TransactionId not set");

            Console.WriteLine("Doubled Transaction Id: {0}", placeOrderStatus.TransactionId);

            using (TestPayment tp = new TestPayment())
            {
                tp.Payment(placeOrderStatus.TerminalUrl, false);
                Assert.IsTrue(tp.Result == "OK", "Payment not OK. Result: " + tp.Result);
            }

            orderStatus = TestNetAxceptCapture(order, placeOrderStatus.TransactionId);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, "Capture error: " + orderStatus.ErrorCode);
            Assert.IsTrue(orderStatus.Id == order.Id, "Capture failed. Id is 0");

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "No Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price == prod.BPrice, "Price is not Correct");

            Assert.IsTrue(orderStatus.Status == "PAID", "Status is not PAID: " + orderStatus.Status);

            Assert.IsTrue(Library.FromUnixTime(orderStatus.SalesDate).Date == Library.FromUnixTime(order.SalesDate).Date, string.Format("Salesdate does not match {0} != {1}", Library.FromUnixTime(orderStatus.SalesDate), Library.FromUnixTime(order.SalesDate)));
            Assert.IsTrue(Library.FromUnixTime(orderStatus.ExpireDate).Date == Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE), string.Format("Expire does not match {0} != {1}", Library.FromUnixTime(orderStatus.ExpireDate), Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE)));

            orderStatus = TestGetOrderStatus(order, placeOrderStatus.TransactionId);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, "Order Status error: " + placeOrderStatus.ErrorCode);
            Assert.IsTrue(orderStatus.Id == order.Id, "Order Status failed. Id is 0");

            Assert.IsTrue(Library.FromUnixTime(orderStatus.SalesDate).Date == Library.FromUnixTime(order.SalesDate).Date, string.Format("Salesdate does not match {0} != {1}", Library.FromUnixTime(orderStatus.SalesDate), Library.FromUnixTime(order.SalesDate)));
            Assert.IsTrue(Library.FromUnixTime(orderStatus.ExpireDate).Date == Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE), string.Format("Expire does not match {0} != {1}", Library.FromUnixTime(orderStatus.ExpireDate), Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE)));

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "Order status: no Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Order status: code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price > 0, "Order status: value is not set");

            Assert.IsTrue(orderStatus.Status == "PAID", "Order status, Status has wrong value: " + orderStatus.Status);

            /* SQL To force double order behaviour
             * 
                DECLARE  @OrderId  int;
                SET  @OrderId  =  <OrderId>;
  
                UPDATE [DigitalPostage].[dbo].[Orders]
                SET AppType=15, OrderStatus=1, SentToCint=NULL
                WHERE Id = @OrderId;
  
                UPDATE [DigitalPostage].[dbo].[OrderLogs]
                SET Text = REPLACE(Text,'Register NetAxcept payment.','Register payment.')
                WHERE OrderId = @OrderId AND Text LIKE 'Register NetAxcept payment. Transaction Id: %';
             */
        }

        /// <summary>
        /// Tests the Web Method PlaceOrder.
        /// </summary>
        [TestMethod]
        public void TestCaptureOrderNetAxceptAuthError()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "test@post.dk";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Create order
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Console.WriteLine("Order created: {0}", order.Id);

            Assert.IsTrue(order.Id > 0, "OrderId is not set!");
            long orderId = order.Id;

            // Normal call
            var placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxcept", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, "NetAxcept error: " + placeOrderStatus.ErrorCode);
            Assert.IsTrue(placeOrderStatus.OrderId == orderId, "NetAxcept: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId != string.Empty, "NetAxcept: TransactionId not set");

            Console.WriteLine("Transaction Id: {0}", placeOrderStatus.TransactionId);
            Console.WriteLine("TerminalUrl: {0}", placeOrderStatus.TerminalUrl);

            using (TestPayment tp = new TestPayment())
            {
                tp.CardNo = "4925000000000087";
                tp.Payment(placeOrderStatus.TerminalUrl, false);
                Assert.IsTrue(tp.Result == "OK", "Payment not OK. Result: " + tp.Result);
            }

            var orderStatus = TestNetAxceptCapture(order, placeOrderStatus.TransactionId);

            Assert.IsTrue(orderStatus.ErrorCode == "NETS_AUTH_FAILURE", "Capture error: " + orderStatus.ErrorCode);

        }


        /// <summary>
        /// Tests the Web Method PlaceOrder.
        /// </summary>
        [TestMethod]
        public void TestCaptureOrderNetAxceptSave()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "test@post.dk";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Create order
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Console.WriteLine("Order created: {0}", order.Id);

            Assert.IsTrue(order.Id > 0, "OrderId is not set!");
            long orderId = order.Id;

            // Wrong type of payment
            var placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "OtherPayment", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == "PAYMENTMETHOD_INVALID", "Invalid payment allowed");

            // Normal call
            placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxceptCardSave", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, "NetAxcept error: " + placeOrderStatus.ErrorCode);
            Assert.IsTrue(placeOrderStatus.OrderId == orderId, "NetAxcept: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId != string.Empty, "NetAxcept: TransactionId not set");

            Console.WriteLine("Transaction Id: {0}", placeOrderStatus.TransactionId);
            Console.WriteLine("TerminalUrl: {0}", placeOrderStatus.TerminalUrl);

            // Pay order
            using (TestPayment tp = new TestPayment())
            {
                tp.Payment(placeOrderStatus.TerminalUrl, false);
                Assert.IsTrue(tp.Result == "OK", "Payment not OK");
            }

            var orderStatus = TestNetAxceptCapture(order, placeOrderStatus.TransactionId);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, string.Format("Capture error: {0} Desc: {1}", orderStatus.ErrorCode, orderStatus.ErrorDescription));
            Assert.IsTrue(orderStatus.Id == order.Id, "Capture failed. Id is 0");

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "No Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price == prod.BPrice, "Price is not Correct");

            Assert.IsTrue(orderStatus.Status == "PAID", "Status is not PAID: " + orderStatus.Status);

            Assert.IsTrue(Library.FromUnixTime(orderStatus.SalesDate).Date == Library.FromUnixTime(order.SalesDate).Date, string.Format("Salesdate does not match {0} != {1}", Library.FromUnixTime(orderStatus.SalesDate), Library.FromUnixTime(order.SalesDate)));
            Assert.IsTrue(Library.FromUnixTime(orderStatus.ExpireDate).Date == Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE), string.Format("Expire does not match {0} != {1}", Library.FromUnixTime(orderStatus.ExpireDate), Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE)));

            Console.WriteLine("Issuer: {0} ScrambledData: {1} Expire: {2} Hash:{3}", orderStatus.CreditCardIssuer, orderStatus.CreditCardScrambledData, orderStatus.CreditCardExpiryDate, orderStatus.CreditCardHash);
            Assert.IsTrue(orderStatus.CreditCardIssuer != string.Empty, "Issuer not set");
            Assert.IsTrue(orderStatus.CreditCardScrambledData != string.Empty, "ScrambledData not set");
            Assert.IsTrue(orderStatus.CreditCardExpiryDate != string.Empty, "CreditcardExpireDate not set");
            Assert.IsTrue(orderStatus.CreditCardHash != string.Empty, "CreditcardHash not set");

            orderStatus = TestGetOrderStatus(order, placeOrderStatus.TransactionId);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, string.Format("Order Status error: {0} Desc: {1}", orderStatus.ErrorCode, orderStatus.ErrorDescription));
            Assert.IsTrue(orderStatus.Id == order.Id, "Order Status failed. Id is 0");

            Assert.IsTrue(Library.FromUnixTime(orderStatus.SalesDate).Date == Library.FromUnixTime(order.SalesDate).Date, string.Format("Salesdate does not match {0} != {1}", Library.FromUnixTime(orderStatus.SalesDate), Library.FromUnixTime(order.SalesDate)));
            Assert.IsTrue(Library.FromUnixTime(orderStatus.ExpireDate).Date == Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE), string.Format("Expire does not match {0} != {1}", Library.FromUnixTime(orderStatus.ExpireDate), Library.FromUnixTime(order.SalesDate).Date.AddDays(Library.VALIDITY_PERIODE)));

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "Order status: no Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Order status: code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price > 0, "Order status: value is not set");

            Assert.IsTrue(orderStatus.Status == "PAID", "Order status, Status has wrong value: " + orderStatus.Status);

            Console.WriteLine("GetOrderStatus");
            Console.WriteLine("Issuer: {0} ScrambledData: {1} Expire: {2} Hash:{3}", orderStatus.CreditCardIssuer, orderStatus.CreditCardScrambledData, orderStatus.CreditCardExpiryDate, orderStatus.CreditCardHash);
            Assert.IsTrue(orderStatus.CreditCardIssuer != string.Empty, "GetOrderStatus - Issuer not set");
            Assert.IsTrue(orderStatus.CreditCardScrambledData != string.Empty, "GetOrderStatus - ScrambledData not set");
            Assert.IsTrue(orderStatus.CreditCardExpiryDate != string.Empty, "GetOrderStatus - CreditcardExpireDate not set");
            Assert.IsTrue(orderStatus.CreditCardHash != string.Empty, "GetOrderStatus - CreditcardHash not set");
        }

        /// <summary>
        /// Tests the Web Method NetAxcept Reuse Credit CardInformation. Creates Order, saves credit card information. Creates second order using saved Credit card information.
        /// </summary>
        [TestMethod]
        public void TestCaptureOrderNetAxceptReuse()
        {
            var productsInfo = dpWs.GetProducts(APP_CLIENT_VERSION, APP_CLIENT_PLATFORM);

            string email = "test@post.dk";
            long today = Library.ToUnixTime(DateTime.Today.ToUniversalTime());
            Assert.IsTrue(Library.FromUnixTime(today) == DateTime.Today.ToUniversalTime(), "Unix time conversion fails!");

            // use first product of first category as Test Item
            var prod = productsInfo.Categories[0].Products[0];

            // Create order
            var order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 1, "B");
            Console.WriteLine("Order created: {0}", order.Id);

            Assert.IsTrue(order.Id > 0, "OrderId is not set!");
            long orderId = order.Id;

            // Normal call, save card
            var placeOrderStatus = dpWs.PlaceOrder(this.Token, orderId, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxceptCardSave", "");
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, "NetAxcept error 1: " + placeOrderStatus.ErrorCode + " Desc: " + placeOrderStatus.ErrorDescription);
            Assert.IsTrue(placeOrderStatus.OrderId == orderId, "NetAxcept: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId != string.Empty, "NetAxcept: TransactionId not set");

            Console.WriteLine("Transaction Id: {0}", placeOrderStatus.TransactionId);
            Console.WriteLine("TerminalUrl: {0}", placeOrderStatus.TerminalUrl);

            // Pay order
            using (TestPayment tp = new TestPayment())
            {
                tp.Payment(placeOrderStatus.TerminalUrl, false);
                Assert.IsTrue(tp.Result == "OK", "Payment not OK");
            }

            var orderStatus = TestNetAxceptCapture(order, placeOrderStatus.TransactionId);
            string creditCardHash = orderStatus.CreditCardHash;
            Assert.IsTrue(creditCardHash != string.Empty, "CreditcardHash not set");
            Console.WriteLine("Creditcard Hash: {0}", creditCardHash);

            order = dpWs.CreateOrder(this.Token, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "APP", prod.Id, 5, "B");
            Console.WriteLine("New order created: {0}", order.Id);

            placeOrderStatus = dpWs.PlaceOrder(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, email, "NetAxceptCardReuse", creditCardHash);
            Assert.IsTrue(placeOrderStatus.ErrorCode == string.Empty, "NetAxcept error 2: " + placeOrderStatus.ErrorCode + " Desc: " + placeOrderStatus.ErrorDescription);
            Assert.IsTrue(placeOrderStatus.OrderId == order.Id, "NetAxcept: OrderId is not set");
            Assert.IsTrue(placeOrderStatus.TransactionId != string.Empty, "NetAxcept: TransactionId not set");

            Console.WriteLine("Transaction Id: {0}", placeOrderStatus.TransactionId);
            Console.WriteLine("TerminalUrl: {0}", placeOrderStatus.TerminalUrl);

            // Pay order with EasyPayment
            using (TestPayment tp = new TestPayment())
            {
                tp.Payment(placeOrderStatus.TerminalUrl, true);
                Assert.IsTrue(tp.Result == "OK", "Payment not OK");
            }
            Console.WriteLine("Easy Payment OK");

            orderStatus = TestNetAxceptCapture(order, placeOrderStatus.TransactionId);

            Assert.IsTrue(orderStatus.ErrorCode == string.Empty, "Capture error: " + orderStatus.ErrorCode);
            Assert.IsTrue(orderStatus.Id == order.Id, "Capture failed. Id is 0");

            Assert.IsTrue(orderStatus.SubOrders[0].Codes.Length > 0, "No Codes found");
            Assert.IsTrue(orderStatus.SubOrders[0].Codes[0].Text.Length == 14, "Code Length is wrong: " + orderStatus.SubOrders[0].Codes[0].Text);
            Assert.IsTrue(orderStatus.SubOrders[0].Price == prod.BPrice, string.Format("Price is not Correct. Expected: {0} Got: {1}", orderStatus.SubOrders[0].Price, prod.BPrice));

            Assert.IsTrue(orderStatus.Status == "PAID", "Status is not PAID: " + orderStatus.Status);

        }

        /// <summary>
        /// Tests Capture with simple NetAxcept.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <returns></returns>
        public OrderStatus TestNetAxceptCapture(DigitalPostageWS.Order order, string transactionId)
        {
            return dpWs.Capture(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, order.Email, false, transactionId, string.Empty);
        }

        /// <summary>
        /// Tests the Web Method GetOrderStatus.
        /// </summary>
        public OrderStatus TestGetOrderStatus(DigitalPostageWS.Order order, string transactionId)
        {
            return dpWs.GetOrderStatus(this.Token, order.Id, APP_CLIENT_VERSION, APP_CLIENT_PLATFORM, order.Email, transactionId, string.Empty);
        }

    }
}
