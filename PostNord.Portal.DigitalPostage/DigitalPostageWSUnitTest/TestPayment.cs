﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace DigitalPostageWSUnitTest
{
    public class TestPayment : IDisposable  
    {

        WebBrowser browser = null;
        public int Count = 0;
        public string Result = string.Empty;

        private string _cardNo = "4925000000000004";

        public string CardNo
        {
            get { return _cardNo; }
            set { _cardNo = value; }
        }

        [STAThread]
        public void Payment(string url, bool reusePayment)
        {
            // Force simple payment form
            url = url.ToLower().Replace("/mobile", "");

            try
            {
                browser = new WebBrowser();
                if (reusePayment)
                {
                    browser.DocumentCompleted += w_ReuseDocumentCompleted;
                }
                else
                {
                    browser.DocumentCompleted += w_DocumentCompleted;
                }

                browser.Navigate(url);

                while (Count < 2)
                {
                    System.Threading.Thread.Sleep(10);
                    System.Windows.Forms.Application.DoEvents();
                }
                Console.WriteLine("END");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }
            finally
            {
                browser.Dispose();
            }
        }

/*
        void w_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                if (Count == 0)
                {
                    Console.WriteLine("Payment form loadet");

                    var inputs = browser.Document.GetElementsByTagName("input");
                    var selects = browser.Document.GetElementsByTagName("select");

                    var easyPaymentMonth = selects.GetElementsByName("CreditCardMobileIssuer$month")[0];
                    easyPaymentMonth.SetAttribute("Value", "12");
                    var easyPaymentYear = selects.GetElementsByName("CreditCardMobileIssuer$year")[0];
                    easyPaymentYear.SetAttribute("Value", "2014");

                    var easyPaymentCardNo = inputs.GetElementsByName("CreditCardMobileIssuer$cardNo")[0];
                    easyPaymentCardNo.SetAttribute("Value", "4925000000000004");

                    var easyPaymentSecurityCode = inputs.GetElementsByName("CreditCardMobileIssuer$securityCode")[0];
                    easyPaymentSecurityCode.SetAttribute("Value", "123");

                    var okButton = inputs.GetElementsByName("payButton")[0];
                    okButton.InvokeMember("Click");

                }
                else
                {
                    Console.WriteLine("Receiving result: {0}", browser.Url.AbsolutePath);
                    var result = browser.Document.GetElementById("responseCode");
                    if (result != null)
                    {
                        this.Result = result.InnerText;
                        Console.WriteLine("Payment result: {0}", this.Result);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DocumentCompleted event error: {0}", ex.Message);
            }

            Count++;

        }
*/

        void w_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                if (Count == 0)
                {
                    Console.WriteLine("Payment form loadet");

                    var inputs = browser.Document.GetElementsByTagName("input");
                    var selects = browser.Document.GetElementsByTagName("select");

                    var easyPaymentMonth = selects.GetElementsByName("EasyPayment$month")[0];
                    easyPaymentMonth.SetAttribute("Value", "12");
                    var easyPaymentYear = selects.GetElementsByName("EasyPayment$year")[0];
                    easyPaymentYear.SetAttribute("Value", DateTime.Now.AddYears(1).ToString("yy"));

                    var easyPaymentCardNo = inputs.GetElementsByName("EasyPayment$cardNumber")[0];
                    easyPaymentCardNo.SetAttribute("Value", _cardNo);

                    var easyPaymentSecurityCode = inputs.GetElementsByName("EasyPayment$securityCode")[0];
                    easyPaymentSecurityCode.SetAttribute("Value", "123");

                    var okButton = inputs.GetElementsByName("okButton")[0];
                    okButton.InvokeMember("Click");

                }
                else
                {
                    Console.WriteLine("Receiving result: {0}", browser.Url.AbsolutePath);
                    var result = browser.Document.GetElementById("responseCode");
                    if (result != null)
                    {
                        this.Result = result.InnerText;
                        Console.WriteLine("Payment result: {0}", this.Result);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DocumentCompleted event error: {0}", ex.Message);
                throw new ApplicationException(string.Format("w_DocumentCompleted error: {0}", ex.Message));
            }

            Count++;

        }


        void w_ReuseDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                if (Count == 0)
                {
                    Console.WriteLine("Reuse Payment form loadet");

                    var inputs = browser.Document.GetElementsByTagName("input");
                    var easyPaymentSecurityCode = browser.Document.GetElementById("securityCode");
                    easyPaymentSecurityCode.SetAttribute("Value", "123");

                    var okButton = inputs.GetElementsByName("okButton")[0];
                    okButton.InvokeMember("Click");
                }
                else
                {
                    Console.WriteLine("Receiving result: {0}", browser.Url.AbsolutePath);
                    var result = browser.Document.GetElementById("responseCode");
                    if (result != null)
                    {
                        this.Result = result.InnerText;
                        Console.WriteLine("Payment result: {0}", this.Result);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DocumentCompleted event error: {0}", ex.Message);
                throw new ApplicationException(string.Format("w_ReuseDocumentCompleted error: {0}", ex.Message));
            }

            Count++;

        }

        public TestPayment()
        {
        }


        public void Dispose()
        {
            if (browser != null)
            {
                browser.Dispose();
            }
        }
    }
}
