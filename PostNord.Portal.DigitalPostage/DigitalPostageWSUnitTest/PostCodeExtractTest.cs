﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PostNord.Portal.DigitalPostage;

namespace DigitalPostageWSUnitTest
{
    [TestClass]
    public class PostCodeExtractTest
    {
        [TestMethod]
        public void TestScrambleCode()
        {
            var scambleResult = PostNord.Portal.DigitalPostage.Bll.Codes.ScrambleCode("99F7893FF5FE");
            Assert.AreEqual(scambleResult, "0AA3AEE13CB7A7CE82C53E5AF42ABDA2E0943EE9");
            /*
             * "99F7-893F-F5FE"
             "99F7893FF5FE"

res
"0AA3AEE13CB7A7CE82C53E5AF42ABDA2E0943EE9"
postCode
"0AA3AEE13CB7A7CE82C53E5AF42ABDA2E0943EE9"
             
             
             */

        }
    }
}
