﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using PostNord.Portal.DigitalPostage.Helpers;
using Microsoft.SharePoint;

namespace AnalyzeJob
{
    public class Initialize
    {

        public static void LoadConfig(string siteUrl)
        {

            ConfigSettings.Values = new System.Collections.Hashtable();

            using (SPSite site = new SPSite(siteUrl))
            {
                using (SPWeb web = site.OpenWeb("/admin"))
                {
                    SPList configList = web.Lists["ConfigSettings"];

                    foreach (SPListItem item in configList.Items)
                    {
                        ConfigSettings.Values.Add(item.Title, (string)item["Value"]);
                    }
                }
            }
        }

        public static void LoadText(string siteUrl)
        {

            //TextSettings.Values = new System.Collections.Hashtable();
            using (SPSite site = new SPSite(siteUrl))
            {
                using (SPWeb web = site.OpenWeb("/admin"))
                {
                    SPList textList = web.Lists["TextSettings"];
                    foreach (SPListItem item in textList.Items)
                    {
                        TextSettings.Values.Add(item.Title, (string)item["Value"]);
                    }
                }
            }
        }


   

    }
}
