﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PN.SoapLibrary.WSSecurity;
using PostNord.Portal.DigitalPostage.NetAxept;
using PostNord.Portal.DigitalPostage.Bll;
using PostNord.Portal.DigitalPostage.Helpers;
using PostNord.Portal.DigitalPostage.MobilePay;
using PostNord.Portal.DigitalPostage;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Configuration;

namespace AnalyzeJob
{
    class Program
    {

        static Parameters _parameters = null;
        static string url = "http://dev";
        static int dayCount = 2;
        static int hourCount = 1;
        static string databaseName = "";
        static string doubleTransactionsFile = "DoubleTransactionsAnalysis";
        static string incorrectRefundStatusFile = "IncorrectRefundStatusAnalysis";
        static string fileExtension = ".csv";
//        static string AnalyzeDoubleTransactionsQuery = @"SELECT	b.*
//                                                        FROM	DigitalPostage.dbo.OrderLogs a
//                                                        JOIN	DigitalPostage.dbo.OrderLogs b
//	                                                    ON b.OrderId = a.OrderId
//                                                        WHERE a.Text LIKE '%deadlocked%'  
//                                                        ORDER BY b.OrderId, b.Id";

        static string multipleTransactionsQuery = @"SELECT * FROM {0}.dbo.OrderLogs
                                                    WHERE Text LIKE '%Register payment. Transaction Id%' 
                                                    and OrderId in (
	                                                    SELECT Orderlogs.OrderId 
	                                                    FROM {0}.dbo.OrderLogs
	                                                    WHERE Text LIKE '%Register payment. Transaction Id%' 
	                                                    GROUP BY Orderlogs.OrderId
	                                                    HAVING COUNT(Orderlogs.OrderId) >= 2 )
                                                    ORDER BY OrderId";

        static string refundedOrdersNotRefundedInNetsQuery = @"SELECT * FROM {0}.dbo.Orders WHERE OrderStatus = '7'";


        static void Main(string[] args)
        {
            databaseName = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
            
            if (string.IsNullOrEmpty(databaseName))
            {
                Console.WriteLine("The configuration key DatabaseName was not set. Please set and then run the applications again.");
                Console.Read();
                System.Environment.Exit(0);
            }
           
            _parameters = new Parameters(args);
            Console.WriteLine("*** AnalyzeJob ***");
            if (_parameters.Help || args.Length == 0)
            {
                Console.WriteLine("Analyze orders");
                Console.WriteLine("Arguments -o analyzedoubletransactions -url <url>");
                Console.WriteLine("Arguments -o analyzewrongrefundstatus -url <url>");
                Console.WriteLine("Url: The url to the web application.");
            }
            

            try
            {
                string cmd = _parameters.GetOptionalParameter("-o").ToLower();
                url = _parameters.GetOptionalParameter("-url");
                if (string.IsNullOrEmpty(url))
                {
                    Console.WriteLine("Url is empty");
                    Console.WriteLine("Please run with url");
                    Console.WriteLine("Terminating application...");
                    Console.WriteLine("Enter a key to terminate...");
                    Console.ReadKey();
                    System.Environment.Exit(-1);

                }
                switch (cmd)
                {

                    case "analyzedoubletransactions":
                        AnalyzeDoubleTransactions();
                        break;
                    case "analyzewrongrefundstatus":
                        AnalyzeWrongRefundStatus();
                        break;


                }
            } catch (Exception ex)
            {

                Console.WriteLine("An error occured: " +ex.Message);
                Console.WriteLine("Terminating application...");
                Console.WriteLine("Press a key to terminate...");
                Console.ReadKey();
            }

        }

        private static int AnalyzeWrongRefundStatus()   
        {

                Initialize.LoadConfig(url);
                string connectionString = HelperClass.GetConnectionString();
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();
                SqlCommand command = new SqlCommand(string.Format(refundedOrdersNotRefundedInNetsQuery, databaseName), conn);
                SqlDataReader reader = null;
                reader = command.ExecuteReader();
                int orderCount = 0;
                int errorCount = 0;
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(incorrectRefundStatusFile+DateTime.Now.ToString().Replace(':', '_').Replace('/', '_') + fileExtension, true))
                {
                    file.WriteLine("OrderId, Status in Digital Postage, NETS Annulled, NETS AnnulledSpecified, NETS Captured, NETS Credited");
                    Console.WriteLine("OrderId, Status in Digital Postage, NETS Annulled, NETS AnnulledSpecified, NETS Captured, NETS Credited");
                    string step = "Reading from database...";
                    while (reader.Read())
                    {
                        //Getting the transaction ID of the text field
                        var oId = reader["Id"].ToString().Trim(); //New way of getting Order ID as ReceiptNumber is empty in some cases. Not knowing why it is empty.
                        var captureId = reader["CaptureId"].ToString().Trim();
                        var orderId = reader["ReceiptNumber"].ToString().Trim();
                        var status = reader["OrderStatus"].ToString().Trim();
                        PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues enumDisplayStatus = (PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues)int.Parse(status);
                        string statusValue = enumDisplayStatus.ToString();
                        
                        //Console.WriteLine("Order Id {0} has multiple transactions. One is: {1} \n", reader["OrderId"], transactionId);
                        step = "Opening order in WebApp...";
                        try
                        {
                            //Orders ordrs = new Orders(long.Parse(orderId));
                            Orders ordrs = new Orders(long.Parse(oId));

                            orderCount++;
                            step = "Opening order in NETS...";
                            PaymentInfo paymentInfo = ordrs.GetNetsStatus();
                            step = "Got order status from NETS...";
                            file.WriteLine(string.Format("{0}, {1}, {2}, {3}, {4}, {5}", orderId, statusValue, paymentInfo.Summary.Annulled, paymentInfo.Summary.AnnulledSpecified, paymentInfo.Summary.AmountCaptured, paymentInfo.Summary.AmountCredited));
                            Console.WriteLine(string.Format("{0}, {1}, {2}, {3}, {4}, {5}", orderId, statusValue, paymentInfo.Summary.Annulled, paymentInfo.Summary.AnnulledSpecified, paymentInfo.Summary.AmountCaptured, paymentInfo.Summary.AmountCredited));
                        }
                        catch (Exception exc)
                        {
                            errorCount++;
                            Console.WriteLine(string.Format("Error occured while {2} on order '{0}' from Web App or NETS: {1}", orderId, exc.Message, step));
                            file.WriteLine(string.Format("Error occured while {2} on orderID '{0}' from Web App or NETS: {1}", orderId, exc.Message, step));

                        }
                        
                    }
                    Console.WriteLine(string.Format("Orders {0} / Errors {1}", orderCount, errorCount));
                    Console.Read();
                    return 1;
                }
        }

        private static int AnalyzeDoubleTransactions()
        {
            //TODO: Query SQL for orders with two "Register payment. Transaction Id" in log
            try
            {
                Initialize.LoadConfig(url);
                string connectionString = HelperClass.GetConnectionString();
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();
                SqlCommand command = new SqlCommand(string.Format(multipleTransactionsQuery, databaseName), conn);
                command.CommandTimeout = 600; ;
                SqlDataReader reader = null;
                reader = command.ExecuteReader();
                string step = "Got results from database...";
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(doubleTransactionsFile + DateTime.Now.ToString().Replace(':', '_').Replace('/', '_') + fileExtension, true))
                {
                    file.WriteLine("OrderId, Transaction ID, Customer e-mail, NETS Annulled, NETS AnnulledSpecified, NETS Captured amount, NETS Credited amount, PaymentMethod");
                    step = "Opening database reader...";
                    while (reader.Read())
                    {
                        //Getting the transaction ID of the text field
                        step = "Getting Text from datbase row..";
                        var transactionId = reader["Text"].ToString().Split(':')[1].Trim();
                        //var oId = reader["Id"].ToString().Trim();
                        step = "Getting OrderId from database row...";
                        var orderId = reader["OrderId"].ToString().Trim();
                        Console.WriteLine("Order Id {0} has multiple transactions. One is: {1} \n", orderId, transactionId);

                        try
                        {
                            step = "Opening order in Digital Postage with Order ID: " + orderId;
                            //Orders ordrs = new Orders(transactionId);
                            Orders ordrs = new Orders(long.Parse(orderId));
                            var meth = ordrs.PaymentMethod;
                            if (meth != Orders.PaymentMethods.MobilePay) //If payment is NETS, NETS Recurring or Unknown
                            {
                                
                                //Orders ordrs = new Orders(long.Parse(orderId);
                                step = "Opening order in NETS...";
                                PaymentInfo paymentInfo = ordrs.GetNetsStatus();
                                step = "Got order status from NETS...";
                                file.WriteLine(orderId + ", " + transactionId + ", " + ", " + ordrs.Email +", " + paymentInfo.Summary.Annulled + ", " + paymentInfo.Summary.AnnulledSpecified + ", " + paymentInfo.Summary.AmountCaptured + ", " + paymentInfo.Summary.AmountCredited + ", " + ordrs.PaymentMethod);
                            }
                            else //If payment is MobilePay
                            {
                                var mpInfo = ordrs.GetMobilePayPaymentInfo();
                                foreach (var mpTransaction in mpInfo.transactions)
                                {
                                    file.WriteLine(orderId + ", " + mpTransaction.TransactionId + ", " + ordrs.Email + ", "+ mpTransaction.PaymentStatus + ", " + mpTransaction.Amount + ", " + ordrs.PaymentMethod);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Error getting the status for the transaction
                            file.WriteLine(orderId + ", " + transactionId + ", An error occured while " + step + ": " + ex.Message);
                        }
                    }

                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("An error occured: " + exc.Message);
                Console.WriteLine("Terminating application....");
                Console.WriteLine("Press a key to exit....");
                Console.ReadKey();
                throw exc;

            } 
            //TODO: Query NETS for status on each of the above orders
            return 0;
        }
    }
}
