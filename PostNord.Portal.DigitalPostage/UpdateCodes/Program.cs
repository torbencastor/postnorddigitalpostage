﻿using Microsoft.ApplicationBlocks.Data;
using PostNord.Portal;
using PostNord.Portal.DigitalPostage.Bll;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace UpdateCodes
{
    class Program
    {

        /// <summary>
        /// Convert Price to Code Generator Price
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        static int ConvertPrice(double value)
        {
            return (int)Math.Round(value * 2);
        }

        /// <summary>
        /// Get the danish week number from given date.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <returns>
        /// The Calendar Week number in da-DK
        /// </returns>
        static int WeekNumber(DateTime fromDate)
        {
            // Get jan 1st of the year
            DateTime startOfYear = new DateTime(fromDate.Year, 1, 1);
            // Get dec 31st of the year
            DateTime endOfYear = new DateTime(fromDate.Year, 12, 31);

            // ISO 8601 weeks start with Monday 
            // The first week of a year includes the first Thursday 
            // DayOfWeek returns 0 for sunday up to 6 for saterday
            int[] iso8601Correction = { 6, 7, 8, 9, 10, 4, 5 };
            int nds = fromDate.Subtract(startOfYear).Days + iso8601Correction[(int)startOfYear.DayOfWeek];
            int wk = nds / 7;
            switch (wk)
            {
                case 0:
                    // Return weeknumber of dec 31st of the previous year
                    return WeekNumber(startOfYear.AddDays(-1));
                case 53:
                    // If dec 31st falls before thursday it is week 01 of next year
                    if (endOfYear.DayOfWeek < DayOfWeek.Thursday)
                        return 1;
                    else
                        return wk;
                default: return wk;
            }
        }

        /// <summary>
        /// Get number of weeks between 2 dates.
        /// </summary>
        /// <param name="periodStart">The period start.</param>
        /// <param name="periodEnd">The period end.</param>
        /// <returns>Number of calendar weeks between 2 days</returns>
        static int WeekCount(DateTime periodStart, DateTime periodEnd)
        {
            const DayOfWeek FIRST_DAY_OF_WEEK = DayOfWeek.Monday;
            const DayOfWeek LAST_DAY_OF_WEEK = DayOfWeek.Sunday;
            const int DAYS_IN_WEEK = 7;

            DateTime firstDayOfWeekBeforeStartDate;
            int daysBetweenStartDateAndPreviousFirstDayOfWeek = (int)periodStart.DayOfWeek - (int)FIRST_DAY_OF_WEEK;
            if (daysBetweenStartDateAndPreviousFirstDayOfWeek >= 0)
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-daysBetweenStartDateAndPreviousFirstDayOfWeek);
            }
            else
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-(daysBetweenStartDateAndPreviousFirstDayOfWeek + DAYS_IN_WEEK));
            }

            DateTime lastDayOfWeekAfterEndDate;
            int daysBetweenEndDateAndFollowingLastDayOfWeek = (int)LAST_DAY_OF_WEEK - (int)periodEnd.DayOfWeek;
            if (daysBetweenEndDateAndFollowingLastDayOfWeek >= 0)
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek);
            }
            else
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek + DAYS_IN_WEEK);
            }

            int calendarWeeks = 1 + (int)((lastDayOfWeekAfterEndDate - firstDayOfWeekBeforeStartDate).TotalDays / DAYS_IN_WEEK);

            return calendarWeeks;
        }



        static int GetPostageNumber(DateTime date)
        {
            int MAX_WEEK_COUNT = 366;
            DateTime startDate = new DateTime(2014, 1, 1);

            int weekCount = WeekCount(startDate, date);
            while (weekCount > MAX_WEEK_COUNT)
            {
                weekCount = weekCount - MAX_WEEK_COUNT;
            }
            return weekCount;
        }


        static void UpdatePostCode(long id, string postCode, string scrambledCode)
        {
            Console.WriteLine("Updating: {0} {1} {2}", id, postCode, scrambledCode);
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, string.Format("UPDATE dbo.Codes SET PostCode='{0}' WHERE Id={1}", scrambledCode,id));
        }


        static bool ProcessCode(IDataReader row, bool test, bool fix)
        {
            long id = (long)row["Id"];
            string postCode = row["PostCode"].ToString();
            int codeId = (int)row["CodeId"];
            double price = (double)row["Price"];
            DateTime issueDate = (DateTime)row["IssueDate"];

            int weekNumber = WeekNumber(issueDate);
            int value = ConvertPrice(price);

            string code = CodeWrapper.GetCode((uint) weekNumber, (uint) value, (uint) codeId);

            if (test)
            {
                code = code.Substring(0, code.Length - 4) + "TEST";
            }

            string scrambledCode = Codes.ScrambleCode(code);

            if (scrambledCode != postCode)
            {
                if (fix)
                {
                    UpdatePostCode(id, code, scrambledCode);
                }
                //Console.WriteLine("Id:{0} Date:{1} Price:{2} Code:{3} PostCode:{4}", codeId, issueDate.Date.ToShortDateString(), price, code, postCode);
                return true;
            }
            else
            {
               // Console.WriteLine("Id:{0} Date:{1} Price:{2} Code:{3} PostCode:{4}", codeId, issueDate.Date.ToShortDateString(), price, code, postCode);
                return false;
            }
        }

        static string connectionString = string.Empty;

        static void Main(string[] args)
        {
            try
            {

                bool fix = false;
                fix = (args.Length > 0 && args[0] == "update");

                connectionString = ConfigurationManager.AppSettings["DigitalPostageConnectionString"];
                bool test = (ConfigurationManager.AppSettings["Test"] == "1");

                int count = 0;
                using (IDataReader row = SqlHelper.ExecuteReader(connectionString, CommandType.Text, "SELECT * FROM dbo.Codes"))
                {
                    int errorCount = 0;
                    while (row.Read())
                    {
                        count++;
                        if (ProcessCode(row, test, fix))
                        {
                            errorCount++;
                        }
                    }
                    Console.WriteLine();
                    Console.WriteLine("Number of Codes: {0} Errors: {1} Difference: {2}", count, errorCount, (count-errorCount));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }



        }
    }
}
