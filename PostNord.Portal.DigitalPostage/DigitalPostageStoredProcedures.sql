﻿GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsUpdateRow]    Script Date: 01/30/2014 14:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsUpdateRow
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: This SP updates ValidationResults table rows.
-- ==========================================================================================


Create Procedure [dbo].[ValidationResultsUpdateRow]
@Id bigint,
@CodeId bigint,
@CodeText nvarchar(15) = NULL,
@Employee nvarchar(50),
@Timestamp datetime,
@ErrorCode int
AS
BEGIN

UPDATE ValidationResults SET [CodeId] = @CodeId, [CodeText] = @CodeText, [Employee] = @Employee, [Timestamp] = @Timestamp, [ErrorCode] = @ErrorCode WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsSelectRow]    Script Date: 01/30/2014 14:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsSelectRow
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: This SP select a specify row from ValidationResults
-- ==========================================================================================


Create Procedure [dbo].[ValidationResultsSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM ValidationResults WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsSelectDate]    Script Date: 01/30/2014 14:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsSelectDate
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/3/2014 7:11:27 AM
-- Description: This SP Inserts value to ValidationErrors table
-- ==========================================================================================

CREATE Procedure [dbo].[ValidationResultsSelectDate]
@FromDate datetime,
@ToDate datetime
AS
BEGIN

SELECT * FROM ValidationResults WHERE [Timestamp]>=@FromDate AND [Timestamp]<=@ToDate ORDER BY Timestamp ASC

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsSelectAll]    Script Date: 01/30/2014 14:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsSelectAll
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: Select all rows form ValidationResults
-- ==========================================================================================



Create Procedure [dbo].[ValidationResultsSelectAll]
AS
BEGIN

SELECT * FROM ValidationResults

END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsInsertRow]    Script Date: 01/30/2014 14:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsInsertRow
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: This SP Inserts value to ValidationResults table
-- ==========================================================================================


Create Procedure [dbo].[ValidationResultsInsertRow]
@CodeId bigint,
@CodeText nvarchar(15) = NULL,
@Employee nvarchar(50),
@Timestamp datetime,
@ErrorCode int
AS
BEGIN

    INSERT INTO ValidationResults
        ([CodeId], [CodeText], [Employee], [Timestamp], [ErrorCode])
    VALUES
        (@CodeId, @CodeText, @Employee, @Timestamp, @ErrorCode)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsDeleteRow]    Script Date: 01/30/2014 14:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsDeleteRow
-- Author:  Hamid Hossein vand
-- Create date: 1/9/2014 1:56:33 PM
-- Description: This SP delete specify row from ValidationResults table
-- ==========================================================================================


Create Procedure [dbo].[ValidationResultsDeleteRow]
@Id bigint
AS
BEGIN

DELETE ValidationResults WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductsUpdateRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: This SP updates Products table rows.
-- ==========================================================================================


Create Procedure [dbo].[ProductsUpdateRow]
@Id bigint,
@Name nvarchar(50)
AS
BEGIN

UPDATE Products SET [Name] = @Name WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductsSelectRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: This SP select a specify row from Products
-- ==========================================================================================


Create Procedure [dbo].[ProductsSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM Products WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[ProductsSelectAll]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: Select all rows form Products
-- ==========================================================================================



Create Procedure [dbo].[ProductsSelectAll]
AS
BEGIN

SELECT * FROM Products

END
GO
/****** Object:  StoredProcedure [dbo].[ProductsInsertRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: This SP Inserts value to Products table
-- ==========================================================================================


CREATE Procedure [dbo].[ProductsInsertRow]
@Name nvarchar(50)
AS
BEGIN

    INSERT INTO Products
        ([Name])
    VALUES
        (@Name)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[ProductsDeleteRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ProductsDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:46
-- Description: This SP delete specify row from Products table
-- ==========================================================================================


Create Procedure [dbo].[ProductsDeleteRow]
@Id bigint
AS
BEGIN

DELETE Products WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectDate]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/10/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Orders table
-- ==========================================================================================
CREATE Procedure [dbo].[OrdersSelectDate]
@FromDate datetime,
@ToDate datetime
AS
BEGIN

SELECT * FROM Orders WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate ORDER BY SalesDate ASC

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectCaptureId]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectCaptureId
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/21/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Orders table
-- ==========================================================================================
CREATE Procedure [dbo].[OrdersSelectCaptureId]
@CaptureId nvarchar(50)

AS
BEGIN

SELECT * FROM Orders WHERE [CaptureId] LIKE @CaptureId ORDER BY SalesDate ASC

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectAll]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: Select all rows form Orders
-- ==========================================================================================



Create Procedure [dbo].[OrdersSelectAll]
AS
BEGIN

SELECT * FROM Orders

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersInsertRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: This SP Inserts value to Orders table
-- ==========================================================================================


CREATE Procedure [dbo].[OrdersInsertRow]
@Email nvarchar(50),
@ReceiptNumber nvarchar(50),
@CaptureId nvarchar(50),
@SalesDate datetime,
@ExpireDate datetime,
@OrderSum float,
@CreditSum float,
@Saldo float,
@OrderStatus smallint
AS
BEGIN

    INSERT INTO Orders
        ([Email], [ReceiptNumber], [CaptureId], [ImageSize], [SalesDate], [ExpireDate], [OrderSum], [CreditSum], [Saldo], [OrderStatus])
    VALUES
        (@Email, @ReceiptNumber, @CaptureId, 0, @SalesDate, @ExpireDate, @OrderSum, @CreditSum, @Saldo, @OrderStatus)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[OrdersDeleteRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: This SP delete specify row from Orders table
-- ==========================================================================================


Create Procedure [dbo].[OrdersDeleteRow]
@Id bigint
AS
BEGIN

DELETE Orders WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsUpdateRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsUpdateRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP updates CodeIds table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[CodeIdsUpdateRow]
@Id bigint
AS
BEGIN

UPDATE CodeIds WITH (ROWLOCK) SET [CodeId] = [CodeId] + 1
OUTPUT INSERTED.CodeId
WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSetRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSetRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP updates CodeIds table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[CodeIdsSetRow]
@Id bigint,
@CodeId int
AS
BEGIN

UPDATE CodeIds WITH (ROWLOCK) SET [CodeId] = @CodeId
OUTPUT INSERTED.CodeId
WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSelectRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP select a specify row from CodeIds
-- ==========================================================================================


Create Procedure [dbo].[CodeIdsSelectRow]
@CodeId int,
@CodeDate date
AS
BEGIN

SELECT * FROM CodeIds WHERE [CodeId]=@CodeId and [CodeDate]=@CodeDate

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSelectDateRange]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSelectDateRange
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP select a specify row from CodeIds
-- ==========================================================================================


Create Procedure [dbo].[CodeIdsSelectDateRange]
@FromDate date,
@ToDate date
AS
BEGIN

SELECT * FROM CodeIds WHERE [CodeDate]>=@FromDate AND [CodeDate]<=@ToDate

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSelectDate]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSelectDate
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP select a specify row from CodeIds
-- ==========================================================================================


CREATE Procedure [dbo].[CodeIdsSelectDate]
@CodeDate date
AS
BEGIN

SELECT * FROM CodeIds WITH (HOLDLOCK) WHERE [CodeDate]=@CodeDate

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsSelectAll]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsSelectAll
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: Select all rows form CodeIds
-- ==========================================================================================



Create Procedure [dbo].[CodeIdsSelectAll]
AS
BEGIN

SELECT * FROM CodeIds

END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsInsertRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsInsertRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP Inserts value to CodeIds table
-- ==========================================================================================


Create Procedure [dbo].[CodeIdsInsertRow]
@CodeId int,
@CodeDate date
AS
BEGIN

    INSERT INTO CodeIds
        ([CodeId], [CodeDate])
    VALUES
        (@CodeId, @CodeDate)
END
GO
/****** Object:  StoredProcedure [dbo].[CodeIdsDeleteRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodeIdsDeleteRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/15/2014 9:29:27 AM
-- Description: This SP delete specify row from CodeIds table
-- ==========================================================================================


CREATE Procedure [dbo].[CodeIdsDeleteRow]
@Id bigint
AS
BEGIN

DELETE CodeIds WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[NextKeyUpdateRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[NextKeyUpdateRow]
@Id bigint,
@CodeId int
AS
BEGIN

UPDATE NextKey SET [CodeId] = @CodeId OUTPUT INSERTED.CodeId WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[NextKeyUpdateKey]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[NextKeyUpdateKey]
@Id bigint,
@CodeCount int
AS
BEGIN

UPDATE NextKey SET [CodeId] = [CodeId] + @CodeCount OUTPUT INSERTED.CodeId WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[NextKeySelectRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: NextKeySelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/23/2014 9:14:37 AM
-- Description: This SP select a specify row from NextKey
-- ==========================================================================================


Create Procedure [dbo].[NextKeySelectRow]
@Id bigint
AS
BEGIN

SELECT CodeId FROM NextKey WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[NextKeyInsertRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[NextKeyInsertRow]
@CodeId int
AS
BEGIN
    INSERT INTO NextKey ([Id],[CodeId]) OUTPUT INSERTED.CodeId VALUES  (1, @CodeId) 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdersUpdateRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: This SP updates Orders table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[OrdersUpdateRow]
@Id bigint,
@Email nvarchar(50),
@ReceiptNumber nvarchar(50),
@CaptureId nvarchar(50),
@SalesDate datetime,
@ExpireDate datetime,
@OrderSum float,
@CreditSum float,
@Saldo float,
@OrderStatus smallint

AS
BEGIN

UPDATE Orders SET [Email] = @Email, [ReceiptNumber] = @ReceiptNumber, [CaptureId] = @CaptureId, [SalesDate] = @SalesDate, [ExpireDate] = @ExpireDate, [OrderSum] = @OrderSum, [CreditSum] = @CreditSum, [Saldo] = @Saldo, [OrderStatus] = @OrderStatus WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersUpdateImage]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Entity Name: OrdersUpdateImage
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 06-01-2013 15:12:34
-- Description: This SP updates Orders table rows.
-- =============================================
CREATE Procedure [dbo].[OrdersUpdateImage]
@Id bigint,
@LabelImage varbinary(max),
@ImageSize int
AS
BEGIN

UPDATE Orders SET [LabelImage] = @LabelImage, [ImageSize] = @ImageSize WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:34
-- Description: This SP select a specify row from Orders
-- ==========================================================================================


Create Procedure [dbo].[OrdersSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM Orders WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectDateEmail]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectDateEmail
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/10/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Orders table
-- ==========================================================================================
CREATE Procedure [dbo].[OrdersSelectDateEmail]
@FromDate datetime,
@ToDate datetime,
@Email nvarchar(50)

AS
BEGIN

SELECT * FROM Orders WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND [Email] LIKE @Email ORDER BY SalesDate ASC

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsUpdateRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: This SP updates OrderLogs table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[OrderLogsUpdateRow]
@Id bigint,
@OrderId bigint,
@Timestamp datetime,
@Severity smallint,
@Text nvarchar(MAX)
AS
BEGIN

UPDATE OrderLogs SET [OrderId] = @OrderId, [Timestamp] = @Timestamp, [Severity] = @Severity, [Text] = @Text WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsSelectRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: This SP select a specify row from OrderLogs
-- ==========================================================================================


Create Procedure [dbo].[OrderLogsSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM OrderLogs WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsSelectForOrder]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: Select all rows form OrderLogs matching
-- ==========================================================================================


CREATE Procedure [dbo].[OrderLogsSelectForOrder]
@OrderId bigint
AS
BEGIN

SELECT * FROM OrderLogs WHERE [OrderId] = @OrderId ORDER BY [Timestamp] ASC

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsSelectAll]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: Select all rows form OrderLogs
-- ==========================================================================================



Create Procedure [dbo].[OrderLogsSelectAll]
AS
BEGIN

SELECT * FROM OrderLogs

END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsInsertRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: This SP Inserts value to OrderLogs table
-- ==========================================================================================


CREATE Procedure [dbo].[OrderLogsInsertRow]
@OrderId bigint,
@Timestamp datetime,
@Severity smallint,
@Text nvarchar(MAX)
AS
BEGIN

    INSERT INTO OrderLogs
        ([OrderId], [Timestamp], [Severity], [Text])
    VALUES
        (@OrderId, @Timestamp, @Severity, @Text)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[OrderLogsDeleteRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrderLogsDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:12:13
-- Description: This SP delete specify row from OrderLogs table
-- ==========================================================================================


Create Procedure [dbo].[OrderLogsDeleteRow]
@Id bigint
AS
BEGIN

DELETE OrderLogs WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsUpdateRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: This SP updates Refunds table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[RefundsUpdateRow]
@Id bigint,
@OrderNr bigint,
@Timestamp datetime,
@Name nvarchar(50),
@Address nvarchar(50),
@PostNumber int,
@City nvarchar(50),
@Phone nvarchar(50) = NULL,
@Message nvarchar(1000) = NULL,
@CreditSum float
AS
BEGIN

UPDATE Refunds SET [OrderNr] = @OrderNr, [Timestamp] = @Timestamp, [Name] = @Name, [Address] = @Address, [PostNumber] = @PostNumber, [City] = @City, [Phone] = @Phone, [Message] = @Message, [CreditSum] = @CreditSum WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsSelectRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: This SP select a specify row from Refunds
-- ==========================================================================================


Create Procedure [dbo].[RefundsSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM Refunds WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsSelectOrderId]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectOrderId
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: Select all rows form Refunds matching orderNr
-- ==========================================================================================



Create Procedure [dbo].[RefundsSelectOrderId]
@OrderNr bigint
AS
BEGIN

SELECT * FROM Refunds WHERE OrderNr=@OrderNr ORDER BY Timestamp ASC

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsSelectDate]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/3/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Refunds table
-- ==========================================================================================
CREATE Procedure [dbo].[RefundsSelectDate]
@FromDate datetime,
@ToDate datetime
AS
BEGIN

SELECT * FROM Refunds WHERE [Timestamp]>=@FromDate AND [Timestamp]<=@ToDate ORDER BY Timestamp ASC

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsSelectAll]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: Select all rows form Refunds
-- ==========================================================================================



CREATE Procedure [dbo].[RefundsSelectAll]
AS
BEGIN

SELECT * FROM Refunds ORDER BY Timestamp ASC

END
GO
/****** Object:  StoredProcedure [dbo].[RefundsInsertRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: This SP Inserts value to Refunds table
-- ==========================================================================================


CREATE Procedure [dbo].[RefundsInsertRow]
@OrderNr bigint,
@Timestamp datetime,
@Name nvarchar(50),
@Address nvarchar(50),
@PostNumber int,
@City nvarchar(50),
@Phone nvarchar(50) = NULL,
@Message nvarchar(1000) = NULL,
@CreditSum float
AS
BEGIN

    INSERT INTO Refunds
        ([OrderNr], [Timestamp], [Name], [Address], [PostNumber], [City], [Phone], [Message], [CreditSum])
    VALUES
        (@OrderNr, @Timestamp, @Name, @Address, @PostNumber, @City, @Phone, @Message, @CreditSum)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[RefundsDeleteRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: RefundsDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:00
-- Description: This SP delete specify row from Refunds table
-- ==========================================================================================


Create Procedure [dbo].[RefundsDeleteRow]
@Id bigint
AS
BEGIN

DELETE Refunds WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersUpdateRow]    Script Date: 01/30/2014 14:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: This SP updates SubOrders table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[SubOrdersUpdateRow]
@Id bigint,
@OrderId bigint,
@ProductId bigint,
@Weight smallint,
@Priority nvarchar(50),
@Destination nvarchar(50),
@Amount int,
@Price float,
@SubOrderSum float,
@SubOrderCreditSum float,
@SubOrderSaldo float
AS
BEGIN

UPDATE SubOrders SET [OrderId] = @OrderId, [ProductId] = @ProductId, [Weight] = @Weight, [Priority] = @Priority, [Destination] = @Destination, [Amount] = @Amount, [Price] = @Price, [SubOrderSum] = @SubOrderSum, [SubOrderCreditSum] = @SubOrderCreditSum, [SubOrderSaldo] = @SubOrderSaldo WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersSelectRow]    Script Date: 01/30/2014 14:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: This SP select a specify row from SubOrders
-- ==========================================================================================


Create Procedure [dbo].[SubOrdersSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM SubOrders WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersSelectFromOrderId]    Script Date: 01/30/2014 14:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[SubOrdersSelectFromOrderId]
@OrderId bigint

AS
BEGIN

SELECT * FROM SubOrders WHERE [OrderId] = @OrderId

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersSelectAll]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: Select all rows form SubOrders
-- ==========================================================================================



Create Procedure [dbo].[SubOrdersSelectAll]
AS
BEGIN

SELECT * FROM SubOrders

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersInsertRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: This SP Inserts value to SubOrders table
-- ==========================================================================================


CREATE Procedure [dbo].[SubOrdersInsertRow]
@OrderId bigint,
@ProductId bigint,
@Weight smallint,
@Priority nvarchar(50),
@Destination nvarchar(50),
@Amount int,
@Price float,
@SubOrderSum float,
@SubOrderCreditSum float,
@SubOrderSaldo float
AS
BEGIN

    INSERT INTO SubOrders
        ([OrderId], [ProductId], [Weight], [Priority], [Destination], [Amount], [Price], [SubOrderSum], [SubOrderCreditSum], SubOrderSaldo)
    VALUES
        (@OrderId, @ProductId, @Weight, @Priority, @Destination, @Amount, @Price, @SubOrderSum, @SubOrderCreditSum, @SubOrderSaldo)
    SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersDeleteRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: This SP delete specify row from SubOrders table
-- ==========================================================================================


Create Procedure [dbo].[SubOrdersDeleteRow]
@Id bigint
AS
BEGIN

DELETE SubOrders WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[SubOrdersChangeCodeStatus]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersChangeCodeStatus
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 28-01-2014 13:12:34
-- Description: This SP Changes the status of codes for a given SubOrder
-- ==========================================================================================

CREATE Procedure [dbo].[SubOrdersChangeCodeStatus]
@SubOrderId bigint,
@OldStatus smallint,
@NewStatus smallint

AS
BEGIN
UPDATE Codes SET [Status] = @NewStatus WHERE [SubOrderId]=@SubOrderId AND [Status] = @OldStatus;
END
GO
/****** Object:  StoredProcedure [dbo].[SubOrderGetCreditCodeCount]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SubOrderGetCreditCodeCount] 
@SubOrderId bigint
AS
BEGIN
	SELECT COUNT(Id) FROM [dbo].[Codes] WHERE SubOrderId=@SubOrderId AND RefundId > 0;
END
GO
/****** Object:  StoredProcedure [dbo].[CodesUpdateUsedDate]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[CodesUpdateUsedDate]
@Id bigint,
@Status smallint,
@UsedDate datetime
AS
BEGIN

UPDATE Codes SET [Status] = @Status, [UsedDate] = @UsedDate WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodesUpdateRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create IssueDate: 17-12-2013 15:11:56
-- Description: This SP updates Codes table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[CodesUpdateRow]
@Id bigint,
@PostCode nchar(15),
@SubOrderId bigint,
@CodeId int,
@Status smallint,
@IssueDate datetime,
@UsedDate datetime,
@Price float,
@ExpireDate date,
@RefundedDate datetime,
@RefundId bigint
AS
BEGIN

UPDATE Codes SET [PostCode] = @PostCode, [SubOrderId] = @SubOrderId,  [CodeId]=@CodeId ,[Status] = @Status, [IssueDate]=@IssueDate, [UsedDate] = @UsedDate, [Price] = @Price, [ExpireDate] = @ExpireDate, [RefundedDate] = @RefundedDate, [RefundId] = @RefundId WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodesUpdateRefund]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[CodesUpdateRefund]
@Id bigint,
@Status smallint,
@RefundedDate datetime,
@RefundId bigint
AS
BEGIN

UPDATE Codes SET [Status] = @Status, [RefundedDate] = @RefundedDate, [RefundId] = @RefundId WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodesSelectRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesSelectRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: This SP select a specify row from Codes
-- ==========================================================================================


Create Procedure [dbo].[CodesSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM Codes WHERE [Id]=@Id

END
GO
/****** Object:  StoredProcedure [dbo].[CodesSelectForSubOrder]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesSelectForSubOrder
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: Select rows from Codes that matches SubOrder
-- ==========================================================================================



Create Procedure [dbo].[CodesSelectForSubOrder]
@SubOrderId as bigint
AS
BEGIN

SELECT * FROM Codes WHERE SubOrderId=@SubOrderId

END
GO
/****** Object:  StoredProcedure [dbo].[CodesSelectCode]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesUpdateRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: This SP updates Codes table rows.
-- ==========================================================================================


CREATE Procedure [dbo].[CodesSelectCode]
@PostCode nchar(50)
AS
BEGIN

SELECT * FROM Codes WHERE [PostCode]=@PostCode

END
GO
/****** Object:  StoredProcedure [dbo].[CodesSelectAll]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesSelectAll
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: Select all rows form Codes
-- ==========================================================================================



Create Procedure [dbo].[CodesSelectAll]
AS
BEGIN

SELECT * FROM Codes

END
GO
/****** Object:  StoredProcedure [dbo].[CodesInsertRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[CodesInsertRow]
@PostCode nchar(50),
@SubOrderId bigint,
@CodeId int,
@Status smallint,
@Price float,
@IssueDate datetime,
@ExpireDate date
AS
BEGIN
    INSERT INTO Codes
        ([PostCode], [SubOrderId], [CodeId], [Status], [Price],[IssueDate], [ExpireDate])
    VALUES
        (@PostCode, @SubOrderId, @CodeId, @Status, @Price, @IssueDate, @ExpireDate)
	SELECT SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[CodesDeleteRow]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesDeleteRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:11:56
-- Description: This SP delete specify row from Codes table
-- ==========================================================================================


Create Procedure [dbo].[CodesDeleteRow]
@Id bigint
AS
BEGIN

DELETE Codes WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectDateCode]    Script Date: 01/30/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: OrdersSelectDateCode
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/10/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Orders table that matches code
-- ==========================================================================================
CREATE Procedure [dbo].[OrdersSelectDateCode]
@FromDate datetime,
@ToDate datetime,
@PostCode nvarchar(50)

AS
BEGIN

SELECT * FROM ViewOrdersSubOrdersCodes 
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND [PostCode] = @PostCode ORDER BY SalesDate ASC

END
GO

/******************************************************************************************

ADDED SPRINT 4

******************************************************************************************/


GO
/****** Object:  StoredProcedure [dbo].[ValidationResultsSelectDateAndCode]    Script Date: 02/11/2014 10:44:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ValidationResultsSelectDateAndCode
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/3/2014 7:11:27 AM
-- Description: Select ValidationResults from daterange and CodeId
-- ==========================================================================================

CREATE Procedure [dbo].[ValidationResultsSelectDateAndCode]
@FromDate datetime,
@ToDate datetime,
@CodeId bigint
AS
BEGIN

SELECT * FROM ValidationResults WHERE [CodeId]=@CodeId AND [Timestamp]>=@FromDate AND [Timestamp]<=@ToDate ORDER BY Timestamp ASC

END
