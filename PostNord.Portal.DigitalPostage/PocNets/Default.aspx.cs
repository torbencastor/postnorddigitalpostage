﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string MerchantId = "12001082";
        string Token = "4m=Q-No6";
        string RedirectUrl = "http://www.google.dk";

        try
        {
            BBS.Netaxept netClient = new BBS.Netaxept();

            // Setup the terminal parameters (required parameters for Netaxept-hosted terminal)
            BBS.Terminal terminal = new BBS.Terminal();
            terminal.OrderDescription = "Just a test transaction";
            terminal.RedirectUrl = RedirectUrl;

            // Setup the order (required parameters)
            BBS.Order order = new BBS.Order();
            order.Amount = "39500";          // 395,00
            order.CurrencyCode = "NOK";      // Norwegian Kroner
            order.OrderNumber = "12345";     // A number to be able to track the order

            // Setup the environment (required for all web service clients)
            BBS.Environment environment = new BBS.Environment();
            environment.WebServicePlatform = "DOTNET20";

            // Finally the complete register request
            BBS.RegisterRequest registerRequest = new BBS.RegisterRequest();
            registerRequest.Order = order;
            registerRequest.Terminal = terminal;
            registerRequest.Environment = environment;

            BBS.RegisterResponse response = netClient.Register(MerchantId, Token, registerRequest);

            this.demoLabelStatus.Text = response.TransactionId;

            BBS.ProcessRequest saleRequest = new BBS.ProcessRequest();
            saleRequest.Operation = "SALE";
            saleRequest.TransactionId = response.TransactionId;

            BBS.ProcessResponse processResponse = netClient.Process(MerchantId, Token, saleRequest);
            this.demoLabelStatus.Text = processResponse.ResponseCode;
        }
        catch(Exception ex)
        {
            this.demoLabelStatus.Text = ex.Message;
        }
    }
}