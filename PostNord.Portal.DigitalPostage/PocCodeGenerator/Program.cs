﻿using System;
using System.Linq;
using Microsoft.SharePoint;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Security.Cryptography;



namespace PocCodeGenerator
{
        
    class Program
    {
        //typedef __declspec(dllimport) int (*dk_mf_encode_c_f)(char* dk_mf, char* dat);
        //typedef __declspec(dllimport) int (*dk_mf_decode_c_f)(char* dat, int* n_c, char* dk_mf_c, char* dk_mf);
        //typedef __declspec(dllimport) char* (*dk_mf_get_version_c_f)(void);

        //<assemblyIdentity type="win32" name="Microsoft.VC90.CRT" version="9.0.21022.8" processorArchitecture="amd64" publicKeyToken="1fc8b3b9a1e18e3b"></assemblyIdentity>
        //[DllImport("unmanaged.dll, MyAssembly, Version= 1.0.0.0, Culture=neutral, PublicKeyToken=a77e0ba5eab10125")]
        //dk_mf.dll Version 1.4.0.0

//        [DllImport(@"C:\tmp\dk_mf.dll, dk_mf, Version 1.4.0.0, Culture=neutral, publicKeyToken=1fc8b3b9a1e18e3b", CharSet = CharSet.Ansi)]
        [DllImport(@"C:\tmp\dk_mf.dll", CharSet = CharSet.Ansi)]
        public static extern IntPtr dk_mf_get_version_c();

        /// <summary>
        /// Dk_mf_encode_cs the specified code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        [DllImport(@"C:\tmp\dk_mf.dll", CharSet = CharSet.Ansi)]
        static extern int dk_mf_encode_c( StringBuilder code, StringBuilder data);

        [DllImport(@"C:\tmp\dk_mf.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern int dk_mf_decode_c(StringBuilder decodedPayloadData, out int numberOfCorrections, StringBuilder correctedMobileFrankingCode, StringBuilder code);

        private static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();  
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }


        static void Main(string[] args)
        {
            int MAX_SIZE = 200;
            try
            {
                IntPtr o = dk_mf_get_version_c();
                string version = Marshal.PtrToStringAnsi(o);
                Console.WriteLine("Version: {0}", version);

                StringBuilder sb = new StringBuilder(MAX_SIZE);

                int res = dk_mf_encode_c(sb, new StringBuilder("183 100 400000"));
                Console.WriteLine("Encode Result: {0} Kode: {1} ", (res == 1 ? "OK" : "Error"), sb);
                Console.WriteLine("Hashed result: {0}", GetHashString(sb.ToString()));

                StringBuilder dk_mf_c = new StringBuilder(200);
                StringBuilder dk_mf = new StringBuilder(200);

                StringBuilder decodedPayloadData = new StringBuilder(1000);
                StringBuilder correctedMobileFrankingCode = new StringBuilder(1000);
                int numberOfCorrections = 0;
                StringBuilder code = new StringBuilder("5L3C 9VE8 99AP");

                res = dk_mf_decode_c(decodedPayloadData, out numberOfCorrections, correctedMobileFrankingCode, code);
                Code decodedCode = new Code(decodedPayloadData.ToString());
                Console.WriteLine(String.Format(new System.Globalization.CultureInfo("da-DK"), "Day:{0} Franking:{1} Value:{2:C} Number:{3}", decodedCode.Day, decodedCode.Franking, decodedCode.FrankingValue, decodedCode.Number));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }

        }
    }
}
