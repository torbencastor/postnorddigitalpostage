/****

Test application showing the use of the mobile-franking-code DLL dk_mf.dll

****/

#include "windows.h"
#include "dk_mf_dll.h"
#include "stdio.h"

#define MAX_STRING 200

/** DLL handle */
static HINSTANCE g_mfDLL=NULL; 

/** handle to function pointers*/     
static dk_mf_encode_c_f  dk_mf_encode_c=NULL; 
static dk_mf_decode_c_f  dk_mf_decode_c=NULL; 
static dk_mf_get_version_c_f dk_mf_get_version_c=NULL; 

/**
  opens the encoder dll and get the functions
  @return 
  -  0 = ok, 
  - -1 = Dll already opened
  - -2 = LoadLibrary() failed
*/
static int load_dll(void){ 
  if(g_mfDLL!= NULL){ 
    printf("\n Error: DLL already open\n");
    return -1;
  } 
  if(NULL==(g_mfDLL = LoadLibrary("dk_mf.dll"))) { 
    printf("\n Error: LoadLibrary() failed\n");
    return -2;
  }
  dk_mf_encode_c = (dk_mf_encode_c_f)GetProcAddress(g_mfDLL,"dk_mf_encode_c");
  dk_mf_decode_c = (dk_mf_decode_c_f)GetProcAddress(g_mfDLL,"dk_mf_decode_c");
  dk_mf_get_version_c = (dk_mf_get_version_c_f)GetProcAddress(g_mfDLL,"dk_mf_get_version_c");
  printf("%s\n",dk_mf_get_version_c());
  return 0;
}

/**
  free the encoder DLL
  @return 
  -  0 = ok, 
  - -1 = DLL not loaded
  - -2 = FreeLibrary() failed
*/
static int free_dll(void) {
  if(NULL==g_mfDLL)           return -1; /* DLL not loaded     */
  if(0==FreeLibrary(g_mfDLL)) return -2; /* FreeLibrary failed */
  g_mfDLL=NULL;  
  return 0;
}

/**
  read in a line from the data_file and remove the cr-lr characters
  @param buf       [out]  stores a line fo the inputfile
  @param data_file [in]   the data file
  @return a char pointer to the buffer or NULL in case of errors
*/
static char* get_line(char* buf, FILE* data_file) {
  int icnt;
  char* ptr=NULL;
  if((NULL==buf)||(NULL==data_file))              return ptr;
  if(NULL==(ptr=fgets(buf,MAX_STRING-1,data_file))) return ptr;
  for(icnt=(strlen(ptr)-1);icnt>=0;icnt--) {
    if(ptr[icnt]=='\n' || ptr[icnt]=='\r') {
      ptr[icnt]=0;
    }
  }
  return ptr;
}

/**
  Opens the encoder DLL and reads in inputdata form a file, and encodes the 
  inputdata to the mobile franking code. The inputdata file is formatted as 
  followed, starting with command and data E=encode D=decode:

        E 366 500 425010
        E   0   0      1
        E 183 100 400000
        D YCNF YMC3 NE00
        D MMFV FHRF HF8M

  @param argc [in] number of commandline arguments
  @param argv [in] command line arguments
  
*/
void main(int argc, char* argv[]) {	
  FILE* df=NULL;
  char* ptr=NULL;
  char str[MAX_STRING];
  char cmd[MAX_STRING];
  char mf_data[MAX_STRING];    /* pay load data */
  char mf_code[MAX_STRING];    /* mobile franking code */
  char mf_corr[MAX_STRING];    /* corrected mobile franking code */
  int len=0,valid=0,n_c=0;
  memset(cmd,0,MAX_STRING);
  memset(str,0,MAX_STRING);
  memset(mf_data,0,MAX_STRING);
  memset(mf_code,0,MAX_STRING);
  memset(mf_corr,0,MAX_STRING);
  printf("(c)2011 SIEMENS: Post Danmark mobile franking encoder\n");
  if(argc<3) {
    printf("usage:\n"
      "encode = %s E 183 100 400000 \n"
      "decode = %s D YCNF YMC3 NE00 \n "
      "file   = %s F testdata.txt\n" ,argv[0],argv[0],argv[0]);
    exit(-1);
  }
  if(0!=load_dll()) {
    exit(-1);
  }
  sprintf(cmd,"%s",argv[1]);
  switch(cmd[0]) {
    case 'E': {
      sprintf(mf_data,"%s %s %s",argv[2],argv[3],argv[4]);
      if(1==(valid=dk_mf_encode_c(mf_code,mf_data))){
        printf("encode mobile franking code %s --> %s\n",mf_data,mf_code);
      } else {
        printf("encode mobile franking code %s --> FAILED\n",mf_data);
      }
      break;
    }
    case 'D': {
      sprintf(mf_code,"%s %s %s",argv[2],argv[3],argv[4]);
      if(1==(valid=dk_mf_decode_c(mf_data,&n_c,mf_corr,mf_code))){
        printf("decode mobile franking code %s --> %s e=%d --> %s\n",mf_code,mf_corr,n_c,mf_data);
      } else {
        printf("decode mobile franking code %s --> FAILED\n",mf_code);
      }
      break;
    }
    case 'F': {
      if(NULL==(df=fopen(argv[2],"r"))) {
        printf("open data file: %s failed\n",argv[2]);
        exit(-1);
      }
      ptr=get_line(str,df);  
      while(NULL!=ptr) {
        switch(str[0]){
          case 'E': {   /* encode day,franking,data to mobile franking code */
            memset(mf_code,0,MAX_STRING);
            if(1==(valid=dk_mf_encode_c(mf_code,&str[1]))){
              printf("encode mobile franking code %s --> %s\n",str,mf_code);
            } else {
              printf("encode mobile franking code %s --> FAILED\n",str);
            }
            break; 
          }
          case 'D': {   /* decode mobile franking code to day,franking, data */
            n_c=0;                  /* number of corrections */
            memset(mf_data,0,MAX_STRING); /* decoded payload data */
            memset(mf_code,0,MAX_STRING); /* corrected mobile franking code */
            if(1==(valid=dk_mf_decode_c(mf_data,&n_c,mf_code,&str[1]))){
              printf("decode mobile franking code %s --> %s e=%d --> %s\n",str,mf_code,n_c,mf_data);
            } else {
              printf("decode mobile franking code %s --> FAILED\n",str);
            }
            break; 
          }
          default: {
            printf("invalid command %s\n",ptr);
            break; 
          }
        }
        ptr=get_line(str,df); /* get next data line form input file */
      }
      break;
    }
  }
  free_dll(); /* unload the dll */
}

