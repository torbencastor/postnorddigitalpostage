#ifndef DK_MF_DLL_H
#define DK_MF_DLL_H

#ifdef  __cplusplus
extern  "C" {  
#endif
                                        
typedef __declspec(dllimport) int   (*dk_mf_encode_c_f)(char* dk_mf, char* dat);
typedef __declspec(dllimport) int   (*dk_mf_decode_c_f)(char* dat, int* n_c, char* dk_mf_c, char* dk_mf);
typedef __declspec(dllimport) char* (*dk_mf_get_version_c_f)(void);

#ifdef  __cplusplus
}
#endif

#endif 
