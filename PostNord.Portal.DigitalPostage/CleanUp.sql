﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[OrdersGetStatus]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT  COUNT(Id)
FROM  dbo.Orders 
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2';


SELECT  COUNT(Id)
FROM    dbo.Orders 
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus <> '2';

END

GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[OrdersDeleteUnPaid]
@FromDate datetime,
@ToDate datetime
AS
BEGIN


DELETE
FROM    dbo.Orders 
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus <> '2';
END

GO
Grant EXECUTE On [OrdersDeleteUnPaid] To [DigitalPostageTest]

Grant EXECUTE On [OrdersGetStatus] To [DigitalPostageTest]