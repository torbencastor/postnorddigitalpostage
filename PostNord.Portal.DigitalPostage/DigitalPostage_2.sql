﻿F/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.SubOrders ADD
	ProductListId int NULL
GO
ALTER TABLE dbo.SubOrders SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/****** Object:  StoredProcedure [dbo].[SubOrdersInsertRow]    Script Date: 07/31/2014 15:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: SubOrdersInsertRow
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 17-12-2013 15:13:25
-- Description: This SP Inserts value to SubOrders table
-- ==========================================================================================


ALTER Procedure [dbo].[SubOrdersInsertRow]
@OrderId bigint,
@ProductId bigint,
@Weight smallint,
@Priority nvarchar(50),
@Destination nvarchar(50),
@Amount int,
@Price float,
@SubOrderSum float,
@SubOrderCreditSum float,
@SubOrderSaldo float,
@ProductListId int
AS
BEGIN

    INSERT INTO SubOrders
        ([OrderId], [ProductId], [Weight], [Priority], [Destination], [Amount], [Price], [SubOrderSum], [SubOrderCreditSum], SubOrderSaldo, ProductListId)
    VALUES
        (@OrderId, @ProductId, @Weight, @Priority, @Destination, @Amount, @Price, @SubOrderSum, @SubOrderCreditSum, @SubOrderSaldo, @ProductListId)
    SELECT SCOPE_IDENTITY()
END



/****** Object:  Table [dbo].[Cards]    Script Date: 08/11/2014 10:37:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Cards]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
    DROP TABLE [dbo].[Cards]


CREATE TABLE [dbo].[Cards](
	[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[Created] [datetime] NOT NULL,
	[LastUsed] [datetime] NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[EncryptedKey] [nvarchar](1024) NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Cards] ADD  CONSTRAINT [DF_Cards_Id]  DEFAULT (newid()) FOR [Id]
GO


--
-- Dropping stored procedure CardsSelectRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CardsSelectRow]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[CardsSelectRow]
  
GO
-- ==========================================================================================
-- Entity Name: CardsSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 8/4/2014 10:40:02 AM
-- Description: This SP select a specify row from Cards
-- ==========================================================================================


Create Procedure CardsSelectRow
@Id uniqueidentifier

AS
BEGIN

SELECT * FROM Cards WHERE Id=@Id

END

GO




--
-- Dropping stored procedure CardsSelectRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CardsSelectRowFromOrderId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[CardsSelectRowFromOrderId]
  
GO
-- ==========================================================================================
-- Entity Name: CardsSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 8/4/2014 10:40:02 AM
-- Description: This SP select a specify row from Cards
-- ==========================================================================================


Create Procedure CardsSelectRowFromOrderId
@OrderId bigint

AS
BEGIN

SELECT * FROM Cards WHERE OrderId=@OrderId

END

GO











--
-- Dropping stored procedure CardsInsertRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CardsInsertRow]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[CardsInsertRow]
  
GO
-- ==========================================================================================
-- Entity Name: CardsInsertRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 8/4/2014 10:40:02 AM
-- Description: This SP Inserts value to Cards table
-- ==========================================================================================


Create Procedure CardsInsertRow
@Id uniqueidentifier,
@OrderId bigint,
@Email nvarchar(100),
@EncryptedKey nvarchar(1024),
@Created datetime,
@LastUsed datetime
AS
BEGIN
    INSERT INTO Cards
        ([Id], [OrderId], [Created], [LastUsed], [Email], [EncryptedKey])
    VALUES
        (@Id, @OrderId, @Created, @LastUsed, @Email, @EncryptedKey)
	SELECT @Id
END

GO


--
-- Dropping stored procedure CardsUpdateRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CardsUpdateRow]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[CardsUpdateRow]
  
GO
-- ==========================================================================================
-- Entity Name: CardsUpdateRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 8/4/2014 10:40:02 AM
-- Description: This SP updates Cards table rows.
-- ==========================================================================================


Create Procedure CardsUpdateRow
@Id uniqueidentifier,
@Email nvarchar(100),
@EncryptedKey nvarchar(1024),
@LastUsed datetime
AS
BEGIN

UPDATE Cards SET [Email] = @Email, 
				 [EncryptedKey] = @EncryptedKey,
				 [LastUsed] = @LastUsed
WHERE Id=@Id

END

GO


--
-- Dropping stored procedure CardsDeleteRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CardsDeleteRow]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[CardsDeleteRow]
  
GO
-- ==========================================================================================
-- Entity Name: CardsDeleteRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 8/4/2014 10:40:02 AM
-- Description: This SP delete specify row from Cards table
-- ==========================================================================================


Create Procedure CardsDeleteRow
@Id uniqueidentifier
AS
BEGIN

DELETE Cards WHERE Id=@Id

END

GO


Grant EXECUTE On [CardsSelectRow] To [DigitalPostageTest]
Grant EXECUTE On [CardsInsertRow] To [DigitalPostageTest]
Grant EXECUTE On [CardsUpdateRow] To [DigitalPostageTest]
Grant EXECUTE On [CardsDeleteRow] To [DigitalPostageTest]
Grant EXECUTE On [CardsSelectRowFromOrderId] To [DigitalPostageTest]












-- VERSION 2.0.5


/****** Object:  Table [dbo].[AppTypes]    Script Date: 09/01/2014 14:02:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AppTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Platform] [nvarchar](50) NOT NULL,
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_AppTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET IDENTITY_INSERT [dbo].[AppTypes] ON
GO
INSERT INTO [dbo].[AppTypes] ([Id],[Platform], [Version])
VALUES (1, 'WebShop',1);
GO
INSERT INTO [dbo].[AppTypes] ([Id],[Platform], [Version])
VALUES (2, 'WebShop',2);
GO
SET IDENTITY_INSERT [dbo].[AppTypes] OFF
GO


--
-- Dropping stored procedure AppTypesSelectRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AppTypesSelectRow]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[AppTypesSelectRow]
  
GO
-- ==========================================================================================
-- Entity Name: AppTypesSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 9/1/2014 2:20:31 PM
-- Description: This SP select a specify row from AppTypes
-- ==========================================================================================


Create Procedure AppTypesSelectRow
@Platform nvarchar(50),
@Version int AS
BEGIN

SELECT * FROM AppTypes WHERE [Platform]=@PlatForm AND [Version] = @Version

END

GO


--
-- Dropping stored procedure AppTypesInsertRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AppTypesInsertRow]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[AppTypesInsertRow]
  
GO
-- ==========================================================================================
-- Entity Name: AppTypesInsertRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 9/1/2014 2:20:31 PM
-- Description: This SP Inserts value to AppTypes table
-- ==========================================================================================


Create Procedure AppTypesInsertRow
@Platform nvarchar(50),
@Version int
AS
BEGIN

    INSERT INTO AppTypes
        ([Platform], [Version])
    VALUES
        (@Platform, @Version)
    SELECT SCOPE_IDENTITY()
END

GO


--
-- Dropping stored procedure AppTypesSelectRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AppTypesSelectById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[AppTypesSelectById]
  
GO
-- ==========================================================================================
-- Entity Name: AppTypesSelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 9/1/2014 2:20:31 PM
-- Description: This SP select a specify row from AppTypes
-- ==========================================================================================


Create Procedure AppTypesSelectById
@Id int
AS
BEGIN

SELECT * FROM AppTypes WHERE [Id]=@Id

END

GO


Grant EXECUTE On [AppTypesSelectRow] To [DigitalPostageTest]
Grant EXECUTE On [AppTypesInsertRow] To [DigitalPostageTest]
Grant EXECUTE On [AppTypesSelectById] To [DigitalPostageTest]


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Orders ADD
	PaymentMethod int NULL,
	AppType int NULL
GO
ALTER TABLE dbo.Orders ADD CONSTRAINT
	DF_Orders_PaymentMethod DEFAULT 0 FOR PaymentMethod
GO
ALTER TABLE dbo.Orders ADD CONSTRAINT
	DF_Orders_AppType DEFAULT 1 FOR AppType
GO
ALTER TABLE dbo.Orders SET (LOCK_ESCALATION = TABLE)
GO

UPDATE dbo.Orders
SET PaymentMethod=1, AppType=1;
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[OrdersSetAppInfo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[AppTypesSelectById]
  
GO

Create Procedure OrdersSetAppInfo
@Id int,
@PaymentMethod int,
@AppType int
AS
BEGIN


Grant EXECUTE On OrdersSetAppInfo To [DigitalPostageTest]

GO
DROP VIEW [dbo].[ViewOrdersSubOrdersCodes]

GO
CREATE VIEW [dbo].[ViewOrdersSubOrdersCodes]
AS
SELECT     dbo.Orders.Id, dbo.Orders.Email, dbo.Orders.ReceiptNumber, dbo.Orders.CaptureId, dbo.Orders.LabelImage, dbo.Orders.ImageSize, dbo.Orders.SalesDate, 
                      dbo.Orders.ExpireDate, dbo.Orders.OrderSum, dbo.Orders.AppType, dbo.Orders.PaymentMethod, dbo.SubOrders.Id AS SubOrdersId, dbo.SubOrders.ProductId, dbo.SubOrders.Weight, dbo.SubOrders.Priority, 
                      dbo.SubOrders.Destination, dbo.SubOrders.Amount, dbo.SubOrders.Price, dbo.SubOrders.SubOrderSum, dbo.Codes.Id AS CodesId, dbo.Codes.PostCode, 
                      dbo.Codes.CodeId, dbo.Codes.Status, dbo.Codes.UsedDate, dbo.Codes.RefundedDate, dbo.Codes.RefundId, dbo.Orders.CreditSum, dbo.Orders.Saldo, 
                      dbo.Orders.OrderStatus, dbo.SubOrders.SubOrderCreditSum, dbo.SubOrders.SubOrderSaldo
FROM         dbo.SubOrders INNER JOIN
                      dbo.Orders ON dbo.SubOrders.OrderId = dbo.Orders.Id INNER JOIN
                      dbo.Codes ON dbo.SubOrders.Id = dbo.Codes.SubOrderId

GO



CREATE Procedure [dbo].[ReportingViewByCustomersOnlinePorto]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT  SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
		SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
		SUM(dbo.SubOrders.SubOrderSaldo) AS Saldo, 
		SUM(dbo.SubOrders.Amount) AS Amount, 
		dbo.Orders.Email
FROM    dbo.Orders INNER JOIN dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2' AND dbo.Orders.AppType <= 2           

GROUP BY dbo.Orders.Email
ORDER BY OrderSum DESC

END
GO


CREATE Procedure [dbo].[ReportingViewByCustomersMobilePorto]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT  SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
		SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
		SUM(dbo.SubOrders.SubOrderSaldo) AS Saldo, 
		SUM(dbo.SubOrders.Amount) AS Amount, 
		dbo.Orders.Email
FROM    dbo.Orders INNER JOIN dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2' AND dbo.Orders.AppType > 2                   

GROUP BY dbo.Orders.Email
ORDER BY OrderSum DESC

END
GO

Grant EXECUTE On [ReportingViewByCustomersOnlinePorto] To [DigitalPostageTest]

Grant EXECUTE On [ReportingViewByCustomersMobilePorto] To [DigitalPostageTest]


/****** Object:  StoredProcedure [dbo].[ReportingViewReimbursement]    Script Date: 09/04/2014 12:47:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[ReportingViewReimbursement]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     TOP (100) PERCENT dbo.Refunds.OrderNr, dbo.Refunds.Timestamp, dbo.Orders.Email, dbo.Orders.SalesDate, dbo.Orders.ExpireDate, dbo.Orders.OrderSum, 
                      dbo.Refunds.CreditSum, dbo.Orders.Saldo, dbo.Refunds.Name, dbo.Refunds.Address, dbo.Refunds.PostNumber, dbo.Refunds.City, dbo.Refunds.Phone, 
                      dbo.Refunds.Message, dbo.Refunds.Employee, dbo.AppTypes.Platform, dbo.AppTypes.Version
FROM         dbo.Refunds INNER JOIN
                      dbo.Orders ON dbo.Refunds.OrderNr = dbo.Orders.Id INNER JOIN
                      dbo.AppTypes ON dbo.Orders.AppType = dbo.AppTypes.Id
WHERE     (dbo.Refunds.Timestamp >= @FromDate) AND (dbo.Refunds.Timestamp <= @ToDate) AND (dbo.Orders.OrderStatus = '2')
ORDER BY dbo.Refunds.CreditSum DESC

END



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE ReportingViewSalesChannels
@FromDate datetime,
@ToDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT      dbo.AppTypes.Platform, 
			SUM(dbo.SubOrders.Amount) AS Amount, 
			SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
			SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
			SUM(dbo.SubOrders.SubOrderSaldo) AS Saldo
FROM        dbo.Orders INNER JOIN
            dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
            dbo.AppTypes ON dbo.Orders.AppType = dbo.AppTypes.Id
WHERE dbo.Orders.SalesDate BETWEEN @FromDate AND @ToDate
GROUP BY dbo.Orders.OrderStatus, dbo.AppTypes.Platform
HAVING      (dbo.Orders.OrderStatus = 2)

END
GO

Grant EXECUTE On [ReportingViewSalesChannels] To [DigitalPostageTest]

GO

/*

Version 2.0.7

*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.SubOrders ADD
	SapId int NULL
GO
ALTER TABLE dbo.SubOrders SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

GO


ALTER Procedure [dbo].[SubOrdersInsertRow]
@OrderId bigint,
@ProductId bigint,
@Weight smallint,
@Priority nvarchar(50),
@Destination nvarchar(50),
@Amount int,
@Price float,
@SubOrderSum float,
@SubOrderCreditSum float,
@SubOrderSaldo float,
@ProductListId int,
@SapId int
AS
BEGIN

    INSERT INTO SubOrders
        ([OrderId], [ProductId], [Weight], [Priority], [Destination], [Amount], [Price], [SubOrderSum], [SubOrderCreditSum], SubOrderSaldo, [ProductListId], SapId)
    VALUES
        (@OrderId, @ProductId, @Weight, @Priority, @Destination, @Amount, @Price, @SubOrderSum, @SubOrderCreditSum, @SubOrderSaldo, @ProductListId, @SapId)
    SELECT SCOPE_IDENTITY()
END

GO

ALTER Procedure [dbo].[SubOrdersUpdateRow]
@Id bigint,
@OrderId bigint,
@ProductId bigint,
@Weight smallint,
@Priority nvarchar(50),
@Destination nvarchar(50),
@Amount int,
@Price float,
@SubOrderSum float,
@SubOrderCreditSum float,
@SubOrderSaldo float,
@ProductListId int,
@SapId int
AS
BEGIN

UPDATE SubOrders SET 
	[OrderId] = @OrderId, 
	[ProductId] = @ProductId, 
	[Weight] = @Weight, 
	[Priority] = @Priority, 
	[Destination] = @Destination, 
	[Amount] = @Amount, 
	[Price] = @Price, 
	[SubOrderSum] = @SubOrderSum, 
	[SubOrderCreditSum] = @SubOrderCreditSum, 
	[SubOrderSaldo] = @SubOrderSaldo, 
	[ProductListId] = @ProductListId, 
	[SapId] = @SapId 
	
	WHERE [Id] = @Id

END

GO


/*

Version 2.1.0

*/


GO
/****** Object:  StoredProcedure [dbo].[AppTypesSelectAll]    Script Date: 10/08/2014 11:35:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: AppTypesSelectAll
-- Author:  Mikael Lindberg Mortensen
-- Create date: 9/1/2014 2:20:31 PM
-- Description: This SP select all rows from AppTypes
-- ==========================================================================================


CREATE Procedure [dbo].[AppTypesSelectAll]
AS
BEGIN

SELECT * FROM AppTypes 
ORDER BY [Platform] ASC

END

Grant EXECUTE On [AppTypesSelectAll] To [DigitalPostageTest]


/*
VERSION 2.1.1
*/

CREATE VIEW [dbo].[OrderView]
AS
SELECT     dbo.Orders.*, dbo.AppTypes.Platform, dbo.AppTypes.Version
FROM         dbo.Orders INNER JOIN
                      dbo.AppTypes ON dbo.Orders.AppType = dbo.AppTypes.Id

GO


ALTER Procedure [dbo].[OrdersSelectDate]
@FromDate datetime,
@ToDate datetime
AS
BEGIN

SELECT * FROM OrderView WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate ORDER BY SalesDate ASC

END

GO

ALTER Procedure [dbo].[OrdersSelectCaptureId]
@CaptureId nvarchar(50)

AS
BEGIN

SELECT * FROM OrderView WHERE [CaptureId] LIKE @CaptureId ORDER BY SalesDate ASC

END

GO

ALTER Procedure [dbo].[OrdersSelectDateEmail]
@FromDate datetime,
@ToDate datetime,
@Email nvarchar(50)

AS
BEGIN

SELECT * FROM OrderView WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND [Email] LIKE @Email ORDER BY SalesDate ASC

END

GO

ALTER Procedure [dbo].[OrdersSelectRow]
@Id bigint
AS
BEGIN

SELECT * FROM OrderView WHERE [Id]=@Id

END

GO

ALTER VIEW [dbo].[ViewOrdersSubOrdersCodes]
AS
SELECT     dbo.Orders.Id, dbo.Orders.Email, dbo.Orders.ReceiptNumber, dbo.Orders.CaptureId, dbo.Orders.LabelImage, dbo.Orders.ImageSize, dbo.Orders.SalesDate, 
                      dbo.Orders.ExpireDate, dbo.Orders.OrderSum, dbo.Orders.AppType, dbo.Orders.PaymentMethod, dbo.SubOrders.Id AS SubOrdersId, dbo.SubOrders.ProductId, 
                      dbo.SubOrders.Weight, dbo.SubOrders.Priority, dbo.SubOrders.Destination, dbo.SubOrders.Amount, dbo.SubOrders.Price, dbo.SubOrders.SubOrderSum, 
                      dbo.Codes.Id AS CodesId, dbo.Codes.PostCode, dbo.Codes.CodeId, dbo.Codes.Status, dbo.Codes.UsedDate, dbo.Codes.RefundedDate, dbo.Codes.RefundId, 
                      dbo.Orders.CreditSum, dbo.Orders.Saldo, dbo.Orders.OrderStatus, dbo.SubOrders.SubOrderCreditSum, dbo.SubOrders.SubOrderSaldo, dbo.AppTypes.Platform, 
                      dbo.AppTypes.Version
FROM         dbo.SubOrders INNER JOIN
                      dbo.Orders ON dbo.SubOrders.OrderId = dbo.Orders.Id INNER JOIN
                      dbo.Codes ON dbo.SubOrders.Id = dbo.Codes.SubOrderId INNER JOIN
                      dbo.AppTypes ON dbo.Orders.AppType = dbo.AppTypes.Id

GO


-- Version 2.2.1 
-- Add Queueing
-- http://rusanu.com/2010/03/26/using-tables-as-queues/
--

/****** Object:  Table [dbo].[OrderQueue]    Script Date: 12/07/2014 08:15:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[OrderQueue]
GO

CREATE TABLE [dbo].[OrderQueue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[RefundId] [bigint] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_OrderQueue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OrderQueue]  WITH CHECK ADD  CONSTRAINT [FK_OrderQueue_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO

ALTER TABLE [dbo].[OrderQueue] CHECK CONSTRAINT [FK_OrderQueue_Orders]
GO



--
-- Dropping stored procedure OrderQueueSelectAll : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[OrderQueueSelectAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[OrderQueueSelectAll]
  
GO

-- ==========================================================================================
-- Entity Name: OrderQueueSelectAll
-- Author:  Mikael Lindberg Mortensen
-- Create date: 12/5/2014 3:24:39 PM
-- Description: Select all rows form OrderQueue
-- ==========================================================================================



Create Procedure OrderQueueSelectAll
AS
BEGIN

SELECT * FROM OrderQueue
ORDER BY Timestamp DESC

END

GO


--
-- Dropping stored procedure OrderQueueDeQueue : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[OrderQueueDeQueue]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[OrderQueueDeQueue]
  
GO
-- ==========================================================================================
-- Entity Name: OrderQueueDeQueue
-- Author:  Mikael Lindberg Mortensen
-- Create date: 12/5/2014 3:24:39 PM
-- Description: This SP select a specify row from OrderQueue
-- ==========================================================================================


Create Procedure OrderQueueDeQueue 
AS
BEGIN
	SET NOCOUNT ON;
  WITH cte AS (
    SELECT TOP(1) Id, OrderId, RefundId, Timestamp
      FROM OrderQueue WITH (ROWLOCK, READPAST)
    ORDER BY Id)
  DELETE FROM cte
    OUTPUT deleted.Id, deleted.OrderId, deleted.RefundId, deleted.Timestamp;

END


GO


--
-- Dropping stored procedure OrderQueueAddQueue : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[OrderQueueAddQueue]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[OrderQueueAddQueue]
  
GO
-- ==========================================================================================
-- Entity Name: OrderQueueAddQueue
-- Author:  Mikael Lindberg Mortensen
-- Create date: 12/5/2014 3:24:39 PM
-- Description: This SP Inserts value to OrderQueue table
-- ==========================================================================================


Create Procedure OrderQueueAddQueue
@OrderId bigint,
@RefundId bigint,
@Timestamp datetime
AS
BEGIN
	SET NOCOUNT ON;
    INSERT INTO OrderQueue
        ([OrderId], [RefundId], [Timestamp])
    VALUES
        (@OrderId, @RefundId, @Timestamp)
END

GO


Grant EXECUTE On [OrderQueueSelectAll] To [DigitalPostageTest]
Grant EXECUTE On [OrderQueueDeQueue] To [DigitalPostageTest]
Grant EXECUTE On [OrderQueueAddQueue] To [DigitalPostageTest]



BEGIN TRANSACTION
GO
ALTER TABLE dbo.Orders ADD
	SentToCint smallint NULL
GO
ALTER TABLE dbo.Orders SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.Refunds ADD
	SentToCint smallint NULL
GO
ALTER TABLE dbo.Refunds SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[OrdersSetCintStatus]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[OrdersSetCintStatus]
  
GO

CREATE Procedure [dbo].[OrdersSetCintStatus]
@Id int,
@CintUpdated smallint
AS
BEGIN

UPDATE dbo.Orders
	SET [SentToCint]=@CintUpdated
	Where Id = @Id
END
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[RefundsSetCintStatus]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[RefundsSetCintStatus]
  
GO

CREATE Procedure [dbo].[RefundsSetCintStatus]
@Id int,
@CintUpdated smallint
AS
BEGIN

UPDATE dbo.Refunds
	SET [SentToCint]=@CintUpdated
	Where Id = @Id
END
GO


Grant EXECUTE On [OrdersSetCintStatus] To [DigitalPostageTest]
Grant EXECUTE On [RefundsSetCintStatus] To [DigitalPostageTest]

GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CodesDeleteRowsForSubOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[CodesDeleteRowsForSubOrder]

GO

Create Procedure [dbo].[CodesDeleteRowsForSubOrder]
@SubOrderId bigint
AS
BEGIN
	DELETE FROM dbo.Codes WHERE [SubOrderId] = @SubOrderId
END
GO

Grant EXECUTE On [CodesDeleteRowsForSubOrder] To [DigitalPostageTest]

GO


IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[OrderView]'))
DROP VIEW [dbo].[OrderView]
GO

CREATE VIEW [dbo].[OrderView]
AS
SELECT     dbo.Orders.Id, dbo.Orders.Email, dbo.Orders.ReceiptNumber, dbo.Orders.CaptureId, dbo.Orders.LabelImage, dbo.Orders.ImageSize, dbo.Orders.SalesDate, 
                      dbo.Orders.ExpireDate, dbo.Orders.OrderSum, dbo.Orders.CreditSum, dbo.Orders.Saldo, dbo.Orders.OrderStatus, dbo.Orders.PaymentMethod, dbo.Orders.AppType, 
                      dbo.Orders.SentToCint, dbo.AppTypes.Version, dbo.AppTypes.Platform
FROM         dbo.Orders INNER JOIN
                      dbo.AppTypes ON dbo.Orders.AppType = dbo.AppTypes.Id

GO
