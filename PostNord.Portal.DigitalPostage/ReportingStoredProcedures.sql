﻿/****** Object:  StoredProcedure [dbo].[ReportingViewReportGroupedOnSalesDate]    Script Date: 02/07/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ReportingViewReportGroupedOnSalesDate
-- Author:  Mikael Lindberg Mortensen
-- Create date: 2/7/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from ViewReportGroupedOnSalesDate
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewReportGroupedOnSalesDate]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120) AS OrderDate, dbo.Orders.SalesDate, dbo.Products.Name AS ProcuctName, 
                      SUM(dbo.SubOrders.Amount) AS SubAmount, SUM(dbo.SubOrders.SubOrderSum) AS SubSum, 
                      SUM(dbo.SubOrders.SubOrderCreditSum) AS SubCredit, SUM(dbo.SubOrders.SubOrderSaldo) AS SubSaldo
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2'                  
GROUP BY dbo.Products.Name, CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120), dbo.Orders.SalesDate
ORDER BY dbo.Orders.SalesDate, ProcuctName
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ReportingViewReportSum
-- Author:  Mikael Lindberg Mortensen
-- Create date: 2/7/2014 7:11:27 AM
-- Description: This SP sums up all orders in period
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewReportSum]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT  SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
		SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
		SUM(dbo.SubOrders.SubOrderSaldo) AS SaldoSum, 
		SUM(dbo.SubOrders.Amount) AS AmountSum
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2'                     

END

GO




/****** Object:  StoredProcedure [dbo].[ReportingViewReportGroupedOnSalesDate]    Script Date: 02/07/2014 14:49:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ReportingViewByCustomers
-- Author:  Mikael Lindberg Mortensen
-- Create date: 2/7/2014 7:11:27 AM
-- Description: This SP selects Creates a summary report grouped on customer email
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewByCustomers]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT  SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
		SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
		SUM(dbo.SubOrders.SubOrderSaldo) AS Saldo, 
		SUM(dbo.SubOrders.Amount) AS Amount, 
		dbo.Orders.Email
FROM    dbo.Orders INNER JOIN dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2'                     

GROUP BY dbo.Orders.Email
ORDER BY OrderSum DESC                    

END

GO




GO
/****** Object:  StoredProcedure [dbo].[ReportingViewByCustomers]    Script Date: 02/13/2014 12:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name: ReportingViewReimbursement
-- Author:  Mikael Lindberg Mortensen
-- Create date: 2/7/2014 7:11:27 AM
-- Description: This SP selects Creates a summary for reimbursement in the given period
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewReimbursement]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     TOP (100) PERCENT dbo.Refunds.OrderNr, dbo.Refunds.Timestamp, dbo.Orders.Email, dbo.Orders.SalesDate, dbo.Orders.ExpireDate, dbo.Orders.OrderSum, 
                      dbo.Refunds.CreditSum, dbo.Orders.Saldo, dbo.Refunds.Name, dbo.Refunds.Address, dbo.Refunds.PostNumber, dbo.Refunds.City, dbo.Refunds.Phone, 
                      dbo.Refunds.Message
FROM         dbo.Refunds INNER JOIN
                      dbo.Orders ON dbo.Refunds.OrderNr = dbo.Orders.Id
WHERE dbo.Refunds.Timestamp>=@FromDate AND dbo.Refunds.Timestamp<=@ToDate AND dbo.Orders.OrderStatus = '2'

ORDER BY dbo.Refunds.CreditSum DESC

END




GO
/****** Object:  StoredProcedure [dbo].[ReportingViewReportSalePerProduct]    Script Date: 02/14/2014 09:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ReportingViewReportSalePerProduct
-- Author:  Mikael Lindberg Mortensen
-- Create date: 2/7/2014 7:11:27 AM
-- Description: This SP selects all Orders and suborders grouped on Product
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewReportSalePerProduct]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     dbo.SubOrders.Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority, dbo.SubOrders.Price, SUM(dbo.SubOrders.Amount) AS SubAmount, 
                      SUM(dbo.SubOrders.SubOrderSum) AS SubSum, SUM(dbo.SubOrders.SubOrderCreditSum) AS SubCredit, SUM(dbo.SubOrders.SubOrderSaldo) AS SubSaldo
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND dbo.Orders.OrderStatus = '2'  
GROUP BY dbo.SubOrders.Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority, dbo.SubOrders.Price
ORDER BY dbo.SubOrders.Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority

END





/****** Object:  Table [dbo].[ValidationStatus]    Script Date: 02/28/2014 11:48:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ValidationStatus](
	[Id] [int] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ValidationStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO




INSERT INTO [dbo].[ValidationStatus] VALUES (1,'OK');
INSERT INTO [dbo].[ValidationStatus] VALUES (2,'Used');
INSERT INTO [dbo].[ValidationStatus] VALUES (4,'Expired');
INSERT INTO [dbo].[ValidationStatus] VALUES (8,'Disabled');
INSERT INTO [dbo].[ValidationStatus] VALUES (16,'Refunded');
INSERT INTO [dbo].[ValidationStatus] VALUES (32,'Does not exist');

GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ValidationStatus SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ValidationResults ADD CONSTRAINT
	FK_ValidationResults_ValidationStat FOREIGN KEY
	(
	ErrorCode
	) REFERENCES dbo.ValidationStatus
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ValidationResults SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



/****** Object:  View [dbo].[ViewValidationResults]    Script Date: 02/14/2014 12:49:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ViewValidationResults]
AS
SELECT     dbo.ValidationResults.CodeText, 
		   dbo.ValidationResults.Timestamp, 
		   dbo.ValidationResults.Employee, 
		   dbo.ValidationStatus.Status, 
           dbo.ValidationResults.ErrorCode
FROM       dbo.ValidationResults INNER JOIN
           dbo.ValidationStatus ON dbo.ValidationResults.ErrorCode = dbo.ValidationStatus.Id
GO


/****** Object:  StoredProcedure [dbo].[ReportingViewByCustomers]    Script Date: 02/14/2014 12:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ReportingViewValidationResults]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT [CodeText]
      ,[Timestamp]
      ,[Employee]
      ,[Status]
FROM [dbo].[ViewValidationResults]
WHERE	Timestamp>=@FromDate AND Timestamp<=@ToDate 

END




GO
/****** Object:  StoredProcedure [dbo].[ReportingViewValidationResultSums]    Script Date: 02/14/2014 12:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ReportingViewValidationResultSums]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     Status, COUNT(ErrorCode) AS ErrorCount
FROM       dbo.ViewValidationResults
WHERE Timestamp>=@FromDate AND Timestamp<=@ToDate 
GROUP BY ErrorCode, Status
ORDER BY ErrorCode

END





/****** Object:  StoredProcedure [dbo].[ValidationResultsSelectDateAndCode]    Script Date: 02/28/2014 14:42:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name: ValidationResultsSelectDateAndCode
-- Author:  Mikael Lindberg Mortensen
-- Create date: 1/3/2014 7:11:27 AM
-- Description: Select ValidationResults from daterange and CodeId
-- ==========================================================================================

CREATE Procedure [dbo].[ValidationResultsSelectDateAndCode]
@FromDate datetime,
@ToDate datetime,
@CodeId bigint
AS
BEGIN

SELECT * FROM ValidationResults WHERE [CodeId]=@CodeId AND [Timestamp]>=@FromDate AND [Timestamp]<=@ToDate ORDER BY Timestamp ASC

END

GO


/**** REPORT CHANGE ****/
USE [DigitalPostage]
GO
/****** Object:  StoredProcedure [dbo].[ReportingViewReportSalePerProduct]    Script Date: 03/07/2014 06:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: ReportingViewReportSalePerProductReimbursement
-- Author:  Mikael Lindberg Mortensen
-- Create date: 3/7/2014 7:11:27 AM
-- Description: This SP selects all rows in given date range from Reimbursements
-- ==========================================================================================
CREATE Procedure [dbo].[ReportingViewReportSalePerProductReimbursement]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     TOP (100) PERCENT dbo.Products.Name AS Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority, COUNT(dbo.Refunds.Id) AS SubAmount, 
                      SUM(dbo.SubOrders.Price) AS SubSum, dbo.SubOrders.Price, dbo.SubOrders.ProductId
FROM         dbo.Refunds INNER JOIN
                      dbo.Codes ON dbo.Refunds.Id = dbo.Codes.RefundId INNER JOIN
                      dbo.SubOrders ON dbo.Codes.SubOrderId = dbo.SubOrders.Id INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
WHERE dbo.Codes.RefundedDate>=@FromDate AND dbo.Codes.RefundedDate<=@ToDate 
GROUP BY dbo.SubOrders.Weight, dbo.SubOrders.Priority, dbo.Products.Name, dbo.SubOrders.Price, dbo.Products.Id, dbo.SubOrders.ProductId
ORDER BY Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority

END
 



GO
/****** Object:  StoredProcedure [dbo].[ReportingViewReimbursementsPerCustomers]    Script Date: 03/14/2014 10:44:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ReportingViewReimbursementsPerCustomers]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT COUNT(dbo.Codes.Id) AS Amount, SUM(dbo.Codes.Price) AS CreditSum, dbo.Orders.Email
FROM         dbo.Refunds INNER JOIN 
				dbo.Orders ON dbo.Refunds.OrderNr = dbo.Orders.Id INNER JOIN
                      dbo.Codes ON dbo.Refunds.Id = dbo.Codes.RefundId

WHERE dbo.Refunds.Timestamp >=@FromDate AND dbo.Refunds.Timestamp <=@ToDate
GROUP BY dbo.Orders.Email
ORDER BY CreditSum DESC

END
