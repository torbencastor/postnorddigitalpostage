﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PN.SoapLibrary.WSSecurity
{
    public class PnConfig
    {
        #region Private Attributes

        string _clientSignaturePath; // = @"C:\Innofactor\MobilePay\6K1280_sign.pfx";
        string _clientSignaturePassword; // = "MobilePay";
        string _clientContentEncryptionPath; // = @"C:\Innofactor\MobilePay\6K1280_crypt.pfx";
        string _clientContentEncryptionPassword; // = "MobilePay";
        string _dbSignaturePath; // = @"C:\Innofactor\MobilePay\DBGSIGN.crt";
        string _dbContentEncryptionPath; // = @"C:\Innofactor\MobilePay\DBGCRYPT.crt";
        
        #endregion

        #region Public Properties
        
        public string ClientSignaturePath
        {
            get { return _clientSignaturePath; }
            set { _clientSignaturePath = value; }
        }
        
        public string ClientSignaturePassword
        {
            get { return _clientSignaturePassword; }
            set { _clientSignaturePassword = value; }
        }
        
        public string ClientContentEncryptionPath
        {
            get { return _clientContentEncryptionPath; }
            set { _clientContentEncryptionPath = value; }
        }
        
        public string ClientContentEncryptionPassword
        {
            get { return _clientContentEncryptionPassword; }
            set { _clientContentEncryptionPassword = value; }
        }
        
        public string DbSignaturePath
        {
            get { return _dbSignaturePath; }
            set { _dbSignaturePath = value; }
        }
        
        public string DbContentEncryptionPath
        {
            get { return _dbContentEncryptionPath; }
            set { _dbContentEncryptionPath = value; }
        }
        
        #endregion
    }
}
