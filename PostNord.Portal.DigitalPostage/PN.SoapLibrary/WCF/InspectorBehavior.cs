﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Security.Cryptography.Xml;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml;
using System.ServiceModel.Channels;
using System.Text;
using PN.SoapLibrary.WSSecurity;
using PN.SoapLibrary.Xml;

namespace PN.SoapLibrary.WCF
{
    public class InspectorBehavior : IEndpointBehavior
    {

        private readonly ClientMessageInspector clientMessageInspector;
        private readonly ParameterInspector parameterInspector;

        public string LastRequestXML
        {
            get
            {
                return clientMessageInspector.LastRequestXML;
            }
        }

        public string LastResponseXML
        {
            get
            {
                return clientMessageInspector.LastResponseXML;
            }
        }

        public InspectorBehavior(PnConfig configuation)
        {
            EncryptXml eXml = new EncryptXml(configuation);
            SignXml sXml = new SignXml(configuation);

            clientMessageInspector = new ClientMessageInspector(eXml, sXml);
            parameterInspector = new ParameterInspector();
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            var wrapped = clientRuntime.OperationSelector;
            clientRuntime.OperationSelector = new ClientOperationSelector(wrapped);

            clientRuntime.MessageInspectors.Add(clientMessageInspector);
            foreach (ClientOperation op in clientRuntime.Operations)
                op.ParameterInspectors.Add(parameterInspector);
        }
    }

    public class ClientOperationSelector : IClientOperationSelector
    {
        private readonly IClientOperationSelector wrapped;

        public ClientOperationSelector(IClientOperationSelector wrapped)
        {
            this.wrapped = wrapped;
        }

        public string SelectOperation(MethodBase method, object[] parameters)
        {
            return wrapped.SelectOperation(method, parameters);
        }

        public bool AreParametersRequiredForSelection
        {
            get { return wrapped.AreParametersRequiredForSelection; }
        }
    }

    public class ClientMessageInspector : IClientMessageInspector
    {
        private readonly IEncryptXml _encryptXml;
        private readonly ISignXml _signXml;
        //private readonly SoapEnvelopesSection _soapEnvelopeConfiguration;

        public string LastRequestXML { get; private set; }
        public string LastResponseXML { get; private set; }

        public ClientMessageInspector(IEncryptXml encryptXml,ISignXml signXml)
        {
            _encryptXml = encryptXml;
            _signXml = signXml;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            // Get xml without pretty printing and preserving whitespaces
            MemoryStream ms = new MemoryStream();
            XmlWriter writer = XmlWriter.Create(ms);
            reply.WriteMessage(writer);
            writer.Flush();
            ms.Position = 0;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Load(ms);

            // Decrypt message
            if (_encryptXml.DecryptBody(xmlDoc))
                // Validate message signature
                _signXml.ValidateSignature(xmlDoc);

            var reader = XmlReader.Create(new StringReader(xmlDoc.OuterXml));

            // Create deserialized message to return to client.
            reply = Message.CreateMessage(reader, int.MaxValue, reply.Version);
        }
        
        public object BeforeSendRequest(ref Message request, System.ServiceModel.IClientChannel channel)
        {
            LastRequestXML = request.ToString();

            XmlDocument xmlDoc = new XmlDocument();       
            xmlDoc.LoadXml(LastRequestXML);
            
            XmlHelpers.AddNamespacesToEnvelope(xmlDoc);
            XmlHelpers.AddNamespaceToBody(xmlDoc);
            XmlHelpers.AddTimestamp(xmlDoc);
            string id1 = XmlHelpers.AddIDToElement(xmlDoc, "Body", "http://www.w3.org/2003/05/soap-envelope", "DB-1");
            XmlHelpers.ReplaceIdWithNamespaceId(xmlDoc);
            _signXml.AddSignature(xmlDoc, "#" + id1);

            _encryptXml.EncryptBody(xmlDoc);

            string id2 = XmlHelpers.AddIDToElement(xmlDoc, "EncryptedData", "http://www.w3.org/2001/04/xmlenc#", "DB-2");
            _signXml.AddSignature(xmlDoc, "#" + id2);

            using (StringWriter stringWriter = new StringWriter())
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    xmlDoc.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    var result = stringWriter.GetStringBuilder().ToString();

                    request = ChangeString(request, result);
                    return request;
                }
        }

        public Message ChangeString(Message oldMessage, string newBody)
        {
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(newBody));
            XmlDictionaryReader xdr = XmlDictionaryReader.CreateTextReader(ms, new XmlDictionaryReaderQuotas());
            Message newMessage = Message.CreateMessage(xdr, int.MaxValue, oldMessage.Version);
            newMessage.Properties.CopyProperties(oldMessage.Properties);
            return newMessage;
        }
    }

    public class ParameterInspector : IParameterInspector
    {
        public object BeforeCall(string operationName, object[] inputs)
        {
            return operationName;
        }

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
        }
    }
}
