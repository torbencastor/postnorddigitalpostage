﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.ServiceModel.Security.Tokens;

namespace PN.SoapLibrary.WCF
{
    public class DanskeBankBinding
    {
        /// <summary>
        /// A basic WCF custom binding implementing SOAP 1.2 encoding over HTTP.
        /// </summary>
        public static Binding CreateCustomBinding(Protocol protocol)
        {
            HttpTransportBindingElement transportBinding = null;
            switch (protocol)
            {
                case Protocol.Http:
                    transportBinding = new HttpTransportBindingElement();
                    break;
                case Protocol.Https:
                    transportBinding = new HttpsTransportBindingElement();
                    break;
            }

            // Create an empty BindingElementCollection to populate, a custom binding will be created from it
            BindingElementCollection outputBec = new BindingElementCollection
            {
                new TextMessageEncodingBindingElement {MessageVersion = MessageVersion.Soap12},
                transportBinding
            };

            return new CustomBinding(outputBec);
        }

        [Obsolete("Warning: do not use WCF based implementation of wssecurity as it is not capable of handling wssecurity requirements of Danske Bank SOAP services")]
        private static SecurityBindingElement CreateCertificateBasedSignatureAndEncryption()
        {
            var version =
                MessageSecurityVersion.WSSecurity11WSTrust13WSSecureConversation13WSSecurityPolicy12BasicSecurityProfile10;
            var sec = (SymmetricSecurityBindingElement)SecurityBindingElement.CreateMutualCertificateBindingElement(version);
            sec.EndpointSupportingTokenParameters.Signed.Add(new X509SecurityTokenParameters());
            sec.MessageSecurityVersion = version;
            sec.IncludeTimestamp = true;
            sec.MessageProtectionOrder = MessageProtectionOrder.SignBeforeEncrypt;

            return sec;
        }
    }

    public enum Protocol
    {
        Http,
        Https
    }
}
