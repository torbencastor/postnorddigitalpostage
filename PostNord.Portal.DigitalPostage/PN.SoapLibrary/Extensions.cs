﻿
using System;

namespace PN.SoapLibrary
{
    public static class Extensions
    {
        public static string DBFormat(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
    }
}
