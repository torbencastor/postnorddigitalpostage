-- ==============================================
--
-- Version 2.4.1 update script
-- Updates Reporting Stored Procedures to include Credited orders (OrderStatus = 3)
--
-- ==============================================


ALTER Procedure [dbo].[ReportingViewByCustomers]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT  SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
		SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
		SUM(dbo.SubOrders.SubOrderSaldo) AS Saldo, 
		SUM(dbo.SubOrders.Amount) AS Amount, 
		dbo.Orders.Email
FROM    dbo.Orders INNER JOIN dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND (dbo.Orders.OrderStatus = '2' OR dbo.Orders.OrderStatus = '3')

GROUP BY dbo.Orders.Email
ORDER BY OrderSum DESC

END

GO

ALTER Procedure [dbo].[ReportingViewByCustomersMobilePorto]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT  SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
		SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
		SUM(dbo.SubOrders.SubOrderSaldo) AS Saldo, 
		SUM(dbo.SubOrders.Amount) AS Amount, 
		dbo.Orders.Email
FROM    dbo.Orders INNER JOIN dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND (dbo.Orders.OrderStatus = '2' OR dbo.Orders.OrderStatus = '3') AND dbo.Orders.AppType > 2                     

GROUP BY dbo.Orders.Email
ORDER BY OrderSum DESC

END

GO

ALTER Procedure [dbo].[ReportingViewByCustomersOnlinePorto]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT  SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
		SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
		SUM(dbo.SubOrders.SubOrderSaldo) AS Saldo, 
		SUM(dbo.SubOrders.Amount) AS Amount, 
		dbo.Orders.Email
FROM    dbo.Orders INNER JOIN dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND (dbo.Orders.OrderStatus = '2' OR dbo.Orders.OrderStatus = '3') AND dbo.Orders.AppType <= 2           

GROUP BY dbo.Orders.Email
ORDER BY OrderSum DESC

END
GO


ALTER Procedure [dbo].[ReportingViewReportGroupedOnSalesDate]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120) AS OrderDate, dbo.Orders.SalesDate, dbo.Products.Name AS ProcuctName, 
                      SUM(dbo.SubOrders.Amount) AS SubAmount, SUM(dbo.SubOrders.SubOrderSum) AS SubSum, 
                      SUM(dbo.SubOrders.SubOrderCreditSum) AS SubCredit, SUM(dbo.SubOrders.SubOrderSaldo) AS SubSaldo
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND (dbo.Orders.OrderStatus = '2' OR dbo.Orders.OrderStatus = '3')                   
GROUP BY dbo.Products.Name, CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120), dbo.Orders.SalesDate
ORDER BY dbo.Orders.SalesDate, ProcuctName

END

GO

ALTER Procedure [dbo].[ReportingViewReportSalePerProduct]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     TOP (100) PERCENT dbo.Products.Name AS Destination, dbo.SubOrders.Weight, dbo.SubOrders.Priority, dbo.SubOrders.Price, SUM(dbo.SubOrders.Amount) 
                      AS SubAmount, SUM(dbo.SubOrders.SubOrderSum) AS SubSum, SUM(dbo.SubOrders.SubOrderCreditSum) AS SubCredit, SUM(dbo.SubOrders.SubOrderSaldo) 
                      AS SubSaldo
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND (dbo.Orders.OrderStatus = '2' OR dbo.Orders.OrderStatus = '3') 
GROUP BY dbo.SubOrders.Weight, dbo.SubOrders.Priority, dbo.SubOrders.Price, dbo.Products.Name
ORDER BY dbo.SubOrders.Weight, dbo.SubOrders.Priority

END
 
GO


ALTER Procedure [dbo].[ReportingViewReportSum]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, SUM(dbo.SubOrders.SubOrderSaldo) AS SaldoSum, 
                      SUM(dbo.SubOrders.Amount) AS AmountSum
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId
WHERE [SalesDate]>=@FromDate AND [SalesDate]<=@ToDate AND (dbo.Orders.OrderStatus = '2' OR dbo.Orders.OrderStatus = '3')                     

END


GO

ALTER PROCEDURE [dbo].[ReportingViewSalesChannels]
@FromDate datetime,
@ToDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT      dbo.AppTypes.Platform,
			COUNT(dbo.Orders.Id) AS OrderCount, 
			SUM(dbo.SubOrders.Amount) AS Amount, 
			SUM(dbo.SubOrders.SubOrderSum) AS OrderSum, 
			SUM(dbo.SubOrders.SubOrderCreditSum) AS CreditSum, 
			SUM(dbo.SubOrders.SubOrderSaldo) AS Saldo,
			dbo.Orders.PaymentMethod
FROM        dbo.Orders INNER JOIN
            dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
            dbo.AppTypes ON dbo.Orders.AppType = dbo.AppTypes.Id
WHERE dbo.Orders.SalesDate BETWEEN @FromDate AND @ToDate
GROUP BY dbo.Orders.OrderStatus, dbo.AppTypes.Platform, dbo.Orders.PaymentMethod
HAVING      (dbo.Orders.OrderStatus = '2' OR dbo.Orders.OrderStatus = '3')
ORDER BY Platform, PaymentMethod

END


GO
ALTER Procedure [dbo].[ReportingViewReimbursement]
@FromDate datetime,
@ToDate datetime

AS
BEGIN

SELECT     TOP (100) PERCENT dbo.Refunds.OrderNr, dbo.Refunds.Timestamp, dbo.Orders.Email, dbo.Orders.SalesDate, dbo.Orders.ExpireDate, dbo.Orders.OrderSum, 
                      dbo.Refunds.CreditSum, dbo.Orders.Saldo, dbo.Refunds.Name, dbo.Refunds.Address, dbo.Refunds.PostNumber, dbo.Refunds.City, dbo.Refunds.Phone, 
                      dbo.Refunds.Message, dbo.Refunds.Employee, dbo.AppTypes.Platform, dbo.AppTypes.Version
FROM         dbo.Refunds INNER JOIN
                      dbo.Orders ON dbo.Refunds.OrderNr = dbo.Orders.Id INNER JOIN
                      dbo.AppTypes ON dbo.Orders.AppType = dbo.AppTypes.Id
WHERE     (dbo.Refunds.Timestamp >= @FromDate) AND (dbo.Refunds.Timestamp <= @ToDate) AND (dbo.Orders.OrderStatus = '2' OR dbo.Orders.OrderStatus = '3')
ORDER BY dbo.Refunds.CreditSum DESC

END


