﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using PostNord.Portal.DigitalPostage.Bll;

namespace PostNord.Portal.DigitalPostageWS.Layouts.DigitalPostageWS
{
    public partial class WsReceipt : UnsecuredLayoutsPageBase
    {

        /// <summary>
        /// Gets a value that indicates whether users can access the page without logging in.
        /// </summary>
        /// <returns>true if anonymous access is allowed; otherwise, false. </returns>
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the transaction identifier.
        /// </summary>
        /// <value>
        /// The transaction identifier.
        /// </value>
        private string TransactionId
        {
            get
            {
                return (this.Page.Request["transactionId"] != null ? this.Page.Request["transactionId"] : string.Empty);
            }
        }

        /// <summary>
        /// Gets the response code.
        /// </summary>
        /// <value>
        /// The response code.
        /// </value>
        private string ResponseCode
        {
            get
            {
                return (this.Page.Request["responseCode"] != null ? this.Page.Request["responseCode"] : string.Empty);
            }
        }


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string returnCode = this.ResponseCode;

            Orders order = null;
            try
            {
                order = new Orders(this.TransactionId);
                if (order.Id > 0)
                {
                    order.AddLog(string.Format("WS Receipt. ResponseCode: {0}", this.ResponseCode));
                }
            }
            catch { }

            if (order == null || !(order.Id > 0) || this.ResponseCode != "OK")
            {
                returnCode = "Cancel";
            }

            this.Response.ContentType = "text/html";
            this.Response.Clear();
            this.Response.Write(string.Format("<div><b>TransactionId: </b><span id='transactionId'>{0}</span></div>", this.TransactionId));
            this.Response.Write(string.Format("<div><b>Response code: </b><span id='responseCode'>{0}</span></div>", returnCode));
            //PostNord uses a new app that iframes the solution and the parent application needs the information
            this.Response.Write("<script>");
            this.Response.Write("var data = {");
            this.Response.Write(string.Format("transactionId: '{0}',", this.TransactionId));
            this.Response.Write(string.Format("responseCode: '{0}'", returnCode));
            this.Response.Write("};");
            this.Response.Write("parent.postMessage(JSON.stringify(data), '*');");
            this.Response.Write("</script>");
            this.Response.End();
            this.Response.Close();
        }
    }
}
