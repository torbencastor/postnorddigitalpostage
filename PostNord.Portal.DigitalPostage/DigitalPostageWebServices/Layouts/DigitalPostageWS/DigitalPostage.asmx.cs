﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services;
using Microsoft.SharePoint;
using PostNord.Portal.DigitalPostageWS.Data;
using PostNord.Portal.DigitalPostage.Bll;
using PostNord.Portal.DigitalPostage.Helpers;
using System.Net;
using System.Web;
using Microsoft.SharePoint.Utilities;
using System.DirectoryServices.AccountManagement;

namespace PostNord.Portal.DigitalPostageWS
{
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    [WebService(Namespace = "http://digitalpostage.post.dk/webservices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class DigitalPostage : WebService
    {
        private int WEBSERVICE_VERSION = 30000;

        #region private Functions
        /// <summary>
        /// Checks the client.
        /// </summary>
        /// <param name="platform">The platform.</param>
        /// <param name="version">The version.</param>
        /// <param name="message">The message.</param>
        /// <param name="appVersion">The application version.</param>
        /// <returns></returns>
        private string CheckClient(string platform, double version, out string message, out AppVersion appVersion)
        {
            appVersion = null;
            message = string.Empty;
            string returnCode = ""; // "" = ok

            if (string.IsNullOrEmpty(platform))
            {
                return PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
            }

            string requestSsl = ConfigSettings.GetText("RequireSSL");
            bool requestSslVal = true;
            if (requestSsl != string.Empty)
            {
                bool.TryParse(requestSsl.Trim(), out requestSslVal);
            }

            if (requestSslVal && !HttpContext.Current.Request.IsSecureConnection)
            {
                return PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.WRONG_WEB_SERVICE.ToString();
            }

            appVersion = AppVersion.GetAppVersion(platform);
            if (appVersion == null)
            {
                return PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.PLATFORM_NOT_SUPPORTED.ToString();
            }

            if (appVersion.Disable)
            {
                returnCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.WEB_SHOP_IS_CLOSED.ToString();
            }
            else if (version < appVersion.MinVersion || version > appVersion.MaxVersion)
            {
                returnCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.VERSION_NOT_SUPPORTED.ToString();
            }
            
            message = appVersion.Message.Replace("\n", "<br/>");
            return returnCode;
        }

        /// <summary>
        /// Checks the client.
        /// </summary>
        /// <param name="platform">The platform.</param>
        /// <param name="version">The version.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        private string CheckClient(string platform, double version, out string message)
        {
            message = string.Empty;
            AppVersion appVersion = null;
            return CheckClient(platform, version, out message, out appVersion);
        }

        /// <summary>
        /// Gets the product from product list
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        private Product GetProduct(int id, int versionId)
        {
            var prodsInfo = this.GetProducts(string.Empty, versionId);
            Product product = null;

            foreach (var cat in prodsInfo.Categories)
            {
                product = (from p in cat.Products.OfType<Product>() where (int)p.Id == id select p).FirstOrDefault<Product>();
                if (product != null)
                {
                    break;
                }
            }

            return product;
        }

        /// <summary>
        /// Validates the specified security token.
        /// </summary>
        /// <param name="token">The security token.</param>
        /// <returns></returns>
        private bool Validate(string token)
        {
            return (token == ConfigSettings.GetText("WebServiceToken"));
/*
            string[] parts = token.Split(';');
            if (parts.Length != 3)
            {
                return false;
            }
            try
            {
                string domain = parts[0];
                string username = parts[1];
                string password = parts[2];

                bool valid = false;
                using (PrincipalContext context = new PrincipalContext(ContextType.Domain, domain))
                {
                    valid = context.ValidateCredentials(username, password);
                }
                return valid;
            }
            catch 
            {
                return false;
            }
 */
        }

        #endregion


        #region WebMethods

        /// <summary>
        /// Adds the product.
        /// </summary>
        /// <param name="token">The security token.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="platform">The platform.</param>
        /// <param name="email">The email.</param>
        /// <param name="channel">The channel.</param>
        /// <param name="productId">The product identifier.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="priority">The priority.</param>
        /// <returns></returns>
        [WebMethod]
        public Order AddProduct(string token, long orderId, int versionId, string platform, string email, string channel, int productId, int amount, string priority)
        {
            Order order = new Order();
            string errorCode = string.Empty;
            string message = string.Empty;

            if (!Validate(token))
            {
                order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ACCESS_DENIED.ToString();
                return order;
            }

            try
            {
                string returnCode = CheckClient(platform, versionId, out message);
                if (returnCode != "")
                {
                    order.ErrorCode = returnCode;
                    order.ErrorDescription = message;
                    return order;
                }

                if (priority != "A" && priority != "B")
                {
                    order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.PRIORITY_OUT_OF_RANGE.ToString();
                    return order;
                }

                Product product = GetProduct(productId, versionId);
                if (product == null)
                {
                    order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.PRODUCT_NOT_FOUND.ToString();
                    return order;
                }

                order.Email = email.ToLower();

                Portal.DigitalPostage.Bll.Orders webshopOrder = new Orders(orderId);
                if (webshopOrder.Id == 0)
                {
                    order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_NOT_FOUND.ToString();
                    return order;
                }

                webshopOrder.AddLogDebug(string.Format("AddProduct: {0} ({1})", HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.UserAgent));

                if (webshopOrder.Email != email.ToLower())
                {
                    order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.EMAIL_DOES_NOT_MATCH.ToString();
                    return order;
                }

                var subOrder = webshopOrder.AddSubOrder(product.Category, (short)product.Weight, priority, product.Category, amount, product.GetTruePrice(priority), productId, (double)product.APriceWithoutVAT/100, product.AVAT);

                // Something is wrong!!
                //webshopOrder = new Orders(orderId);
                order = new Order(webshopOrder);

            }
            catch (Exception ex)
            {
                order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                order.ErrorDescription = ex.Message;
            }
            return order;
        }

        /// <summary>
        /// Captures the specified order. Transferring the money from customer to Post.dk
        /// </summary>
        /// <param name="token">The security token.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="platform">The platform.</param>
        /// <param name="email">The email.</param>
        /// <param name="sendReceipt">if set to <c>true</c> send receipt to Customer.</param>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <param name="mobilePaySignature">The mobile pay signature.</param>
        /// <returns>
        /// Dataset containing order status
        /// </returns>
        [WebMethod]
        public OrderStatus Capture(string token, long orderId, int versionId, string platform, string email, bool sendReceipt, string transactionId, string mobilePaySignature)
        {
            string message = string.Empty;

            if (transactionId == null)
            {
                transactionId = string.Empty;
            }

            if (mobilePaySignature == null)
            {
                mobilePaySignature = string.Empty;
            }

            if (TextSettings.IsCacheDirty())
            {
                TextSettings.ClearCache(SPContext.Current.Web.Language);
            }

            if (ConfigSettings.IsCacheDirty())
            {
                ConfigSettings.ClearCache(SPContext.Current.Web.Language);
            }

            OrderStatus orderStatus = new OrderStatus();
            string error = string.Empty;

            string returnCode = CheckClient(platform, versionId, out message);
            // if webshop is closed during capture we allow the buyer to finish his shopping
            if (returnCode != "" && returnCode != PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.WEB_SHOP_IS_CLOSED.ToString())
            {
                orderStatus.ErrorCode = returnCode;
                return orderStatus;
            }

            if (!Validate(token))
            {
                orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ACCESS_DENIED.ToString();
                return orderStatus;
            }

            try
            {
                // Load Order
                Portal.DigitalPostage.Bll.Orders webshopOrder = new Orders(orderId);

                if (webshopOrder.Id == 0)
                {
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_NOT_FOUND.ToString();
                    return orderStatus;
                }

                webshopOrder.AddLogDebug(string.Format("Capture: {0} ({1})", HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.UserAgent));

                if (webshopOrder.Email != email.ToLower())
                {
                    webshopOrder.AddLogError(string.Format("Capture failed. Email: {0} does not match order email.", email));
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.EMAIL_DOES_NOT_MATCH.ToString();
                    return orderStatus;
                }

                if (webshopOrder.Status == Orders.OrderStatusValues.Paid)
                {
                    webshopOrder.AddLogError("Capture failed. Order is already paid.");
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_ALREADY_PAID.ToString();
                    return orderStatus;
                }

                if (webshopOrder.Status == Orders.OrderStatusValues.Cancelled )
                {
                    webshopOrder.AddLogError(string.Format("Capture failed. Order is {0}", webshopOrder.Status));
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_CANCELLED.ToString();
                    return orderStatus;
                }

                if (webshopOrder.Status == Orders.OrderStatusValues.Refunded || webshopOrder.Status == Orders.OrderStatusValues.Credited)
                {
                    webshopOrder.AddLogError(string.Format("Capture failed. Order is {0}", webshopOrder.Status));
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_REFUNDED.ToString();
                    return orderStatus;
                }

                // Create WebService Order
                var order = new Order(webshopOrder);
                orderStatus = new OrderStatus(orderId, order.SalesDate, order.ExpireDate);

                // If mobile Pay we we call CaptureMobilePay
                if (webshopOrder.Status == Orders.OrderStatusValues.ReadyForPaymentMobilePay)
                {
                    // If we dont have a signature we assume that payment is reserved and not yet captured
                    if (string.IsNullOrEmpty(mobilePaySignature))
                    {
                        webshopOrder.AddLog(string.Format("MobilePay Capture V2. TransactionId: {0} OrderNr: {1}", transactionId, webshopOrder.MobilePayOrderNr));

                        string err = webshopOrder.CaptureMobilePay(transactionId);
                        if (err != "OK")
                        {
                            error = string.Format("MobilePay payment Capture failed. Error: {0}", err);
                            webshopOrder.AddLogError(error);
                            orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.MOBILE_PAY_CAPTURE_ERROR.ToString();
                            orderStatus.ErrorDescription = error;
                            return orderStatus;
                        }
                    }
                    // Check signature
                    else if (!webshopOrder.CaptureMobilePay(transactionId, mobilePaySignature))
                    {
                        webshopOrder.AddLog(string.Format("MobilePay Capture. TransactionId: {0} signature:{1}", transactionId, mobilePaySignature));
                        error = "MobilePay payment validation failed.";
                        webshopOrder.AddLogError(error);
                        orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.MOBILE_PAY_SIGNATURE_INVALID.ToString();
                        orderStatus.ErrorDescription = error;
                        return orderStatus;
                    }
                }
                // Capture through NetAxcept
                else
                {
                    if (webshopOrder.CaptureId != transactionId)
                    {
                        error = string.Format("Capture failed. TransactionId mismatch. Expected: {0} Got: {1}", webshopOrder.CaptureId, transactionId);
                        webshopOrder.AddLogError(error);
                        orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.TRANSACTION_ID_MISMATCH.ToString();
                        orderStatus.ErrorDescription = error;
                        return orderStatus;
                    }

                    webshopOrder.AddLog(string.Format("WS NETS Capture. TransactionId: {0}", transactionId));

                    // Capture and get CreditCard Info from NETS
                    Orders.CreditCardInfo cardInfo = webshopOrder.Capture();

                    if (!string.IsNullOrEmpty(cardInfo.ErrorCode))
                    {
                        orderStatus.ErrorCode = cardInfo.ErrorCode;
                        orderStatus.ErrorDescription = cardInfo.ErrorMessage;
                        return orderStatus;
                    }

                    if (cardInfo.CreditCardHash != string.Empty)
                    {
                        orderStatus.CreditCardHash = cardInfo.CreditCardHash;
                    }

                    orderStatus.CreditCardExpiryDate = cardInfo.ExpiryDate;
                    orderStatus.CreditCardIssuer = cardInfo.Issuer;
                    orderStatus.CreditCardScrambledData = cardInfo.ScrambledData;
                }

                orderStatus.Status = webshopOrder.Status.ToString().ToUpper();
                // If order is payed all systems are GO, retrieve codes from order and add it to WebService Order
                if (webshopOrder.Status == Orders.OrderStatusValues.Paid)
                {
                    foreach (var webSubOrder in webshopOrder.SubOrders)
                    {
                        SubOrder subOrder = new SubOrder();
                        subOrder.Weight = webSubOrder.Weight;
                        subOrder.Priority = webSubOrder.Priority;
                        subOrder.Category = webSubOrder.Category;
                        subOrder.Price = Library.ConvertPrice(webSubOrder.Price);

                        foreach (var webShopCode in webSubOrder.Codes)
                        {
                            Code code = new Code(webShopCode.PostCode);
                            subOrder.Codes.Add(code);
                        }
                        orderStatus.SubOrders.Add(subOrder);
                    }
                    if (sendReceipt || true) // We always want to send a receipt
                    {
                        Guid siteId = SPContext.Current.Site.ID;

                        try
                        {
                            SPSecurity.RunWithElevatedPrivileges(delegate()
                            {
                                using (SPSite siteElevated = new SPSite(siteId))
                                {
                                    // Send receipt only!
                                    webshopOrder.SendMail(true);
                                    webshopOrder.AddLog("Receipt sent.");
                                }
                            });
                        }
                        catch (Exception ex)
                        {
                            error = string.Format("Error sending mail: {0}", ex.Message);
                            webshopOrder.AddLogError(error);
                            orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNABLE_TO_SEND_MAIL.ToString();
                            orderStatus.ErrorDescription = error;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                orderStatus.ErrorDescription = ex.Message;
            }

            return orderStatus;
        }

        /// <summary>
        /// Creates the order.
        /// </summary>
        /// <param name="token">The security token.</param>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="platform">The platform.</param>
        /// <param name="email">The email.</param>
        /// <param name="channel">The channel.</param>
        /// <param name="productId">The product identifier.</param>
        /// <param name="amount">The amount. Number of stamps</param>
        /// <param name="priority">The priority. (A/B)</param>
        /// <returns></returns>
        [WebMethod]
        public Order CreateOrder(string token, int versionId, string platform, string email, string channel, int productId, int amount, string priority)
        {
            Order order = new Order();
            string message = string.Empty;
            AppVersion appVersion = null;

            try
            {
                string returnCode = CheckClient(platform, versionId, out message, out appVersion);
                if (returnCode != "")
                {
                    order.ErrorCode = returnCode;
                    order.ErrorDescription = message;
                    return order;
                }

                if (!Validate(token))
                {
                    order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ACCESS_DENIED.ToString();
                    return order;
                }

                if (priority != "A" && priority != "B")
                {
                    order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.PRIORITY_OUT_OF_RANGE.ToString();
                    return order;
                }

                if (string.IsNullOrEmpty(email) || !Library.ValidateEmail(email))
                {
                    order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.EMAIL_NOT_VALID.ToString();
                    return order;
                }

                Product product = GetProduct(productId, versionId);
                if (product == null)
                {
                    order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.PRODUCT_NOT_FOUND.ToString();
                    return order;
                }


                Portal.DigitalPostage.Bll.Orders webshopOrder = new Orders();
                webshopOrder.Email = email.ToLower();
                webshopOrder.Save();
                webshopOrder.AddLogDebug(string.Format("CreateOrder: {0} ({1})", HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.UserAgent));
                webshopOrder.AddLog(string.Format("APP Order created. Channel: {0} Platform: {1} Version: {2}", channel, platform, versionId), OrderLogs.SeverityValues.Info);
                webshopOrder.SetAppInfo(Orders.PaymentMethods.Unknown, platform, versionId);
                order.ExpireDate = Library.ToUnixTime(DateTime.Today.AddDays(Orders.PostageMobileValidInDaysInApp));
                order = new Order(email, product, amount, priority);
                order.Id = webshopOrder.Id;
                order.Status = webshopOrder.Status.ToString().ToUpper();
                order.PaymentMethods = new PaymentMethods();
                order.PaymentMethods.Nets = !appVersion.DisableNets;
                order.PaymentMethods.MobilePay = !appVersion.DisableMobilePay;

                var subOrder = webshopOrder.AddSubOrder(product.Category, (short)product.Weight, priority, product.Category, amount, product.GetTruePrice(priority), productId, (double)product.APriceWithoutVAT/100,product.AVAT);
            }
            catch (Exception ex)
            {
                order.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                order.ErrorDescription = ex.Message;
            }
            return order;
        }

        private ProductsInfo GetProducts(string platform, int versionId)
        {
            int VATIndex = 100;
            TextSettings.ListName = "TextSettings";
            // MPE: Use another Textsettings list if version is 50000 or higher.
            // If versionId is more than 50000 then add DK-VAT to product APrice
            if (versionId >= 50000)
            {
                //VATIndex = 125;
                TextSettings.ListName = "TextSettingsV5";
            }
 

            Guid siteId = SPContext.Current.Site.ID;
            ProductsInfo prodInfo = new ProductsInfo();

            
            try
            {
                prodInfo = new ProductsInfo(WEBSERVICE_VERSION, platform);
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite siteElevated = new SPSite(siteId))
                    {
                        string propertyKey = ConfigSettings.GetPropertyBagKey(siteElevated.RootWeb);
                        if (siteElevated.RootWeb.Properties.ContainsKey(propertyKey))
                        {
                            string url = siteElevated.RootWeb.Properties[propertyKey];
                            using (SPWeb adminWeb = siteElevated.OpenWeb(url))
                            {
                                PriceCategory pcat = new PriceCategory();
                                //ProductInfo pInfo = new ProductInfo();

                                List<PriceCategory> priceCategoryList = pcat.GetProductCategories(adminWeb);
                                List<ProductInfo> productInfoList = ProductInfo.GetProductInfos(adminWeb);

                                foreach (PriceCategory pCatItem in priceCategoryList)
                                {
                                    var catProducts = ProductInfo.GetProductInfosCat(adminWeb, pCatItem.Id);
                                    Category c = new Category(pCatItem.Id, pCatItem.Title);
                                    prodInfo.Categories.Add(c);

                                    foreach (var prodInfoListItem in catProducts)
                                    {
                                        if (prodInfoListItem.PriceCategoryId == pCatItem.Id && 
                                            (prodInfoListItem.APrice > 0 || prodInfoListItem.BPrice > 0)
                                            )
                                        {
                                            Product p = new Product(
                                                    prodInfoListItem.Id,
                                                    prodInfoListItem.Title,
                                                    prodInfoListItem.Weight,
                                                    (prodInfoListItem.APrice * prodInfoListItem.VAT) / 100,
                                                    prodInfoListItem.BPrice,
                                                    prodInfoListItem.FromDate,
                                                    prodInfoListItem.ToDate,
                                                    prodInfoListItem.PriceCategory,
                                                    prodInfoListItem.VAT
                                                );
                                            c.Products.Add(p);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            throw new ApplicationException("AdminUrl not found!");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                prodInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                prodInfo.ErrorDescription = ex.Message;
            }
            return prodInfo;
        }

        /// <summary>
        /// Gets the category list.
        /// </summary>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="platform">The platform.</param>
        /// <returns>
        /// DataSet containing all Categories and their corresponding products
        /// </returns>
        [WebMethod]
        public ProductsInfo GetProducts(int versionId, string platform)
        {
            TextSettings.ListName = "TextSettings";           
            if (versionId >= 50000) TextSettings.ListName = "TextSettingsV5"; 

            string result = string.Empty;
            string message = string.Empty;
            ProductsInfo prodInfo = new ProductsInfo(versionId, platform);

            string returnCode = CheckClient(platform, versionId, out message);
            if (returnCode != "")
            {
                prodInfo.ErrorCode = returnCode;
                prodInfo.ErrorDescription = message;
                return prodInfo;
            }

            return GetProducts(platform, versionId);
        }

        /// <summary>
        /// Get Order Status.
        /// </summary>
        /// <param name="token">The security token.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="platform">The platform.</param>
        /// <param name="email">The email.</param>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <param name="mobilePaySignature">The mobile pay signature.</param>
        /// <returns>
        /// Dataset containing order status
        /// </returns>
        [WebMethod]
        public OrderStatus GetOrderStatus(string token, long orderId, int versionId, string platform, string email, string transactionId, string mobilePaySignature)
        {
            OrderStatus orderStatus = new OrderStatus();
            Portal.DigitalPostage.Bll.Orders webshopOrder = null;
            try
            {
                webshopOrder = new Orders(orderId);

                if (webshopOrder.Id == 0)
                {
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_NOT_FOUND.ToString();
                    return orderStatus;
                }

                if (webshopOrder.Email != email.ToLower())
                {
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.EMAIL_DOES_NOT_MATCH.ToString();
                    return orderStatus;
                }

                if (!Validate(token))
                {
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ACCESS_DENIED.ToString();
                    return orderStatus;
                }

                var order = new Order(webshopOrder);
                webshopOrder.AddLogDebug(string.Format("GetOrderStatus: {0} ({1})", HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.UserAgent));
                orderStatus = new OrderStatus(orderId, order.SalesDate, order.ExpireDate);
                orderStatus.Status = webshopOrder.Status.ToString().ToUpper();

                if (webshopOrder.Status == Orders.OrderStatusValues.Cancelled)
                {
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_CANCELLED.ToString();
                    webshopOrder.AddLogError(string.Format("GetOrderStatus failed. Order is {0}. GetStatus result: {1}", webshopOrder.Status, orderStatus.ErrorCode));
                    return orderStatus;
                }

                if (webshopOrder.Status == Orders.OrderStatusValues.Refunded || webshopOrder.Status == Orders.OrderStatusValues.Credited)
                {
                    orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_REFUNDED.ToString();
                    webshopOrder.AddLogError(string.Format("GetOrderStatus failed. Order is {0}. GetStatus result: {1}", webshopOrder.Status, orderStatus.ErrorCode));
                    return orderStatus;
                }

                webshopOrder.AddLogDebug(string.Format("Order status: {0}", orderStatus.Status));

                if (webshopOrder.Status == Orders.OrderStatusValues.Paid)
                {
                    if (webshopOrder.PaymentMethod != Orders.PaymentMethods.MobilePay)
                    {
                        if (webshopOrder.CaptureId != transactionId)
                        {
                            orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.TRANSACTION_ID_MISMATCH.ToString();
                            return orderStatus;
                        }

                        Orders.CreditCardInfo cardInfo = webshopOrder.GetOrderStatus();
                        if (cardInfo != null)
                        {
                            orderStatus.CreditCardExpiryDate = cardInfo.ExpiryDate;
                            orderStatus.CreditCardHash = cardInfo.CreditCardHash;
                            orderStatus.CreditCardIssuer = cardInfo.Issuer;
                            orderStatus.CreditCardScrambledData = cardInfo.ScrambledData;
                        }
                    }

                    foreach (var webSubOrder in webshopOrder.SubOrders)
                    {
                        SubOrder subOrder = new SubOrder();
                        subOrder.Weight = webSubOrder.Weight;
                        subOrder.Priority = webSubOrder.Priority;
                        subOrder.Category = webSubOrder.Category;
                        subOrder.Destination = webSubOrder.Destination;
                        subOrder.Price = Library.ConvertPrice(webSubOrder.Price);

                        foreach (var webShopCode in webSubOrder.Codes)
                        {
                            if (webShopCode.Status == Codes.CodeStatusValues.Unused)
                            {
                                Code code = new Code(webShopCode.PostCode);
                                subOrder.Codes.Add(code);
                            }
                        }
                        orderStatus.SubOrders.Add(subOrder);
                    }
                } 
                else if (webshopOrder.Status == Orders.OrderStatusValues.ReadyForPaymentMobilePay)
                {
                    string mpStatus = string.Empty;
                    if (bool.Parse(ConfigSettings.GetText("MobilePayGetOrderStatusTest", "false")))
                    {
                        webshopOrder.AddLogDebug("MobilePayDisableValidation enabled. Signature now valid.");
                        if (transactionId == "TestPaidInMobilePay")
                        {
                            mpStatus = "OK";
                        }
                        else if (transactionId == "TestNotPaidInMobilePay")
                        {
                            mpStatus = "N";
                        }
                        else
                        {
                            orderStatus.Status = ConfigSettings.GetText("MobilePayGetOrderStatusTestValue");
                            webshopOrder.AddLogDebug(string.Format("MobilePayGetOrderStatusTestValue: {0}", orderStatus.Status));
                            return orderStatus;
                        }
                    }
                    else
                    {
                        mpStatus = webshopOrder.CheckMobilePay();
                        webshopOrder.AddLogDebug(string.Format("MobilePay WS Result: Status:{0} ", mpStatus));
                    }
                    
                    if (mpStatus == "OK")
                    {
                        orderStatus.Status = "PAIDINMOBILEPAY";
                    }
                    else if (mpStatus == PostNord.Portal.DigitalPostage.Helpers.MobilePay.PAYMENTSTATUS_CANCELLED)
                    {
                        orderStatus.Status = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_CANCELLED.ToString();
                    }
                    else if (mpStatus == PostNord.Portal.DigitalPostage.Helpers.MobilePay.PAYMENTSTATUS_REFUNDED)
                    {
                        orderStatus.Status = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_REFUNDED.ToString();
                    }
                    else
                    {
                        orderStatus.Status = webshopOrder.Status.ToString().ToUpper();
                    }
                }
                else
                {
                    orderStatus.Status = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                    orderStatus.ErrorDescription = string.Format("Unexpected order status: {0}", webshopOrder.Status);
                }
                webshopOrder.AddLogDebug(string.Format("GetStatus result:{0} {1}", orderStatus.Status, (!string.IsNullOrEmpty(orderStatus.ErrorDescription)?"- " + orderStatus.ErrorDescription:"")));
            }
            catch (Exception ex)
            {
                orderStatus.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                orderStatus.ErrorDescription = string.Format("Mobile WS Exception: {0}", ex.Message);
                webshopOrder.AddLogError(string.Format("Mobile WS Exception: {0}", ex));
            }

            return orderStatus;
        }

        /// <summary>
        /// Send the order to NETS payment 
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="platform">The platform.</param>
        /// <param name="email">The email.</param>
        /// <param name="paymentMethod">The payment method.</param>
        /// <param name="cardHash">The card hash.</param>
        /// <returns>Return a dataset containing transactionId from NETS</returns>
        [WebMethod]
        public PlaceOrderStatus PlaceOrder(string token, long orderId, int versionId, string platform, string email, string paymentMethod, string cardHash)
        {
            PlaceOrderStatus placeOrder = new PlaceOrderStatus();
            string message = string.Empty;

            string returnCode = CheckClient(platform, versionId, out message);
            if (returnCode != "")
            {
                placeOrder.ErrorCode = returnCode;
                placeOrder.ErrorDescription = message;
                return placeOrder;
            }

            if (!Validate(token))
            {
                placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ACCESS_DENIED.ToString();
                return placeOrder;
            }

            Portal.DigitalPostage.Bll.Orders webshopOrder = new Orders(orderId);
            if (webshopOrder.Id == 0)
            {
                placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_NOT_FOUND.ToString();
                return placeOrder;
            }

            if (webshopOrder.Email != email.ToLower())
            {
                placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.EMAIL_DOES_NOT_MATCH.ToString();
                return placeOrder;
            }

            if (webshopOrder.Status ==  Orders.OrderStatusValues.Paid || webshopOrder.Status == Orders.OrderStatusValues.Credited)
            {
                placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_IS_CLOSED.ToString();
                return placeOrder;
            }

            switch (paymentMethod)
            {
                case "MobilePay":
                    try
                    {
                        webshopOrder.SetAppInfo(Orders.PaymentMethods.MobilePay, platform, versionId);
                        webshopOrder.CreatePaymentMobilePay();
                        placeOrder = new PlaceOrderStatus(orderId);
                        placeOrder.TerminalUrl = string.Empty;
                        placeOrder.TransactionId = string.Empty;
                    }
                    catch (WebException ex)
                    {
                        if (ex.Message == "The operation has timed out")
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_TIME_OUT.ToString();
                            placeOrder.ErrorDescription = ex.Message;
                            webshopOrder.AddLogError("PlaceOrder MP TimeOut. " + ex.Message);
                        }
                        else
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                            placeOrder.ErrorDescription = ex.Message;
                            webshopOrder.AddLogError("PlaceOrder MP unknown WebException. " + ex.Message);
                        }
                        return placeOrder;
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "Timeout på forbindelse.")
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_TIME_OUT.ToString();
                            webshopOrder.AddLogError("PlaceOrder MP Timeout. " + ex.Message);
                        }
                        else
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                            webshopOrder.AddLogError("PlaceOrder MP unknown Exception. " + ex.Message);
                        }
                        placeOrder.ErrorDescription = ex.Message;
                        return placeOrder;
                    }

                    break;
                case "NetAxcept":
                    try
                    {
                        webshopOrder.SetAppInfo(Orders.PaymentMethods.Nets, platform, versionId);
                        string url = webshopOrder.CreatePaymentNetAxcept();
                        placeOrder = new PlaceOrderStatus(orderId);
                        placeOrder.TerminalUrl = url;
                        placeOrder.TransactionId = webshopOrder.CaptureId;
                    }
                    catch (WebException ex)
                    {
                        if (ex.Message == "The operation has timed out")
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_TIME_OUT.ToString();
                            placeOrder.ErrorDescription = ex.Message;
                            webshopOrder.AddLogError("PlaceOrder NETS TimeOut. " + ex.Message);
                        }
                        else
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                            placeOrder.ErrorDescription = ex.Message;
                            webshopOrder.AddLogError("PlaceOrder NETS unknown WebException. " + ex.Message);
                        }
                        return placeOrder;
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "Timeout på forbindelse.")
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_TIME_OUT.ToString();
                            webshopOrder.AddLogError("PlaceOrder NETS Timeout. " + ex.Message);
                        }
                        else
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                            webshopOrder.AddLogError("PlaceOrder NETS unknown Exception. " + ex.Message);
                        }
                        placeOrder.ErrorDescription = ex.ToString();
                        return placeOrder;
                    }
                    break;

                case "NetAxceptCardSave":
                    try
                    {
                        webshopOrder.SetAppInfo(Orders.PaymentMethods.NetsRecurring, platform, versionId);
                        string url = webshopOrder.CreatePaymentNetAxcept(true, string.Empty);
                        placeOrder = new PlaceOrderStatus(orderId);
                        placeOrder.TerminalUrl = url;
                        placeOrder.TransactionId = webshopOrder.CaptureId;
                    }
                    catch (WebException ex)
                    {
                        if (ex.Message == "The operation has timed out")
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_TIME_OUT.ToString();
                            placeOrder.ErrorDescription = ex.Message;
                            webshopOrder.AddLogError("PlaceOrder NETS TimeOut. " + ex.Message);
                        }
                        else
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                            placeOrder.ErrorDescription = ex.Message;
                            webshopOrder.AddLogError("PlaceOrder NETS unknown WebException. " + ex.Message);
                        }
                        return placeOrder;
                    }
                    catch (Exception ex)
                    {
                        placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                        placeOrder.ErrorDescription = ex.ToString();
                        return placeOrder;
                    }
                    break;
                case "NetAxceptCardReuse":
                    webshopOrder.SetAppInfo(Orders.PaymentMethods.NetsRecurring, platform, versionId);
                    if (cardHash == string.Empty)
                    {
                        placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.HASHCODE_MISSING.ToString();
                        return placeOrder;
                    }
                    try
                    {
                        string url = webshopOrder.CreatePaymentNetAxcept(true, cardHash);
                        placeOrder = new PlaceOrderStatus(orderId);
                        placeOrder.TerminalUrl = url;
                        placeOrder.TransactionId = webshopOrder.CaptureId;
                    }
                    catch (WebException ex)
                    {
                        if (ex.Message == "The operation has timed out")
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_TIME_OUT.ToString();
                            placeOrder.ErrorDescription = ex.Message;
                            webshopOrder.AddLogError("PlaceOrder NETS TimeOut. " + ex.Message);
                        }
                        else
                        {
                            placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                            placeOrder.ErrorDescription = ex.Message;
                            webshopOrder.AddLogError("PlaceOrder NETS unknown WebException. " + ex.Message);
                        }
                        return placeOrder;
                    }
                    catch (Exception ex)
                    {
                        placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                        placeOrder.ErrorDescription = ex.ToString();
                        return placeOrder;
                    }
                    break;
                default:
                    placeOrder.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.PAYMENTMETHOD_INVALID.ToString();
                    return placeOrder;
            }

            return placeOrder;
        }


        [WebMethod]
        public string GetText(string key, string versionId)
        {
            int _versionId = 40000;
            TextSettings.ListName = "TextSettings";
            if (versionId == string.Empty) _versionId = 40000; else _versionId = Int32.Parse(versionId);

            if (_versionId >= 50000) TextSettings.ListName = "TextSettingsV5"; 


            if (TextSettings.IsCacheDirty())
            {
                TextSettings.ClearCache(SPContext.Current.Web.Language);
            }

            return TextSettings.GetText(key);
        }

        #endregion
    }
}
