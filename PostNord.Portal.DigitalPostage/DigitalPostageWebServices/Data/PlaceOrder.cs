﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class PlaceOrderStatus
    {
        public PlaceOrderStatus()
        {
        }

        public PlaceOrderStatus(long orderId)
        {
            this.OrderId = orderId;
            this.TransactionId = "dffb4a8946eb4d42af533b142ac2f7ac";
            this.ErrorCode = string.Empty;
        }

        public long OrderId
        {
            get;
            set;
        }

        public string TransactionId
        {
            get;
            set;
        }

        public string ErrorCode
        {
            get;
            set;
        }

        public string TerminalUrl
        {
            get;
            set;    
        }

        public string ErrorDescription
        {
            get;
            set;
        }
    }
}
