﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class Code
    {
        public Code()
        {

        }

        public Code (string text)
        {
            //this.Value = value;
            this.Text = text;
        }

        //public int Value
        //{
        //    get;
        //    set;
        //}

        public string Text
        {
            get;
            set;
        }
    }
}
