﻿using Microsoft.SharePoint;
using PostNord.Portal.DigitalPostage.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class AppVersion
    {

        bool _disable;

        const string APP_VERSIONS_LIST_NAME = "AppVersions";

        /// <summary>
        /// Gets the application version from list AppVersions
        /// </summary>
        /// <param name="platform">The platform.</param>
        /// <param name="version">The version.</param>
        /// <returns></returns>
        public static AppVersion GetAppVersion(string platform)
        {
            Guid siteId = SPContext.Current.Site.ID;
            AppVersion appVersion = null;

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite siteElevated = new SPSite(siteId))
                {
                    string propertyKey = ConfigSettings.GetPropertyBagKey(siteElevated.RootWeb);
                    if (siteElevated.RootWeb.Properties.ContainsKey(propertyKey))
                    {
                        string url = siteElevated.RootWeb.Properties[propertyKey];
                        using (SPWeb adminWeb = siteElevated.OpenWeb(url))
                        {
                            SPList versions = adminWeb.Lists[APP_VERSIONS_LIST_NAME];

                            SPQuery query = new SPQuery();
                            query.ViewFields = "<FieldRef Name='LinkTitle' /><FieldRef Name='MinVersion' /><FieldRef Name='MaxVersion' /><FieldRef Name='Disabled' /><FieldRef Name='DisableMobilePay' /><FieldRef Name='DisableNets' /><FieldRef Name='Message' />";
                            query.Query = string.Format(@"<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>{0}</Value></Eq></Where>", platform);

                            SPListItemCollection items = versions.GetItems(query);

                            if (items.Count > 0) // Platform not supported 
                            {
                                SPListItem item = items[0];
                                appVersion = new AppVersion()
                                {
                                    MinVersion = (double)item[versions.Fields.GetFieldByInternalName("MinVersion").Id],
                                    MaxVersion = (double)item[versions.Fields.GetFieldByInternalName("MaxVersion").Id],
                                    Name = (string)item[versions.Fields.GetFieldByInternalName("Title").Id],
                                    Disable = (bool)item[versions.Fields.GetFieldByInternalName("Disabled").Id],
                                    DisableMobilePay = (bool)item[versions.Fields.GetFieldByInternalName("DisableMobilePay").Id],
                                    DisableNets = (bool)item[versions.Fields.GetFieldByInternalName("DisableNets").Id]
                                };

                                if (item[versions.Fields.GetFieldByInternalName("Message").Id] != null)
                                {
                                    appVersion.Message = (string)item[versions.Fields.GetFieldByInternalName("Message").Id];
                                }
                            }
                        }
                    }
                }
            });

            return appVersion;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="AppVersion"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="minVersion">The minimum version.</param>
        /// <param name="maxVersion">The maximum version.</param>
        /// <param name="disable">if set to <c>true</c> [disable].</param>
        /// <param name="disableMobilePay">if set to <c>true</c> [disable mobile pay].</param>
        /// <param name="disableNets">if set to <c>true</c> [disable nets].</param>
        /// <param name="message">The message.</param>
        public AppVersion(string name, int minVersion, int maxVersion, bool disable, bool disableMobilePay, bool disableNets, string message)
        {
            this.Name = name;
            this.MinVersion = minVersion;
            this.MaxVersion = maxVersion;
            this.Disable = disable;
            this.DisableMobilePay = disableMobilePay;
            this.DisableNets = disableNets;
            this.Message = message;
        }

        public AppVersion()
        {
            this.Message = string.Empty;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the minimum version.
        /// </summary>
        /// <value>
        /// The minimum version.
        /// </value>
        public double MinVersion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the maximum version.
        /// </summary>
        /// <value>
        /// The maximum version.
        /// </value>
        public double MaxVersion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether APP is disabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [disable]; otherwise, <c>false</c>.
        ///   If both DisableNets and DisableMobilePay are <c>true</c> Disable is also <c>true</c>
        /// </value>
        public bool Disable
        {
            get
            {
                if (this.DisableNets && this.DisableMobilePay)
                {
                    return true;
                }
                return _disable;
            }
            set 
            {
                _disable = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [disable mobile pay].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [disable mobile pay]; otherwise, <c>false</c>.
        /// </value>
        public bool DisableMobilePay
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [disable nets].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [disable nets]; otherwise, <c>false</c>.
        /// </value>
        public bool DisableNets
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message
        {
            get;
            set;
        }

    }
}
