﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class Category
    {
        public int Id
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }
        public List<Product> Products
        {
            get;
            set;
        }
        internal Category()
        {
        }
        public Category(int id, string title)
        {
            this.Id = id;
            this.Title = title;
            this.Products = new List<Product>();
        }
    }
}