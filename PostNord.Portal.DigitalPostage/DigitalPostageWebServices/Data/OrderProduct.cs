﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class OrderProduct
    {
        public int Id
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }
        public int Weight
        {
            get;
            set;
        }
        public int Amount
        {
            get;
            set;
        }
        public string Priority
        {
            get;
            set;
        }
        public int Price
        {
            get;
            set;
        }
        public int ProductSum
        {
            get
            {
                return this.Amount * this.Price;
            }
            set { }
        }
        internal OrderProduct()
        {

        }

        public OrderProduct(Product product, string priority, int amount)
        {
            this.Id = product.Id;
            this.Title = product.Title;
            this.Weight = product.Weight;
            this.Priority = priority;
            this.Price = product.GetPrice(priority);
            this.Amount = amount;
        }

        //public OrderProduct(int id, string title, int weight, string priority, int price, int amount)
        //{
        //    this.Id = id;
        //    this.Title = title;
        //    this.Weight = weight;
        //    this.Priority = priority;
        //    this.Price = price;
        //    this.Amount = amount;
        //}
    }
}
