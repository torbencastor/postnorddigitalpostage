﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class Message
    {

        public string Headline
        { 
            get; 
            set; 
        }

        public string Text
        {
            get;
            set;
        }

        public Message()
        { }
    
        public Message(string headline, string text)
        {
            this.Headline = headline;
            this.Text = text;
        }
    }
}
