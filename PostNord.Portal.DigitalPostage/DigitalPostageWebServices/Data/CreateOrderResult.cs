﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class CreateOrderResult
    {
        internal CreateOrderResult()
        { }

        public CreateOrderResult(long orderNr)
        {
            this.OrderNr = orderNr;
            this.ErrorCode = string.Empty;
        }

        public long OrderNr
        {
            get;
            set;
        }
        public string ErrorCode
        {
            get;
            set;
        }
    }
}