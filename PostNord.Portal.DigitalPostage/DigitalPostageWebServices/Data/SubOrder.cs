﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class SubOrder
    {

        public int Weight { get; set; }
        public int Price { get; set; }
        public string Priority { get; set; }
        public string Category { get; set; }
        public string Destination { get; set; }
        public List<Code> Codes { get; set; }

        public SubOrder()
        {
            this.Codes = new List<Code>();
        }

    }
}
