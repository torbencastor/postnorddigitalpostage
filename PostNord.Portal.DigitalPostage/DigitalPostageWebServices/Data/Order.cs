﻿using PostNord.Portal.DigitalPostage.Bll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class Order
    {

        public Order()
        {
            this.Products = new List<OrderProduct>();
            this.PaymentMethods = new PaymentMethods();
        }

        public Order(PostNord.Portal.DigitalPostage.Bll.Orders webshopOrder)
        {
            this.PaymentMethods = new PaymentMethods();
            this.Id = webshopOrder.Id;
            this.SalesDate = Library.ToUnixTime(webshopOrder.SalesDate);
            this.ExpireDate = Library.ToUnixTime(webshopOrder.SalesDate.AddDays(Orders.PostageMobileValidInDaysInApp));
            this.Email = webshopOrder.Email;
            this.Products = new List<OrderProduct>();
            this.Status = webshopOrder.Status.ToString().ToUpper();

            foreach(var webShopProduct in webshopOrder.SubOrders)
            {
                OrderProduct orderProd = new OrderProduct
                {
                    Amount = webShopProduct.Amount,
                    Id = (int)webShopProduct.ProductListId,
                    Price = (int)webShopProduct.Price * 100,
                    Priority = webShopProduct.Priority,
                    Title = webShopProduct.Destination,
                    Weight = webShopProduct.Weight
                };
                this.Products.Add(orderProd);
            }
        }

        public Order(string email, Product product, int amount, string priority)
        {
            this.SalesDate = Library.ToUnixTime(DateTime.Now);
            this.ExpireDate = Library.ToUnixTime(DateTime.Today.AddDays(Orders.PostageMobileValidInDaysInApp));

            this.Email = email.ToLower();
            this.Products = new List<OrderProduct>();
            OrderProduct orderProd = new OrderProduct(product, priority, amount);
            this.Products.Add(orderProd);
            this.Saldo = orderProd.ProductSum;
            this.Status = string.Empty;
            this.ErrorCode = string.Empty;
        }

        public long Id
        {
            get;
            set;
        }

        public long SalesDate
        {
            get;
            set;
        }

        public long ExpireDate
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }
        public string Status
        {
            get;
            set;
        }
        public int Saldo
        {
            get {
                int saldo = 0;
                foreach(var orderProduct in this.Products)
                {
                    saldo += orderProduct.ProductSum;
                }
                return saldo;
            }
            set { }
        }

        public List<OrderProduct> Products
        {
            get;
            set;
        }

        public PaymentMethods PaymentMethods
        {
            get;
            set;
        }

        public string ErrorCode 
        {
            get;
            set;
        }

        public string ErrorDescription
        {
            get;
            set;
        }
    }
}
