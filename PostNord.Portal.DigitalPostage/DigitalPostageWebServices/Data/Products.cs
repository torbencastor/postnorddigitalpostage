﻿using PostNord.Portal.DigitalPostage.Bll;
using PostNord.Portal.DigitalPostage.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class ProductsInfo
    {
        public int Version
        {
            get;
            set;
        }
        public List<Category> Categories
        {
            get;
            set;
        }

        public List<Text> Texts
        {
            get;
            set;
        }

        public Message Message
        {
            get;
            set;
        }

        internal ProductsInfo()
        {
            this.Version = 0;
            this.ErrorCode = string.Empty;
            this.Categories = new List<Category>();
        }

        public ProductsInfo(int version, string platform)
        {
            this.Version = version;
            this.ErrorCode = string.Empty;
            this.Categories = new List<Category>();
            this.Texts = new List<Text>();


            if (version >= 50000) TextSettings.ListName = "TextSettingsV5"; 

            Hashtable appTexts = TextSettings.GetValuesWithPrefix("APP_");
            foreach (var text in appTexts.Keys)
            {
                this.Texts.Add(new Text(text.ToString(), appTexts[text].ToString()));
            }

            if (platform != string.Empty)
            {
                var item = Messages.GetMessage(platform);
                if (item != null)
                {
                    this.Message = new Message(item.Title, string.Empty);
                    if (item["Message"] != null)
                    {
                        this.Message.Text = item["Message"].ToString();
                    }
                }
            }
        }
        
        public string ErrorCode
        {
            get;
            set;
        }
        public string ErrorDescription
        {
            get;
            set;
        }
    }
}