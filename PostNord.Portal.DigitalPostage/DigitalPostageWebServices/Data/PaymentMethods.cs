﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class PaymentMethods
    {
        /// <summary>
        /// Gets or sets a value indicating whether [nets].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [nets]; otherwise, <c>false</c>.
        /// </value>
        public bool Nets
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [mobile pay].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [mobile pay]; otherwise, <c>false</c>.
        /// </value>
        public bool MobilePay
        {
            get;
            set;
        }
    }
}
