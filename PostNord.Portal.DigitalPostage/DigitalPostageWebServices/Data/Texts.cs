﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class Text
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlText]
        public string Value { get; set; }

        public Text()
        {
        }

        public Text(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }
    }
}
