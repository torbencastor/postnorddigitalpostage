﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    /// <summary>
    /// OrderStatus is used for XML transformation
    /// </summary>
    public class OrderStatus
    {

        #region Properties

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sales date.
        /// </summary>
        /// <value>
        /// The sales date.
        /// </value>
        public long SalesDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the expire date.
        /// </summary>
        /// <value>
        /// The expire date.
        /// </value>
        public long ExpireDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the codes.
        /// </summary>
        /// <value>
        /// The codes.
        /// </value>
        public List<SubOrder> SubOrders
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the credit card expiry date.
        /// </summary>
        /// <value>
        /// The credit card expiry date.
        /// </value>
        public string CreditCardExpiryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the credit card issuer.
        /// </summary>
        /// <value>
        /// The credit card issuer.
        /// </value>
        public string CreditCardIssuer
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the credit card scrambled data.
        /// </summary>
        /// <value>
        /// The credit card scrambled data.
        /// </value>
        public string CreditCardScrambledData
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the credit card hash.
        /// </summary>
        /// <value>
        /// The credit card hash.
        /// </value>
        public string CreditCardHash
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        public string ErrorCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the error description.
        /// </summary>
        /// <value>
        /// The error description.
        /// </value>
        public string ErrorDescription
        {
            get;
            set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderStatus" /> class.
        /// </summary>
        public OrderStatus()
        {
            this.SubOrders = new List<SubOrder>();
        }

/*
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderStatus"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="salesDate">The sales date.</param>
        /// <param name="expireDate">The expire date.</param>
        /// <param name="creditCardExpiryDate">The credit card expiry date.</param>
        /// <param name="issuer">The issuer.</param>
        /// <param name="scrambledData">The scrambled data.</param>
        /// <param name="creditHash">The credit hash.</param>
        public OrderStatus(long id, long salesDate, long expireDate, string creditCardExpiryDate, string issuer, string scrambledData, string creditHash)
        {
            this.Id = id;
            this.SalesDate = salesDate;
            this.ExpireDate = expireDate;
            this.CreditCardExpiryDate = creditCardExpiryDate;
            this.CreditCardIssuer = issuer;
            this.CreditCardScrambledData = scrambledData;
            this.CreditCardHash = creditHash;
            this.ErrorCode = string.Empty;
            this.SubOrders = new List<SubOrder>();
        }
*/
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderStatus"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="salesDate">The sales date.</param>
        /// <param name="expireDate">The expire date.</param>
        public OrderStatus(long id, long salesDate, long expireDate)
        {
            this.Id = id;
            this.SalesDate = salesDate;
            this.ExpireDate = expireDate;
            this.ErrorCode = string.Empty;
            this.SubOrders = new List<SubOrder>();
        }
        #endregion
    }
}
