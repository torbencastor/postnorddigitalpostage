﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostNord.Portal.DigitalPostageWS.Data
{
    public class Product
    {
        public int Id
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }
        public int Weight
        {
            get;
            set;
        }
        public int APrice
        {
            get;
            set;
        }
        public int BPrice
        {
            get;
            set;
        }
        public long FromDate
        {
            get;
            set;
        }
        public long ToDate
        {
            get;
            set;
        }
        public string Category
        {
            get;
            set;
        }

        public int AVAT
        {
            get;
            set;
        }

        public int APriceWithoutVAT
        {
            get;
            set;
        }
        

        /// <summary>
        /// Gets the price used by WebService clients
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException"></exception>
        public int GetPrice(string priority)
        {
            if (priority == "A")
            {
                return this.APrice;
            }
            if (priority == "B")
            {
                return this.BPrice;
            }
            throw new ApplicationException(string.Format("Priority: {0} was out of range.", priority));
        }

        /// <summary>
        /// Gets the price used by the business layer
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <returns></returns>
        public double GetTruePrice(string priority)
        {
            return ((double)this.GetPrice(priority)) / 100;
        }

        internal Product()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="weight">The weight.</param>
        /// <param name="aPrice">A price.</param>
        /// <param name="bPrice">The b price.</param>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="category">The category.</param>
        public Product(int id, string title, int weight, double aPrice, double bPrice, DateTime fromDate, DateTime toDate, string category, double VAT)
        {
            this.Id = id;
            this.Title = title;
            this.Weight = weight;
            this.APrice = (int) (aPrice * 100);
            this.BPrice = (int) (bPrice * 100);
            this.AVAT = (int) VAT;
            this.APriceWithoutVAT = (int) (((double)this.APrice / (double)this.AVAT) * (double)100);
            this.Category = category;
            this.FromDate = Library.ToUnixTime(fromDate);
            if (toDate != DateTime.MinValue)
                this.ToDate = Library.ToUnixTime(toDate.AddHours(23).AddMinutes(59).AddSeconds(59));
            else
                this.ToDate = Library.ToUnixTime(toDate);
        }
    }
}