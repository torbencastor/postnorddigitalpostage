﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

public class Library
{
   

    /// <summary>
    /// Validates the email.
    /// </summary>
    /// <param name="emailaddress">The emailaddress.</param>
    /// <returns>True if valid false if invalid</returns>
    public static bool ValidateEmail(string emailaddress)
    {
        // Return true if strIn is in valid e-mail format.
        return Regex.IsMatch(emailaddress, @"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$");
    }

    /// <summary>
    /// Convert Unix based Epoch timestamp into DateTime
    /// </summary>
    /// <param name="unixTime">Epoch based timestamp to convert. Definition: Number of seconds since Epoch (1970-01-01 00:00.00)</param>
    /// <returns>DateTime object</returns>
    public static DateTime FromUnixTime(long unixTime)
    {
        if (unixTime < 0)
        {
            return DateTime.MinValue;
        }
        var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        return epoch.AddSeconds(unixTime);
    }

    /// <summary>
    /// Convert DateTime object to Unix based Epoch timestamp
    /// </summary>
    /// <param name="date">DateTime object to convert</param>
    /// <returns>Epoch based timestamp. Definition: Number of seconds since Epoch (1970-01-01 00:00.00)</returns>
    public static long ToUnixTime(DateTime date)
    {
        if (date == null || date == DateTime.MinValue)
        {
            return -1;
        }
        var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        return Convert.ToInt64((date.ToUniversalTime() - epoch).TotalSeconds);
    }

    /// <summary>
    /// Converts the price to øre
    /// </summary>
    /// <param name="price">The price in øre</param>
    /// <returns></returns>
    public static int ConvertPrice(double price)
    {
        return Convert.ToInt32(price * 100);
    }

}