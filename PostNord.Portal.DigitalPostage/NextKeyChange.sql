﻿
--
-- Dropping stored procedure NextKeyInsertRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[NextKey]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
    DROP TABLE [dbo].[NextKey]
  
GO


/****** Object:  Table [dbo].[NextKey]    Script Date: 03/09/2014 06:49:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NextKey](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CodeId] [int] NOT NULL,
	[Price] [float] NOT NULL,
 CONSTRAINT [PK_NextKey] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



--
-- Dropping stored procedure NextKeyInsertRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[NextKeyInsertRow]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[NextKeyInsertRow]
  
GO
-- ==========================================================================================
-- Entity Name: NextKeyInsertRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 3/9/2014 5:53:45 AM
-- Description: This SP Inserts value to NextKey table
-- ==========================================================================================


Create Procedure NextKeyInsertRow
@Price float,
@CodeId int
AS
BEGIN

    INSERT INTO NextKey
        ([CodeId], [Price])
	OUTPUT INSERTED.CodeId 
    VALUES
        (@CodeId, @Price)
END

GO



--
-- Dropping stored procedure NextKeyUpdateRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[NextKeyUpdateKey]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[NextKeyUpdateKey]
  
GO
-- ==========================================================================================
-- Entity Name: NextKeyUpdateRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 3/9/2014 5:53:45 AM
-- Description: This SP updates NextKey table rows.
-- ==========================================================================================


Create Procedure NextKeyUpdateKey
@Price float,
@CodeCount int

AS
BEGIN

UPDATE NextKey SET [CodeId] = [CodeId] + @CodeCount 
OUTPUT INSERTED.CodeId 
WHERE [Price] = @Price

END

GO



--
-- Dropping stored procedure NextKeySelectRow : 
--

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[NextKeySelectRow]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[NextKeySelectRow]
  
GO
-- ==========================================================================================
-- Entity Name: NextKeySelectRow
-- Author:  Mikael Lindberg Mortensen
-- Create date: 3/9/2014 5:53:45 AM
-- Description: This SP select a specify row from NextKey
-- ==========================================================================================


Create Procedure NextKeySelectRow
@Price float
AS
BEGIN

SELECT CodeId FROM NextKey 
WHERE [Price] = @Price

END

GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[NextKeyUpdateRow]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [dbo].[NextKeyUpdateRow]
  
GO


CREATE Procedure [dbo].[NextKeyUpdateRow]
@Price float,
@CodeId int
AS
BEGIN

UPDATE NextKey SET [CodeId] = @CodeId OUTPUT INSERTED.CodeId 
WHERE [Price] = @Price

END

Grant EXECUTE On [NextKeySelectRow] To [DigitalPostageTest]
Grant EXECUTE On [NextKeyUpdateKey] To [DigitalPostageTest]
Grant EXECUTE On [NextKeyInsertRow] To [DigitalPostageTest]
Grant EXECUTE On [NextKeyUpdateRow] To [DigitalPostageTest]
GO
