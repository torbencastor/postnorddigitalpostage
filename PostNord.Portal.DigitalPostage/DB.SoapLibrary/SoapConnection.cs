﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using DB.SoapLibrary.WCF;

namespace DB.SoapLibrary
{
    public class SoapConnection<TClient, TProxy>
        where TClient : ClientBase<TProxy>
        where TProxy : class
    {
        private const string DnsIdentity = "DPCRYPT";

        private readonly TClient client;

        private readonly string authorizedClientConfiguration;

        public delegate TOutput InvokeProxy<out TOutput>(TClient client);

        public SoapConnection(string authorizedClientConfiguration, string address)
        {
            this.authorizedClientConfiguration = authorizedClientConfiguration;
            
            Binding binding;
            if (address.StartsWith("http://"))
            {
                binding = DanskeBankBinding.CreateCustomBinding(Protocol.Http);
            }
            else if (address.StartsWith("https://"))
            {
                binding = DanskeBankBinding.CreateCustomBinding(Protocol.Https);
            }
            else
            {
                throw new InvalidOperationException("Expected protocol http:// or https://");
            }
  
            var endpointAddress = new EndpointAddress(
                new Uri(address),
                new DnsEndpointIdentity(DnsIdentity));

            client = (TClient)Activator.CreateInstance(typeof(TClient), binding, endpointAddress);

            try
            {
                // Remove VsDebuggerCausalityData element from soap header
                var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                client.Endpoint.Behaviors.Remove(vs.Single());
            }
            catch { }
        }

        public TOutput Send<TOutput>(InvokeProxy<TOutput> invokeProxy)
        {
            client.Endpoint.Behaviors.Add(new InspectorBehavior(authorizedClientConfiguration));
            var result = invokeProxy(client);
            client.Close();

            return result;
        }
    }
}
