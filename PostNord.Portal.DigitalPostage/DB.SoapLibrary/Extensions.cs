﻿
using System;

namespace DB.SoapLibrary
{
    public static class Extensions
    {
        public static string DBFormat(this DateTime dateTime)
        {
            return dateTime.AddHours(-1).ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
    }
}
