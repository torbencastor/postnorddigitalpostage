﻿using System;
using System.Configuration;
using System.Linq;

namespace DB.SoapLibrary.Configuration.SoapEnvelope
{
    public class SoapEnvelopesSection : ConfigurationSection
    {
        const string DebugTag = "debug";

        [ConfigurationProperty(DebugTag, IsRequired = true)]
        public bool Debug
        {
            get { return (bool)base[DebugTag]; }
        }

        [ConfigurationProperty("", IsDefaultCollection = true)]
        public SoapEnvelopeElementCollection Elements
        {
            get { return (SoapEnvelopeElementCollection)base[""]; }
        }

        public SoapEnvelopeElement SoapEnvelopeElement(Direction direction)
        {
            try
            {
                return Elements.Single(client => client.Direction == direction);
            }
            catch (InvalidOperationException ioe)
            {
                throw;
            }
        }

        public ApplyElement ApplyElement(Direction direction, Usage usage)
        {
            try
            {
                return
                    Elements.Single(client => client.Direction == direction)
                        .Elements.Single(apply => apply.Usage == usage);
            }
            catch (InvalidOperationException ioe)
            {
                throw;
            }
        }
    }
}
