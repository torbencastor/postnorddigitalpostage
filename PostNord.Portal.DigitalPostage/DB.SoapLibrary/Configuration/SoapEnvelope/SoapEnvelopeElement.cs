﻿using System;
using System.Configuration;
using System.Linq;

namespace DB.SoapLibrary.Configuration.SoapEnvelope
{
    public class SoapEnvelopeElement : ConfigurationElement
    {
        const string DirectionTag = "direction";
        const string PathTag = "path";

        [ConfigurationProperty(DirectionTag, IsRequired = true)]
        public Direction Direction
        {
            get { return (Direction)base[DirectionTag]; }
        }

        [ConfigurationProperty(PathTag, IsRequired = true)]
        public string Path
        {
            get { return (string)base[PathTag]; }
        }

        [ConfigurationProperty("", IsDefaultCollection = true)]
        public ApplyElementCollection Elements
        {
            get { return (ApplyElementCollection)base[""]; }
        }

        public ApplyElement ApplyElement(Usage usage)
        {
            try
            {
                return Elements.Single(apply => apply.Usage == usage);
            }
            catch (InvalidOperationException ioe)
            {
                throw;
            }
        }
    }

    public enum Direction
    {
        Outgoing,
        Incoming
    }
}
