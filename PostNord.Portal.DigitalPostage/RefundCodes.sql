﻿GO
/****** Object:  StoredProcedure [dbo].[CodesSelectForSubOrder]    Script Date: 03/31/2014 14:26:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name: CodesSelectForRefund
-- Author:  Mikael Lindberg Mortensen, Enabling
-- Create date: 31-3-2015 15:11:56
-- Description: Select rows from Codes that matches Refund
-- ==========================================================================================



CREATE Procedure [dbo].[CodesSelectForRefund]
@RefundId as bigint
AS
BEGIN

SELECT * FROM Codes WHERE RefundId=@RefundId

END

GO

Grant EXECUTE On [CodesSelectForRefund] To [DigitalPostageTest]
GO