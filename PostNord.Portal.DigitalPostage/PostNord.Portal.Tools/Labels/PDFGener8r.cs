﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.IO;

namespace PostNord.Portal.Tools.Labels
{
    public class LabelCreator
    {
        /// <summary>
        /// Holds the information regarding the product
        /// </summary>
        public struct ProductInfo
        {
            public string Code;
            public decimal Price;
            public DateTime ExpiryDate;
            public string OverlayText;
            public string ProductName;
        }

        /// <summary>
        /// Type of the Placeholder taken from the template
        /// </summary>
        private enum PlaceHolderType
        {
            Background,
            Border,
            Fixed,
            Image,
            Overlay,
            OverlayText,
            Text
        }

        /// <summary>
        /// Information regarding the Placeholder used for generating the Label sheet
        /// </summary>
        private struct PlaceHolder
        {
            public XRect Placement;
            public string Text;
            public byte Z_Order;
            public PlaceHolderType Type;
            public XFont Font;
            public XStringFormat Alignment;
        }

        #region Private Variables
        private XUnit labelHeight;
        private XUnit labelWidth;
        private XUnit labelSpacingHor;
        private XUnit labelSpacingVer;
        private XUnit leftMargin;
        private XUnit topMargin;
        private int cols;
        private int rows;
        private int labelsPerSheet;
        private Dictionary<string, PlaceHolder> placeHolders;
        private XImage labelImage;
        #endregion Private Variables

        
        /// <summary>
        /// Creates a PDF generator object
        /// </summary>
        /// <param name="template">Object describing the Label</param>

        public LabelCreator(LabelTemplate template):this(template, null)
        {
        }

        /// <summary>
        /// Creates a PDF generator object
        /// </summary>
        /// <param name="template">Object describing the Label</param>
        /// <param name="img">Image to use on the label</param>
        public LabelCreator(LabelTemplate template, System.Drawing.Image img)
        {
            this.placeHolders = new Dictionary<string, PlaceHolder>();

            this.labelWidth = Milimeters2Units(template.LabelInfo.Width);
            this.labelHeight = Milimeters2Units(template.LabelInfo.Height);

            this.labelSpacingHor = Milimeters2Units(template.Margins.HorizontalSpacing);
            this.labelSpacingVer = Milimeters2Units(template.Margins.VerticalSpacing);

            this.leftMargin = Milimeters2Units(template.Margins.Horizontal);
            this.topMargin = Milimeters2Units(template.Margins.Vertical);

            this.cols = template.Labels.Columns;
            this.rows = template.Labels.Rows;

            this.labelsPerSheet = this.cols * this.rows;

            if (img != null)
            {
                labelImage = XImage.FromGdiPlusImage(img);
            }

            if (template.PlaceHolders != null)
            {
                foreach (LabelTemplatePlaceHolder placeHolder in template.PlaceHolders)
                {
                    XUnit left = new XUnit((double)placeHolder.Position.x, XGraphicsUnit.Millimeter);
                    XUnit top = new XUnit((double)placeHolder.Position.y, XGraphicsUnit.Millimeter);
                    XUnit width = new XUnit((double)placeHolder.Size.width, XGraphicsUnit.Millimeter);
                    XUnit height = new XUnit((double)placeHolder.Size.height, XGraphicsUnit.Millimeter);

                    PlaceHolder holder = new PlaceHolder();
                    holder.Placement = new XRect(left.Point, top.Point, width.Point, height.Point);
                    holder.Z_Order = placeHolder.Position.z;

                    if (placeHolder.TextFont != null)
                    {
                        holder.Font = new XFont(placeHolder.TextFont.family, (double)placeHolder.TextFont.size, placeHolder.TextFont.bold ? XFontStyle.Bold : XFontStyle.Regular);
                    }

                    holder.Type = PlaceHolderType.Text;
                    holder.Text = placeHolder.TextValue;

                    string typ = placeHolder.type.ToLower();

                    if (typ == "image")
                    {
                        holder.Type = PlaceHolderType.Image;
                    }
                    else if (typ == "fixed")
                    {
                        holder.Type = PlaceHolderType.Fixed;
                    }
                    else if (typ == "background")
                    {
                        holder.Type = PlaceHolderType.Background;
                    }
                    else if (typ == "border")
                    {
                        holder.Type = PlaceHolderType.Border;
                    }
                    else if ( typ == "overlay")
                    {
                        holder.Type = PlaceHolderType.Overlay;
                    }
                    else if ( typ == "overlaytext")
                    {
                        holder.Type = PlaceHolderType.OverlayText;
                    }

                    string placeHolderID = placeHolder.PlaceHolderID;

                    if (string.IsNullOrEmpty(placeHolderID))
                    {
                        placeHolderID = Guid.NewGuid().ToString();
                    }

                    holder.Alignment = XStringFormats.Center;

                    if (!string.IsNullOrEmpty(placeHolder.Alignment))
                    {
                        string alignment = placeHolder.Alignment.ToLower();

                        if ( alignment == "left")
                        {
                            holder.Alignment = XStringFormats.TopLeft;
                        }
                    }

                    this.placeHolders[placeHolderID] = holder;
                }
            }
        }

        /// <summary>
        /// Creates a PDF document containing all the products in a single document
        /// </summary>
        /// <param name="filename">Name of the file to save the sheets as.</param>
        /// <param name="products">List of products to create.</param>
        public void CreateLabelSheets(string filename, ProductInfo[] products)
        {
            using (PdfDocument pdfDoc = new PdfDocument())
            {
                int colIndex = 0;
                int rowIndex = 0;

                PdfPage page = null;
                XGraphics gfx = null;

                int count = products.Length;

                foreach (ProductInfo info in products)
                {
                    if (colIndex >= this.cols)
                    {
                        colIndex = 0;
                        rowIndex++;
                    }

                    if (rowIndex >= this.rows)
                    {
                        colIndex = 0;
                        rowIndex = 0;
                    }

                    if ((colIndex == 0) && (rowIndex == 0))
                    {
                        if (gfx != null)
                        {
                            gfx.Dispose();
                        }

                        page = pdfDoc.AddPage();
                        page.Size = PdfSharp.PageSize.A4;
                        gfx = XGraphics.FromPdfPage(page);
                    }
                    CreateLabel(gfx, info, colIndex++, rowIndex);
                }

                if (gfx != null)
                {
                    gfx.Dispose();
                }

                pdfDoc.Save(filename);
                pdfDoc.Close();
            }
        }

        /// <summary>
        /// Creates a PDF document containing all the products in a single document
        /// </summary>
        /// <param name="dataStream">Stream to save data to.</param>
        /// <param name="products">List of products to create.</param>
        public byte[] CreateLabelSheets(ProductInfo[] products)
        {
            using (PdfDocument pdfDoc = new PdfDocument())
            {
                int colIndex = 0;
                int rowIndex = 0;

                PdfPage page = null;
                XGraphics gfx = null;

                int count = products.Length;

                foreach (ProductInfo info in products)
                {
                    if (colIndex >= this.cols)
                    {
                        colIndex = 0;
                        rowIndex++;
                    }

                    if (rowIndex >= this.rows)
                    {
                        colIndex = 0;
                        rowIndex = 0;
                    }

                    if ((colIndex == 0) && (rowIndex == 0))
                    {
                        if (gfx != null)
                        {
                            gfx.Dispose();
                        }

                        page = pdfDoc.AddPage();
                        page.Size = PdfSharp.PageSize.A4;
                        gfx = XGraphics.FromPdfPage(page);
                    }
                    CreateLabel(gfx, info, colIndex++, rowIndex);
                }

                if (gfx != null)
                {
                    gfx.Dispose();
                }

                byte[] data = null;
                using (MemoryStream dataStream = new MemoryStream())
                {
                    pdfDoc.Save(dataStream, false);
                    pdfDoc.Close();

                    data = dataStream.ToArray();
                }
                return data;
            }
        }

        /// <summary>
        /// Creates a single label based on the row and column indexes
        /// </summary>
        /// <param name="gfx">Graphic unit to use</param>
        /// <param name="info">Product info</param>
        /// <param name="colIndex">Index of the column</param>
        /// <param name="rowIndex">Index of the row</param>
        private void CreateLabel(XGraphics gfx, ProductInfo info, int colIndex, int rowIndex)
        {
            // Calculate the coordinates of the label
            XUnit offsetHor = colIndex * (this.labelWidth + this.labelSpacingHor);

            XUnit left = this.leftMargin + offsetHor;

            XUnit offsetVer = rowIndex * (this.labelHeight + this.labelSpacingVer);
            XUnit top = this.topMargin + offsetVer;

            XPen borderPen = new XPen(new XColor() { R = 0, G = 255, B = 255 }, 0.5);

            // Calculate the absolute coordinates of the label
            XRect label = new XRect(left.Point, top.Point, this.labelWidth.Point, this.labelHeight.Point);

            if ( this.labelImage != null )
            {
                PrintPlaceHolder(gfx, label, "Image");
            }

            string price = string.Format("{0} kr.", info.Price);
            // If decimal, make sure we have 2 digits.
            if (System.Math.Round(info.Price) != info.Price)
            {
                price = string.Format(new System.Globalization.CultureInfo("da-DK"),"{0:C} kr.", info.Price);
                price = price.Replace("kr ", ""); // dirty hack since we are launching shortly
            } 

            //PrintPlaceHolder(gfx, label, info.Code, "Code");
            PrintCode(gfx, label, info.Code);
            PrintPlaceHolder(gfx, label, price, "Price");
            PrintPlaceHolder(gfx, label, info.ExpiryDate.ToString("dd.MM.yyyy"), "Expire");
            PrintFixedTexts(gfx, label);
            PrintBorders(gfx, label);
            PrintOverlays(gfx, label);
            PrintPlaceHolder(gfx, label, info.OverlayText, "PostType");
            PrintPlaceHolder(gfx, label, info.ProductName, "ProductName");
        }

        #region Private Helper Functions

        /// <summary>
        /// Prints the borders
        /// </summary>
        /// <param name="gfx">Graphics component to use</param>
        /// <param name="label">Current label</param>
        private void PrintBorders(XGraphics gfx, XRect label)
        {
            var borders = from place in this.placeHolders where place.Value.Type == PlaceHolderType.Border select place.Value;

            foreach (PlaceHolder holder in borders)
            {
                PrintBorder(gfx, label, holder);
            }
        }

        /// <summary>
        /// Prints the fixed texts on to the label
        /// </summary>
        /// <param name="gfx">Graphics object to use</param>
        /// <param name="label">Current label</param>
        private void PrintFixedTexts(XGraphics gfx, XRect label)
        {
            var fixedTexts = from place in this.placeHolders where place.Value.Type == PlaceHolderType.Fixed select place.Value;

            foreach (PlaceHolder holder in fixedTexts)
            {
                PrintText(gfx, label, holder);
            }
        }


        /// <summary>
        /// Prints the Content of the placeholder
        /// </summary>
        /// <param name="gfx">Graphics component to use</param>
        /// <param name="label">Position and size of the label</param>
        /// <param name="placeHolder">Place holder of the fixed texts</param>
        private void PrintPlaceHolder(XGraphics gfx, XRect label, PlaceHolder placeHolder)
        {
            if (placeHolder.Type == PlaceHolderType.Text || 
                placeHolder.Type == PlaceHolderType.Fixed ||
                placeHolder.Type == PlaceHolderType.OverlayText)
            {
                PrintText(gfx, label, placeHolder);
            }
            else if (placeHolder.Type == PlaceHolderType.Image ||
                placeHolder.Type == PlaceHolderType.Background ||
                placeHolder.Type == PlaceHolderType.Overlay)
            {
                PrintImage(gfx, label, placeHolder);
            }
        }

        /// <summary>
        /// Prints the Content of the placeholder
        /// </summary>
        /// <param name="gfx">Graphics component to use</param>
        /// <param name="label">Position and size of the label</param>
        /// <param name="placeHolderID">ID of the placeholder to use</param>
        private void PrintPlaceHolder(XGraphics gfx, XRect label, string placeHolderID)
        {
            PrintPlaceHolder(gfx, label, null, placeHolderID);
        }

        /// <summary>
        /// Prints the Content of the placeholder
        /// </summary>
        /// <param name="gfx">Graphics component to use</param>
        /// <param name="label">Position and size of the label</param>
        /// <param name="info">Information to insert</param>
        /// <param name="placeHolderID">ID of the placeholder to use</param>
        private void PrintPlaceHolder(XGraphics gfx, XRect label, string info, string placeHolderID)
        {
            // Exit if the List of place holders does not contain the desired place holder.
            if (!this.placeHolders.ContainsKey(placeHolderID))
            {
                return;
            }

            PlaceHolder placeHolder = this.placeHolders[placeHolderID];

            if (!string.IsNullOrEmpty(info))
            {
                placeHolder.Text = info;
            }

            PrintPlaceHolder(gfx, label, placeHolder);
        }


        /// <summary>
        /// Prints a text to the provided placeholder
        /// </summary>
        /// <param name="gfx">Graphics component to use</param>
        /// <param name="label">Current label</param>
        /// <param name="placeHolder">Placeholder</param>
        private void PrintCode(XGraphics gfx, XRect label, string code)
        {
            PlaceHolder placeHolder = this.placeHolders["Code"];

            if (!string.IsNullOrEmpty(code))
            {
                placeHolder.Text = code;
            }

            XRect placeHolderPlacement = new XRect(
                label.Left + placeHolder.Placement.Left,
                label.Top + placeHolder.Placement.Top,
                placeHolder.Placement.Size.Width,
                placeHolder.Placement.Size.Height);

            double fontHeight = placeHolder.Font.Height;

            string info = code.Replace(" ", "");

            if (string.IsNullOrEmpty(info))
            {
                return;
            }

            if (info.Contains("\n"))
            {
                string[] lines = info.Split(new string[] { "\n" }, StringSplitOptions.None);
                int lineCount = lines.Length;

                double txtHeight = lineCount * fontHeight;
                double txtOffset = (placeHolder.Placement.Height - txtHeight) / 2;

                for (int n = 0; n < lineCount; n++)
                {
                    string line = lines[n];
                    if (line == string.Empty)
                    {
                        break;
                    }
                    for (int i = 0; i < line.Length; i++)
                    {
                        string chr = line.Substring(i,1);
                        if (chr == "\r")
                        {
                            break;
                        }
                        XRect linePlaceHolderPlacement = new XRect(
                            label.Left + placeHolder.Placement.Left + (placeHolder.Placement.Size.Width * i / 4),
                            label.Top + placeHolder.Placement.Top + txtOffset + ((fontHeight + 1) * n),
                            placeHolder.Placement.Size.Width / 4,
                            fontHeight);
                        gfx.DrawString(chr, placeHolder.Font, XBrushes.Black, linePlaceHolderPlacement, placeHolder.Alignment);
                    }
                }
            }
            else
            {
                gfx.DrawString(info, placeHolder.Font, XBrushes.Black, placeHolderPlacement, placeHolder.Alignment);
            }

#if DEBUG
            //PrintBorder(gfx, placeHolderPlacement);
#endif
        }


        /// <summary>
        /// Prints a text to the provided placeholder
        /// </summary>
        /// <param name="gfx">Graphics component to use</param>
        /// <param name="label">Current label</param>
        /// <param name="placeHolder">Placeholder</param>
        private void PrintText(XGraphics gfx, XRect label, PlaceHolder placeHolder)
        {
            XRect placeHolderPlacement = new XRect(
                label.Left + placeHolder.Placement.Left,
                label.Top + placeHolder.Placement.Top,
                placeHolder.Placement.Size.Width,
                placeHolder.Placement.Size.Height);

            double fontHeight = placeHolder.Font.Height;

            string info = placeHolder.Text;

            if (string.IsNullOrEmpty(info))
            {
                return;
            }

            if (info.Contains("\n"))
            {
                string[] lines = info.Split(new string[] { "\n" }, StringSplitOptions.None);
                int lineCount = lines.Length;

                double txtHeight = lineCount * fontHeight;
                double txtOffset = (placeHolder.Placement.Height - txtHeight) / 2;

                for (int n = 0; n < lineCount; n++)
                {
                    XRect linePlaceHolderPlacement = new XRect(
                        label.Left + placeHolder.Placement.Left,
                        label.Top + placeHolder.Placement.Top + txtOffset + ((fontHeight + 1) * n),
                        placeHolder.Placement.Size.Width,
                        fontHeight);
                    gfx.DrawString(lines[n], placeHolder.Font, XBrushes.Black, linePlaceHolderPlacement, placeHolder.Alignment);
                }
            }
            else
            {
                gfx.DrawString(info, placeHolder.Font, XBrushes.Black, placeHolderPlacement, placeHolder.Alignment);
            }

#if DEBUG
            //PrintBorder(gfx, placeHolderPlacement);
#endif
        }

        /// <summary>
        /// Prints image on label
        /// </summary>
        /// <param name="gfx">Graphics component to use</param>
        /// <param name="label">Current label</param>
        /// <param name="placeHolder">Placeholder to use</param>
        private void PrintImage(XGraphics gfx, XRect label, PlaceHolder placeHolder)
        {
            XRect placeHolderPlacement = new XRect(
            label.Left + placeHolder.Placement.Left,
            label.Top + placeHolder.Placement.Top,
            placeHolder.Placement.Size.Width,
            placeHolder.Placement.Size.Height);

            XUnit imageWidth = new XUnit(labelImage.PointWidth, XGraphicsUnit.Point);
            XUnit imageHeight = new XUnit(labelImage.PointHeight, XGraphicsUnit.Point);

            if ( imageWidth > placeHolderPlacement.Width)
            {
                imageWidth = placeHolderPlacement.Width;
            }

            if ( imageHeight > placeHolderPlacement.Height)
            {
                imageHeight = placeHolderPlacement.Height;
            }

            XRect imagePlaceHolder = new XRect(
            label.Left + placeHolder.Placement.Left + (placeHolderPlacement.Width - imageWidth)/2,
            label.Top + placeHolder.Placement.Top + (placeHolderPlacement.Height - imageHeight)/2,
            imageWidth,
            imageHeight);

            gfx.DrawImage(labelImage, imagePlaceHolder);
        }

        /// <summary>
        /// Prints border
        /// </summary>
        /// <param name="gfx">Graphics component to use</param>
        /// <param name="label">Current label</param>
        /// <param name="placeHolder">Placeholder to use</param>
        private void PrintBorder(XGraphics gfx, XRect label, PlaceHolder placeHolder)
        {
            XRect placeHolderPlacement = new XRect(
            label.Left + placeHolder.Placement.Left,
            label.Top + placeHolder.Placement.Top,
            placeHolder.Placement.Size.Width,
            placeHolder.Placement.Size.Height);

            PrintBorder(gfx, placeHolderPlacement);
        }

        /// <summary>
        /// Prints the border
        /// </summary>
        /// <param name="gfx">Graphics Component to use</param>
        /// <param name="placement">Surrounding box</param>
        private void PrintBorder(XGraphics gfx, XRect placement)
        {
            gfx.DrawRectangle(new XPen(XColor.FromArgb(0), 0.5), placement);
        }

        /// <summary>
        /// Prints the overlay text
        /// </summary>
        /// <param name="gfx">Graphics component to use</param>
        /// <param name="label">Current label</param>
        private void PrintOverlays(XGraphics gfx, XRect label)
        {
            var overlays = from place in this.placeHolders where place.Value.Type == PlaceHolderType.Overlay select place.Value;

            foreach( PlaceHolder overlay in overlays)
            {
                PrintImage(gfx, label, overlay);
            }
        }

        /// <summary>
        /// Converts the units used in the configuration file into XUnits
        /// </summary>
        /// <param name="mils">Dimension in milimeters from file</param>
        /// <returns>Converted unit</returns>
        private XUnit Milimeters2Units(decimal mils)
        {
            return new XUnit((double)mils, XGraphicsUnit.Millimeter);
        }
        #endregion Private Helper Functions
    }
}
