﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace PostNord.Portal
{
    public class CodeWrapper
    {
        const int MAX_SIZE = 200;

        [DllImport(@"c:\innofactor\dlls\dk_mf.dll", CharSet = CharSet.Ansi)]
        private static extern IntPtr dk_mf_get_version_c();

        /// <summary>
        /// Encode the specified code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="data">The data.</param>
        /// <returns>Errorcode. 0 if OK 1 if error</returns>
        [DllImport(@"c:\innofactor\dlls\dk_mf.dll", CharSet = CharSet.Ansi)]
        private static extern int dk_mf_encode_c(StringBuilder code, StringBuilder data);

        /// <summary>
        ///Decode the specified decoded payload data.
        /// </summary>
        /// <param name="decodedPayloadData">The decoded payload data.</param>
        /// <param name="numberOfCorrections">The number of corrections.</param>
        /// <param name="correctedMobileFrankingCode">The corrected mobile franking code.</param>
        /// <param name="code">The code.</param>
        /// <returns>Errorcode. 0 if OK 1 if error</returns>
        [DllImport(@"c:\innofactor\dlls\dk_mf.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int dk_mf_decode_c(StringBuilder decodedPayloadData, out int numberOfCorrections, StringBuilder correctedMobileFrankingCode, StringBuilder code);

        /// <summary>
        /// Gets the code details.
        /// </summary>
        /// <param name="codeVal">The code value.</param>
        /// <param name="day">The day.</param>
        /// <param name="value">The value.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="number">The number.</param>
        /// <returns>
        /// Errorcode. 0 if OK 1 if error
        /// </returns>
        static public int GetCodeDetails(string codeVal, out uint day, out uint value, out float amount, out uint number)
        {
            StringBuilder code = new StringBuilder(codeVal.Replace('-',' '));
            StringBuilder dk_mf_c = new StringBuilder(MAX_SIZE);
            StringBuilder dk_mf = new StringBuilder(MAX_SIZE);

            StringBuilder decodedPayloadData = new StringBuilder(1000);
            StringBuilder correctedMobileFrankingCode = new StringBuilder(1000);
            int numberOfCorrections = 0;
            day = 0;
            value = 0;
            number = 0;
            amount = 0;

            int res = dk_mf_decode_c(decodedPayloadData, out numberOfCorrections, correctedMobileFrankingCode, code);

            if (numberOfCorrections == 0 && res == 1)
            {
                Code c = new Code(decodedPayloadData.ToString());

                day = c.Day;
                value = c.Franking;
                amount = c.FrankingValue;
                number = c.Number;
            }
            return res;
        }

        /// <summary>
        /// Gets multiple postal codes.
        /// </summary>
        /// <param name="day">The day.</param>
        /// <param name="value">The value.</param>
        /// <param name="number">The start number.</param>
        /// <param name="numberOfCodes">The number of codes.</param>
        /// <returns>Array containing the number of codes request</returns>
        static public string[] GetCodes(uint day, uint value, uint number, uint numberOfCodes)
        {
            List<string> codes = new List<string>();
            uint maxNumber = number + numberOfCodes;
            for (uint i = number; i < maxNumber; i++)
            {
                codes.Add(GetCode(day, value, i));
            }
            return codes.ToArray();
        }

        /// <summary>
        /// Gets postal code.
        /// </summary>
        /// <param name="day">The day.</param>
        /// <param name="value">The value.</param>
        /// <param name="number">The number.</param>
        /// <returns></returns>
        static public string GetCode(uint day, uint value, uint number)
        {
            StringBuilder sb = new StringBuilder(MAX_SIZE);
            string parameters = string.Format("{0} {1} {2}", day, value, number);
            int res = dk_mf_encode_c(sb, new StringBuilder(parameters));
            return sb.ToString().Replace(" ", "-");
        }

        /// <summary>
        /// Gets the CodeGenerator version.
        /// </summary>
        /// <returns></returns>
        static public string GetVersion()
        {
            IntPtr o = dk_mf_get_version_c();
            return Marshal.PtrToStringAnsi(o);
        }

        /// <summary>
        /// Gets the hash value for a string.
        /// </summary>
        /// <param name="inputString">The input string.</param>
        /// <returns></returns>
        private static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        /// <summary>
        /// Gets the hash string.
        /// </summary>
        /// <param name="inputString">The input string.</param>
        /// <returns>Hashed string</returns>
        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
