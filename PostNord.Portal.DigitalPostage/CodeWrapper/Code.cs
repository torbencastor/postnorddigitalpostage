﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal
{
    /// <summary>
    /// Day [0..366] Day in year
    ///0 = all days
    ///1 = day 1
    ///...
    ///365 = day 365
    ///366 = in leap years only
    ///Franking [0..500] Franking in 0,50 kr steps
    ///0 = 0,00 kr
    ///1 = 0,50 kr
    ///...
    ///499 = 249,50 kr
    ///500 = 250,00 kr
    ///Data [1..425910] Number
    /// </summary>
    public class Code
    {
        uint _day;
        uint _franking;
        uint _number;

        /// <summary>
        /// Gets or sets the day.
        /// </summary>
        /// <value>
        /// The day.
        /// </value>
        /// <exception cref="System.ArgumentOutOfRangeException">Day max value is 366</exception>
        public uint Day
        {
            get { return _day; }
            set
            {
                if (value > 366)
                {
                    throw new ArgumentOutOfRangeException("Day max value is 366");
                }
                _day = value;
            }
        }

        /// <summary>
        /// Gets or sets the franking.
        /// </summary>
        /// <value>
        /// The franking.
        /// </value>
        /// <exception cref="System.ArgumentOutOfRangeException">Franking max value is 500</exception>
        public uint Franking
        {
            get { return _franking; }
            set
            {
                if (value > 500)
                {
                    throw new ArgumentOutOfRangeException("Franking max value is 500");
                }
                _franking = value;
            }
        }

        /// <summary>
        /// Gets or sets the franking value.
        /// </summary>
        /// <value>
        /// The franking value.
        /// </value>
        public float FrankingValue
        {
            get
            {
                return (float)Franking / 2;
            }
            set
            {
                Franking = (uint)value * 2;
            }
        }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        /// <exception cref="System.ArgumentOutOfRangeException">Number must be in range 1-425910</exception>
        public uint Number
        {
            get { return _number; }
            set
            {
                if (value < 1 || value > 425910)
                {
                    throw new ArgumentOutOfRangeException("Number must be in range 1-425910");
                }
                _number = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Code"/> class.
        /// </summary>
        /// <param name="code">The code represented as string '{Day} {Franking} {Number}'</param>
        /// <exception cref="System.ApplicationException">Number of elements are wrong</exception>
        public Code(string code)
        {
            string[] arr = code.Split(' ');
            if (arr.Length != 3)
            {
                throw new ApplicationException("Number of elements are wrong");
            }

            this.Day = uint.Parse(arr[0]);
            this.Franking = uint.Parse(arr[1]);
            this.Number = uint.Parse(arr[2]);
        }

        public new string ToString()
        {
            return string.Format("{0} {1} {2}", this.Day, this.Franking, this.Number);
        }

    }
}
