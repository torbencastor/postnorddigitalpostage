﻿using iTextSharp.text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using PN.SoapLibrary.WSSecurity;
using PostNord.Portal.DigitalPostage.Bll;
using PostNord.Portal.DigitalPostage.Helpers;
using PostNord.Portal.DigitalPostage.MobilePay;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace PostNord.Portal.DigitalPostage.Utility
{
    /// <summary>
    /// ClanUp Job. Utility to Cancel Unpaid orders and refund orders not capture
    /// </summary>
    class CleanUpJob
    {
        #region Global Vars
        static string _xsltMail = string.Empty;
        static string _xsltMailNets = string.Empty;
        static string _multiPaymentMail = string.Empty;
        static string url = "http://dev";
        static int dayCount = 2;
        static int hourCount = 1;
        static bool doRefund = true;
        static bool recheck = true;
        static int paidCount = 0;
        static int refundCount = 0;
        static int cancelCount = 0;
        static int count = 0;
        static string transactionId = string.Empty;
        static string fileUrl = string.Empty;
        static Parameters _parameters = null;
        static string creditnotaFileMp = "creditNoteHTML.xslt";
        static string creditnotaFileNets = "creditNoteHTMLNETS.xslt";
        static string creditnotaMultipleNets = "CreditNoteMultipleNets.htm";
        #endregion

        #region Program Actions
        
        /// <summary>
        /// Loops through orders according to console parameters and updates status to Cancel / Refund / Credit, according to order status in payment system
        /// </summary>
        /// <returns>Number of orders</returns>
        private static int CancelOrders()
        {
            dayCount = 1;
            hourCount = 1;

            int manualOrderId = 0;
            DateTime manualFromDate = DateTime.MinValue;
            DateTime manualToDate = DateTime.MinValue;

            url = _parameters.GetOptionalParameter("-url");
            if (string.IsNullOrEmpty(url))
            {
                Console.WriteLine("Url is empty");
                return -1;
            }

            if (_parameters.Contains("-daycount"))
            {
                if (!int.TryParse(_parameters.GetOptionalParameter("-daycount"), out dayCount))
                {
                    Console.WriteLine("DayCount: '{0}' is not a number", _parameters.GetOptionalParameter("-daycount"));
                    return -1;
                }
            }

            if (_parameters.Contains("-hourcount"))
            {
                if (!int.TryParse(_parameters.GetOptionalParameter("-hourcount"), out hourCount))
                {
                    Console.WriteLine("HourCount: '{0}' is not a number", _parameters.GetOptionalParameter("-hourcount"));
                    return -1;
                }
            }

            if (_parameters.Contains("-id"))
            {
                if (!int.TryParse(_parameters.GetOptionalParameter("-id"), out manualOrderId))
                {
                    Console.WriteLine("Id: '{0}' is not a number", _parameters.GetOptionalParameter("-hourcount"));
                    return -1;
                }
            }

            if (_parameters.Contains("-fromdate"))
            {
                if (!DateTime.TryParse(_parameters.GetOptionalParameter("-fromdate"), out manualFromDate))
                {
                    Console.WriteLine("FromDate: '{0}' is not a valid date", _parameters.GetOptionalParameter("-fromdate"));
                    return -1;
                }
            }

            if (_parameters.Contains("-todate"))
            {
                if (!DateTime.TryParse(_parameters.GetOptionalParameter("-todate"), out manualToDate))
                {
                    Console.WriteLine("ToDate: '{0}' is not a valid date", _parameters.GetOptionalParameter("-todate"));
                    return -1;
                }
            }


            try
            {
                _xsltMail = System.IO.File.ReadAllText(creditnotaFileMp, Encoding.UTF8);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading MP credit note file: {0} ({1})", creditnotaFileMp, ex.Message);
                return -1;
            }

            try
            {
                _xsltMailNets = System.IO.File.ReadAllText(creditnotaFileNets, Encoding.UTF8);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading NETS credit note file: {0} ({1})", creditnotaFileNets, ex.Message);
                return -1;
            }

            try
            {
                _multiPaymentMail = System.IO.File.ReadAllText(creditnotaMultipleNets, Encoding.UTF8);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading NETS credit note file: {0} ({1})", creditnotaMultipleNets, ex.Message);
            }

            Initialize.LoadConfig(url);

            DateTime firstOrderDate = DateTime.Now.AddDays(-1 * dayCount);
            firstOrderDate = firstOrderDate.AddHours(firstOrderDate.Hour * -1).AddMinutes(firstOrderDate.Minute * -1).AddSeconds(firstOrderDate.Second * -1);
            DateTime lastOrderDate = DateTime.Now.AddHours(-1 * hourCount);

            if (manualFromDate > DateTime.MinValue)
            {
                firstOrderDate = manualFromDate;
            }

            if (manualToDate > DateTime.MinValue)
            {
                lastOrderDate = manualToDate;
            }

            List<Orders> orders = null;

            if (manualOrderId <= 0)
            {
                orders = PostNord.Portal.DigitalPostage.Bll.Orders.GetOrders(firstOrderDate, lastOrderDate);
            }
            else
            {
                try
                {
                    Orders singeOrder = new Orders(manualOrderId);
                    orders = new List<Orders>();
                    orders.Add(singeOrder);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error loading Order with id: {0}, {1}", manualOrderId, ex.Message);
                    return -1;
                }
            }

            int orderCount = orders.Count;
            Console.WriteLine("Total Number of orders: {0}", orderCount);
            int mpOrderCount = orders.Count(order => order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentMobilePay);

            Console.WriteLine("First order date: {0}", firstOrderDate);
            Console.WriteLine("Last order date: {0}", lastOrderDate);

            Console.WriteLine("Number of unfinished MobilePay orders: {0}", mpOrderCount);
            int netsOrderCount = orders.Count(order => (order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPayment ||
                                                        order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentCardSave ||
                                                        order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentCreditCardReused));
            Console.WriteLine("Number of unfinished NETS orders: {0}", netsOrderCount);
            Console.WriteLine();

            if (netsOrderCount == 0 && mpOrderCount == 0)
            {
                Console.WriteLine("Nothing to process");
                return -1;
            }

            var orderList = orders.Where(order => (order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPayment ||
                                                        order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentCardSave ||
                                                        order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentCreditCardReused ||
                                                        order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentMobilePay));

            if (_parameters.Contains("-paymenttype"))
            {
                string paymentType = _parameters.GetOptionalParameter("-paymenttype");

                if (paymentType.ToUpper() == "NETS")
                {
                    orderList = orders.Where(order => (order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPayment ||
                                                                order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentCardSave ||
                                                                order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentCreditCardReused));
                    Console.WriteLine("Payment type: NETS. Number of orders: {0}", netsOrderCount);
                    if (netsOrderCount == 0)
                    {
                        return 0;
                    }
                }
                else
                {
                    orderList = orders.Where(order => (order.Status == PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentMobilePay));
                    Console.WriteLine("Payment type: MobilePay. Number of orders: {0}", mpOrderCount);
                    if (mpOrderCount == 0)
                    {
                        return 0;
                    }
                }
            }

            orderCount = orderList.Count();

            int count = 0;
            int autoUpdatedToCint = 0;
            DateTime startTime = DateTime.Now;

            using (SPSite site = new SPSite(url))
            {
                Guid siteId = site.ID;
                foreach (var order in orderList)
                {
                    if (++count % 100 == 0)
                    {
                        var elapsed = DateTime.Now - startTime;
                        Console.Write("Processing: {0} of {1} - {2}%", count, orderCount, (count * 100 / orderCount));

                        if (elapsed.TotalMinutes >= 60)
                        {
                            Console.WriteLine("Elapsed: {0} hours and {1} minutes", elapsed.Hours, elapsed.Minutes);
                        }
                        else
                        {
                            Console.WriteLine(" Elapsed: {0} minutes", elapsed.Minutes);
                        }
                    }
                    try
                    {
                        switch (order.Status)
                        {
                            case PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Initialized:
                                HandleInitialized(order);
                                break;
                            case PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPayment:
                            case PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentCardSave:
                            case PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentCreditCardReused:
                                if (order.SentToCint)
                                {
                                    if (order.Logs.Where(logLine => logLine.Text.Contains("Register payment. Transaction Id:")).Count() > 1)
                                    {
                                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): IN CINT - Multiple transactions - Skipping", order.Id, order.SalesDate, order.Status);
                                        continue;
                                    }

                                    // Receipt has been sent we update to PAID
                                    if (order.Logs.Where(logLine => logLine.Text.Equals("Receipt sent.")).Count() > 0)
                                    {
                                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): IN CINT - PAID", order.Id, order.SalesDate, order.Status);
                                        order.SetStatus(Orders.OrderStatusValues.Paid);
                                        autoUpdatedToCint++;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): IN CINT - Skipping", order.Id, order.SalesDate, order.Status);
                                    }
                                    continue;
                                }
                                HandleNetsPayment(order, siteId);
                                break;
                            case PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.ReadyForPaymentMobilePay:
                                if (order.SentToCint)
                                {
                                    // Receipt has been sent we update to PAID
                                    if (order.Logs.Where(logLine => logLine.Text.Equals("Receipt sent.")).Count() > 0)
                                    {
                                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): IN CINT - PAID", order.Id, order.SalesDate, order.Status);
                                        order.SetStatus(Orders.OrderStatusValues.Paid);
                                        autoUpdatedToCint++;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): IN CINT - Skipping", order.Id, order.SalesDate, order.Status);
                                    }
                                    continue;
                                }
                                HandleStatusReadyForPaymentMobilePay(order);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Generel error: {0}", ex.Message);
                    }
                    Application.DoEvents();
                    if (Console.KeyAvailable)
                    {
                        ConsoleKeyInfo cki = Console.ReadKey();
                        if (cki.Key == ConsoleKey.Escape)
                        {
                            Console.WriteLine();
                            Console.WriteLine("Number of orders processed: {0}", count);
                            Console.WriteLine("Number of orders updated to paid: {0}", paidCount);
                            Console.WriteLine("Number of orders refunded: {0}", refundCount);
                            Console.WriteLine("Number of orders cancelled: {0}", cancelCount);
                            return -1;
                        }
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine("Number of orders processed: {0}", count);
            Console.WriteLine("Number of orders updated to paid: {0}", paidCount);
            Console.WriteLine("Number of orders refunded: {0}", refundCount);
            Console.WriteLine("Number of orders cancelled: {0}", cancelCount);
            Console.WriteLine("Number of orders autoupdated to paid: {0}", autoUpdatedToCint);
            return orderCount;
        }

        /// <summary>
        /// Fixes refund error caused by bug in CleanUpJob version 3.1.0
        /// Loops NETS refunds given in console parameters and refunds amount, if this has not been done.
        /// </summary>
        /// <returns>
        /// -1 if error else 0
        /// </returns>
        static int FixRefunds()
        {
            dayCount = 1;
            hourCount = 1;

            int manualOrderId = 0;
            DateTime manualFromDate = DateTime.MinValue;
            DateTime manualToDate = DateTime.MinValue;

            url = _parameters.GetOptionalParameter("-url");
            if (string.IsNullOrEmpty(url))
            {
                Console.WriteLine("Url is empty");
                return -1;
            }

            if (_parameters.Contains("-fromdate"))
            {
                if (!DateTime.TryParse(_parameters.GetOptionalParameter("-fromdate"), out manualFromDate))
                {
                    Console.WriteLine("FromDate: '{0}' is not a valid date", _parameters.GetOptionalParameter("-fromdate"));
                    return -1;
                }
            }

            if (_parameters.Contains("-todate"))
            {
                if (!DateTime.TryParse(_parameters.GetOptionalParameter("-todate"), out manualToDate))
                {
                    Console.WriteLine("ToDate: '{0}' is not a valid date", _parameters.GetOptionalParameter("-todate"));
                    return -1;
                }
            }

            if (_parameters.Contains("-id"))
            {
                if (!int.TryParse(_parameters.GetOptionalParameter("-id"), out manualOrderId))
                {
                    Console.WriteLine("Id: '{0}' is not a number", _parameters.GetOptionalParameter("-hourcount"));
                    return -1;
                }
            }

            Initialize.LoadConfig(url);

            int fixCount = 0;

            if (manualOrderId > 0)
            {
                if (FixRefund(manualOrderId))
                {
                    fixCount++;
                }
                return 0;
            }

            var refunds = Refunds.GetRefunds(manualFromDate, manualToDate);

            // Get NETS transactions only
            var netsRefunds = refunds.Where(refund => refund.Order.PaymentMethod != Orders.PaymentMethods.MobilePay);

            Console.WriteLine("{0} NETS Refunds loadet", netsRefunds.Count());
            foreach (var refund in netsRefunds)
            {
                if (FixRefund(refund.Order.Id))
                {
                    fixCount++;
                }
            }

            Console.WriteLine("{0} Refunds fixed out of {1}", fixCount, netsRefunds.Count());

            return 0;
        }

        /// <summary>
        /// Deletes the cancelled orders given by console parameters
        /// </summary>
        /// <returns>
        /// false if errors were found in parameter
        /// </returns>
        private static bool DeleteCancelledOrders()
        {
            dayCount = 30;
            DateTime firstOrderDate = DateTime.Parse("2014-1-1");
            DateTime lastOrderDate = DateTime.Today.AddDays(-1 * dayCount);

            string url = _parameters.GetOptionalParameter("-url");

            if (string.IsNullOrEmpty(url))
            {
                Console.WriteLine("Url is empty");
                return false;
            }

            if (_parameters.Contains("-daycount"))
            {
                if (!int.TryParse(_parameters.GetOptionalParameter("-daycount"), out dayCount))
                {
                    Console.WriteLine("DayCount: '{0}' is not a number", _parameters.GetOptionalParameter("-daycount"));
                    return false;
                }
            }

            DateTime.TryParse(_parameters.GetOptionalParameter("-fromdate"), out firstOrderDate);
            if (firstOrderDate == DateTime.MinValue)
            {
                Console.WriteLine("Fromdate: is not valid");
                return false;
            }

            if (_parameters.GetOptionalParameter("-todate") != string.Empty)
            {
                DateTime.TryParse(_parameters.GetOptionalParameter("-todate"), out lastOrderDate);
                if (lastOrderDate == DateTime.MinValue)
                {
                    Console.WriteLine("Todate: is not valid");
                    return false;
                }
            }

            Console.WriteLine("Delete Cancelled orders");
            Console.WriteLine();
            Console.WriteLine("Url: {0}", url);
            Console.WriteLine("Start date: {0:dd-MM-yyyy}", firstOrderDate);
            Console.WriteLine("End date: {0:dd-MM-yyyy}", lastOrderDate);
            Console.WriteLine();

            if (firstOrderDate > lastOrderDate)
            {
                Console.WriteLine("Start date is greated than end date!!!");
                return false;
            }

            Initialize.LoadConfig(url);

            int unpaidOrdersStart = 0;
            int unpaidOrdersEnd = 0;
            int paidOrders = 0;
            DateTime startTime = DateTime.Now;

            Bll.Orders.GetStatus(firstOrderDate, lastOrderDate, out paidOrders, out unpaidOrdersStart);
            Console.WriteLine("Total Number of orders: {0}", paidOrders + unpaidOrdersStart);
            Dal.Orders.DeleteUnUsedOrders(firstOrderDate, lastOrderDate);
            Bll.Orders.GetStatus(firstOrderDate, lastOrderDate, out paidOrders, out unpaidOrdersEnd);

            var elapsed = DateTime.Now - startTime;

            string elapsedTime = string.Format("{0} seconds.", elapsed.TotalSeconds);
            if (elapsed.TotalSeconds > 60)
            {
                elapsedTime = string.Format("{0} minutes and {1} seconds.", elapsed.Minutes, elapsed.Seconds);
            }
            if (elapsed.TotalMinutes > 60)
            {
                elapsedTime = string.Format("{0} hours {1} minutes and {2} seconds.", elapsed.Hours, elapsed.Minutes, elapsed.Seconds);
            }

            Console.WriteLine("{0} orders deleted in {1}", (unpaidOrdersStart - unpaidOrdersEnd), elapsedTime);

            return true;
        }
        
        #endregion

        #region Order Actions

        /// <summary>
        /// Fixes refund error caused by bug in CleanUpJob version 3.1.0
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>true if fixed, false if not</returns>
        static bool FixRefund(long orderId)
        {
            var order = new Orders(orderId);
            Console.Write("Order: {0} - {1} ", order.Id, order.CaptureId);
            try
            {
                var paymentInfo = order.GetNetsPaymentInfo();
                float amountCaptured = (int.Parse(paymentInfo.Summary.AmountCaptured) / 100);
                float amountCredited = (int.Parse(paymentInfo.Summary.AmountCredited) / 100);
                bool auth = paymentInfo.Summary.Authorized;

                if (amountCaptured != amountCredited && order.Status == Orders.OrderStatusValues.Refunded && order.SentToCint == false)
                {
                    if (RefundNets(order, null))
                    {
                        Console.WriteLine("Refund fixed", orderId);
                        order.AddLog("Refund fixed");
                        return true;
                    }
                    else
                    {
                        order.AddLog("Refund NOT fixed");
                    }
                }
                else
                {
                    Console.WriteLine("Refund OK");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine("Error fixing: {0}", ex.Message);
            }
            return false;
        }

        /// <summary>
        /// Cancel Initialized order
        /// </summary>
        /// <param name="order">The order.</param>
        static void HandleInitialized(Orders order)
        {
            cancelCount++;
            order.AddLog("Cleanup - Status is Initialized");
            Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): Initialized => Cancel", order.Id, order.SalesDate, order.Status);
            order.SetStatus(Orders.OrderStatusValues.Cancelled);
            return;
        }

        /// <summary>
        /// Refunds NETS order if amount captured differs from amount refunded
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="siteId">The site identifier.</param>
        static void HandleNetsPayment(Orders order, Guid siteId)
        {
            if (order.CaptureId == string.Empty)
            {
                cancelCount++;
                order.AddLog("Cleanup - No transaction Id.");
                Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): No TransactionId => Cancel", order.Id, order.SalesDate, order.Status);
                order.SetStatus(Orders.OrderStatusValues.Cancelled);
                return;
            }

            try
            {
                var paymentInfo = order.GetNetsPaymentInfo();
                float amountCaptured = (int.Parse(paymentInfo.Summary.AmountCaptured) / 100);
                float amountCredited = (int.Parse(paymentInfo.Summary.AmountCredited) / 100);
                bool auth = paymentInfo.Summary.Authorized;
                if (!auth)
                {
                    cancelCount++;
                    order.AddLog("Cleanup - Payment not authorized.");
                    Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): Not Authorized => Cancel", order.Id, order.SalesDate, order.Status);
                    order.SetStatus(Orders.OrderStatusValues.Cancelled);
                    return;
                }

                if (amountCaptured == 0)
                {
                    cancelCount++;
                    order.AddLog("Cleanup - Amount captured is 0.");
                    Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): Amount captured is 0 => Cancel", order.Id, order.SalesDate, order.Status);
                    order.SetStatus(Orders.OrderStatusValues.Cancelled);
                    return;
                }

                if (amountCaptured > 0 && amountCaptured == amountCredited)
                {
                    Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): Already refunded => Refunded", order.Id, order.SalesDate, order.Status);
                    order.SetStatus(Orders.OrderStatusValues.Refunded);
                    return;
                }

                if (amountCaptured > 0)
                {
                    if (string.IsNullOrEmpty(order.ReceiptNumber))
                    {
                        order.ReceiptNumber = order.Id.ToString();
                    }

                    Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): Refunding: {3}", order.Id, order.SalesDate, order.Status, amountCaptured);
                    RefundOrder(order, siteId);
                    refundCount++;
                    return;
                }
            }
            catch (Exception ex)
            {

                if (ex.Message.Contains("Unable to find transaction"))
                {
                    order.AddLog("Cleanup - Transaction not found.");
                    order.SetStatus(Orders.OrderStatusValues.Cancelled);
                }
                else
                {
                    order.AddLogError(string.Format("Error looking up order: {0}", ex.Message));
                    Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): Error: {3}", order.Id, order.SalesDate, order.Status, ex.Message);
                }

                return;
            }
        }

        /// <summary>
        /// Refunds MobilePay transaction
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">
        /// MobilePayMerchantId not found. Please add to Configuration
        /// or
        /// MobilePayClientId not found. Please add to Configuration
        /// </exception>
        static string RepayMobilePay(Orders order, out string transactionId)
        {
            string result = transactionId = string.Empty;

            PnConfig configuration = new PnConfig()
            {
                ClientContentEncryptionPassword = ConfigSettings.GetText("ClientContentEncryptionPassword"),
                ClientContentEncryptionPath = ConfigSettings.GetText("ClientContentEncryptionPath"),
                ClientSignaturePassword = ConfigSettings.GetText("ClientSignaturePassword"),
                ClientSignaturePath = ConfigSettings.GetText("ClientSignaturePath"),
                DbContentEncryptionPath = ConfigSettings.GetText("DBContentEncryptionPath"),
                DbSignaturePath = ConfigSettings.GetText("DBSignaturePath")
            };

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            try
            {
                var dbOutput = Proxy.CallRefund(configuration, clientId, merchantId, order.MobilePayOrderNr, (decimal)order.OrderSum);
                transactionId = dbOutput.TransactionId;
                if (dbOutput.ReturnCode != "00")
                {
                    string error = string.Format("MobilePay Refund Error: Error: {0} ({1}) Reason: {2} ({3}) ", dbOutput.ReturnCode, PostNord.Portal.DigitalPostage.Helpers.MobilePay.V2Returncode(dbOutput.ReturnCode), dbOutput.ReasonCode, PostNord.Portal.DigitalPostage.Helpers.MobilePay.V2Reasoncode(dbOutput.ReasonCode));
                    order.AddLogError(error);
                    return error;
                }
            }
            catch (Exception ex)
            {
                return string.Format("MobilePay WS Error: {0}", ex.Message);
            }
            return string.Empty;
        }

        /// <summary>
        /// Refund / Credit or Cancel order based on MP Status
        /// </summary>
        /// <param name="order">The order.</param>
        static void HandleStatusReadyForPaymentMobilePay(Orders order)
        {
            bool refundOrder = false;

            // We have not looked up this order yet
            if (order.CaptureId == string.Empty || recheck)
            {
                DB.MobilePayV02.GetStatus.dacGetStatusOutput res = order.GetMobilePayPaymentInfo();
                transactionId = res.OriginalTransactionId;
                switch (res.LatestPaymentStatus)
                {
                    case PostNord.Portal.DigitalPostage.Helpers.MobilePay.PAYMENTSTATUS_CAPTURED:
                        paidCount++;
                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): PAID => Credit", order.Id, order.SalesDate, order.Status);
                        refundOrder = true; // Make sure we send refund mail
                        break;
                    case PostNord.Portal.DigitalPostage.Helpers.MobilePay.PAYMENTSTATUS_REFUNDED:
                        refundCount++;
                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): REFUNDED  => Refund", order.Id, order.SalesDate, order.Status);
                        order.CaptureId = transactionId;
                        order.SetStatus(PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Refunded);
                        break;
                    case PostNord.Portal.DigitalPostage.Helpers.MobilePay.PAYMENTSTATUS_CANCELLED:
                        cancelCount++;
                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): CANCELLED => Cancel", order.Id, order.SalesDate, order.Status);
                        order.CaptureId = transactionId;
                        order.SetStatus(PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Cancelled);
                        break;
                    case PostNord.Portal.DigitalPostage.Helpers.MobilePay.PAYMENTSTATUS_RESERVED:
                        cancelCount++;
                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): RESERVED => Cancel", order.Id, order.SalesDate, order.Status);
                        try
                        {
                            order.CancelOrder();
                            order.CaptureId = transactionId;
                            order.SetStatus(PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Cancelled);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Cancel order error: {0}", ex.Message);
                            return;
                        }
                        break;
                    default:
                        cancelCount++;
                        Console.WriteLine("{0} {1:dd:MM:yyyy HH:mm:ss} ({2}): OTHER: {3} => Cancel", order.Id, order.SalesDate, order.Status, res.LatestPaymentStatus);
                        //Console.WriteLine("  Return: {0}, {1} - Reason: {2}, {3}", res.ReturnCode, PostNord.Portal.DigitalPostage.Helpers.MobilePay.V2Returncode(res.ReturnCode), res.ReasonCode, PostNord.Portal.DigitalPostage.Helpers.MobilePay.V2Reasoncode(res.ReasonCode));
                        order.SetStatus(Orders.OrderStatusValues.Cancelled);
                        order.Save();
                        break;
                }
            }
            count++;

            //If order.captureId != 0 && order.captureId != "Refunded", order has been paid and should be refunded

            if (doRefund)
            {
                if (refundOrder)
                {
                    order.AddLogDebug("Scheduled MobilePay Refund");
                    string res = string.Empty;
                    res = RepayMobilePay(order, out transactionId);

                    if (res != string.Empty)
                    {
                        order.AddLogError(string.Format("Error refunding Manual MobilePay : {0}", res));
                    }
                    else
                    {
                        try
                        {
                            Bll.Refunds refund = new Refunds();
                            refund.OrderNr = order.Id;
                            refund.Timestamp = DateTime.Now;

                            refund.Address = "None";
                            refund.City = "None";
                            refund.Message = "None";
                            refund.Name = "None";
                            refund.Phone = "None";
                            refund.PostNumber = 1000;
                            refund.Message = "MP Auto refunded";
                            refund.SiteId = Guid.Empty;
                            refund.Employee = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                            var codes = GetCodes(order);
                            int codesRefunded = RefundCodes(codes, order, refund);
                            order.AddLogDebug(string.Format("Refunded {0} of {1} codes", codesRefunded, codes.Count));
                        }
                        catch (Exception ex)
                        {
                            order.AddLogError(string.Format("Error refunding codes. {0}", ex.Message));
                        }

                        order.CaptureId = transactionId;
                        order.SetStatus(PostNord.Portal.DigitalPostage.Bll.Orders.OrderStatusValues.Refunded);
                        try
                        {
                            SendMail(order);
                        }
                        catch (Exception ex)
                        {
                            order.AddLogError(string.Format("Error sending mail : {0}", ex.Message));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get All codes for order. Loops suborders and returns all generated codes
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns>
        /// All Codes generated for order
        /// </returns>
        private static List<Codes> GetCodes(Orders order)
        {
            List<Codes> codes = new List<Codes>();
            foreach (SubOrders subOrder in order.SubOrders)
            {
                codes.AddRange(subOrder.GetCodes());
            }
            return codes;
        }

        /// <summary>
        /// Refunds the order.
        /// </summary>
        private static void RefundOrder(Orders order, Guid siteId)
        {
            order.AddLog("Auto refunding order");

            Bll.Refunds refund = new Refunds();
            refund.OrderNr = order.Id;
            refund.Timestamp = DateTime.Now;

            refund.Address = "None";
            refund.City = "None";
            refund.Message = "None";
            refund.Name = "None";
            refund.Phone = "None";
            refund.PostNumber = 1000;
            refund.Message = "Auto refunded";
            refund.SiteId = siteId;
            refund.Employee = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            if (refund.Employee == string.Empty)
            {
                refund.Employee = "CleanUpJob";
            }

            try
            {
                if (order.AppPlatform == "WebShop")
                {
                    // Find all transaction
                    var transactions = order.Logs.Where(logLine => logLine.Text.Contains("Register payment. Transaction Id:"));

                    // Multiple transactions error
                    if (transactions != null && transactions.Count<OrderLogs>() > 1)
                    {
                        List<string> extraTransactions = new List<string>();
                        // create list of transactions
                        foreach (var tLine in transactions)
                        {
                            string trans = tLine.Text.Split(':')[1].Trim();
                            Console.WriteLine("Transaction: {0}", trans);
                            extraTransactions.Add(trans);
                        }

                        // If we have refunded lines we send mail contraining refund info
                        if (RefundNetsMultipleTransaction(extraTransactions, order, order.OrderSum, refund))
                        {
                            order.AddLogDebug("Refund mail sent");
                            SendMultiMail(order, refund);
                            refund.Message = "None";
                            refund.Save();
                        }
                    }
                    // Normal WebShop NETS transaction
                    else
                    {
                        refund.Save();
                        if (RefundNets(order, refund))
                        {
                            var codes = GetCodes(order);
                            int codesRefunded = RefundCodes(codes, order, refund);
                            order.AddLogDebug(string.Format("Refunded {0} of {1} codes", codesRefunded, codes.Count));
                            order.SetStatus(Orders.OrderStatusValues.Refunded);

                            try
                            {
                                SendMail(order);
                            }
                            catch (Exception ex)
                            {
                                order.AddLogError("Error sending Refund mail. " + ex.Message);
                            }
                        }
                    }
                }
                else
                {
                    if (RefundNets(order, refund))
                    {
                        var codes = GetCodes(order);
                        int codesRefunded = RefundCodes(codes, order, refund);
                        order.AddLogDebug(string.Format("Refunding {0} of {1} codes", codesRefunded, codes.Count));
                        order.SetStatus(Orders.OrderStatusValues.Refunded);

                        try
                        {
                            SendMail(order);
                        }
                        catch (Exception ex)
                        {
                            order.AddLogError("Error sending Refund mail. " + ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error refunding code: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Refunds the codes.
        /// </summary>
        /// <param name="codes">The codes.</param>
        /// <param name="order">The order.</param>
        /// <param name="refund">The refund.</param>
        /// <returns>Number of codes refunded</returns>
        static int RefundCodes(List<Codes> codes, Orders order, Refunds refund)
        {
            if (refund.Id <= 0)
            {
                refund.Save();
            }
            int codesRefunded = 0;
            foreach (var code in codes)
            {
                if (code.Status == Codes.CodeStatusValues.Unused)
                {
                    code.Refund(refund.Id, DateTime.Now);
                    codesRefunded++;
                }
            }
            return codesRefunded;
        }

        /// <summary>
        /// Refunds NETS multiple transactions.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="refund">The refund.</param>
        /// <returns>
        /// TRUE if all transactions were refunded
        /// </returns>
        static bool RefundNets(Orders order, Refunds refund)
        {
            string merchantId = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("MerchantId");
            string token = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("Token");
            string terminalUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TerminalUrl");
            string netaxptWsUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("NetaxeptWebserviceUrl");
            double refundValue = 0;

            int capturedCount = 0;
            int refundedCount = 0;

            using (NetAxept.Netaxept client = new NetAxept.Netaxept())
            {
                client.Url = ConfigSettings.GetText("NetaxeptWebserviceUrl");
                client.Url = netaxptWsUrl;

                try
                {
                    NetAxept.QueryRequest qReq = new NetAxept.QueryRequest();
                    qReq.TransactionId = order.CaptureId;
                    NetAxept.PaymentInfo paymentInfo = (NetAxept.PaymentInfo)client.Query(merchantId, token, qReq);
                    int amountCaptured = 0;
                    int amountCredited = 0;
                    int.TryParse(paymentInfo.Summary.AmountCaptured, out amountCaptured);
                    int.TryParse(paymentInfo.Summary.AmountCredited, out amountCredited);

                    if (amountCaptured > 0)
                    {
                        capturedCount++;
                    }

                    if (amountCredited > 0)
                    {
                        refundedCount++;
                    }

                    if (amountCaptured > 0 && amountCredited == 0)
                    {
                        NetAxept.ProcessRequest request = new NetAxept.ProcessRequest();
                        request.Operation = "CREDIT";
                        request.TransactionReconRef = order.Id.ToString();
                        request.TransactionId = order.CaptureId;
                        request.TransactionAmount = paymentInfo.Summary.AmountCaptured;
                        order.AddLog(string.Format("Refunding transaction: {0} Amount: {1}", transactionId, paymentInfo.Summary.AmountCaptured));
                        NetAxept.ProcessResponse response = client.Process(merchantId, token, request);
                        refundValue += amountCaptured;
                        order.AddLog(string.Format("Refund result Status code: {0} Message: {1}", response.ResponseCode, response.ResponseText));
                        refundedCount++;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error refunding: {0}", ex.Message);
                    order.AddLogError(string.Format("Error refunding: {0}", ex.Message));
                    return false;
                }
            }

            // If everything Refunded we change status to Credited
            if (capturedCount > 0 && capturedCount == refundedCount && refund != null)
            {
                var codes = GetCodes(order);
                int codesRefunded = RefundCodes(codes, order, refund);
                order.AddLogDebug(string.Format("Refunding {0} of {1} codes", codesRefunded, codes.Count));
                order.SetStatus(Orders.OrderStatusValues.Refunded);
            }

            // If refunded we create a mail
            if (capturedCount > 0 && capturedCount == refundedCount)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Refunds NETS multiple transactions.
        /// </summary>
        /// <param name="transactionIds">The transaction ids.</param>
        /// <param name="order">The order.</param>
        /// <param name="amount">The amount.</param>
        /// <returns>TRUE if all transactions were refunded</returns>
        static bool RefundNetsMultipleTransaction(List<string> transactionIds, Orders order, double amount, Refunds refund)
        {
            string merchantId = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("MerchantId");
            string token = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("Token");
            string terminalUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TerminalUrl");
            string netaxptWsUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("NetaxeptWebserviceUrl");
            double refundValue = 0;

            int capturedCount = 0;
            int refundedCount = 0;
            string creditLines = string.Empty;

            string refundTemplate = "<tr><td>Transaktion: {0}</td><td style='text-align:right'>DKK {1:n2}</td></tr>";

            using (NetAxept.Netaxept client = new NetAxept.Netaxept())
            {
                client.Url = ConfigSettings.GetText("NetaxeptWebserviceUrl");
                client.Url = netaxptWsUrl;

                foreach (string transactionId in transactionIds)
                {
                    try
                    {
                        NetAxept.QueryRequest qReq = new NetAxept.QueryRequest();
                        qReq.TransactionId = transactionId;
                        NetAxept.PaymentInfo paymentInfo = (NetAxept.PaymentInfo)client.Query(merchantId, token, qReq);
                        int amountCaptured = 0;
                        int amountCredited = 0;
                        int.TryParse(paymentInfo.Summary.AmountCaptured, out amountCaptured);
                        int.TryParse(paymentInfo.Summary.AmountCredited, out amountCredited);

                        if (amountCaptured > 0)
                        {
                            capturedCount++;
                        }

                        if (amountCredited > 0)
                        {
                            refundedCount++;
                        }

                        if (amountCaptured > 0 && amountCredited == 0)
                        {
                            NetAxept.ProcessRequest request = new NetAxept.ProcessRequest();
                            request.Operation = "CREDIT";
                            request.TransactionReconRef = order.Id.ToString();
                            request.TransactionId = transactionId;
                            request.TransactionAmount = paymentInfo.Summary.AmountCaptured;
                            order.AddLog(string.Format("Refunding transaction: {0} Amount: {1}", transactionId, paymentInfo.Summary.AmountCaptured));
                            NetAxept.ProcessResponse response = client.Process(merchantId, token, request);
                            refundValue += amountCaptured;
                            order.AddLog(string.Format("Refund result Status code: {0} Message: {1}", response.ResponseCode, response.ResponseText));
                            refundedCount++;
                            creditLines += string.Format(new System.Globalization.CultureInfo("da-DK"), refundTemplate + "<br/>", transactionId, (amountCaptured / 100));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error refunding: {0}", ex.Message);
                        order.AddLogError(string.Format("Error refunding: {0}", ex.Message));
                    }
                }
            }

            // If everything Refunded we change status to Credited
            if (capturedCount > 0 && capturedCount == refundedCount)
            {
                var codes = GetCodes(order);
                int codesRefunded = RefundCodes(codes, order, refund);
                order.AddLogDebug(string.Format("Refunding {0} of {1} codes", codesRefunded, codes.Count));
                order.SetStatus(Orders.OrderStatusValues.Refunded);
            }

            // If refunded we create a mail
            if (capturedCount > 0 && capturedCount == refundedCount && creditLines != string.Empty)
            {
                refund.Message = _multiPaymentMail;
                refund.Message = refund.Message.Replace("{OrderId}", order.Id.ToString());
                refund.Message = refund.Message.Replace("{SalesDate}", order.SalesDate.ToString("dd-MM-yyyy"));
                refund.Message = refund.Message.Replace("{RefundDate}", DateTime.Now.ToString("dd-MM-yyyy"));
                refund.Message = refund.Message.Replace("{CreditSum}", string.Format(new System.Globalization.CultureInfo("da-DK"), "DKK {0:n2}", (refundValue / 100)));
                refund.Message = refund.Message.Replace("{CreditLines}", creditLines);
                refund.Save();
                return true;
            }

            return false;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Sends the status mail.
        /// </summary>
        /// <param name="numberOfMPOrders">The number of mp orders.</param>
        /// <param name="numberOfOrdersRefunded">The number of orders refunded.</param>
        /// <param name="url">The URL.</param>
        private static void SendStatusMail(int numberOfMPOrders, int numberOfOrdersRefunded, string url, int dayCount, DateTime firstOrderDate, DateTime lastOrderDate)
        {
            string filename = "EmailNotifications.txt";
            string[] emails = null;
            try
            {
                emails = File.ReadAllLines(filename);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading file: '{0}' - {1}", filename, ex.Message);
                Console.WriteLine("No email sent.");
                return;
            }

            string mailTitle = "Refund job complete";

            string bodyHtml = string.Format("<p>Automatic refund job complete.</p><p>Number of orders processed:{0}<br/>Number of refunds made:{1}</br></p>", numberOfMPOrders, numberOfOrdersRefunded);
            bodyHtml += string.Format("<p>Order period: {0:dd:MM:yyyy HH:mm:ss} - {1:dd:MM:yyyy HH:mm:ss} ({2} days)</p>", firstOrderDate, lastOrderDate, dayCount);
            if (numberOfOrdersRefunded > 0)
            {
                bodyHtml += string.Format("<p>Log file uploaded to: <br/>{0}</p>", url);
            }
            else
            {
                bodyHtml += string.Format("<p>Log file not added</p>");
            }

            SmtpClient mailClient = new SmtpClient(ConfigSettings.GetText("SMTPServer"));

            foreach (string s in emails)
            {
                if (s.Contains('@'))
                {
                    try
                    {
                        Console.WriteLine("Sending mail to: {0}", s);
                        MailMessage message = new MailMessage(ConfigSettings.GetText("ReplyMailAddress"), s.Trim());
                        message.IsBodyHtml = true;
                        message.Body = bodyHtml;
                        message.Subject = mailTitle;
                        message.SubjectEncoding = Encoding.UTF8;
                        message.BodyEncoding = Encoding.UTF8;
                        mailClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error sending mail to : '{0}' - {1} ", s.Trim(), ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Removes the XML tags and namespaces from html
        /// </summary>
        /// <param name="html">Raw HTML</param>
        /// <returns>Cleaned up HTML</returns>
        private static string RemoveXml(string html)
        {
            string pattern = "^<\\?xml.*\\?>";
            Regex reg = new Regex(pattern);
            return reg.Replace(html, "<!DOCTYPE html>" + Environment.NewLine);
        }

        /// <summary>
        /// Gets HTML representation of Order based on XSLT
        /// </summary>
        /// <param name="xslt">The XSLT.</param>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        private static string GetHtml(string xslt, Orders order)
        {
            string html;

            XslCompiledTransform xsl = new XslCompiledTransform();
            xsl.Load(new XmlTextReader(new StringReader(xslt)));

            DateTime oldDate = order.ExpireDate;
            order.ExpireDate = DateTime.Now;
            string xml = order.Xml();
            order.ExpireDate = oldDate;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            using (MemoryStream xmlStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml)))
            {
                XPathDocument xPathDoc = new XPathDocument(new StreamReader(xmlStream));
                StringBuilder htmlBuilder = new StringBuilder();

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Encoding = Encoding.UTF8;
                xmlSettings.OmitXmlDeclaration = true;
                xmlSettings.ConformanceLevel = ConformanceLevel.Document;

                XmlWriter xmlWriter = XmlWriter.Create(htmlBuilder, xmlSettings);
                xsl.Transform(xPathDoc, xmlWriter);
                html = htmlBuilder.ToString();
            }

            return RemoveXml(html);
        }

        /// <summary>
        /// Send refund mail to customer.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="refund">The refund.</param>
        private static void SendMultiMail(Orders order, Refunds refund)
        {
            string mailTitle = "Refundering vedr. køb af portokode";

            string bodyHtml = string.Empty;
            DateTime oldDate = order.ExpireDate;

            SmtpClient mailClient = new SmtpClient(ConfigSettings.GetText("SMTPServer"));
            MailMessage message = new MailMessage(ConfigSettings.GetText("ReplyMailAddress"), order.Email);
            message.IsBodyHtml = true;
            message.Body = refund.Message;
            message.Subject = mailTitle;
            message.SubjectEncoding = Encoding.UTF8;
            message.BodyEncoding = Encoding.UTF8;

            try
            {
                mailClient.Send(message);
            }
            catch (Exception ex)
            {
                order.AddLogError(string.Format("Error sending multi refund mail : {0}", ex.Message));
            }
        }

        /// <summary>
        /// Send refund mail to customer.
        /// </summary>
        /// <param name="order">The order.</param>
        private static void SendMail(Orders order)
        {
            string mailTitle = "Refundering vedr. køb af portokode";

            string bodyHtml = string.Empty;
            DateTime oldDate = order.ExpireDate;
            if (order.PaymentMethod != Orders.PaymentMethods.MobilePay)
            {
                bodyHtml = GetHtml(_xsltMailNets, order);
            }
            else
            {
                bodyHtml = GetHtml(_xsltMail, order);
            }

            SmtpClient mailClient = new SmtpClient(ConfigSettings.GetText("SMTPServer"));
            MailMessage message = new MailMessage(ConfigSettings.GetText("ReplyMailAddress"), order.Email);
            message.IsBodyHtml = true;
            message.Body = bodyHtml;
            message.Subject = mailTitle;
            message.SubjectEncoding = Encoding.UTF8;
            message.BodyEncoding = Encoding.UTF8;

            try
            {
                mailClient.Send(message);
            }
            catch (Exception ex)
            {
                order.AddLogError(string.Format("Error sending refund mail : {0}", ex.Message));
            }
        }


        #endregion

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
         static void Main(string[] args)
        {
            _parameters = new Parameters(args);

            Console.WriteLine("*** CleanUpJob ***");
            if (_parameters.Help || args.Length == 0)
            {
                Console.WriteLine("Cancel orders");
                Console.WriteLine("Arguments -o CancelOrders -url <url> -daycount <daycount> -hourcount <hourcount>");
                Console.WriteLine("Daycount: Number of days start from. Default is 1 day");
                Console.WriteLine("Hourcount: Number of hours to end. Default is 1 hour");
                Console.WriteLine("Options: -paymenttype  NETS / MobilePay - Handles either NETS or MobilePay");
                Console.WriteLine("         -repeat  true - The job is looped until <ESCAPE> is pressed");
                Console.WriteLine("         -id <orderId> - runs cancel method on single order");
                Console.WriteLine("         -fromdate <yyyy-mm-dd> - set from date");
                Console.WriteLine("         -todate <yyyy-mm-dd> - set to date");
                Console.WriteLine();
                Console.WriteLine("Delete cancelled orders");
                Console.WriteLine("Arguments -o DeleteCancelledOrders -url <url> -daycount <daycount>");
                Console.WriteLine("Daycount: Number of days start from. Default is 30 days");
                Console.WriteLine("Options:");
                Console.WriteLine("         -fromdate <yyyy-mm-dd> - set from date");
                Console.WriteLine("         -todate <yyyy-mm-dd> - set to date");
                Console.WriteLine();
                Console.WriteLine("Refund from Logsearch");
                Console.WriteLine("Arguments -o RefundOrders -url <url> -fromDate 'yyyy-mm-dd' -toDate 'yyyy-mm-dd' -logtext '<error text>'");
                return;
            }
            else
            {
                Console.WriteLine("CleanUpJob -?  for help");
            }

            try
            {
                string cmd = _parameters.GetOptionalParameter("-o").ToLower();
                switch (cmd)
                {
                    case "cancelorders" :
                        CancelOrders();
                        break;
                    case "deletecancelledorders":
                        DeleteCancelledOrders();
                        break;
                    case "fixrefunds":
                        FixRefunds();
                        break;
                    default:
                        Console.WriteLine("Unknown command :{0}", cmd);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }
        }

    }
}

