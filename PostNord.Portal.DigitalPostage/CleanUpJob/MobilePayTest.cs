﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PN.SoapLibrary.WSSecurity;
using PostNord.Portal.DigitalPostage.Bll;

namespace UnitTests
{
    [TestClass]
    public class MobilePayTest
    {

        string certId = "6K1280";
        string appId = "APPDK5964951001";
        long orderId = 33384;

        [TestMethod]
        public void TestValidation()
        {
            string mobilePaySignature = "MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0BBwGggCSABDgzMzM1MSNBUFBESzAwMDAwMDAwMDAjMTIzNDU2Nzg5MDEyMzQ1Njc4OTAjMTAuNTAjREtLI0RLIwAAAAAAAKCCBAUwggQBMIIC6aADAgECAgUCEdB6ETANBgkqhkiG9w0BAQsFADCBmDEQMA4GA1UEAxMHREJHUk9PVDELMAkGA1UEBhMCREsxEzARBgNVBAcTCkNvcGVuaGFnZW4xEDAOBgNVBAgTB0Rlbm1hcmsxGjAYBgNVBAoTEURhbnNrZSBCYW5rIEdyb3VwMRowGAYDVQQLExFEYW5za2UgQmFuayBHcm91cDEYMBYGA1UEBRMPNjExMjYyMjgxMTEwMDAyMB4XDTE0MDUxMjAwMDAwMFoXDTE2MDUxMjAwMDAwMFowgZYxDjAMBgNVBAMTBU1TSUdOMQswCQYDVQQGEwJESzETMBEGA1UEBxMKQ29wZW5oYWdlbjEQMA4GA1UECBMHRGVubWFyazEaMBgGA1UEChMRRGFuc2tlIEJhbmsgR3JvdXAxGjAYBgNVBAsTEURhbnNrZSBCYW5rIEdyb3VwMRgwFgYDVQQFEw82MTEyNjIyODg4MTAwMDEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCtJiF4u/CL39avAbSnJhOzj4sQg2dksv/iRoJOtISIosP2exSaxes4tRycCWBU1Owrbr8ksSXpPSu2UzLCMQ4+qcsbYkEeSpXjQak5JccVWZdXFoUTq72f7LSnnuK1fo8kTyybv8Z5MDn6daDd9z/KJg+J4x3z9O9A/5L553+ZsNsd70VlpcEVOWXPkNf9w/ze2ztcosbzuZYp+YSI36fjjsn0UmBDaYk9ajuk1o0odda3qq0BqrhUlU3G29jD73L+ZWhmRpUEtPfjY+7uRcpl7Bo7IoscXWk1943Gj4ATBZ/hW60boSQStgQlDxglyi6yGyPE0MTp3ngqXO8HX7LfAgMBAAGjUjBQMB8GA1UdIwQYMBaAFIT65b/ekUlm38WKUsOzt7MgHMdtMB0GA1UdDgQWBBSZb6E7shVfskN4t9sqioYjiJmfFTAOBgNVHQ8BAf8EBAMCBsAwDQYJKoZIhvcNAQELBQADggEBAEHGaztr1YjWJsGi0nJBakoTliwRPcsBiQYhFIIhPMPpbByg9P4KxHrSYI4oyz+QoC6s3htqrn98rUNUP05jvizRDVZ1CGY0vjB3Xn1adJMWS3pB5HPOLyooBfGR8AUK1BX61Xc0ggVfYdiEKkuopjsFAfYpWA2tF+fSA4tzbWCc1ZXf7bNtgbvqWg2x2S+gWsYZScgYn1ssUB/ho0pT9Ys0MXhdfMAMLPA0KO/OKoskpC/5E3clSNijfju5TyM7rTxQMoZYFcPUDCyHqL1SInAh/cGuqv3StPeT0ymCyB00QsyfFkQKhH4QFmBnTRCqAsDrX95coWHu77aBZFfsn90xggHOMIIBygIBATCBojCBmDEQMA4GA1UEAxMHREJHUk9PVDELMAkGA1UEBhMCREsxEzARBgNVBAcTCkNvcGVuaGFnZW4xEDAOBgNVBAgTB0Rlbm1hcmsxGjAYBgNVBAoTEURhbnNrZSBCYW5rIEdyb3VwMRowGAYDVQQLExFEYW5za2UgQmFuayBHcm91cDEYMBYGA1UEBRMPNjExMjYyMjgxMTEwMDAyAgUCEdB6ETANBglghkgBZQMEAgEFADANBgkqhkiG9w0BAQEFAASCAQBh0Wrb79KVEnfEmAu3Hjt2Z2kPOX1GUuYhNsVTqPBvb26vodboqVI87rwOKmeCI+00AkPzZ+Cmn6QiewpQKusF8XO4lG0dzqKEpnMMxMYlj3mrCyehOQlv3eIKpiP+xchFJyjf3AH1A/aAtKG+q1pkgXey1x1npiO1+o6bQRc2MOMcAjITgZTZlVW7DpCmI6m7gDHc2aIsWWFb7sy3Gjzjk1tB1LU24JIrrYfOGElclAn6BXX14exudWey8dS1fL33opFoqyPzPvYNCzEE6W5fPamykzqpZPos0/zRVM+u02pngKF5N6YoooVlgUd83CcM/OCcuZaIICs1qlMdj/9CAAAAAAAA";
            string orderId = "33351";
            string merchantId = "APPDK0000000000";
            string transactionId = "12345678901234567890";
            string orderSum = "10.50";

            mobilePaySignature = "MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0BBwGggCSABC0zMzM4NCNBUFBESzU5NjQ5NTEwMDEjMTAyNDU3ODU2MyM3LjAwI0RLSyNESyMAAAAAAACgggQFMIIEATCCAumgAwIBAgIFAhHQehEwDQYJKoZIhvcNAQELBQAwgZgxEDAOBgNVBAMTB0RCR1JPT1QxCzAJBgNVBAYTAkRLMRMwEQYDVQQHEwpDb3BlbmhhZ2VuMRAwDgYDVQQIEwdEZW5tYXJrMRowGAYDVQQKExFEYW5za2UgQmFuayBHcm91cDEaMBgGA1UECxMRRGFuc2tlIEJhbmsgR3JvdXAxGDAWBgNVBAUTDzYxMTI2MjI4MTExMDAwMjAeFw0xNDA1MTIwMDAwMDBaFw0xNjA1MTIwMDAwMDBaMIGWMQ4wDAYDVQQDEwVNU0lHTjELMAkGA1UEBhMCREsxEzARBgNVBAcTCkNvcGVuaGFnZW4xEDAOBgNVBAgTB0Rlbm1hcmsxGjAYBgNVBAoTEURhbnNrZSBCYW5rIEdyb3VwMRowGAYDVQQLExFEYW5za2UgQmFuayBHcm91cDEYMBYGA1UEBRMPNjExMjYyMjg4ODEwMDAxMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArSYheLvwi9/WrwG0pyYTs4+LEINnZLL/4kaCTrSEiKLD9nsUmsXrOLUcnAlgVNTsK26/JLEl6T0rtlMywjEOPqnLG2JBHkqV40GpOSXHFVmXVxaFE6u9n+y0p57itX6PJE8sm7/GeTA5+nWg3fc/yiYPieMd8/TvQP+S+ed/mbDbHe9FZaXBFTllz5DX/cP83ts7XKLG87mWKfmEiN+n447J9FJgQ2mJPWo7pNaNKHXWt6qtAaq4VJVNxtvYw+9y/mVoZkaVBLT342Pu7kXKZewaOyKLHF1pNfeNxo+AEwWf4VutG6EkErYEJQ8YJcoushsjxNDE6d54KlzvB1+y3wIDAQABo1IwUDAfBgNVHSMEGDAWgBSE+uW/3pFJZt/FilLDs7ezIBzHbTAdBgNVHQ4EFgQUmW+hO7IVX7JDeLfbKoqGI4iZnxUwDgYDVR0PAQH/BAQDAgbAMA0GCSqGSIb3DQEBCwUAA4IBAQBBxms7a9WI1ibBotJyQWpKE5YsET3LAYkGIRSCITzD6WwcoPT+CsR60mCOKMs/kKAurN4baq5/fK1DVD9OY74s0Q1WdQhmNL4wd159WnSTFkt6QeRzzi8qKAXxkfAFCtQV+tV3NIIFX2HYhCpLqKY7BQH2KVgNrRfn0gOLc21gnNWV3+2zbYG76loNsdkvoFrGGUnIGJ9bLFAf4aNKU/WLNDF4XXzADCzwNCjvziqLJKQv+RN3JUjYo347uU8jO608UDKGWBXD1Awsh6i9UiJwIf3Brqr90rT3k9MpgsgdNELMnxZECoR+EBZgZ00QqgLA61/eXKFh7u+2gWRX7J/dMYIBzjCCAcoCAQEwgaIwgZgxEDAOBgNVBAMTB0RCR1JPT1QxCzAJBgNVBAYTAkRLMRMwEQYDVQQHEwpDb3BlbmhhZ2VuMRAwDgYDVQQIEwdEZW5tYXJrMRowGAYDVQQKExFEYW5za2UgQmFuayBHcm91cDEaMBgGA1UECxMRRGFuc2tlIEJhbmsgR3JvdXAxGDAWBgNVBAUTDzYxMTI2MjI4MTExMDAwMgIFAhHQehEwDQYJYIZIAWUDBAIBBQAwDQYJKoZIhvcNAQEBBQAEggEAXspiliziXltyb4nbSeP0arSWN3VTC1EZ5je9yzBCO6oT65caksGRdrvf/uQNQdNR7xq1j+Bhx1/XyDjVhHQI06QMvV2RDHgdu3msaLYU9iIShmnQ8DqWhHO3TMQRUqqNAUzToek485wM89ebOItwXn9Qt+K5sGwIFQ20QH9VkNh1/f0LF3TCMAL2HWazB7R3xG0JCj/3SEVR0GUu5A06X4kLoRUA9SjJNw2Jw6jeDEsyAOFn79uBtI1Jdwyew8QIfCe50Fu1CmpX+Vg6lAQk+LxJ+HpihREj04Huicf+WbAMKhPAnawPwOSKMofHpwn2RgkcGjkoUwUtjrwvciExngAAAAAAAA==";
            orderId = "33384";
            merchantId = appId;
            transactionId = "1024578563";
            orderSum = "7.00";


            MobilePaySignatureValidator validator = new MobilePaySignatureValidator();
            Console.WriteLine("Validating Signature. OrderId: {0} TransactionId: {1}",orderId,  transactionId);
            var isValidSignature = validator.ValidateSignature(mobilePaySignature, orderId, merchantId, transactionId, orderSum);
            Console.WriteLine("Valid : {0}", isValidSignature);

            orderId = "33333";
            Console.WriteLine("Validating wrong Signature. OrderId: {0} TransactionId: {1}", orderId, transactionId);
            isValidSignature = validator.ValidateSignature(mobilePaySignature, orderId, merchantId, transactionId, orderSum);
            Console.WriteLine("Valid : {0}", isValidSignature);
        }


        [TestMethod]
        public void TestMobilePayGetStatus()
        {
            PnConfig configuration = new PnConfig();
            configuration.ClientSignaturePath = @"C:\Innofactor\MobilePay\6K1280_sign.pfx";
            configuration.ClientSignaturePassword = "MobilePay";
            configuration.ClientContentEncryptionPath = @"C:\Innofactor\MobilePay\6K1280_crypt.pfx";
            configuration.ClientContentEncryptionPassword = "MobilePay";
            configuration.DbSignaturePath = @"C:\Innofactor\MobilePay\DBGSIGN.crt";
            configuration.DbContentEncryptionPath = @"C:\Innofactor\MobilePay\DBGCRYPT.crt";

            string orderNr = orderId.ToString().PadLeft(12, '0');

            DB.MobilePay.GetStatus.Output output = PostNord.Portal.DigitalPostage.MobilePay.Proxy.CallGetStatusSoapService(configuration, certId, appId, orderNr);
            Assert.IsTrue(output.ReturnCode == "0", string.Format("Return code wrong: {0}", output.ReturnCode));
            //Assert.IsTrue(output.Refunded == "Y", string.Format("Refunded wrong: {0}", output.Refunded));
            Assert.IsTrue(output.Captured == "Y", string.Format("Captured wrong: {0}", output.Captured));
            Console.WriteLine("Status: Code: {0} Trans Id:{1} {2}", output.ReturnCode, output.TransactionId, DateTime.Now);
            Console.WriteLine("Refund: Amount: {0} Captured:{1} Refunded:{2}", output.Amount, output.Captured, output.Refunded);
        }

        public void TestMobilePayRefund()
        {
            string orderNr = orderId.ToString().PadLeft(12, '0');
            PnConfig configuration = new PnConfig();
            DB.MobilePay.Refund.dacRefund_Output output = PostNord.Portal.DigitalPostage.MobilePay.Proxy.CallRefundSoapService(configuration, certId, appId, orderNr);
            Console.WriteLine("Refund: Code: {0} Trans Id:{1} {2}", output.ReturnCode, output.TransactionId, DateTime.Now);
        }

        //public void TestMobilePayRefundTest()
        //{
        //    DB.MobilePay.TestRefund.dacRefund_Output output = PostNord.Portal.DigitalPostage.MobilePay.Proxy.CallTestRefundSoapService(certId, appId, orderId);
        //    Console.WriteLine("RefundTest: Code: {0} Trans Id:{1} {2}", output.ReturnCode, output.TransactionId, DateTime.Now);
        //}
    }
}
