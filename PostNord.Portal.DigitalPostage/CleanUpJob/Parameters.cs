﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Utility
{
    public class Parameters
    {
        StringDictionary _parameters;
        bool _help = false;

        /// <summary>
        /// Gets or sets a value indicating whether [help].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [help]; otherwise, <c>false</c>.
        /// </value>
        public bool Help
        {
            get { return _help; }
            set { _help = value; }
        }

        public bool Contains(string argument)
        {
            return _parameters.ContainsKey(argument);
        }

        /// <summary>
        /// Populate parameter collection.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public Parameters(string[] args)
        {
            _parameters = new System.Collections.Specialized.StringDictionary();
            for (int i = 0; i < args.Length; i++)
            {
                if (args.Length > i)
                {
                    // Get Command
                    if (args[i].Substring(0, 1) == "-")
                    {
                        if (args.Length < (i + 1))
                        {
                            Console.WriteLine("Command Error. No value found for parameter: {0}", args[i]);
                            _parameters = null;
                            return;
                        }

                        if (args[i].ToLower() == "-h" || args[i].ToLower() == "-?")
                        {
                            _help = true;
                            i++;
                            continue;
                        }

                        if (args.Length > i + 1)
                        {
                            _parameters.Add(args[i].ToLower(), args[i + 1]);
                        }
                        else
                        {
                            Console.WriteLine("Missing value for argument : {0}", args[i]);
                            return;
                        }

                        // Skip next since we just used it
                        i++;
                    }
                    else
                    {
                        Console.WriteLine("Command Error. Commands should start with '-'. Parameter:");
                        Console.WriteLine(args[i]);
                        _parameters = null;
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the parameter.
        /// </summary>
        /// <param name="arguments">The arguments collection</param>
        /// <param name="argument">The named argument.</param>
        /// <returns>
        /// argument if found. If argument is not found nd Exception is thrown
        /// </returns>
        public string GetParameter(string argument)
        {
            if (!_parameters.ContainsKey(argument))
            {
                throw new ArgumentException(string.Format("Argument {0} is missing", argument));
            }
            return _parameters[argument];
        }

        /// <summary>
        /// Gets the parameter.
        /// </summary>
        /// <param name="arguments">The arguments collection</param>
        /// <param name="argument">The named argument.</param>
        /// <returns>
        /// argument if found. If argument is not found an empty string is returned
        /// </returns>
        public string GetOptionalParameter(string argument)
        {
            if (!_parameters.ContainsKey(argument))
            {
                return string.Empty;
            }
            return _parameters[argument];
        }
    }
}
