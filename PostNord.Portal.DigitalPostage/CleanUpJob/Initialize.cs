﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PostNord.Portal.DigitalPostage.Helpers;
using Microsoft.SharePoint;

namespace PostNord.Portal.DigitalPostage.Utility
{
    public class Initialize
    {

        public static void LoadConfig(string siteUrl) {

            ConfigSettings.Values = new System.Collections.Hashtable();

            using(SPSite site = new SPSite(siteUrl))
            {
                using (SPWeb web = site.OpenWeb("/admin"))
                {
                    SPList configList = web.Lists["ConfigSettings"];

                    foreach(SPListItem item in configList.Items)
                    {
                        ConfigSettings.Values.Add(item.Title, (string)item["Value"]);
                    }
                }
            }
        }

        public static void LoadText(string siteUrl)
        {

            //TextSettings.Values = new System.Collections.Hashtable();
            using (SPSite site = new SPSite(siteUrl))
            {
                using (SPWeb web = site.OpenWeb("/admin"))
                {
                    SPList textList = web.Lists["TextSettings"];
                    foreach (SPListItem item in textList.Items)
                    {
                        TextSettings.Values.Add(item.Title, (string)item["Value"]);
                    }
                }
            }
        }

        
        /// <summary>
        /// Initializes the configuration with SQL user
        /// </summary>
        public static void InitConfig()
        {
            ConfigSettings.Values = new System.Collections.Hashtable();
            ConfigSettings.Values.Add("DigitalPostageConnectionString", "Data Source=localhost;Initial Catalog=DigitalPostage;User Id=DigitalPostageTest;Password=!QAZ2wsx;");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsUrl", "https://atapi.postnord.com/Soap/Operations/Ba/Mail/PurchaseDigitalPostageService_v0100");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsUserName", "PwpDigitalPostageUser");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsPassword", "74DpJA8h");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsPaymentTerm", "Z002");
            ConfigSettings.Values.Add("GenerateAndValidateDigitalPostageServiceWsUrl", "https://atapi.postnord.com/Soap/Operations/Ba/Mail/GenerateAndValidateDigitalPostageService_v0100");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsSoldToParty", "10008");
        }

        /// <summary>
        /// Initializes the configuration with Integrated Security
        /// </summary>
        public static void InitIntSecurity()
        {
            ConfigSettings.Values = new System.Collections.Hashtable();
            ConfigSettings.Values.Add("DigitalPostageConnectionString", "Data Source=localhost;Initial Catalog=DigitalPostage;User Id=DigitalPostageTest;Password=!QAZ2wsx;");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsUrl", "https://atapi.postnord.com/Soap/Operations/Ba/Mail/PurchaseDigitalPostageService_v0100");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsUserName", "PwpDigitalPostageUser");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsPassword", "74DpJA8h");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsPaymentTerm", "Z002");
            ConfigSettings.Values.Add("GenerateAndValidateDigitalPostageServiceWsUrl", "https://atapi.postnord.com/Soap/Operations/Ba/Mail/GenerateAndValidateDigitalPostageService_v0100");
            ConfigSettings.Values.Add("PurchaseDigitalPostageServiceWsSoldToParty", "10008");

        }

    }
}
