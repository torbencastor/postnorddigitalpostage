﻿USE [DigitalPostage]
GO

/****** Object:  View [dbo].[ViewReportGroupedOnSalesDate]    Script Date: 02/07/2014 14:37:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ViewReportGroupedOnSalesDate]
AS
SELECT     CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120) AS OrderDate, dbo.Orders.SalesDate, dbo.Products.Name AS ProcuctName, 
                      dbo.SubOrders.Weight, dbo.SubOrders.Priority, SUM(dbo.SubOrders.Amount) AS SubAmount, SUM(dbo.SubOrders.SubOrderSum) AS SubSum, 
                      SUM(dbo.SubOrders.SubOrderCreditSum) AS SubCredit, SUM(dbo.SubOrders.SubOrderSaldo) AS SubSaldo
FROM         dbo.Orders INNER JOIN
                      dbo.SubOrders ON dbo.Orders.Id = dbo.SubOrders.OrderId INNER JOIN
                      dbo.Products ON dbo.SubOrders.ProductId = dbo.Products.Id
GROUP BY dbo.Products.Name, dbo.SubOrders.Weight, dbo.SubOrders.Priority, CONVERT(VARCHAR(10), dbo.Orders.SalesDate, 120), dbo.Orders.SalesDate
ORDER BY dbo.Orders.SalesDate, ProcuctName, dbo.SubOrders.Weight, dbo.SubOrders.Priority

GO
