﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{

    /// <summary>
    /// Message class. Encapsulates access to admin/Messages list
    /// </summary>
    public class Messages
    {

        /// <summary>
        /// Gets the message text.
        /// </summary>
        /// <param name="platform">The platform.</param>
        /// <returns>Message text if found or emtry string</returns>
        static public string GetMessageText(string platform)
        {
            var messageItem = GetMessage(platform);

            if (messageItem != null && messageItem["Message"] != null)
            {
                return messageItem["Message"].ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets the message
        /// </summary>
        /// <param name="platform">The platform.</param>
        /// <returns>The mesaage or null</returns>
        static public SPListItem GetMessage(string platform)
        {
            SPListItem message = null;
            Guid siteId = SPContext.Current.Site.ID;

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite siteElevated = new SPSite(siteId))
                {
                    string propertyKey = PostNord.Portal.DigitalPostage.Helpers.ConfigSettings.GetPropertyBagKey(siteElevated.RootWeb);
                    if (siteElevated.RootWeb.Properties.ContainsKey(propertyKey))
                    {
                        string url = siteElevated.RootWeb.Properties[propertyKey];
                        using (SPWeb adminWeb = siteElevated.OpenWeb(url))
                        {
                            SPList messagesList = adminWeb.Lists["Messages"];

                            SPQuery query = new SPQuery();
                            query.Query = string.Format("<OrderBy><FieldRef Name='FromDate' Ascending='FALSE' /></OrderBy><Where><And><Eq><FieldRef Name='Platform' /><Value Type='Text'>{0}</Value></Eq><Eq><FieldRef Name='Enable' /><Value Type='Boolean'>1</Value></Eq></And></Where>", platform);
                            SPListItemCollection items = messagesList.GetItems(query);
                            if (items.Count > 0)
                            {
                                foreach(SPListItem item in items)
                                {
                                    // if no fromdate we grab this message
                                    if (item["FromDate"] == null)
                                    {
                                        message = item;
                                        break;
                                    }

                                    DateTime fromDate = (DateTime)item["FromDate"];

                                    // if no ToDate we let fromdate decide
                                    if (item["ToDate"] == null)
                                    {
                                        if (DateTime.Now >= fromDate)
                                        {
                                            message = item;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        DateTime toDate = (DateTime)item["ToDate"];
                                        if (DateTime.Now >= fromDate && toDate >= DateTime.Now)
                                        {
                                            message = item;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new ApplicationException("AdminUrl not found!");
                    }
                }
            });
            
            return message;
        }

    }
}
