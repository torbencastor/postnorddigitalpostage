﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Net;
using PostNord.Portal.DigitalPostage.Helpers;
using System.Web.Services.Protocols;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Implements the business logic for handling of Codes
    /// </summary>
    public class Codes
    {
        private static int MAX_CODE_LENGTH = 50;

        private Bll.SubOrders _subOrder = null;

        private string _destination = string.Empty;
        private string _code = string.Empty;

        private long id;

        #region Properties

        /// <summary>
        /// Valid Status values
        /// </summary>
        public enum CodeStatusValues
        {
            /// <summary>
            /// Code is disabled
            /// </summary>
            Disabled = 0,

            /// <summary>
            /// Code has not been used
            /// </summary>
            Unused = 1,
            
            /// <summary>
            /// Code has been used
            /// </summary>
            Used = 2,
            
            /// <summary>
            /// Code has been refunded
            /// </summary>
            Refunded = 3,

            /// <summary>
            /// Code has no value
            /// </summary>
            None = 4
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id
        {
            get { return id; }
            set { }
        }

        public string PostCodeHyphen
        {
            get
            {
                return this.PostCode.Replace(" ", "-");
            }
            set {}
        }

        private string _postCode;
        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>
        /// The post code.
        /// </value>
        public string PostCode
        {
            /*
            get
            {
                if (_code == string.Empty)
                {
                    _code = this.GetCode(this.ExpireDate, this.Price, this.CodeId);
                }
                return _code;
            }
            set {
                _code = value;
            }
             * */
            get{return _postCode;}
            set { _postCode = value; }
        }

        /// <summary>
        /// Gets the sub order identifier.
        /// </summary>
        /// <value>
        /// The sub order identifier.
        /// </value>
        public long SubOrderId { get; set; }

        /// <summary>
        /// Gets the code identifier.
        /// </summary>
        /// <value>
        /// The code identifier.
        /// </value>
        public int CodeId { get; set; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public CodeStatusValues Status { get; set; }

        /// <summary>
        /// Gets the limited status. Used and Unused are returned as None
        /// </summary>
        /// <remarks>
        /// Used to disguise usage for Service staff
        /// </remarks>
        /// <value>
        /// The status limitid. 
        /// </value>
        public CodeStatusValues StatusLimited
        {
            get
            {
                switch (this.Status)
                {
                    case CodeStatusValues.Used:
                    case CodeStatusValues.Unused:
                        return CodeStatusValues.None;
                    default :
                        return this.Status;
                }
            }
        }

        /// <summary>
        /// Gets Code date.
        /// </summary>
        /// <value>
        /// The date the Code was issued.
        /// </value>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// Gets the used date.
        /// </summary>
        /// <value>
        /// The used date.
        /// </value>
        public DateTime? UsedDate { get; set;}

        /// <summary>
        /// Gets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public double Price { get; set; }

        /// <summary>
        /// Gets the expire date.
        /// </summary>
        /// <value>
        /// The expire date.
        /// </value>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// Gets the refunded date.
        /// </summary>
        /// <value>
        /// The refunded date.
        /// </value>
        public DateTime? RefundedDate { get; set; }

        /// <summary>
        /// Gets the refund identifier.
        /// </summary>
        /// <value>
        /// The refund identifier.
        /// </value>
        public long? RefundId { get; set; }


        /// <summary>
        /// Gets a value indicating whether code can be refunded.
        /// </summary>
        /// <value>
        ///   <c>true</c> if code can be refunded otherwise, <c>false</c>.
        /// </value>
        /// <remarks>
        /// Allow Refund even though code has beed used!
        /// </remarks>
        public bool CanRefund
        {
            get
            {
                return (this.Status != CodeStatusValues.Refunded && this.Status != CodeStatusValues.Disabled);
            }
        }

        /// <summary>
        /// Gets the Parent Sub Order.
        /// </summary>
        /// <value>
        /// The Parent Sub Order.
        /// </value>
        public SubOrders SubOrder
        {
            get
            {
                if (_subOrder == null)
                {
                    _subOrder = new SubOrders(this.SubOrderId);
                }
                return _subOrder;
            }
        }

        /// <summary>
        /// Gets the destination from the Suborder.
        /// </summary>
        /// <value>
        /// The destination.
        /// </value>
        public string Destination
        {
            get
            {
                if (_destination == string.Empty)
                {
                    _destination = _subOrder.Destination;
                }
                return _destination;
            }
            set
            {
                _destination = value;
            }
        }

        #endregion

        #region Codes Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Codes"/> class.
        /// </summary>
        /// <remarks>Only used for serialization</remarks>
        public Codes()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Codes"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public Codes(long id)
        {
            using (IDataReader row = Dal.Codes.CodesSelectRow(id))
            {
                if (row.Read())
                {
                    this.Initialize(row);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Codes" /> class.
        /// </summary>
        /// <param name="row">The row.</param>
        public Codes(IDataReader row)
        {
            this.Initialize(row);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Codes" /> class.
        /// </summary>
        /// <param name="postCode">The post code.</param>
        /// <exception cref="System.IndexOutOfRangeException">If postCode is longer than MAX_CODE_LENGTH an exception is thrown</exception>
        public Codes(string postCode)
        {
            if (postCode.Length > MAX_CODE_LENGTH)
            {
                throw new IndexOutOfRangeException(string.Format("PostCode: {0} is too long ",postCode));
            }

            using (IDataReader row = Dal.Codes.CodesSelectCode(ScrambleCode(postCode)))
            {
                if (row.Read())
                {
                    this.Initialize(row);
                    this.PostCode = postCode;
                }
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Codes" /> class and adds it to Database
        /// </summary>
        /// <param name="subOrderId">The sub order identifier.</param>
        /// <param name="price">The price.</param>
        /// <param name="issueDate">The issue date.</param>
        /// <param name="expireDate">The expire date.</param>
        /// <param name="codeId">The code identifier.</param>
        /// <exception cref="System.IndexOutOfRangeException">CodeId was out of range
        /// or
        /// Code length was out of range</exception>
        public Codes(long subOrderId, double price, DateTime issueDate, DateTime expireDate, int codeId, string postCode)
        {
            this.SubOrderId = subOrderId;
            this.CodeId = codeId;
            this.Price = price;
            this.IssueDate = issueDate;
            this.ExpireDate = expireDate;
            this.Status = CodeStatusValues.Disabled;
            this.PostCode = postCode;

            id = Dal.Codes.CodesInsertRow(ScrambleCode(postCode), this.SubOrderId, this.CodeId, (short)this.Status, this.Price, this.IssueDate, this.ExpireDate);
        }


        #endregion

        /// <summary>
        /// Gets the codes from Database. PostCodes are empty
        /// </summary>
        /// <param name="subOrderId">The sub order identifier.</param>
        /// <returns></returns>
        public List<Codes> GetCodes(long subOrderId)
        {
            List<Codes> codes = new List<Codes>();
            using (IDataReader row = Dal.Codes.CodesSelectForSubOrderRow(subOrderId))
            {
                while (row.Read())
                {
                    Codes code = new Codes(row);
                    codes.Add(code);
                }
            }

            if (codes.Count > 0)
            {
                codes = this.GetCodes(codes);
            }
            return codes;
        }

        #region Public Methods

        /// <summary>
        /// Add Add PostCodes to loaded Codes
        /// </summary>
        /// <param name="codes">The codes.</param>
        /// <returns>
        /// List of Codes
        /// </returns>
        /// <exception cref="System.ApplicationException">
        /// Exception is thrown if number of codes returned from CINT does not match or if the Ids differs
        /// </exception>
        private List<Codes> GetCodes(List<Codes> codes)
        {
            StringBuilder csvCodes = new StringBuilder();
            // Create CSV with all Codes Ids
            foreach(Codes code in codes)
            {
                if (csvCodes.Length > 0)
                {
                    csvCodes.Append(",");
                }

                csvCodes.Append(code.CodeId.ToString());
            }

            var ws = this.CreateWebService();

            GenerateAndValidateDigitalPostageServiceWs.ResendCodeRequest resendReq = new GenerateAndValidateDigitalPostageServiceWs.ResendCodeRequest();
            resendReq.CodeID = csvCodes.ToString();
            resendReq.Date = GetPostageNumber(codes[0].ExpireDate);
            resendReq.Value = ConvertPrice(codes[0].Price);

            var codeArr = ws.ResendCode(resendReq);

            if (codeArr.Length != codes.Count)
            {
                throw new ApplicationException(string.Format("CINT returned wrong number of codes! Expected: {0} Received: {1}", codes.Count, codeArr.Length));
            }

            bool testConfig = (ConfigSettings.GetText("TestConfig", "0") == "1");

            foreach(GenerateAndValidateDigitalPostageServiceWs.ResendCodeResponseCodeResponse codeEntry in codeArr)
            {
                var codeFoundList = (from c in codes.OfType<Codes>() where c.CodeId == codeEntry.CodeID select c).ToList<Codes>();
                Codes codeFound = codeFoundList.FirstOrDefault<Codes>();
                if (codeFound == null)
                {
                    throw new ApplicationException(string.Format("CINT returned wrong code: {0}", codeEntry.CodeID));
                }

                codeFound.PostCode = codeEntry.Code.Replace(" ", "-");
                if (testConfig)
                {
                    codeFound.PostCode = codeFound.PostCode.Substring(0, codeEntry.Code.Length - 4) + "TEST";
                }
            }

            return codes;
        }


        /// <summary>
        /// Creates the codes. 
        /// </summary>
        /// <param name="subOrderId">The sub order identifier.</param>
        /// <param name="issueDate">The issue date.</param>
        /// <param name="expireDate">The expire date.</param>
        /// <param name="franking">The franking.</param>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        public List<Codes> CreateCodes(long subOrderId, DateTime issueDate, DateTime expireDate, double franking, int amount, double frankingWithoutVAT)
        {
            List<Codes> codes = new List<Codes>();

            var ws = this.CreateWebService();

            GenerateAndValidateDigitalPostageServiceWs.GenerateCodeRequest genReq = new GenerateAndValidateDigitalPostageServiceWs.GenerateCodeRequest();
            genReq.Date = GetPostageNumber(expireDate);
            genReq.Quantity = amount;
            genReq.Value = ConvertPrice(franking);

            var codeArr = ws.GenerateCode(genReq);

            bool testConfig = (ConfigSettings.GetText("TestConfig", "0") == "1");

            foreach(GenerateAndValidateDigitalPostageServiceWs.GenerateCodeResponseCodeResponse codeEntry in codeArr)
            {
                string postCode = codeEntry.Code.Replace(" ", "-");
                if (testConfig)
                {
                    postCode = postCode.Substring(0, codeEntry.Code.Length - 4) + "TEST";
                }
                Codes newCode = new Codes(subOrderId, franking, issueDate, expireDate, codeEntry.CodeID, postCode);
                codes.Add(newCode);
            }

            return codes;
        }


        /// <summary>
        /// Gets the codes for a given Refund Id
        /// </summary>
        /// <param name="refundId">The sub order identifier.</param>
        /// <returns>
        /// List of Codes
        /// </returns>
        public static List<Codes> GetRefunds(long refundId)
        {
            List<Codes> refundCodes = new List<Codes>();

            using (IDataReader row = Dal.Codes.CodesSelectForRefund(refundId))
            {
                while (row.Read())
                {
                    Codes myOrderLogs = new Codes(row);
                    refundCodes.Add(myOrderLogs);
                }
            }

            return refundCodes;
        }


        /// <summary>
        /// Refunds the Code.
        /// </summary>
        /// <param name="refundId">The refund identifier.</param>
        /// <param name="refundedDate">The refunded date.</param>
        /// <exception cref="System.ApplicationException">Exception is thrown if code is not Unused.</exception>
        public void Refund(long refundId, DateTime refundedDate)
        {
            string error = string.Empty;

            // We allow for Used codes to be refunded!
            if (this.Status != CodeStatusValues.Unused && this.Status != CodeStatusValues.Used && this.Status != CodeStatusValues.None && this.Status != CodeStatusValues.Disabled)
            {
                switch (this.Status)
                {
                    case CodeStatusValues.Refunded:
                        error = string.Format("Code: {0} has already been refunded and cannot be refunded", this.Id);
                        break;
                    //case CodeStatusValues.Used:
                    //    error = string.Format("Code: {0} has been used and cannot be refunded", this.Id);
                    //    break;
                    default:
                        error = string.Format("Code: {0} has unhandled status: {1}", this.Id, this.Status);
                        break;
                }
                throw new ApplicationException(error);
            }

            this.RefundedDate = refundedDate;
            this.RefundId = refundId;
            this.Status = CodeStatusValues.Refunded; 
            Dal.Codes.CodesUpdateRefund(this.Id, (short)this.Status, this.RefundedDate.Value, this.RefundId.Value);
        }

        /// <summary>
        /// Sets the codes as used.
        /// </summary>
        /// <param name="employee">The logged in employee.</param>
        /// <exception cref="System.ApplicationException">Exception is thrown if code is not Unused.</exception>
        public void SetCodeAsUsed(string employee)
        {
            if (this.Status == CodeStatusValues.Disabled)
            {
                throw new ApplicationException("Code is disabled!");
            }

            if (this.Status == CodeStatusValues.Refunded)
            {
                throw new ApplicationException("Code is refunded!");
            }

            if (this.Status != CodeStatusValues.Unused)
            {
                throw new ApplicationException("Code is already in use!");
            }

            Dal.ValidationResults.ValidationResultsInsertRow(this.Id, this.PostCodeHyphen, employee, DateTime.Now, (int)ValidationResults.ValidationValues.OK);
            this.Status = CodeStatusValues.Used;
            this.UsedDate = DateTime.Now;
            Dal.Codes.CodesUpdateUsedDate(this.Id, (short)this.Status, this.UsedDate.Value);
        }

        /// <summary>
        /// Validates this instance. If Validation is OK SetAsUsed() is called and Status is updated to Used.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="updateValidationResults">if set to <c>true</c> [update validation results].</param>
        /// <returns>
        /// Validation result
        /// </returns>
        /// <exception cref="System.ApplicationException">Exception is thrown if result is unknown.</exception>
        public ValidationResults.ValidationValues Validate(string employee, bool updateValidationResults)
        {
            switch (this.Status)
            {
                case CodeStatusValues.Unused:
                    if (this.ExpireDate.Date >= DateTime.Today.Date.AddDays(1).AddSeconds(-1))
                    {
                        // Change status on code and add entry to Validation log
                        if (updateValidationResults)
                        {
                            SetCodeAsUsed(employee);
                        }
                        return ValidationResults.ValidationValues.OK;
                    }
                    else
                    {
                        if (updateValidationResults)
                        {
                            Dal.ValidationResults.ValidationResultsInsertRow(this.Id, this.PostCodeHyphen, employee, DateTime.Now, (int)ValidationResults.ValidationValues.Expired);
                        }
                        return ValidationResults.ValidationValues.Expired;
                    }
                    case CodeStatusValues.Disabled:
                            if (updateValidationResults)
                            {
                                Dal.ValidationResults.ValidationResultsInsertRow(this.Id, this.PostCodeHyphen, employee, DateTime.Now, (int)ValidationResults.ValidationValues.Disabled);
                            }                        
                            return ValidationResults.ValidationValues.Disabled;
                    case CodeStatusValues.Refunded:
                            if (updateValidationResults)
                            {
                                Dal.ValidationResults.ValidationResultsInsertRow(this.Id, this.PostCodeHyphen, employee, DateTime.Now, (int)ValidationResults.ValidationValues.Refunded);
                            }
                            return ValidationResults.ValidationValues.Refunded;
                    case CodeStatusValues.Used:
                            if (updateValidationResults)
                            {
                                Dal.ValidationResults.ValidationResultsInsertRow(this.Id, this.PostCodeHyphen, employee, DateTime.Now, (int)ValidationResults.ValidationValues.AlreadyUsed);
                            }
                            return ValidationResults.ValidationValues.AlreadyUsed;
                    default:
                        throw new ApplicationException(string.Format("Code Status value unknown: {0}",(int)this.Status));

            }
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Dal.Codes.CodesDeleteRow(this.Id);
        }


        /// <summary>
        /// Gets all the orders in date range where code matches given code
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="code">The code.</param>
        /// <returns>
        /// List of Order Objects
        /// </returns>
        public static IDataReader GetOrdersFromCode(DateTime fromDate, DateTime toDate, string code)
        {
            return Dal.Orders.GetOrdersFromCode(fromDate, toDate, ScrambleCode(code));
        }


        /// <summary>
        /// Gets all the codes in date range where code matches given code
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="postCode">The code.</param>
        /// <returns>
        /// List of Code Objects
        /// </returns>
        public static List<Codes> GetCodesFromCode(DateTime fromDate, DateTime toDate, string postCode)
        {
            List<Codes> codesList = new List<Codes>();
            using (IDataReader row = Dal.Orders.GetOrdersFromCode(fromDate, toDate.AddDays(1).AddSeconds(-1), ScrambleCode(postCode)))
            {
                while (row.Read())
                {
                    Codes code = new Codes(row);
                    codesList.Add(code);
                }
            }
            return codesList;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Sets the Object values.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            id = (long)row["Id"];
            this.SubOrderId = (long)row["SubOrderId"];
            this.CodeId = (int)row["CodeId"];
            if (row["UsedDate"] != DBNull.Value)
            {
                this.UsedDate = (DateTime?)row["UsedDate"];
            }
            this.Price = (double)row["Price"];
            this.IssueDate = (DateTime)row["IssueDate"];
            this.ExpireDate = (DateTime)row["ExpireDate"];

            if (row["RefundedDate"] != DBNull.Value)
            { 
                this.RefundedDate = (DateTime?)row["RefundedDate"];
                this.RefundId = (long?)row["RefundId"];
            }

            this.Status = (CodeStatusValues) int.Parse(row["Status"].ToString());
        }


        /// <summary>
        /// Formats the code in the format: NNNN-NNNN-NNNN. 
        /// If the length of the code is different from 12 the code is returned unchanged.
        /// </summary>
        /// <param name="code">The code in the format NNNNNNNNNNNN</param>
        /// <returns>
        /// Formatted code.
        /// </returns>
        public static string FormatCode(string code)
        {
            if (code.Length == 12)
            {
                return code.Insert(4, "-").Insert(9, "-");
            } 
            else
            {
                return code;
            }
        }

        /// <summary>
        /// Creates the web service connection to CINT
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Config Error: GenerateAndValidateDigitalPostageServiceWsUrl is empty</exception>
        private GenerateAndValidateDigitalPostageServiceWs.GenerateAndValidateDigitalPostageService CreateWebService()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolTypeExtensions.Tls12;//use latest TLS version

            // Disable Certficate check
            if (System.Net.ServicePointManager.ServerCertificateValidationCallback == null)
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            }

            GenerateAndValidateDigitalPostageServiceWs.GenerateAndValidateDigitalPostageService gvWs = new GenerateAndValidateDigitalPostageServiceWs.GenerateAndValidateDigitalPostageService();
            
            string url = ConfigSettings.GetText("GenerateAndValidateDigitalPostageServiceWsUrl");
            if (gvWs.Url == string.Empty)
            {
                throw new ApplicationException("Config Error: GenerateAndValidateDigitalPostageServiceWsUrl is empty");
            }

            gvWs.Url = url;

            string wsUserName = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsUserName");
            string wsPassword = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsPassword");

            if (wsUserName == string.Empty || wsPassword == string.Empty)
            {
                throw new ApplicationException("Config Error: GenerateAndValidateDigitalPostageServiceWs username or password is missing");
            }

            GenerateAndValidateDigitalPostageServiceWs.EifHeaders headers = new GenerateAndValidateDigitalPostageServiceWs.EifHeaders();
            gvWs.EifHeadersValue = headers;


            System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
            NetworkCredential netCred = new NetworkCredential(wsUserName, wsPassword);
            myCredentials.Add(new Uri(gvWs.Url), "Basic", netCred);
            gvWs.Credentials = myCredentials;

            return gvWs;
        }

        /// <summary>
        /// Gets the code details.
        /// </summary>
        /// <param name="codeVal">The code value.</param>
        /// <param name="day">The day.</param>
        /// <param name="value">The value.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>
        /// false if error
        /// </returns>
        public bool GetCodeDetails(string codeVal, out int day, out int value, out float amount, out int codeId, out string errorMessage)
        {
            errorMessage = string.Empty;
            string code = codeVal.Replace('-', ' ');

            //StringBuilder decodedPayloadData = new StringBuilder(1000);
            //StringBuilder correctedMobileFrankingCode = new StringBuilder(1000);

            amount = day =  value = codeId = 0;

            var ws = this.CreateWebService();
            GenerateAndValidateDigitalPostageServiceWs.ValidateCodeRequest valReq = new GenerateAndValidateDigitalPostageServiceWs.ValidateCodeRequest();
            valReq.Code = codeVal;

            try
            {
                var valResp = ws.ValidateCode(valReq);

                codeId = valResp.CodeID;
                day = valResp.Date;
                value = valResp.Value;
            }
            catch (SoapException ex)
            {
                // Expected behavior if code is not valid
                if (ex.Message == "Invalid input from the web service.")
                {
                    return false;
                }

                // Unexpected behavior, return error
                errorMessage = ex.Message;
                if (ex.InnerException != null && ex.InnerException.Message != null)
                {
                    errorMessage += " Details: " + ex.InnerException.Message;
                }

                return false;
            }
            catch (Exception ex)
            {
                // Other error - Very unexpected
                errorMessage = string.Format("Unknown error: {0}", ex.Message);
                if (ex.InnerException != null && ex.InnerException.Message != null)
                {
                    errorMessage += " Details: " + ex.InnerException.Message;
                }
                return false;
            }
            return true;
        }


        /// <summary>
        /// Generate postal code
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="franking">The franking in 0,50 kr steps</param>
        /// <param name="codeId">CodeId identifier 1..425910.</param>
        /// <returns>
        /// Generated Post Code
        /// </returns>
        /// <exception cref="System.ApplicationException">Exception is thrown if number of codes found is not 1</exception>
        public string GetCode(DateTime date, double franking, int codeId, double frankingWithoutVAT)
        {
            var ws = CreateWebService();
            var resendReq = new GenerateAndValidateDigitalPostageServiceWs.ResendCodeRequest();

            resendReq.CodeID = codeId.ToString();
            resendReq.Date = GetPostageNumber(date);
            resendReq.Value = ConvertPrice(franking);

            var resendResult = ws.ResendCode(resendReq);

            if (resendResult.Length != 1)
            {
                throw new ApplicationException(string.Format("GetCode Error. Number of Codes found: {0}", resendResult.Length));
            }

            string code = resendResult[0].Code;

            if (ConfigSettings.GetText("TestConfig", "0") == "1")
            {
                code = code.Substring(0, code.Length - 4) + "TEST";
            }

            return code;
        }

        /// <summary>
        /// Scrambles the code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns>
        /// Scrambled Code
        /// </returns>
        /// <exception cref="System.IndexOutOfRangeException">
        /// Code is empty
        /// or code length differs for 12
        /// </exception>
        static public string ScrambleCode(string code)
        {
            if (code.Length == 0)
            {
                throw new IndexOutOfRangeException("Code is empty");
            }
            code = code.Replace("-", "").Replace(" ", "");
            if (code.Length != 12)
            {
                throw new IndexOutOfRangeException(string.Format("Code length is wrong. Expected: 12. Found : {0}", code.Length));
            }
            string res = Helpers.HelperClass.GetHashString(code);
            return res;
        }

        /// <summary>
        /// Convert Price to Code Generator Price
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        static int ConvertPrice(double value)
        {
            return (int)Math.Round(value * 2);
        }

/*
        /// <summary>
        /// Get the danish week number from given date.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <returns>
        /// The Calendar Week number in da-DK
        /// </returns>
        static int WeekNumber(DateTime fromDate)
        {
            // Get jan 1st of the year
            DateTime startOfYear = new DateTime(fromDate.Year, 1, 1);
            // Get dec 31st of the year
            DateTime endOfYear = new DateTime(fromDate.Year, 12, 31);

            // ISO 8601 weeks start with Monday 
            // The first week of a year includes the first Thursday 
            // DayOfWeek returns 0 for sunday up to 6 for saterday
            int[] iso8601Correction = { 6, 7, 8, 9, 10, 4, 5 };
            int nds = fromDate.Subtract(startOfYear).Days + iso8601Correction[(int)startOfYear.DayOfWeek];
            int wk = nds / 7;
            switch (wk)
            {
                case 0:
                    // Return weeknumber of dec 31st of the previous year
                    return WeekNumber(startOfYear.AddDays(-1));
                case 53:
                    // If dec 31st falls before thursday it is week 01 of next year
                    if (endOfYear.DayOfWeek < DayOfWeek.Thursday)
                        return 1;
                    else
                        return wk;
                default: return wk;
            }
        }
*/
        /// <summary>
        /// Get number of weeks between 2 dates.
        /// </summary>
        /// <param name="periodStart">The period start.</param>
        /// <param name="periodEnd">The period end.</param>
        /// <returns>Number of calendar weeks between 2 days</returns>
        static int WeekCount(DateTime periodStart, DateTime periodEnd)
        {
            const DayOfWeek FIRST_DAY_OF_WEEK = DayOfWeek.Monday;
            const DayOfWeek LAST_DAY_OF_WEEK = DayOfWeek.Sunday;
            const int DAYS_IN_WEEK = 7;

            DateTime firstDayOfWeekBeforeStartDate;
            int daysBetweenStartDateAndPreviousFirstDayOfWeek = (int)periodStart.DayOfWeek - (int)FIRST_DAY_OF_WEEK;
            if (daysBetweenStartDateAndPreviousFirstDayOfWeek >= 0)
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-daysBetweenStartDateAndPreviousFirstDayOfWeek);
            }
            else
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-(daysBetweenStartDateAndPreviousFirstDayOfWeek + DAYS_IN_WEEK));
            }

            DateTime lastDayOfWeekAfterEndDate;
            int daysBetweenEndDateAndFollowingLastDayOfWeek = (int)LAST_DAY_OF_WEEK - (int)periodEnd.DayOfWeek;
            if (daysBetweenEndDateAndFollowingLastDayOfWeek >= 0)
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek);
            }
            else
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek + DAYS_IN_WEEK);
            }

            int calendarWeeks = 1 + (int)((lastDayOfWeekAfterEndDate - firstDayOfWeekBeforeStartDate).TotalDays / DAYS_IN_WEEK);

            return calendarWeeks;
        }

        /// <summary>
        /// Gets the postage number. 
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>
        /// The postage week number. (1-366)
        /// </returns>
        /// <remarks>
        /// The Postage number is defined as number of weeks since 1-1-2014. Every 366 weeks (7 years) it is recycled.
        /// </remarks>
        static int GetPostageNumber(DateTime date)
        {
            int MAX_WEEK_COUNT = 366;
            DateTime startDate = new DateTime(2014, 1, 1);

            int weekCount = WeekCount(startDate, date);
            while (weekCount > MAX_WEEK_COUNT)
            {
                weekCount = weekCount - MAX_WEEK_COUNT;
            }
            return weekCount;
        }

        #endregion
    }
}
