﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Encapsulates the OrderLogs business logic
    /// </summary>
    public class OrderLogs
    {
        /// <summary>
        /// Severity Values
        /// </summary>
        public enum SeverityValues
        {
             /// <summary>
             /// Information
             /// </summary>
            Info = 1,
            /// <summary>
            /// Debugging
            /// </summary>
            Debug = 2,
            /// <summary>
            /// Error
            /// </summary>
            Error = 4
        }

        #region Attributes

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        public long OrderId { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the severity.
        /// </summary>
        /// <value>
        /// The severity.
        /// </value>
        public SeverityValues Severity { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text { get; set; }

        #endregion


        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderLogs"/> class.
        /// </summary>
        public OrderLogs()
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderLogs"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public OrderLogs(long id) 
        {
            using (IDataReader row = Dal.OrderLogs.OrderLogsSelectRow(id))
            {
                if (row.Read())
                {
                    this.Initialize(row);
                }
            }
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="OrderLogs"/> class from being created.
        /// </summary>
        /// <param name="row">The row.</param>
        private OrderLogs(IDataReader row)
        {
            this.Initialize(row);
        }

        /// <summary>
        /// Initializes the specified row.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            this.Id = (long)row["Id"];
            this.OrderId = (long)row["OrderId"];
            this.Timestamp = (DateTime)row["Timestamp"];
            this.Text = (string)row["Text"];
            this.Severity = (SeverityValues) int.Parse(row["Severity"].ToString());
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the order logs for the given order
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>
        /// All OrderLogs for the given order
        /// </returns>
        public static List<OrderLogs> GetOrderLogs(long orderId)
        {
            List<OrderLogs> OrderLogsList = new List<OrderLogs>();

            using (IDataReader row = Dal.OrderLogs.OrderLogsSelectForOrder(orderId))
            { 
                while (row.Read())
                {
                    OrderLogs myOrderLogs = new OrderLogs(row);
                    OrderLogsList.Add(myOrderLogs);
                }
            }

            return OrderLogsList;
        }

        /// <summary>
        /// Updates this instance.
        /// </summary>
        public void Save()
        {
            if (this.Id == 0)
            {
                this.Id = Dal.OrderLogs.OrderLogsInsertRow(this.OrderId, this.Timestamp, (short)this.Severity, this.Text);
            }
            else
            {
                Dal.OrderLogs.OrderLogsUpdateRow(this.Id, this.OrderId, this.Timestamp, (short)this.Severity, this.Text);
            }
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Dal.OrderLogs.OrderLogsDeleteRow(this.Id);
        }
        #endregion
    }
}
