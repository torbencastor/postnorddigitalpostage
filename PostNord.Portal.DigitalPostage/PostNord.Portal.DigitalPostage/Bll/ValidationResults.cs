﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Validation Errors
    /// </summary>
    public class ValidationResults
    {

        /// <summary>
        /// Validation Values
        /// </summary>
        public enum ValidationValues
        {
            /// <summary>
            /// Validation is OK
            /// </summary>
            OK = 1,
            /// <summary>
            /// Code has already been used
            /// </summary>
            AlreadyUsed = 2,
            /// <summary>
            /// Code is out of date
            /// </summary>
            Expired = 4,
            /// <summary>
            /// Code has been disabled
            /// </summary>
            Disabled = 8,
            /// <summary>
            /// Code has been refunded
            /// </summary>
            Refunded = 16,
            /// <summary>
            /// Code does not exist
            /// </summary>
            DoesNotExist = 32
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the code identifier.
        /// </summary>
        /// <value>
        /// The code identifier.
        /// </value>
        public long CodeId { get; set; }

        /// <summary>
        /// Gets or sets the code text.
        /// </summary>
        /// <value>
        /// The code text.
        /// </value>
        public string CodeText { get; set; }

        /// <summary>
        /// Gets or sets the employee.
        /// </summary>
        /// <value>
        /// The employee.
        /// </value>
        public string Employee { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        public ValidationValues ErrorCode { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationResults"/> class.
        /// </summary>
        public ValidationResults()
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationResults"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public ValidationResults(long id)
        {
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.ValidationResults.ValidationResultsSelectRow(id))
            {
                if (row.Read())
                {
                    Initialize(row);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationResults"/> class.
        /// </summary>
        /// <param name="row">The row.</param>
        public ValidationResults(IDataReader row)
        {
            this.Initialize(row);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the validation errors.
        /// </summary>
        /// <returns></returns>
        public static List<ValidationResults> GetValidationResults()
        {
            List<ValidationResults> ValidationResultsList = new List<ValidationResults>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.ValidationResults.ValidationResultsSelectAll())
            {
                while (row.Read())
                {
                    ValidationResults myValidationResults = new ValidationResults(row);
                    ValidationResultsList.Add(myValidationResults);
                }
            }
            return ValidationResultsList;
        }

        /// <summary>
        /// Gets the validation errors.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns></returns>
        public static List<ValidationResults> GetValidationResults(DateTime fromDate, DateTime toDate)
        {
            List<ValidationResults> ValidationResultsList = new List<ValidationResults>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.ValidationResults.ValidationResultsSelectDate(fromDate, toDate))
            {
                while (row.Read())
                {
                    ValidationResults myValidationResults = new ValidationResults(row);
                    ValidationResultsList.Add(myValidationResults);
                }
            }
            return ValidationResultsList;
        }

        /// <summary>
        /// Gets the validation errors.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="codeId">The code identifier.</param>
        /// <returns></returns>
        public static List<ValidationResults> GetValidationResults(DateTime fromDate, DateTime toDate, long codeId)
        {
            List<ValidationResults> ValidationResultsList = new List<ValidationResults>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.ValidationResults.ValidationResultsSelectCode(fromDate, toDate, codeId))
            {
                while (row.Read())
                {
                    ValidationResults myValidationResults = new ValidationResults(row);
                    ValidationResultsList.Add(myValidationResults);
                }
            }
            return ValidationResultsList;
        }

        /// <summary>
        /// Validates the code.
        /// </summary>
        /// <param name="codeText">The code text.</param>
        /// <param name="employee">The employee.</param>
        /// <returns></returns>
        public static ValidationValues ValidateCode(string codeText, string employee)
        {
            Codes code = new Codes(codeText);

            if (code.Id == 0)
            {
                Dal.ValidationResults.ValidationResultsInsertRow(0, codeText.Replace(" ", "-"), employee, DateTime.Today, (int)ValidationResults.ValidationValues.DoesNotExist);
                return ValidationResults.ValidationValues.DoesNotExist;
            }
            else
            {
                return code.Validate(employee, true);
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            if (this.Id > 0)
            {
                PostNord.Portal.DigitalPostage.Dal.ValidationResults.ValidationResultsUpdateRow(this.Id, this.CodeId, this.CodeText.Replace(" ", "-"), this.Employee, this.Timestamp, (int)this.ErrorCode);
            }
            else
            {
                this.Id = PostNord.Portal.DigitalPostage.Dal.ValidationResults.ValidationResultsInsertRow(this.CodeId, this.CodeText.Replace(" ", "-"), this.Employee, this.Timestamp, (int)this.ErrorCode);
            }
        }


        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            if (this.Id > 0)
            {
                PostNord.Portal.DigitalPostage.Dal.ValidationResults.ValidationResultsDeleteRow(this.Id);
                this.Id = 0;
            }
        }
        #endregion


        #region Private Methods

        /// <summary>
        /// Initializes the specified row.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            this.Id = (long)row["Id"];
            this.CodeId = (long)row["CodeId"];
            this.CodeText = (string)row["CodeText"];
            this.Employee = (string)row["Employee"];
            this.Timestamp = (DateTime)row["Timestamp"];
            this.ErrorCode = (ValidationValues)int.Parse(row["ErrorCode"].ToString());
        }
        
        #endregion

    }
}
