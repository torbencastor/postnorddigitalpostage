﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Handles product names
    /// </summary>
    public class Products
    {
        private static string CACHE_KEY = "DigitalStampProducts";
        private static object lockKey = new object();

        #region Attributes

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets all products. Values are cached.
        /// </summary>
        /// <value>
        /// All products.
        /// </value>
        public List<Products> AllProducts
        {
          get {

            if (HttpContext.Current == null)
            {
                return GetProductsSelectAll();
            }

            object cachedVersion = null;
            try
            {
                cachedVersion = HttpContext.Current.Cache.Get(CACHE_KEY);
            }
            catch 
            {}

            if (cachedVersion != null)
            {
                return (List<Products>) cachedVersion;
            }

            List<Products> allProducts = GetProductsSelectAll();
            lock (lockKey)
            {
                HttpContext.Current.Cache.Add(CACHE_KEY, allProducts, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Default, null);
            }
            return allProducts;
          }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Products"/> class.
        /// </summary>
        public Products()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Products"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <exception cref="System.IndexOutOfRangeException">Exception is thrown if id is not found</exception>
        public Products(long id)
        {
            Products prod = AllProducts.FirstOrDefault( s => s.Id == id);

            if (prod == null || prod.Id != id)
            {
                InvalidateCache();
                prod = AllProducts.FirstOrDefault(s => s.Id == id);
                if (prod == null || prod.Id != id)
                {
                    throw new IndexOutOfRangeException(string.Format("Product with ID: {0} was not found", id));
                }
            }
            this.Id = prod.Id;
            this.Name = prod.Name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Products"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <exception cref="System.IndexOutOfRangeException">Exception is thrown if name is not found</exception>
        public Products(string name)
        {
            Products prod = AllProducts.FirstOrDefault(s => s.Name == name);

            if (prod == null)
            {
                this.Name = name;
                this.Save();
                InvalidateCache();
            }
            else
            {
                this.Id = prod.Id;
                this.Name = prod.Name;
            }
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="Products"/> class from being created.
        /// </summary>
        /// <param name="row">The row.</param>
        private Products(IDataReader row)
        {
            this.Id = (long) row["Id"];
            this.Name = (string) row["Name"];
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the product identifier.
        /// </summary>
        /// <param name="name">The product name.</param>
        /// <returns>
        /// Product Id for given name
        /// </returns>
        public static long GetProductId(string name)
        {
            Products p = new Products(name);
            return p.Id;
        }

        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns>Name of product</returns>
        public static string GetProduct(long productId)
        {
            Products p = new Products(productId);
            return p.Name;
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save() 
        {
            if (this.Id == 0)
            {
                this.Id = PostNord.Portal.DigitalPostage.Dal.Products.ProductsInsertRow(this.Name);
            }
            else
            {
                PostNord.Portal.DigitalPostage.Dal.Products.ProductsUpdateRow(this.Id, this.Name);
            }
            InvalidateCache();
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            if (this.Id > 0)
            {
                PostNord.Portal.DigitalPostage.Dal.Products.ProductsDeleteRow(this.Id);
                Id = 0;
                Name = string.Empty;
                InvalidateCache();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Invalidates the cache.
        /// </summary>
        private void InvalidateCache()
        {
            if (HttpContext.Current != null && HttpContext.Current.Cache[CACHE_KEY] != null)
            {
                HttpContext.Current.Cache.Remove(CACHE_KEY);
            }
        }

        /// <summary>
        /// Gets the products select all.
        /// </summary>
        /// <returns></returns>
        private static List<Products> GetProductsSelectAll()
        {
            List<Products> ProductsList = new List<Products>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.Products.ProductsSelectAll())
            {
                while (row.Read())
                {
                    Products myProducts = new Products(row);
                    ProductsList.Add(myProducts);
                }
            }
            return ProductsList;
        }

        /// <summary>
        /// Select Products row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        private static Products ProductsSelectRow(long id)
        {
            Products product = null;
            using (IDataReader dr = PostNord.Portal.DigitalPostage.Dal.Products.ProductsSelectRow(id))
            {
                product = new Products(dr);
            }
            return product;
        }

        #endregion
    
    }
}
