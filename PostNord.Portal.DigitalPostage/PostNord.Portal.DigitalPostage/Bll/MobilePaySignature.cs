﻿using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Pkix;
using Org.BouncyCastle.Utilities.Collections;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.X509.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    public class MobilePaySignature
    {

        /// <summary>
        /// Verifies the sinature.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="certificate">The certificate.</param>
        /// <param name="signature">The signature.</param>
        /// <returns></returns>
        public static bool VerifySignature(byte[] data, byte[] certificate, byte[] signature)
        {
            //1. Find the signer certificate
            //2. Validate the signerinfo with the certificate
            //3. Validate the signer certificate chain to the trusted root
            //4. Validate the signed data matches the expected data
            try
            {
                CmsSignedData signedData = new CmsSignedData(signature);
                X509CertificateParser certParser = new X509CertificateParser();
                var rootCert = certParser.ReadCertificate(certificate);

                var certsInSignature = signedData.GetCertificates("Collection");
                var certsCollection = certsInSignature.GetMatches(null).Cast<X509Certificate>();

                SignerInformationStore signers = signedData.GetSignerInfos();
                var valid = false;
                foreach (var signer in signers.GetSigners())
                {
                    var signerInfo = (SignerInformation)signer;

                    //1. Find the signer certificate
                    var certSignedWith = certsInSignature.GetMatches(signerInfo.SignerID).Cast<X509Certificate>().FirstOrDefault();

                    //2. Validate the signerinfo with the certificate
                    var signerValid = signerInfo.Verify(certSignedWith);

                    //3. Validate the signer certificate chain to the trusted root
                    var chain = BuildCertificateChain(rootCert, certSignedWith, certsCollection);

                    valid = valid || signerValid;
                }

                //4. Validate the signed data matches the expected data
                var signedBytes = (byte[])((CmsProcessableByteArray)signedData.SignedContent).GetContent();
                var validData = Enumerable.SequenceEqual(signedBytes, data);
                return valid && validData;
            }
            catch 
            {
                return false;
            }
        }

        /// <summary>
        /// Builds the certificate chain.
        /// </summary>
        /// <param name="root">The root.</param>
        /// <param name="primary">The primary.</param>
        /// <param name="additional">The additional.</param>
        /// <returns></returns>
        private static IEnumerable<X509Certificate> BuildCertificateChain(X509Certificate root, X509Certificate primary, IEnumerable<X509Certificate> additional)
        {
            PkixCertPathBuilder builder = new PkixCertPathBuilder();

            // Separate root from itermediate
            List<X509Certificate> intermediateCerts = new List<X509Certificate>();
            HashSet rootCerts = new HashSet();

            foreach (var cert in additional)
            {
                intermediateCerts.Add(cert);
            }

            // Create chain for this primary certificate
            X509CertStoreSelector holder = new X509CertStoreSelector();
            holder.Certificate = primary;

            //Root certificate
            rootCerts.Add(new TrustAnchor(root, null));

            PkixBuilderParameters builderParams = new PkixBuilderParameters(rootCerts, holder);
            builderParams.IsRevocationEnabled = false;

            X509CollectionStoreParameters intermediateStoreParameters =
                new X509CollectionStoreParameters(intermediateCerts);

            builderParams.AddStore(X509StoreFactory.Create(
                "Certificate/Collection", intermediateStoreParameters));

            PkixCertPathBuilderResult result = builder.Build(builderParams); //Throws exception in case of invalid chain

            return result.CertPath.Certificates.Cast<X509Certificate>();
        }
    }
}
