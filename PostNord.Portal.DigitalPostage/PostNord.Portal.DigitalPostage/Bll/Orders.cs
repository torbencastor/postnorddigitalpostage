﻿using Microsoft.SharePoint;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PostNord.Portal.DigitalPostage.Helpers;
using System.IO;
using Microsoft.SharePoint.Utilities;
using System.Xml.Serialization;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using PostNord.Portal.Tools.Labels;
using System.Net;
using System.Web.Services.Protocols;
using PN.SoapLibrary.WSSecurity;
using PostNord.Portal.DigitalPostage.NetAxept;

namespace PostNord.Portal.DigitalPostage.Bll
{

    /// <summary>
    /// Handles to Orders class
    /// </summary>
    public class Orders
    {

        public const string WEBSHOP_NAME = "WebShop";
        public const int WEBSHOP_VERSION = 2;

        private List<SubOrders> _suborders = null;
        private Guid _siteId = Guid.Empty;

        /// <summary>
        /// CreditCard Info Data
        /// </summary>
        public class CreditCardInfo
        {
            /// <summary>
            /// Gets or sets the issuer.
            /// </summary>
            /// <value>
            /// The issuer.
            /// </value>
            public string Issuer { get; set; }

            /// <summary>
            /// Gets or sets the scrambled data.
            /// </summary>
            /// <value>
            /// The scrambled data.
            /// </value>
            public string ScrambledData { get; set; }

            /// <summary>
            /// Gets or sets the expiry date.
            /// </summary>
            /// <value>
            /// The expiry date.
            /// </value>
            public string ExpiryDate { get; set; }

            /// <summary>
            /// Gets or sets the credit card hash.
            /// </summary>
            /// <value>
            /// The credit card hash.
            /// </value>
            public string CreditCardHash { get; set; }

            /// <summary>
            /// Gets or sets the error code.
            /// </summary>
            /// <value>
            /// The error code.
            /// </value>
            public string ErrorCode { get; set; }

            /// <summary>
            /// Gets or sets the error message.
            /// </summary>
            /// <value>
            /// The error message.
            /// </value>
            public string ErrorMessage { get; set; }


            public CreditCardInfo(string errorCode, string errorMessage)
            {
                this.ErrorCode = errorCode;
                this.ErrorMessage = errorMessage;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="CreditCardInfo"/> class.
            /// </summary>
            /// <param name="issuer">The issuer.</param>
            /// <param name="scrambledData">The scrambled data.</param>
            /// <param name="expiryDate">The expiry date.</param>
            public CreditCardInfo(string issuer, string scrambledData, string expiryDate)
            {
                this.Issuer = issuer;
                this.ScrambledData = scrambledData;
                this.ExpiryDate = expiryDate;
                this.CreditCardHash = string.Empty;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="CreditCardInfo"/> class.
            /// </summary>
            /// <param name="issuer">The issuer.</param>
            /// <param name="scrambledData">The scrambled data.</param>
            /// <param name="expiryDate">The expiry date.</param>
            /// <param name="creditCardHash">The credit card hash.</param>
            public CreditCardInfo(string issuer, string scrambledData, string expiryDate, string creditCardHash)
            {
                this.Issuer = issuer;
                this.ScrambledData = scrambledData;
                this.ExpiryDate = expiryDate;
                this.CreditCardHash = creditCardHash;
            }



        }

        #region Config Settings

        const short DEFAULT_POSTAGE_VALID_IN_DAYS = 180;
        const short DEFAULT_POSTAGE_MOBILE_VALID_IN_DAYS = 14;
        const short DEFAULT_POSTAGE_MOBILE_VALID_IN_DAYS_IN_APP = 7;

        /// <summary>
        /// Get Number of days Postage is valid in in days.
        /// </summary>
        /// <returns>
        /// Number of days postage is valid
        /// </returns>
        public static short PostageValidInDays
        {
            get
            {
                short postageValidInInDays = DEFAULT_POSTAGE_VALID_IN_DAYS;
                short.TryParse(Portal.DigitalPostage.Helpers.ConfigSettings.GetText("PostageValidInDays"), out postageValidInInDays);
                return postageValidInInDays;
            }
        }


        /// <summary>
        /// Get Number of days App Postage is valid in in days.
        /// </summary>
        /// <returns>
        /// Number of days App postage is valid
        /// </returns>
        public static short PostageMobileValidInDays
        {
            get
            {
                short postageValidInInDays = DEFAULT_POSTAGE_MOBILE_VALID_IN_DAYS;
                short.TryParse(Portal.DigitalPostage.Helpers.ConfigSettings.GetText("PostageMobileValidInDays"), out postageValidInInDays);
                return postageValidInInDays;
            }
        }

        /// <summary>
        /// Get Number of days App Postage is valid in in days.
        /// </summary>
        /// <returns>
        /// Number of days App postage is valid
        /// </returns>
        public static short PostageMobileValidInDaysInApp
        {
            get
            {
                short postageValidInInDays = DEFAULT_POSTAGE_MOBILE_VALID_IN_DAYS_IN_APP;
                short.TryParse(Portal.DigitalPostage.Helpers.ConfigSettings.GetText("PostageMobileValidInDaysInApps"), out postageValidInInDays);
                return postageValidInInDays;
            }
        }

        private string MerchantId
        {
            get
            {
                return Portal.DigitalPostage.Helpers.ConfigSettings.GetText("MerchantId");
            }
        }

        private string Token
        {
            get
            {
                return Portal.DigitalPostage.Helpers.ConfigSettings.GetText("Token");
            }
        }
        private string TerminalUrl
        {
            get
            {
                return Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TerminalUrl");
            }
        }

        private string NetaxptWsUrl
        {
            get
            {
                return Portal.DigitalPostage.Helpers.ConfigSettings.GetText("NetaxeptWebserviceUrl");
            }
        }

        #endregion

        #region Attributes

        /// <summary>
        /// Defined Payment Methods
        /// </summary>
        public enum PaymentMethods
        {
            /// <summary>
            /// Unknown payment. Initial value
            /// </summary>
            Unknown = 0,
            /// <summary>
            /// NetAxcept payment
            /// </summary>
            Nets = 1,
            /// <summary>
            /// NetAxcept Recurring Payment
            /// </summary>
            NetsRecurring = 2,
            /// <summary>
            /// NetAxcept MobilePay Payment
            /// </summary>
            MobilePay = 3
        }

        /// <summary>
        /// Valid Status values
        /// </summary>
        public enum OrderStatusValues
        {
            /// <summary>
            /// Order is created but not finished
            /// </summary>
            Initialized = 0,

            /// <summary>
            /// Ready for payment with creditcard
            /// </summary>
            ReadyForPayment = 1,

            /// <summary>
            /// Paid
            /// </summary>
            Paid = 2,

            /// <summary>
            /// All Suborders are credited
            /// </summary>
            Credited = 3,

            /// <summary>
            /// Ready for Payment, save CreditCard information
            /// </summary>
            ReadyForPaymentCardSave = 4,

            /// <summary>
            /// Ready for Payment with reused Credit card
            /// </summary>
            ReadyForPaymentCreditCardReused = 5,

            /// <summary>
            /// Ready for Payment with MobilePay
            /// </summary>
            ReadyForPaymentMobilePay = 6,

            /// <summary>
            /// Payment refunded. Order not sent to CINT
            /// </summary>
            /// <remarks>
            /// Payment has been refunded manually, differs from credited since CINT has never been informed about the order.
            /// Added in version 2.3.3, due to MobilePay payment errors.
            ///</remarks>
            Refunded = 7,

            /// <summary>
            /// Payment cancelled 
            /// </summary>
            /// <remarks>
            /// Payment has been cancelled in MobilePay
            /// </remarks>
            Cancelled = 8

        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// MobilePay Order numbers should hold 12 chars
        /// </summary>
        /// <value>
        /// The mobile pay order nr. with 12 chars.
        /// </value>
        public string MobilePayOrderNr
        {
            get
            {
                return this.Id.ToString().PadLeft(12, '0');
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the receipt number.
        /// </summary>
        /// <value>
        /// The receipt number.
        /// </value>
        public string ReceiptNumber { get; set; }

        /// <summary>
        /// Gets or sets the capture identifier.
        /// </summary>
        /// <value>
        /// The capture identifier.
        /// </value>
        public string CaptureId { get; set; }

        /// <summary>
        /// Gets or sets the label image.
        /// </summary>
        /// <value>
        /// The label image.
        /// </value>
        public byte[] LabelImage { get; set; }

        /// <summary>
        /// Gets or sets the size of the image.
        /// </summary>
        /// <value>
        /// The size of the image.
        /// </value>
        public int ImageSize
        {
            get
            {
                if (this.LabelImage == null)
                {
                    return 0;
                }
                return this.LabelImage.Length;
            }
        }

        /// <summary>
        /// Gets or sets the sales date.
        /// </summary>
        /// <value>
        /// The sales date.
        /// </value>
        public DateTime SalesDate { get; set; }

        /// <summary>
        /// Gets or sets the expire date.
        /// </summary>
        /// <value>
        /// The expire date.
        /// </value>
        public DateTime ExpireDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the order sum.
        /// </summary>
        /// <value>
        /// The order sum.
        /// </value>
        public double OrderSum
        {
            get
            {
                // If Order has not been saved there will be no SubOrders
                if (this.Id == 0)
                {
                    return 0;
                }
                double sum = 0;
                foreach (SubOrders subOrder in this.SubOrders)
                {
                    sum += subOrder.SubOrderSum;
                }
                return sum;
            }
            // This is done to enable serialization
            set { }
        }

        /// <summary>
        /// Gets the credit sum.
        /// </summary>
        /// <value>
        /// The credit sum.
        /// </value>
        public double CreditSum
        {
            get
            {
                // If Order has not been saved there will be no SubOrders
                if (this.Id == 0)
                {
                    return 0;
                }
                double sum = 0;
                foreach (SubOrders subOrder in this.SubOrders)
                {
                    sum += subOrder.SubOrderCreditSum;
                }
                return sum;
            }
            // This is done to enable serialization
            set { }
        }

        /// <summary>
        /// Gets the saldo.
        /// </summary>
        /// <value>
        /// The saldo.
        /// </value>
        public double Saldo
        {
            get
            {
                return this.OrderSum - this.CreditSum;
            }
            // This is done to enable serialization
            set { }
        }


        private int _paymentMethod
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the payment method.
        /// </summary>
        /// <value>
        /// The payment method.
        /// </value>
        [XmlIgnore]
        public PaymentMethods PaymentMethod
        {
            get
            {
                return (PaymentMethods)Enum.Parse(typeof(PaymentMethods), _paymentMethod.ToString());
            }
            set { }
        }

        /// <summary>
        /// Gets the payment.
        /// </summary>
        /// <value>
        /// The payment.
        /// </value>
        public string Payment
        {
            get
            {
                switch (this._paymentMethod)
                {
                    case 0:
                        return "Unknown";
                    case 1:
                        return "Nets";
                    case 2:
                        return "NetsRecurring";
                    case 3:
                        return "MobilePay";
                    default:
                        return string.Format("Wrong value: {0}", _paymentMethod);
                }
            }
            set { }
        }


        private int _appTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the application version.
        /// </summary>
        /// <value>
        /// The application version.
        /// </value>
        public int AppVersion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the application platform.
        /// </summary>
        /// <value>
        /// The application platform.
        /// </value>
        public string AppPlatform
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether order has been transferred to CINT
        /// </summary>
        /// <value>
        ///   <c>true</c> if [sent to cint]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool SentToCint
        {
            get;
            set;
        }

        [XmlIgnore]
        public bool IsOldVersion
        {
            get
            {
                return (this.AppPlatform == WEBSHOP_NAME && this.AppVersion == 1);
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public OrderStatusValues Status { get; set; }

        /// <summary>
        /// Subs the orders.
        /// </summary>
        /// <value>
        /// The sub orders.
        /// </value>
        /// <returns>
        /// List containing all SubOrders created for this order.
        ///   </returns>
        /// <exception cref="IndexOutOfRangeException">Order Id is zero</exception>
        public List<SubOrders> SubOrders
        {
            get
            {
                if (this.Id == 0)
                {
                    throw new IndexOutOfRangeException("Order Id is zero");
                }

                if (_suborders == null)
                {
                    _suborders = DigitalPostage.Bll.SubOrders.GetOrderSubOrders(this.Id);
                }

                return _suborders;
            }
            // This is done to enable serialization
            set
            {
            }
        }

        /// <summary>
        /// Get all Logs for order.
        /// </summary>
        /// <returns>List containing all Log entries for this order</returns>
        /// <exception cref="IndexOutOfRangeException">Order Id is zero</exception>
        [XmlIgnore]
        public List<OrderLogs> Logs
        {
            get
            {
                if (this.Id == 0)
                {
                    throw new IndexOutOfRangeException("Order Id is zero");
                }
                return DigitalPostage.Bll.OrderLogs.GetOrderLogs(this.Id);
            }
            set { }
        }

        /// <summary>
        /// Gets the refunds.
        /// </summary>
        /// <value>
        /// The refunds.
        /// </value>
        /// <exception cref="System.IndexOutOfRangeException">Order Id is zero</exception>
        public List<Refunds> Refunds
        {
            get
            {
                if (this.Id == 0)
                {
                    throw new IndexOutOfRangeException("Order Id is zero");
                }
                return DigitalPostage.Bll.Refunds.GetRefunds(this.Id);
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Orders"/> class.
        /// </summary>
        public Orders()
        {
            this.ReceiptNumber = string.Empty;
            this.CaptureId = string.Empty;
            this.Email = string.Empty;
            this.SalesDate = DateTime.Now;
            this.Status = OrderStatusValues.Initialized;
            this._appTypeId = 0;
            this._paymentMethod = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Orders" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public Orders(long id)
        {
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.Orders.OrdersSelectRow(id))
            {
                if (row.Read())
                {
                    this.Initialize(row);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Orders" /> class.
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <exception cref="System.ApplicationException">CaptureId does not match the Order</exception>
        public Orders(string transactionId)
        {

            using (NetAxept.Netaxept client = new NetAxept.Netaxept())
            {
                client.Url = this.NetaxptWsUrl;
                NetAxept.QueryRequest request = new NetAxept.QueryRequest();
                request.TransactionId = transactionId;

                NetAxept.QueryResponse response = client.Query(this.MerchantId, this.Token, request);

                // The response needs to be casted to an instance of payment info
                NetAxept.PaymentInfo paymentInfo = (NetAxept.PaymentInfo)response;

                // Load Order
                long orderNumber = long.Parse(paymentInfo.OrderInformation.OrderNumber);

                using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.Orders.OrdersSelectRow(orderNumber))
                {
                    if (row.Read())
                    {
                        this.Initialize(row);
                    }
                }

                if (this.Id == 0)
                {
                    throw new ApplicationException("CaptureId does not match the Order. Order not found.");
                }

                if (this.CaptureId != transactionId)
                {
                    throw new ApplicationException("CaptureId does not match the Order!");
                }
            }
        }


        public PaymentInfo GetNetsPaymentInfo()
        {
            PaymentInfo paymentInfo = null;

            try
            {
                using (NetAxept.Netaxept saleClient = new NetAxept.Netaxept())
                {
                    saleClient.Url = ConfigSettings.GetText("NetaxeptWebserviceUrl");
                    int netsTimeOut = 10000;
                    int.TryParse(ConfigSettings.GetText("NetsTimeOut", "10000"), out netsTimeOut);
                    saleClient.Timeout = netsTimeOut;

                    NetAxept.QueryRequest qReq = new NetAxept.QueryRequest();
                    qReq.TransactionId = this.CaptureId;
                    paymentInfo = (NetAxept.PaymentInfo)saleClient.Query(this.MerchantId, this.Token, qReq);
                }
            }
            catch (System.Net.WebException ex)
            {
                this.AddLogError(string.Format("WebException - GetOrderStatus: {0} Message: {1} Details:{2}", ex.Status, ex.Message, ex.InnerException != null ? ex.InnerException.Message : ""));
                throw new ApplicationException(GetErrorMessage(ex));
            }
            catch (Exception ex)
            {
                string error = string.Format("Error Getting Order Status. {0}. ", ex.Message);
                if (ex.InnerException != null)
                {
                    error += ex.InnerException.Message;
                }
                this.AddLogError(error);
                throw new ApplicationException(error);
            }

            return paymentInfo;
        }


        /// <summary>
        /// Gets the order status.
        /// </summary>
        /// <returns>
        /// CreditCardInfo
        /// </returns>
        public CreditCardInfo GetOrderStatus()
        {
            CreditCardInfo cardInfo = null;

            try
            {
                using (NetAxept.Netaxept saleClient = new NetAxept.Netaxept())
                {
                    saleClient.Url = ConfigSettings.GetText("NetaxeptWebserviceUrl");
                    int netsTimeOut = 10000;
                    int.TryParse(ConfigSettings.GetText("NetsTimeOut", "10000"), out netsTimeOut);
                    saleClient.Timeout = netsTimeOut;

                    NetAxept.QueryRequest qReq = new NetAxept.QueryRequest();
                    qReq.TransactionId = this.CaptureId;
                    NetAxept.PaymentInfo paymentInfo = (NetAxept.PaymentInfo)saleClient.Query(this.MerchantId, this.Token, qReq);

                    cardInfo = new CreditCardInfo(paymentInfo.CardInformation.Issuer, paymentInfo.CardInformation.MaskedPAN, paymentInfo.CardInformation.ExpiryDate);
                    if (!string.IsNullOrEmpty(paymentInfo.CardInformation.PanHash))
                    {
                        // If we have a hashcode its encrypted. We therefore load it from OrderId 
                        Cards crd = new Cards(this.Id);
                        if (crd.Id != Guid.Empty)
                        {
                            cardInfo.CreditCardHash = crd.Id.ToString();
                        }
                    }
                }
            }
            catch (System.Net.WebException ex)
            {
                this.AddLogError(string.Format("WebException - GetOrderStatus: {0} Message: {1} Details:{2}", ex.Status, ex.Message, ex.InnerException != null ? ex.InnerException.Message : ""));
                throw new ApplicationException(GetErrorMessage(ex));
            }
            catch (Exception ex)
            {
                string error = string.Format("Error Getting Order Status. {0}. ", ex.Message);
                if (ex.InnerException != null)
                {
                    error += ex.InnerException.Message;
                }
                this.AddLogError(error);
                throw new ApplicationException(error);
            }

            return cardInfo;
        }

        /// <summary>
        /// Checks status mobile pay payment.
        /// </summary>
        /// <returns>OK if order is paid, otherwise an error string</returns>
        /// <exception cref="System.ApplicationException">
        /// MobilePayMerchantId not found. Please add to Configuration
        /// or
        /// MobilePayClientId not found. Please add to Configuration
        /// </exception>
        public string CheckMobilePay()
        {
            string status = string.Empty;

            PnConfig configuration = this.GetPnConfig();

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                this.AddLogError("MobilePayMerchantId not found. Unable to validate Mobile Payment.");
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                this.AddLogError("MobilePayClientId not found. Unable to validate Mobile Payment.");
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            var dbOutput = MobilePay.Proxy.CallGetStatus(configuration, clientId, merchantId, this.MobilePayOrderNr);

            // if Return Code != 0 accept that WS is not used
            if (dbOutput.ReturnCode != Helpers.MobilePay.RETURNCODE_OK)
            {
                status = string.Format("MobilePay Error! Error code: {0} ({1}) Reason code:{2} ({3})", dbOutput.ReturnCode, Helpers.MobilePay.V2Returncode(dbOutput.ReturnCode), dbOutput.ReasonCode, Helpers.MobilePay.V2Reasoncode(dbOutput.ReasonCode));
            }
            else if (dbOutput.LatestPaymentStatus == Helpers.MobilePay.PAYMENTSTATUS_CAPTURED || dbOutput.LatestPaymentStatus == Helpers.MobilePay.PAYMENTSTATUS_RESERVED)
            {
                status = "OK";
                this.CaptureId = dbOutput.OriginalTransactionId;
                this.Save();
            }
            else
            {
                status = dbOutput.LatestPaymentStatus;
            }
            return status;
        }

        /// <summary>
        /// Checks the mobile pay signature.
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <param name="mobilePaySignature">The mobile pay signature.</param>
        /// <returns></returns>
        public bool CaptureMobilePay(string transactionId, string mobilePaySignature)
        {
            int retryMobilePayTimeout = 5000;
            int.TryParse(ConfigSettings.GetText("MobilePayRetryValidationTimeOut", "6000"), out retryMobilePayTimeout);

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                this.AddLogError("MobilePayMerchantId not found. Unable to validate Mobile Payment.");
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                this.AddLogError("MobilePayClientId not found. Unable to validate Mobile Payment.");
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            PnConfig configuration = this.GetPnConfig();

            //if (!string.IsNullOrEmpty(transactionId))
            //{
            //    this.CaptureId = transactionId;
            //    this.Save();
            //}

            var isValidSignature = false;
            bool skipMobilePayValidation = false;

            if (bool.Parse(ConfigSettings.GetText("MobilePayDisableValidation", "false")))
            {
                isValidSignature = true;
                this.AddLog("MobilePayDisableValidation enabled. Signature now valid.");
            }

            if (bool.Parse(ConfigSettings.GetText("AcceptTestMerchantId", "false")) && merchantId == "APPDK0000000000")
            {
                skipMobilePayValidation = true;
                this.AddLog("AcceptTestMerchantId enabled. Skipping WS test");
            }

            // If we have transactionID and MP Signature we will validate it
            if (!string.IsNullOrEmpty(transactionId) && !string.IsNullOrEmpty(mobilePaySignature) && transactionId != "903506140")
            {
                //Validate
                var validator = new MobilePaySignatureValidator();
                isValidSignature = validator.ValidateSignature(mobilePaySignature, this.MobilePayOrderNr, merchantId, transactionId, String.Format("{0:0.00}", this.OrderSum));
                this.CaptureId = transactionId;
                this.Save();
                this.AddLog(string.Format("Signature Validation: Order Nr: {0} Valid: {1}", this.MobilePayOrderNr, isValidSignature));
            }
            else if (this.CaptureId == "903506140" && merchantId == "APPDK0000000000")
            {
                isValidSignature = true;
                skipMobilePayValidation = true;
                this.AddLog("Unit test - Signature Validation Skipped.");
            }
            else if (isValidSignature && skipMobilePayValidation)
            {
                this.AddLog("Testing -  Signature Validation Skipped");
            }
            else
            {
                this.AddLog("No Signature or transactionId. Forcing MobilePay validation");
                isValidSignature = true;
                skipMobilePayValidation = false;
            }

            if (isValidSignature)
            {

                // if AcceptTestMerchantId skip MobilePay testing
                if (skipMobilePayValidation == false)
                {
                    this.AddLog(string.Format("Calling DB WS. MerchantId: {0} ClientId:{1}", merchantId, clientId));
                    try
                    {
                        var dbOutput = MobilePay.Proxy.CallGetStatus(configuration, clientId, merchantId, this.MobilePayOrderNr);
                        this.AddLog(string.Format("WS called. ReturnCode: {0} ReasonCode: {1}", dbOutput.ReturnCode, dbOutput.ReasonCode));

                        // If validation fails retry after timeout
                        if (dbOutput.ReturnCode == Helpers.MobilePay.RETURNCODE_GENERAL_ERROR && dbOutput.ReasonCode == Helpers.MobilePay.REASONCODE_TIMEOUT && retryMobilePayTimeout > 0)
                        {
                            this.AddLogError(string.Format("Timeout. Retrying in {0} ms.", retryMobilePayTimeout));
                            System.Threading.Thread.Sleep(retryMobilePayTimeout);
                            dbOutput = MobilePay.Proxy.CallGetStatus(configuration, clientId, merchantId, this.MobilePayOrderNr);
                        }

                        // if Return Code != 0 Log Error description
                        if (dbOutput.ReturnCode != Helpers.MobilePay.RETURNCODE_OK)
                        {
                            string errorDesc = string.Format("ReturnCode: {0} ({1}) Reason: {2} ({3})", dbOutput.ReturnCode, Helpers.MobilePay.V2Returncode(dbOutput.ReturnCode), dbOutput.ReasonCode, Helpers.MobilePay.V2Reasoncode(dbOutput.ReasonCode));

                            // If we dont have validated signature we return error
                            if (string.IsNullOrEmpty(transactionId) && string.IsNullOrEmpty(mobilePaySignature))
                            {
                                this.AddLogError(string.Format("MobilePay Error - Canceling Capture! {0}", errorDesc));
                                return false;
                            }
                            else
                            {
                                this.AddLogError(string.Format("MobilePay Error - {1} Continue since signature was OK", errorDesc));
                            }
                        }
                        else
                        {
                            if (dbOutput.LatestPaymentStatus != Helpers.MobilePay.PAYMENTSTATUS_CAPTURED)
                            {
                                this.AddLogError(string.Format("MobilePay Error: Order not captured. Status: {0}", dbOutput.LatestPaymentStatus));
                                return false;
                            }
                            if ((double)dbOutput.OriginalAmount != this.OrderSum)
                            {
                                this.AddLogError(string.Format("MobilePay Error: Order amount differs: Expected:{0:0.00} Got: {1:0.00}", this.OrderSum, dbOutput.OriginalAmount));
                                return false;
                            }
                            if (dbOutput.OriginalTransactionId != this.CaptureId)
                            {
                                this.AddLogError(string.Format("MobilePay Error: TransactionId differs: Expected:{0} Got: {1}", dbOutput.OriginalTransactionId, this.CaptureId));
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        this.AddLogError(string.Format("MobilePay WS Error: {0}", ex.Message));
                        return false;
                    }
                }
                int queueLength = 5;
                int.TryParse(ConfigSettings.GetText("OrderQueueLength", "5"), out queueLength);

                this.ActivateCodes();

                Bll.OrderQueue.Add(this.Id);
                this.SetStatus(OrderStatusValues.Paid);
                Bll.OrderQueue.FlushQueue(queueLength);

                this.AddLog("MobilePay Payment OK");
                return true;
            }
            this.AddLog("MobilePay Payment Not OK");


            return false;
        }


        /// <summary>
        /// Checks the mobile pay signature using API V2
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">MobilePayMerchantId not found. Please add to Configuration
        /// or
        /// MobilePayClientId not found. Please add to Configuration</exception>
        public string CaptureMobilePay(string transactionId)
        {
            int retryMobilePayTimeout = 5000;
            string errorDesc = string.Empty;

            int.TryParse(ConfigSettings.GetText("MobilePayRetryValidationTimeOut", "6000"), out retryMobilePayTimeout);

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                this.AddLogError("MobilePayMerchantId not found. Unable to validate Mobile Payment.");
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                this.AddLogError("MobilePayClientId not found. Unable to validate Mobile Payment.");
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            PnConfig configuration = this.GetPnConfig();

            this.AddLog(string.Format("Calling DB WS V2 MerchantId: {0} ClientId:{1}", merchantId, clientId));
            try
            {
                DB.MobilePayV02.GetStatus.dacGetStatusOutput dbOutput = MobilePay.Proxy.CallGetStatus(configuration, clientId, merchantId, this.MobilePayOrderNr);
                this.AddLog(string.Format("WS V2 called. ReturnCode: {0} ReasonCode: {1}", dbOutput.ReturnCode, dbOutput.ReasonCode));

                // If validation fails retry after timeout
                if (dbOutput.ReturnCode == Helpers.MobilePay.RETURNCODE_GENERAL_ERROR && dbOutput.ReasonCode == Helpers.MobilePay.REASONCODE_TIMEOUT && retryMobilePayTimeout > 0)
                {
                    this.AddLogError(string.Format("Order not captured. Retrying in {0} ms.", retryMobilePayTimeout));
                    System.Threading.Thread.Sleep(retryMobilePayTimeout);
                    dbOutput = MobilePay.Proxy.CallGetStatus(configuration, clientId, merchantId, this.MobilePayOrderNr);
                }

                // if Return Code != '00' Log Error description
                if (dbOutput.ReturnCode != Helpers.MobilePay.RETURNCODE_OK)
                {
                    errorDesc = string.Format("ReturnCode: {0} ({1}) Reason: {2} ({3})", dbOutput.ReturnCode, Helpers.MobilePay.V2Returncode(dbOutput.ReturnCode), dbOutput.ReasonCode, Helpers.MobilePay.V2Reasoncode(dbOutput.ReasonCode));
                    this.AddLogError(string.Format("MobilePay Get Status error: {0}", errorDesc));
                    return errorDesc;
                }
                else
                {
                    bool testMode = false;
                    bool.TryParse(ConfigSettings.GetText("MobilePayGetOrderStatusTest", "false"), out testMode);

                    if (testMode)
                    {
                        if (merchantId == "APPDK0000000000")
                        {
                            this.AddLog("TestMode enabled");
                        }
                        else
                        {
                            this.AddLog("TestMode not enabled since merchantId is not test");
                            testMode = false;
                        }
                    }

                    // check order sum
                    if ((double)dbOutput.OriginalAmount != this.OrderSum && !testMode)
                    {
                        errorDesc = string.Format("Order amount differs: Expected:{0:0.00} Got: {1:0.00}", this.OrderSum, dbOutput.OriginalAmount);
                        this.AddLogError(string.Format("MobilePay Error: {0}", errorDesc));
                        return errorDesc;
                    }

                    // Check transaction Id
                    //if (dbOutput.OriginalTransactionId != this.CaptureId && !testMode)
                    //{
                    //    errorDesc = string.Format("TransactionId differs: Expected:{0} Got: {1}", dbOutput.OriginalTransactionId, this.CaptureId);
                    //    this.AddLogError(string.Format("MobilePay Error: {0}", errorDesc));
                    //    return errorDesc;
                    //}

                    // If payment is already Captured we are happy
                    switch (dbOutput.LatestPaymentStatus)
                    {
                        case Helpers.MobilePay.PAYMENTSTATUS_CAPTURED:
                            this.AddLog(string.Format("Order already paid! Skipping Capture. Updating TransactionId: {0}", dbOutput.OriginalTransactionId));
                            this.CaptureId = dbOutput.OriginalTransactionId;
                            this.Save();
                            break;
                        case Helpers.MobilePay.PAYMENTSTATUS_RESERVED:
                            this.AddLog("Order reserved! Capturing payment");

                            var captureOutput = MobilePay.Proxy.CallCapture(configuration, clientId, merchantId, this.MobilePayOrderNr, (decimal)this.OrderSum);
                            // If validation fails retry after timeout
                            if (captureOutput.ReturnCode == Helpers.MobilePay.RETURNCODE_GENERAL_ERROR && captureOutput.ReasonCode == Helpers.MobilePay.REASONCODE_TIMEOUT && retryMobilePayTimeout > 0)
                            {
                                this.AddLogError(string.Format("Capturing timed out. Retrying in {0} ms.", retryMobilePayTimeout));
                                System.Threading.Thread.Sleep(retryMobilePayTimeout);
                                captureOutput = MobilePay.Proxy.CallCapture(configuration, clientId, merchantId, this.MobilePayOrderNr, (decimal)this.OrderSum);
                            }

                            if (captureOutput.ReturnCode != Helpers.MobilePay.RETURNCODE_OK)
                            {
                                errorDesc = string.Format("ReturnCode: {0} ({1}) Reason: {2} ({3})", captureOutput.ReturnCode, Helpers.MobilePay.V2Returncode(captureOutput.ReturnCode), captureOutput.ReasonCode, Helpers.MobilePay.V2Reasoncode(captureOutput.ReasonCode));
                                this.AddLogError(string.Format("MobilePay Capture error: {0}", errorDesc));
                                return errorDesc;
                            }

                            this.AddLog(string.Format("Capture succesfull! TransactionId: {0} ", captureOutput.TransactionId));
                            this.CaptureId = captureOutput.TransactionId;
                            this.Save();
                            break;
                        default:
                            if (testMode)
                            {
                                this.AddLogDebug(string.Format("Unexpected PaymentStatus: {0} allowed in Testmode", dbOutput.LatestPaymentStatus));
                            }
                            else
                            {
                                errorDesc = string.Format("Unexpected PaymentStatus: {0}", dbOutput.LatestPaymentStatus);
                                this.AddLogError(errorDesc);
                                return errorDesc;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                this.AddLogError(string.Format("MobilePay WS Error: {0}", ex.Message));
                return "";
            }
            int queueLength = 5;
            int.TryParse(ConfigSettings.GetText("OrderQueueLength", "5"), out queueLength);

            this.ActivateCodes();

            Bll.OrderQueue.Add(this.Id);
            this.SetStatus(OrderStatusValues.Paid);
            Bll.OrderQueue.FlushQueue(queueLength);

            this.AddLog("MobilePay Payment OK");

            return "OK";
        }



        /// <summary>
        /// Debits the cint.
        /// </summary>
        /// <param name="subOrders">The sub orders.</param>
        /// <exception cref="System.ApplicationException">CINT Purchase Config Information missing</exception>
        /// <exception cref="System.IndexOutOfRangeException">
        /// </exception>
        public void DebitCint()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolTypeExtensions.Tls12;
            // Disable Certficate check
            if (System.Net.ServicePointManager.ServerCertificateValidationCallback == null)
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            }

            string purchaseWsUrl = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsUrl");
            string purchaseWsUserName = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsUserName");
            string purchaseWsPassword = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsPassword");
            string purchaseWsPaymentTerm = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsPaymentTerm");
            string purchaseWsSoldToParty = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsSoldToParty");
            string cintCountryCode = ConfigSettings.GetText("CintCountryCode", "da-DK");
            string cintNumberFormat = ConfigSettings.GetText("CintNumberFormat", "{0:0.00}");

            if (string.IsNullOrEmpty(purchaseWsUrl) || string.IsNullOrEmpty(purchaseWsUserName) || string.IsNullOrEmpty(purchaseWsPassword) ||
                string.IsNullOrEmpty(purchaseWsPaymentTerm) || string.IsNullOrEmpty(purchaseWsSoldToParty))
            {
                throw new ApplicationException("CINT Purchase Config Information missing");
            }

            PurchaseDigitalPostageService.PurchaseDigitalPostageService cintWs = new PurchaseDigitalPostageService.PurchaseDigitalPostageService();

            PurchaseDigitalPostageService.EifHeaders headers = new PurchaseDigitalPostageService.EifHeaders();
            cintWs.EifHeadersValue = headers;

            PurchaseDigitalPostageService.purchaseCodeRequest purchaseReq = new PurchaseDigitalPostageService.purchaseCodeRequest();
            cintWs.Url = purchaseWsUrl;

            System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
            NetworkCredential netCred = new NetworkCredential(purchaseWsUserName, purchaseWsPassword);
            myCredentials.Add(new Uri(cintWs.Url), "Basic", netCred);
            cintWs.Credentials = myCredentials;

            PurchaseDigitalPostageService.orderInfo purchaseOrderInfo = new PurchaseDigitalPostageService.orderInfo();
            purchaseOrderInfo.BSTKD = (int)this.Id;
            purchaseOrderInfo.OrderType = PurchaseDigitalPostageService.OrderType.debit;
            purchaseOrderInfo.SoldToParty = short.Parse(purchaseWsSoldToParty);
            purchaseOrderInfo.Timestamp = DateTime.Now;
            purchaseOrderInfo.PaymentTerm = purchaseWsPaymentTerm;

            purchaseReq.orderInfo = purchaseOrderInfo;

            List<PurchaseDigitalPostageService.orderItem> purchaseOrderItems = new List<PurchaseDigitalPostageService.orderItem>();
            System.Globalization.CultureInfo cif = new System.Globalization.CultureInfo(cintCountryCode);

            try
            {
                foreach (var subOrder in this.SubOrders)
                {
                    if (subOrder.SapId <= 0)
                    {
                        throw new IndexOutOfRangeException(string.Format("SapId missing in Payment for SubOrderId: {0}", subOrder.Id));
                    }

                    PurchaseDigitalPostageService.orderItem purchaseOrderItem = new PurchaseDigitalPostageService.orderItem();
                    purchaseOrderItem.Material = subOrder.SapId.ToString();                           // Product Code in SAP
                    purchaseOrderItem.Pricing = string.Format(cif, cintNumberFormat, subOrder.PriceWithoutVAT); // Price in DK format: 10,50
                    purchaseOrderItem.Quantity = subOrder.Amount.ToString();                          // Number of codes

                    purchaseOrderItems.Add(purchaseOrderItem);

                    this.AddLog(string.Format("Debit CINT - Material: {0} Amount: {1} Price: {2} ", purchaseOrderItem.Material, purchaseOrderItem.Quantity, purchaseOrderItem.Pricing));
                }

                purchaseReq.orderItem = purchaseOrderItems.ToArray();
                cintWs.PurchaseCode(purchaseReq);
                this.UpdateCintStatus(true);
                this.AddLog(string.Format("Debit CINT WS called for {0} suborders", purchaseReq.orderItem.Length));
            }
            catch (Exception ex)
            {
                this.AddLogError(string.Format("Debit CINT Failed. {0}", ex.Message));
                throw new IndexOutOfRangeException(string.Format("General Communication Error to CINT: {0}", ex.Message));
            }
        }


        /// <summary>
        /// Updates the cint status.
        /// </summary>
        /// <param name="transferredToCint">if set to <c>true</c> [transferred to cint].</param>
        public void UpdateCintStatus(bool transferredToCint)
        {
            if (transferredToCint)
                Dal.Orders.UpdateCintStatus(this.Id, 1);
            else
                Dal.Orders.UpdateCintStatus(this.Id, 0);
            this.SentToCint = transferredToCint;
        }

        /// <summary>
        /// Debits the cint.
        /// </summary>
        /// <param name="subOrders">The sub orders.</param>
        /// <exception cref="System.ApplicationException">CINT Purchase Config Information missing</exception>
        /// <exception cref="System.IndexOutOfRangeException">
        /// </exception>
        private void CreditCint(List<SubOrders> subOrders)
        {
            string purchaseWsUrl = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsUrl");
            string purchaseWsUserName = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsUserName");
            string purchaseWsPassword = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsPassword");
            string purchaseWsPaymentTerm = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsPaymentTerm");
            string purchaseWsSoldToParty = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsSoldToParty");
            string cintCountryCode = ConfigSettings.GetText("CintCountryCode", "da-DK");
            string cintNumberFormat = ConfigSettings.GetText("CintNumberFormat", "{0:0.00}");

            if (string.IsNullOrEmpty(purchaseWsUrl) || string.IsNullOrEmpty(purchaseWsUserName) || string.IsNullOrEmpty(purchaseWsPassword) ||
                string.IsNullOrEmpty(purchaseWsPaymentTerm) || string.IsNullOrEmpty(purchaseWsSoldToParty))
            {
                throw new ApplicationException("CINT Purchase Config Information missing");
            }

            PurchaseDigitalPostageService.PurchaseDigitalPostageService cintWs = new PurchaseDigitalPostageService.PurchaseDigitalPostageService();

            PurchaseDigitalPostageService.EifHeaders headers = new PurchaseDigitalPostageService.EifHeaders();
            cintWs.EifHeadersValue = headers;

            PurchaseDigitalPostageService.purchaseCodeRequest purchaseReq = new PurchaseDigitalPostageService.purchaseCodeRequest();
            cintWs.Url = purchaseWsUrl;

            System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
            NetworkCredential netCred = new NetworkCredential(purchaseWsUserName, purchaseWsPassword);
            myCredentials.Add(new Uri(cintWs.Url), "Basic", netCred);
            cintWs.Credentials = myCredentials;

            PurchaseDigitalPostageService.orderInfo purchaseOrderInfo = new PurchaseDigitalPostageService.orderInfo();
            purchaseOrderInfo.BSTKD = (int)this.Id;
            purchaseOrderInfo.OrderType = PurchaseDigitalPostageService.OrderType.credit;
            purchaseOrderInfo.SoldToParty = short.Parse(purchaseWsSoldToParty);
            purchaseOrderInfo.Timestamp = DateTime.Now;
            purchaseOrderInfo.PaymentTerm = purchaseWsPaymentTerm;
            purchaseReq.orderInfo = purchaseOrderInfo;

            List<PurchaseDigitalPostageService.orderItem> purchaseOrderItems = new List<PurchaseDigitalPostageService.orderItem>();
            System.Globalization.CultureInfo cif = new System.Globalization.CultureInfo(cintCountryCode);

            try
            {
                foreach (var subOrder in subOrders)
                {
                    if (subOrder.SapId <= 0)
                    {
                        throw new IndexOutOfRangeException(string.Format("SapId missing in Payment for SubOrderId: {0}", subOrder.Id));
                    }

                    PurchaseDigitalPostageService.orderItem purchaseOrderItem = new PurchaseDigitalPostageService.orderItem();
                    purchaseOrderItem.Material = subOrder.SapId.ToString();                           // Product Code in SAP
                    purchaseOrderItem.Pricing = string.Format(cif, cintNumberFormat, subOrder.PriceWithoutVAT); // Price in DK format: 10,50
                    purchaseOrderItem.Quantity = subOrder.Amount.ToString();                          // Number of codes

                    purchaseOrderItems.Add(purchaseOrderItem);

                    this.AddLog(string.Format("Credit CINT - Material: {0} Amount: {1} Price: {2} ", purchaseOrderItem.Material, purchaseOrderItem.Quantity, purchaseOrderItem.Pricing));
                }

                purchaseReq.orderItem = purchaseOrderItems.ToArray();
                cintWs.PurchaseCode(purchaseReq);

                this.AddLog(string.Format("Credit CINT WS called for {0} suborders", purchaseReq.orderItem.Length));
            }
            catch (Exception ex)
            {
                throw new IndexOutOfRangeException(string.Format("General Communication Error to CINT: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Debits cint order for all refunds.
        /// </summary>
        /// <param name="refunds">The refunds.</param>
        /// <exception cref="System.ApplicationException">CINT Purchase Config Information missing</exception>
        /// <exception cref="System.IndexOutOfRangeException"></exception>
        public void CreditCint(Hashtable refunds)
        {
            string purchaseWsUrl = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsUrl");
            string purchaseWsUserName = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsUserName");
            string purchaseWsPassword = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsPassword");
            string purchaseWsPaymentTerm = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsPaymentTerm");
            string purchaseWsSoldToParty = ConfigSettings.GetText("PurchaseDigitalPostageServiceWsSoldToParty");
            string cintCountryCode = ConfigSettings.GetText("CintCountryCode", "da-DK");
            string cintNumberFormat = ConfigSettings.GetText("CintNumberFormat", "{0:0.00}");

            if (string.IsNullOrEmpty(purchaseWsUrl) || string.IsNullOrEmpty(purchaseWsUserName) || string.IsNullOrEmpty(purchaseWsPassword) ||
                string.IsNullOrEmpty(purchaseWsPaymentTerm) || string.IsNullOrEmpty(purchaseWsSoldToParty))
            {
                throw new ApplicationException("CINT Purchase Config Information missing");
            }

            PurchaseDigitalPostageService.PurchaseDigitalPostageService cintWs = new PurchaseDigitalPostageService.PurchaseDigitalPostageService();

            PurchaseDigitalPostageService.EifHeaders headers = new PurchaseDigitalPostageService.EifHeaders();
            cintWs.EifHeadersValue = headers;

            PurchaseDigitalPostageService.purchaseCodeRequest purchaseReq = new PurchaseDigitalPostageService.purchaseCodeRequest();
            cintWs.Url = purchaseWsUrl;

            System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
            NetworkCredential netCred = new NetworkCredential(purchaseWsUserName, purchaseWsPassword);
            myCredentials.Add(new Uri(cintWs.Url), "Basic", netCred);
            cintWs.Credentials = myCredentials;

            PurchaseDigitalPostageService.orderInfo purchaseOrderInfo = new PurchaseDigitalPostageService.orderInfo();
            purchaseOrderInfo.BSTKD = (int)this.Id;
            purchaseOrderInfo.OrderType = PurchaseDigitalPostageService.OrderType.credit;
            purchaseOrderInfo.SoldToParty = short.Parse(purchaseWsSoldToParty);
            purchaseOrderInfo.Timestamp = DateTime.Now;
            purchaseOrderInfo.PaymentTerm = purchaseWsPaymentTerm;
            purchaseReq.orderInfo = purchaseOrderInfo;

            List<PurchaseDigitalPostageService.orderItem> purchaseOrderItems = new List<PurchaseDigitalPostageService.orderItem>();
            System.Globalization.CultureInfo cif = new System.Globalization.CultureInfo(cintCountryCode);

            try
            {
                foreach (long subOrderId in refunds.Keys)
                {
                    var subOrder = new SubOrders(subOrderId);
                    int amount = (int)refunds[subOrderId];
                    if (subOrder.SapId <= 0)
                    {
                        throw new IndexOutOfRangeException(string.Format("SapId missing in Payment for SubOrderId: {0}", subOrder.Id));
                    }

                    PurchaseDigitalPostageService.orderItem purchaseOrderItem = new PurchaseDigitalPostageService.orderItem();
                    purchaseOrderItem.Material = subOrder.SapId.ToString();                           // Product Code in SAP
                    purchaseOrderItem.Pricing = string.Format(cif, cintNumberFormat, subOrder.PriceWithoutVAT); // Price in DK format: 10,50
                    purchaseOrderItem.Quantity = amount.ToString();                                   // Number of codes

                    purchaseOrderItems.Add(purchaseOrderItem);

                    this.AddLog(string.Format("Refund CINT - Material: {0} Amount: {1} Price: {2} ", purchaseOrderItem.Material, purchaseOrderItem.Quantity, purchaseOrderItem.Pricing));
                }

                purchaseReq.orderItem = purchaseOrderItems.ToArray();
                cintWs.PurchaseCode(purchaseReq);

                this.AddLog(string.Format("Credit CINT WS called for {0} suborders", purchaseReq.orderItem.Length));
            }
            catch (Exception ex)
            {
                this.AddLog(string.Format("Credit CINT Failed: {0}", ex.Message));
                throw new IndexOutOfRangeException(string.Format("General Communication Error Refunding: {0}", ex.Message));
            }
        }


        /// <summary>
        /// Gets the nets status.
        /// </summary>
        /// <returns></returns>
        public NetAxept.PaymentInfo GetNetsStatus()
        {
            NetAxept.PaymentInfo paymentInfo = null;

            using (NetAxept.Netaxept saleClient = new NetAxept.Netaxept())
            {
                saleClient.Url = ConfigSettings.GetText("NetaxeptWebserviceUrl");
                int netsTimeOut = 10000;
                int.TryParse(ConfigSettings.GetText("NetsTimeOut", "10000"), out netsTimeOut);
                saleClient.Timeout = netsTimeOut;

                NetAxept.QueryRequest qReq = new NetAxept.QueryRequest();
                qReq.TransactionId = this.CaptureId;
                paymentInfo = (NetAxept.PaymentInfo)saleClient.Query(this.MerchantId, this.Token, qReq);
            }
            return paymentInfo;
        }



        public void CancelOrder()
        {
            this.AddLogDebug("Cancel order");

            PnConfig configuration = this.GetPnConfig();
            string error = string.Empty;

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            MobilePay.Proxy.CallCancel(GetPnConfig(), clientId, merchantId, this.MobilePayOrderNr, false);
            this.SetStatus(OrderStatusValues.Cancelled);
        }

        /// <summary>
        /// Repairs the order.
        /// </summary>
        /// <param name="paid">if set to <c>true</c> [paid].</param>
        /// <exception cref="System.ApplicationException"></exception>
        public void RepairOrder(bool paid)
        {
            if (paid)
            {
                this.AddLogDebug("Repair Order - Paid");
                try
                {
                    var codes = this.GetCodes();
                    if (codes.Count == 0)
                    {
                        this.AddLogDebug("Adding codes");
                        foreach (var subOrder in this.SubOrders)
                        {
                            subOrder.CreateCodes();
                        }
                    }
                    this.ActivateCodes();
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Error loading codes: {0}", ex.Message));
                }

                if (!this.SentToCint)
                {
                    this.AddLogDebug("Queing order for CINT");
                    OrderQueue.Add(this.Id, DateTime.Now);
                    OrderQueue.FlushQueue(5);
                }

                if (this.Status != OrderStatusValues.Paid)
                {
                    this.SetStatus(OrderStatusValues.Paid);
                }

                this.SendMail();
                this.AddLogDebug("Repair Done!");
            }
        }


        /// <summary>
        /// Captures money for the order
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Order has already been payed. Payment annulled!
        /// or
        /// or
        /// Error capturing order!. The error was logged.</exception>
        public CreditCardInfo Capture()
        {
            int progress = 1;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolTypeExtensions.Tls12;
                // Disable Certficate check
                if (System.Net.ServicePointManager.ServerCertificateValidationCallback == null)
                {
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                }
            }
            catch (Exception ex)
            {
                this.AddLogError(string.Format("Callback error: {0}", ex.Message));
            }

            CreditCardInfo cardInfo = null;

            int queueLength = 5;

            int.TryParse(ConfigSettings.GetText("OrderQueueLength", "5"), out queueLength);

            if (this.Status == OrderStatusValues.Paid)
            {
                string error = string.Format("Capture failed. Order has already been payed.");
                this.AddLogError(error);
                cardInfo = new CreditCardInfo(PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.ORDER_ALREADY_PAID.ToString(), error);
                return cardInfo;
            }

            try
            {
                progress = 2;
                var subOrders = this.SubOrders;
                progress = 3;
                using (NetAxept.Netaxept saleClient = new NetAxept.Netaxept())
                {
                    saleClient.Url = ConfigSettings.GetText("NetaxeptWebserviceUrl");
                    int netsTimeOut = 10000;
                    int.TryParse(ConfigSettings.GetText("NetsTimeOut", "10000"), out netsTimeOut);
                    saleClient.Timeout = netsTimeOut;

                    NetAxept.QueryRequest qReq = new NetAxept.QueryRequest();
                    qReq.TransactionId = this.CaptureId;
                    NetAxept.PaymentInfo paymentInfo = (NetAxept.PaymentInfo)saleClient.Query(this.MerchantId, this.Token, qReq);
                    progress = 4;

                    if (this.Status == OrderStatusValues.ReadyForPaymentCardSave)
                    {
                        progress = 5;
                        Cards card = new Cards(this.Id, this.Email, paymentInfo.CardInformation.PanHash);
                        card.Save();
                        progress = 6;
                        cardInfo = new CreditCardInfo(paymentInfo.CardInformation.Issuer, paymentInfo.CardInformation.MaskedPAN, paymentInfo.CardInformation.ExpiryDate, card.Id.ToString());
                        progress = 7;
                    }
                    else
                    {
                        progress = 8;
                        cardInfo = new CreditCardInfo(paymentInfo.CardInformation.Issuer, paymentInfo.CardInformation.MaskedPAN, paymentInfo.CardInformation.ExpiryDate);
                        progress = 9;
                    }


                    try
                    {
                        double capturedAmount = 0;
                        double.TryParse(paymentInfo.Summary.AmountCaptured, out capturedAmount);
                        if (capturedAmount > 0)
                        {
                            capturedAmount = capturedAmount / 100;
                        }

                        double creditedAmount = 0;
                        double.TryParse(paymentInfo.Summary.AmountCredited, out creditedAmount);

                        if (capturedAmount == this.OrderSum && creditedAmount == 0)
                        {
                            cardInfo.ErrorCode = string.Empty;
                            cardInfo.ErrorMessage = string.Empty;

                            this.AddLog("Order already paid. Activating codes");
                            this.ActivateCodes();
                            progress = 17;
                            if (this.Status != OrderStatusValues.Paid)
                            {
                                Bll.OrderQueue.Add(this.Id);
                                progress = 18;
                                this.SetStatus(OrderStatusValues.Paid);
                                progress = 19;
                                Bll.OrderQueue.FlushQueue(queueLength);
                                progress = 20;
                            }
                        }
                        else
                        {
                            // Capture Money 
                            NetAxept.ProcessRequest saleRequest = new NetAxept.ProcessRequest();
                            saleRequest.Operation = "SALE";
                            saleRequest.TransactionId = this.CaptureId;
                            saleRequest.TransactionReconRef = this.Id.ToString();

                            NetAxept.ProcessResponse processResponse = saleClient.Process(this.MerchantId, this.Token, saleRequest);
                            progress = 10;

                            this.AddLog(string.Format("Sale. Code: {0} Message: {1}", processResponse.ResponseCode, processResponse.ResponseText));

                            if (processResponse.ResponseCode == "OK")
                            {
                                this.ActivateCodes();
                                progress = 11;
                                Bll.OrderQueue.Add(this.Id);
                                progress = 12;
                                this.SetStatus(OrderStatusValues.Paid);
                                progress = 13;
                                Bll.OrderQueue.FlushQueue(queueLength);
                                progress = 14;
                            }
                            else
                            {
                                progress = 16;
                                string error = string.Format("Capture failed! Error code: {0}. Progress:{1} Message: {2}", processResponse.ResponseCode, progress, processResponse.ResponseText);
                                this.AddLogError(error);
                                throw new ApplicationException("Der opstod en fejl under betalingen. Prøv igen senere eller kontakt kundeservice.");
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        if (ex.Message == "The operation has timed out")
                        {
                            cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_TIME_OUT.ToString();
                            cardInfo.ErrorMessage = string.Format("Progress: {0} Message: {1}", progress, ex.Message);
                        }
                        else
                        {
                            cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_ERROR.ToString();
                            cardInfo.ErrorMessage = string.Format("Progress: {0}. NETS Capture Error, Unknown WebException: {1}", progress, ex.Message);
                        }

                        this.AddLogError(cardInfo.ErrorMessage);
                        this.SetStatus(OrderStatusValues.ReadyForPayment);

                        return cardInfo;
                    }
                    catch (SoapException ex)
                    {
                        this.AddLogError(string.Format("Capture Soap Exception! Progress: {0}. Message: {1}", progress, ex.Message));
                        try
                        {
                            paymentInfo = (NetAxept.PaymentInfo)saleClient.Query(this.MerchantId, this.Token, qReq);

                            if (paymentInfo == null)
                            {
                                this.AddLogError("PaymentInfo is NULL");
                                cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                                cardInfo.ErrorMessage = "NETS Unknown Capture Error, Unknown Exception: " + ex.Message;
                            }
                            else if (paymentInfo.Error == null)
                            {
                                this.AddLogError("PaymentInfo.Error is NULL");
                                cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                                cardInfo.ErrorMessage = "NETS Unknown Capture Error, Unknown Exception: " + ex.Message;
                            }
                            else if (paymentInfo.Error.ResponseSource == null)
                            {
                                this.AddLogError("PaymentInfo.Error.ResponseSource is NULL");
                                cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                                cardInfo.ErrorMessage = "NETS Unknown Capture Error, Unknown Exception: " + ex.Message;
                            }
                            else if (paymentInfo.Error.ResponseSource.Trim() == "Netaxept")
                            {
                                this.AddLogError(string.Format("NETS Error details: Operation: '{0}' ResponseCode: '{1}' Source: '{2}' ResponseText: '{3}'", paymentInfo.Error.Operation, paymentInfo.Error.ResponseCode, paymentInfo.Error.ResponseSource, paymentInfo.Error.ResponseText));
                                switch (paymentInfo.Error.ResponseCode.Trim())
                                {
                                    case "99": // Several errors can result in this code. Refer to the Response text for details.
                                        if (ex.Message.Contains("Auth Reg Comp Failure"))
                                        {
                                            cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_AUTH_FAILURE.ToString();
                                            cardInfo.ErrorMessage = ex.Message;
                                        }
                                        break;
                                    case "14": // Invalid card number.
                                    case "25": // Transaction not found.
                                    case "30": // “KID invalid” or “Field missing PAN”.
                                    case "84": // Original transaction rejected.
                                    case "86": // Transaction already reversed.
                                    case "96": // Internal failure.
                                    case "97": // No transaction
                                    case "MZ": // Denied by 3D Secure.
                                    default:
                                        cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_ERROR.ToString();
                                        cardInfo.ErrorMessage = ex.Message;
                                        break;
                                }
                            }
                            else if (paymentInfo.Error.ResponseSource.Trim() == "Issuer")
                            {
                                switch (paymentInfo.Error.ResponseCode.Trim())
                                {
                                    case "01": // Refused by Issuer. Contact Issuer.
                                    case "02": // Refused by Issuer. Contact Issuer.
                                    case "03": // Refused by Issuer because of Invalid merchant.
                                    case "04": // Refused by Issuer.
                                    case "05": // Refused by Issuer.
                                    case "06": // Refused by Issuer.
                                    case "07": // Refused by Issuer.
                                    case "12": // invalid transaction.
                                    case "13": // Refused by Issuer because of invalid amount.
                                    case "14": // Refused by Issuer.
                                    case "19": // Refused by Issuer. Try again.
                                    case "30": // Refused by Issuer because of format error.
                                    case "31": // Issuer could not be reached, contact Nets support.
                                    case "33": // Expired card.
                                    case "36": // Restricted card.
                                    case "41": // Refused by Issuer. Contact Issuer.
                                    case "43": // Refused by Issuer. Contact Issuer.
                                    case "51": // Refused by Issuer. Contact Issuer.
                                    case "52": // Refused by Issuer because of no checking account.
                                    case "54": // Refused by Issuer because of expired card.
                                    case "56": // Refused by Issuer because of no card record.
                                    case "57": // Refused by Issuer of transaction not permitted.
                                    case "58": // Refused by Issuer of transaction not permitted.
                                    case "59": // Refused by Issuer. Contact Issuer.
                                    case "61": // Refused by Issuer. Contact Issuer.
                                    case "62": // Refused by Issuer. Contact Issuer.
                                    case "63": // Refused by Issuer. Contact Issuer.
                                    case "65": // Refused by Issuer. Contact Issuer.
                                    case "68": // Refused by Issuer because response received to late.
                                    case "78": // Refused by Issuer.
                                    case "79": // Refused by Issuer.
                                    case "80": // Refused by Issuer.
                                    case "91": // Refused by Issuer because Issuer is temporarily inoperative.
                                    case "92": // Refused by Issuer because Issuer is temporarily inoperative.
                                    case "93": // Refused by Issuer. Contact Issuer.
                                    case "96": // Refused by Issuer because of system malfunction.
                                    case "C9": // Refused by Issuer.
                                    case "N0": // Refused by Issuer.
                                    case "N7": // Refused by Issuer because of invalid 3 digit code from back of credit card.
                                    case "P1": // Refused by Issuer.
                                    case "P9": // Refused by Issuer.
                                    case "T3": // Refused by Issuer.
                                    case "T8": // Refused by Issuer.
                                        cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_AUTH_FAILURE.ToString();
                                        cardInfo.ErrorMessage = ex.Message;
                                        break;
                                    default:
                                        cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_ERROR.ToString();
                                        cardInfo.ErrorMessage = ex.Message;
                                        break;
                                }
                            }
                            else // “Module” or “Transport”, 
                            {
                                cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_ERROR.ToString();
                                cardInfo.ErrorMessage = ex.Message;
                            }
                        }
                        catch (Exception queryEx)
                        {
                            this.AddLogError(string.Format("Capture Error. {0}. Progress:{1}", cardInfo.ErrorMessage, progress));
                            cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_ERROR.ToString();
                            cardInfo.ErrorMessage = "NETS Capture Status Error, Unknown Exception: " + queryEx.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_ERROR.ToString();
                        cardInfo.ErrorMessage = "NETS Capture Error, Unknown Exception: " + ex.Message;
                    }
                }
            }
            catch (System.Net.WebException ex)
            {
                if (ex.Message == "The operation has timed out")
                {
                    cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_TIME_OUT.ToString();
                    cardInfo.ErrorMessage = ex.Message;
                }
                else
                {
                    cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                    cardInfo.ErrorMessage = "NETS General Capture Error, Unknown WebException: " + ex.Message;
                }
                this.AddLogError(string.Format("WebException: Status:{0} Progress: {1} Message: {2} Details: {3}", ex.Status, progress, ex.Message, ex));
            }
            catch (Exception ex)
            {
                cardInfo.ErrorCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
                cardInfo.ErrorMessage = "NETS Unknown Capture Error, Unknown Exception: " + ex.Message;
                this.AddLogError(string.Format("Exception. Message: {0} Progress: {1} Details: {2}", ex.Message, progress, ex));
            }

            return cardInfo;
        }

        /// <summary>
        /// Create codes for all SubOrders
        /// </summary>
        private void CreateCodes()
        {
            try
            {
                this.AddLogDebug("Creating codes from CINT");
                // Create all codes for order 
                foreach (SubOrders subOrder in this.SubOrders)
                {
                    subOrder.CreateCodes();
                    this.AddLog(string.Format("Created {0} codes: for suborder:{1}", subOrder.Codes.Count, subOrder.Id));
                }
            }
            catch (Exception ex)
            {
                this.AddLogError(string.Format("Error generation code: {0}", ex.Message));
                throw new ApplicationException("Code generation failed!");
            }
        }

        /// <summary>
        /// Create codes for all SubOrders
        /// </summary>
        private void ActivateCodes()
        {
            try
            {
                //create array of suborder id's
                var listOfSubIds = (from s in this.SubOrders select s.Id.ToString()).ToArray();
                //convert the array to a string og id's with ',' as seperator
                var stringOfSubIds = string.Join(",", listOfSubIds, 0, listOfSubIds.Count());

                //Activate the codes for all of the sub id's in the list by updating the database 
                PostNord.Portal.DigitalPostage.Dal.SubOrders.SubOrdersChangeCodeStatusByList(stringOfSubIds, (short)Bll.Codes.CodeStatusValues.Disabled, (short)Bll.Codes.CodeStatusValues.Unused);

                //update the status for each suborder id's
                foreach (SubOrders subOrder in this.SubOrders)
                {
                    this.AddLog(string.Format("Activated {0} codes for suborder:{1}", subOrder.Codes.Count, subOrder.Id));
                }
            }
            catch (Exception ex)
            {
                //update the status 'failed' for each suborder id's
                foreach (SubOrders subOrder in this.SubOrders)
                {
                    this.AddLogError(string.Format("Attempt to activate {0} codes for suborder:{1} failed", subOrder.Codes.Count, subOrder.Id));
                }
                this.AddLogError(string.Format("Attempt to activate, error: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Create codes for all SubOrders
        /// </summary>
        private void DeleteCodes()
        {
            // Create all codes for order 
            foreach (SubOrders subOrder in this.SubOrders)
            {
                subOrder.DeleteAllCodes();
                this.AddLog(string.Format("Deleted {0} codes: for suborder:{1}", subOrder.Codes.Count, subOrder.Id));
            }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="numberOfPaidOrders">The number of paid orders.</param>
        /// <param name="numberOfUnPaidOrders">The number of un paid orders.</param>
        static public void GetStatus(DateTime fromDate, DateTime toDate, out int numberOfPaidOrders, out int numberOfUnPaidOrders)
        {
            numberOfPaidOrders = 0;
            numberOfUnPaidOrders = 0;
            DataSet res = Dal.Orders.GetStatus(fromDate, toDate);

            numberOfPaidOrders = (int)res.Tables[0].Rows[0][0];
            numberOfUnPaidOrders = (int)res.Tables[1].Rows[0][0];


        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns>Error message for specific exception</returns>
        string GetErrorMessage(System.Net.WebException ex)
        {
            switch (ex.Status)
            {
                case System.Net.WebExceptionStatus.CacheEntryNotFound:
                    return "Der blev ikke fundet nogen cache.";
                case System.Net.WebExceptionStatus.ConnectFailure:
                    return "Forbindelsesfejl.";
                case System.Net.WebExceptionStatus.ConnectionClosed:
                    return "Forbindelsen blev afvist.";
                case System.Net.WebExceptionStatus.KeepAliveFailure:
                    return "KeepAliveFailure.";
                case System.Net.WebExceptionStatus.MessageLengthLimitExceeded:
                    return "Maksimalt længde er overskredet.";
                case System.Net.WebExceptionStatus.NameResolutionFailure:
                    return "Det var ikke muligt at opløse navnet.";
                case System.Net.WebExceptionStatus.Pending:
                    return "Pending";
                case System.Net.WebExceptionStatus.PipelineFailure:
                    return "PipelineFailure";
                case System.Net.WebExceptionStatus.ProtocolError:
                    return "Protokol fejl";
                case System.Net.WebExceptionStatus.ProxyNameResolutionFailure:
                    return "ProxyNameResolutionFailure";
                case System.Net.WebExceptionStatus.ReceiveFailure:
                    return "Fejl ved modtagelse af data";
                case System.Net.WebExceptionStatus.RequestCanceled:
                    return "Forespørgsel blev afbrudt";
                case System.Net.WebExceptionStatus.RequestProhibitedByCachePolicy:
                    return "RequestProhibitedByCachePolicy";
                case System.Net.WebExceptionStatus.RequestProhibitedByProxy:
                    return "RequestProhibitedByProxy";
                case System.Net.WebExceptionStatus.SecureChannelFailure:
                    return "SecureChannelFailure";
                case System.Net.WebExceptionStatus.SendFailure:
                    return "Fejl ved afsendelse";
                case System.Net.WebExceptionStatus.ServerProtocolViolation:
                    return "ServerProtocolViolation";
                case System.Net.WebExceptionStatus.Success:
                    return "Succes";
                case System.Net.WebExceptionStatus.Timeout:
                    return "Timeout på forbindelse.";
                case System.Net.WebExceptionStatus.TrustFailure:
                    return "TrustFailure";
                case System.Net.WebExceptionStatus.UnknownError:
                    return "Ukendt fejl";
            }
            return "Helt ukendt fejl";
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="Orders"/> class from being created.
        /// </summary>
        /// <param name="row">DataReader row</param>
        private Orders(IDataReader row)
        {
            this.Initialize(row);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Sets the Oject values.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            this.Id = (long)row["Id"];
            this.Email = (string)row["Email"];
            this.ReceiptNumber = (string)row["ReceiptNumber"];
            this.CaptureId = (string)row["CaptureId"];
            if (row["LabelImage"] != DBNull.Value)
            {
                this.LabelImage = (byte[])row["LabelImage"];
            }
            else
            {
                this.LabelImage = null;
            }
            this.SalesDate = (DateTime)row["SalesDate"];
            this.ExpireDate = (DateTime)row["ExpireDate"];
            this.Status = (OrderStatusValues)int.Parse(row["OrderStatus"].ToString());

            if (row["AppType"] != DBNull.Value)
            {
                this._appTypeId = (int)row["AppType"];
            }

            this.SentToCint = false;
            if (row["SentToCint"] != DBNull.Value)
            {
                short sentToCintVal = (short)row["SentToCint"];
                this.SentToCint = sentToCintVal != 0;
            }

            if (row["PaymentMethod"] != DBNull.Value)
            {
                this._paymentMethod = (int)row["PaymentMethod"];
            }

            this.AppPlatform = (string)row["Platform"];
            this.AppVersion = (int)row["Version"];
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets all the orders in date range.
        /// </summary>
        /// <param name="captureId">The capture identifier.</param>
        /// <returns>
        /// List of Order Objects
        /// </returns>
        public static List<Orders> GetOrders(string captureId)
        {
            List<Orders> orderList = new List<Orders>();
            using (IDataReader row = Dal.Orders.GetOrders(captureId))
            {
                while (row.Read())
                {
                    Orders myOrders = new Orders(row);
                    orderList.Add(myOrders);
                }
            }

            return orderList;
        }

        /// <summary>
        /// Gets all the orders in date range.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>
        /// List of Order Objects
        /// </returns>
        public static List<Orders> GetOrders(DateTime fromDate, DateTime toDate)
        {
            List<Orders> orderList = new List<Orders>();
            using (IDataReader row = Dal.Orders.GetOrders(fromDate, toDate))
            {
                while (row.Read())
                {
                    Orders myOrders = new Orders(row);
                    orderList.Add(myOrders);
                }
            }

            return orderList;
        }

        /// <summary>
        /// Gets all the orders in date range grouped on amount. Add intervals to hashtable.
        /// 
        /// </summary>
        /// <example>
        ///  1-10, 11-100, 101-500, 501-1000 or more than 1000 codes
        /// </example>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>
        /// Hashtable containing intervals
        /// </returns>
        public static Hashtable GetOrdersIntervalsAmount(DateTime fromDate, DateTime toDate, string reportType)
        {
            Hashtable intervals = new Hashtable();

            int zero = 0;

            //  Revised 2014-09-02
            //	Derudover er der behov for justering af grænserne
            //	Antal koder (1, 2-5, 6-10, 11-100, over 100)

            intervals.Add("1", zero);
            intervals.Add("2", zero);
            intervals.Add("6", zero);
            intervals.Add("11", zero);
            intervals.Add("101", zero);

            using (IDataReader row = Dal.Orders.GetReportGroupedOnCustomers(fromDate, toDate, reportType))
            {
                while (row.Read())
                {
                    int amount = (int)row["Amount"];

                    if (amount > 100)
                        intervals["101"] = (int)intervals["101"] + 1;
                    else if (amount > 10)
                        intervals["11"] = (int)intervals["11"] + 1;
                    else if (amount > 5)
                        intervals["6"] = (int)intervals["6"] + 1;
                    else if (amount > 1)
                        intervals["2"] = (int)intervals["2"] + 1;
                    else
                        intervals["1"] = (int)intervals["1"] + 1;
                }
            }

            return intervals;
        }

        /// <summary>
        /// Gets all the orders in date range grouped on customer. Add intervals to hashtable
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <example>: 0-500, 501-1000, 1001-5000, 5001-10000, more than 10000 DKK</example>
        /// <returns>
        /// Hashtable containing intervals
        /// </returns>
        public static Hashtable GetOrdersIntervalsSales(DateTime fromDate, DateTime toDate, string reportType)
        {
            Hashtable intervals = new Hashtable();

            //  Revised 2014-09-02
            //	Derudover er der behov for justering af grænserne
            //	Sale DKK (0-50, 51-100, 101-500, 501-1000, over 1000)

            int zero = 0;
            intervals.Add("0", zero);
            intervals.Add("51", zero);
            intervals.Add("101", zero);
            intervals.Add("501", zero);
            intervals.Add("1001", zero);

            using (IDataReader row = Dal.Orders.GetReportGroupedOnCustomers(fromDate, toDate, reportType))
            {
                while (row.Read())
                {
                    double orderSum = (double)row["OrderSum"];

                    if (orderSum > 1000)
                        intervals["1001"] = (int)intervals["1001"] + 1;
                    else if (orderSum > 500)
                        intervals["501"] = (int)intervals["501"] + 1;
                    else if (orderSum > 100)
                        intervals["101"] = (int)intervals["101"] + 1;
                    else if (orderSum > 50)
                        intervals["51"] = (int)intervals["51"] + 1;
                    else
                        intervals["0"] = (int)intervals["0"] + 1;
                }
            }

            return intervals;
        }

        /// <summary>
        /// Gets all codes for order
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Unable to show codes before Order is saved</exception>
        public List<Codes> GetCodes()
        {
            if (this.Id == 0)
            {
                throw new ApplicationException("Unable to show codes before Order is saved");
            }

            List<Codes> codes = new List<Codes>();

            foreach (SubOrders subOrder in this.SubOrders)
            {
                codes.AddRange(subOrder.Codes);
            }

            return codes;
        }

        /// <summary>
        /// Gets all the orders in date range where email matches given email pattern.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="emailPattern">The email pattern.</param>
        /// <returns>
        /// List of Order Objects
        /// </returns>
        public static List<Orders> GetOrders(DateTime fromDate, DateTime toDate, string emailPattern)
        {
            List<Orders> orderList = new List<Orders>();
            using (IDataReader row = Dal.Orders.GetOrders(fromDate, toDate, emailPattern.Replace('*', '%')))
            {
                while (row.Read())
                {
                    Orders myOrders = new Orders(row);
                    orderList.Add(myOrders);
                }
            }
            return orderList;
        }

        /// <summary>
        /// Gets all the orders in date range where code matches given code
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="code">The code.</param>
        /// <returns>
        /// List of Order Objects
        /// </returns>
        public static List<Orders> GetOrdersFromCode(DateTime fromDate, DateTime toDate, string code)
        {
            List<Orders> orderList = new List<Orders>();
            using (IDataReader row = Bll.Codes.GetOrdersFromCode(fromDate, toDate, code))
            {
                while (row.Read())
                {
                    Orders myOrders = new Orders(row);
                    orderList.Add(myOrders);
                }
            }
            return orderList;
        }

        /// <summary>
        /// Adds the order log.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="severity">The severity.</param>
        /// <exception cref="System.ApplicationException">Unable to add to log before Order is saved</exception>
        public void AddLog(string text, PostNord.Portal.DigitalPostage.Bll.OrderLogs.SeverityValues severity)
        {
            if (this.Id == 0)
            {
                throw new ApplicationException("Unable to add to log before Order is saved");
            }
            PostNord.Portal.DigitalPostage.Bll.OrderLogs logEntry = new OrderLogs
            {
                OrderId = this.Id,
                Text = text,
                Severity = severity,
                Timestamp = DateTime.Now
            };
            logEntry.Save();
        }

        /// <summary>
        /// Adds information to the order log.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <exception cref="System.ApplicationException">Unable to add to log before Order is saved</exception>
        public void AddLog(string text)
        {
            if (this.Id == 0)
            {
                throw new ApplicationException("Unable to add to log before Order is saved");
            }
            PostNord.Portal.DigitalPostage.Bll.OrderLogs logEntry = new OrderLogs
            {
                OrderId = this.Id,
                Text = text,
                Severity = PostNord.Portal.DigitalPostage.Bll.OrderLogs.SeverityValues.Info,
                Timestamp = DateTime.Now
            };
            logEntry.Save();
        }

        /// <summary>
        /// Adds debug info to the order log.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <exception cref="System.ApplicationException">Unable to add to log before Order is saved</exception>
        public void AddLogDebug(string text)
        {
            if (this.Id == 0)
            {
                throw new ApplicationException("Unable to add to log before Order is saved");
            }
            PostNord.Portal.DigitalPostage.Bll.OrderLogs logEntry = new OrderLogs
            {
                OrderId = this.Id,
                Text = text,
                Severity = PostNord.Portal.DigitalPostage.Bll.OrderLogs.SeverityValues.Debug,
                Timestamp = DateTime.Now
            };
            logEntry.Save();
        }

        /// <summary>
        /// Adds Error info to the order log.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <exception cref="System.ApplicationException">Unable to add to log before Order is saved</exception>
        public void AddLogError(string text)
        {
            Logging.LogEvent(text, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
            if (this.Id == 0)
            {
                throw new ApplicationException("Unable to add to log before Order is saved");
            }
            PostNord.Portal.DigitalPostage.Bll.OrderLogs logEntry = new OrderLogs
            {
                OrderId = this.Id,
                Text = text,
                Severity = PostNord.Portal.DigitalPostage.Bll.OrderLogs.SeverityValues.Error,
                Timestamp = DateTime.Now
            };
            logEntry.Save();
        }

        /// <summary>
        /// Adds the order log.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="timeStamp">The time stamp.</param>
        /// <exception cref="System.ApplicationException">Unable to add to log before Order is saved</exception>
        public void AddLog(string text, PostNord.Portal.DigitalPostage.Bll.OrderLogs.SeverityValues severity, DateTime timeStamp)
        {
            if (this.Id == 0)
            {
                Logging.LogEvent("Error could not log the following message due to a missing order id: " + text, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                throw new ApplicationException("Unable to add to log before Order is saved");
            }
            PostNord.Portal.DigitalPostage.Bll.OrderLogs logEntry = new OrderLogs
            {
                OrderId = this.Id,
                Text = text,
                Severity = severity,
                Timestamp = timeStamp
            };
            logEntry.Save();
        }

        /// <summary>
        /// Adds sub order to order.
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <param name="weight">The weight.</param>
        /// <param name="priority">The priority.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="price">The price.</param>
        /// <param name="productListId">The product list identifier.</param>
        /// <returns></returns>
        public SubOrders AddSubOrder(string productName, short weight, string priority, string destination, int amount, double price, int productListId, double PriceWithoutVAT, int VAT)
        {
            // Force loading of SubOrders
            int count = this.SubOrders.Count;
            SubOrders subOrder = new SubOrders(this.Id, productName, weight, priority, destination, amount, price, PriceWithoutVAT, VAT);
            subOrder.ProductListId = productListId;
            subOrder.Save();
            this.AddLog(string.Format("SubOrder added to Order. Product: {0} Weight: {1} Priority: {2} Destination: {3} Amount: {4} Price: {5} Price without VAT: {6} VAT: {7} ListId: {8}", productName, weight, priority, destination, amount, price, PriceWithoutVAT, VAT, productListId), Bll.OrderLogs.SeverityValues.Info);
            this.SubOrders.Add(subOrder);
            return subOrder;
        }

        /// <summary>
        /// Add SapId for all SubOrders.
        /// </summary>
        /// <exception cref="System.ApplicationException">
        /// Error loading Price info
        /// </exception>
        /// <exception cref="System.IndexOutOfRangeException">
        /// Exception thrown if product is not found or SapId is not found
        /// </exception>
        public void AddSapIds()
        {
            if (this.HasSapIds())
            {
                return;
            }

            try
            {
                Guid siteId = SPContext.Current.Site.ID;
                string adminUrl = ConfigSettings.GetAdminWebUrl(SPContext.Current.Web);

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite siteElevated = new SPSite(siteId))
                    {
                        using (SPWeb web = siteElevated.OpenWeb(adminUrl))
                        {
                            SPList products = web.Lists[ProductInfo.PRODUCT_LIST_NAME];
                            Guid sapAFieldId = products.Fields.GetFieldByInternalName("SapIdA").Id;
                            Guid sapBFieldId = products.Fields.GetFieldByInternalName("SapIdB").Id;

                            foreach (var subOrder in this.SubOrders)
                            {
                                if (subOrder.ProductListId == 0)
                                {
                                    string error = string.Format("ProductListId missing for SubOrderId: {0}", subOrder.Id);
                                    this.AddLogError(error);
                                    throw new IndexOutOfRangeException(error);
                                }

                                SPListItem prodInfo = products.GetItemById(subOrder.ProductListId);

                                int sapIdA = 0;
                                int sapIdB = 0;

                                if (prodInfo[sapAFieldId] != null)
                                {
                                    sapIdA = int.Parse(prodInfo[sapAFieldId].ToString());
                                }

                                if (prodInfo[sapBFieldId] != null)
                                {
                                    sapIdB = int.Parse(prodInfo[sapBFieldId].ToString());
                                }

                                if (sapIdA == 0 || sapIdB == 0)
                                {
                                    throw new IndexOutOfRangeException(string.Format("SapId missing for SubOrderId: {0}", subOrder.Id));
                                }

                                switch (subOrder.Priority)
                                {
                                    case "A":
                                        subOrder.SapId = sapIdA;
                                        break;
                                    case "B":
                                        subOrder.SapId = sapIdB;
                                        break;
                                    default:
                                        throw new ApplicationException(string.Format("Unknown priority: {0}", subOrder.Priority));
                                }
                                subOrder.Save();
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error loading Price info", ex);
            }
        }

        /// <summary>
        /// Calls NETS and creates payment. Used by WebInterface
        /// </summary>
        /// <returns>
        /// Redirect Url is returned
        /// </returns>
        public string CreatePayment()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolTypeExtensions.Tls12;
            // Disable Certficate check
            if (System.Net.ServicePointManager.ServerCertificateValidationCallback == null)
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            }

            if (this.Id == 0)
            {
                throw new ApplicationException("Order has not been saved. Unable to create payment.");
            }

            if (string.IsNullOrEmpty(this.Email))
            {
                throw new ApplicationException("Email is not set. Unable to create payment.");
            }

            double orderSum = this.OrderSum;
            if (orderSum <= 0)
            {
                throw new ApplicationException("Ordersum should be larger than 0. Unable to create payment.");
            }

            // Loop SubOrders and Add SapId for each order
            AddSapIds();

            this.CreateCodes();

            // load payment info!
            string receiptUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("ReceiptUrl");
            //is a relative URL?
            if (receiptUrl.IndexOf("http", StringComparison.OrdinalIgnoreCase) == -1)
            {
                receiptUrl = string.Format("{0}{1}", SPContext.Current.Site.Url, receiptUrl);
            }
            string merchantId = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("MerchantId");
            string token = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("Token");
            string terminalUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TerminalUrl");
            string netaxptWsUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("NetaxeptWebserviceUrl");
            string currencyCode = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("CurrencyCode", "DKK");
            string orderDesc = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("OrderDescription", "DigitalPostage");
            string webServicePlatform = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("WebServicePlatform", "DOTNET20");

            if (string.IsNullOrEmpty(receiptUrl) || string.IsNullOrEmpty(merchantId) || string.IsNullOrEmpty(token) ||
                string.IsNullOrEmpty(terminalUrl) || string.IsNullOrEmpty(netaxptWsUrl) || string.IsNullOrEmpty(currencyCode))
            {
                throw new ApplicationException("Payment information is missing from Configuration. Unable to create payment");
            }

            this.AddLog("Contacting NETS ");
            try
            {
                using (NetAxept.Netaxept client = new NetAxept.Netaxept())
                {
                    int netsTimeOut = 10000;
                    client.Url = ConfigSettings.GetText("NetaxeptWebserviceUrl");
                    int.TryParse(ConfigSettings.GetText("NetsTimeOut", "10000"), out netsTimeOut);
                    client.Timeout = netsTimeOut;

                    client.Url = netaxptWsUrl;

                    // Setup the terminal parameters (required parameters for Netaxept-hosted terminal)
                    NetAxept.Terminal terminal = new NetAxept.Terminal();
                    terminal.OrderDescription = orderDesc;
                    terminal.RedirectUrl = receiptUrl;
                    terminal.Language = "da_DK";

                    // Setup the order (required parameters)
                    NetAxept.Order netsOrder = new NetAxept.Order();
                    netsOrder.Amount = Helpers.HelperClass.ConvertPaymentAmount(orderSum);           // Total price in øre as a string
                    netsOrder.CurrencyCode = currencyCode;          // Danish Kroner
                    netsOrder.OrderNumber = this.Id.ToString();     // A number to be able to track the order

                    // Setup the environment (required for all web service clients)
                    NetAxept.Environment environment = new NetAxept.Environment();
                    environment.WebServicePlatform = webServicePlatform;

                    // Finally the complete register request
                    NetAxept.RegisterRequest request = new NetAxept.RegisterRequest();
                    request.Order = netsOrder;
                    request.Terminal = terminal;
                    request.Environment = environment;

                    NetAxept.RegisterResponse response = client.Register(merchantId, token, request);
                    this.AddLog(string.Format("Register payment. Transaction Id: {0} ", response.TransactionId));

                    this.CaptureId = response.TransactionId;
                    this.Status = OrderStatusValues.ReadyForPayment;
                    this.Save();

                    return String.Format(terminalUrl, merchantId, response.TransactionId);
                }
            }
            catch (System.Net.WebException ex)
            {
                this.AddLogError(string.Format("WebException: Status:{0} Message: {1} Details:{2}", ex.Status, ex.Message, ex.InnerException != null ? ex.InnerException.Message : ""));
                throw new ApplicationException(GetErrorMessage(ex));
            }
            catch (Exception ex)
            {
                string error = string.Format("Error Creating payment. {0}. ", ex.Message);
                if (ex.InnerException != null)
                {
                    error += ex.InnerException.Message;
                }
                this.AddLogError(error);
                throw new ApplicationException("");
            }
        }

        /// <summary>
        /// Creates the payment mobile pay.
        /// </summary>
        /// <remarks>
        /// Status is changed. Order is sent to Mobile Pay from APP.
        /// </remarks>
        public void CreatePaymentMobilePay()
        {
            this.Status = OrderStatusValues.ReadyForPaymentMobilePay;
            this.CreateCodes();
            this.Save();
        }

        /// <summary>
        /// Calls NETS and create NetAxcept payment. Used by WebService
        /// </summary>
        /// <returns>
        /// Redirect Url is returned
        /// </returns>
        /// <exception cref="System.ApplicationException">
        /// Order has not been saved. Unable to create payment.
        /// or
        /// Email is not set. Unable to create payment.
        /// or
        /// Ordersum should be larger than 0. Unable to create payment.
        /// or
        /// Payment information is missing from Configuration. Unable to create payment
        /// or
        /// or
        /// </exception>
        public string CreatePaymentNetAxcept()
        {
            return CreatePaymentNetAxcept(false, string.Empty);
        }

        /// <summary>
        /// Calls NETS and create NetAxcept payment. Used by WebService
        /// </summary>
        /// <param name="saveCreditCardInfo">if set to <c>true</c> [save credit card information].</param>
        /// <param name="cardHash">The card hash.</param>
        /// <returns>
        /// Redirect Url is returned
        /// </returns>
        /// <exception cref="System.ApplicationException">
        /// Order has not been saved. Unable to create payment.
        /// or
        /// Email is not set. Unable to create payment.
        /// or
        /// Ordersum should be larger than 0. Unable to create payment.
        /// or
        /// Payment information is missing from Configuration. Unable to create payment
        /// or
        /// or
        /// </exception>
        public string CreatePaymentNetAxcept(bool saveCreditCardInfo, string cardHash)
        {
            if (this.Id == 0)
            {
                throw new ApplicationException("Order has not been saved. Unable to create payment.");
            }

            if (string.IsNullOrEmpty(this.Email))
            {
                throw new ApplicationException("Email is not set. Unable to create payment.");
            }

            double orderSum = this.OrderSum;
            if (orderSum <= 0)
            {
                throw new ApplicationException("Ordersum should be larger than 0. Unable to create payment.");
            }

            this.AddSapIds();
            this.CreateCodes();

            // load payment info!
            string receiptUrl = string.Format("{0}{1}", SPContext.Current.Site.Url, Portal.DigitalPostage.Helpers.ConfigSettings.GetText("ReceiptWsUrl", "/_layouts/DigitalPostageWs/WsReceipt.aspx"));
            string merchantId = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("MerchantId");
            string token = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("Token");
            string terminalUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TerminalUrl");

            // Load Mobile Url if available
            if (this.AppPlatform != WEBSHOP_NAME)
            {
                terminalUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TerminalUrlMobile", terminalUrl);
            }

            string netaxptWsUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("NetaxeptWebserviceUrl");
            string currencyCode = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("CurrencyCode", "DKK");
            string orderDesc = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("OrderDescription", "DigitalPostage");
            string webServicePlatform = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("WebServicePlatform", "DOTNET20");

            if (string.IsNullOrEmpty(merchantId) || string.IsNullOrEmpty(token) ||
                string.IsNullOrEmpty(terminalUrl) || string.IsNullOrEmpty(netaxptWsUrl) || string.IsNullOrEmpty(currencyCode))
            {
                throw new ApplicationException("Payment information is missing from Configuration. Unable to create payment");
            }

            this.AddLog("Contacting NETS ");
            try
            {
                using (NetAxept.Netaxept client = new NetAxept.Netaxept())
                {
                    int netsTimeOut = 10000;
                    client.Url = ConfigSettings.GetText("NetaxeptWebserviceUrl");
                    int.TryParse(ConfigSettings.GetText("NetsTimeOut", "10000"), out netsTimeOut);
                    client.Timeout = netsTimeOut;

                    client.Url = netaxptWsUrl;

                    // Setup the terminal parameters (required parameters for Netaxept-hosted terminal)
                    NetAxept.Terminal terminal = new NetAxept.Terminal();
                    terminal.OrderDescription = orderDesc;
                    terminal.RedirectUrl = receiptUrl;
                    terminal.Language = "da_DK";
                    terminal.SinglePage = "TRUE";

                    // Setup the order (required parameters)
                    NetAxept.Order netsOrder = new NetAxept.Order();
                    netsOrder.Amount = Helpers.HelperClass.ConvertPaymentAmount(orderSum);           // Total price in øre as a string
                    netsOrder.CurrencyCode = currencyCode;          // Danish Kroner
                    netsOrder.OrderNumber = this.Id.ToString();     // A number to be able to track the order

                    // Setup the environment (required for all web service clients)
                    NetAxept.Environment environment = new NetAxept.Environment();
                    environment.WebServicePlatform = webServicePlatform;

                    // Finally the complete register request
                    NetAxept.RegisterRequest request = new NetAxept.RegisterRequest();
                    request.Order = netsOrder;
                    request.Terminal = terminal;
                    request.Environment = environment;

                    if (saveCreditCardInfo)
                    {
                        NetAxept.Recurring recurringInfo = new NetAxept.Recurring
                        {
                            Type = "S"  // Store CreditCard information in NETS
                        };

                        // If cardHash 
                        if (cardHash != string.Empty)
                        {
                            Guid crdHash = new Guid(cardHash);
                            Cards crd = new Cards(crdHash);
                            recurringInfo.PanHash = crd.PanHash;
                        }
                        request.Recurring = recurringInfo;
                    }

                    NetAxept.RegisterResponse response = client.Register(merchantId, token, request);
                    if (saveCreditCardInfo)
                    {
                        if (cardHash != string.Empty)
                        {
                            this.AddLog(string.Format("Register NetAxcept Reuse Payment ({0}). Transaction Id: {1} ", cardHash, response.TransactionId));
                        }
                        else
                        {
                            this.AddLog(string.Format("Register NetAxcept Save Payment. Transaction Id: {0} ", response.TransactionId));
                        }
                        this.Status = OrderStatusValues.ReadyForPaymentCardSave;
                    }
                    else
                    {
                        this.Status = OrderStatusValues.ReadyForPayment;
                        this.AddLog(string.Format("Register NetAxcept payment. Transaction Id: {0} ", response.TransactionId));
                    }

                    this.CaptureId = response.TransactionId;
                    this.Save();

                    return String.Format(terminalUrl, merchantId, response.TransactionId);
                }
            }
            catch (System.Web.Services.Protocols.SoapHeaderException ex)
            {
                this.AddLogError(string.Format("SoapHeaderException: Code:{0} Message: {1} Details:{2}", ex.Code, ex.Message, ex.InnerException != null ? ex.InnerException.Message : ""));
                throw new ApplicationException(ex.Message);
            }
            catch (System.Net.WebException ex)
            {
                this.AddLogError(string.Format("WebException: Status:{0} Message: {1} Details:{2}", ex.Status, ex.Message, ex.InnerException != null ? ex.InnerException.Message : ""));
                throw new ApplicationException(GetErrorMessage(ex));
            }
            catch (Exception ex)
            {
                string error = string.Format("Error Creating payment. {0}. ", ex.Message);
                if (ex.InnerException != null)
                {
                    error += ex.InnerException.Message;
                }
                this.AddLogError(error);
                throw new ApplicationException("");
            }
        }


        /// <summary>
        /// Gets all orders.
        /// </summary>
        /// <returns>
        /// Orders found, if no orders are found an empty list is returned
        /// </returns>
        public static List<Orders> GetAllOrders()
        {
            List<Orders> OrdersList = new List<Orders>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.Orders.OrdersSelectAll())
            {
                while (row.Read())
                {
                    Orders myOrders = new Orders(row);
                    OrdersList.Add(myOrders);
                }
            }
            return OrdersList;
        }

        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Returns order if one is found, otherwise returns null</returns>
        public static Orders GetOrder(long id)
        {
            Orders order = null;
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.Orders.OrdersSelectRow(id))
            {
                if (row.Read())
                {
                    order = new Orders(row);
                }
            }
            return order;
        }

        /// <summary>
        /// Reloads Order Data.
        /// </summary>
        public void Reload()
        {
            this.Initialize(Dal.Orders.OrdersSelectRow(this.Id));
        }


        public bool HasSapIds()
        {
            bool hasIds = true;
            if (this.SubOrders.Count == 0)
            {
                return false;
            }
            foreach (var subOrder in this.SubOrders)
            {
                if (subOrder.SapId == 0)
                {
                    hasIds = false;
                }
            }
            return hasIds;
        }

        /// <summary>
        /// Sets the status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <remarks>
        /// Only called from WebService!!!
        /// </remarks>
        public void SetStatus(OrderStatusValues status)
        {
            // Are we updating status to Paid??
            if (status == OrderStatusValues.Paid && this.Status != OrderStatusValues.Paid)
            {
                this.AddSapIds();
                this.Status = status;
                this.ReceiptNumber = this.Id.ToString();
                this.Save();
            }
            else
            {
                this.Status = status;
                this.Save();
            }
            this.AddLogDebug(string.Format("Status updated to : {0}", status.ToString()));
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        /// <remarks>
        /// Image Information is not saved
        /// </remarks>
        public void Save()
        {
            if (this.Id == 0)
            {
                // Default expire period is 180 days
                this.ExpireDate = this.SalesDate.AddDays(Orders.PostageValidInDays);
                this.Id = (long)PostNord.Portal.DigitalPostage.Dal.Orders.OrdersInsertRow(this.Email.ToLower(), this.ReceiptNumber, this.CaptureId, this.SalesDate, this.ExpireDate, this.OrderSum, this.CreditSum, this.Saldo, (short)this.Status);
            }
            else
            {
                PostNord.Portal.DigitalPostage.Dal.Orders.OrdersUpdateRow(this.Id, this.Email.ToLower(), this.ReceiptNumber, this.CaptureId, this.SalesDate, this.ExpireDate, this.OrderSum, this.CreditSum, this.Saldo, (short)this.Status);
            }
        }

        /// <summary>
        /// Sets the application information.
        /// </summary>
        /// <param name="paymentMethod">The payment method.</param>
        /// <param name="platform">The platform.</param>
        /// <param name="version">The version.</param>
        public void SetAppInfo(PaymentMethods paymentMethod, string platform, int version)
        {
            AppTypes appType = new AppTypes(platform, version);
            _paymentMethod = (int)paymentMethod;
            _appTypeId = appType.Id;
            Dal.Orders.SetAppInfo(this.Id, _paymentMethod, _appTypeId);

            if (platform != WEBSHOP_NAME)
            {
                this.ExpireDate = this.SalesDate.AddDays(Orders.PostageMobileValidInDays);
            }
            this.Save();
        }

        /// <summary>
        /// Refunds the amount.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="refunds">HashTable containing SubOrderId as Key and Number of codes refunded as value</param>
        /// <exception cref="System.IndexOutOfRangeException">Id is zero</exception>
        /// <exception cref="System.ApplicationException">Order can not be Refunded since it does not have a receipt Nr.
        /// or
        /// Order can not be Refunded since it does not have a Capture Id.</exception>
        public void RefundAmount(double amount, Hashtable refunds)
        {
            if (this.Id == 0)
            {
                throw new IndexOutOfRangeException("Id is zero");
            }

            if (string.IsNullOrEmpty(this.ReceiptNumber))
            {
                throw new ApplicationException("Order can not be Refunded since it does not have a receipt Nr.");
            }

            if (string.IsNullOrEmpty(this.CaptureId))
            {
                throw new ApplicationException("Order can not be Refunded since it does not have a Capture Id.");
            }

            if (amount <= 0)
            {
                return;
            }

            if (this.PaymentMethod == PaymentMethods.MobilePay)
            {
                this.RefundMobilePay(amount, refunds);
            }
            else
            {
                this.RefundNets(amount, refunds);
            }
        }

        /// <summary>
        /// Refunds the order.
        /// </summary>
        public void RefundOrder()
        {
            this.AddLog("Auto refunding order");

            Bll.Refunds refund = new Refunds();
            refund.OrderNr = this.Id;
            refund.Timestamp = DateTime.Now;

            refund.Address = "None";
            refund.City = "None";
            refund.Message = "None";
            refund.Name = "None";
            refund.Phone = "None";
            refund.PostNumber = 1000;
            refund.Message = "Auto refunded";

            if (!this.SentToCint)
            {
                try
                {
                    this.AddLogDebug("Adding SapIds");
                    this.AddSapIds();
                    this.AddLogDebug("Queing order for CINT");
                    OrderQueue.Add(this.Id, DateTime.Now);
                    OrderQueue.FlushQueue(5);
                }
                catch (Exception ex)
                {
                    this.AddLogError(string.Format("Error adding SAP Ids: {0}", ex.Message));
                    return;
                }
            }

            if (this.Status != OrderStatusValues.Paid)
            {
                this.SetStatus(OrderStatusValues.Paid);
            }

            var codes = this.GetCodes();

            if (codes.Count == 0)
            {
                this.CreateCodes();
            }
            this.ActivateCodes();

            // Reload suborders
            _suborders = DigitalPostage.Bll.SubOrders.GetOrderSubOrders(this.Id);
            codes = this.GetCodes();

            refund.RefundCodes(codes);
            refund.SendMail();
            this.AddLog("Auto refund mail sent");

            this.SetStatus(OrderStatusValues.Refunded);
        }

        /// <summary>
        /// Gets the pn configuration.
        /// </summary>
        /// <returns></returns>
        private PnConfig GetPnConfig()
        {
            return new PnConfig()
            {
                ClientContentEncryptionPassword = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("ClientContentEncryptionPassword"),
                ClientContentEncryptionPath = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("ClientContentEncryptionPath"),
                ClientSignaturePassword = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("ClientSignaturePassword"),
                ClientSignaturePath = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("ClientSignaturePath"),
                DbContentEncryptionPath = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("DBContentEncryptionPath"),
                DbSignaturePath = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("DBSignaturePath")
            };
        }

        /// <summary>
        /// Gets the mobile pay payment information.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">
        /// MobilePayMerchantId not found. Please add to Configuration
        /// or
        /// MobilePayClientId not found. Please add to Configuration
        /// or
        /// MobilePay not found in version 1!
        /// </exception>
        public DB.MobilePayV02.GetStatus.dacGetStatusOutput GetMobilePayPaymentInfo()
        {

            PnConfig configuration = this.GetPnConfig();
            string error = string.Empty;

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            // Is this is version 1 we have an error
            if (this.IsOldVersion)
            {
                throw new ApplicationException("MobilePay not found in version 1!");
            }

            return MobilePay.Proxy.CallGetStatus(configuration, clientId, merchantId, this.MobilePayOrderNr);
        }

        /// <summary>
        /// Refunds the amount.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="refunds">HashTable containing SubOrderId as Key and Number of codes refunded as value</param>
        /// <exception cref="System.IndexOutOfRangeException">Id is zero</exception>
        /// <exception cref="System.ApplicationException">Order can not be Refunded since it does not have a receipt Nr.
        /// or
        /// Order can not be Refunded since it does not have a Capture Id.</exception>
        private void RefundMobilePay(double amount, Hashtable refunds)
        {
            PnConfig configuration = this.GetPnConfig();

            string merchantId = ConfigSettings.GetText("MobilePayMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                this.AddLogError("MobilePayMerchantId not found. Unable to validate Mobile Payment.");
                throw new ApplicationException("MobilePayMerchantId not found. Please add to Configuration");
            }

            string clientId = ConfigSettings.GetText("MobilePayClientId");
            if (string.IsNullOrEmpty(clientId))
            {
                this.AddLogError("MobilePayClientId not found. Unable to validate Mobile Payment.");
                throw new ApplicationException("MobilePayClientId not found. Please add to Configuration");
            }

            // Is this is version 1 we have an error
            if (this.IsOldVersion)
            {
                this.AddLogError("MobilePay not found in version 1!");
                throw new ApplicationException("MobilePay not found in version 1!");
            }

            try
            {
                this.AddLog(string.Format("Refunding: OrderNr: {0} MerchantId:{1} ClientId:{2}", this.MobilePayOrderNr, merchantId, clientId));
                var dbOutput = MobilePay.Proxy.CallGetStatus(configuration, clientId, merchantId, this.MobilePayOrderNr);

                string error = string.Empty;
                if (dbOutput.ReturnCode != Helpers.MobilePay.RETURNCODE_OK)
                {
                    error = string.Format("MobilePay Error! Error code: {0} ({1}) Reason code:{2} ({3})", dbOutput.ReturnCode, Helpers.MobilePay.V2Returncode(dbOutput.ReturnCode), dbOutput.ReasonCode, Helpers.MobilePay.V2Reasoncode(dbOutput.ReasonCode));
                    this.AddLogError(error);
                    throw new ApplicationException(error);
                }

                if (dbOutput.LatestPaymentStatus == Helpers.MobilePay.PAYMENTSTATUS_REFUNDED)
                {
                    error = "MobilePay Error: Order already refunded.";
                    this.AddLogError(error);
                    throw new ApplicationException(error);
                }

                if (dbOutput.LatestPaymentStatus != Helpers.MobilePay.PAYMENTSTATUS_CAPTURED)
                {
                    error = string.Format("MobilePay Error: Order not captured. Status: {0}", dbOutput.LatestPaymentStatus);
                    this.AddLogError(error);
                    throw new ApplicationException(error);
                }

                if ((double)dbOutput.OriginalAmount != this.OrderSum)
                {
                    error = string.Format("MobilePay Error: Order amount differs: Expected:{0:0.00} Got: {1:0.00}", this.OrderSum, dbOutput.OriginalAmount);
                    this.AddLogError(error);
                    throw new ApplicationException(error);
                }
                if (dbOutput.OriginalTransactionId != this.CaptureId)
                {
                    this.AddLog(string.Format("Updating transaction id from:{0} to: {1}", this.CaptureId, dbOutput.OriginalTransactionId));
                    this.CaptureId = dbOutput.OriginalTransactionId;

                    //error = string.Format("MobilePay Error: TransactionId differs: Expected:{0} Got: {1}", dbOutput.OriginalTransactionId, this.CaptureId);
                    //this.AddLogError(error);
                    //throw new ApplicationException(error);
                }

                var dbRefund = MobilePay.Proxy.CallRefund(configuration, clientId, merchantId, this.MobilePayOrderNr, (decimal)this.OrderSum);

                if (dbRefund.ReturnCode != Helpers.MobilePay.RETURNCODE_OK)
                {
                    error = string.Format("MobilePay Refund Error: Error: {0} ({1}) Reason: {2} ({3}) ", dbRefund.ReturnCode, Helpers.MobilePay.V2Returncode(dbRefund.ReturnCode), dbRefund.ReasonCode, Helpers.MobilePay.V2Reasoncode(dbRefund.ReasonCode));
                    this.AddLogError(error);
                    throw new ApplicationException(error);
                }

                this.AddLog(string.Format("Order refunded : {0} Status code: {1} Reason code: {2}", amount, dbRefund.ReturnCode, dbRefund.ReasonCode));
                this.Save();
            }
            catch (Exception ex)
            {
                this.AddLogError(string.Format("MobilePay error crediting : {0}", ex.Message));
                throw new ApplicationException(string.Format("MobilePay error crediting : {0}", ex.Message));
            }
        }

        /// <summary>
        /// Refunds the amount.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="refunds">HashTable containing SubOrderId as Key and Number of codes refunded as value</param>
        /// <exception cref="System.IndexOutOfRangeException">Id is zero</exception>
        /// <exception cref="System.ApplicationException">Order can not be Refunded since it does not have a receipt Nr.
        /// or
        /// Order can not be Refunded since it does not have a Capture Id.</exception>
        private void RefundNets(double amount, Hashtable refunds)
        {
            string merchantId = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("MerchantId");
            string token = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("Token");
            string terminalUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TerminalUrl");
            string netaxptWsUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("NetaxeptWebserviceUrl");

            // If this is version 1 call Old NETS Merchant Id
            if (this.IsOldVersion)
            {
                merchantId = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("MerchantIdVersion1");
                token = Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TokenVersion1");
                if (merchantId == string.Empty || token == string.Empty)
                {
                    throw new ApplicationException("Merchant Id or Token for version 1 not found!");
                }
            }

            try
            {
                using (NetAxept.Netaxept client = new NetAxept.Netaxept())
                {
                    client.Url = ConfigSettings.GetText("NetaxeptWebserviceUrl");
                    client.Url = netaxptWsUrl;

                    NetAxept.ProcessRequest request = new NetAxept.ProcessRequest();
                    request.TransactionId = this.CaptureId;
                    request.Operation = "CREDIT";
                    request.TransactionReconRef = this.Id.ToString();

                    request.TransactionAmount = Helpers.HelperClass.ConvertPaymentAmount(amount);
                    NetAxept.ProcessResponse response = client.Process(merchantId, token, request);

                    this.AddLog(string.Format("Refunded : {0} Status code: {1} Message: {2}", amount, response.ResponseCode, response.ResponseText));
                    this.Save();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("NETS error crediting : {0}", ex.Message));
            }
        }



        /// <summary>
        /// Deletes this instance.
        /// </summary>
        /// <exception cref="System.IndexOutOfRangeException">Id is zero</exception>
        public void Delete()
        {
            if (this.Id == 0)
            {
                throw new IndexOutOfRangeException("Id is zero");
            }
            PostNord.Portal.DigitalPostage.Dal.Orders.OrdersDeleteRow(this.Id);
            this.Id = 0;
        }

        /// <summary>
        /// Sets the image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <exception cref="System.IndexOutOfRangeException">Id is zero. Unable to set image before Order is saved!</exception>
        public void SetImage(byte[] image)
        {
            this.LabelImage = image;
            if (this.Id == 0)
            {
                throw new IndexOutOfRangeException("Id is zero. Unable to set image before Order is saved!");
            }
            PostNord.Portal.DigitalPostage.Dal.Orders.OrdersUpdateImage(this.Id, this.LabelImage, this.ImageSize);
        }

        /// <summary>
        /// Converts the order into XML
        /// </summary>
        /// <returns>XML string</returns>
        public string Xml()
        {
            string xml = string.Empty;

            using (MemoryStream mem = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Orders));
                serializer.Serialize(mem, this);
                using (StreamWriter writer = new StreamWriter(mem))
                {
                    writer.Flush();
                    mem.Position = 0;
                    using (StreamReader reader = new StreamReader(mem))
                    {
                        xml = reader.ReadToEnd();
                    }
                }
            }
            return xml;
        }

        /// <summary>
        /// Sends the order to mail address in order.
        /// </summary>
        public void SendMail()
        {
            AddEmailToQueue(this.Email);
        }

        /// <summary>
        /// Sends the order to mail address in order.
        /// </summary>
        public void SendMail(bool receiptOnly)
        {
            AddEmailToQueue(this.Email);
        }

        /// <summary>
        /// Sends the order to mail address in order.
        /// </summary>
        public void SendMail(bool receiptOnly, Guid siteId)
        {
            _siteId = siteId;
            AddEmailToQueue(this.Email);
        }

        private void AddEmailToQueue(string email)
        {
            this.AddLog(string.Format("Creating mail queue data : {0}", email));
            try
            {
                string receiptPdfTemplateUrl;
                string receiptMailTemplateUrl;
                // Are we called from WS and do we have the templates?
                if (this.AppPlatform != "WebShop")
                {
                    receiptPdfTemplateUrl = ConfigSettings.GetText("ReceiptPdfTemplateWS");
                    receiptMailTemplateUrl = ConfigSettings.GetText("ReceiptMailTemplateWS");
                }
                else
                {
                    receiptPdfTemplateUrl = ConfigSettings.GetText("ReceiptPdfTemplate");
                    receiptMailTemplateUrl = ConfigSettings.GetText("ReceiptMailTemplate");
                }

                var emailData = new Entity.EmailData();
                emailData.RecieptBody = this.GetHtml(receiptPdfTemplateUrl);
                emailData.MessageBody = this.GetHtml(receiptMailTemplateUrl);
                emailData.PdfLabels = GetProductsJSON();
                emailData.OrderId = this.Id;
                emailData.LabelImage = this.LabelImage;
                emailData.ImageSize = this.ImageSize;
                emailData.CreationDatetime = DateTime.Now;
                emailData.RecipientEmail = email;
                var dalEmailDate = new Dal.EmailData();
                //add a row to the email queue
                int rowsAffected = dalEmailDate.InsertRow(emailData);
                
                if (rowsAffected > 0)
                {
                    this.AddLog(string.Format("Mail added to queue in datasource"));
                }
                else
                {
                    this.AddLogError(string.Format("Error, mail not added to datasource"));
                }

            }
            catch (Exception ex)
            {
                this.AddLogError(string.Format("Error adding mail to queue: {0}", ex.Message));
            }

        }

        /// <summary>
        /// Sends the mail to given email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="receiptOnly">if set to <c>true</c> Codes are not sent. Only Receipt. Used for WebService calls</param>
        /// <exception cref="System.ApplicationException"></exception>
        [Obsolete("replased by AddEmailToQueue")]
        public void SendMail(string email, bool receiptOnly)
        {
            AddEmailToQueue(email); //adding it here to compare the original with the new feature

            this.AddLog(string.Format("Sending Mail: {0}", email));

            string labelTemplateUrl = ConfigSettings.GetText("LabelTemplate");

            string receiptPdfTemplateUrl = string.Empty;
            string receiptMailTemplateUrl = string.Empty;

            // Are we called from WS and do we have the templates?
            if (this.AppPlatform != "WebShop")
            {
                receiptPdfTemplateUrl = ConfigSettings.GetText("ReceiptPdfTemplateWS");
                receiptMailTemplateUrl = ConfigSettings.GetText("ReceiptMailTemplateWS");
            }
            else
            {
                receiptPdfTemplateUrl = ConfigSettings.GetText("ReceiptPdfTemplate");
                receiptMailTemplateUrl = ConfigSettings.GetText("ReceiptMailTemplate");
            }
            string filename = ConfigSettings.GetText("ReceiptPdfFileName", "Kvittering.pdf");
            string codesPdfFilename = ConfigSettings.GetText("CodesFileName", "Portokoder.pdf");
            string mailTitle = ConfigSettings.GetText("LabelMailTitle", "Kvittering");

            byte[] receiptData = new byte[1];

            string bodyHtml = this.GetHtml(receiptMailTemplateUrl);
            string receiptPdfHtml = this.GetHtml(receiptPdfTemplateUrl);

            SmtpClient mailClient = new SmtpClient(ConfigSettings.GetText("SMTPServer"));
            MailMessage message = new MailMessage(ConfigSettings.GetText("ReplyMailAddress"), email);
            message.IsBodyHtml = true;
            message.Body = bodyHtml;
            message.Subject = TextSettings.GetText("MailSubject");
            byte[] pdfReceipt = null;
            byte[] labelPdf = null;
            if (!receiptOnly)
            {
                labelPdf = this.GenerateLabels(labelTemplateUrl);

                if (labelPdf == null)
                {
                    this.AddLogError(string.Format("Generate Labels failed for Template: {0}", labelTemplateUrl));
                }

            }

            if (labelPdf != null)
            {
                using (MemoryStream labelStream = new MemoryStream(labelPdf))
                {
                    message.Attachments.Add(new Attachment(labelStream, codesPdfFilename, "application/pdf"));

                    pdfReceipt = Helpers.Pdf.CreatePdf(receiptPdfHtml, PageSize.A4, 10f, 10f, 10f, 0f);

                    using (MemoryStream pdfStream = new MemoryStream(pdfReceipt))
                    {
                        Attachment att = new Attachment(pdfStream, filename, "application/pdf");
                        message.Attachments.Add(att);
                        try
                        {
                            mailClient.Send(message);
                        }
                        catch (Exception ex)
                        {
                            this.AddLogError(string.Format("Error sending label mail: {0}", ex.Message));
                        }
                    }
                }
                this.AddLog("Codes sent");
            }
            else
            {
                pdfReceipt = Helpers.Pdf.CreatePdf(receiptPdfHtml, PageSize.A4, 10f, 10f, 10f, 0f);

                using (MemoryStream pdfStream = new MemoryStream(pdfReceipt))
                {
                    Attachment att = new Attachment(pdfStream, filename, "application/pdf");
                    message.Attachments.Add(att);

                    try
                    {
                        mailClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        this.AddLogError(string.Format("Error sending receipt mail: {0}", ex.Message));
                    }
                }
                this.AddLog(string.Format("Receipt sent. Html: {0} Pdf: {1}", receiptMailTemplateUrl, receiptPdfTemplateUrl));
            }
        }

        /// <summary>
        /// Gets order as HTML.
        /// </summary>
        /// <param name="templateUrl">The template URL.</param>
        /// <returns>Order as HTML</returns>
        public string GetHtml(string templateUrl)
        {
            if (_siteId == Guid.Empty)
            {
                return Helpers.Templates.ParseTemplate(this.Xml(), templateUrl, SPContext.Current.Site.ID);
            }
            else
            {
                return Helpers.Templates.ParseTemplate(this.Xml(), templateUrl, _siteId);
            }
        }

        /// <summary>
        /// Generates the labels from the codes
        /// </summary>
        /// <param name="templateUrl">Url of the template to use</param>
        /// <returns>
        /// Filename
        /// </returns>
        /// <exception cref="System.ApplicationException"></exception>
        public byte[] GenerateLabels(string templateUrl)
        {
            System.Drawing.Image labelImage = null;

            if (this.ImageSize > 0 && this.LabelImage.Length > 0)
            {
                using (MemoryStream mem = new MemoryStream(this.LabelImage))
                {
                    labelImage = System.Drawing.Image.FromStream(mem);
                }
            }
            else
            {
                string defaultImageFile = string.Empty;
                SPFile file = null;
                try
                {
                    defaultImageFile = ConfigSettings.GetText("DefaultLabelImage");
                    if (_siteId == Guid.Empty)
                    {
                        file = SPContext.Current.Site.RootWeb.GetFile(defaultImageFile);
                    }
                    else
                    {
                        using (SPSite site = new SPSite(_siteId))
                        {
                            file = site.RootWeb.GetFile(defaultImageFile);
                        }
                    }
                    labelImage = System.Drawing.Image.FromStream(file.OpenBinaryStream());
                }
                catch (Exception ex)
                {
                    string error = string.Format("Error loading default label image: {0}. {1}", defaultImageFile, ex.Message);
                    throw new ApplicationException(error, ex);
                }
            }

            List<LabelCreator.ProductInfo> products = new List<LabelCreator.ProductInfo>();
            int numberOfCodes = 0;

            foreach (var subOrder in this.SubOrders)
            {
                foreach (var code in subOrder.Codes)
                {
                    if (code.StatusLimited != Codes.CodeStatusValues.Refunded)
                    {
                        string letterType = "";
                        if (subOrder.Priority == "A")
                        {
                            letterType = "Q";
                        }

                        products.Add(new LabelCreator.ProductInfo()
                        {
                            Code = FormatCode(code.PostCode),
                            ExpiryDate = code.ExpireDate,
                            Price = (decimal)code.Price,
                            OverlayText = letterType
                        });
                        numberOfCodes++;
                    }
                }
            }

            if (numberOfCodes == 0)
            {
                return null;
            }

            LabelTemplate template = null;

            try
            {


                SPFile file = null;
                if (_siteId == Guid.Empty)
                {
                    file = SPContext.Current.Site.RootWeb.GetFile(templateUrl);
                }
                else
                {
                    using (SPSite site = new SPSite(_siteId))
                    {
                        file = site.RootWeb.GetFile(templateUrl);
                    }
                }

                using (TextReader reader = new StreamReader(file.OpenBinaryStream()))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(LabelTemplate));
                    template = (LabelTemplate)serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                string error = string.Format("Error generating labels! Template: {0}. Error:{1}", templateUrl, ex.Message);
                throw new ApplicationException(error, ex);
            }

            string filename = string.Format("Postage_{0}.pdf", this.CaptureId);
            LabelCreator labelCreator = new LabelCreator(template, labelImage);

            return labelCreator.CreateLabelSheets(products.ToArray());
        }
        private string GetProductsJSON()
        {
            List<LabelCreator.ProductInfo> products = new List<LabelCreator.ProductInfo>();
            int numberOfCodes = 0;

            foreach (var subOrder in this.SubOrders)
            {
                foreach (var code in subOrder.Codes)
                {
                    if (code.StatusLimited != Codes.CodeStatusValues.Refunded)
                    {
                        products.Add(new LabelCreator.ProductInfo()
                        {
                            Code = code.PostCode,
                            ExpiryDate = code.ExpireDate,
                            Price = (decimal)code.Price,
                            OverlayText = subOrder.Priority
                        });
                        numberOfCodes++;
                    }
                }
            }
            var json = new JSON();
            return json.GetAsString(products);
        }



        /// <summary>
        /// Formats the code
        /// </summary>
        /// <param name="code">Code to be formatted</param>
        /// <returns>Formatted code</returns>
        private string FormatCode(string code)
        {
            string[] parts = code.Split(new char[] { '-' });

            StringBuilder result = new StringBuilder();

            foreach (string part in parts)
            {
                result.AppendLine((part.Aggregate(string.Empty, (c, i) => c + i + ' ')).Trim());
            }

            return result.ToString();
        }

        #endregion
    }
}
