﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Encapsulates the SubOrders business logic
    /// </summary>
    public class SubOrders
    {

        #region Private Variables

        private PostNord.Portal.DigitalPostage.Bll.Orders _order = null;
        private List<Codes> _codes = null;

        #endregion


        #region Attributes

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        public long OrderId { get; set; }

        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public long ProductId { get; set; }

        public bool beforeVAT {
            get; set;
        }


        /// <summary>
        /// Gets the product name
        /// </summary>
        /// <value>
        /// The product name
        /// </value>
        /// <exception cref="IndexOutOfRangeException">ProductId is zero</exception>
        public string Product
        {
            get
            {
                if (this.ProductId == 0)
                {
                    throw new IndexOutOfRangeException("ProductId is zero");
                }

                string letterType = "Brev";
                if (this.beforeVAT)
                {
                    letterType = "B-brev";
                    if (this.Priority == "A") letterType = "A-brev";

                }
                else
                {
                    letterType = "Brev";
                    if (this.Priority == "A") letterType = "Quickbrev";

                }

                return string.Format("{0} {1} {2}g", DigitalPostage.Bll.Products.GetProduct(this.ProductId), letterType, this.Weight);
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the category for the Product.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        /// <exception cref="System.IndexOutOfRangeException">ProductId is zero</exception>
        /// <exception cref="IndexOutOfRangeException">ProductId is zero</exception>
        public string Category
        {
            get
            {
                if (this.ProductId == 0)
                {
                    throw new IndexOutOfRangeException("ProductId is zero");
                }
                return DigitalPostage.Bll.Products.GetProduct(this.ProductId);
            }
            set { }
        }


        /// <summary>
        /// Gets the destination product info
        /// </summary>
        /// <value>
        /// The product name
        /// </value>
        /// <exception cref="IndexOutOfRangeException">ProductId is zero</exception>
        public string DestinationInfo
        {
            get
            {
                if (this.ProductId == 0)
                {
                    throw new IndexOutOfRangeException("ProductId is zero");
                }
                string letterType = "Brev";
                if (this.Priority == "A") letterType = "Quickbrev";

                return string.Format("{0} {1} {2}g", this.Destination, letterType, this.Weight);
            }
            set { }
        }


        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        public short Weight { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        public string Priority { get; set; }

        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        /// <value>
        /// The destination.
        /// </value>
        public string Destination { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public int Amount { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public double Price { get; set; }

        /// <summary>
        /// Gets or sets the price without VAT.
        /// </summary>
        /// <value>
        /// The price without VAT.
        /// </value>
        public double PriceWithoutVAT { get; set; }

        public double SubOrderSumWithoutVAT
        {
            get
            {
                if (this.Priority == "A")
                    return (double)this.Amount * this.PriceWithoutVAT;
                return (double)this.Amount * this.Price;
            }
            set
            {
            }
        }

        public double SubOrderSumVAT
        {
            get
            {
                if (this.Priority == "A")
                    return (double)this.Amount * (this.Price - this.PriceWithoutVAT);
                return 0.0;
            }
            set
            {
            }
        }


        /// <summary>
        /// Gets or sets the VAT.
        /// </summary>
        /// <value>
        /// The VAT.
        /// </value>
        public int VAT { get; set; }


        /// <summary>
        /// Gets or sets the sub order sum.
        /// </summary>
        /// <value>
        /// The sub order sum.
        /// </value>
        public double SubOrderSum
        {
            get { return this.Amount * this.Price; }
            set { }
        }

        /// <summary>
        /// Gets or sets the sub order credit sum.
        /// </summary>
        /// <value>
        /// The sub order credit sum.
        /// </value>
        public double SubOrderCreditSum
        {
            get
            {
                var refunds = Refunds.GetRefunds(this.OrderId);
                if (refunds.Count == 0)
                {
                    return 0;
                }
                return this.Price * Dal.SubOrders.SubOrderGetCreditCodeCount(this.Id);
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the sub order credit sum.
        /// </summary>
        /// <value>
        /// The sub order credit sum.
        /// </value>
        public int SubOrderCreditAmount
        {
            get
            {
                var refunds = Refunds.GetRefunds(this.OrderId);
                if (refunds.Count == 0)
                {
                    return 0;
                }
                return Dal.SubOrders.SubOrderGetCreditCodeCount(this.Id);
            }
            set { }
        }


        /// <summary>
        /// Gets the sub order saldo.
        /// </summary>
        /// <value>
        /// The sub order saldo.
        /// </value>
        public double SubOrderSaldo
        {
            get
            {
                return this.SubOrderSum - SubOrderCreditSum;
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the sap identifier.
        /// </summary>
        /// <value>
        /// The sap identifier.
        /// </value>
        public int SapId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the Parent Order.
        /// </summary>
        /// <value>
        /// The Parent Order.
        /// </value>
        [XmlIgnore]
        public PostNord.Portal.DigitalPostage.Bll.Orders Order
        {
            get
            {
                if (_order == null)
                {
                    _order = new PostNord.Portal.DigitalPostage.Bll.Orders(this.OrderId);
                }

                return _order;
            }
        }

        /// <summary>
        /// Gets Codes
        /// </summary>
        /// <value>
        /// Codes.
        /// </value>
        [XmlIgnore]
        public List<Codes> Codes
        {
            get
            {
                if (_codes == null)
                {
                    Codes codes = new Codes();
                    _codes = codes.GetCodes(this.Id);
                }

                return _codes;
            }
            // This is done to enable serialization
            set
            {
                _codes = value;
            }
        }

        /// <summary>
        /// Gets or sets the product list identifier.
        /// </summary>
        /// <value>
        /// The product list identifier.
        /// </value>
        public int ProductListId
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SubOrders"/> class.
        /// </summary>
        /// <remarks>Only used for serialization</remarks>
        public SubOrders()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubOrders"/> class.
        /// </summary>
        /// <param name="subOrderId">The sub order identifier.</param>
        public SubOrders(long subOrderId)
        {
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.SubOrders.SubOrdersSelectRow(subOrderId))
            {
                if (row.Read())
                {
                    this.Initialize(row);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubOrders" /> class.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="category">Name of the product.</param>
        /// <param name="weight">The weight.</param>
        /// <param name="priority">The priority.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="price">The price.</param>
        public SubOrders(long orderId, string category, short weight, string priority, string destination, int amount, double price, double PriceWithoutVAT, int VAT)
        {
            this.OrderId = orderId;
            this.Price = price;
            this.Amount = amount;
            this.ProductId = Bll.Products.GetProductId(category);
            this.Weight = weight;
            this.Priority = priority;
            this.Destination = destination;
            this.beforeVAT = false;
            
            if(VAT == null) {
                this.beforeVAT = true;
            }
            
            if (priority != "A")
            {
                this.PriceWithoutVAT = price;
                this.VAT = 100;
            }
            else
            {
                this.PriceWithoutVAT = PriceWithoutVAT;
                this.VAT = VAT;
            }
        }


        /// <summary>
        /// Prevents a default instance of the <see cref="Orders"/> class from being created.
        /// </summary>
        /// <param name="row">DataReader row</param>
        private SubOrders(IDataReader row)
        {
            this.Initialize(row);
        }


        #endregion

        #region Methods

        /// <summary>
        /// Gets all SubOrders for given Order.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns></returns>
        public static List<SubOrders> GetOrderSubOrders(long orderId)
        {
            List<SubOrders> SubOrdersList = new List<SubOrders>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.SubOrders.SubOrdersSelectFromOrderId(orderId))
            {
                while (row.Read())
                {
                    SubOrders mySubOrders = new SubOrders(row);
                    SubOrdersList.Add(mySubOrders);
                }
            }
            return SubOrdersList;
        }

        /// <summary>
        /// Gets Sub Orders.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static SubOrders GetSubOrders(long id)
        {
            SubOrders subOrders = null;
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.SubOrders.SubOrdersSelectRow(id))
            {
                if (row.Read())
                {
                    subOrders = new SubOrders(row);
                }
            }
            return subOrders;
        }

        /// <summary>
        /// Creates the codes.
        /// </summary>
        /// <exception cref="System.IndexOutOfRangeException">SubOrder has not been saved. Unable to generate codes.</exception>
        /// <exception cref="System.ApplicationException">Codes are already created!</exception>
        public void CreateCodes()
        {
            if (this.Id == 0)
            {
                throw new IndexOutOfRangeException("SubOrder has not been saved. Unable to generate codes.");
            }

            if (this.Codes != null && this.Codes.Count > 0)
            {
                return;
            }

            Codes codes = new Bll.Codes();


            this.Codes = codes.CreateCodes(this.Id, this.Order.SalesDate, this.Order.ExpireDate, this.Price, this.Amount, this.PriceWithoutVAT);

        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            if (this.Id == 0)
            {
                this.Id = PostNord.Portal.DigitalPostage.Dal.SubOrders.SubOrdersInsertRow(OrderId, ProductId, Weight, Priority, Destination, Amount, Price, SubOrderSum, SubOrderCreditSum, SubOrderSaldo, ProductListId, SapId, PriceWithoutVAT, VAT);
            }
            else
            {
                PostNord.Portal.DigitalPostage.Dal.SubOrders.SubOrdersUpdateRow(Id, OrderId, ProductId, Weight, Priority, Destination, Amount, Price, SubOrderSum, SubOrderCreditSum, SubOrderSaldo, ProductListId, SapId, PriceWithoutVAT, VAT);
            }

            this.Order.Save();
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        /// <exception cref="System.IndexOutOfRangeException">Id is zero</exception>
        public void Delete()
        {
            if (this.Id == 0)
            {
                throw new IndexOutOfRangeException("Id is zero");
            }
            PostNord.Portal.DigitalPostage.Dal.SubOrders.SubOrdersDeleteRow(this.Id);
            this.Id = 0;
        }

        /// <summary>
        /// Set all disabled codes as unused.
        /// </summary>
        /// <exception cref="System.IndexOutOfRangeException">Id is zero</exception>
        public void SetCodesAsUnused()
        {
            if (this.Id == 0)
            {
                throw new IndexOutOfRangeException("Id is zero");
            }
            PostNord.Portal.DigitalPostage.Dal.SubOrders.SubOrdersChangeCodeStatus(this.Id, (short)Bll.Codes.CodeStatusValues.Disabled, (short)Bll.Codes.CodeStatusValues.Unused);
        }

        /// <summary>
        /// Deletes all codes.
        /// </summary>
        /// <exception cref="System.IndexOutOfRangeException">Id is zero</exception>
        public void DeleteAllCodes()
        {
            if (this.Id == 0)
            {
                throw new IndexOutOfRangeException("Id is zero");
            }
            PostNord.Portal.DigitalPostage.Dal.Codes.DeleteCodesForSubOrder(this.Id);
        }

        /// <summary>
        /// Gets codes.
        /// </summary>
        /// <value>
        /// Codes without PostCode set
        /// </value>
        public List<Codes> GetCodes()
        {
            if (_codes == null)
            {
                _codes = new List<Codes>();
                using (IDataReader row = Dal.Codes.CodesSelectForSubOrderRow(this.Id))
                {
                    while (row.Read())
                    {
                        Codes code = new Codes(row);
                        _codes.Add(code);
                    }
                }
            }
            return _codes;
        }


        #endregion

        #region Private Methods

        /// <summary>
        /// Initializes the specified row.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            this.Id = (long)row["Id"];
            this.OrderId = (long)row["OrderId"];
            this.ProductId = (long)row["ProductId"];
            this.Weight = (short)row["Weight"];
            this.Priority = (string)row["Priority"];
            this.Destination = (string)row["Destination"];
            this.Amount = (int)row["Amount"];
            this.Price = (double)row["Price"];
            this.ProductListId = (row["ProductListId"] != DBNull.Value ? (int)row["ProductListId"] : 0);
            this.SapId = (row["SapId"] != DBNull.Value ? (int)row["SapId"] : 0);
            this.beforeVAT = row["VAT"] == DBNull.Value ? true: false;
            this.PriceWithoutVAT = row["PriceWithoutVAT"] != DBNull.Value ? (double)row["PriceWithoutVAT"] : this.Price;
            this.VAT = row["VAT"] != DBNull.Value ? (int)row["VAT"] : 100;
        }

        #endregion
    }

}
