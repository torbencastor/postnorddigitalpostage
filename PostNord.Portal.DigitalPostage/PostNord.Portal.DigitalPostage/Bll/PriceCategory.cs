﻿using Microsoft.SharePoint;
using PostNord.Portal.DigitalPostage.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Handles Category retrieval from SharePoint Lists
    /// </summary>
    public class PriceCategory
    {
        const string PRICE_CATEGORY_LIST_NAME = "PriceCategories";
        Guid FLD_CATEGORIES_COUNTRIES = new Guid("{50eff1a9-add7-48ad-8a41-09f36d11ce19}");
        Guid FLD_CATEGORIES_A_DELIVERY_TEXT = new Guid("{eaf40198-acfe-4f86-b19d-563fca22877f}");
        Guid FLD_CATEGORIES_B_DELIVERY_TEXT = new Guid("{fc5dad8c-7115-4da3-be78-f3c7478e133d}");

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets a text.
        /// </summary>
        /// <value>
        /// A text.
        /// </value>
        public string AText { get; set; }
        /// <summary>
        /// Gets or sets the b text.
        /// </summary>
        /// <value>
        /// The b text.
        /// </value>
        public string BText { get; set; }

        /// <summary>
        /// Gets the product categories.
        /// </summary>
        /// <param name="web">The web.</param>
        /// <returns>
        /// List of available PriceCategories
        /// </returns>
        public List<PriceCategory> GetProductCategories(SPWeb web)
        {

            List<PriceCategory> categories = new List<PriceCategory>();
            SPList priceCategories = web.Lists[PRICE_CATEGORY_LIST_NAME];
            foreach (SPListItem item in priceCategories.Items)
            {
                string aText = string.Empty;
                string bText = string.Empty;
                string format = "<span>{0}</span><br/><span class=\"redtext\">{1}</span>";
                switch (item.Title)
                {
                    case "Danmark" :
                        aText = string.Format(format, TextSettings.GetText("BUYPOSTAGE_RADIO_A_DENMARK"), TextSettings.GetText("BUYPOSTAGE_RADIO_A_RED_DENMARK"));
                        bText = string.Format(format, TextSettings.GetText("BUYPOSTAGE_RADIO_B_DENMARK"), TextSettings.GetText("BUYPOSTAGE_RADIO_B_RED_DENMARK"));
                        break;
                    case "Europa, Færøerne og Grønland":
                        aText = string.Format(format, TextSettings.GetText("BUYPOSTAGE_RADIO_A_EUROPE"), TextSettings.GetText("BUYPOSTAGE_RADIO_A_RED_EUROPE"));
                        bText = string.Format(format, TextSettings.GetText("BUYPOSTAGE_RADIO_B_EUROPE"), TextSettings.GetText("BUYPOSTAGE_RADIO_B_RED_EUROPE"));
                        break;
                    case "Udland":
                        aText = string.Format(format, TextSettings.GetText("BUYPOSTAGE_RADIO_A_EUROPE"), TextSettings.GetText("BUYPOSTAGE_RADIO_A_RED_EUROPE"));
                        bText = string.Format(format, TextSettings.GetText("BUYPOSTAGE_RADIO_B_EUROPE"), TextSettings.GetText("BUYPOSTAGE_RADIO_B_RED_EUROPE"));
                        break;
                    case "Øvrige udland":
                        aText = string.Format(format, TextSettings.GetText("BUYPOSTAGE_RADIO_A_WORLD"), TextSettings.GetText("BUYPOSTAGE_RADIO_A_RED_WORLD"));
                        bText = string.Format(format, TextSettings.GetText("BUYPOSTAGE_RADIO_B_WORLD"), TextSettings.GetText("BUYPOSTAGE_RADIO_B_RED_WORLD"));
                        break;
                }

                PriceCategory pCat = new PriceCategory
                {
                    Id = item.ID,
                    Title = item.Title,
                    AText = aText,
                    BText = bText
                    //AText = item[FLD_CATEGORIES_A_DELIVERY_TEXT].ToString(),
                    //BText = item[FLD_CATEGORIES_B_DELIVERY_TEXT].ToString()
                };
                categories.Add(pCat);
            }

            return categories;
        }
    }
}
