﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Refund Details
    /// </summary>
    public class RefundDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RefundDetails"/> class.
        /// </summary>
        public RefundDetails()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RefundDetails"/> class.
        /// </summary>
        /// <param name="subOrderId">The sub order identifier.</param>
        /// <param name="amount">The amount.</param>
        /// <exception cref="System.ApplicationException"></exception>
        public RefundDetails(long subOrderId, int amount)
        {
            SubOrders subOrder = new SubOrders(subOrderId);
          //  Refunds refunds = new Refunds(this.)

            if (subOrder.Id == 0)
            {
                throw new ApplicationException(string.Format("SubOrderId not found: {0}", subOrderId));
            }

            this.SubOrderId = subOrder.Id;
            this.Amount = amount;
            this.SubOrderCreditAmount = (int)subOrder.SubOrderCreditAmount;
            this.Product = subOrder.Product;
            this.Destination = subOrder.Destination;
            this.DestinationInfo = subOrder.DestinationInfo;
            this.Price = subOrder.Price;
            this.Priority = subOrder.Priority;
            this.VAT = subOrder.VAT;


            if (subOrder.Priority != "A")
            {
                this.PriceWithoutVAT = subOrder.Price;
                this.VAT = 100;
                this.SubOrderSumWithoutVAT = (double)amount * this.Price;
                this.SubOrderSumVAT = 0.0;
            }
            else
            { // A Price product
                this.PriceWithoutVAT = subOrder.PriceWithoutVAT;
                this.VAT = subOrder.VAT;
                this.SubOrderSumWithoutVAT = (double)amount * this.PriceWithoutVAT;
                this.SubOrderSumVAT = (double)amount * (this.Price - this.PriceWithoutVAT);

            }

            //this.SubOrderSum = subOrder.SubOrderCreditSum;
            this.SubOrderSum = (double)amount * subOrder.Price;
        }

        /// <summary>
        /// Gets or sets the sub order identifier.
        /// </summary>
        /// <value>
        /// The sub order identifier.
        /// </value>
        public long SubOrderId { get; set; }

        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        /// <value>
        /// The destination.
        /// </value>
        public string Destination { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        public string Priority { get; set; }

        /// <summary>
        /// Gets or sets the destination information.
        /// </summary>
        /// <value>
        /// The destination information.
        /// </value>
        public string DestinationInfo { get; set; }

        /// <summary>
        /// Gets or sets the product.
        /// </summary>
        /// <value>
        /// The product.
        /// </value>
        public string Product { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public int Amount { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public int SubOrderCreditAmount { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public double Price { get; set; }


        /// <summary>
        /// Gets or sets the price without VAT.
        /// </summary>
        /// <value>
        /// The price without VAT.
        /// </value>
        public double PriceWithoutVAT { get; set; }

        public double SubOrderSumWithoutVAT
        {
            get;
            set;
        }

        public double SubOrderSumVAT
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the VAT.
        /// </summary>
        /// <value>
        /// The VAT.
        /// </value>
        public int VAT { get; set; }


        /// <summary>
        /// Gets or sets the sub order sum.
        /// </summary>
        /// <value>
        /// The sub order sum.
        /// </value>
        public double SubOrderSum
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sum.
        /// </summary>
        /// <value>
        /// The sum.
        /// </value>
        public double Sum
        {
            get
            {
                return this.Price * this.Amount;
            }
            set { }

        }

    }
}
