﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    public class AppTypes
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
        
        /// <summary>
        /// Gets or sets the platform.
        /// </summary>
        /// <value>
        /// The platform.
        /// </value>
        public string Platform { get; set; }
        
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>
        /// The version.
        /// </value>
        public int Version { get; set; }
        
        /// <summary>
        /// Sets the Oject values.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            this.Id = (int)row["Id"];
            this.Platform = (string)row["Platform"];
            this.Version = (int)row["Version"];
        }

        public AppTypes()
        {

        }

        /// <summary>
        /// S.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public AppTypes(int id)
        {
            IDataReader dr = Dal.AppTypes.SelectRow(id);
            if (dr.Read())
            {
                this.Initialize(dr);
            }
        }

        /// <summary>
        /// Create or load Platform
        /// </summary>
        /// <param name="platform">The platform.</param>
        /// <param name="version">The version.</param>
        /// <returns></returns>
        public AppTypes(string platform, int version)
        {
            IDataReader dr = Dal.AppTypes.SelectRow(platform, version);
            if (dr.Read())
            {
                this.Initialize(dr);
            } 
            else
            {
                this.Id = Dal.AppTypes.InsertRow(platform, version);
            }
        }

        /// <summary>
        /// Gets all app types.
        /// </summary>
        /// <returns></returns>
        public static List<AppTypes> GetAllAppTypes()
        {
            IDataReader dr = Dal.AppTypes.SelectAllRows();
            List<AppTypes> appTypes = new List<AppTypes>();

            while (dr.Read())
            {
                AppTypes appType = new AppTypes();
                appType.Initialize(dr);
                appTypes.Add(appType);
            }

            return appTypes;
        }

    }
}
