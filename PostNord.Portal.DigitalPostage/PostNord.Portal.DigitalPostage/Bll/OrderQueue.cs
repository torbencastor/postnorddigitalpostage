﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Business Layer for OrderQueue
    /// </summary>
    public class OrderQueue
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id { get; set; }
        
        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        public long OrderId { get; set; }
        
        /// <summary>
        /// Gets or sets the refund identifier.
        /// </summary>
        /// <value>
        /// The refund identifier.
        /// </value>
        public long RefundId { get; set; }
        
        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets the action.
        /// </summary>
        /// <value>
        /// Returns the Action: Credit / Debit
        /// </value>
        public string Action
        {
            get
            {
                if (this.RefundId > 0)
                    return "Credit";
                else
                    return "Debit";
            }
        }


        /// <summary>
        /// Adds the specified order identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        static public void Add(long orderId)
        {
            Dal.OrderQueue.OrderQueueAddQueue(orderId, 0, DateTime.Now);
        }

        /// <summary>
        /// Adds the specified order identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="timestamp">The timestamp.</param>
        static public void Add(long orderId, DateTime timestamp)
        {
            Dal.OrderQueue.OrderQueueAddQueue(orderId, 0, timestamp);
        }

        /// <summary>
        /// Adds the specified order identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="refundId">The credit identifier.</param>
        static public void Add(long orderId, long refundId)
        {
            Dal.OrderQueue.OrderQueueAddQueue(orderId, refundId, DateTime.Now);
        }

        /// <summary>
        /// Adds the specified order identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="refundId">The credit identifier.</param>
        /// <param name="timestamp">The timestamp.</param>
        static public void Add(long orderId, long refundId, DateTime timestamp)
        {
            Dal.OrderQueue.OrderQueueAddQueue(orderId, refundId, timestamp);
        }

        /// <summary>
        /// Gets the order queue.
        /// </summary>
        /// <returns></returns>
        public static List<OrderQueue> GetOrderQueue()
        {
            List<OrderQueue> queue = new List<OrderQueue>();

            using (IDataReader row = Dal.OrderQueue.OrderQueueSelectAll())
            {
                while (row.Read())
                {
                    OrderQueue queueEntry = new OrderQueue();
                    queueEntry.Initialize(row);
                    queue.Add(queueEntry);
                }
            }

            return queue;
        }

        /// <summary>
        /// Gets the queue next entry.
        /// </summary>
        /// <returns></returns>
        public static OrderQueue GetQueueNextEntry()
        {
            using (IDataReader row = Dal.OrderQueue.OrderQueueDeQueue())
            {
                if (row.Read())
                {
                    OrderQueue queueEntry = new OrderQueue();
                    queueEntry.Initialize(row);
                    return queueEntry;
                }
            }
            return null;
        }

        /// <summary>
        /// Flushes the queue.
        /// </summary>
        public static void FlushQueue(int queueLength)
        {
            for (int i = 0; i < queueLength; i++)
            {
                OrderQueue queueEntry = Bll.OrderQueue.GetQueueNextEntry();
                // If no orders in queue drop out
                if (queueEntry == null)
                {
                    return;
                }

                Orders order = new Orders(queueEntry.OrderId);

                if (queueEntry.Action == "Debit")
                {
                    try
                    {
                        // Register the sale with CINT
                        order.AddLog("Calling Debit CINT");
                        order.DebitCint();
                    }
                    catch (Exception ex)
                    {
                        // Put Order back in queue and stop flushing
                        OrderQueue.Add(queueEntry.OrderId, queueEntry.Timestamp);
                        order.AddLogError(string.Format("Flush queue error: {0}. Requeue Debit Order", ex.Message));
                        return;
                    }
                }
                else // Credit
                {
                    try
                    {
                        Bll.Refunds refund = new Refunds(queueEntry.RefundId);
                        order.AddLog("Calling Credit CINT");
                        refund.CreditCint();
                    }
                    catch (Exception ex)
                    {
                        // Put Order back in queue and stop flushing
                        OrderQueue.Add(queueEntry.OrderId, queueEntry.RefundId, queueEntry.Timestamp);
                        order.AddLogError(string.Format("Flush queue error: {0}. Requeue Credit Order", ex.Message));
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Initializes the specified row.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            this.Id = (long)row["Id"];
            this.OrderId = (long)row["OrderId"];
            this.RefundId = (long)row["RefundId"];
            this.Timestamp = (DateTime)row["Timestamp"];
        }
    }
}
