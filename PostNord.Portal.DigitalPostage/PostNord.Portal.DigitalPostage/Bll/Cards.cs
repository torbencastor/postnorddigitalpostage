﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Business Layer for Cards table
    /// </summary>
    public class Cards
    {
        private string seed = "1qazxsw23edc";
        private string _encryptedKey = string.Empty;

        private readonly string SaltKey = "S@LT&KEY";
        private readonly string VIKey = "@1B2c3D4e5F6g7H8";
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        public long OrderId { get; set;}

        /// <summary>
        /// Gets or sets the created property.
        /// </summary>
        /// <value>
        /// The created.
        /// </value>
        public DateTime Created { get; set; }
        
        /// <summary>
        /// Gets or sets the last used property.
        /// </summary>
        /// <value>
        /// The last used.
        /// </value>
        public DateTime LastUsed { get; set; }
        
        /// <summary>
        /// Gets or sets the email property.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the encrypted key property.
        /// </summary>
        /// <value>
        /// The encrypted key.
        /// </value>
        protected string EncryptedKey {
            get
            {
                if (this.PanHash == string.Empty)
                {
                    return string.Empty;
                }
                return Encrypt(this.PanHash);
            }
        }

        /// <summary>
        /// Gets or sets the pan hash. Value is encrypted in DB
        /// </summary>
        /// <value>
        /// The pan hash.
        /// </value>
        public string PanHash { get; set; }

        /// <summary>
        /// Sets the Oject values.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            this.Id = (Guid)row["Id"];
            this.OrderId = (long)row["OrderId"];
            this.Email = (string)row["Email"];
            this.Created = (DateTime)row["Created"];
            this.LastUsed = (DateTime)row["LastUsed"];
            this.PanHash = Decrypt((string)row["EncryptedKey"]);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Cards" /> class.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="email">The email.</param>
        /// <param name="panHash">The pan hash.</param>
        public Cards(long orderId, string email, string panHash)
        {
            this.Id = Guid.Empty;
            this.OrderId = orderId;
            this.Email = email;
            this.PanHash = panHash;
        }

        /// <summary>
        /// Load saved Card
        /// </summary>
        /// <param name="id">The identifier.</param>
        public Cards(Guid id)
        {
            IDataReader dr = Dal.Cards.CardsSelectRow(id);

            if (dr.Read())
            {
                Initialize(dr);
            }
            else
            {
                this.Id = Guid.Empty;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Cards"/> class.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        public Cards(long orderId)
        {
            IDataReader dr = Dal.Cards.CardsSelectRow(orderId);

            if (dr.Read())
            {
                Initialize(dr);
            }
            else
            {
                this.Id = Guid.Empty;
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            if (this.Id == Guid.Empty)
            {
                this.Id = Guid.NewGuid();
                this.Created = this.LastUsed = DateTime.Now;
                Dal.Cards.CardsInsertRow(this.Id, this.OrderId, this.Email, this.EncryptedKey, this.Created, this.LastUsed);
            }
            else
            {
                this.LastUsed = DateTime.Now;
                Dal.Cards.CardsUpdateRow(this.Id, this.Email, this.EncryptedKey, this.LastUsed);
            }
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Dal.Cards.CardsDeleteRow(this.Id);
        }


        private string Encrypt(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes = new Rfc2898DeriveBytes(this.Id.ToString(), Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }

        private string Decrypt(string encryptedText)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
            byte[] keyBytes = new Rfc2898DeriveBytes(this.Id.ToString(), Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }


    }
}
