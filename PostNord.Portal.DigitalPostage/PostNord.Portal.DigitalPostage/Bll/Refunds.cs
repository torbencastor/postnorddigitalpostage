﻿using iTextSharp.text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using PostNord.Portal.DigitalPostage.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Xml.Serialization;

namespace PostNord.Portal.DigitalPostage.Bll
{


    /// <summary>
    /// Used for refunding in SAP
    /// </summary>
    public class RefundStruct
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RefundStruct"/> class.
        /// </summary>
        /// <param name="sapId">The sap identifier.</param>
        /// <param name="price">The price.</param>
        public RefundStruct(int sapId, double price)
        {
            this.SapId = sapId;
            this.Price = price;
        }

        /// <summary>
        /// Gets or sets the sap identifier.
        /// </summary>
        /// <value>
        /// The sap identifier.
        /// </value>
        public int SapId
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public double Price
        {
            get;
            set;
        }

        /// <summary>
        /// Adds the price to Price
        /// </summary>
        /// <param name="price">The price.</param>
        public void AddPrice(double price)
        {
            this.Price += price;
        }
    }


    /// <summary>
    /// Handles Refund Operations
    /// </summary>
    public class Refunds
    {


        private Bll.Orders _order = null;
        private Guid siteId = Guid.Empty;

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id { get; set; }
        
        /// <summary>
        /// Gets or sets the order nr.
        /// </summary>
        /// <value>
        /// The order nr.
        /// </value>
        public long OrderNr { get; set; }
        
        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email {
            get
            {
                Orders order = new Orders(this.OrderNr);
                return order.Email;
            }
            set { }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        public string Address { get; set; }
        
        /// <summary>
        /// Gets or sets the post number.
        /// </summary>
        /// <value>
        /// The post number.
        /// </value>
        public int PostNumber { get; set; }
        
        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }
        
        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the credit sum.
        /// </summary>
        /// <value>
        /// The credit sum.
        /// </value>
        public double CreditSum { get; set; }

        /// <summary>
        /// Gets or sets the employee.
        /// </summary>
        /// <value>
        /// The employee.
        /// </value>
        public string Employee { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Refund has been sent to CINT.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [sent to cint]; otherwise, <c>false</c>.
        /// </value>
        public bool SentToCint { get; set; }

        /// <summary>
        /// Gets or sets the site identifier.
        /// </summary>
        /// <value>
        /// The site identifier.
        /// </value>
        public Guid SiteId
        {
            get { return siteId; }
            set { siteId = value; }
        }


        /// <summary>
        /// Gets the refund URL.
        /// Used in ViewOrder.aspx
        /// </summary>
        /// <value>
        /// The refund URL.
        /// </value>
        public string RefundUrl
        {
            get
            {
                return string.Format("{0}?orderid={1}&refundId={2}", HttpContext.Current.Request.Url.AbsolutePath, this.Order.Id, this.Id);
            }
        }

        /// <summary>
        /// Gets or sets the refunded detail list.
        /// </summary>
        /// <value>
        /// The refunded detail list.
        /// </value>
        public List<RefundDetails> RefundDetails
        {
            get {
                List<RefundDetails> refundedDetailList = new List<RefundDetails>();
                List<Codes> refundedCodes = Codes.GetRefunds(this.Id);

                List<SubOrders> OrderSubOrders = SubOrders.GetOrderSubOrders(this.OrderNr);


                foreach (SubOrders subOrder in OrderSubOrders)
                {

                    int codeAmount = 0;
                    foreach (Codes code in refundedCodes)
                    {
                        if (code.SubOrderId == subOrder.Id)
                        {
                            codeAmount++;
                        }
                    }


                    if (codeAmount > 0)
                    {
                        RefundDetails refundDetail = refundedDetailList.FirstOrDefault(refund => refund.SubOrderId == subOrder.Id);
                        if (refundDetail == null)
                        {
                            refundDetail = new RefundDetails(subOrder.Id, codeAmount);
                            refundedDetailList.Add(refundDetail);
                        }
                    }


                }                
                
                return refundedDetailList;
            }
            set { }
        }

        /// <summary>
        /// Gets the order related to this instance
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        /// <exception cref="System.IndexOutOfRangeException">OrdreNr is 0. Unable to load Order</exception>
        public Bll.Orders Order
        {
            get
            {
                if (this.OrderNr == 0)
                {
                    throw new IndexOutOfRangeException("OrdreNr is 0. Unable to load Order");
                }

                if (_order == null)
                {
                    _order = new Orders(this.OrderNr);
                }

                return _order;
            }
        }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Refunds"/> class.
        /// </summary>
        public Refunds()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Refunds"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public Refunds(long id)
        {
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.Refunds.RefundsSelectRow(id))
            {
                if (row.Read())
                {
                    this.Initialize(row);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Refunds" /> class.
        /// </summary>
        /// <param name="dr">The Datareader Row.</param>
        public Refunds(IDataReader dr)
        {
            this.Initialize(dr);
        }

        /// <summary>
        /// Refund added to CINT.
        /// </summary>
        private void CintUpdated()
        {
            Dal.Refunds.UpdateCintStatus(this.Id, 1);
            this.SentToCint = true;
        }

        /// <summary>
        /// Converts the order into XML
        /// </summary>
        /// <returns>XML string</returns>
        public string Xml()
        {
            string xml = string.Empty;

            using (MemoryStream mem = new MemoryStream())
            {
                Type[] extra = {typeof(RefundDetails)};

                XmlSerializer serializer = new XmlSerializer(typeof(Refunds), extra);
                serializer.Serialize(mem, this);

                using (StreamWriter writer = new StreamWriter(mem))
                {
                    writer.Flush();
                    mem.Position = 0;
                    using (StreamReader reader = new StreamReader(mem))
                    {
                        xml = reader.ReadToEnd();
                    }
                }
            }

            return xml;
        }

        #endregion

        #region public Methods

        /// <summary>
        /// Gets order as HTML.
        /// </summary>
        /// <param name="templateUrl">The template URL.</param>
        /// <returns>Order as HTML</returns>
        public string GetHtml(string templateUrl)
        {
            if (this.SiteId != Guid.Empty)
            {
                return Helpers.Templates.ParseTemplate(this.Xml(), templateUrl, this.SiteId);
            }
            else
            {
                return Helpers.Templates.ParseTemplate(this.Xml(), templateUrl);
            }
        }


        /// <summary>
        /// Sends the order to mail address in order.
        /// </summary>
        public void SendMail()
        {
            this.SendMail(this.Email);
        }

        /// <summary>
        /// Sends the mail to given email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <exception cref="System.ApplicationException"></exception>
        public void SendMail(string email)
        {
            string creditNotaPdfTemplateUrl = ConfigSettings.GetText("CreditNotaPDFTemplate", "/Style%20Library/DigitalPostage/templates/creditNotePDF.xslt");
            string creditNotaMailTemplateUrl = ConfigSettings.GetText("CreditNotaHTMLTemplate", "/Style%20Library/DigitalPostage/templates/CreditNotePDF.xslt");
            string filename = ConfigSettings.GetText("CreditNotaPdfFilename", "Kreditnota.pdf");
            string mailTitle = ConfigSettings.GetText("CreditNotaMailTitle", "Kreditnota");

            string bodyHtml = this.GetHtml(creditNotaMailTemplateUrl);
            string creditNotaPdfHtml = this.GetHtml(creditNotaPdfTemplateUrl);

            SmtpClient mailClient = new SmtpClient(ConfigSettings.GetText("SMTPServer"));
            MailMessage message = new MailMessage(ConfigSettings.GetText("ReplyMailAddress"), email);
            message.IsBodyHtml = true;
            message.Body = bodyHtml;
            message.Subject = mailTitle;

            byte[] pdfReceipt = Helpers.Pdf.CreatePdf(creditNotaPdfHtml, PageSize.A4, 10f, 10f, 10f, 0f);

            using (MemoryStream pdfStream = new MemoryStream(pdfReceipt))
            {
                Attachment att = new Attachment(pdfStream, filename, "application/pdf");
                message.Attachments.Add(att);
                try
                {
                    mailClient.Send(message);
                }
                catch (Exception ex)
                {
                    this._order.AddLogError(string.Format("Error sending Refund mail: {0}", ex.Message));
                }
            }
        }

        /// <summary>
        /// Gets all refunds.
        /// </summary>
        /// <returns>
        /// All Refunds from Refund Table
        /// </returns>
        public static List<Refunds> GetRefunds()
        {
            List<Refunds> RefundsList = new List<Refunds>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.Refunds.GetRefunds())
            {
                while (row.Read())
                {
                    Refunds myRefunds = new Refunds(row);
                    RefundsList.Add(myRefunds);
                }
            }
            return RefundsList;
        }

        /// <summary>
        /// Gets all refunds for Order Id
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>
        /// All Refunds from Refund Table for order
        /// </returns>
        public static List<Refunds> GetRefunds(long orderId)
        {
            List<Refunds> RefundsList = new List<Refunds>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.Refunds.GetRefunds(orderId))
            {
                while (row.Read())
                {
                    Refunds myRefunds = new Refunds(row);
                    RefundsList.Add(myRefunds);
                }
            }
            return RefundsList;
        }

        /// <summary>
        /// Credits the cint.
        /// </summary>
        public void CreditCint()
        {
            List<Codes> codes = Bll.Codes.GetRefunds(this.Id);
            List<SubOrders> subOrderWithRefunds = new List<SubOrders>();

            var refunds = new System.Collections.Hashtable();

            // Calculate Refund Sum and keep track of SubOrders affected
            foreach (Bll.Codes code in codes)
            {
                // Keep track of subOrders affected
                SubOrders subOrder = subOrderWithRefunds.FirstOrDefault(s => s.Id == code.SubOrderId);
                if (subOrder == null)
                {
                    subOrderWithRefunds.Add(code.SubOrder);
                }

                if (refunds[code.SubOrderId] != null)
                {
                    refunds[code.SubOrderId] = (int)refunds[code.SubOrderId] + 1;
                }
                else
                {
                    refunds.Add(code.SubOrderId, 1);
                }
            }

            this.Order.CreditCint(refunds);
            this.CintUpdated();
        }

        /// <summary>
        /// Gets the refunds.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public static List<Refunds> GetRefunds(DateTime startDate, DateTime endDate)
        {
            List<Refunds> RefundsList = new List<Refunds>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.Refunds.GetRefunds(startDate, endDate))
            {
                while (row.Read())
                {
                    Refunds myRefunds = new Refunds(row);
                    RefundsList.Add(myRefunds);
                }
            }
            return RefundsList;
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            if (string.IsNullOrEmpty(this.Employee) && SPContext.Current != null)
            {
                this.Employee = SPContext.Current.Web.CurrentUser.LoginName;
            }

            if (this.Id == 0)
            {
                this.Id = PostNord.Portal.DigitalPostage.Dal.Refunds.RefundsInsertRow(this.OrderNr, this.Timestamp, this.Name, this.Address, this.PostNumber, this.City, this.Phone, this.Message, this.CreditSum, this.Employee);
            }
            else
            {
                PostNord.Portal.DigitalPostage.Dal.Refunds.RefundsUpdateRow(this.Id, this.OrderNr, this.Timestamp, this.Name, this.Address, this.PostNumber, this.City, this.Phone, this.Message, this.CreditSum, this.Employee);
            }
        }

        /// <summary>
        /// Refunds the codes.
        /// </summary>
        /// <param name="codes">The codes.</param>
        public void RefundCodes(List<Bll.Codes> codes)
        {
            List<SubOrders> subOrderWithRefunds = new List<SubOrders>();

            var refunds = new System.Collections.Hashtable();

            // Calculate Refund Sum and keep track of SubOrders affected
            foreach (Bll.Codes code in codes)
            {
                // Keep track of subOrders affected
                SubOrders subOrder = subOrderWithRefunds.FirstOrDefault(s => s.Id == code.SubOrderId);
                if (subOrder == null)
                {
                    subOrderWithRefunds.Add(code.SubOrder);
                }

                if (refunds[code.SubOrderId] != null)
                {
                    refunds[code.SubOrderId] = (int) refunds[code.SubOrderId] + 1;
                }
                else
                {
                    refunds.Add(code.SubOrderId, 1);
                }

                this.CreditSum += code.Price;
            }

            if (this.CreditSum <= 0)
            {
                throw new ApplicationException(string.Format("Credit sum should be larger than 0. Creditsum: {0}", this.CreditSum));
            }

            // refund Amount from Nets
            this.Order.RefundAmount(this.CreditSum, refunds);

            string currentUser = "Unknown";
            if (SPContext.Current != null && SPContext.Current.Web.CurrentUser != null)
            {
                currentUser = SPContext.Current.Web.CurrentUser.LoginName;
            }
            this.Order.AddLog(string.Format(new System.Globalization.CultureInfo("da-DK"), "{0} codes refunded to a total value of {1:C} By user: {2}", codes.Count, this.CreditSum, currentUser));
            this.Save();
            this.Order.AddLog(string.Format("Refund: {0} saved.", this.Id));

            // Money is refunded now Update codes to reflect new status
            foreach (Bll.Codes code in codes)
            {
                code.Refund(this.Id, DateTime.Now);
            }

            // Update all SubOrders affected to persist Credit data
            foreach (Bll.SubOrders subOrder in subOrderWithRefunds)
	        {
                subOrder.Save();		 
	        } 

            this.Order.AddLog(string.Format("{0} SubOrders updated", subOrderWithRefunds.Count));
            this.Order.Save();
            this.Order.AddLog(string.Format("Order: {0} saved", Order.Id));

            // If this is the old version we dont inform CINT about anything
            if (this.Order.IsOldVersion)
            {
                this.Order.AddLog("Order (version 1) not sent to CINT ");
            }
            else
            {
                Bll.OrderQueue.Add(this.OrderNr, this.Id);

                int queueLength = 5;
                int.TryParse(ConfigSettings.GetText("OrderQueueLength", "5"), out queueLength);
                Bll.OrderQueue.FlushQueue(queueLength);
            }
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            if (this.Id > 0)
            {
                PostNord.Portal.DigitalPostage.Dal.Refunds.RefundsDeleteRow(Id);
            }
            this.Id = 0;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Initializes the specified row.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            this.Id = (long)row["Id"];
            this.OrderNr = (long)row["OrderNr"];
            this.Timestamp = (DateTime)row["Timestamp"];
            this.Name = (string)row["Name"];
            this.Address = (string)row["Address"];
            this.PostNumber = (int)row["PostNumber"];
            this.City = (string)row["City"];
            this.Phone = (string)row["Phone"];
            this.Message = (string)row["Message"];
            this.CreditSum = (double)row["CreditSum"];
            this.Employee =  (row["Employee"] != null ? (string) row["Employee"] : "");

            this.SentToCint = false;
            if (row["SentToCint"] != DBNull.Value)
            {
                short sentToCintVal = (short)row["SentToCint"];
                this.SentToCint = sentToCintVal != 0;
            }
        }

        #endregion
    }
}
