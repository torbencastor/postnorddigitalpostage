﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{

    //For production the certificate DBGROOT_Prod_B64.crt should be installed in root certificates of Windows. 
    //See http://technet.microsoft.com/en-us/library/cc754841.aspx on how to install certificate.
    public class MobilePaySignatureValidator
    {
        /*
        public bool ValidateSignature(string signature, string orderId, string merchantId, string transactionId)
        {
            //encode expected signed data
            var data = Encoding.UTF8.GetBytes(string.Format("{0}#{1}#{2}#", orderId, merchantId, transactionId));

            //data = Encoding.UTF8.GetBytes(string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}#{9}#{10}", merchantId, orderId, productName, productPrice, receiptMessage, sdkVersion, signatureVersion, successUrl, failureUrl, cancelUrl));
            var signatureBytes = Convert.FromBase64String(signature);
            return ValidateSignature(signatureBytes, data);
        }
        */

        public bool ValidateSignature(string signature, string orderId, string merchantId, string transactionId, string productPrice)
        {
            //encode expected signed data
            var data = Encoding.UTF8.GetBytes(string.Format("{0}#{1}#{2}#{3}#{4}#{5}#", orderId, merchantId, transactionId, productPrice, "DKK", "DK"));
            var signatureBytes = Convert.FromBase64String(signature);
            return ValidateSignature(signatureBytes, data);
        }
         

        private bool ValidateSignature(byte[] signature, byte[] data)
        {
            try
            {

                //Decode signature
                var signedCms = new System.Security.Cryptography.Pkcs.SignedCms();
                signedCms.Decode(signature);

                //1. Validate the signature
                signedCms.CheckSignature(true); //throws exception on invalid

                //2. Validate the signer certificate chain to the trusted root with no revocation check
                foreach (var signer in signedCms.SignerInfos)
                {
                    var certificate = signer.Certificate;
                    var chain = new System.Security.Cryptography.X509Certificates.X509Chain();
                    chain.ChainPolicy.ExtraStore.AddRange(signedCms.Certificates); //Add intermediate certificates
                    chain.ChainPolicy.RevocationMode = System.Security.Cryptography.X509Certificates.X509RevocationMode.NoCheck;
                    var valid = chain.Build(certificate);
                    if (!valid)
                    {
                        return false;
                    }
                }

                //3. Validate the signed data matches the expected data
                var signedBytes = signedCms.ContentInfo.Content;
                var validData = Enumerable.SequenceEqual(signedBytes, data);

                return validData;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
