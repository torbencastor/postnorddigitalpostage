﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{

    /// <summary>
    /// Implements the business logic for the handling of CodeIds
    /// </summary>
    /// <remarks>
    /// A CodeId is an unique code generated per Date. To Obtain a CodeId use static method GenerateCode()
    /// </remarks>
    public class CodeIds
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id { get; set; }
        /// <summary>
        /// Gets or sets the code identifier.
        /// </summary>
        /// <value>
        /// The code identifier.
        /// </value>
        public int CodeId { get; set; }
        /// <summary>
        /// Gets or sets the code date.
        /// </summary>
        /// <value>
        /// The code date.
        /// </value>
        public DateTime CodeDate { get; set; }


        /// <summary>
        /// Gets or sets the delay time. Used for Unit test only!
        /// </summary>
        /// <value>
        /// The delay time.
        /// </value>
        public int DelayTime
        {
            get;
            set;
        }

        #region Public Methods

        /// <summary>
        /// Generates the code.
        /// </summary>
        /// <returns>
        /// Unique code for current Date
        /// </returns>
        public int GenerateCode()
        {
            if (this.DelayTime > 0)
            {
                return PostNord.Portal.DigitalPostage.Dal.CodeIds.GenerateCode(DateTime.Now, this.DelayTime);
            }
            else
            {
                return PostNord.Portal.DigitalPostage.Dal.CodeIds.GenerateCode(DateTime.Now);
            }
        }

        /// <summary>
        /// Generates the code.
        /// </summary>
        /// <returns>
        /// Unique code for current Date
        /// </returns>
        public void GenerateCodeTest()
        {
            if (this.DelayTime > 0)
            {
                PostNord.Portal.DigitalPostage.Dal.CodeIds.GenerateCode(DateTime.Now, this.DelayTime);
            }
            else
            {
                PostNord.Portal.DigitalPostage.Dal.CodeIds.GenerateCode(DateTime.Now);
            }
        }
        
        /// <summary>
        /// Codes the ids select all.
        /// </summary>
        /// <returns>
        /// List containing all CodeIds
        /// </returns>
        public static List<CodeIds> CodeIdsSelectAll()
        {
            List<CodeIds> CodeIdsList = new List<CodeIds>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.CodeIds.CodeIdsSelectAll())
            {
                while (row.Read())
                {
                    CodeIds myCodeIds = new CodeIds();
                    myCodeIds.Initialize(row);
                    CodeIdsList.Add(myCodeIds);
                }
            }
            
            return CodeIdsList;
        }

        /// <summary>
        /// Codes the ids select date.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>All CodeIds Generated in the selected Period</returns>
        public static List<CodeIds> CodeIdsSelectDate(DateTime fromDate, DateTime toDate)
        {
            List<CodeIds> CodeIdsList = new List<CodeIds>();
            using (IDataReader row = PostNord.Portal.DigitalPostage.Dal.CodeIds.CodeIdsSelectDateRange(fromDate, toDate))
            {
                while (row.Read())
                {
                    CodeIds myCodeIds = new CodeIds();
                    myCodeIds.Initialize(row);
                    CodeIdsList.Add(myCodeIds);
                }
            }
            return CodeIdsList;
        }

        #endregion


        #region Private Methods
        
        /// <summary>
        /// Sets the Object values.
        /// </summary>
        /// <param name="row">The row.</param>
        private void Initialize(IDataReader row)
        {
            Id = (long)row["Id"];
            CodeId = (int)row["CodeId"];
            CodeDate = (DateTime)row["CodeDate"];
            CodeDate = CodeDate.Date;
        }

        #endregion
    }
}
