﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Handles price retrieval from SharePoint Lists
    /// </summary>
    public class Prices
    {
        const string PRODUCT_LIST_NAME = "Products";

        Guid FLD_PRODUCTS_CATEGORY = new Guid("{652740e3-5e5e-454a-9f33-cd2f317ae4c3}");
        Guid FLD_PRODUCTS_WEIGHT = new Guid("{94e18ae7-89db-45a4-ac20-33c339dd1268}");
        Guid FLD_PRODUCTS_APRICE = new Guid("{50e14eea-b947-450e-959a-104baf5c9fac}");
        Guid FLD_PRODUCTS_BPRICE = new Guid("{824ac6d4-f8aa-4455-aedc-83bcca907fab}");
        Guid FLD_PRODUCTS_FROMDATE = new Guid("{bf5a2dea-6ca3-4820-afb7-5ab286b302db}");
        Guid FLD_PRODUCTS_TODATE = new Guid("{dde0fd49-270a-4b23-8474-bc9fbe116deb}");
        Guid FLD_PRODUCTS_AVAT = new Guid("{5fd4a3ae-ece1-462b-8b64-956ee1135fda}");

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set;  }
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        public string Category { get; set; }
        /// <summary>
        /// Gets or sets the category identifier.
        /// </summary>
        /// <value>
        /// The category identifier.
        /// </value>
        public int CategoryId { get; set; }
        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        public double Weight { get; set; }
        /// <summary>
        /// Gets or sets a price.
        /// </summary>
        /// <value>
        /// A price.
        /// </value>
        public double APriceWithoutVAT { get; set; }
        /// <summary>
        /// Gets or sets a price without VAT.
        /// </summary>
        /// <value>
        /// A price without VAT
        /// </value>
        public double APrice { get; set; }
        /// <summary>
        /// Gets or sets the b price.
        /// </summary>
        /// <value>
        /// The b price.
        /// </value>
        public double BPrice { get; set; }
        /// <summary>
        /// Gets or sets the A VAT (Value Added Tax).
        /// Index value 100 = no tax
        /// Index value 125 = 25% tax
        /// </summary>
        /// <value>
        /// The AVAT.
        /// </value>
        public double AVAT { get; set; }

        /// <summary>
        /// Gets the product categories.
        /// </summary>
        /// <param name="web">The web.</param>
        /// <returns>
        /// List of available PriceCategories
        /// </returns>
        public List<Prices> GetPrices(SPWeb web)
        {
            List<Prices> prices = new List<Prices>();
            SPList priceCategories = web.Lists[PRODUCT_LIST_NAME];

            SPQuery query = new SPQuery();
            query.Query = "<OrderBy><FieldRef Name='PriceCategory' /><FieldRef Name='Weight' /></OrderBy><Where><Leq><FieldRef Name='FromDate' /><Value Type='DateTime' IncludeTimeValue='FALSE'><Today /></Value></Leq></Where>";
            SPListItemCollection prodItems = priceCategories.GetItems(query);

            foreach (SPListItem item in prodItems)
            {
                if(item["ToDate"] != null)
                {
                    DateTime toDate = (DateTime)item["ToDate"];
                    if (toDate < DateTime.Today)
                    {
                        continue;
                    }
                }
                SPFieldLookupValue catLookUp = new SPFieldLookupValue(item[FLD_PRODUCTS_CATEGORY].ToString());

                Prices price = new Prices
                {
                    Id = item.ID,
                    Title = item.Title,
                    Category = catLookUp.LookupValue,
                    CategoryId = catLookUp.LookupId,
                    Weight = (double)item[FLD_PRODUCTS_WEIGHT],
                    APrice = Math.Round(((double)item[FLD_PRODUCTS_APRICE] * (double)item[FLD_PRODUCTS_AVAT]) / 100,2),
                    APriceWithoutVAT = Math.Round((double)item[FLD_PRODUCTS_APRICE],2),
                    BPrice = (double)item[FLD_PRODUCTS_BPRICE],
                    AVAT = (double)item[FLD_PRODUCTS_AVAT]
                };
                prices.Add(price);
            }

            return prices;
        }

        /// <summary>
        /// Gets the sap identifier.
        /// </summary>
        /// <param name="web">The web.</param>
        /// <param name="productId">The product identifier.</param>
        /// <param name="priority">The priority (A/B)</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public static int GetSapId(SPWeb web, int productId, string priority)
        {
            SPList products = web.Lists[PRODUCT_LIST_NAME];
            SPListItem priceItem = products.GetItemById(productId);
            Guid sapFldId = Guid.Empty;
            switch (priority)
            {
                case "A":
                    sapFldId = products.Fields.GetFieldByInternalName("SapIdA").Id;
                    break;
                case "B":
                    sapFldId = products.Fields.GetFieldByInternalName("SapIdB").Id;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("Priority: '{0}' was out of range", priority));
            }

            if (priceItem[sapFldId] != null)
            {
                return (int)priceItem[sapFldId];
            }

            return 0;
        }
    }
}
