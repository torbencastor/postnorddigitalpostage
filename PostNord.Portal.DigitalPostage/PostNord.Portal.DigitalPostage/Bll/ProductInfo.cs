﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Bll
{
    /// <summary>
    /// Business Layer for Product lists
    /// </summary>
    public class ProductInfo
    {

        public const string PRODUCT_LIST_NAME = "Products";
        public const string PRICE_CATEGORY_LIST_NAME = "PriceCategories";
        public const string COUNTRIES_LIST_NAME = "Countries";

        static Guid FLD_CATEGORIES_CONTRIES = new Guid("{50eff1a9-add7-48ad-8a41-09f36d11ce19}");
        static Guid FLD_CATEGORIES_A_DELIVERY_TEXT = new Guid("{eaf40198-acfe-4f86-b19d-563fca22877f}");
        static Guid FLD_CATEGORIES_B_DELIVERY_TEXT = new Guid("{fc5dad8c-7115-4da3-be78-f3c7478e133d}");

        static Guid FLD_PRODUCTS_CATEGORY = new Guid("{652740e3-5e5e-454a-9f33-cd2f317ae4c3}"); // old 652740e3-5e5e-454a-9f33-cd2f317ae4c3
        static Guid FLD_PRODUCTS_WEIGHT = new Guid("{94e18ae7-89db-45a4-ac20-33c339dd1268}");
        static Guid FLD_PRODUCTS_APRICE = new Guid("{50e14eea-b947-450e-959a-104baf5c9fac}");
        static Guid FLD_PRODUCTS_BPRICE = new Guid("{824ac6d4-f8aa-4455-aedc-83bcca907fab}");
        static Guid FLD_PRODUCTS_FROMDATE = new Guid("{bf5a2dea-6ca3-4820-afb7-5ab286b302db}");
        static Guid FLD_PRODUCTS_TODATE = new Guid("{dde0fd49-270a-4b23-8474-bc9fbe116deb}");
        static Guid FLD_PRODUCTS_AVAT = new Guid("{5fd4a3ae-ece1-462b-8b64-956ee1135fda}");


        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title  { get; set; }

        /// <summary>
        /// Gets the weight.
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        public int Weight { get; set; }

        /// <summary>
        /// Gets a price.
        /// </summary>
        /// <value>
        /// A price.
        /// </value>
        public double APrice { get; set; }

        /// <summary>
        /// Gets the b price.
        /// </summary>
        /// <value>
        /// The b price.
        /// </value>
        public double BPrice { get; set; }

        /// <summary>
        /// Gets a delivery text.
        /// </summary>
        /// <value>
        /// A delivery text.
        /// </value>
        public string ADeliveryText { get; set; }

        /// <summary>
        /// Gets the b delivery text.
        /// </summary>
        /// <value>
        /// The b delivery text.
        /// </value>
        public string BDeliveryText { get; set; }

        /// <summary>
        /// Gets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country { get; set; }

        /// <summary>
        /// Gets the price category.
        /// </summary>
        /// <value>
        /// The price category.
        /// </value>
        public string PriceCategory { get; internal set; }

        /// <summary>
        /// Gets the price category identifier.
        /// </summary>
        /// <value>
        /// The price category identifier.
        /// </value>
        public int PriceCategoryId { get; internal set; }

        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public string ProductNameA { get; internal set; }

        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public string ProductNameB { get; internal set; }

        /// <summary>
        /// Gets or sets from date.
        /// </summary>
        /// <value>
        /// From date.
        /// </value>
        public DateTime FromDate { get; set; }

        /// <summary>
        /// Gets or sets to date.
        /// </summary>
        /// <value>
        /// To date.
        /// </value>
        public DateTime ToDate { get; set; }


        /// <summary>
        /// Gets or sets the sap identifier for A Porto.
        /// </summary>
        /// <value>
        /// The sap identifier.
        /// </value>
        public int SapIdA { get; set; }

        /// <summary>
        /// Gets or sets the sap identifier for B Porto.
        /// </summary>
        /// <value>
        /// The sap identifier.
        /// </value>
        public int SapIdB { get; set; }


        /// <summary>
        /// Gets or sets the VAT for A Price
        /// </summary>
        /// <value>
        /// The VAT for A Price
        /// </value>
        public double VAT { get; set; }



        /// <summary>
        /// Gets the product list.
        /// </summary>
        /// <param name="web">The web.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Exception</exception>
        static public List<ProductInfo> GetProductList(SPWeb web)
        {
            List<ProductInfo> prodInfos = new List<ProductInfo>();
            try
            {
                SPList products = web.Lists[PRODUCT_LIST_NAME];
                SPList priceCategories = web.Lists[PRICE_CATEGORY_LIST_NAME];
                SPList countries = web.Lists[COUNTRIES_LIST_NAME];

                // Get all products newer than FromDate
                string today = SPUtility.CreateISO8601DateTimeFromSystemDateTime(DateTime.Today.Date);
                SPQuery query = new SPQuery();
                query.Query = string.Format("<Where><Leq><FieldRef Name='FromDate' /><Value Type='DateTime' IncludeTimeValue='FALSE'><Today /></Value></Leq></Where>", today);
                SPListItemCollection prodItems = products.GetItems(query);

                foreach (SPListItem item in prodItems)
                {
                    ProductInfo pInfo = new ProductInfo();

                    // If ToDate set and before today skip it
                    DateTime? toDate = (DateTime?)item[FLD_PRODUCTS_TODATE];
                    if (toDate.HasValue && toDate.Value.Date < DateTime.Today.Date)
                    {
                        continue;
                    }

                    // Save values from item (for clarity)
                    DateTime fromDate = (DateTime)item[FLD_PRODUCTS_FROMDATE];
                    SPFieldLookupValue category = new SPFieldLookupValue(item[FLD_PRODUCTS_CATEGORY] as String);
                    double weight = (double)item[FLD_PRODUCTS_WEIGHT];
                    double aPrice = (double)item[FLD_PRODUCTS_APRICE];
                    double bPrice = (double)item[FLD_PRODUCTS_BPRICE];
                    double VAT = (double)item[FLD_PRODUCTS_AVAT];

                    Guid sapAFieldId = products.Fields.GetFieldByInternalName("SapIdA").Id;
                    Guid sapBFieldId = products.Fields.GetFieldByInternalName("SapIdB").Id;

                    if (item[sapAFieldId] != null)
                    {
                        pInfo.SapIdA = int.Parse(item[sapAFieldId].ToString());
                    }

                    if (item[sapBFieldId] != null)
                    {
                        pInfo.SapIdB = int.Parse(item[sapBFieldId].ToString());
                    }

                    pInfo.Id = item.ID;
                    pInfo.Title = item.Title;
                    pInfo.Weight = (int)weight;
                    pInfo.APrice = aPrice;
                    pInfo.BPrice = bPrice;
                    pInfo.VAT = VAT;


                    prodInfos.Add(pInfo);

                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error loading Products info. {0}", ex.Message));
            }
            return prodInfos;
        }



        /// <summary>
        /// Gets the product infos.
        /// </summary>
        /// <param name="web">The web.</param>
        /// <returns>List of ProductInfos</returns>
        /// <exception cref="System.ApplicationException">Error loading Product info.</exception>
        /// <remarks>
        /// Todo Add Caching
        /// </remarks>
        static public List<ProductInfo> GetProductInfos(SPWeb web)
        {
            List<ProductInfo> prodInfos = new List<ProductInfo>();
            try
            {
                SPList products = web.Lists[PRODUCT_LIST_NAME];
                SPList priceCategories = web.Lists[PRICE_CATEGORY_LIST_NAME];
                SPList countries = web.Lists[COUNTRIES_LIST_NAME];

                // Load all Categories
                SPListItemCollection priceCatItems = priceCategories.Items;

                // Get all products newer than FromDate
                string today = SPUtility.CreateISO8601DateTimeFromSystemDateTime(DateTime.Today.Date);
                SPQuery query = new SPQuery();
                query.Query = string.Format("<Where><Leq><FieldRef Name='FromDate' /><Value Type='DateTime' IncludeTimeValue='FALSE'><Today /></Value></Leq></Where>", today);
                SPListItemCollection prodItems = products.GetItems(query);

                foreach (SPListItem item in prodItems)
                {
                    // If ToDate set and before today skip it
                    DateTime? toDate = (DateTime?)item[FLD_PRODUCTS_TODATE];
                    if (toDate.HasValue && toDate.Value.Date < DateTime.Today.Date)
                    {
                        continue;
                    }

                    // Save values from item (for clarity)
                    DateTime fromDate = (DateTime)item[FLD_PRODUCTS_FROMDATE];
                    SPFieldLookupValue category = new SPFieldLookupValue(item[FLD_PRODUCTS_CATEGORY] as String);
                    double weight = (double)item[FLD_PRODUCTS_WEIGHT];
                    double aPrice = (double)item[FLD_PRODUCTS_APRICE];
                    double bPrice = (double)item[FLD_PRODUCTS_BPRICE];
                    double VAT = (double)item[FLD_PRODUCTS_AVAT];

                    // Get Category from category items
                    List<SPListItem> items = (from l in priceCatItems.OfType<SPListItem>() where (int)l.ID == category.LookupId select l).ToList<SPListItem>();
                    SPListItem catListItem = items.FirstOrDefault();
                    if (catListItem == null)
                    {
                        continue;
                    }
                    
                    // Save values from item (for clarity)
                    string aDeliveryText = (string)catListItem[FLD_CATEGORIES_A_DELIVERY_TEXT];
                    string bDeliveryText = (string)catListItem[FLD_CATEGORIES_B_DELIVERY_TEXT];

                    SPFieldLookupValueCollection countriesList = (SPFieldLookupValueCollection)catListItem[FLD_CATEGORIES_CONTRIES];

                    // Loop countries and add to list
                    foreach (SPFieldLookupValue countryLookup in countriesList)
                    {
                        ProductInfo pi = new ProductInfo();
                        pi.Id = item.ID;
                        pi.Title = item.Title;
                        pi.Weight = (int)weight;
                        pi.APrice = aPrice;
                        pi.BPrice = bPrice;
                        pi.ADeliveryText = aDeliveryText;
                        pi.BDeliveryText = bDeliveryText;
                        pi.Country = countryLookup.LookupValue;
                        pi.PriceCategory = category.LookupValue;
                        pi.PriceCategoryId = category.LookupId;
                        pi.VAT = VAT;

                        pi.ProductNameA = string.Format(new System.Globalization.CultureInfo("da-DK"), "{0} {1} A - {2:C}", pi.PriceCategory, weight, aPrice);
                        pi.ProductNameB = string.Format(new System.Globalization.CultureInfo("da-DK"), "{0} {1} B - {2:C}", pi.PriceCategory, weight, bPrice);

                        prodInfos.Add(pi);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error loading Product info. {0}", ex.Message));
            }

            return prodInfos;
        }


        /// <summary>
        /// Gets the product infos for at given category
        /// </summary>
        /// <param name="web">The web.</param>
        /// <param name="category">The category.</param>
        /// <returns>
        /// List of ProductInfos
        /// </returns>
        /// <exception cref="System.ApplicationException">Error loading Product infoCat.</exception>
        /// <remarks>
        /// Todo Add Caching
        /// </remarks>
        static public List<ProductInfo> GetProductInfosCat(SPWeb web, int category)
        {
            List<ProductInfo> prodInfos = new List<ProductInfo>();
            try
            {
                SPList products = web.Lists[PRODUCT_LIST_NAME];

                // Get all products newer than FromDate
                string today = SPUtility.CreateISO8601DateTimeFromSystemDateTime(DateTime.Today.Date);
                SPQuery query = new SPQuery();
                query.Query = string.Format("<OrderBy><FieldRef Name='PriceCategory' /><FieldRef Name='Weight' /></OrderBy><Where><Leq><FieldRef Name='FromDate' /><Value Type='DateTime' IncludeTimeValue='FALSE'><Today /></Value></Leq></Where>", today);
                SPListItemCollection prodItems = products.GetItems(query);

                Guid sapAFieldId = products.Fields.GetFieldByInternalName("SapIdA").Id;
                Guid sapBFieldId = products.Fields.GetFieldByInternalName("SapIdB").Id;

                foreach (SPListItem item in prodItems)
                {
                    // If ToDate set and before today skip it
                    DateTime? toDate = (DateTime?)item[FLD_PRODUCTS_TODATE];
                    if (toDate.HasValue && toDate.Value.Date < DateTime.Today.Date)
                    {
                        continue;
                    }

                    // Save values from item (for clarity)
                    DateTime fromDate = (DateTime)item[FLD_PRODUCTS_FROMDATE];
                    SPFieldLookupValue categoryValue = new SPFieldLookupValue(item[FLD_PRODUCTS_CATEGORY] as String);
                    if (categoryValue.LookupId != category)
                    {
                        continue;
                    }

                    double weight = (double)item[FLD_PRODUCTS_WEIGHT];
                    double aPrice = (double)item[FLD_PRODUCTS_APRICE];
                    double bPrice = (double)item[FLD_PRODUCTS_BPRICE];
                    double VAT = (double)item[FLD_PRODUCTS_AVAT];

                    ProductInfo pi = new ProductInfo();
                    pi.FromDate = (DateTime)item[FLD_PRODUCTS_FROMDATE];
                    if (toDate.HasValue)
                    {
                        pi.ToDate = toDate.Value;
                    }


                    pi.Id = item.ID;
                    pi.Title = item.Title;
                    pi.Weight = (int)weight;
                    pi.APrice = aPrice;
                    pi.BPrice = bPrice;
                    pi.PriceCategory = categoryValue.LookupValue;
                    pi.PriceCategoryId = categoryValue.LookupId;
                    pi.VAT = VAT;

                    // Added version 2.0.8
                    int sapIdA = 0;
                    int sapIdB = 0;
                    double sapIdVal = 0;
                    if (item[sapAFieldId] != null)
                    {
                        sapIdVal = (double)item[sapAFieldId];
                        sapIdA = (int)sapIdVal;
                    }
                    pi.SapIdA = sapIdA;

                    if (item[sapBFieldId] != null)
                    {
                        sapIdVal = (double)item[sapBFieldId];
                        sapIdB = (int)sapIdVal;
                    }
                    pi.SapIdB = sapIdB;

                    prodInfos.Add(pi);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error loading Product infoCat. {0}", ex.Message));
            }

            return prodInfos;
        }
    
    
    }
}
