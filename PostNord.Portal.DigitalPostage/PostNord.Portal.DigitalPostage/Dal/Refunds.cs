﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.ApplicationBlocks.Data;
    using System.Data.SqlClient;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// Handles CRUD operations for Refunds table.
    /// </summary>
    public class Refunds
    {
        /// <summary>
        /// Gets refunds from specified period.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>
        /// Returns all refunds from given period
        /// </returns>
        public static SqlDataReader GetRefunds(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "RefundsSelectDate", fromDate, toDate);
        }

        /// <summary>
        /// Gets all refunds.
        /// </summary>
        /// <returns>
        /// Returns all refunds from Refunds Table
        /// </returns>
        public static SqlDataReader GetRefunds()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "RefundsSelectAll");
        }

        /// <summary>
        /// Gets all refunds for a given Order Id.
        /// </summary>
        /// <param name="OrderId">The order identifier.</param>
        /// <returns>
        /// Returns all refunds from Refunds Table
        /// </returns>
        public static SqlDataReader GetRefunds(long OrderId)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "RefundsSelectOrderId", OrderId);
        }

        /// <summary>
        /// Get Refunds for specified Id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Returns selected row
        /// </returns>
        public static SqlDataReader RefundsSelectRow(long id)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "RefundsSelectRow", id);
        }

        /// <summary>
        /// Add Refunds Row.
        /// </summary>
        /// <param name="orderNr">The order nr.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="name">The name.</param>
        /// <param name="address">The address.</param>
        /// <param name="postNumber">The post number.</param>
        /// <param name="city">The city.</param>
        /// <param name="phone">The phone.</param>
        /// <param name="message">The message.</param>
        /// <param name="creditSum">The credit sum.</param>
        /// <param name="employee">The employee.</param>
        /// <returns>
        /// Generated identifier
        /// </returns>
        public static long RefundsInsertRow(long orderNr, DateTime timestamp, string name, string address, int postNumber, string city, string phone, string message, double creditSum, string employee)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "RefundsInsertRow", orderNr, timestamp, name, address, postNumber, city, phone, message, creditSum, employee);
            return long.Parse(res.ToString());
        }

        /// <summary>
        /// Update row in Refunds table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="name">The name.</param>
        /// <param name="address">The address.</param>
        /// <param name="postNumber">The post number.</param>
        /// <param name="city">The city.</param>
        /// <param name="phone">The phone.</param>
        /// <param name="message">The message.</param>
        /// <param name="creditSum">The credit sum.</param>
        /// <param name="employee">The employee.</param>
        public static void RefundsUpdateRow(long id, long orderNr, DateTime timestamp, string name, string address, int postNumber, string city, string phone, string message, double creditSum, string employee)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "RefundsUpdateRow", id, orderNr, timestamp, name, address, postNumber, city, phone, message, creditSum, employee);
        }

        /// <summary>
        /// Delete Refunds row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public static void RefundsDeleteRow(long id)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "RefundsDeleteRow", id);
        }

        /// <summary>
        /// Update CINT status
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="sentToCint">Sent to CINT status. 1 means transferred to CINT, 0 means it is not</param>
        public static void UpdateCintStatus(long id, short sentToCint)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "RefundsSetCintStatus", id, sentToCint);
        }
    }
}
