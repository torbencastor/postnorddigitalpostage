﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// Handles CRUD operations for Codes table. 
    /// </summary>
    public class Codes
    {

        /// <summary>
        /// Select all rows from the Codes Table.
        /// </summary>
        /// <returns>
        /// Rows selected
        /// </returns>
        private static SqlDataReader CodesSelectAll()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CodesSelectAllRows");
        }

        /// <summary>
        /// Select row from Codes table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Selected row
        /// </returns>
        public static SqlDataReader CodesSelectRow(long id)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CodesSelectRow", id);
        }

        /// <summary>
        /// Select rows from Codes table for given SubOrder.
        /// </summary>
        /// <param name="suborderId">The suborder identifier.</param>
        /// <returns>
        /// Rows selected
        /// </returns>
        public static SqlDataReader CodesSelectForSubOrderRow(long suborderId)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CodesSelectForSubOrder", suborderId);
        }

        /// <summary>
        /// Select rows from Codes table for given Refund.
        /// </summary>
        /// <param name="refundId">The refund identifier.</param>
        /// <returns>
        /// Rows selected
        /// </returns>
        public static SqlDataReader CodesSelectForRefund(long refundId)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CodesSelectForRefund", refundId);
        }

        /// <summary>
        /// Select code from code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns>
        /// Selected row
        /// </returns>
        public static SqlDataReader CodesSelectCode(string code)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CodesSelectCode", code);
        }

        /// <summary>
        /// Insert row in Codes table.
        /// </summary>
        /// <param name="postCode">The post code.</param>
        /// <param name="subOrderId">The sub order identifier.</param>
        /// <param name="codeId">The code identifier.</param>
        /// <param name="status">The status.</param>
        /// <param name="price">The price.</param>
        /// <param name="issueDate">The issue date.</param>
        /// <param name="expireDate">The expire date.</param>
        /// <returns>
        /// Generated identifier
        /// </returns>
        public static long CodesInsertRow(string postCode, long subOrderId, int codeId, short status, double price, DateTime issueDate, DateTime expireDate)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "CodesInsertRow", postCode, subOrderId, codeId, status, price, issueDate, expireDate);
            return long.Parse(res.ToString());
        }

        /// <summary>
        /// Update used date.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">The status.</param>
        /// <param name="usedDate">The used date.</param>
        public static void CodesUpdateUsedDate(long id, short status, DateTime usedDate)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "CodesUpdateUsedDate", id, status, usedDate);
        }

        /// <summary>
        /// Set Code as refunded.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">The status.</param>
        /// <param name="refundedDate">The refunded date.</param>
        /// <param name="refundId">The refund identifier.</param>
        public static void CodesUpdateRefund(long id, short status, DateTime refundedDate, long refundId)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "CodesUpdateRefund", id, status, refundedDate, refundId);
        }

        /// <summary>
        /// Delete row from Codes table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public static void CodesDeleteRow(long id)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "CodesDeleteRow", id);
        }

        /// <summary>
        /// Delete all Codes for given SubOrder.
        /// </summary>
        /// <param name="subOrderId">The sub order identifier.</param>
        public static void DeleteCodesForSubOrder(long subOrderId)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "CodesDeleteRowsForSubOrder", subOrderId);
        }
    
    }
}
