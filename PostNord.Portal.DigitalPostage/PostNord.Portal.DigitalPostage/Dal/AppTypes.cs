﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// AppTypes table
    /// </summary>
    public class AppTypes
    {

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        private static string ConnectionString
        {
            get
            {
                return Helpers.HelperClass.GetConnectionString();
            }
        }

        /// <summary>
        /// Select AppType from Platform and Version
        /// </summary>
        /// <param name="platform">The platform.</param>
        /// <param name="version">The version.</param>
        /// <returns></returns>
        public static SqlDataReader SelectRow(string platform, int version)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "AppTypesSelectRow", platform, version);
        }

        /// <summary>
        /// Select AppType from Id
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
        public static SqlDataReader SelectRow(int Id)
        {
            return SqlHelper.ExecuteReader(ConnectionString, "AppTypesSelectById", Id);
        }

        /// <summary>
        /// Selects all rows.
        /// </summary>
        /// <returns>All Rows from AppType sorted by Platform ascending</returns>
        public static SqlDataReader SelectAllRows()
        {
            return SqlHelper.ExecuteReader(ConnectionString, "AppTypesSelectAll");
        }

        /// <summary>
        /// Insert AppType row.
        /// </summary>
        /// <param name="Platform">The platform.</param>
        /// <param name="Version">The version.</param>
        /// <returns>Id of Row inserted</returns>
        public static int InsertRow(string Platform, int Version)
        {
            object o = SqlHelper.ExecuteScalar(ConnectionString, "AppTypesInsertRow", Platform, Version);

            if (o != null)
            {
                return int.Parse(o.ToString());
            }
            return -1;
        }


    }
}
