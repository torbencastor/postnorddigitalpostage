﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// Handles CRUD operations for Products table.
    /// </summary>
    public class Products
    {

        /// <summary>
        /// Select all Products.
        /// </summary>
        /// <returns>
        /// All Rows from table
        /// </returns>
        public static SqlDataReader ProductsSelectAll()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ProductsSelectAll");
        }

        /// <summary>
        /// Select one Product row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Selected row
        /// </returns>
        public static SqlDataReader ProductsSelectRow(long id)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ProductsSelectRow", id);
        }

        /// <summary>
        /// Insert Product row.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>Generated identifier</returns>
        public static long ProductsInsertRow(string name)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "ProductsInsertRow", name);
            return long.Parse(res.ToString());
        }

        /// <summary>
        /// Update product row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        public static void ProductsUpdateRow(long id, string name)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "ProductsUpdateRow", id, name);
        }

        /// <summary>
        /// Delete Product row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public static void ProductsDeleteRow(long id)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "ProductsDeleteRow", id);
        }
    }
}
