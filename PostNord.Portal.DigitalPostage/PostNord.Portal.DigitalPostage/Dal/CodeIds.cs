﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// DAL for table CodeIds
    /// </summary>
    
    public class CodeIds
    {

        #region Public Methods

        /// <summary>
        /// Generates new code.
        /// </summary>
        /// <param name="codeDate">The code date.</param>
        /// <returns>
        /// Unique Code for Day
        /// </returns>
        /// <exception cref="System.ApplicationException">DAL Error Creating Code</exception>
        public static int GenerateCode(DateTime codeDate)
        {
            int codeId = 0;
            long id = 0;

            using (SqlConnection scon = new SqlConnection(Helpers.HelperClass.GetConnectionString()))
            {
                scon.Open();
                
                using (SqlTransaction sqlTrans = scon.BeginTransaction())
                {
                    try
                    {
                        using (IDataReader row = CodeIdsSelectDate(codeDate))
                        {
                            if (row.Read())
                            {
                                id = (long)row["Id"];
                                codeId = (int)row["CodeId"];
                            }

                            if (codeId == 0)
                            {
                                codeId = 1;
                                CodeIdsInsertRow(sqlTrans, codeId, codeDate);
                            }
                            else
                            {
                                codeId = CodeIdsUpdateRow(sqlTrans, id);
                            }

                            sqlTrans.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        sqlTrans.Rollback();
                        throw new ApplicationException("DAL Error Creating Code ", ex);
                    }
                    finally
                    {
                        scon.Close();
                    }
                }
            }
            return codeId;
        }

        /// <summary>
        /// Generates new code.
        /// </summary>
        /// <param name="codeDate">The code date.</param>
        /// <param name="delay">The delay in miliseconds</param>
        /// <returns>
        /// Unique Code for Day
        /// </returns>
        /// <exception cref="System.ApplicationException">DAL Error Creating Code</exception>
        public static int GenerateCode(DateTime codeDate, int delay)
        {
            int codeId = 0;
            long id = 0;

            using (SqlConnection scon = new SqlConnection(Helpers.HelperClass.GetConnectionString()))
            {
                if (scon.State != ConnectionState.Open)
                {
                    scon.Open();
                }

                using (SqlTransaction sqlTrans = scon.BeginTransaction())
                {
                    try
                    {
                        using (IDataReader row = CodeIdsSelectDate(codeDate))
                        {
                            if (row.Read())
                            {
                                id = (long)row["Id"];
                                codeId = (int)row["CodeId"];
                            }

                            if (delay > 0)
                            {
                                DateTime toTime = DateTime.Now.AddMilliseconds(delay);
                                while (toTime > DateTime.Now)
                                {

                                }
                            }

                            if (codeId == 0)
                            {
                                codeId = 1;
                                CodeIdsInsertRow(sqlTrans, codeId, codeDate);
                            }
                            else
                            {
                                codeId = CodeIdsUpdateRow(sqlTrans, id);
                            }

                            sqlTrans.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        sqlTrans.Rollback();
                        throw new ApplicationException("DAL Error Creating Code ", ex);
                    }
                    finally
                    {
                        scon.Close();
                    }
                }
            }
            return codeId;
        }

        /// <summary>
        /// Select all code ids
        /// </summary>
        /// <returns>
        /// Returns all CodeId rows
        /// </returns>
        public static SqlDataReader CodeIdsSelectAll()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CodeIdsSelectAllRows");
        }

        /// <summary>
        /// Select a date range of CodeIds.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>
        /// Returns selected CodeId rows
        /// </returns>
        public static SqlDataReader CodeIdsSelectDateRange(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CodeIdsSelectDateRange", fromDate, toDate);
        }

        /// <summary>
        /// Select CodeId
        /// </summary>
        /// <param name="codeId">The code identifier.</param>
        /// <param name="codeDate">The code date.</param>
        /// <returns>
        /// Returns selected CodeId row
        /// </returns>
        public static SqlDataReader CodeIdsSelectRow(int codeId, DateTime codeDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CodeIdsSelectRow", codeId, codeDate.Date);
        }

        /// <summary>
        /// Select CodeId by date.
        /// </summary>
        /// <param name="codeDate">The code date.</param>
        /// <returns>
        /// Returns selected CodeId row
        /// </returns>
        public static SqlDataReader CodeIdsSelectDate(DateTime codeDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CodeIdsSelectDate", codeDate.Date);
        }

        /// <summary>
        /// Delete CodeId row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public static void CodeIdsDeleteRow(int id)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "CodeIdsDeleteRow", id);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Select Code Id from Date.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="codeDate">The code date.</param>
        /// <returns></returns>
        private static SqlDataReader CodeIdsSelectDate(SqlTransaction transaction, DateTime codeDate)
        {
            return SqlHelper.ExecuteReader(transaction, "CodeIdsSelectDate", codeDate.Date);
        }

        /// <summary>
        /// Insert new Code Id.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="codeId">The code identifier.</param>
        /// <param name="codeDate">The code date.</param>
        private static void CodeIdsInsertRow(SqlTransaction transaction, int codeId, DateTime codeDate)
        {
            SqlHelper.ExecuteNonQuery(transaction, "CodeIdsInsertRow", codeId, codeDate.Date);
        }

        /// <summary>
        /// Update Code Id.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        private static int CodeIdsUpdateRow(SqlTransaction transaction, long id)
        {
            object res = SqlHelper.ExecuteScalar(transaction, "CodeIdsUpdateRow", id);
            return int.Parse(res.ToString());
        }
        #endregion

    }
}
