﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// OrderQueue Data Access Layer
    /// </summary>
    public class OrderQueue
    {
        /// <summary>
        /// select all orders from queue
        /// </summary>
        /// <returns></returns>
        public static SqlDataReader OrderQueueSelectAll()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrderQueueSelectAll");
        }

        /// <summary>
        /// Add order to queue
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="refundId">The refund identifier.</param>
        /// <param name="timestamp">The timestamp.</param>
        public static void OrderQueueAddQueue(long orderId, long refundId, DateTime timestamp)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "OrderQueueAddQueue", orderId, refundId, timestamp);
        }

        /// <summary>
        /// Dequeue order
        /// </summary>
        /// <returns>
        /// Order Id last inserted
        /// </returns>
        public static SqlDataReader OrderQueueDeQueue()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrderQueueDeQueue");
        }

    }
}
