﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// Handles CRUD operations for ValidationResults table.
    /// </summary>
    public class ValidationResults
    {
        /// <summary>
        /// Gets the report of refunded orders without vat
        /// </summary>
        /// <param name="fromDate">Report start date</param>
        /// <param name="toDate">Report end date</param>
        /// <returns></returns>
        internal static SqlDataReader GetReportCodeValidationAndOrder(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewValidationResultsAndOrder", fromDate, toDate);
        }

        /// <summary>
        /// Select all rows from the ValidationResults Table.
        /// </summary>
        /// <returns>
        /// Selected rows.
        /// </returns>
        public static SqlDataReader ValidationResultsSelectAll()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ValidationResultsSelectAllRows");
        }

        /// <summary>
        /// Select rows from the ValidationResults table matching date interval.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>
        /// Selected rows.
        /// </returns>
        public static SqlDataReader ValidationResultsSelectDate(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ValidationResultsSelectDate", fromDate, toDate);
        }

        /// <summary>
        /// Select rows from the ValidationResults table matching date interval and CodeId
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="codeId">The code identifier.</param>
        /// <returns>
        /// Selected rows.
        /// </returns>
        public static SqlDataReader ValidationResultsSelectCode(DateTime fromDate, DateTime toDate, long codeId)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ValidationResultsSelectDateAndCode", fromDate, toDate, codeId);
        }

        /// <summary>
        /// Select row matching identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Selected row.</returns>
        public static SqlDataReader ValidationResultsSelectRow(long id)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ValidationResultsSelectRow", id);
        }

        /// <summary>
        /// Insert row into ValidationResults table.
        /// </summary>
        /// <param name="codeId">The code identifier.</param>
        /// <param name="codeText">The code text.</param>
        /// <param name="employee">The employee.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="errorCode">The error code.</param>
        /// <returns>Generated identifier</returns>
        public static long ValidationResultsInsertRow(long codeId, string codeText, string employee, DateTime timestamp, int errorCode)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "ValidationResultsInsertRow", codeId, codeText, employee, timestamp, errorCode);
            return long.Parse(res.ToString());
        }

        /// <summary>
        /// Update row in ValidationResults table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="codeId">The code identifier.</param>
        /// <param name="codeText">The code text.</param>
        /// <param name="employee">The employee.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="errorCode">The error code.</param>
        public static void ValidationResultsUpdateRow(long id, long codeId, string codeText, string employee, DateTime timestamp, int errorCode)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "ValidationResultsUpdateRow", id, codeId, codeText, employee, timestamp, errorCode);
        }

        /// <summary>
        /// Delete row from ValidationResults Table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public static void ValidationResultsDeleteRow(long id)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "ValidationResultsDeleteRow", id);
        }

        /// <summary>
        /// Gets the report Validation results
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Validation data</returns>
        public static SqlDataReader GetReportValidationResults(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewValidationResults", fromDate, toDate);
        }

        /// <summary>
        /// Gets the report Validation results summary
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Validation data summary</returns>
        public static SqlDataReader GetReportValidationResultSums(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewValidationResultSums", fromDate, toDate);
        }

    }
}