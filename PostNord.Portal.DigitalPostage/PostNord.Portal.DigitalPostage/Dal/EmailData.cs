﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Dal
{
    public class EmailData
    {
        public int InsertRow(Entity.EmailData emailData)
        {
            int rowsAffected = 0;
            using (var sqlConnection = new SqlConnection(Helpers.HelperClass.GetConnectionString()))
            {
                try
                {
                    sqlConnection.Open();
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "EmailToSendInsertRow";
                        command.Parameters.AddWithValue("@CreationDatetime", emailData.CreationDatetime);
                        command.Parameters.AddWithValue("@MessageBody", emailData.MessageBody);
                        command.Parameters.AddWithValue("@RecieptBody", emailData.RecieptBody);
                        command.Parameters.AddWithValue("@OrderId", emailData.OrderId);
                        command.Parameters.AddWithValue("@RecipientEmail", emailData.RecipientEmail);
                        command.Parameters.AddWithValue("@Status", emailData.Status);
                        command.Parameters.AddWithValue("@PdfLabels", emailData.PdfLabels);
                        command.Parameters.AddWithValue("@LabelImage", emailData.LabelImage);
                        command.Parameters.AddWithValue("@ImageSize", emailData.ImageSize);
                        rowsAffected = command.ExecuteNonQuery();

                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            return rowsAffected;
        }
    }
}
