﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// Handles CRUD operations for Orders table. 
    /// </summary>
    public class Orders
    {

        /// <summary>
        /// Select all Orders.
        /// </summary>
        /// <returns></returns>
        public static SqlDataReader OrdersSelectAll()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrdersSelectAllRows");
        }

        /// <summary>
        /// Select Orders row.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>
        /// Selected order
        /// </returns>
        public static SqlDataReader OrdersSelectRow(long Id)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrdersSelectRow", Id);
        }

        /// <summary>
        /// Gets orders from specified period.
        /// </summary>
        /// <param name="captureId">The capture identifier.</param>
        /// <returns>
        /// Selected orders
        /// </returns>
        public static SqlDataReader GetOrders(string captureId)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrdersSelectCaptureId", captureId);
        }

        /// <summary>
        /// Gets orders from specified period.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>
        /// Selected orders
        /// </returns>
        public static SqlDataReader GetOrders(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrdersSelectDate", fromDate, toDate);
        }

        /// <summary>
        /// Gets order status Info from specified period.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>
        /// Order Status information
        /// </returns>
        public static DataSet GetOrderStatus(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "OrderStatus", fromDate, toDate);
        }

        /// <summary>
        /// Gets order status Info from specified period with specified status.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="status">The status.</param>
        /// <returns>
        /// Order Status information
        /// </returns>
        public static DataSet GetOrderStatusDay(DateTime fromDate, DateTime toDate, int status)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "OrderStatusTimeline", fromDate, toDate, status);
        }

        /// <summary>
        /// Gets order status Info from specified period with specified status.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="status">The status.</param>
        /// <returns>
        /// Order Status information
        /// </returns>
        public static DataSet GetOrderStatusDays(DateTime fromDate, DateTime toDate, int status)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "OrderStatusTimelineDate", fromDate, toDate, status);
        }

        /// <summary>
        /// Gets Id of last order with given status
        /// </summary>
        /// <param name="status">status value</param>
        /// <returns>Id of last order found</returns>
        public static long GetLastOrder(int status, int paymentType)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "GetLastOrder", status, paymentType);

            if (res == null)
            {
                return -1;
            }

            long orderId = -1;
            long.TryParse(res.ToString(), out orderId);

            return orderId;
        }

        /// <summary>
        /// Gets the reports grouped on sales date.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Report Order sold in the selected Date range on a daily basis</returns>
        public static SqlDataReader GetReportGroupedOnSalesDate(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewReportGroupedOnSalesDate", fromDate, toDate);
        }

        /// <summary>
        /// Gets the reports grouped on product
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Report Order sold in the selected Date grouped per product</returns>
        public static DataSet GetReportSalePerProductWithVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "ReportingViewReportSalePerProduct", fromDate, toDate);
        }

        /// <summary>
        /// Gets the reports grouped on product
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Report Order sold in the selected Date grouped per product</returns>
        public static DataSet GetReportSalePerProductWithoutVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "ReportingViewReportSalePerProductWithoutVat", fromDate, toDate);
        }

        /// <summary>
        /// Gets the reimbursement report grouped on product
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Report Order sold in the selected Date grouped per product</returns>
        public static DataSet GetReportReimbursedSalePerProductWithVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "ReportingViewReportSalePerProductReimbursement", fromDate, toDate);
        }
        /// <summary>
        /// Gets the reimbursement report grouped on product without vat
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Report Order sold in the selected Date grouped per product</returns>
        public static DataSet GetReportReimbursedSalePerProductWithoutVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "ReportingViewReportSalePerProductReimbursementWithoutVat", fromDate, toDate);
        }


        /// <summary>
        /// Gets the reports grouped on sales date.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="reportType">Type of the report.</param>
        /// <returns>
        /// Report Order sold in the selected Date range grouped by customer
        /// </returns>
        /// <exception cref="System.ApplicationException"></exception>
        public static SqlDataReader GetReportGroupedOnCustomers(DateTime fromDate, DateTime toDate, string reportType)
        {            
            switch (reportType)
            {
                case "CustomerGroups":
                     return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewByCustomers", fromDate, toDate);
                case "CustomerGroupsWithoutVat":
                   return  SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewByCustomersWithoutVat", fromDate, toDate) ;
                case "CustomerGroupsOnlinePorto":
                    return  SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewByCustomersOnlinePortoWithVat", fromDate, toDate);
                case "CustomerGroupsOnlinePortoWithoutVat":
                    return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewByCustomersOnlinePortoWithoutVat", fromDate, toDate);
                case "CustomerGroupsMobilPorto":
                    return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewByCustomersMobilePortoWithVat", fromDate, toDate);
                case "CustomerGroupsMobilPortoWithoutVat":
                    return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewByCustomersMobilePortoWithoutVat", fromDate, toDate);
            }

            throw new ApplicationException(string.Format("Unknown ReportType: {0}", reportType));

        }

        /// <summary>
        /// Gets the reports grouped on sales date as a Dataset.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Report Order sold in the selected Date range grouped by customer</returns>
        public static DataSet GetReportGroupedOnCustomersDataSetWithVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "ReportingViewByCustomers", fromDate, toDate);
        }
        /// <summary>
        /// Gets the reports grouped on sales date as a Dataset.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Report Order sold in the selected Date range grouped by customer</returns>
        public static DataSet GetReportGroupedOnCustomersDataSetWithoutVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "ReportingViewByCustomersWithoutVat", fromDate, toDate);
        }

        /// <summary>
        /// Get Order status from Database for orders in date range
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>DataSet containing 2 tables with count per status</returns>
        public static DataSet GetStatus(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "OrdersGetStatus", fromDate, toDate);
        }

        /// <summary>
        /// Delete orders in Date range where status is not Paid
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        public static void DeleteUnUsedOrders(DateTime fromDate, DateTime toDate)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "OrdersDeleteUnPaid", fromDate, toDate);
        }

        /// <summary>
        /// Gets the reports grouped on sales date with vat.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Report Order sold in the selected Date range grouped by customer</returns>
        public static DataSet GetReportViewReimbursementsPerCustomerWithVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "ReportingViewReimbursementsPerCustomers", fromDate, toDate);
        }
        /// <summary>
        /// Gets the reports grouped on sales date with vat.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Report Order sold in the selected Date range grouped by customer</returns>
        public static DataSet GetReportViewReimbursementsPerCustomerWithoutVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "ReportingViewReimbursementsPerCustomersWithoutVat", fromDate, toDate);
        }

        /// <summary>
        /// Gets the reimbursement report.
        /// </summary>
        /// <param name="fromDate">Reimbursement from date.</param>
        /// <param name="toDate">Reimbursement to date.</param>
        /// <returns>Report reimbursements in the selected Date range grouped by customer</returns>
        public static SqlDataReader GetReportReimbursementsWithVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewReimbursement", fromDate, toDate);
        }
  
        /// <summary>
        /// Gets the reimbursement report.
        /// </summary>
        /// <param name="fromDate">Reimbursement from date.</param>
        /// <param name="toDate">Reimbursement to date.</param>
        /// <returns>Report reimbursements in the selected Date range grouped by customer</returns>
        public static SqlDataReader GetReportReimbursementsWithoutVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewReimbursementWithoutVat", fromDate, toDate);
        }


        /// <summary>
        /// Gets the report of refunded orders with vat
        /// </summary>
        /// <param name="fromDate">Report start date</param>
        /// <param name="toDate">Report end date</param>
        /// <returns></returns>
        internal static SqlDataReader GetReportRefundedOrderWithVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewRefundedOrderWithVat", fromDate, toDate);
        }

        /// <summary>
        /// Gets the report of refunded orders without vat
        /// </summary>
        /// <param name="fromDate">Report start date</param>
        /// <param name="toDate">Report end date</param>
        /// <returns></returns>
        internal static SqlDataReader GetReportRefundedOrderWithoutVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewRefundedOrderWithoutVat", fromDate, toDate);
        }

        /// <summary>
        /// Gets the report for sales channels.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>
        /// Report Number of codes sold per channel in the selected date range grouped by customer
        /// </returns>
        public static SqlDataReader GetReportReportChannelsWithVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewSalesChannels", fromDate, toDate);
        }

        /// <summary>
        /// Gets the report for sales channels.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>
        /// Report Number of codes sold per channel in the selected date range grouped by customer
        /// </returns>
        public static SqlDataReader GetReportReportChannelsWithoutVat(DateTime fromDate, DateTime toDate)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewSalesChannelsWithOutVat", fromDate, toDate);
        }

        /// <summary>
        /// Gets the report reporting view report sum.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="orderSum">The order sum.</param>
        /// <param name="creditSum">The credit sum.</param>
        /// <param name="orderSaldo">The order saldo.</param>
        /// <param name="amount">The amount.</param>
        /// <exception cref="System.ApplicationException">Error reading report: ReportingViewReportSum. No rows returned.</exception>
        public static void GetReportReportingViewReportSum(DateTime fromDate, DateTime toDate,
                                                                    out double orderSum, out double creditSum,
                                                                    out double orderSaldo, out int amount)
        {
            using (SqlDataReader reportRow = SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "ReportingViewReportSum", fromDate, toDate))
            {
                if (reportRow.Read())
                {
                    orderSum = (double)reportRow["OrderSum"];
                    creditSum = (double)reportRow["CreditSum"];
                    orderSaldo = (double)reportRow["SaldoSum"];
                    amount = (int)reportRow["AmountSum"];
                }
                else
                {
                    throw new ApplicationException("Error reading report: ReportingViewReportSum. No rows returned.");
                }
            }
        }

        /// <summary>
        /// Gets orders from specified period with specified email
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="email">The email.</param>
        /// <returns>
        /// Selected orders
        /// </returns>
        public static SqlDataReader GetOrders(DateTime fromDate, DateTime toDate, string email)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrdersSelectDateEmail", fromDate, toDate, email);
        }

        /// <summary>
        /// Select rows from Codes table for given Code.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="code">The code.</param>
        /// <returns>
        /// Rows selected
        /// </returns>
        public static SqlDataReader GetOrdersFromCode(DateTime fromDate, DateTime toDate, string code)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrdersSelectDateCode", fromDate, toDate, code);
        }

        /// <summary>
        /// Insert Order row.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="receiptNumber">The receipt number.</param>
        /// <param name="captureId">The capture identifier.</param>
        /// <param name="salesDate">The sales date.</param>
        /// <param name="expireDate">The expire date.</param>
        /// <param name="orderSum">The order sum.</param>
        /// <param name="creditSum">The credit sum.</param>
        /// <param name="saldo">The saldo.</param>
        /// <param name="status">The status.</param>
        /// <returns>
        /// Generated identifier
        /// </returns>
        public static long OrdersInsertRow(string email, string receiptNumber, string captureId, DateTime salesDate, DateTime expireDate, double orderSum, double creditSum, double saldo, short status)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "OrdersInsertRow", email, receiptNumber, captureId, salesDate, expireDate, orderSum, creditSum, saldo, status);
            return long.Parse(res.ToString());
        }

        /// <summary>
        /// Update CINT status
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="sentToCint">Sent to CINT status. 1 means transferred to CINT, 0 means it is not</param>
        public static void UpdateCintStatus(long id, short sentToCint)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "OrdersSetCintStatus", id, sentToCint);
        }

        /// <summary>
        /// Sets the application information.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="paymentMethod">The payment method.</param>
        /// <param name="appType">Type of the application.</param>
        public static void SetAppInfo(long id, int paymentMethod, int appType)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "OrdersSetAppInfo", id, paymentMethod, appType);
        }

        /// <summary>
        /// Insert Order row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="labelImage">The label image.</param>
        /// <param name="imageSize">Size of the image.</param>
        public static void OrdersUpdateImage(long id, byte[] labelImage, int imageSize)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "OrdersUpdateImage", id, labelImage, imageSize);
        }

        /// <summary>
        /// Update Orders row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="email">The email.</param>
        /// <param name="receiptNumber">The receipt number.</param>
        /// <param name="captureId">The capture identifier.</param>
        /// <param name="salesDate">The sales date.</param>
        /// <param name="expireDate">The expire date.</param>
        /// <param name="orderSum">The order sum.</param>
        /// <param name="creditSum">The credit sum.</param>
        /// <param name="saldo">The saldo.</param>
        /// <param name="status">The status.</param>
        public static void OrdersUpdateRow(long id, string email, string receiptNumber, string captureId, DateTime salesDate, DateTime expireDate, double orderSum, double creditSum, double saldo, short status)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "OrdersUpdateRow", id, email, receiptNumber, captureId, salesDate, expireDate, orderSum, creditSum, saldo, status);
        }

        /// <summary>
        /// Delete Orders row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public static void OrdersDeleteRow(long id)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "OrdersDeleteRow", id);
        }
    }
}