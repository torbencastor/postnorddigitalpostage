﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// Cards Data Access Layer
    /// </summary>
    public class Cards
    {

        /// <summary>
        /// Get Card Row from Id
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>selected row</returns>
        public static SqlDataReader CardsSelectRow(Guid id)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CardsSelectRow", id);
        }

        /// <summary>
        /// Get Card Row from OrderId
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>selected row</returns>
        public static SqlDataReader CardsSelectRow(long orderId)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "CardsSelectRowFromOrderId", orderId);
        }

        /// <summary>
        /// Cardses the insert row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="email">The email.</param>
        /// <param name="encryptedKey">The encrypted key.</param>
        /// <param name="created">The created.</param>
        /// <param name="lastUsed">The last used.</param>
        public static void CardsInsertRow(Guid id, long orderId, string email, string encryptedKey, DateTime created, DateTime lastUsed)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "CardsInsertRow", id, orderId, email, encryptedKey, created, lastUsed);
        }

        /// <summary>
        /// Update row in Cards table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="email">The email.</param>
        /// <param name="encryptedKey">The encrypted key.</param>
        /// <param name="lastUsed">The last used.</param>
        public static void CardsUpdateRow(Guid id, string email, string encryptedKey, DateTime lastUsed)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "CardsUpdateRow", id, email, encryptedKey, lastUsed);
        }

        /// <summary>
        /// Delete row in Cards table.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public static void CardsDeleteRow(Guid id)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "CardsDeleteRow", id);
        }
    }
}
