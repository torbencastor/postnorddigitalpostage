﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Dal
{

    /// <summary>
    /// Handles CRUD operations for SubOrders table.
    /// </summary>
    public class SubOrders
    {

        /// <summary>
        /// Select all SubOrders.
        /// </summary>
        /// <returns></returns>
        public static SqlDataReader SubOrdersSelectAll()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "SubOrdersSelectAllRows");
        }

        /// <summary>
        /// Select All Suborder for an order.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns></returns>
        public static SqlDataReader SubOrdersSelectFromOrderId(long orderId)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "SubOrdersSelectFromOrderId", orderId);
        }

        /// <summary>
        /// Select SubOrder row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static SqlDataReader SubOrdersSelectRow(long id)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "SubOrdersSelectRow", id);
        }

        /// <summary>
        /// Get Number of codes credited for given suborder
        /// </summary>
        /// <param name="subOrderId">The sub order identifier.</param>
        /// <returns></returns>
        public static int SubOrderGetCreditCodeCount(long subOrderId)
        {
            return (int)SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "SubOrderGetCreditCodeCount", subOrderId);
        }

        /// <summary>
        /// Insert SubOrders row.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="productId">The product identifier.</param>
        /// <param name="weight">The weight.</param>
        /// <param name="priority">The priority.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="price">The price.</param>
        /// <param name="subOrderSum">The sub order sum.</param>
        /// <param name="subOrderCredit">The sub order credit.</param>
        /// <param name="subOrderSaldo">The sub order saldo.</param>
        /// <param name="subOrderProductListId">The sub order product list identifier.</param>
        /// <returns>
        /// Generated identifier
        /// </returns>
        public static long SubOrdersInsertRow(long orderId, long productId, short weight, string priority, string destination, int amount, double price, double subOrderSum, double subOrderCredit, double subOrderSaldo, int subOrderProductListId, int sapId, double priceWithoutVAT, int VAT)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "SubOrdersInsertRow", orderId, productId, weight, priority, destination, amount, price, subOrderSum, subOrderCredit, subOrderSaldo, subOrderProductListId, sapId, priceWithoutVAT, VAT);
            return long.Parse(res.ToString());
        }

        /// <summary>
        /// Update SubOrder row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="productId">The product identifier.</param>
        /// <param name="weight">The weight.</param>
        /// <param name="priority">The priority.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="price">The price.</param>
        /// <param name="subOrderSum">The sub order sum.</param>
        /// <param name="subOrderCredit">The sub order credit.</param>
        /// <param name="subOrderSaldo">The sub order saldo.</param>
        public static void SubOrdersUpdateRow(long id, long orderId, long productId, short weight, string priority, string destination, int amount, double price, double subOrderSum, double subOrderCredit, double subOrderSaldo, int subOrderProductListId, int sapId, double priceWithoutVAT, int VAT)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "SubOrdersUpdateRow", id, orderId, productId, weight, priority, destination, amount, price, subOrderSum, subOrderCredit, subOrderSaldo, subOrderProductListId, sapId, priceWithoutVAT, VAT);
        }

        /// <summary>
        /// Delete SubOrder row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public static void SubOrdersDeleteRow(long id)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "SubOrdersDeleteRow", id);
        }
       
        /// <summary>
        /// Change code status for all codes in suborder.
        /// </summary>
        /// <param name="subOrderId">The sub order identifier.</param>
        /// <param name="oldStatus">The old status.</param>
        /// <param name="status">The status.</param>
        public static void SubOrdersChangeCodeStatus(long subOrderId, short oldStatus, short status)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "SubOrdersChangeCodeStatus", subOrderId, oldStatus, status);
        }

        /// <summary>
        /// Change code status for all codes in suborder.
        /// </summary>
        /// <param name="stringArrayOfSubOrderId">String array of sub order identifiers.</param>
        /// <param name="oldStatus">The old status.</param>
        /// <param name="status">The status.</param>
        public static void SubOrdersChangeCodeStatusByList(string stringArrayOfSubOrderId, short oldStatus, short status)
        {
            var connectionString = Helpers.HelperClass.GetConnectionString();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "SubOrdersChangeCodeStatusByList";
                        command.Parameters.Add(new SqlParameter("listOfSuborderId", stringArrayOfSubOrderId));
                        command.Parameters.Add(new SqlParameter("OldStatus", oldStatus));
                        command.Parameters.Add(new SqlParameter("NewStatus", status));
                        command.ExecuteNonQuery();
                        /*
                        var n = 0;
                        var i = 345 / n;
                         */
                    }
                }
                catch (Exception e)
                {
                    throw new ApplicationException(string.Format("SubOrdersChangeCodeStatusByList | Exception Source: {0} | Exception message: {1} | Exception StackTrace: {2}",
                        e.Source, e.Message, e.StackTrace));
                }
                finally
                {
                    connection.Close();
                }
            }


            //  SqlHelper.ExecuteNonQuery(, "SubOrdersChangeCodeStatusByList", stringArrayOfSubOrderId, oldStatus, status);
        }
    }
}
