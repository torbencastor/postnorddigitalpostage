﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// Handles the Code counting
    /// </summary>
    public class NextCode
    {
        /// <summary>
        /// Maximum Code Value
        /// </summary>
        public static int MAX_CODE = 425910;


        #region Public Methods

        /// <summary>
        /// Gets the next code.
        /// </summary>
        /// <param name="codeCount">The code count.</param>
        /// <returns>
        /// value of last code
        /// </returns>
        /// <exception cref="System.ApplicationException">Error Generating code</exception>
        public static int GetNextCodeTransaction(int codeCount)
        {
            int tmpCode = -2;
            try
            {
                string connectionString = Helpers.HelperClass.GetConnectionString();
                using (SqlConnection scon = new SqlConnection(connectionString))
                {
                    scon.Open();
                    SqlTransaction sqlTrans = scon.BeginTransaction(IsolationLevel.Serializable);

                    tmpCode = NextKeyUpdateKeyTransaction(sqlTrans, codeCount);

                    // if tmpCode is -1 code was not found in table, so we generate new one 
                    if (tmpCode == -1)
                    {
                        tmpCode = NextKeyInsertRowTransaction(sqlTrans, codeCount);
                    }

                    // if code is bigger than MAX code reset it
                    if (tmpCode > MAX_CODE)
                    {
                        tmpCode = codeCount + 1;
                        tmpCode = NextKeyResetRowTransaction(sqlTrans, tmpCode);
                    }

                    sqlTrans.Commit();
                    scon.Close();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error Generating codeId: {0}", ex.Message), ex);
            }
            return tmpCode;
        }


        /// <summary>
        /// Gets the next code.
        /// </summary>
        /// <param name="price">The price.</param>
        /// <param name="codeCount">The code count.</param>
        /// <returns>
        /// value of last code
        /// </returns>
        /// <exception cref="System.ApplicationException">Error Generating code</exception>
        public static int GetNextCode(double price, int codeCount)
        {
            int tmpCode = -2;
            try
            {
                tmpCode = NextKeyUpdateKey(price, codeCount);

                // if tmpCode is -1 code was not found in table, so we generate new one 
                if (tmpCode == -1)
                {
                    tmpCode = NextKeyInsertRow(price, codeCount);
                }

                // if code is bigger than MAX code reset it
                if (tmpCode > MAX_CODE)
                {
                    tmpCode = codeCount + 1;
                    tmpCode = NextKeyResetRow(price, tmpCode);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error Generating codeId: {0}", ex.Message), ex);
            }
            return tmpCode;
        }


        /// <summary>
        /// Gets the current code.
        /// </summary>
        /// <returns>
        /// Current Code
        /// </returns>
        /// <remarks>
        /// Used for Unit Tests only
        /// </remarks>
        public static int GetCurrentCode(double price)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "NextKeySelectRow", price);
            if (res == null)
            {
                return -1;
            }
            return int.Parse(res.ToString());
        }

        /// <summary>
        /// Create initial Row.
        /// </summary>
        /// <param name="price">The price.</param>
        /// <param name="codeCount">The code count.</param>
        /// <returns>
        /// The code count
        /// </returns>
        private static int NextKeyInsertRow(double price, int codeCount)
        {
            return (int)SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "NextKeyInsertRow", price, codeCount);
        }

        /// <summary>
        /// Create initial Row.
        /// </summary>
        /// <param name="price">The price.</param>
        /// <param name="codeCount">The code count.</param>
        /// <returns>
        /// The code count
        /// </returns>
        public static int NextKeyResetRow(double price, int codeCount)
        {
            return (int)SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "NextKeyUpdateRow", price, codeCount);
        }

        /// <summary>
        /// Create initial Row.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="codeCount">The code count.</param>
        /// <returns>
        /// The code count
        /// </returns>
        private static int NextKeyInsertRowTransaction(SqlTransaction transaction, int codeCount)
        {
            return (int)SqlHelper.ExecuteScalar(transaction, "NextKeyInsertRow", codeCount);
        }


        /// <summary>
        /// Set Code Count to specified value.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="codeCount">The code count.</param>
        /// <returns>
        /// Code count after reset
        /// </returns>
        private static int NextKeyResetRowTransaction(SqlTransaction transaction, int codeCount)
        {
            object res = SqlHelper.ExecuteScalar(transaction, "NextKeyUpdateRow", 1, codeCount);
            if (res == null)
            {
                return -1;
            }
            return int.Parse(res.ToString());
        }

        /// <summary>
        /// Updates the key with the given codeCount value.
        /// </summary>
        /// <param name="price">The price.</param>
        /// <param name="codeCount">The code count.</param>
        /// <returns>
        /// The new CodeCount value
        /// </returns>
        private static int NextKeyUpdateKey(double price, int codeCount)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "NextKeyUpdateKey",price, codeCount);
            if (res==null)
            {
                return -1;
            }
            return int.Parse(res.ToString());
        }

        /// <summary>
        /// Updates the key with the given codeCount value.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="codeCount">The code count.</param>
        /// <returns>
        /// The new CodeCount value
        /// </returns>
        private static int NextKeyUpdateKeyTransaction(SqlTransaction transaction, int codeCount)
        {
            object res = SqlHelper.ExecuteScalar(transaction, "NextKeyUpdateKey", 1, codeCount);
            if (res == null)
            {
                return -1;
            }
            return int.Parse(res.ToString());
        }


        #endregion

    }
}
