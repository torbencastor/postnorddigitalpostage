﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Dal
{
    /// <summary>
    /// Handles CRUD operations for OrderLogs table. 
    /// </summary>

    public class OrderLogs
    {

        /// <summary>
        /// Select all OrderLogs.
        /// </summary>
        /// <returns>
        /// Rows selected
        /// </returns>
        public static SqlDataReader OrderLogsSelectAll()
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrderLogsSelectAllRows");
        }

        /// <summary>
        /// Select All OrderLogs for given Order Id.
        /// </summary>
        /// <returns>
        /// All OrderLogs rows for the given Order Id
        /// </returns>
        public static SqlDataReader OrderLogsSelectForOrder(long orderId)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrderLogsSelectForOrder", orderId);
        }

        /// <summary>
        /// Select OrderLogs row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Selected row
        /// </returns>
        public static SqlDataReader OrderLogsSelectRow(long id)
        {
            return SqlHelper.ExecuteReader(Helpers.HelperClass.GetConnectionString(), "OrderLogsSelectRow", id);
        }

        /// <summary>
        /// Insert OrderLogs row.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="text">The text.</param>
        /// <returns>Generated identifier</returns>
        public static long OrderLogsInsertRow(long orderId, DateTime timestamp, short severity, string text)
        {
            object res = SqlHelper.ExecuteScalar(Helpers.HelperClass.GetConnectionString(), "OrderLogsInsertRow", orderId, timestamp, severity, text);
            return long.Parse(res.ToString());
        }

        /// <summary>
        /// Update OrderLogs row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="text">The text.</param>
        public static void OrderLogsUpdateRow(long id, long orderId, DateTime timestamp, short severity, string text)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "OrderLogsUpdateRow", id, orderId, timestamp, severity, text);
        }

        /// <summary>
        /// Delete OrderLog Row.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public static void OrderLogsDeleteRow(long id)
        {
            SqlHelper.ExecuteNonQuery(Helpers.HelperClass.GetConnectionString(), "OrderLogsDeleteRow", id);
        }

        /// <summary>
        /// Gets the order log status.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public static DataSet GetOrderLogStatus(DateTime fromDate, DateTime toDate, short status)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "OrderLogsSelectStatus", fromDate, toDate, status);
        }

        /// <summary>
        /// Search Orderlog.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="search">The text to match.</param>
        /// <returns></returns>
        public static DataSet GetOrderLogSearch(DateTime fromDate, DateTime toDate, string search)
        {
            return SqlHelper.ExecuteDataset(Helpers.HelperClass.GetConnectionString(), "OrderLogsSearch", fromDate, toDate, search);
        }
        

    }
}
