﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace PostNord.Portal.DigitalPostage.WebControls
{
    /// <summary>
    /// Text Settings WebControl. Renders text from textSettings list.
    /// </summary>
    public class GetText : System.Web.UI.WebControls.Literal
    {
        /// <summary>
        /// internal value
        /// </summary>
        private string _default = string.Empty;

        /// <summary>
        /// internal value
        /// </summary>
        private string _key = string.Empty;

        private bool _useConfigSetting = false;


        /// <summary>
        /// Gets or sets the default value
        /// </summary>
        /// <value>The default is shown when translation is not found</value>
        public string Default
        {
            get { return this._default; }
            set { this._default = value; }
        }

        /// <summary>
        /// Gets or sets the translation key
        /// </summary>
        /// <value>The translation key.</value>
        public string Key
        {
            get { return this._key; }
            set { this._key = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to read data from Config List instead of Text list.
        /// </summary>
        /// <value>
        /// <c>true</c> if [use configuration setting]; otherwise, <c>false</c>.
        /// </value>
        public bool UseConfigSetting
        {
          get { return _useConfigSetting; }
          set { _useConfigSetting = value; }
        }

        /// <summary>
        /// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param _name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            try
            {
                if (string.IsNullOrEmpty(this._key))
                {
                    writer.Write(string.Format("Translation parameters missing: key:{0}", this._key));
                    return;
                }

                if (this.UseConfigSetting)
                {
                    writer.Write(PostNord.Portal.DigitalPostage.Helpers.ConfigSettings.GetText(this._key, this._default));
                }
                else
                {
                    writer.Write(PostNord.Portal.DigitalPostage.Helpers.TextSettings.GetText(this._key, this._default));
                }
            }
            catch (Exception ex)
            {
                writer.Write("GetText error: {0}", ex.Message);
            }
        }
    }
}
