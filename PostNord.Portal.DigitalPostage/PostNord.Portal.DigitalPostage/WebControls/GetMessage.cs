﻿using Microsoft.SharePoint;
using PostNord.Portal.DigitalPostage.Bll;
using PostNord.Portal.DigitalPostage.WebParts.BuyPostage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace PostNord.Portal.DigitalPostage.WebControls
{
    /// <summary>
    /// Handles display of messsages for the list admin/Messages/
    /// </summary>
    public class GetMessage : System.Web.UI.WebControls.Literal
    {
        string html = @"<div id='dialog-message' title='{0}' style='display:none'><p>{1}</p></div>";

        /// <summary>
        /// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the server control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            try
            {
                if (HttpContext.Current.Request.QueryString.Count == 0)
                {
                    SPListItem item = Messages.GetMessage(BuyPostageWP.PLATFORM);
                    if (item != null && item["Message"] != null)
                    {
                        writer.Write(string.Format(html, item.Title, item["Message"].ToString().Replace(Environment.NewLine, "<br/>")));
                    }
                }
            }
            catch (Exception ex)
            {
                writer.Write("GetMessage error: {0}", ex.Message);
            }
        }
    }
}
