﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="86ff8b42-f662-4e64-b6be-23a08473eff5" description="Adds Page definitions" featureId="86ff8b42-f662-4e64-b6be-23a08473eff5" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="Digital Postage -  Create publishingPages" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="7aec24c3-9dd1-4c8b-8abc-770ef646656f" />
    <projectItemReference itemId="000355f6-8c3e-444e-9eab-85ef7e3f9936" />
    <projectItemReference itemId="c0238492-0499-4e95-bc55-1170aa0bb02f" />
    <projectItemReference itemId="041cff28-7802-4400-8a5c-ba13d4a0514e" />
    <projectItemReference itemId="d3be7894-05bf-4f34-bcef-a08d97abef40" />
    <projectItemReference itemId="43429124-518a-4e0b-abc6-036ae0f5112b" />
  </projectItems>
</feature>