﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="63e6a15e-726d-41b7-b84f-d9c34f916905" description="Add DigitalPostage Webparts" featureId="63e6a15e-726d-41b7-b84f-d9c34f916905" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="Digital Postage - WebParts" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="6d4af74b-3b34-4398-8332-f0ad3c7d50b0" />
    <projectItemReference itemId="a701b842-7130-41bc-b05c-925910a72df0" />
    <projectItemReference itemId="ebbfc806-78d0-439a-96ab-119f99b9ac4b" />
    <projectItemReference itemId="a3db8064-f3e8-40e7-b3be-7d702cf308ee" />
    <projectItemReference itemId="72e427b7-003c-47cd-9d30-5fc5a123fd22" />
    <projectItemReference itemId="6a6768b2-a75f-49a7-b8d0-8e17a0e2e95e" />
    <projectItemReference itemId="b6c0b983-27d0-4bcf-bf41-d44f00ea9314" />
    <projectItemReference itemId="6ba46f0a-040c-4054-ad71-5ad765c90a45" />
    <projectItemReference itemId="b31e1547-47c6-4595-9bc4-16b62b5429f3" />
    <projectItemReference itemId="041cff28-7802-4400-8a5c-ba13d4a0514e" />
  </projectItems>
</feature>