using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;

namespace PostNord.Portal.DigitalPostage.Features.Feature1
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("6ea5d3b2-ea65-4388-bb4a-26c3b09e47c5")]
    public class Feature1EventReceiver : SPFeatureReceiver
    {
        /// <summary>
        /// Updates PriceCategory items from the country list.
        /// Occurs after a Feature is activated.
        /// </summary>
        /// <param name="properties">An <see cref="T:Microsoft.SharePoint.SPFeatureReceiverProperties" /> object that represents the properties of the event.</param>
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            using (SPWeb web = properties.Feature.Parent as SPWeb)
            {
                try
                {
                    SPList categoryList = web.Lists["PriceCategories"];
                    SPList countries = web.Lists["Countries"];

                    SPFieldLookupValueCollection values = null;
                    SPFieldLookupValue val = null;

                    foreach (SPListItem item in categoryList.Items)
                    {
                        Console.WriteLine(item.Title);
                        switch (item.Title)
                        {
                            case "Danmark":
                                values = new SPFieldLookupValueCollection();
                                foreach (SPListItem country in countries.Items)
                                {
                                    string cat = country["CountryCategory"].ToString();
                                    if (cat == "Danmark")
                                    {
                                        val = new SPFieldLookupValue(country.ID, country.Title);
                                        values.Add(val);
                                        Console.WriteLine("  " + country.Title);
                                    }
                                }
                                item["Countries"] = values;
                                item.Update();
                                break;
                            case "Europa, F�r�erne og Gr�nland":
                                values = new SPFieldLookupValueCollection();
                                foreach (SPListItem country in countries.Items)
                                {
                                    string cat = country["CountryCategory"].ToString();
                                    if (cat == "Europa" || cat == "Norden" || cat == "Gr�nland" || cat == "F�r�erne")
                                    {
                                        val = new SPFieldLookupValue(country.ID, country.Title);
                                        values.Add(val);
                                        Console.WriteLine("  " + country.Title);
                                    }
                                }
                                item["Countries"] = values;
                                item.Update();
                                break;
                            case "�vrige udland":
                                values = new SPFieldLookupValueCollection();

                                foreach (SPListItem country in countries.Items)
                                {
                                    string cat = country["CountryCategory"].ToString();
                                    if (cat == "�vrige udland")
                                    {
                                        val = new SPFieldLookupValue(country.ID, country.Title);
                                        values.Add(val);
                                        Console.WriteLine("  " + country.Title);
                                    }
                                }
                                item["Countries"] = values;
                                item.Update();
                                break;
                        }
                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Error: {0}", ex.Message);
                }
            }
        }

    }
}
