﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="34834d71-6d1c-4097-b738-6a9ec5c05954" activateOnDefault="false" description="Updates PriceCategory items from the country list" featureId="34834d71-6d1c-4097-b738-6a9ec5c05954" imageUrl="" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.6ea5d3b2-ea65-4388-bb4a-26c3b09e47c5.FullName$" solutionId="00000000-0000-0000-0000-000000000000" title="Digital Postage - Update Categories" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <activationDependencies>
    <referencedFeatureActivationDependency minimumVersion="" itemId="f476053d-abdf-4269-a71a-184c5019aed8" />
  </activationDependencies>
</feature>