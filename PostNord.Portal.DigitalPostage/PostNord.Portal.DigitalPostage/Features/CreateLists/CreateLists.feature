﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="15211e5a-8cee-460c-b797-bc7fbd6a6c01" alwaysForceInstall="true" description="Create the following lists: ConfigSettings, TextSettings, Products, PriceCategories, Countries and Messages" featureId="15211e5a-8cee-460c-b797-bc7fbd6a6c01" imageUrl="" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.6b27d600-6110-45ed-94dc-415c16d07bd3.FullName$" solutionId="00000000-0000-0000-0000-000000000000" title="Digital Postage - Create Lists" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="60c99bf9-fd97-4974-aa91-a066bc07b24b" />
    <projectItemReference itemId="c680e12d-95c4-4f20-8604-4c4c69ccf0eb" />
    <projectItemReference itemId="798faad0-010e-459c-8629-34273915366a" />
    <projectItemReference itemId="2c73f305-11f6-48b5-b132-c5c69088c620" />
    <projectItemReference itemId="a3ff119f-a199-4edf-afb5-dca0826d2c25" />
    <projectItemReference itemId="ca10ea5b-8a5c-4bf0-9088-26a70b1d8c1b" />
    <projectItemReference itemId="83a81e32-f630-435b-825b-6139f7b256b2" />
    <projectItemReference itemId="3907b13b-fdf7-4f08-8411-ef47dc05c8ee" />
    <projectItemReference itemId="4d6ceda6-7713-4faa-adcf-53113e42cb12" />
    <projectItemReference itemId="104e928d-f1af-4e62-bac4-63fab3e7d509" />
    <projectItemReference itemId="7d7e6868-cdcf-4de3-976e-f6f632fa911f" />
    <projectItemReference itemId="a569e03f-24d0-4806-bff3-e3635e87d6c5" />
    <projectItemReference itemId="9c0500d1-02bc-466b-9fbb-b3a95b4d8ca0" />
    <projectItemReference itemId="b0b2a5a2-3da0-41ba-8f72-35478998861f" />
  </projectItems>
</feature>