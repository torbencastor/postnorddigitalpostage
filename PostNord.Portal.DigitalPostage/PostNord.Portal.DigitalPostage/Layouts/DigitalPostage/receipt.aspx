﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Register Tagprefix="pnc" Namespace="PostNord.Portal.DigitalPostage.WebControls" Assembly="$SharePoint.Project.AssemblyFullName$" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="receipt.aspx.cs" Inherits="PostNord.Portal.DigitalPostage.Layouts.DigitalPostage.Receipt" %>
<!DOCTYPE html>
<html>
<head>
    <title><pnc:GetText runat="server" Key="BUYPOSTAGE_RECEIPT_TITLE" Default="" /></title>
    <link rel="stylesheet" href="/Style%20Library/DigitalPostage/css/wizstep.css" />
    <!--[if IE]>
        <link rel="stylesheet" href="/Style%20Library/DigitalPostage/css/wizstepie.css" />
    <![endif]-->

</head>
<body>
  <div id="wizardToolBar">
        <div id="wizStep1" class="wizStepLeft"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_1" Default="" /></span></div>
        <div id="wizStep2" class="wizStep"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_2" Default="" /></span></div>
        <div id="wizStep3" class="wizStep"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_3" Default="" /></span></div>
        <div id="wizStep4" class="wizStep"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_4" Default="" /></span></div>
        <div id="wizStep5" class="wizStep wizStepActive"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_5" Default="" /></span></div>
    </div>
    <div id="steps">
        <div id="form5" class="stepsForm">
            <form runat="server">
                <h2><pnc:GetText runat="server" Key="BUYPOSTAGE_RECEIPT_HEADLINE" Default="" /></h2>
                <div id="receiptButtons">
                    <asp:Button ID="btnPrintReceipt" runat="server" Text="Udskrive Kvittering" OnClick="btnPrintReceipt_Click" />
                    <asp:Button ID="btnPrintLabels" runat="server" Text="Udskriv Portokoder" OnClick="btnPrintLabels_Click" />
                </div>
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
                <asp:Panel ID="receiptForm" runat="server"></asp:Panel>
            </form>
        </div>
    </div>
</body>
</html>
