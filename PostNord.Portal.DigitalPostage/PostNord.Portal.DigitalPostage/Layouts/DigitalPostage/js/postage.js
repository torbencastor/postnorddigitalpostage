﻿function PostageInit() {

   

    var uploader = $("#fileUploader");
    //$("#go2PaymentBtn").hide();

    var startPage = 1;

    DisplayForm(startPage);
    DisplayStep(startPage);
    UpdatePrize();

    $("#newPostage").spinner({
        min: 0,
        stop: function (e, ui) {
            UpdatePrize();
        }
    });

    $("#newPostage").on("spinchange", function (event, ui) {
        UpdatePrize();
    });

    $("input[type='button']").button();
    $("input[type='submit']").button();
    $("#countrySelector").change(function () {
        UpdatePrize();
    });
    $("#weightSelector").change(function () {
        UpdatePrize();
    });
    $("#apost").change(function () {
        UpdatePrize();
    });
    $("#bpost").change(function () {
        UpdatePrize();
    });

    $("#form1Proceed").click(function () {
        ValidateForm(1);
        if (shoppingCart.length > 0) {
            DisplayForm(2);
            DisplayStep(2);
            DisplayCart();
        }
    });

    $("#BuyMore").click(function () {
        DisplayForm(1);
        DisplayStep(1);
    })

    $("#MakeLabel").click(function () {

        if (shoppingCart.length > 0) {
            DisplayForm(3);
            DisplayStep(3);
        }
    });

    $("#uploadBtn").click(function () {
        if ($("#BrowserHiddenFileField")[0].value.length > 0) {
            $($("#BrowserHiddenFileField").parent()).removeClass("errorBorder");
        }
        else{
            $($("#BrowserHiddenFileField").parent()).addClass("errorBorder");
            return false;
        }
    });

    $("#resetImg").click(function () {
        $("#imgFrame").attr("src", "/_layouts/digitalpostage/helpers/showimage.aspx");
        $("#fileForm")[0].reset();
    });

    $("#Go2Payment").click(function () {
        DisplayForm(4);
        DisplayStep(4);
    });

    $("#labelPrevious").click(function () {
        DisplayForm(2);
        DisplayStep(2);
    });

    $("#labelSelect").click(function () {
        DisplayForm(4);
        DisplayStep(4);
    });

    $("#seeOrderBtn").click(function () {
        DisplayForm(2);
        DisplayStep(2);
    });

    $("#makeLabelBtn").click(function () {
        DisplayForm(3);
        DisplayStep(3);
    });

    $("#email1").focusout(function () {
        ValidateEmail("#email1");
        CompareEmails();
    });
    $("#email2").focusout(function () {
        //ValidateEmail("#email2");
        CompareEmails();
    });

    $("#acceptTerms").change(function () {
        ValidateTermsCheck();
    });

    $("#go2PaymentBtn").click(function () {

        if (!EnablePayment()) {
            if (!ValidateForm(4)) {
                return;
            }
        }

        var imgDisplayUrl = $("#imgFrame")[0].contentWindow.location.href;
        var email = $("#email1").val();
        var transactionId = '';
        if ($.getUrlVar('transactionId')) {
            transactionId = $.getUrlVar('transactionId');
        }
    
        var data = JSON.stringify(new OrderData(email, shoppingCart, imgDisplayUrl, transactionId));

        $.ajax({
            type: "POST",
            url: "/_layouts/DigitalPostage/Helpers/placeorder.aspx",
            data: data,
            dataType: "json",
            success: function (msg) {
                alert('OK: ' + msg);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 200) {
                    var response = jqXHR.responseText;
                    if (response.indexOf('failed') > 0) {
                        alert(response);
                        alert('error: ' + errorThrown.message + '. ' + errorThrown.stack);
                        return;
                    }
                    if (response.indexOf('http') > -1) {
                        document.location.href = response;
                    } else {
                        alert('Location is wrong: ' + response);
                    }
                }
            }
        });
    });

    if ($.getUrlVar('debug')) {
        var email = $.getUrlVar('debug');
        $("#email1").val(email);
        $("#email2").val(email);
        $('#acceptTerms').prop('checked', true);
        $('#newPostage').val('5');
    }

}

function UpdateOrderLine(id) {
    var item = null;
    var index = -1;

    for (var n = 0; n < shoppingCart.length; n++) {
        if (shoppingCart[n].no == id) {
            item = shoppingCart[n];
            index = n;
            break;
        }
    }

    if (index > -1) {
        var total = item.price * $("#countEditor" + id).val();
        $("#lineTotal" + id).val(GetDisplayPrice(total));
        shoppingCart[index].count = total / item.price;
        shoppingCart[index].total = total;
        UpdateTotalPriceWithOutUpdatingCart();
    }
}

function GetPrice(categoryId, weight, postageType) {
    if (prices == null) {
        alert('Prices not found');
        return 0;
    }

    for (var i = 0; i < prices.length; i++) {
        if (prices[i][4] == categoryId && weight == prices[i][1]) {
            switch (postageType) {
                case 'A':
                    return prices[i][2];
                    break;
                case 'B':
                    return prices[i][3];
                    break;
                default:
                    alert('Error: unkown postagetype:' + postageType);
                    break;
            }
        }
    }
}

function UpdatePrize() {
    try {
        var categoryId = $("#countrySelector").val();
        var category = priceCategories[categoryId];

        $('#radioBtnTextA').text(category[0]);
        $('#radioBtnTextB').text(category[1]);

        var weight = $("#weightSelector").val();
        var postageType = $("#apost").is(":checked") ? "A" : "B";

        var postagePrize = GetPrice(categoryId, weight, postageType);

        $("#unitPrize").val(GetDisplayPrice(postagePrize));

        var count = $("#newPostage").val();

        if (count < 0) {
            count = 0;
            $("#newPostage").val(count);
        }

        var total = count * postagePrize;

        $("#unitTotal").val(GetDisplayPrice(total));
    }
    catch (e) {
        alert('Update Price exception: ' + e.message);
    }
}


function AddNewProduct(count, destination, categoryId, weight, postageType, postagePrize) {
    var total = count * postagePrize;
    var date = new Date();
    var id = date.getTime() + shoppingCart.length;

    if (categoryId == 0) {
        var drp = document.getElementById('countrySelector');
        for (var i = 0; i < drp.options.length; i++) {
            if (drp.options[i].text == destination) {
                categoryId = drp.options[i].value;
                break;
            }
        }
    }

    $("#newPostage").val(0);
    shoppingCart.push(new PostageItem(count, destination, weight, postageType, postagePrize, id, total, categoryId));
    DisplayCart();
    UpdateTotalPrice();
}


function AddProduct() {
    var count = parseInt($("#newPostage").val());
    var categoryId = $("#countrySelector").val();
    var weight = $("#weightSelector").val();
    var postageType = $("#apost").is(":checked") ? "A" : "B";
    var destination = $("#countrySelector option:selected").text()
    var postagePrize = GetPrice(categoryId, weight, postageType);
    AddNewProduct(count, destination, categoryId, weight, postageType, postagePrize);
}

function GetDisplayPrice(price) {
    var kroner = Math.round(price / 100);
    var rest = price - (kroner * 100);
    var unitPrice = rest;
    rest = ("00" + rest).slice(-2);

    var text = kroner + "," + rest + " kr.";
    return text;
}

function DisplayStep(id) {
    $(".wizStep").removeClass("wizStepActive");
    $(".wizStepLeft").removeClass("wizStepActiveLeft");
    if (id != 1)
    {
        $("#wizStep" + id).addClass("wizStepActive");
    }
    else
    {
        $("#wizStep" + id).addClass("wizStepActiveLeft");
    }
}

function ResetInputErrors(id) {
    errorLabels = $(".errorLabel")
    for (var i = 0; i < errorLabels.length; i++) {
        removeStar("#"+errorLabels[i].id);
        $(errorLabels[i]).removeClass("erroLabel");
    }
    $(".errorBorder").removeClass("errorBorder");
    $("#errorBox").html("");
    $("#errorBox").hide();
}

function ValidateForm(id) {
    //add product
    if (id == 1) {
        var count = $("#newPostage").val();

        if (count > 0) {
            AddProduct();
        }
        else {
            addStar("#productTitle")
            $("#productTitle").addClass("errorLabel");
            $(($("#newPostage")[0]).parentNode).addClass("errorBorder");
        }
    }

    //go to payment
    if (id == 4) {
        if (ValidateEmail("#email1") && ValidateEmail("#email2") && EnablePayment())
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
}

function DisplayForm(id) {
    ResetInputErrors();
    if (id == 2) {
        UpdatePrize();
    }
    if (id == 2) {
        DisplayCart();
    }
    $(".stepsForm").hide();
    $("#form" + id).show();
}

function addStar(id) {
    var node = $(id)[0]
    if (node.innerText.indexOf(" *") == -1) {
        node.innerText = node.innerText.concat(" *");
    }
}

function removeStar(id) {
    var idx = $(id)[0].innerText.indexOf(" *");
    if (idx > -1) {
        $(id)[0].innerText= $(id)[0].innerText.substring(0, idx);
    }
}

function ValidateEmail(id) {
    var email = $(id).val();
    if (email == '') {
        $(id).focus();
        $(id).removeClass("errorBorder");
        $(id).addClass("errorBorder");
        
        $("#errorBox").html("E-mail skal udfyldes.");
        $("#errorBox").show();
        return false;
    }
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
        $(id).focus();
        $(id).removeClass("errorBorder");
        $(id).addClass("errorBorder");
        $("#errorBox").html("E-mail er ikke gyldig.");
        $("#errorBox").show();
        return false;
    }

    $("#errorBox").html("");
    $(id).removeClass("errorBorder");
    
    return true;
}

function CompareEmails() {
    var emailsMatch = true;

    var emailAdr1 = $("#email1").val();

    if (emailAdr1 != "") {
        emailAdr1 = emailAdr1.toLowerCase();
    }
    else {
        enabled = false;
        addStar("#email1Text");
        $("#errorBox").html("Angiv E-mail");
        $("#errorBox").show();
        $("#email1").addClass("errorBorder");
    }

    var emailAdr2 = $("#email2").val();

    if (emailAdr2 != "") {
        emailAdr2 = emailAdr2.toLowerCase();
    }    
    else {
        enabled = false;
        $("#errorBox").html("Angiv E-mail");
        $("#errorBox").show();
        addStar("#email2Text");
        $("#email2").addClass("errorBorder");
    }
    var emailsMatch = (emailAdr1 == emailAdr2);

    if (emailsMatch && emailAdr1 != "") {
        $("#errorBox").html("");
        removeStar("#email1Text");
        removeStar("#email2Text");
        $("#email1").removeClass("errorBorder");
        $("#email2").removeClass("errorBorder");
    }
    else {
        addStar("#email1Text");
        addStar("#email2Text");
        $("#email1").addClass("errorBorder");
        $("#email2").addClass("errorBorder");
        $("#errorBox").html("De indtastede emails er ikke ens. <br/>Kontrollér venligst.");
    }

    return emailsMatch;
}

function ValidateTermsCheck() {
    var termsChecked = $("#acceptTerms").is(":checked")

    var termsSpan = $("#termsText")[0];
    if (!termsChecked) {
        addStar("#termsText");
        $(termsSpan.parentNode).addClass("errorBorder");
        enabled = false;
    }
    else {
        removeStar("#termsText");
        $(termsSpan.parentNode).removeClass("errorBorder")
    }

    return termsChecked
}

function RemoveProduct(id) {
    for (var i = 0; i < shoppingCart.length; i++) {
        if (shoppingCart[i].no === id) {
            shoppingCart.splice(i, 1);
            i--;
        }
    }
    DisplayCart();
    UpdateTotalPrice();
}

function UpdateProduct(id, count) {
    if (count > 0) {
        for (var i = 0; i < shoppingCart.length; i++) {
            var item = shoppingCart[i];

            if (item.id == id) {
                item.count = count;
            }
        }
    }
    else {
        RemoveProduct(id);
    }

    DisplayCart();
    UpdateTotalPrice();
}

function UpdateTotalPrice() {
    UpdateTotalPriceWithOutUpdatingCart();
    DisplayCart();
}

function UpdateTotalPriceWithOutUpdatingCart() {
    var totalPrice = 0;

    if (shoppingCart.length > 0) {
        for (var i = 0; i < shoppingCart.length; i++) {
            var count = shoppingCart[i].count;
            var price = shoppingCart[i].price;
            totalPrice += (count * price);
        }
    }
    $("#totalOrderAmount").text(GetDisplayPrice(totalPrice));
}

function EnableEditing(id) {
    $("#editCount" + id).attr("style", "visibility:visible");
    $("#count" + id).attr("style", "visibility:hidden");
    $("#countEditor" + id).spinner({
        min: 0,
        stop: function (e, ui) {
            UpdateOrderLine(id);
        }
    });
}

function DisableEditing(id) {
    $("#editCount" + id).attr("style", "visibility:hidden");
    $("#count" + id).attr("style", "visibility:visible");
}

function DisplayCart() {
    $(".orderLine").remove();

    for (var i = 0; i < shoppingCart.length; i++) {
        var item = shoppingCart[i];

        var tr = '<tr class="orderLine">' +
            '<td class="item">' + item.postageType + '</td>' +
            '<td class="item">' + item.weight + '</td>' +
            '<td class="item">' + item.destination + '</td>' +
            '<td class="item">' + GetDisplayPrice(item.price) + '</td>' +
            '<td class="item"><input type="text" value="' + item.count + '" id="countEditor' + item.no + '" class="countField" style="border:none; background-color:White;" disabled/></td>' +
            '<td class="item"><input type="text" value="' + GetDisplayPrice(item.total) + '" id="lineTotal' + item.no + '" style="border:none; background-color:White;" disabled/></td>' +
            '<td class="toolIcons">' +
                    '<img src="imgs/edit.png" alt="Ret antal" title="Ret antal" onclick="EnableEditing(' + item.no + ');"/>' +
                    '<img src="imgs/delete.png" alt="Slet ordre linie" title="Slet ordre linie" onclick="RemoveProduct(' + item.no + ');" />' +
            '</td>' +
        '</tr>';
        $('#orderTotalRow').before(tr);
    }
}

function PostageItem(count, country, weight, postageType, postagePrize, id, total, categoryId) {
    this.count = count;
    this.destination = country;
    this.weight = weight;
    this.postageType = postageType;
    this.price = postagePrize;
    this.no = id;
    this.total = total;
    this.categoryId = categoryId;
}

function OrderData(email, cart, imageFile, transactionId) {
    this.mailAddress = email;
    this.shop = cart;
    this.image = imageFile;
    this.transactionId = transactionId;
}

function EnablePayment() {
    var enabled = true;

    var emailsAreAlike = CompareEmails();
    var termsChecked = ValidateTermsCheck();
    enabled &= termsChecked && emailsAreAlike;
    return enabled;
}


$.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});