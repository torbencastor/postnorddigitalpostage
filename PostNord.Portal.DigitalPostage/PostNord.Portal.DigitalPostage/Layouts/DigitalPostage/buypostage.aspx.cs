﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using PostNord.Portal.DigitalPostage.Bll;
using PostNord.Portal.DigitalPostage.Helpers;
using PostNord.Portal.Tools.Labels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage
{
    /// <summary>
    /// Buy Postage Object
    /// </summary>
    public partial class BuyPostage : UnsecuredLayoutsPageBase
    {
        private const string TextListName = "TextSettings";

        /// <summary>
        /// Constant script names
        /// </summary>
        private enum ScriptNames
        {
            /// <summary>
            /// Prices script
            /// </summary>
            PostagePrices,
            /// <summary>
            /// Weight script
            /// </summary>
            PostageWeight,
            /// <summary>
            /// Delivery script
            /// </summary>
            PostageDelivery,
            /// <summary>
            /// Country script
            /// </summary>
            PostageCountry
        }

        /// <summary>
        /// Holds the type of the object
        /// </summary>
        private Type csType;

        /// <summary>
        /// Gets a value that indicates whether users can access the page without logging in.
        /// </summary>
        /// <returns>true if anonymous access is allowed; otherwise, false. </returns>
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Creates the order.
        /// </summary>
        /// <param name="transactionID">The transaction identifier.</param>
        /// <param name="debug">The debug.</param>
        private void CreateOrder(string transactionID, string debug)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite siteElevated = new SPSite(SPContext.Current.Site.ID))
                {
                    using (SPWeb web = siteElevated.OpenWeb(siteElevated.RootWeb.ID))
                    {
                        Bll.Orders order = new Orders(transactionID);
                        if (debug != "skip")
                        {
                            try
                            {
                                order.Capture();
                            }
                            catch (Exception ex)
                            {
                                var errMsg = string.Format("The sale was not succesfull. {0}", ex.Message);
                                Logging.LogEvent(errMsg, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                                lblError.Text = errMsg;
                                lblError.Visible = true;
                                return;
                            }
                        }

                        List<Codes> codes = order.GetCodes();

                        if (codes.Count == 0)
                        {
                            lblError.Text = "Error: Node codes found";
                            lblError.Visible = true;
                            return;
                        }

                        if (codes.Count > 0)
                        {
                            order.SendMail();

                            string resultUrl = ConfigSettings.GetText("FinalReceiptPage") + transactionID;
                            try
                            {
                                this.Response.Redirect(resultUrl, false);
                            }
                            catch (Exception ex)
                            {
                                lblError.Text = string.Format("Error forwarding to receipt: {0}. {1}", resultUrl, ex.Message);
                                lblError.Visible = true;
                                Logging.LogEvent(lblError.Text, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                            }
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Page being loaded event handler
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event argument</param>
        protected void Page_Load(object sender, EventArgs e)
        {

            string transactionID = HttpContext.Current.Request["transactionid"];
            string responseCode = HttpContext.Current.Request["responsecode"];
            string debug = HttpContext.Current.Request["debug"];
            bool showImg = (HttpContext.Current.Request["showimg"] == "true");
            
            try
            {
                this.csType = this.GetType();
                LoadData();

                string rawUrl = this.Page.Request.RawUrl;

                if (this.Request.UrlReferrer != null)
                {
                    rawUrl = this.Request.UrlReferrer.AbsoluteUri;
                }

                //string transactionID = match.Groups["id"].Value;
                //string responseCode = match.Groups["code"].Value;

                if (!string.IsNullOrEmpty(transactionID) && !string.IsNullOrEmpty(responseCode))
                {
                    if (responseCode.ToLower() == "ok")
                    {
                        CreateOrder(transactionID, debug);
                    }
                }

            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error: {0}", ex.Message);
                lblError.Visible = true;
                Logging.LogEvent("Error in BuyPostage Page_Load: " + ex.Message, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
            }
        }

        /// <summary>
        /// Loads data product lists 
        /// </summary>
        private void LoadData()
        {
            List<ProductInfo> products = null;
            List<PriceCategory> priceCategories = null;
            List<Prices> priceList = null;

            // Load product and price data for lists
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.ID))
                {
                    string adminWebUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetAdminWebUrl(site.RootWeb);
                    using (SPWeb web = site.OpenWeb(adminWebUrl))
                    {
                        ProductInfo info = new ProductInfo();
                        products = info.GetProductInfos(web);

                        PriceCategory pcat = new PriceCategory();
                        priceCategories = pcat.GetProductCategories(web);

                        Prices prices = new Prices();
                        priceList = prices.GetPrices(web);
                    }
                }
            });

            SortedList<string, string> countries = new SortedList<string, string>();
            List<int> weights = new List<int>();

            this.countrySelector.Width = new Unit(200, UnitType.Pixel);
            this.weightSelector.Width = new Unit(200, UnitType.Pixel);

            StringBuilder scriptBuilder = new StringBuilder();
            scriptBuilder.AppendLine(@"<script type='text/javascript'>");
            scriptBuilder.AppendLine(@"  var priceCategories = new Array();");
            scriptBuilder.AppendLine(@"  var prices = new Array();");
            scriptBuilder.AppendLine(@"  var texts = new Array();");
            scriptBuilder.AppendLine();

            // Create pricecategory JS array
            foreach (PriceCategory pc in priceCategories)
            {
                scriptBuilder.AppendFormat("priceCategories[{0}]=['{1}','{2}','{3}']; {4}", pc.Id, pc.AText, pc.BText, pc.Title, Environment.NewLine);
            }
            scriptBuilder.AppendLine();

            // create prices JS array
            foreach (Prices priceObj in priceList)
            {
                scriptBuilder.AppendFormat("prices.push(['{0}','{1}','{2}','{3}','{4}','{5}','{6}']); {7}",
                                            priceObj.Id,
                                            priceObj.Weight,
                                            priceObj.APrice * 100,
                                            priceObj.BPrice * 100,
                                            priceObj.CategoryId,
                                            priceObj.Category,
                                            priceObj.Title,
                                            Environment.NewLine);
            }
            scriptBuilder.AppendLine();

            // Add countries to drop down
            foreach (ProductInfo info in products)
            {
                string country = info.Country;
                int weight = info.Weight;

                if (!countries.ContainsKey(country))
                {
                    countries.Add(country, country);
                    this.countrySelector.Items.Add(new System.Web.UI.WebControls.ListItem(country, info.PriceCategoryId.ToString()));
                }

                if (!weights.Contains(weight))
                {
                    weights.Add(weight);
                    this.weightSelector.Items.Add(new System.Web.UI.WebControls.ListItem(string.Format("{0} g", weight), weight.ToString()));
                }

                double aprice = info.APrice;
                double bprice = info.BPrice;

                if (aprice < 100)
                {
                    aprice *= 100;
                }
                if (bprice < 100)
                {
                    bprice *= 100;
                }
            }

            // sort countries and select DK as default
            HelperClass.SortListItems(this.countrySelector.Items, false);
            this.countrySelector.Items.FindByText("Danmark").Selected = true;

            string transactionID = HttpContext.Current.Request["transactionid"];
            string responseCode = HttpContext.Current.Request["responsecode"];

            // Create JS for loading existing order
            scriptBuilder.AppendLine("function LoadOrder() {");

            // Load order if we have transaction code and response is nok OK
            if (!string.IsNullOrEmpty(transactionID) &&  !string.IsNullOrEmpty(responseCode) && responseCode.ToLower() != "ok")
            {
                var order = new Orders(transactionID);
                if (order.Id > 0 && order.Status != Orders.OrderStatusValues.Paid)
                {
                    int prodCount = 0;
                    foreach(SubOrders subOrder in order.SubOrders)
                    {
                        prodCount++;
                        scriptBuilder.AppendFormat("    AddNewProduct({0}, '{1}', 0, {2}, '{3}', {4});{5}", subOrder.Amount, subOrder.Destination, subOrder.Weight, subOrder.Priority, subOrder.Price*100, Environment.NewLine);
                    }
                    if (prodCount > 0)
                    {
                        scriptBuilder.AppendLine("  DisplayForm(2);");
                        scriptBuilder.AppendLine("  DisplayStep(2);");

                        scriptBuilder.AppendFormat("  $('#email1').val('{0}');{1}", order.Email, Environment.NewLine);
                        scriptBuilder.AppendFormat("  $('#email2').val('{0}');{1}", order.Email, Environment.NewLine);
                        scriptBuilder.AppendLine("  $('#acceptTerms').prop('checked', true);");
                    }

                    if (order.ImageSize > 0)
                    {
                        scriptBuilder.AppendFormat("   $('#imgFrame').attr('src', '/_layouts/digitalpostage/helpers/showimage.aspx?id={0}'); {1}", transactionID, Environment.NewLine);
                    }
                }
            }
            scriptBuilder.AppendLine("}");



            scriptBuilder.AppendLine("</script>");

            if (!ClientScript.IsClientScriptBlockRegistered(ScriptNames.PostagePrices.ToString()))
            {
                ClientScript.RegisterClientScriptBlock(csType, ScriptNames.PostagePrices.ToString(), scriptBuilder.ToString());
            }
        }
    }
}
