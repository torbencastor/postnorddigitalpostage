﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage
{
    /// <summary>
    /// 
    /// </summary>
    public partial class DebugConfigSettings : LayoutsPageBase
    {

        /// <summary>
        /// Creates the child controls on the <see cref="T:Microsoft.SharePoint.WebControls.UnsecuredLayoutsPageBase" />.
        /// </summary>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void LoadData()
        {
            try
            {
                Hashtable values = null;

                switch (blType.SelectedValue)
                {
                    case "TextSettings":
                        values = Portal.DigitalPostage.Helpers.TextSettings.GetValues();
                        break;
                    case "Terms":
                        values = Portal.DigitalPostage.Helpers.Terms.GetValues();
                        break;
                    case "ConfigSettings":
                    default:
                        values = Portal.DigitalPostage.Helpers.ConfigSettings.GetValues();
                        break;
                }

                SortedList<string, string> lstValuest = new SortedList<string, string>();

                foreach (var key in values.Keys)
                {
                    lstValuest.Add(key.ToString(), values[key].ToString());
                }

                string oldKey = string.Empty;
                List<string> duplicates = new List<string>();
                foreach(string key in lstValuest.Keys)
                {
                    if (key == oldKey)
                    {
                        duplicates.Add(key);
                    }
                    oldKey = key;
                }

                if (duplicates.Count > 0)
                {
                    lblError.Text = "Duplicates found:<br/>";
                }
                foreach(string dup in duplicates)
                {
                    lblError.Text += string.Format("{0}<br/>", dup);
                }

                gvData.Enabled = true;
                gvData.DataSource = lstValuest;
                gvData.DataBind();
                lblResult.Text = string.Empty;

                //foreach (var key in values.Keys)
                //{
                //    TableRow tr = new TableRow();
                //    TableCell td = new TableCell();
                //    td.Text = key.ToString();
                //    tr.Cells.Add(td);
                //    td = new TableCell();
                //    td.Text = values[key].ToString();
                //    tr.Cells.Add(td);
                //    tblAllTranslations.Rows.Add(tr);
                //}
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }


        /// <summary>
        /// Handles the SelectedIndexChanged event of the blType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void blType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        /// <summary>
        /// Handles the Click1 event of the btnOk control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnOk_Click1(object sender, EventArgs e)
        {
            if (txtLookup.Text != string.Empty)
            {
                gvData.Enabled = false;

                if (blType.Items[0].Selected)
                {
                    lblResult.Text = string.Format("<b>Result:</b> {0}", Portal.DigitalPostage.Helpers.ConfigSettings.GetText(txtLookup.Text.Trim()));
                }
                else
                {
                    lblResult.Text = string.Format("<b>Result:</b> {0}", Portal.DigitalPostage.Helpers.TextSettings.GetText(txtLookup.Text.Trim()));
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnClearCache control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnClearCache_Click(object sender, EventArgs e)
        {
            Portal.DigitalPostage.Helpers.TextSettings.ClearCache(SPContext.Current.Web.Language);
            Portal.DigitalPostage.Helpers.ConfigSettings.ClearCache(SPContext.Current.Web.Language);
            Portal.DigitalPostage.Helpers.Terms.ClearCache(SPContext.Current.Web.Language);
            LoadData();
        }
    }
}
