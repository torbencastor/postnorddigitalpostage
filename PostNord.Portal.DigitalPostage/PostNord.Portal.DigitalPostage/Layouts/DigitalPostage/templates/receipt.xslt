﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes"/>
  <xsl:decimal-format name="danish" decimal-separator="," grouping-separator="." />

  <xsl:template match="Orders">

    <html lang="dk" xmlns="http://www.w3.org/1999/xhtml" charset="UTF-8">
      <head>
        <meta charset="utf-8" />
        <title>.</title>
        <link href="styles.css" type="text/css" rel="stylesheet" />
      </head>
      <body style="font-family:Calibri Regular; font-size:10px;" encoding="utf-8">
        <span style="font-size:8px;">Kvittering fra Post Danmark</span>
        <p>
          <table style="width:100%;">
            <tr>
              <td style="width:100px;">
                <img src="http://dev/_layouts/digitalpostage/imgs/postlogo.png" alt="" title=""/>
              </td>
              <td>
                <h1 style="color:#cc0000; font-size:18px; margin-bottom:0px;padding-bottom:2px;font-weight:normal;">Kvittering</h1>
                <span style="font-size:12px; padding-top:0px;margin-top:0px;">Denne e-mail kan ikke besvares, men du kan kontakte kundeservice på 70 10 20 50 </span>
              </td>
            </tr>
          </table>
        </p>

        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:160px; padding:2px 8px 2px 8px;font-weight:bold;">
                      Fakturanummer :
                    </td>
                    <td style="color:#cc0000;padding:2px 8px 2px 8px; margin:0px;">
                      <xsl:value-of select="ReceiptNumber"/>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="padding:10px 10px 40px; 10px">
                      Tak for at du handler på postdanmark.dk
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:400px;padding:2px 40px 2px 8px;font-weight:bold;">
                      Produkt :
                    </td>
                    <td style="text-align:right;font-weight:bold;padding:2px 40px 2px 8px;">
                      Antal
                    </td>
                    <td style="text-align:right;padding:2px 40px 2px 8px; margin:0px;font-weight:bold;">
                      Pris
                    </td>
                  </tr>

                  <xsl:apply-templates select="SubOrders" />

                </table>
              </td>
            </tr>
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;width:130px; padding:10px 40px 2px 10px;font-weight:bold;">
                    </td>
                    <td style="width:25%;text-align:left;">
                      Total :
                    </td>
                    <td style="width:25%;text-align:right;padding:10px 40px 2px 0px; margin:0px;">
                      <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="OrderSum" />
                      </xsl:call-template>
                    </td>
                  </tr>
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;width:130px; padding:10px 40px 2px 10px;font-weight:bold;">
                    </td>
                    <td style="width:25%;text-align:left;">
                      Total eksl. moms :
                    </td>
                    <td style="width:25%;text-align:right;padding:10px 40px 2px 0px; margin:0px;">
                      <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="OrderSum" />
                      </xsl:call-template>
                    </td>
                  </tr>
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;width:130px; padding:10px 40px 2px 10px;font-weight:bold;">
                    </td>
                    <td style="width:25%;text-align:left;">
                      Moms :
                    </td>
                    <td style="width:25%;text-align:right;padding:10px 40px 2px 0px; margin:0px;">
                      DKK 0,00
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <table style="width:100%;border:solid 2px #DBD4C4;border-collapse:collapse;">
            <tr style="background-color:#DBD4C4;">
              <td style="width:50%;width:130px; padding:10px 40px 2px 10px;font-weight:bold;">
              </td>
              <td style="width:25%;text-align:left;">
                At betale :
              </td>
              <td style="width:25%;text-align:right;color:red;padding:10px 40px 2px 0px; margin:0px;">
                <xsl:call-template name="FormatPrice">
                  <xsl:with-param name="Price" select="OrderSum" />
                </xsl:call-template>
              </td>
            </tr>
          </table>
        </p>

        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="width:50%;width:130px; padding:2px 40px 2px 10px;font-weight:bold; background-color:#E4E4E4;">
                Købsinformation
              </td>
            </tr>
            <tr>
              <td style="padding-left:10px;padding-top:10px;">
                <table style="width:100%;">
                  <tr style="border-bottom:solid 2px #c1c1c1;">
                    <td style="width:400px;">
                      Betalingsdato :
                    </td>
                    <td>
                      <xsl:call-template name="FormatDate">
                        <xsl:with-param name="Date" select="SalesDate" />
                      </xsl:call-template>

                    </td>
                  </tr>
                  <tr>
                    <td style="width:400px;padding-top:10px;">
                      Email adresse :
                    </td>
                    <td style="padding-top:10px;">
                      <xsl:value-of select="Email"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="width:400px;padding-top:10px;padding-bottom:10px;i">
                      Transaktions ID :
                    </td>
                    <td style="padding-top:10px;padding-bottom:10px;">
                      <xsl:value-of select="CaptureId"/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </p>

        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="width:50%;width:130px; padding:2px 40px 2px 10px;font-weight:bold; background-color:#E4E4E4;">
                Sådan gør du
              </td>
            </tr>
            <tr>
              <td style="width:50%;width:130px; padding:10px 40px 2px 10px;">
                <span style="color:#cc0000;">Labels</span>
                <ol>
                  <li>Læg ark med labels i din printer</li>
                  <li>Print koderne ( i faktisk størrelse )</li>
                  <li>Sæt en label på konvoluttens øverste højre hjørne</li>
                </ol>
                <span style="color:#cc0000;margin-top:20px;">Eller papir</span>
                <ol>
                  <li>Koderne printes på almindeligt papir</li>
                  <li>Labels klippes ud</li>
                  <li>Labels limes/klistres på konvoluttens øverste højre hjørne</li>
                </ol>
                <span style="color:#cc0000;">Eller håndskrevet</span>
                <ol>
                  <li>Skriv koden direkte på konvoluttens øverste højre hjørne</li>
                </ol>
                Husk at skrive ”B” på konvolutten, hvis du har købt porto til et B-brev og vælger at skrive koderne i hånden.
              </td>
            </tr>
          </table>
        </p>

        <p>
          <br/>
          <span style="font-size:14px;">
            <br/>
            Med venlig hilsen<br/><br/>
          </span>
          <table style="width:100%;">
            <tr>
              <td>
                Post Danmark A/S<br/>
                Tietgensgade 37<br/>
                1566 København V<br/>
                CVR: 26663903
              </td>
            </tr>
            <tr>
              <td>
                IBAN DK2530000001035886<br/>
                SWIFT-BIC DABADKKK
              </td>
            </tr>
          </table>
        </p>
      </body>
    </html>

  </xsl:template>

  <xsl:template match="SubOrders" name="SubOrders">
    <xsl:for-each select="SubOrders">
      <tr style="background-color:#FFFFFF;">
        <td style="width:400px;width:130px;padding:10px 1px 10px 10px;">
          <xsl:value-of select="Destination" />, <xsl:value-of select="Weight" /> g, <xsl:value-of select="Priority"/>
        </td>
        <td style="text-align:right;font-weight:bold;padding:10px 1px 10px 10px;">
          <xsl:value-of select="Amount"/>
        </td>
        <td style="text-align:right;padding:10px 40px 10px 10px;">
          <xsl:call-template name="FormatPrice">
            <xsl:with-param name="Price" select="Price" />
          </xsl:call-template>
        </td>
      </tr>

    </xsl:for-each>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date"/>
    <xsl:value-of select="substring($Date,0,11)"/>
  </xsl:template>

  <xsl:template name="FormatPrice">
    <xsl:param name="Price" />
    DKK <xsl:value-of select='format-number($Price, "#,00", "danish")'/>
  </xsl:template>

</xsl:stylesheet>
