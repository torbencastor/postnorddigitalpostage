﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CintQueue.aspx.cs" Inherits="PostNord.Portal.DigitalPostage.Layouts.DigitalPostage.CintQueue" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

    <style type="text/css">
        .header {
            background-color:#e3e3e3;
        }

        .odd {
            background-color:#f3f3f3;
        }

        .table {
            border : 1px solid #d3d3d3;
        }
    </style>

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div style="padding:15px;">
        <h1>CINT Queue</h1>
        <hr />
            <asp:Label ID="lblInfo" runat="server" Text=""></asp:Label>
        <hr />
    
        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>


            <asp:Repeater ID="rptQueue" runat="server" EnableViewState="true">
                <HeaderTemplate>
                    <table id="refunds" class="table">
                        <tr class="header">
                            <th>Id</th>
                            <th>Receipt No</th>
                            <th>Refund No</th>
                            <th>Added</th>
                            <th>Action</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
                            <td><%# Eval("Id") %></td>
                            <td><%# Eval("OrderId") %></td>
                            <td><%# Eval("RefundId") %></td>
                            <td><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d} {0:t}", DataBinder.Eval(Container.DataItem, "Timestamp"))  %></td>
                            <td><%# Eval("Action") %></td>
                        </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                        <tr class="odd">
                            <td><%# Eval("Id") %></td>
                            <td><%# Eval("OrderId") %></td>
                            <td><%# Eval("RefundId") %></td>
                            <td><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d} {0:t}", DataBinder.Eval(Container.DataItem, "Timestamp"))  %></td>
                            <td><%# Eval("Action") %></td>
                        </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater> 
        <div>
            <asp:DropDownList ID="drpUpdateCount" runat="server">
                <asp:ListItem Selected="True">5</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>25</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="btnFlush" runat="server" Text="Flush Queue" OnClick="btnFlush_Click" />
        </div>
    </div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    CINT Queue
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
    CINT Queue
</asp:Content>
