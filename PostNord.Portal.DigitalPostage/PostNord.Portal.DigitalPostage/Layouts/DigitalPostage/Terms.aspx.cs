﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web;
using System.Xml;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage
{
    public partial class Terms : UnsecuredLayoutsPageBase
    {

        /// <summary>
        /// Gets a value that indicates whether users can access the page without logging in.
        /// </summary>
        /// <returns>true if anonymous access is allowed; otherwise, false. </returns>
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string termcss = string.Empty;

            if (HttpContext.Current.Request["platform"] != null)
            {
                switch (HttpContext.Current.Request["platform"].ToLower())
                {
                    case "ipad":
                        termcss = PostNord.Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TermsIpad");
                        break;  
                    case "wp":
                        termcss = PostNord.Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TermsWp");
                        break;
                    case "android":
                        termcss = PostNord.Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TermsAndroid");
                        break;
                    case "ios":
                    default:
                        termcss = PostNord.Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TermsIos");
                        break;
                }
            } else
            {
                termcss = PostNord.Portal.DigitalPostage.Helpers.ConfigSettings.GetText("TermsIos");
            }

            string geo = "DENMARK";
            if (HttpContext.Current.Request["geo"] != null)
            {
                string getParam = HttpContext.Current.Request["geo"].ToUpper();
                switch(getParam)
                {
                    case "EUROPE" :
                        geo = getParam;
                        break;
                    case "WORLD":
                        geo = getParam;
                        break;
                    case "DENMARK":
                        geo = getParam;
                        break;
                }
            }

            string type = "TERMS";
            if (HttpContext.Current.Request["type"] != null)
            {
                type = HttpContext.Current.Request["type"].ToUpper();
            }

            string html = PostNord.Portal.DigitalPostage.Helpers.Terms.GetText(string.Format("{0}_{1}",type, geo.ToUpper()));
            if (html == string.Empty)
            {
                html = string.Format("<body><p>Terms missing</p><p>Geo: {0}</p><p>Please add: {0}_{1} to Terms list</p></body>", type, geo);
            }

            this.Response.ContentType = "text/html";
            this.Response.Clear();

            this.Response.Write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
            this.Response.Write("<html>");
            this.Response.Write("<head>");
            this.Response.Write("<meta http-equiv='pragma' content='no-cache' />");
            this.Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />");
            this.Response.Write("<title>MOBILPORTO</title>");
            this.Response.Write(string.Format("<style type='text/css' media='screen'>{0}</style>", termcss.Replace("<br/>","")));
            this.Response.Write("</head>");
            this.Response.Write("<body>");
            this.Response.Write(html);
            this.Response.Write("</body>");
            this.Response.End();
            this.Response.Close();
        }
    }
}
