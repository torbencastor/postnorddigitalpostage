﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web.Services;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Data;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage
{



    public partial class LogData : LayoutsPageBase
    {
        private class Counter
        {
            public int Android { get; set; }
            public int IOS { get; set; }
            public int WebShop { get; set; }
            public int WP { get; set; }

            public Counter()
            {
                this.Android = 0;
                this.IOS = 0;
                this.WebShop = 0;
                this.WP = 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }


        /// <summary>
        /// Get Graph data for 1 day.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="hourCount">The hour count. (Not used)</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Error loading data:  + ex.Message</exception>
        [WebMethod]
        public static List<object> GraphDataDay(string date, int hourCount, int status)
        {
            try
            {
                DateTime startTime = DateTime.Parse(date, new CultureInfo("da-DK", false));
                DateTime endTime = startTime.AddDays(1).AddSeconds(-1);

                DataSet ds = Dal.Orders.GetOrderStatusDay(startTime, endTime, status);
                DataTable dt = ds.Tables[0];

                // Create empty dataset
                List<Counter> counterData = new List<Counter>();

                int maxHourCount = 24;

                if (startTime == DateTime.Now.Date)
                {
                    maxHourCount = DateTime.Now.Hour;
                }

                for (int i = 0; i <= maxHourCount; i++)
                {
                    counterData.Add(new Counter());
                }

                // Populate counter dataset
                foreach (DataRow row in dt.Rows)
                {
                    string platform = (string)row["platform"];
                    int hour = (int)row["OnHour"];
                    int count = (int)row["OrderCount"];
                    switch (platform)
                    {
                        case "Android":
                            counterData[hour].Android = count;
                            break;
                        case "IOS":
                            counterData[hour].IOS = count;
                            break;
                        case "WebShop":
                            counterData[hour].WebShop = count;
                            break;
                        case "WP":
                            counterData[hour].WP = count;
                            break;
                    }
                }

                // Create return data, formatted for google chart
                List<object> data = new List<object>();
                data.Add(new[] { "Time", "Android", "IOS", "WebShop", "WP" });
                for (int i = 0; i < counterData.Count; i++)
                {
                    Counter c = counterData[i];
                    data.Add(new object[] { string.Format("{0:D2}", i), c.Android, c.IOS, c.WebShop, c.WP });
                }

                return data;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw new ApplicationException("Error loading data: " + ex.Message, ex);
            }
        }


        /// <summary>
        /// Get Graph data for Days with given status
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="hourCount">The hour count.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Error loading data:  + ex.Message</exception>
        [WebMethod]
        public static List<object> GraphDataDays(string date, int hourCount, int status)
        {
            try
            {
                DateTime startTime = DateTime.Parse(date, new CultureInfo("da-DK", false));
                DateTime endTime = startTime.AddHours(hourCount);

                int dayCount = (int)(endTime - startTime).TotalDays;

                DataSet ds = Dal.Orders.GetOrderStatusDays(startTime, endTime, status);
                DataTable dt = ds.Tables[0];

                // Create empty dataset
                List<Counter> counterData = new List<Counter>();
                for (int i = 0; i < dayCount; i++)
                {
                    counterData.Add(new Counter());
                }

                // Populate counter dataset
                foreach (DataRow row in dt.Rows)
                {
                    string platform = (string)row["platform"];
                    DateTime rowDate = (DateTime)row["ForDate"];
                    int dayNum = (int)(rowDate - startTime).TotalDays;
                    int count = (int)row["OrderCount"];
                    switch (platform)
                    {
                        case "Android":
                            counterData[dayNum].Android = count;
                            break;
                        case "IOS":
                            counterData[dayNum].IOS = count;
                            break;
                        case "WebShop":
                            counterData[dayNum].WebShop = count;
                            break;
                        case "WP":
                            counterData[dayNum].WP = count;
                            break;
                    }
                }

                // Create return data, formatted for google chart
                List<object> data = new List<object>();
                data.Add(new[] { "Time", "Android", "IOS", "WebShop", "WP" });
                for (int i = 0; i < counterData.Count; i++)
                {
                    Counter c = counterData[i];
                    DateTime dataDate = startTime.AddDays(i);
                    data.Add(new object[] { string.Format("{0:dd-MM}", dataDate), c.Android, c.IOS, c.WebShop, c.WP });
                }

                return data;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw new ApplicationException("Error loading data: " + ex.Message, ex);
            }

        }


    }
}
