﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Register Tagprefix="pnc" Namespace="PostNord.Portal.DigitalPostage.WebControls" Assembly="$SharePoint.Project.AssemblyFullName$" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="buypostage.aspx.cs" Inherits="PostNord.Portal.DigitalPostage.Layouts.DigitalPostage.BuyPostage" %>

<!DOCTYPE html>
<html>
<head>
    <base target="_parent" />
    <link rel="stylesheet" href="/Style%20Library/DigitalPostage/css/wizstep.css" />
    <!--[if IE]>
        <link rel="stylesheet" href="/Style%20Library/DigitalPostage/css/wizstepie.css" />
    <![endif]-->

    <title><pnc:GetText runat="server" Key="BUYPOSTAGE_TITLE" Default="" /></title>

    <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <link href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="/Style%20Library/DigitalPostage/js/jquery.selectBox.js" type="text/javascript"></script>
    <script src="/Style%20Library/DigitalPostage/js/json.js" type="text/javascript"></script>
    <link href="/Style%20Library/DigitalPostage/css/jquery.selectBox.css" rel="stylesheet" type="text/css" />
    <link href="/Style%20Library/DigitalPostage/css/style.css" rel="stylesheet" type="text/css" />
    <script src="js/postage.js" type="text/javascript"></script>

    <script type="text/javascript">
        var shoppingCart = new Array();
        $(document).ready(function () {
            PostageInit();
            LoadOrder();
        });
    </script>
</head>
<body>
    <input type="hidden" id="wizMaxStep" />
    <div id="wizardToolBar">
        <div id="wizStep1" class="wizStepLeft wizStepActiveLeft"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_1" Default="" /></span></div>
        <div id="wizStep2" class="wizStep"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_2" Default="" /></span></div>
        <div id="wizStep3" class="wizStep"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_3" Default="" /></span></div>
        <div id="wizStep4" class="wizStep"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_4" Default="" /></span></div>
        <div id="wizStep5" class="wizStep"><span class="wizStepText"><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_4" Default="" /></span></div>
    </div>
    <div id="steps">
        <div id="form1" class="stepsForm">
            <form action="_self" method="post" runat="server" visible="true">
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
                <div class="headline formHeader"><pnc:GetText runat="server" Key="BUYPOSTAGE_STEP_1_HEADLINE" Default="" /></div>
                <div class="infoBlock">
                    <span class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_CHOOSE_COUNTRY" Default="" /></span><br />
                    <asp:DropDownList runat="server" ID="countrySelector"></asp:DropDownList>
                </div>

                <div class="infoBlock">
                    <span class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_CHOOSE_WEIGHT" Default="" /></span><br />
                    <asp:DropDownList runat="server" ID="weightSelector"></asp:DropDownList>
                </div>

                <div class="infoBlock">
                    <span class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_CHOOSE_PRIORITY" Default="" /></span><br />
                    <input type="radio" name="post" title="A Post" id="apost" checked="checked" /><label class="radioBtn" id="radioBtnTextA" for="apost"></label><br />
                    <input type="radio" name="post" title="B Post" id="bpost" /><label class="radioBtn" id="radioBtnTextB" for="bpost"></label><br />
                </div>

                <div class="headline"><pnc:GetText runat="server" Key="BUYPOSTAGE_CHOOSE_AMOUNT" Default="" /></div>
                <table id="shoppingBasket">
                    <tr class="productTitleBar">
                        <th id="productTitle" class="productListingCol1 productTitle"><pnc:GetText runat="server" Key="BUYPOSTAGE_AMOUNT" Default="" /></th>
                        <th id="productAmount" class="productListingCol2 productTitle productAmount"><pnc:GetText runat="server" Key="BUYPOSTAGE_UNIT_PRICE" Default="" /></th>
                        <th id="productPrice" class="productListingCol3 productTitle productAmount"><pnc:GetText runat="server" Key="BUYPOSTAGE_PRICE" Default="" /></th>
                    </tr>
                    <tr id="productListingRow">
                        <td colspan="3" id="productListingBlock">
                            <table id="productListing">
                                <tr class="productTitleBar">
                                    <td class="productListingCol1">
                                        <input type="text" value="0" id="newPostage" /></td>
                                    <td class="productListingCol2 productAmount">
                                        <input type="text" value="" id="unitPrize" /></td>
                                    <td class="productListingCol3 productAmount">
                                        <input type="text" value="" id="unitTotal" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2" id="productsButtons">
                            <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_1_BTN" Default="" />' id="form1Proceed" class="buttonClass" title='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_1_BTN" Default="" />' />
                        </td>
                    </tr>
                </table>
            </form>
        </div>

        <div id="form2" class="stepsForm">
            <div class="headline formHeader"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_2_HEADLINE" Default="" /></div>
            <table id="orderView">
                <tr id="orderHeaderRow">
                    <th><pnc:GetText runat="server" Key="BUYPOSTAGE_SPEED" Default="" /></th>
                    <th class="item"><pnc:GetText runat="server" Key="BUYPOSTAGE_WEIGHT" Default="" /></th>
                    <th class="item"><pnc:GetText runat="server" Key="BUYPOSTAGE_DESTINATION" Default="" /></th>
                    <th class="item"><pnc:GetText runat="server" Key="BUYPOSTAGE_UNIT_PRICE" Default="" /></th>
                    <th class="item"><pnc:GetText runat="server" Key="BUYPOSTAGE_AMOUNT" Default="" /></th>
                    <th class="item"><pnc:GetText runat="server" Key="BUYPOSTAGE_PRICE" Default="" /></th>
                    <th></th>
                </tr>
                <tr id="orderTotalRow">
                    <td class="total"><pnc:GetText runat="server" Key="BUYPOSTAGE_TOTAL" Default="" /></td>
                    <td colspan="4"></td>
                    <td class="item total" id="totalOrderAmount"></td>
                    <td></td>
                </tr>
            </table>
            <table id="orderViewButtons">
                <tr id="orderButtonsRow">
                    <td colspan="7" id="orderButtons" style="text-align: right;">
                        <input type="button" class="buttonClass" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_2_BTN_MORE" Default="" />' id="BuyMore" />
                        <input type="button" class="buttonClass" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_2_BTN_LABEL" Default="" />' id="MakeLabel" />
                        <input type="button" class="buttonClass" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_2_BTN_PAY" Default="" />' id="Go2Payment" />
                    </td>
                </tr>
            </table>

        </div>

        <div id="form3" class="stepsForm">
            <form method="post" action="/_layouts/digitalpostage/helpers/uploadimg.aspx" enctype="multipart/form-data" id="fileForm" target="imgFrame" visible="false">
                <div class="headline formHeader"><pnc:GetText runat="server" Key="BUYPOSTAGE_HEADLINE_LABEL" Default="" /></div>
                <table>
                    <tr>
                        <td id="labelCell">
                            <table id="labelDesign" style="border: solid 1px black;">
                                <tr>
                                    <td>
                                        <iframe id="imgFrame" name="imgFrame" style="border: none; overflow: hidden;" frameborder="0" scrolling="no" src="/_layouts/digitalpostage/helpers/showimage.aspx"></iframe>
                                    </td>
                                    <td id="dummyImg">
                                        <img src="/_layouts/digitalpostage/imgs/labeltext.png" alt="" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td id="textCell">
                            <div class="headline"><pnc:GetText runat="server" Key="BUYPOSTAGE_LAYOUT" Default="" /></div>
                            <br />
                            <pnc:GetText runat="server" Key="BUYPOSTAGE_LAYOUT_DESC" Default="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="width:361px;">
                                <input type="file" id="BrowserHiddenFileField" name="fileProp" class="uploadClass" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_BTN_BROWSE" Default="" />' />
                            </div>
                            <div style="padding-top: 10px;">
                                <input type="submit" name="Upload" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_BTN_UPLOAD" Default="" />' id="uploadBtn" class="buttonClass" />
                                <input type="button" name="ResetImg" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_BTN_RESET" Default="" />' id="resetImg" class="buttonClass" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="form3Buttons">
                            <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_3_BTN_BACK" Default="" />' class="buttonClass" id="labelPrevious" />
                        </td>
                        <td style="text-align: right;" class="form3Buttons">
                            <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_3_BTN_CHOOSE" Default="" />' class="buttonClass" id="labelSelect" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>

        <div id="form4" class="stepsForm">
            <div class="headline formHeader"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_HEADLINE" Default="" /></div>

            <table>
                <tr>
                    <td id="emails">
                        <div id="email1Text" class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_EMAIL_TOP" Default="" /></div>
                        <input type="text" class="email" id="email1" value="" />
                        <div id="email2Text" class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_EMAIL_BOTTOM" Default="" /></div>
                        <input type="text" class="email" id="email2" title="E-mail adresse" value="" />
                        <div id="errorBox"></div>
                    </td>
                    <td>
                        <div class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_INFO_HEADLINE" Default="" /></div>
                        <pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_INFO" Default="" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" id="terms">
                        <input type="checkbox" id="acceptTerms" /><label id="termsText" for="acceptTerms"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_ACCEPT" Default="" /></label>
                            <a href='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_ACCEPT_LINK_URL" Default="" />' title='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_ACCEPT_LINK_TITLE" Default="" />' target="_blank"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_ACCEPT_LINK_TEXT" Default="" /></a>
                    </td>
                </tr>
            </table>
            <table id="buttons">
                <tr>
                    <td>
                        <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_BTN_VIEW_ORDER" Default="" />' class="buttonClass" id="seeOrderBtn" /><input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_BTN_LABEL" Default="" />' class="buttonClass" id="makeLabelBtn" />
                    </td>
                    <td style="text-align: right;">
                        <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_BTN_PAY" Default="" />' class="buttonClass" id="go2PaymentBtn" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
