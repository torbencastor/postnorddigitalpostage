﻿using System;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using PostNord.Portal.DigitalPostage.Helpers;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage.Helpers
{
    /// <summary>
    /// Shows uploaded image
    /// </summary>
    public partial class ShowImage : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.Request["id"]))
                {
                    imgImage.ImageUrl = string.Format("showorderimg.aspx?id={0}", this.Request["id"]);
                    return;
                }
                if (!string.IsNullOrEmpty(this.Request["img"]))
                {
                    imgImage.ImageUrl = this.Request["img"];
                } else
                {
                    string imgUrl = ConfigSettings.GetText("DefaultLabelImage");
                    if (imgUrl != string.Empty)
                    {
                        imgImage.ImageUrl = ConfigSettings.GetText("DefaultLabelImage");
                    }
                    else
                    {
                        imgImage.Visible = false;
                        lblError.Visible = true;
                        lblError.Text = "Default label image not found!";
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = string.Format("Error showing Image: {0}", ex.Message);
            }
        }
    }
}
