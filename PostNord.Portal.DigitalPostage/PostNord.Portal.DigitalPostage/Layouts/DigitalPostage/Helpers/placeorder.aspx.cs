﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Microsoft.SharePoint.Utilities;
using PostNord.Portal.DigitalPostage.Helpers;
using PostNord.Portal.DigitalPostage.Bll;
using System.Linq;
using System.Web;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage.Helpers
{
    /// <summary>
    /// Create Order in DB
    /// </summary>
    public partial class PlaceOrder : UnsecuredLayoutsPageBase
    {

        /// <summary>
        /// Gets a value that indicates whether users can access the page without logging in.
        /// </summary>
        /// <returns>true if anonymous access is allowed; otherwise, false. </returns>
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        private enum ShoppingItemNames
        {
            count,
            destination,
            weight,
            postageType,
            price,
            priceWithoutVAT,
            VAT,
            total,
            categoryId,
            productId
        }

        protected string GetLink()
        {
            string link = string.Empty;

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite siteElevated = new SPSite(SPContext.Current.Site.ID))
                {
                    using (SPWeb web = siteElevated.OpenWeb(siteElevated.RootWeb.ID))
                    {

                        string jsonString = string.Empty;

                        this.Request.InputStream.Position = 0;
                        using (var inputStream = new StreamReader(this.Request.InputStream))
                        {
                            jsonString = inputStream.ReadToEnd();
                        }

                        if (jsonString == string.Empty)
                        {
                            throw new ApplicationException("Input string is empty");
                        }

                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        Dictionary<string, object> data = (Dictionary<string, object>)serializer.DeserializeObject(jsonString);

                        string mailAddress = (string)data["mailAddress"];
                        object[] shoppingCart = (object[])data["shop"];
                        string imageUrl = (string)data["image"];
                        string transactionId = (string)data["transactionId"];

                        if (imageUrl.Contains("?img="))
                        {
                            imageUrl = imageUrl.Substring(imageUrl.IndexOf("?img=") + 5);
                        }
                        else
                        {
                            imageUrl = string.Empty;
                        }

                        DateTime salesDate = DateTime.Now;

                        Bll.Orders order = null;

                        if (transactionId != string.Empty)
                        {
                            order = new Orders(transactionId);
                            order.AddLogDebug(string.Format("Order loadet from transaction id: {0}", transactionId));

                            var paymentInfo = order.GetNetsPaymentInfo();
                            float amountCaptured = (int.Parse(paymentInfo.Summary.AmountCaptured) / 100);
                            float amountCredited = (int.Parse(paymentInfo.Summary.AmountCredited) / 100);
                            order.AddLogDebug(string.Format("NETS Status: Captured: {0} Credited: {1}", amountCaptured, amountCredited));

                            if (amountCaptured == order.OrderSum && amountCredited == 0)
                            {
                                link = string.Format("{0}{1}?transactionId={2}&responseCode=OK", SPContext.Current.Site.Url, Portal.DigitalPostage.Helpers.ConfigSettings.GetText("ReceiptUrl"), transactionId);
                                order.AddLogDebug(string.Format("Order already paid! Redirecting: " + link));
                            }
                        }
                        else
                        {
                            order = new Orders();
                            order.CaptureId = GetTransactionID(mailAddress);
                        }

                        if (link == string.Empty)
                        {
                            order.Email = mailAddress;
                            order.SalesDate = salesDate;
                            order.Save();

                            order.AddLogDebug(string.Format("WebShop Create: {0} ({1})", HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.UserAgent));

                            // Set Payment info if undefined
                            if (order.PaymentMethod == 0)
                            {
                                string webshopName = Orders.WEBSHOP_NAME;
                                string webshopAppId = Orders.WEBSHOP_VERSION.ToString();

                                int appId = 2;
                                int.TryParse(webshopAppId, out appId);

                                order.SetAppInfo(Orders.PaymentMethods.Nets, webshopName, appId);
                            }

                            // If we have an image get it from Img Lib and add it to DB
                            if (imageUrl != string.Empty)
                            {
                                byte[] imgData = null;

                                string imgName = Path.GetFileName(imageUrl);
                                web.AllowUnsafeUpdates = true;
                                SPFolder docLib = web.Folders["Lists"].SubFolders["LabelImages"];

                                SPQuery dQuery = new SPQuery();
                                string QueryString = "<Where>" +
                                                        "<Eq>" +
                                                          "<FieldRef Name=\"FileLeafRef\"/>" +
                                                          "<Value Type=\"Text\">" + imgName + "</Value>" +
                                                        "</Eq>" +
                                                     "</Where>";
                                dQuery.Query = QueryString;
                                SPListItemCollection collListItems = docLib.DocumentLibrary.GetItems(dQuery);

                                if (collListItems.Count > 0)
                                {
                                    SPFile file = collListItems[0].File;
                                    imgData = file.OpenBinary();
                                    // We have got it. Delete it!
                                    file.Delete();
                                }

                                web.AllowUnsafeUpdates = true;

                                if (imgData != null)
                                {
                                    order.SetImage(imgData);
                                }
                            }

                            PriceCategory pcat = new PriceCategory();
                            string adminWebUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetAdminWebUrl(siteElevated.RootWeb);
                            List<PriceCategory> priceCategories = null;
                            using (SPWeb adminWeb = siteElevated.OpenWeb(adminWebUrl))
                            {
                                priceCategories = pcat.GetProductCategories(adminWeb);
                            }

                            // Delete all suborders if any is found. In case this is not the first attampt to pay
                            while (order.SubOrders.Count > 0)
                            {
                                order.SubOrders[0].Delete();
                                order.SubOrders.Remove(order.SubOrders[0]);
                            }

                            foreach (var shoppingItemObject in shoppingCart)
                            {
                                Dictionary<string, object> shoppingItem = (Dictionary<string, object>)shoppingItemObject;

                                string productName = "Ukendt produkt";

                                int categoryId = int.Parse(shoppingItem[ShoppingItemNames.categoryId.ToString()].ToString());
                                PriceCategory priceCat = priceCategories.FirstOrDefault(s => s.Id == categoryId);
                                if (priceCat != null)
                                {
                                    productName = priceCat.Title;
                                }

                                string weightTxt = (string)shoppingItem[ShoppingItemNames.weight.ToString()].ToString();
                                short weight = short.Parse(weightTxt);
                                string priority = (string)shoppingItem[ShoppingItemNames.postageType.ToString()];
                                string destination = (string)shoppingItem[ShoppingItemNames.destination.ToString()];
                                int amount = (int)shoppingItem[ShoppingItemNames.count.ToString()];
                                int productId = int.Parse(shoppingItem[ShoppingItemNames.productId.ToString()].ToString());

                                string priceString = shoppingItem[ShoppingItemNames.price.ToString()].ToString();
                                int priceInt = int.Parse(priceString);
                                double price = priceInt;
                                string sVAT = shoppingItem[ShoppingItemNames.VAT.ToString()].ToString();
                                int VAT = int.Parse(sVAT);
                                double PriceWithoutVAT = (double)((priceInt*100)/VAT)/100; // Remove VAT from price

                                if (price > 100)
                                {
                                    price /= 100;
                                }

                                order.AddSubOrder(priceCat.Title, weight, priority, destination, amount, price, productId, PriceWithoutVAT, VAT);
                            }

                            link = order.CreatePayment();

                        }
                    }
                }
            });

            return link;

        }


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                string link = GetLink();

                this.Response.ContentType = "text/plain";
                this.Response.Clear();
                this.Response.Write(link);
            }
            catch (Exception ex)
            {
                string errMsg = string.Format("Exception creating order: {0}", ex.Message);
                this.Response.Clear();
                this.Response.ContentType = "text/plain";
                this.Response.Write("Der er opstået en teknisk fejl. Prøv venligst igen senere.");
                Logging.LogEvent(errMsg, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
            }
        }

        /// <summary>
        /// Gets the transaction identifier.
        /// </summary>
        /// <param name="mailAddress">The mail address.</param>
        /// <returns></returns>
        private string GetTransactionID(string mailAddress)
        {
            return string.Format("{0}_{1}", mailAddress, DateTime.Now.ToString("yyyyMMddHHmmss")); ;
        }
    }
}
