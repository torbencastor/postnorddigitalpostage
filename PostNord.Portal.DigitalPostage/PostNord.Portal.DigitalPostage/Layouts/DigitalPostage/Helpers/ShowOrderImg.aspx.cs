﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web;
using PostNord.Portal.DigitalPostage.Bll;
using PostNord.Portal.DigitalPostage.Helpers;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage.Helpers
{
    /// <summary>
    /// Show Image from order
    /// </summary>
    public class ShowOrderImg : UnsecuredLayoutsPageBase
    {
        /// <summary>
        /// Gets a value that indicates whether users can access the page without logging in.
        /// </summary>
        /// <returns>true if anonymous access is allowed; otherwise, false. </returns>
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Shows the image.
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        private void ShowImage(string transactionId)
        {
            Orders order = new Orders(transactionId);
            if (order.Id > 0 && order.ImageSize > 0)
            {
                PostNord.Portal.DigitalPostage.Helpers.Image.ShowImage("buypostage.png", order.LabelImage);
            }
            else
            {
                Response.Write("Order not found or no image was found");
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string transactionID = HttpContext.Current.Request["id"];

            try
            {
                if (!string.IsNullOrEmpty(transactionID))
                {
                    ShowImage(transactionID);
                    return;
                } else if (!string.IsNullOrEmpty(HttpContext.Current.Request["img"]))
                {
                    this.Response.Redirect(string.Format("/lists/labelimages/{0}", HttpContext.Current.Request["img"]), true);
                } 
                else
                {
                    this.Response.Redirect(ConfigSettings.GetText("DefaultLabelImage"), true);
                }
            }
            catch (Exception ex)
            {
                Response.Write(string.Format("Error loading Order Image: {0}", ex.Message));
            }
        }
    }
}
