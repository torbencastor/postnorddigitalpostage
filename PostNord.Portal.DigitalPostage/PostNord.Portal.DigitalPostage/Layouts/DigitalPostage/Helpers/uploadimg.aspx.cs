﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage
{
    /// <summary>
    /// Upload Image object - handles image upload.
    /// </summary>
    public class UploadImg : System.Web.UI.Page
    {
        private const long MaxFileSize = 5 * 1024 * 1024;

        private const int MAX_WIDTH = 172;
        private const int MAX_HEIGHT = 172;

        private bool ThumbnailCallback()
        {
            return false;
        }

        /// <summary>
        /// Initializes the <see cref="T:System.Web.UI.HtmlTextWriter" /> object and calls on the child controls of the <see cref="T:System.Web.UI.Page" /> to render.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> that receives the page content.</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            try
            {
                HttpPostedFile file = this.Request.Files[0];

                // Check the submitted file
                if (file != null)
                {
                    string fname = Path.GetFileName(file.FileName);
                    string extension = Path.GetExtension(fname);
                    string fileUrl = "none";
                    fname = GenerateNewFilename(".png").ToLower();

                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        using (SPSite siteElevated = new SPSite(SPContext.Current.Site.ID))
                        {
                            using (SPWeb web = siteElevated.OpenWeb(siteElevated.RootWeb.ID))
                            {
                                web.AllowUnsafeUpdates = true;

                                SPFolder docLib = web.Folders["Lists"].SubFolders["LabelImages"];
                                Image img = null;
                                try
                                {
                                    img = Image.FromStream(file.InputStream);
                                }
                                catch 
                                {
                                    writer.Write("{error: 'Filen er ikke et godkendt format',msg: ''}");
                                    return;
                                }
                                Image.GetThumbnailImageAbort myCallback = new Image.GetThumbnailImageAbort(ThumbnailCallback);
                                Image thumbnail = img.GetThumbnailImage(MAX_WIDTH, MAX_HEIGHT, myCallback, IntPtr.Zero);

                                var stream = new System.IO.MemoryStream();
                                MemoryStream ms = new MemoryStream();

                                ImageFormat format = ImageFormat.Jpeg;
                                switch (extension)
                                {
                                    case ".png":
                                        format = ImageFormat.Png;
                                        break;
                                    case ".gif":
                                        format = ImageFormat.Gif;
                                        break;
                                }

                                thumbnail.Save(ms, ImageFormat.Png);
                                byte[] ImageByte = ms.ToArray();

                                SPFile savedFile = docLib.Files.Add(fname, ImageByte, true);

                                fileUrl = string.Format("/_layouts/DigitalPostage/Helpers/ShowOrderImg.aspx?img={0}", savedFile.Name);

                                web.AllowUnsafeUpdates = false;

                                writer.Write("{" + string.Format("error: '',msg: '{0}'", fileUrl) + "}");
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                writer.Write("{error: 'Ukendt fejl: " + ex.Message +"',msg: ''}");
            }
        }

        /// <summary>
        /// Validates the submitted file based on type and size.
        /// </summary>
        /// <param name="file">Submitted file.</param>
        /// <returns>'true' if the file lives up to requirements</returns>
        private bool IsValidContent(HttpPostedFile file)
        {
            bool result = true;
            Regex reg = new Regex("image/(x-png)|(pjpeg)|(gif)|(png)|(jpeg)");

            result &= (file.ContentLength > 0 && file.ContentLength < MaxFileSize);

            result &= reg.IsMatch(file.ContentType);

            return result;
        }

        private string GenerateNewFilename(string extension)
        {
            Guid guid = Guid.NewGuid();
            string enc = Convert.ToBase64String(guid.ToByteArray());
            enc = enc.Replace("/", "_");
            enc = enc.Replace("+", "-");
            return enc.Substring(0, 22) + extension;
        }
    }
}
