﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web;
using PostNord.Portal.DigitalPostage;

namespace PostNord.Portal.DigitalPostage.Layouts
{
    public partial class GetStatus : LayoutsPageBase
    {

        private string NetsStatus(long id)
        {

            try
            {
                var order = new Bll.Orders(id);
                var paymentInfo = order.GetNetsStatus();

                if (paymentInfo.Summary.Authorized)
                {
                    return "OK";
                }
                else
                {
                    return "Not Paid";
                }
            }
            catch (Exception ex)
            {
                return string.Format("Nets status error. Id: {0} - {1}", id, ex.Message);
            }
        }

        private string CintCodeStatus(long id)
        {
            try
            {
                var order = new Bll.Orders(id);
                var codes = order.GetCodes();
                if (codes.Count > 0)
                {
                    return "OK";
                }
                return "No codes returned";
            }
            catch (Exception ex)
            {
                return string.Format("CINT Get Code error. Id: {0} - {1}", id, ex.Message);
            }
        }

        private string CintOrderStatus(long id)
        {
            string status = "OK";

            return status;
        }

        private string MobilePayStatus(long id)
        {
            try
            {
                var order = new Bll.Orders(id);
                var mpStatus = order.CheckMobilePay();

                if (mpStatus == "OK")
                {
                    return "OK";
                }
                else
                {
                    return string.Format("MobilePay not paid: Id: {0} - Status: {1}", id, mpStatus);
                }
            }
            catch (Exception ex)
            {
                return string.Format("MobilePay status error: Id: {0} - {1}", id, ex.Message);
            }
        }

        private long GetOrderId(int status, int paymentType)
        {
            //int status = Bll.Orders.OrderStatusValues.Paid as int;
            return  Dal.Orders.GetLastOrder(status, paymentType);
        }
        
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string status = string.Empty;
            long orderId = 0;

            if (HttpContext.Current.Request["id"] != null)
            {
                long.TryParse(HttpContext.Current.Request["id"], out orderId);
            }

            if (HttpContext.Current.Request["component"] != null)
            {
                switch (HttpContext.Current.Request["component"].ToLower())
                {
                    case "nets":
                        if (orderId == 0)
                        {
                            orderId = GetOrderId((int)Bll.Orders.OrderStatusValues.Paid, (int)Bll.Orders.PaymentMethods.Nets);
                        }
                        if (orderId < 1)
                        {
                            status = string.Format("No orders found with status: {0} Method:{1}", (int)Bll.Orders.OrderStatusValues.Paid, (int)Bll.Orders.PaymentMethods.Nets);
                        }
                        else
                        {
                            status = NetsStatus(orderId);
                        }
                        break;
                    case "cintcode":
                        if (orderId == 0)
                        {
                            orderId = GetOrderId((int)Bll.Orders.OrderStatusValues.Paid, (int)Bll.Orders.PaymentMethods.Nets);
                        }
                        if (orderId < 1)
                        {
                            status = string.Format("No orders found with status: {0} Method:{1}", (int)Bll.Orders.OrderStatusValues.Paid, (int)Bll.Orders.PaymentMethods.Nets);
                        }
                        else
                        {
                            status = CintCodeStatus(orderId);
                        }
                        break;
                    case "cintorder":
                        if (orderId == 0)
                        {
                            orderId = GetOrderId((int)Bll.Orders.OrderStatusValues.Paid, (int)Bll.Orders.PaymentMethods.Nets);
                        }
                        if (orderId < 1)
                        {
                            status = string.Format("No orders found with status: {0} Method:{1}", (int)Bll.Orders.OrderStatusValues.Paid, (int)Bll.Orders.PaymentMethods.Nets);
                        }
                        else
                        {
                            status = CintOrderStatus(orderId);
                        }
                        break;
                    case "mp":
                        if (orderId == 0)
                        {
                            orderId = GetOrderId((int)Bll.Orders.OrderStatusValues.Paid, (int)Bll.Orders.PaymentMethods.MobilePay);
                        }
                        if (orderId < 1)
                        {
                            status = string.Format("No orders found with status: {0} Method:{1}", (int)Bll.Orders.OrderStatusValues.Paid, (int)Bll.Orders.PaymentMethods.MobilePay);
                        }
                        else
                        {
                            status = MobilePayStatus(orderId);
                        }
                        break;
                    default:
                        break;
                }
            } 
            else  if (HttpContext.Current.Request["status"] != null)
            {
                int statusVal = (int) Bll.Orders.OrderStatusValues.Paid;
                int paymentType = (int)Bll.Orders.OrderStatusValues.Paid;
                int.TryParse(status, out statusVal);
                if (HttpContext.Current.Request["type"] != null)
                {
                    int.TryParse(HttpContext.Current.Request["type"], out paymentType);
                }
                status = GetOrderId(statusVal, paymentType).ToString();
            }
            else  
            {
                int statusVal = (int)Bll.Orders.OrderStatusValues.Paid;
                int paymentType = (int)Bll.Orders.OrderStatusValues.Paid;
                status = GetOrderId(statusVal, paymentType).ToString();
            }

            this.Response.ContentType = "text/html";
            this.Response.Clear();
            this.Response.Write(status);
            this.Response.End();
            this.Response.Close();
        }
    }
}
