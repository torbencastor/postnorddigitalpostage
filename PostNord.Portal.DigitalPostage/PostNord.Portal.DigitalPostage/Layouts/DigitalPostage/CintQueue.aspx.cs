﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Collections.Generic;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage
{
    public partial class CintQueue : LayoutsPageBase
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowQueue();
        }

        /// <summary>
        /// Shows the queue.
        /// </summary>
        protected void ShowQueue()
        {
            try
            {
                List<Bll.OrderQueue> queue = Bll.OrderQueue.GetOrderQueue();
                lblInfo.Text = string.Format("Number of queue entries: {0}", queue.Count);
                btnFlush.Enabled = (queue.Count > 0);
                rptQueue.DataSource = queue;
                rptQueue.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Load Error: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnFlush control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnFlush_Click(object sender, EventArgs e)
        {
            try
            {
                int queueLength = 5;

                int.TryParse(drpUpdateCount.SelectedValue, out queueLength);
                Bll.OrderQueue.FlushQueue(queueLength);
                ShowQueue();

            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Flush Error: {0}", ex.Message);
            }

        }
    }
}
