﻿using iTextSharp.text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using PostNord.Portal.DigitalPostage.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage
{
    /// <summary>
    /// Receipt Page
    /// </summary>
    public partial class Receipt : UnsecuredLayoutsPageBase
    {

        private Bll.Orders _order = null;

        /// <summary>
        /// Gets the transaction identifier.
        /// </summary>
        /// <value>
        /// The transaction identifier.
        /// </value>
        private string TransactionId {
            get
            {
                return  HttpContext.Current.Request["transactionId"];

            }
        }

        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        private Bll.Orders Order
        {
            get { 
                if (_order == null)
                {
                    _order = new Bll.Orders(this.TransactionId);
                }
                
                return _order; 
            }
        }
        


        /// <summary>
        /// Gets a value that indicates whether users can access the page without logging in.
        /// </summary>
        /// <returns>true if anonymous access is allowed; otherwise, false. </returns>
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.ApplicationException"></exception>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.TransactionId) || this.TransactionId.Length != 32)
            {
                lblError.Text = "Necessary information not found!";
                lblError.Visible = true;
                return;
            }
                
            try
            {
                if (!Page.IsPostBack)
                {
                    btnPrintReceipt.Text = Portal.DigitalPostage.Helpers.TextSettings.GetText("BUYPOSTAGE_RECEIPT_PRINT_RECEIPT");
                    btnPrintLabels.Text = Portal.DigitalPostage.Helpers.TextSettings.GetText("BUYPOSTAGE_RECEIPT_PRINT_LABELS");

                    string template = ConfigSettings.GetText("ReceiptMailTemplate");
                    if (template == string.Empty)
                    {
                        lblError.Text = "Template not found!";
                        lblError.Visible = true;
                        return;
                    }

                    if (this.Order.Id == 0)
                    {
                        lblError.Text = "Order not found!";
                        lblError.Visible = true;
                        return;
                    }

                    string receiptTemplateUrl = SPUtility.ConcatUrls(SPContext.Current.Site.Url, ConfigSettings.GetText("ReceiptMailTemplate"));

                    string html = this.Order.GetHtml(receiptTemplateUrl);

                    // Remove XML + html headers
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(html);

                    // Remove xml namespaces
                    xmlDoc = Portal.DigitalPostage.Helpers.HelperClass.RemoveXmlns(xmlDoc);
                    XmlNodeList bodys = xmlDoc.GetElementsByTagName("body");
                    if (bodys.Count > 0)
                    {
                        html = bodys[0].InnerXml;
                    }
                    else
                    {
                        html = string.Format("<div style='color:red'>Error parsing XML: {0}</div>", receiptTemplateUrl);
                    }
                    this.receiptForm.Controls.Add(new LiteralControl(html));
                }

            }
            catch (Exception ex)
            {
                lblError.Text  = string.Format("Error loading contents: {0}", ex.Message);
                lblError.Visible = true;
                Logging.LogEvent(lblError.Text, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnPrintReceipt control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnPrintReceipt_Click(object sender, EventArgs e)
        {
            try
            {
                string receiptTemplateUrl = SPUtility.ConcatUrls(SPContext.Current.Site.Url, ConfigSettings.GetText("ReceiptPdfTemplate"));
                string html = this.Order.GetHtml(receiptTemplateUrl);

                byte[] pdfReceipt = Portal.DigitalPostage.Helpers.Pdf.CreatePdf(html, PageSize.A4, 10f, 10f, 10f, 0f);
                string PdfFilename = string.Format("{0}.pdf", this.Order.CaptureId);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", PdfFilename));
                using (MemoryStream ms = new MemoryStream(pdfReceipt))
                {
                    ms.WriteTo(HttpContext.Current.Response.OutputStream);
                    ms.Close();
                }
                try
                {
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
                catch { }

            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error showing Receipt. {0}", ex.Message);
                lblError.Visible = true;
                Logging.LogEvent(lblError.Text, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnPrintLabels control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnPrintLabels_Click(object sender, EventArgs e)
        {
            try
            {
                string labelTemplateUrl = SPUtility.ConcatUrls(SPContext.Current.Site.Url, ConfigSettings.GetText("LabelTemplate"));

                byte[] pdfLabels = this.Order.GenerateLabels(labelTemplateUrl);
                string PdfFilename = string.Format("Postage_{0}.pdf", this.Order.CaptureId);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", PdfFilename));
                using (MemoryStream ms = new MemoryStream(pdfLabels))
                {
                    ms.WriteTo(HttpContext.Current.Response.OutputStream);
                    ms.Close();
                }

                try
                {
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
                catch { }
            
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error showing labels. {0}", ex.Message);
                lblError.Visible = true;
                Logging.LogEvent(lblError.Text, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                
            }
        }
    }
}
