﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web.UI;

namespace PostNord.Portal.DigitalPostage.Layouts.DigitalPostage
{
    /// <summary>
    /// TEST PAGE
    /// </summary>
    public partial class TestCodeGen : UnsecuredLayoutsPageBase
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Gets a value that indicates whether users can access the page without logging in.
        /// </summary>
        /// <returns>true if anonymous access is allowed; otherwise, false. </returns>
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Sends server control content to the specified <see cref="T:System.Web.UI." /><see cref="HtmlTextWriter" /> object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI." /><see cref="HtmlTextWriter" /> object that receives the rendered content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            writer.Write("<h1>CodeGen TEST</h1>");
            try{
                //writer.Write("Version : {0}", Portal.CodeWrapper.HelloWorld());
                writer.Write("Version : {0}", Portal.CodeWrapper.GetVersion());

                writer.Write("<h1>CodeGen version: {0} </h1>", Portal.CodeWrapper.GetVersion());

                uint inpDay = 183;
                uint inpValue = 100;
                uint inpNumber = 400000;

                string code = Portal.CodeWrapper.GetCode(inpDay, inpValue, inpNumber);
                writer.Write("Input - Day: {0} Value:{1} Number:{2}<br/>", inpDay, inpValue, inpNumber);
                writer.Write("Code: {0}<br/>", code);

                uint day;
                uint val;
                uint number;

                Portal.CodeWrapper.GetCodeDetails(code, out day, out val, out number);
                writer.Write("Output - Day: {0} Value:{1} Number:{2}<br/>", day, val, number);

                writer.Write("<h2>Generating 10 codes</h2>");

                string[] codes = Portal.CodeWrapper.GetCodes(inpDay, inpValue, number, 10);
                int c = 0;
                foreach (string s in codes)
                {
                    c++;
                    writer.Write("{0} {1}<br/>", c, s);
                }

                double price = 1;

                writer.Write("<h2>Code Id Test</h2>");
                writer.Write("Current Code Id : {0}<br/>", Portal.DigitalPostage.Dal.NextCode.GetCurrentCode(price));
                writer.Write("Get 10 Codes: {0}<br/>", Portal.DigitalPostage.Dal.NextCode.GetNextCode(price, 10));
                writer.Write("New Code Id : {0}<br/>", Portal.DigitalPostage.Dal.NextCode.GetCurrentCode(price));

                writer.Write("<h2>Writing in ULS</h2>");
                try
                {
                    PostNord.Portal.DigitalPostage.Helpers.Logging.LogEvent("Test logging", Portal.DigitalPostage.Helpers.Logging.TraceSeverityLevel.Medium, Portal.DigitalPostage.Helpers.Logging.EventSeverityLevel.Error);
                }
                catch (Exception ex)
                {
                    writer.Write("Error logging : {0}", ex.Message);
                }
                writer.Write("<p>Done!</p>");


            }
            catch (Exception ex)
            {
                writer.Write("Error: {0}", ex);
            }
        
        
        }
    }
}
