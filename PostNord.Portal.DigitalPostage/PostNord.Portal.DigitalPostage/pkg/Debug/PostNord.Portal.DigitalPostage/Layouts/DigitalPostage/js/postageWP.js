﻿var edits = {};

var allOptions = new Array();
var allCountryOptions = new Array();

function PostageInit() {

    if ($('#form4 .error').first().html().trim() == '') {
        $('#form4 .error').first().hide();
    }

    var uploader = $("#fileUploader");
    var startPage = 1;

    DisplayForm(startPage);
    FilterWeights();
    UpdatePrize();

    $("#newPostage").spinner({
        min: 0,
        max: maxNumberOfItems,
        stop: function (e, ui) {
            UpdatePrize();
        }
    }).val(1);

    $("input[type='button']").button();
    $("input[type='submit']").button();
    $('[id$="countrySelector"]').change(function () {


        var categoryId = $('[id$="countrySelector"]').val();

        FilterWeights();
        UpdatePrize();
    });
    $('[id$="weightSelector"]').change(function () {
        //FilterRadios();
        UpdatePrize();
    });
    $("#apost").change(function () {
        FilterWeights();
        UpdatePrize();
    });
    $("#bpost").change(function () {
        FilterWeights();
        UpdatePrize();
    });

    $("#form1Proceed").click(function () {
        if (!ValidateForm(1)) {
            return;
        }
        if (shoppingCart.length > 0) {
            DisplayForm(2);
        }
    });

    $("#BuyMore").click(function () {
        DisplayForm(1);
    })

    $("#MakeLabel").click(function () {

        if (shoppingCart.length > 0) {
            DisplayForm(3);
        }
    });

    $("#uploadBtn").click(function () {
        if ($("#BrowserHiddenFileField")[0].value.length > 0) {
            $("#fileDiv").removeClass("errorBorder");
            DisableInput("#uploadBtn");
            DisableInput("#resetImg");
            ajaxFileUpload();
            return false;
        }
        else {
            $("#fileDiv").addClass("errorBorder");
            showError(3, errorSelectImageFile);
            return false;
        }
    });

    $("#resetImg").click(function () {
        $($("#BrowserHiddenFileField").parent()).removeClass("errorBorder");
        $("#img").attr("src", defaultImage);
        $("#defaultLayoutText").show();
        $("#selectedLayoutText").hide();
        hideError(3);
    });

    $("#Go2Payment").click(function () {
        EnableControls(4)
        DisplayForm(4);
    });

    $("#labelPrevious").click(function () {
        DisplayForm(2);
    });

    $("#labelSelect").click(function () {
        DisplayForm(4);
    });

    $("#seeOrderBtn").click(function () {
        DisplayForm(2);
    });

    $("#makeLabelBtn").click(function () {
        DisplayForm(3);
    });

    $("#acceptTerms").change(function () {
        ValidateTermsCheck();
    });

    $("#go2PaymentBtn").click(function () {

        if (!ValidateEmail("#email1") || !CompareEmails()) {
            return;
        }

        if ($("#go2PaymentBtn")[0].Disabled) {
            return;
        }
        if (!EnablePayment()) {
            if (!ValidateForm(4)) {
                return;
            }
        }
        DisableInput("#go2PaymentBtn");

        var imgDisplayUrl = $("#img").attr('src');
        var email = $("#email1").val();
        var transactionId = '';
        if ($.getUrlVar('transactionId')) {
            transactionId = $.getUrlVar('transactionId');
        }

        var data = JSON.stringify(new OrderData(email, shoppingCart, imgDisplayUrl, transactionId));

        $.ajax({
            type: "POST",
            url: "/_layouts/DigitalPostage/Helpers/placeorder.aspx",
            data: data,
            dataType: "json",
            success: function (msg) {
                alert('OK: ' + msg);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 200) {
                    var response = jqXHR.responseText;
                    if (response.indexOf('failed') > 0) {
                        alert(response);
                        return;
                    }
                    if (response.indexOf('http') > -1) {
                        document.location.href = response;
                    } else {
                        $("#form4error").text(response);
                        $("#form4error").show();
                        EnableInput("#go2PaymentBtn");
                    }
                }
            }
        });
    });

    if ($.getUrlVar('debug')) {
        var email = $.getUrlVar('debug');
        $("#email1").val(email);
        $("#email2").val(email);
        $('#acceptTerms').prop('checked', true);
        $('#newPostage').val('5');
        UpdatePrize();
    }

    $('a.info').click(function (event) {
        var tooltip = '#' + $(this).attr('tooltip');

        if ($(tooltip).attr('shown') == 'true') {
            $(tooltip).hide();
            $(tooltip).attr('shown', 'false');
        } else {
            $('.ui-tooltip').hide();
            $(tooltip).show();
            $(tooltip).attr('shown', 'true');
        }
        event.stopPropagation();
    });

    $('html').click(function () {
        $('.ui-tooltip').hide();
        $('.ui-tooltip').attr('shown', 'false');
    });

    $('#newPostage').focus();

    if ($('#dialog-message').length) {
        $('#dialog-message').dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog('close');
                }
            },
            position: {
                my: 'center',
                at: 'center',
                of: $('#steps')
            }
        });
    }
}


function FilterRadios() {
    var categoryId = $('[id$="countrySelector"]').val();
    var category = priceCategories[categoryId];
    var weight = $('[id$="weightSelector"]').val();

    var productId = GetProductId(categoryId, weight, "A");
    var postagePrize = GetPrice(productId, "A");
    if (postagePrize == 0) {
        $("#divRadioA").hide();
    } else {
        $("#divRadioA").show();
    }

    postagePrize = GetPrice(productId, "B");
    if (postagePrize == 0) {
        $("#divRadioB").hide();
    } else {
        $("#divRadioB").show();
    }
}

function FilterWeights() {
    var weight = $('[id$="weightSelector"]').val();
    var postageType = $("#apost").is(":checked") ? "A" : "B";
    var categoryId = $('[id$="countrySelector"]').val();
    var category = priceCategories[categoryId];
    var unselected = false;

    if (allOptions.length == 0) {
        $("[id$='weightSelector'] > option").each(function () {
            allOptions.push(this);
        });
    }

    var selectedValue = Number($("[id$='weightSelector']").val());
    $("[id$='weightSelector']").empty();

    for (var i = 0; i < allOptions.length; i++) {
        var productId = GetProductId(categoryId, allOptions[i].value, postageType);
        var postagePrize = GetPrice(productId, postageType);
        if (postagePrize != 0) {
            if (Number(allOptions[i].value) >= selectedValue && selectedValue > 0) {
                selectedValue = -1;
                $("[id$='weightSelector']").append('<option selected ="selected" value="' + allOptions[i].value + '">' + allOptions[i].text + '</option>');
            } else {
                $("[id$='weightSelector']").append('<option value="' + allOptions[i].value + '">' + allOptions[i].text + '</option>');
            }
        }
    }


    // Update country list
    // Only show Denmark if product is A Post
    if (allCountryOptions.length == 0) {
        $("[id$='countrySelector'] > option").each(function () {
            allCountryOptions.push(this);
        });
    }


    var selectedValue = Number($("[id$='countrySelector']").val());
    var selectedText = $("[id$='countrySelector'] option:selected").text();
    $("[id$='countrySelector']").empty();


    for (var i = 0; i < allCountryOptions.length; i++) {
        if (postageType == "A") {
            if (allCountryOptions[i].text == "Danmark") {
                selectedValue = -1;
                $("[id$='countrySelector']").append('<option selected ="selected" value="' + allCountryOptions[i].value + '">' + allCountryOptions[i].text + '</option>');
            }
        } else {
            if (allCountryOptions[i].text == selectedText) {
              selectedValue = -1;
                $("[id$='countrySelector']").append('<option selected ="selected" value="' + allCountryOptions[i].value + '">' + allCountryOptions[i].text + '</option>');
            } else {
                $("[id$='countrySelector']").append('<option value="' + allCountryOptions[i].value + '">' + allCountryOptions[i].text + '</option>');
            }

        }


    }


}

function UpdatePrize() {
    try {
        var categoryId = $('[id$="countrySelector"]').val();
        var category = priceCategories[categoryId];

        $('#radioBtnTextA').html(category[0]);
        $('#radioBtnTextB').html(category[1]);

        var weight = $('[id$="weightSelector"]').val();
        var postageType = $("#apost").is(":checked") ? "A" : "B";

        var productId = GetProductId(categoryId, weight, postageType);
        var postagePrize = GetPrice(productId, postageType);


        $("#unitPrize").val(GetDisplayPrice(postagePrize));
        $("#productId").val(productId);

        var count = 0;

        // do we have a value?
        if ($("#newPostage").val() != '') {
            count = $("#newPostage").val();
        }

        // Is it a number?
        if (!isValidAmount(count)) {
            $("#unitTotal").val('0');
            $("#newPostage").val(cleanNumber(count));
            UpdatePrize();
            return;
        } else {
            count = Number(count);
        }

        // Do we have leading zeros, then clear them
        if (count.toString() != $("#newPostage").val()) {
            $("#newPostage").val(count);
        }

        // Do we have to many numbers
        if (count.toString().length > maxNumberOfItems.toString().length) {
            $("#newPostage").val(count.toString().substring(0, maxNumberOfItems.toString().length));
            count = $("#newPostage").val();
        }

        // Are we below zero?
        if (count < 0) {
            count = 0;
            $("#newPostage").val(count);
        }

        var total = count * postagePrize;

        $("#unitTotal").val(GetDisplayPrice(total));
    }
    catch (e) {
        alert('Update Price exception: ' + e.message);
    }
}

function AddNewProduct(count, destination, categoryId, weight, postageType, postagePrize, productId, postagePrizeWithoutVAT, VAT) {
    var total = count * postagePrize;
    var date = new Date();
    var id = date.getTime() + shoppingCart.length;
    var countryDrpId = $('[id$="countrySelector"]').attr('id');

    if (categoryId == 0) {
        var drp = document.getElementById(countryDrpId);
        for (var i = 0; i < drp.options.length; i++) {
            if (drp.options[i].text == destination) {
                categoryId = drp.options[i].value;
                break;
            }
        }
    }

    $("#newPostage").val(1);

    shoppingCart.push(new PostageItem(count, destination, weight, postageType, postagePrize, id, total, categoryId, productId, postagePrizeWithoutVAT, VAT));
    DisplayCart();
    UpdateTotalPrice();
}

function UpdateOrderLine(id) {
    var item = null;
    var index = -1;

    for (var n = 0; n < shoppingCart.length; n++) {
        if (shoppingCart[n].no == id) {
            item = shoppingCart[n];
            index = n;
            break;
        }
    }

    if (index > -1) {
        var total = item.price * $("#countEditor" + id).val();
        $("#lineTotal" + id).val(GetDisplayPrice(total));
        shoppingCart[index].count = total / item.price;
        shoppingCart[index].total = total;
        UpdateTotalPriceWithOutUpdatingCart();
    }
}

function GetProductId(categoryId, weight) {
    if (prices == null) {
        alert('Prices not found');
        return 0;
    }

    for (var i = 0; i < prices.length; i++) {
        if (prices[i][4] == categoryId && weight == prices[i][1]) {
            return prices[i][0]; // Get ProductId
        }
    }
}

function GetPrice(productId, postageType) {
    if (prices == null) {
        alert('Prices not found');
        return 0;
    }

    for (var i = 0; i < prices.length; i++) {
        if (prices[i][0] == productId) {
            switch (postageType) {
                case 'A':
                    return prices[i][2];
                    break;
                case 'B':
                    return prices[i][3];
                    break;
                default:
                    alert('Error: unkown postagetype:' + postageType);
                    break;
            }
        }
    }
}

function GetVAT(productId) {
    if (prices == null) {
        alert('Prices not found');
        return 0;
    }

    for (var i = 0; i < prices.length; i++) {
        if (prices[i][0] == productId) {
            return prices[i][7];
        }
    }
}


function isValidAmount(n) {
    return parseFloat(n) == parseInt(n, 10) && !isNaN(n);
} // 6 characters

function cleanNumber(inp) {
    output = '';
    for (var i = 0; i < inp.length; i++) {
        if (inp[i] >= '0' && inp[i] <= '9') {
            output += inp[i];
        }
    }
    return output;
}

function AddProduct() {
    var count = parseInt($("#newPostage").val());
    var categoryId = $('[id$="countrySelector"]').val();
    var weight = $('[id$="weightSelector"]').val();
    var postageType = $("#apost").is(":checked") ? "A" : "B";
    var destination = $('[id$="countrySelector"]').find("option:selected").text();
    var productId = $("#productId").val();
    var postagePrize = GetPrice(productId, postageType);
    var postagePrizeWithoutVAT = GetPrice(productId, postageType);
    var VAT = GetVAT(productId);
    if (postagePrize > 0) {
        AddNewProduct(count, destination, categoryId, weight, postageType, postagePrize, productId, postagePrizeWithoutVAT, VAT);
    } else {
        alert('Prisen kan ikke være 0 kr.');
    }
}

function GetDisplayPrice(price) {
    var kroner = Math.floor(price / 100);
    var rest = price - (kroner * 100);
    var unitPrice = rest;
    rest = ("00" + rest).slice(-2);

    var text = valutaCode + " " + kroner + "," + rest;
    return text;
}

function DisplayStep(step) {
    $('.nav li').removeClass('active');
    $(".nav li:eq(" + (step - 1) + ")").addClass('active');
}

function ResetInputErrors(id) {
    errorLabels = $(".errorLabel")
    for (var i = 0; i < errorLabels.length; i++) {
        removeStar("#" + errorLabels[i].id);
        $(errorLabels[i]).removeClass("erroLabel");
    }
    $(".errorBorder").removeClass("errorBorder");
    $("#errorBox").html("");
    $("#errorBox").hide();
}

function ValidateForm(id) {
    //add product
    if (id == 1) {
        var count = $("#newPostage").val();

        if (count > maxNumberOfItems) {
            addStar("#productTitle");
            showError(1, errorTooManyItems.replace('{0}', maxNumberOfItems));
            $("#productTitle").addClass("errorLabel");
            $(($("#newPostage")[0]).parentNode).addClass("errorBorder");
            return false;
        }
        else if (count > 0 || shoppingCart.length > 0) {
            hideError(1);
            if (count > 0) {
                AddProduct();
            }
            return true;
        }
        else {
            addStar("#productTitle");
            showError(1, errorSelectNumber);
            $("#productTitle").addClass("errorLabel");
            $(($("#newPostage")[0]).parentNode).addClass("errorBorder");
            return false;
        }
    }

    //go to payment
    if (id == 4) {
        if (ValidateEmail("#email1") && ValidateEmail("#email2") && EnablePayment()) {
            return true;
        }
        else {
            return false;
        }
    }
}

function EnableControls(id) {
    if (id == 3) {
        EnableInput("#uploadBtn");
        EnableInput("#resetImg");
        EnableInput("#BrowserHiddenFileField");
    }
    if (id == 4) {
        EnableInput("#go2PaymentBtn");
    }
}

function DisableInput(id) {
    var ctrl = $(id)[0]
    if (ctrl != null && ctrl.nodeName.toLowerCase() == "input") {
        ctrl.disabled = true;
        $(ctrl).addClass("input-disabled");
        $("#" + ctrl.id + "-loading").show();
    }
}

function EnableInput(id) {
    var ctrl = $(id)[0]
    if (ctrl != null && ctrl.nodeName.toLowerCase() == "input") {
        ctrl.disabled = false;
        $(ctrl).removeClass("input-disabled");
        $("#" + ctrl.id + "-loading").hide();
    }
}

function DisplayForm(id) {
    ResetInputErrors();
    if (id == 2) {
        UpdatePrize();
        DisplayCart();
    }
    if (id == 3) {
        EnableControls(3)
    }
    if (id == 4) {
        EnableControls(4)
    }
    $(".stepsForm").hide();
    $("#form" + id).show();

    if (id == 1) {
        $('#newPostage').focus();
    }

    DisplayStep(id);
}

function addStar(id) {
    var node = $(id);
    var txt = String(node.html());
    if (txt == 'undefined') {
        txt = '';
    }
    if (txt.indexOf(" *") == -1) {
        node.html(txt.concat(" *"));
    }
}

function removeStar(id) {
    var txt = String($(id).html());
    if (txt == 'undefined') {
        txt = '';
    }
    var idx = txt.indexOf(" *");
    if (idx > -1) {
        $(id).html(txt.substring(0, idx));
    }
}

function showError(id, error) {
    var divId = '#form' + id + 'error';
    $(divId).html(error);
    $(divId).show();
}

function hideError(id) {
    var divId = '#form' + id + 'error';
    $(divId).html('');
    $(divId).hide();
}

function ValidateEmail(id) {
    var email = $(id).val();
    if (email == '') {
        $(id).focus();
        $(id).removeClass("errorBorder");
        $(id).addClass("errorBorder");

        showError(4, errorSelectEmail);
        return false;
    }
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
        $(id).focus();
        $(id).removeClass("errorBorder");
        $(id).addClass("errorBorder");
        showError(4, errorEmailNotValid);
        return false;
    }

    hideError(4);
    $(id).removeClass("errorBorder");

    return true;
}

function CompareEmails() {
    var emailsMatch = true;

    var emailAdr1 = $("#email1").val();

    if (emailAdr1 != "") {
        emailAdr1 = emailAdr1.toLowerCase();
    }
    else {
        enabled = false;
        addStar("#email1Text");
        showError(4, errorSelectEmail);
        $("#email1").addClass("errorBorder");
        return false;
    }

    var emailAdr2 = $("#email2").val();

    if (emailAdr2 != "") {
        emailAdr2 = emailAdr2.toLowerCase();
    }
    else {
        enabled = false;
        showError(4, errorSelectEmail);
        addStar("#email2Text");
        $("#email2").addClass("errorBorder");
        return false;
    }
    var emailsMatch = (emailAdr1 == emailAdr2);

    if (emailsMatch && emailAdr1 != "") {
        hideError(4);
        removeStar("#email1Text");
        removeStar("#email2Text");
        $("#email1").removeClass("errorBorder");
        $("#email2").removeClass("errorBorder");
    }
    else {
        addStar("#email1Text");
        addStar("#email2Text");
        $("#email1").addClass("errorBorder");
        $("#email2").addClass("errorBorder");
        showError(4, errorEmailsDontCompare);
        return false;
    }

    return emailsMatch;
}

function ValidateTermsCheck() {
    var termsChecked = $("#acceptTerms").is(":checked")

    if (!termsChecked) {
        addStar("#termsError");
        $('#terms').addClass("errorBorder");
        showError(4, errorAcceptTerms);
        enabled = false;
    }
    else {
        removeStar("#termsError");
        hideError(4);
        $('#terms').removeClass("errorBorder")
    }

    return termsChecked
}

function RemoveProduct(id) {
    if (confirm(deleteConfirmMessage)) {
        for (var i = 0; i < shoppingCart.length; i++) {
            if (shoppingCart[i].no === id) {
                shoppingCart.splice(i, 1);
                i--;
            }
        }
        DisplayCart();
        UpdateTotalPrice();
    }
}

function UpdateTotalPrice() {
    UpdateTotalPriceWithOutUpdatingCart();
    //DisplayCart();
}

function UpdateTotalPriceWithOutUpdatingCart() {
    var totalPrice = 0;
    var totalCount = 0;

    if (shoppingCart.length > 0) {
        for (var i = 0; i < shoppingCart.length; i++) {
            var count = shoppingCart[i].count;
            var price = shoppingCart[i].price;
            totalPrice += (count * price);
            totalCount += count;
        }
    }
    $("#totalOrderAmount").text(GetDisplayPrice(totalPrice));
    if (totalCount > maxNumberOfItems) {
        $("#form2error").text("Du kan maks. købe " + maxNumberOfItems + " portokoder på en ordre.");
        $("#form2error").show();
        DisableInput('#Go2Payment');
        DisableInput('#MakeLabel');
    } else {
        $("#form2error").hide();
        EnableInput('#Go2Payment');
        EnableInput('#MakeLabel');
    }
}

function EnableEditing(id) {
    DisplayCart();

    if (!edits[id]) {
        edits = {};
        edits[id] = true;
    } else {
        edits = {};
        return;
    }

    $("#editCount" + id).attr("style", "visibility:visible");
    $("#count" + id).attr("style", "visibility:hidden");
    $("#countEditor" + id).spinner({
        min: 1,
        max: maxNumberOfItems,
        stop: function (e, ui) {
            UpdateOrderLine(id);
        }
    });
}

function DisplayCart() {

    $(".orderLine").remove();

    for (var i = 0; i < shoppingCart.length; i++) {
        var item = shoppingCart[i];

        var category = priceCategories[item.categoryId];
        var postageType = '';
        if (item.postageType == 'A') {
            postageType = category[0];
        } else {
            postageType = category[1];
        }

        var tr = '<tr class="orderLine">' +
            '<td class="item">' + item.destination + '</td>' +
            '<td class="item">' + item.weight + '</td>' +
            '<td class="item">' + postageType + '</td>' +
            '<td class="item nobreak">' + GetDisplayPrice(item.price) + '</td>' +
            '<td class="item"><input type="text" value="' + item.count + '" id="countEditor' + item.no + '" class="countField" style="border:none; background-color:White;" disabled="disabled" /></td>' +
            '<td class="item"><input type="text" value="' + GetDisplayPrice(item.total) + '" id="lineTotal' + item.no + '" style="border:none; background-color:White;" disabled="disabled" /></td>' +
            '<td class="toolIcons">' +
                    '<img src="/_layouts/digitalpostage/imgs/edit.png" alt="Ret antal" title="Ret antal" onclick="EnableEditing(' + item.no + ');" style="cursor: pointer; cursor: hand;" />' +
                    '<img src="/_layouts/digitalpostage/imgs/delete.png" alt="Slet ordre linie" title="Slet ordre linie" onclick="RemoveProduct(' + item.no + ');" style="cursor: pointer; cursor: hand;" />' +
            '</td>' +
        '</tr>';
        $('#orderTotalRow').before(tr);
    }

    // if basket is empty send user to first screen
    if (shoppingCart.length == 0) {
        DisplayForm(1);
        hideError(1);
    }
}

function PostageItem(count, country, weight, postageType, postagePrize, id, total, categoryId, productId, postagePrizeWithoutVAT, VAT) {
    this.count = count;
    this.destination = country;
    this.weight = weight;
    this.postageType = postageType;
    this.price = postagePrize;
    this.priceWithoutVAT = postagePrizeWithoutVAT;
    this.VAT = VAT;
    this.price = postagePrize;
    this.no = id;
    this.total = total;
    this.categoryId = categoryId;
    this.productId = productId;
}

function OrderData(email, cart, imageFile, transactionId) {
    this.mailAddress = email;
    this.shop = cart;
    this.image = imageFile;
    this.transactionId = transactionId;
}

function EnablePayment() {
    var enabled = true;

    var emailsAreAlike = CompareEmails();
    var termsChecked = ValidateTermsCheck();
    enabled &= termsChecked && emailsAreAlike;
    return enabled;
}

$.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});

function ajaxFileUpload() {
    $("#loading")
    .ajaxStart(function () {
        $(this).show();
    })
    .ajaxComplete(function () {
        $(this).hide();
    });

    $.ajaxFileUpload
    (
        {
            url: '/_layouts/DigitalPostage/Helpers/UploadImg.aspx',
            secureuri: false,
            async: true,
            contentType: "application/json; charset=utf-8",
            fileElementId: 'BrowserHiddenFileField',
            dataType: 'json',
            data: { name: 'logan', id: 'id' },
            success: function (data, status) {
                if (typeof (data.error) != 'undefined') {
                    if (data.error != '') {
                        EnableControls(3);
                        $("#fileDiv").addClass("errorBorder");
                        $("#defaultLayoutText").show();
                        $("#selectedLayoutText").hide();
                        showError(3, data.error);
                    } else {
                        hideError(3);
                        $("#fileDiv").removeClass("errorBorder");
                        $("#img").attr("src", data.msg);
                        $("#img").show();
                        $("#defaultLayoutText").hide();
                        $("#selectedLayoutText").show();
                        EnableControls(3);
                    }
                }
            },
            error: function (data, status, e) {
                showError(3, data.error + ', ' + e.message);
            }
        }
    )
    return false;
}
