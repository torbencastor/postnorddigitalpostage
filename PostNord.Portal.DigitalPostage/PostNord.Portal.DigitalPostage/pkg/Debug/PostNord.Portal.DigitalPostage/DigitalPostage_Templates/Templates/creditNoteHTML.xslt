﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes"/>
  <xsl:decimal-format name="danish" decimal-separator="," grouping-separator="." />

  <xsl:template match="Refunds">

    <html lang="dk" xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"  />
        <style type="text/css">
          body,p,b,td,li,span {
          font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif;
          font-size:14px;
          }
        </style>
      </head>
      <body style="">
        <p>*[TEMPLATE_CREDITNOTE_INTRO]* </p>
        <p>
          <table>
            <tr>
              <td>
                <b>*[TEMPLATE_CREDITNOTE_NUMBER]* :</b>
              </td>
              <td style="color:#cc0000; padding-left:20px;">
                <xsl:value-of select="Id"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>*[TEMPLATE_CREDITNOTE_AMOUNT]* :</b>
              </td>
              <td style="padding-left:20px;">
                <xsl:call-template name="FormatPrice">
                  <xsl:with-param name="Price" select="CreditSum" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td>
                <b>*[TEMPLATE_CREDITNOTE_TIMESTAMP]* :</b>
              </td>
              <td style="padding-left:20px;">
                <xsl:call-template name="FormatDate">
                  <xsl:with-param name="Date" select="Timestamp" />
                </xsl:call-template>
              </td>
            </tr>
          </table>
        </p>

        <p>*[TEMPLATE_CREDITNOTE_OUTRO]*</p>
        <p>
          <br/>
          <span style="font-size:14px;">
            <br/>
            <br/>
            <br/>
            <br/>
            *[TEMPLATE_INVOICE_BESTREGARDS]*
            <br/>
            <br/>
          </span>
        </p>
        <p>
          *[TEMPLATE_INVOICE_SENDER_LINE1]*<br/>
          *[TEMPLATE_INVOICE_SENDER_LINE2]*<br/>
          *[TEMPLATE_INVOICE_SENDER_LINE3]*<br/>
          *[TEMPLATE_INVOICE_SENDER_LINE4]*</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date"/>

    <xsl:value-of select="concat(
                      substring($Date, 9, 2),
                      '-',
                      substring($Date, 6, 2),
                      '-',
                      substring($Date, 1, 4)
                      )"/>
  </xsl:template>

  <xsl:template name="FormatPrice">
    <xsl:param name="Price" />
    DKK <xsl:value-of select='format-number($Price, "#,00", "danish")'/>
  </xsl:template>


</xsl:stylesheet>
