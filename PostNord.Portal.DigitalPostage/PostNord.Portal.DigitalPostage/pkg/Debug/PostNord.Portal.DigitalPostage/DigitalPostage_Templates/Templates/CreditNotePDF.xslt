﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes"/>
  <xsl:decimal-format name="danish" decimal-separator="," grouping-separator="." />

  <xsl:template match="Refunds">

    <html lang="dk" xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"  />
        <style type="text/css">
          body,p,b,td,li,span {
          font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif;
          font-size:14px;
          }
        </style>
      </head>
      <body style="font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif; font-size:14px;">
        <p>
          <table style="width:100%;">
            <tr>
              <td style="width:100px;">
                <img src="*[TEMPLATE_INVOICE_LOGO]*" alt="" title=""/>
              </td>
              <td>
                <h1 style="color:#cc0000; font-size:18px; margin-bottom:0px;padding-bottom:2px;font-weight:normal;">
                  *[TEMPLATE_CREDITNOTE_HEADER]* 
                </h1>
              </td>
            </tr>
          </table>
        </p>
        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:160px; padding:2px 8px 2px 8px;font-weight:bold;">
                      *[TEMPLATE_CREDITNOTE_NUMBER]*
                    </td>
                    <td style="color:#cc0000;padding:2px 8px 2px 8px; margin:0px;">
                      <xsl:value-of select="Id"/>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="padding:10px 10px 40px 10px">
                      *[TEMPLATE_CREDITNOTE_INTRO]*
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;padding:2px 40px 2px 8px;font-weight:bold;">*[TEMPLATE_INVOICE_HEADER_PRODUCT]*</td>
                    <td style="width:16%;text-align:right;font-weight:bold;padding:2px 0px 2px 8px;">*[TEMPLATE_INVOICE_HEADER_COUNT]*</td>
                    <td style="width:16%;text-align:right;font-weight:bold;padding:2px 0px 2px 8px;">*[TEMPLATE_INVOICE_HEADER_ITEM_PRICE]*</td>
                      <td style="width:16%;text-align:right;font-weight:bold;padding:2px 0px 2px 8px;">Heraf moms</td>
                      <td style="width:16%;text-align:right;font-weight:bold;padding:2px 0px 2px 8px;">Pris eksl. moms</td>
                    <td style="width:16%;text-align:right;padding:2px 40px 2px 8px; margin:0px;font-weight:bold;">*[TEMPLATE_INVOICE_HEADER_PRICE]*</td>
                  </tr>

                  <xsl:apply-templates select="RefundDetails" />

                </table>
              </td>
            </tr>
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">

                    <xsl:apply-templates select="RefundDetails" mode="mySubOrders"/>

                </table>
              </td>
            </tr>
          </table>
          <table style="width:100%;border:solid 2px #DBD4C4;border-collapse:collapse;">
            <tr style="background-color:#DBD4C4;">
              <td style="width:50%;padding:10px 40px 2px 10px;font-weight:bold;">
              </td>
              <td style="width:25%;text-align:left;">*[TEMPLATE_INVOICE_TOTAL_AMOUNT_DUE]* :</td>
              <td style="width:25%;text-align:right;color:red;padding:10px 40px 2px 0px; margin:0px;">
                <xsl:call-template name="FormatPrice">
                  <xsl:with-param name="Price" select="CreditSum" />
                </xsl:call-template>
              </td>
            </tr>
          </table>
        </p>
        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="width:50%;width:130px; padding:2px 40px 2px 10px;font-weight:bold; background-color:#E4E4E4;">*[TEMPLATE_INVOICE_PURCHASE_HEADER]*</td>
            </tr>
            <tr>
              <td style="padding-left:10px;padding-top:10px;">
                <table style="width:100%;">
                  <tr style="border-bottom:solid 2px #c1c1c1;">
                    <td style="padding-top:10px;">*[TEMPLATE_INVOICE_ORDER_NR]*</td>
                    <td style="padding-top:10px;">
                      <xsl:value-of select="OrderNr"/>
                    </td>
                  </tr>
                  <tr style="border-bottom:solid 2px #c1c1c1;">
                    <td style="padding-top:10px;">
                      *[TEMPLATE_CREDITNOTE_TIMESTAMP]*
                    </td>
                    <td style="padding-top:10px;">
                      <xsl:call-template name="FormatDate">
                        <xsl:with-param name="Date" select="Timestamp" />
                      </xsl:call-template>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:10px;">*[TEMPLATE_INVOICE_PURCHASE_EMAIL]*</td>
                    <td style="padding-top:10px;">
                      <xsl:value-of select="Email"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:10px;">
                      *[TEMPLATE_CREDITNOTE_NAME]*
                    </td>
                    <td style="padding-top:10px;">
                      <xsl:value-of select="Name"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:10px;">
                      *[TEMPLATE_CREDITNOTE_ADDRESS]*
                    </td>
                    <td style="padding-top:10px;">
                      <xsl:value-of select="Address"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:10px;">
                      *[TEMPLATE_CREDITNOTE_POSTAGE]*
                    </td>
                    <td style="padding-top:10px;">
                        <xsl:value-of select="PostNumber"/>
                        &#160;
                        <xsl:value-of select="City"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:10px;padding-bottom:10px;">
                      *[TEMPLATE_CREDITNOTE_PHONE]*
                    </td>
                    <td style="padding-top:10px;padding-bottom:10px;">
                      <xsl:value-of select="Phone"/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </p>

        <p>
          <br/>
          <span style="font-size:14px;">
            *[TEMPLATE_CREDITNOTE_OUTRO]*
            <br/>
            <br/>
            <br/>
            <br/>
            *[TEMPLATE_INVOICE_BESTREGARDS]*<br/><br/>
          </span>
          <table style="width:100%;">
            <tr>
              <td>
                *[TEMPLATE_INVOICE_SENDER_LINE1]*<br/>
                *[TEMPLATE_INVOICE_SENDER_LINE2]*<br/>
                *[TEMPLATE_INVOICE_SENDER_LINE3]*<br/>
                *[TEMPLATE_INVOICE_SENDER_LINE4]*
              </td>
            </tr>
          </table>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="RefundDetails" name="RefundDetails">
    <xsl:for-each select="RefundDetails">
      <tr style="background-color:#FFFFFF;">
        <td style="width:400px;width:130px;padding:10px 1px 10px 10px;" width="25%">
          <xsl:value-of select="DestinationInfo" />
        </td>
        <td style="text-align:right;padding:10px 1px 10px 10px;" width="10%">
          <xsl:value-of select="Amount"/>
        </td>
        <td style="text-align:right;padding:10px 1px 10px 10px;" width="15%">
          <xsl:call-template name="FormatPrice">
            <xsl:with-param name="Price" select="Price"/>
          </xsl:call-template>
        </td>
          <!--Heraf moms.-->
          <td style="text-align:right;padding:5px 1px 5px 10px;" width="15%">
              <xsl:call-template name="FormatPrice">
                  <xsl:with-param name="Price" select="SubOrderSumVAT"/>
              </xsl:call-template>
          </td>
          <!--Pris eksl. moms-->
          <td style="text-align:right;padding:5px 1px 5px 10px;" width="15%">
              <xsl:call-template name="FormatPrice">
                  <xsl:with-param name="Price" select="SubOrderSumWithoutVAT"/>
              </xsl:call-template>
          </td>
        <td style="text-align:right;padding:10px 40px 10px 10px;" width="20%">
          <xsl:call-template name="FormatPrice">
            <xsl:with-param name="Price" select="Sum" />
          </xsl:call-template>
        </td>
      </tr>
    </xsl:for-each>
  </xsl:template>








    <xsl:key name="TotalSubOrders" match="RefundDetails" use="VAT"/>
    <xsl:template match="RefundDetails" mode="mySubOrders">
        <xsl:for-each select="RefundDetails[generate-id()=generate-id(key('TotalSubOrders',VAT)[1])]">
            <xsl:sort select="VAT" order="descending" />

            <xsl:if test="VAT != 100">
                <tr style="background-color:#e0e0e0;">
                    <td style="width:650px;padding:5px 40px 2px 10px;font-weight:bold;"></td>
                    <td style="width:70%;text-align:left;">Total Momspligtig - Quickbreve :</td>
                    <td style="width:30%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                        <xsl:call-template name="FormatPrice">
                            <xsl:with-param name="Price" select="sum(key('TotalSubOrders',VAT)/SubOrderSum)" />
                        </xsl:call-template>
                    </td>

                </tr>
                <tr style="background-color:#e0e0e0;color:#666">
                    <td style="width:650px;padding:5px 40px 2px 10px;font-weight:bold;"></td>
                    <td style="width:70%;text-align:left;">*[TEMPLATE_INVOICE_TOTAL_AMOUNT_EX_VAT]* :</td>
                    <td style="width:30%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                        <xsl:call-template name="FormatPrice">
                            <xsl:with-param name="Price" select="sum(key('TotalSubOrders',VAT)/SubOrderSumWithoutVAT)" />
                        </xsl:call-template>
                    </td>

                </tr>
                <tr style="background-color:#e0e0e0;color:#666">
                    <td style="width:650px;padding:5px 40px 2px 10px;font-weight:bold;"></td>
                    <td style="width:70%;text-align:left;">
                        *[TEMPLATE_INVOICE_TOTAL_AMOUNT_VAT]*
                        (<xsl:call-template name="FormatPct">
                            <xsl:with-param name="pct" select="key('TotalSubOrders',VAT)/VAT" />
                        </xsl:call-template>)
                        :
                    </td>
                    <td style="width:30%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                        <xsl:call-template name="FormatPrice">
                            <xsl:with-param name="Price" select="sum(key('TotalSubOrders',VAT)/SubOrderSumVAT)" />
                        </xsl:call-template>
                    </td>

                </tr>
            </xsl:if>

            <xsl:if test="VAT = 100">
                <tr style="background-color:#e0e0e0;">
                    <td style="width:650px;padding:5px 40px 2px 10px;font-weight:bold;"></td>
                    <td style="width:70%;text-align:left;">Total Momsfri - Breve :</td>
                    <td style="width:30%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                        <xsl:call-template name="FormatPrice">
                            <xsl:with-param name="Price" select="sum(key('TotalSubOrders',VAT)/SubOrderSum)" />
                        </xsl:call-template>
                    </td>

                </tr>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>






    <xsl:template name="FormatDate">
    <xsl:param name="Date"/>

    <xsl:value-of select="concat(
                      substring($Date, 9, 2),
                      '-',
                      substring($Date, 6, 2),
                      '-',
                      substring($Date, 1, 4)
                      )"/>
  </xsl:template>

    <xsl:template name="FormatPrice">
        <xsl:param name="Price" />
        <xsl:if test="$Price != 0">
            DKK <xsl:value-of select='format-number($Price, "#,00", "danish")'/>
        </xsl:if>
        <xsl:if test="$Price = 0">
            DKK 0,00
        </xsl:if>
    </xsl:template>
    

    <xsl:template name="FormatPct">
        <xsl:param name="pct" /><xsl:value-of select='$pct - 100'/>%
    </xsl:template>


</xsl:stylesheet>
