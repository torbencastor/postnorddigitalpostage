﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes"/>
  <xsl:decimal-format name="danish" decimal-separator="," grouping-separator="." />

  <xsl:template match="Orders">

    <html lang="dk" xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"  />
        <style type="text/css">
          body,p,b,td,li,span {
          font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif;
          font-size:14px;
          }
        </style>
      </head>
      <body style="font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif; font-size:14px;">
        <span style="font-size:12px;">*[TEMPLATE_APP_INVOICE_HEADER]*</span>
        <p>
          <table style="width:100%;">
            <tr>
              <td style="width:100px;">
                <img src="*[TEMPLATE_APP_INVOICE_LOGO]*" alt="" title=""/>
              </td>
              <td>
                <h1 style="color:#cc0000; font-size:18px; margin-bottom:0px;padding-bottom:2px;font-weight:normal;">*[TEMPLATE_APP_INVOICE_HEADLINE]*</h1>
              </td>
            </tr>
          </table>
        </p>
        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:160px; padding:2px 8px 2px 8px;font-weight:bold;">
                      *[TEMPLATE_APP_INVOICE_NO]*
                    </td>
                    <td style="color:#cc0000;padding:2px 8px 2px 8px; margin:0px;">
                      <xsl:value-of select="ReceiptNumber"/>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="padding:5px 10px 8px 10px">
                      *[TEMPLATE_APP_INVOICE_GREETING]*
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;padding:2px 40px 2px 8px;font-weight:bold;">*[TEMPLATE_APP_INVOICE_HEADER_PRODUCT]*</td>
                    <td style="width:16%;text-align:right;font-weight:bold;padding:2px 0px 2px 8px;">*[TEMPLATE_APP_INVOICE_HEADER_COUNT]*</td>
                    <td style="width:16%;text-align:right;font-weight:bold;padding:2px 0px 2px 8px;">*[TEMPLATE_APP_INVOICE_HEADER_ITEM_PRICE]*</td>
                    <td style="width:16%;text-align:right;padding:2px 40px 2px 8px; margin:0px;font-weight:bold;">*[TEMPLATE_APP_INVOICE_HEADER_PRICE]*</td>
                  </tr>

                  <xsl:apply-templates select="SubOrders" />

                </table>
              </td>
            </tr>
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;padding:5px 40px 2px 10px;font-weight:bold;">
                    </td>
                    <td style="width:25%;text-align:left;">*[TEMPLATE_APP_INVOICE_TOTAL_AMOUNT]* :</td>
                    <td style="width:25%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                      <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="OrderSum" />
                      </xsl:call-template>
                    </td>
                  </tr>
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;width:130px; padding:5px 40px 2px 10px;font-weight:bold;">
                    </td>
                    <td style="width:25%;text-align:left;">*[TEMPLATE_APP_INVOICE_TOTAL_AMOUNT_EX_VAT]* :</td>
                    <td style="width:25%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                      <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="OrderSum" />
                      </xsl:call-template>
                    </td>
                  </tr>
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;width:130px; padding:5px 40px 2px 10px;font-weight:bold;">
                    </td>
                    <td style="width:25%;text-align:left;">*[TEMPLATE_APP_INVOICE_TOTAL_AMOUNT_VAT]* :</td>
                    <td style="width:25%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">DKK 0,00</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <table style="width:100%;border:solid 2px #DBD4C4;border-collapse:collapse;">
            <tr style="background-color:#DBD4C4;">
              <td style="width:50%;padding:5px 40px 2px 10px;font-weight:bold;">
              </td>
              <td style="width:25%;text-align:left;">*[TEMPLATE_APP_INVOICE_TOTAL_AMOUNT_DUE]* :</td>
              <td style="width:25%;text-align:right;color:red;padding:5px 40px 2px 0px; margin:0px;">
                <xsl:call-template name="FormatPrice">
                  <xsl:with-param name="Price" select="OrderSum" />
                </xsl:call-template>
              </td>
            </tr>
          </table>
        </p>
        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="width:50%;width:130px; padding:2px 40px 2px 10px;font-weight:bold; background-color:#E4E4E4;">*[TEMPLATE_APP_INVOICE_PURCHASE_HEADER]*</td>
            </tr>
            <tr>
              <td style="padding-left:10px;padding-top:5px;">
                <table style="width:100%;">
                  <tr style="border-bottom:solid 2px #c1c1c1;">
                    <td style="padding-top:5px;">*[TEMPLATE_APP_INVOICE_PURCHASE_DATE]*</td>
                    <td style="padding-top:5px;">
                      <xsl:call-template name="FormatDate">
                        <xsl:with-param name="Date" select="SalesDate" />
                      </xsl:call-template>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:5px;">*[TEMPLATE_APP_INVOICE_PURCHASE_EMAIL]*</td>
                    <td style="padding-top:5px;">
                      <xsl:value-of select="Email"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:5px;">*[TEMPLATE_APP_INVOICE_PURCHASE_TRANSACTIONID]*</td>
                    <td style="padding-top:5px;">
                      <xsl:value-of select="CaptureId"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:5px;padding-bottom:5px;">
                      *[TEMPLATE_APP_INVOICE_PAYMENT_METHOD]*
                    </td>
                    <td style="padding-top:5px;padding-bottom:5px;">
                      <xsl:choose>
                        <xsl:when test="Payment='MobilePay'">
                          MobilePay
                        </xsl:when>
                        <xsl:otherwise>
                          Betalingskort
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:8px;padding-bottom:5px;" colspan="2">*[TEMPLATE_APP_INVOICE_EXPIRATION]*</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </p>
        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="padding:2px 40px 2px 10px;font-weight:bold; background-color:#E4E4E4;">*[TEMPLATE_APP_INVOICE_HOWTO_HEADER]*</td>
            </tr>
            <tr>
              <td style="padding:5px 40px 2px 10px;">
                <p style="margin-top:3px">*[TEMPLATE_APP_INVOICE_HOWTO_1]*</p>
                <p style="margin-top:3px">*[TEMPLATE_APP_INVOICE_HOWTO_2]*</p>
                <p style="margin-top:3px">*[TEMPLATE_APP_INVOICE_HOWTO_EPILOG]*</p>
                <p style="margin-top:3px">*[TEMPLATE_APP_INVOICE_CONTACT]*</p>
                <p style="margin-top:3px">*[TEMPLATE_APP_INVOICE_TERMS]*</p>
              </td>
            </tr>
          </table>
        </p>
        <p style="padding:2px">
          <span style="font-size:14px;">
            *[TEMPLATE_APP_INVOICE_BESTREGARDS]*<br/>
          </span>
          <table style="width:100%;">
            <tr>
              <td>
                *[TEMPLATE_APP_INVOICE_SENDER_LINE1]*<br/>
                *[TEMPLATE_APP_INVOICE_SENDER_LINE2]*<br/>
                *[TEMPLATE_APP_INVOICE_SENDER_LINE3]*<br/>
                *[TEMPLATE_APP_INVOICE_SENDER_LINE4]*
              </td>
            </tr>
          </table>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="SubOrders" name="SubOrders">
    <xsl:for-each select="SubOrders">
      <tr style="background-color:#FFFFFF;">
        <td style="width:400px;width:130px;padding:5px 1px 5px 10px;">
          <xsl:value-of select="DestinationInfo" />
        </td>
        <td style="text-align:right;padding:5px 1px 5px 10px;">
          <xsl:value-of select="Amount"/>
        </td>
        <td style="text-align:right;padding:5px 1px 5px 10px;">
          <xsl:call-template name="FormatPrice">
            <xsl:with-param name="Price" select="Price"/>
          </xsl:call-template>
        </td>
        <td style="text-align:right;padding:5px 40px 5px 10px;">
          <xsl:call-template name="FormatPrice">
            <xsl:with-param name="Price" select="SubOrderSum" />
          </xsl:call-template>
        </td>
      </tr>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date"/>

    <xsl:value-of select="concat(
                      substring($Date, 9, 2),
                      '-',
                      substring($Date, 6, 2),
                      '-',
                      substring($Date, 1, 4)
                      )"/>
  </xsl:template>

  <xsl:template name="FormatPrice">
    <xsl:param name="Price" />
    DKK <xsl:value-of select='format-number($Price, "#,00", "danish")'/>
  </xsl:template>

</xsl:stylesheet>
