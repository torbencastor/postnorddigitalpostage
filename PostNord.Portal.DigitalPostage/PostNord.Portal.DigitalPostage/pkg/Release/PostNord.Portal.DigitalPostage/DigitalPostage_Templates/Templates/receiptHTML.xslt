﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="yes"/>
    <xsl:decimal-format name="danish" decimal-separator="," grouping-separator="." />

    <xsl:template match="Orders">

        <html lang="dk" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"  />
                <style type="text/css">
                    body,p,b,td,li,span {
                    font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif;
                    font-size:14px;
                    }
                </style>
            </head>
            <body style="">
                <p>*[TEMPLATE_INVOICE_INTRO]*</p>
                <p>
                    <table>
                        <tr>
                            <td>
                                <b>*[TEMPLATE_INVOICE_ORDER_NR]* :</b>
                            </td>
                            <td style="color:#0099bb; padding-left:20px;">
                                <xsl:value-of select="ReceiptNumber"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>*[TEMPLATE_INVOICE_TOTAL_AMOUNT_DUE]* :</b>
                            </td>
                            <td style="padding-left:20px;">
                                <xsl:call-template name="FormatPrice">
                                    <xsl:with-param name="Price" select="OrderSum" />
                                </xsl:call-template>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>*[TEMPLATE_INVOICE_PURCHASE_DATE]* :</b>
                            </td>
                            <td style="padding-left:20px;">
                                <xsl:call-template name="FormatDate">
                                    <xsl:with-param name="Date" select="SalesDate" />
                                </xsl:call-template>
                            </td>
                        </tr>
                    </table>
                </p>
                <p>*[TEMPLATE_INVOICE_USAGE_DESC]*</p>
                <ol>
                    <li>*[TEMPLATE_INVOICE_HOWTO_LABELS_STEP1]*</li>
                    <li>*[TEMPLATE_INVOICE_HOWTO_LABELS_STEP2]*</li>
                    <li>*[TEMPLATE_INVOICE_HOWTO_LABELS_STEP3]*</li>
                </ol>
                <span style="color:#0099bb;">*[TEMPLATE_INVOICE_SECOND_CHOICE]*</span>
                <ol>
                    <li>*[TEMPLATE_INVOICE_HOWTO_HANDWRITTEN_STEP1]*</li>
                </ol>
                <p>*[TEMPLATE_INVOICE_EXPIRATION]*</p>
                <p>
                    <br/>
                    <span style="font-size:14px;">
                        <br/>
                        *[TEMPLATE_INVOICE_BESTREGARDS]*
                        <br/>
                        <br/>
                    </span>
                </p>
                <p>
                    *[TEMPLATE_INVOICE_SENDER_LINE1]*<br/>
                    *[TEMPLATE_INVOICE_SENDER_LINE2]*<br/>
                    *[TEMPLATE_INVOICE_SENDER_LINE3]*<br/>
                    *[TEMPLATE_INVOICE_SENDER_LINE4]*
                </p>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="FormatDate">
        <xsl:param name="Date"/>

        <xsl:value-of select="concat(
                      substring($Date, 9, 2),
                      '-',
                      substring($Date, 6, 2),
                      '-',
                      substring($Date, 1, 4)
                      )"/>
    </xsl:template>

    <xsl:template name="FormatPrice">
        <xsl:param name="Price" />
        DKK <xsl:value-of select='format-number($Price, "#,00", "danish")'/>
    </xsl:template>


    <xsl:template name="FormatVAT">
        <xsl:param name="VAT" />
        DKK <xsl:value-of select='format-number($VAT, "#,00", "danish")'/>
    </xsl:template>

</xsl:stylesheet>
