﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes"/>
  <xsl:decimal-format name="danish" decimal-separator="," grouping-separator="." />

  <xsl:template match="Orders">

    <html lang="dk" xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"  />
        <style type="text/css">
          body,p,b,td,li,span {
          font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif;
          font-size:14px;
          }
        </style>
      </head>
      <body style="font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif; font-size:10px;">
        <span style="font-size:12px;">*[TEMPLATE_INVOICE_HEADER]*</span>
        <p>
          <table style="width:100%;">
            <tr>
              <td style="width:100px;">
                <img src="*[TEMPLATE_INVOICE_LOGO]*" alt="" title=""/>
              </td>
              <td>
                <h1 style="color:#cc0000; font-size:18px; margin-bottom:0px;padding-bottom:2px;font-weight:normal;">*[TEMPLATE_INVOICE_HEADLINE]*</h1>
                <span style="font-size:12px; padding-top:0px;margin-top:0px;">*[TEMPLATE_INVOICE_HEADLINE_DESCRIPTION]*</span>
              </td>
            </tr>
          </table>
        </p>
        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:160px; padding:2px 8px 2px 8px;font-weight:bold;">
                      *[TEMPLATE_INVOICE_NO]*
                    </td>
                    <td style="color:#cc0000;padding:2px 8px 2px 8px; margin:0px;">
                      <xsl:value-of select="ReceiptNumber"/>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="padding:10px 10px 40px 10px">
                      *[TEMPLATE_INVOICE_GREETING]*
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;padding:2px 40px 2px 8px;font-weight:bold;">*[TEMPLATE_INVOICE_HEADER_PRODUCT]*</td>
                    <td style="width:25%;text-align:right;font-weight:bold;padding:2px 0px 2px 8px;">*[TEMPLATE_INVOICE_HEADER_COUNT]*</td>
                    <td style="width:25%;text-align:right;padding:2px 40px 2px 8px; margin:0px;font-weight:bold;">*[TEMPLATE_INVOICE_HEADER_PRICE]*</td>
                  </tr>

                  <xsl:apply-templates select="SubOrders" />

                </table>
              </td>
            </tr>
            <tr>
              <td style="width:100%; padding:0px;">
                <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;padding:10px 40px 2px 10px;font-weight:bold;">
                    </td>
                    <td style="width:25%;text-align:left;">*[TEMPLATE_INVOICE_TOTAL_AMOUNT]* :</td>
                    <td style="width:25%;text-align:right;padding:10px 40px 2px 0px; margin:0px;">
                      <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="OrderSum" />
                      </xsl:call-template>
                    </td>
                  </tr>
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;width:130px; padding:10px 40px 2px 10px;font-weight:bold;">
                    </td>
                    <td style="width:25%;text-align:left;">*[TEMPLATE_INVOICE_TOTAL_AMOUNT_EX_VAT]* :</td>
                    <td style="width:25%;text-align:right;padding:10px 40px 2px 0px; margin:0px;">
                      <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="OrderSum" />
                      </xsl:call-template>
                    </td>
                  </tr>
                  <tr style="background-color:#E4E4E4;">
                    <td style="width:50%;width:130px; padding:10px 40px 2px 10px;font-weight:bold;">
                    </td>
                    <td style="width:25%;text-align:left;">*[TEMPLATE_INVOICE_TOTAL_AMOUNT_VAT]* :</td>
                    <td style="width:25%;text-align:right;padding:10px 40px 2px 0px; margin:0px;">DKK 0,00</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <table style="width:100%;border:solid 2px #DBD4C4;border-collapse:collapse;">
            <tr style="background-color:#DBD4C4;">
              <td style="width:50%;padding:10px 40px 2px 10px;font-weight:bold;">
              </td>
              <td style="width:25%;text-align:left;">*[TEMPLATE_INVOICE_TOTAL_AMOUNT_DUE]* :</td>
              <td style="width:25%;text-align:right;color:red;padding:10px 40px 2px 0px; margin:0px;">
                <xsl:call-template name="FormatPrice">
                  <xsl:with-param name="Price" select="OrderSum" />
                </xsl:call-template>
              </td>
            </tr>
          </table>
        </p>
        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="width:50%;width:130px; padding:2px 40px 2px 10px;font-weight:bold; background-color:#E4E4E4;">*[TEMPLATE_INVOICE_PURCHASE_HEADER]*</td>
            </tr>
            <tr>
              <td style="padding-left:10px;padding-top:10px;">
                <table style="width:100%;">
                  <tr style="border-bottom:solid 2px #c1c1c1;">
                    <td style="padding-top:10px;">*[TEMPLATE_INVOICE_PURCHASE_DATE]*</td>
                    <td style="padding-top:10px;">
                      <xsl:call-template name="FormatDate">
                        <xsl:with-param name="Date" select="SalesDate" />
                      </xsl:call-template>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:10px;">*[TEMPLATE_INVOICE_PURCHASE_EMAIL]*</td>
                    <td style="padding-top:10px;">
                      <xsl:value-of select="Email"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top:10px;padding-bottom:10px;">*[TEMPLATE_INVOICE_PURCHASE_TRANSACTIONID]*</td>
                    <td style="padding-top:10px;padding-bottom:10px;">
                      <xsl:value-of select="CaptureId"/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </p>
        <p>
          <table style="width:100%;border:solid 2px #E4E4E4;border-collapse:collapse;">
            <tr>
              <td style="padding:2px 40px 2px 10px;font-weight:bold; background-color:#E4E4E4;">*[TEMPLATE_INVOICE_HOWTO_HEADER]*</td>
            </tr>
            <tr>
              <td style="padding:10px 40px 2px 10px;">
                <span style="color:#cc0000;">*[TEMPLATE_INVOICE_HOWTO_LABELS_HEADER]*</span>
                <ol>
                  <li>*[TEMPLATE_INVOICE_HOWTO_LABELS_STEP1]*</li>
                  <li>*[TEMPLATE_INVOICE_HOWTO_LABELS_STEP2]*</li>
                  <li>*[TEMPLATE_INVOICE_HOWTO_LABELS_STEP3]*</li>
                </ol>
                <span style="color:#cc0000;">*[TEMPLATE_INVOICE_HOWTO_HANDWRITTEN_HEADER]*</span>
                <ol>
                  <li>*[TEMPLATE_INVOICE_HOWTO_HANDWRITTEN_STEP1]*</li>
                </ol>
                *[TEMPLATE_INVOICE_HOWTO_EPILOG]*
              </td>
            </tr>
          </table>
        </p>
        <p>
          <br/>
          <span style="font-size:14px;">
            <br/>
            *[TEMPLATE_INVOICE_BESTREGARDS]*<br/><br/>
          </span>
          <table style="width:100%;">
            <tr>
              <td>
                *[TEMPLATE_INVOICE_SENDER_LINE1]*<br/>
                *[TEMPLATE_INVOICE_SENDER_LINE2]*<br/>
                *[TEMPLATE_INVOICE_SENDER_LINE3]*<br/>
                *[TEMPLATE_INVOICE_SENDER_LINE4]*
              </td>
            </tr>
          </table>
        </p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="SubOrders" name="SubOrders">
    <xsl:for-each select="SubOrders">
      <tr style="background-color:#FFFFFF;">
        <td style="width:400px;width:130px;padding:10px 1px 10px 10px;">
          <xsl:value-of select="Product" />
        </td>
        <td style="text-align:right;padding:10px 1px 10px 10px;">
          <xsl:value-of select="Amount"/>
        </td>
        <td style="text-align:right;padding:10px 40px 10px 10px;">
          <xsl:call-template name="FormatPrice">
            <xsl:with-param name="Price" select="Price" />
          </xsl:call-template>
        </td>
      </tr>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date"/>

    <xsl:value-of select="concat(
                      substring($Date, 9, 2),
                      '-',
                      substring($Date, 6, 2),
                      '-',
                      substring($Date, 1, 4)
                      )"/>
  </xsl:template>

  <xsl:template name="FormatPrice">
    <xsl:param name="Price" />
    DKK <xsl:value-of select='format-number($Price, "#,00", "danish")'/>
  </xsl:template>

</xsl:stylesheet>
