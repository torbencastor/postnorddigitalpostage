﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes"/>
  <xsl:decimal-format name="danish" decimal-separator="," grouping-separator="." />
  
  <xsl:template match="Orders">

    <html lang="dk" xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"  />
        <style type="text/css">
          body,p,b,td,li,span {
          font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif;
          font-size:14px;
          }
        </style>
      </head>
      <body style="">
        <p>*[TEMPLATE_APP_INVOICE_GREETING]*</p>
        <p>
          <table>
            <tr>
              <td>
                <b>*[TEMPLATE_APP_INVOICE_NO]*:</b>
              </td>
              <td style="color:#0099bb; padding-left:20px;">
                <xsl:value-of select="ReceiptNumber"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>*[TEMPLATE_APP_INVOICE_TOTAL_AMOUNT_DUE]*:</b>
              </td>
              <td style="padding-left:20px;">
                <xsl:call-template name="FormatPrice">
                  <xsl:with-param name="Price" select="OrderSum" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td>
                <b>*[TEMPLATE_APP_INVOICE_PURCHASE_DATE]*:</b>
              </td>
              <td style="padding-left:20px;">
                <xsl:call-template name="FormatDate">
                  <xsl:with-param name="Date" select="SalesDate" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  *[TEMPLATE_APP_INVOICE_PAYMENT_METHOD]*:</b>
              </td>
              <td style="padding-left:20px;">
                <xsl:choose>
                  <xsl:when test="Payment='MobilePay'">
                    MobilePay
                  </xsl:when>
                  <xsl:otherwise>
                    Betalingskort
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
          </table>
        </p>
        <p>*[TEMPLATE_APP_INVOICE_EXPIRATION]*</p>
        <p>*[TEMPLATE_APP_INVOICE_HOWTO_HEADER]*</p>
        <p>*[TEMPLATE_APP_INVOICE_HOWTO_1]*</p>
        <p>*[TEMPLATE_APP_INVOICE_HOWTO_2]*</p>
        <p>*[TEMPLATE_APP_INVOICE_HOWTO_EPILOG]*</p>
        <p>*[TEMPLATE_APP_INVOICE_CONTACT]*</p>
        <p>*[TEMPLATE_APP_INVOICE_TERMS]*</p>
        <p>
          <span style="font-size:14px;">
            <br/>
            *[TEMPLATE_APP_INVOICE_BESTREGARDS]*
            <br/>
            <br/>
          </span>
        </p>
        <p>
          *[TEMPLATE_APP_INVOICE_SENDER_LINE1]*<br/>
          *[TEMPLATE_APP_INVOICE_SENDER_LINE2]*<br/>
          *[TEMPLATE_APP_INVOICE_SENDER_LINE3]*<br/>
          *[TEMPLATE_APP_INVOICE_SENDER_LINE4]*</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date"/>

    <xsl:value-of select="concat(
                      substring($Date, 9, 2),
                      '-',
                      substring($Date, 6, 2),
                      '-',
                      substring($Date, 1, 4)
                      )"/>
  </xsl:template>

  <xsl:template name="FormatPrice">
    <xsl:param name="Price" />
    DKK <xsl:value-of select='format-number($Price, "#,00", "danish")'/>
  </xsl:template>

</xsl:stylesheet>
