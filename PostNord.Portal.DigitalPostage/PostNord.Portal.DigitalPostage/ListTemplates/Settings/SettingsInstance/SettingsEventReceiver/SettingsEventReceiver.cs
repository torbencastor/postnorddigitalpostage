﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;
using System.Web;

namespace PostNord.Portal.DigitalPostage.ListTemplates.Settings.SettingsInstance.SettingsEventReceiver
{
    /// <summary>
    /// Settings List Item Events
    /// </summary>
    public class SettingsEventReceiver : SPItemEventReceiver
    {

        /// <summary>
        /// Gets the cache key.
        /// </summary>
        /// <param name="listName">Name of the list.</param>
        /// <param name="language">The language.</param>
        /// <returns>
        /// CacheKey for current list in current language
        /// </returns>
        private static string GetCacheKey(string listName, uint language)
        {
            return string.Format("Settings_{0}_{1}", language, listName);
        }

        /// <summary>
        /// Clears the cached version of the list.
        /// </summary>
        /// <param name="list">The list.</param>
        protected void ClearCache(SPList list)
        {
            try
            {
                using (SPWeb web = list.ParentWeb)
                {
                    string cacheKey = GetCacheKey(list.Title, web.Language);
                    if (HttpRuntime.Cache[cacheKey] != null)
                    {
                        HttpRuntime.Cache.Remove(cacheKey);
                    }
                }
            }
            catch {}
        }

        /// <summary>
        /// An item was added.
        /// </summary>
        public override void ItemAdded(SPItemEventProperties properties)
        {
            base.ItemAdded(properties);
            this.ClearCache(properties.List);
        }

        /// <summary>
        /// An item was updated.
        /// </summary>
        public override void ItemUpdated(SPItemEventProperties properties)
        {
            base.ItemUpdated(properties);
            this.ClearCache(properties.List);
        }

        /// <summary>
        /// An item was deleted.
        /// </summary>
        public override void ItemDeleted(SPItemEventProperties properties)
        {
            base.ItemDeleted(properties);
            this.ClearCache(properties.List);
        }
    }
}