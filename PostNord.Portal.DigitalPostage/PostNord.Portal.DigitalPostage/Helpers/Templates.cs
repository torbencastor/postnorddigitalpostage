﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace PostNord.Portal.DigitalPostage.Helpers
{
    /// <summary>
    /// Template parsing
    /// </summary>
    public class Templates
    {

        private static Regex EnvoiceReg = new Regex(@"\*\[(?<TagName>TEMPLATE_\S*)+\]\*");

        /// <summary>
        /// Replaces template tags with relevant text.
        /// </summary>
        /// <param name="xslt">The XSLT.</param>
        /// <remarks>
        /// Template tag: *[TEMPLATE_SOME_TEXT]*
        /// </remarks>
        /// <returns></returns>
        private static string ReplaceInvoiceTexts(string xslt)
        {

            Hashtable texts = TextSettings.GetValues();
            foreach (string tag in texts.Keys)
            {
                xslt = EnvoiceReg.Replace(xslt, (m => m.Groups["TagName"].Value == tag ? texts[tag].ToString() : m.Value));
            }
            return xslt;
        }

        public static string ParseTemplate(string xml, string receiptTemplateUrl)
        {
            return ParseTemplate(xml, receiptTemplateUrl, SPContext.Current.Site.ID);
        }

        /// <summary>
        /// Parses xslt template from XML
        /// </summary>
        /// <param name="xml">xml</param>
        /// <param name="receiptTemplateUrl">Url pointing to the XSLT template</param>
        /// <returns>
        /// HTML representation of the xml
        /// </returns>
        public static string ParseTemplate(string xml, string receiptTemplateUrl, Guid siteId)
        {
            string html = "";
            string xslt = "";

            string url = string.Empty;

            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite siteElevated = new SPSite(siteId))
                    {
                        url = SPUtility.ConcatUrls(siteElevated.Url, receiptTemplateUrl);
                        SPFile file = siteElevated.RootWeb.GetFile(url);
                        using (TextReader reader = new StreamReader(file.OpenBinaryStream()))
                        {
                            xslt = reader.ReadToEnd();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error loading reciept template: {0}. {1}", url, ex.Message), ex);
            }

            xslt = ReplaceInvoiceTexts(xslt);

            XslCompiledTransform xsl = new XslCompiledTransform();
            xsl.Load(new XmlTextReader(new StringReader(xslt)));

            try
            {
                using (MemoryStream xmlStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml)))
                {
                    XPathDocument xPathDoc = new XPathDocument(new StreamReader(xmlStream));
                    StringBuilder htmlBuilder = new StringBuilder();
                    XmlWriter xmlWriter = XmlWriter.Create(htmlBuilder);
                    xsl.Transform(xPathDoc, xmlWriter);
                    html = htmlBuilder.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Error transforming XML. File: {0}. {1}", receiptTemplateUrl, ex.Message), ex);
            }

            return RemoveXml(html);
        }

        /// <summary>
        /// Removes the XML tags and namespaces from html
        /// </summary>
        /// <param name="html">Raw HTML</param>
        /// <returns>Cleaned up HTML</returns>
        private static string RemoveXml(string html)
        {
            string pattern = "^<\\?xml.*\\?>";
            Regex reg = new Regex(pattern);
            return reg.Replace(html, "<!DOCTYPE html>" + Environment.NewLine);
        }

    }
}
