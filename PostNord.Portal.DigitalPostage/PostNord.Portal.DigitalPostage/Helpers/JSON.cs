﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Helpers
{
    public class JSON
    {
        public string GetAsString(List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo> list)
        {
            try
            {
                var jsonString =new System.Text.StringBuilder();
                jsonString.Append("[");

                foreach(var procuctinfo  in list){
                    jsonString.Append("{");
                    jsonString.Append("Code:"+procuctinfo.Code+"|" );
                    jsonString.Append("ExpiryDate:"+procuctinfo.ExpiryDate.Ticks+"|" );
                    jsonString.Append("OverlayText:"+procuctinfo.OverlayText+"|" );
                    jsonString.Append("Price:"+procuctinfo.Price );
                    jsonString.Append("}");
                }

                return jsonString.ToString() + "]"; 
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
