﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using System.Web;
using System.Globalization;
using System.Web.Caching;
using System.Collections;
using Microsoft.SharePoint.Utilities;

namespace PostNord.Portal.DigitalPostage.Helpers
{

    /// <summary>
    /// Text settings helper
    /// </summary>
    public class TextSettings
    {
        /// <summary>
        /// Default Name of List used to look up translations
        /// </summary>
        /// 
        protected static string _ListName = "TextSettingsV5";
        public static string ListName
        {
            get { return _ListName; }
            set { _ListName = value; }
        }

        /// <summary>
        /// Fieldname in list containing translated value
        /// </summary>
        private const string ListItemTranslatedFieldName = "Value";

        private static object _lock = new object();

        /// <summary>
        /// The values used in TextSettings. Used for performance boost.
        /// </summary>
        public static Hashtable Values = null;

        /// <summary>
        /// Gets the cache key.
        /// </summary>
        /// <param name="listName">Name of the list.</param>
        /// <param name="language">The language.</param>
        /// <returns>
        /// CacheKey for current list in current language
        /// </returns>
        private static string GetCacheKey(string listName, uint language)
        {
            return string.Format("Settings_{0}_{1}_{2:yyyyMMdd}", language, listName, DateTime.Now);
        }

        /// <summary>
        /// Clears the cache.
        /// </summary>
        /// <param name="language">The language.</param>
        public static void ClearCache(uint language)
        {
            ClearCache(ListName, language);
        }

        /// <summary>
        /// Clears the cache.
        /// </summary>
        /// <param name="listName">Name of the list.</param>
        /// <param name="language">The language.</param>
        protected static void ClearCache(string listName, uint language)
        {
            string cacheKey = GetCacheKey(listName, language);
            if (HttpRuntime.Cache[cacheKey] != null)
            {
                HttpRuntime.Cache.Remove(cacheKey);
            }
        }

        /// <summary>
        /// Adds the list items to cache.
        /// </summary>  
        /// <param name="list">The list.</param>
        private static Hashtable AddListItemsToCache(SPList list)
        {
            SPQuery query = new SPQuery();
            query.RowLimit = (uint)list.ItemCount;
            //query.ViewFields = "<FieldRef Name='Title' /><FieldRef Name='Value' />";
            query.Query = "<OrderBy><FieldRef Name='LinkTitle' /></OrderBy>";

            if (list.Fields.ContainsField("FromDate"))
            {
                query.Query = "<OrderBy><FieldRef Name='FromDate' /></OrderBy>";
            }

            SPListItemCollection settings = list.GetItems(query);

            Hashtable settingsTable = new Hashtable();

            foreach(SPListItem setting in settings)
            {
                if (list.Fields.ContainsField("FromDate") && setting["FromDate"] != null)
                {
                    DateTime fromDate = (DateTime)setting["FromDate"];
                    if (fromDate > DateTime.Today)
                    {
                        continue;
                    }
                }

                if (!settingsTable.ContainsKey(setting.Title))
                {
                    if (setting[ListItemTranslatedFieldName] != null)
                    {
                        settingsTable.Add(setting.Title, setting[ListItemTranslatedFieldName].ToString());
                    }
                } 
                else
                {
                    settingsTable[setting.Title] = setting[ListItemTranslatedFieldName].ToString();
                    //Helpers.Logging.LogEvent(string.Format("Multiple Settings found: {0}", setting.Title), Logging.TraceSeverityLevel.Medium, Logging.EventSeverityLevel.Error);
                }
            }

            if (settingsTable.Count > 0)
            {
                string cacheKey = GetCacheKey(list.Title, list.ParentWeb.Language);

                // cache the result in 1 hour
                lock (_lock)
                {
                    HttpRuntime.Cache.Add(cacheKey, settingsTable, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Default, null);
                }
            }
            SetLastModifiedDate(list.Title);
            return settingsTable;
        }

        /// <summary>
        /// Get Setting Values
        /// </summary>
        /// <returns>
        /// Key / Value table of settings
        /// </returns>
        public static Hashtable GetValues()
        {
            return GetValues(ListName);
        }

        /// <summary>
        /// Get Setting Values prefixed with "prefix"
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>
        /// Key / Value table of settings
        /// </returns>
        /// <example>
        /// Return all items prefixed with "APP_"
        /// </example>
        public static Hashtable GetValuesWithPrefix(string prefix)
        {

            Hashtable allValues = GetValues(ListName);
            Hashtable prefixedvalues = new Hashtable();

            foreach (var key in allValues.Keys)
            {
                if (key.ToString().Substring(0,4) == "APP_")
                {
                    prefixedvalues.Add(key, allValues[key]);
                }
            }

            return prefixedvalues;
        }


        /// <summary>
        /// Date when list was last modified or deleted from
        /// </summary>
        /// <returns></returns>
        public static bool IsCacheDirty()
        {
            return IsCacheDirty(ListName);
        }

        /// <summary>
        /// Sets the last modified date.
        /// </summary>
        public static void SetLastModifiedDate()
        {
            SetLastModifiedDate(ListName);
        }

        /// <summary>
        /// Sets the last modified date.
        /// </summary>
        /// <param name="listname">The listname.</param>
        /// <exception cref="System.ApplicationException">Admin Web not found. Please activate feature on Admin Web.</exception>
        protected static void SetLastModifiedDate(string listname)
        {
            string propertyKey = string.Format("{0}_LastModified", listname);

            DateTime returnDate = DateTime.MinValue;
            // Determine where the admin list are placed by looking in RootWeb PropertyBag
            string adminWebUrl = Helpers.ConfigSettings.GetAdminWebUrl(SPContext.Current.Web);

            if (adminWebUrl == string.Empty)
            {
                throw new ApplicationException("Admin Web not found. Please activate feature on Admin Web.");
            }
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.ID))
                {
                    try
                    {
                        using (SPWeb web = site.OpenWeb(adminWebUrl))
                        {
                            SPList list = web.Lists[listname];

                            if (list.LastItemModifiedDate > list.LastItemDeletedDate)
                            {
                                returnDate = list.LastItemModifiedDate;
                            }
                            else
                            {
                                returnDate = list.LastItemDeletedDate;
                            }

                            if (web.Properties[propertyKey] != null)
                            {
                                HelperClass.RemoveProperty(web, propertyKey);
                            }
                            HelperClass.AddProperty(web, propertyKey, SPUtility.CreateISO8601DateTimeFromSystemDateTime(DateTime.Now));
                        }
                    }
                    catch (Exception ex)
                    {
                        var errMsg = string.Format("Error loading list: {0} from admin web: {1}. Error message: {1}", listname, adminWebUrl, ex.Message);
                        Logging.LogEvent(errMsg, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                        throw new ApplicationException(errMsg, ex);
                    }
                }
            });
        }


        /// <summary>
        /// Date when list was last modified or deleted from
        /// </summary>
        /// <param name="listname">The listname.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Admin Web not found. Please activate feature on Admin Web.</exception>
        protected static bool IsCacheDirty(string listname)
        {
            string propertyKey = string.Format("{0}_LastModified", listname);
            DateTime returnDate = DateTime.MinValue;
            bool cacheDirty = false;
            // Determine where the admin list are placed by looking in RootWeb PropertyBag
            string adminWebUrl = Helpers.ConfigSettings.GetAdminWebUrl(SPContext.Current.Web);

            if (adminWebUrl == string.Empty)
            {
                throw new ApplicationException("Admin Web not found. Please activate feature on Admin Web.");
            }

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.ID))
                {
                    try
                    {
                        using (SPWeb web = site.OpenWeb(adminWebUrl))
                        { 
                            SPList list = web.Lists[listname];

                            if (list.LastItemModifiedDate > list.LastItemDeletedDate)
                            {
                                returnDate = list.LastItemModifiedDate;
                            }
                            else
                            {
                                returnDate = list.LastItemDeletedDate;
                            }

                            if (web.Properties[propertyKey] == null)
                            {
                                cacheDirty = true;
                            }
                            else
                            {
                                DateTime propertyDate = SPUtility.CreateDateTimeFromISO8601DateTimeString(web.Properties[propertyKey].ToString());
                                if (returnDate > propertyDate)
                                {
                                    cacheDirty = true;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var errMsg = string.Format("Error loading list: {0} from admin web: {1}. Error message: {1}", listname, adminWebUrl, ex.Message);
                        Logging.LogEvent(errMsg, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                        throw new ApplicationException(errMsg, ex);
                    }
                }
            });
            return cacheDirty;
        }

        /// <summary>
        /// Get Setting Values
        /// </summary>
        /// <param name="listname">The listname.</param>
        /// <returns>Key / Value table of settings</returns>
        public static Hashtable GetValues(string listname)
        {
            // if Values is set we are in a unit test where Values are set in the test
            if (Values  != null)
            {
                return Values;
            }
            uint language = SPContext.Current != null ? SPContext.Current.Web.Language : 406;//set as danish if null
            string cacheKey = GetCacheKey(listname, language);

            // if values are cached we grab them
            if (HttpRuntime.Cache[cacheKey] != null)
            {
                Hashtable cachedTable = (Hashtable)HttpRuntime.Cache[cacheKey];
                if (cachedTable.Count > 0)
                {
                    return (System.Collections.Hashtable)HttpRuntime.Cache[cacheKey];
                }
            }

            System.Collections.Hashtable values = null;

            // Determine where the admin list are placed by looking in RootWeb PropertyBag
            string adminWebUrl = Helpers.ConfigSettings.GetAdminWebUrl(SPContext.Current.Web);

            if (adminWebUrl == string.Empty)
            {
                throw new ApplicationException("Admin Web not found. Please activate feature on Admin Web.");
            }

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.ID))
                {
                    try
                    {
                        using (SPWeb web = site.OpenWeb(adminWebUrl))
                        { 
                            SPList list = web.Lists[listname];
                            values = AddListItemsToCache(list);
                        }
                    }
                    catch (Exception ex)
                    {
                        var errMsg = string.Format("Error loading list: {0} from admin web: {1}. Error message: {1}", listname, adminWebUrl, ex.Message);
                        Logging.LogEvent(errMsg, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                        throw new ApplicationException(errMsg, ex);
                    }
                }
            });

            return values;
        }

        /// <summary>
        /// Gets translated text from Translation List
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="listname">The listname.</param>
        /// <returns>
        /// Translated text if found or else Default text
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        public static string GetText(string key, string defaultValue, string listname)
        {
            Hashtable values = GetValues(listname);

            // something unexpected is wrong!
            if (values == null)
            {
                throw new ApplicationException(string.Format("Error loading Text settings for list: {0}", listname));
            }

            if (values != null && values.ContainsKey(key))
            {
                return values[key].ToString().Replace(Environment.NewLine, "<br/>");
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Gets translated text from Translation List
        /// </summary>
        /// <param _name="Key">The key.</param>
        /// <param _name="DefaultValue">The default value.</param>
        /// <returns>
        /// Translated text if found or else Default text
        /// </returns>
        public static string GetText(string key, string defaultValue)
        {
            return GetText(key, defaultValue, ListName);
        }

        /// <summary>
        /// Gets translated text from Translation List
        /// </summary>
        /// <param _name="Key">The key.</param>
        /// <returns>
        /// Translated text if found or else Empty string
        /// </returns>
        public static string GetText(string key)
        {
            return GetText(key, string.Empty);
        }
    }

}
