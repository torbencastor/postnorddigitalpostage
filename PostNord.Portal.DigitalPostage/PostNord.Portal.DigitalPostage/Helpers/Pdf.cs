﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Helpers
{
    /// <summary>
    /// PDF generator helper class
    /// </summary>
    public class Pdf
    {
        /// <summary>
        /// Creates the PDF from xhtml text.
        /// </summary>
        /// <param name="html">The HTML.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="marginLeft">The margin left.</param>
        /// <param name="marginRight">The margin right.</param>
        /// <param name="marginTop">The margin top.</param>
        /// <param name="marginBottom">The margin bottom.</param>
        /// <returns>PDF contents</returns>
        public static byte[] CreatePdf(string html, iTextSharp.text.Rectangle pageSize, float marginLeft, float marginRight, float marginTop, float marginBottom)
        {
            using (var msOutput = new MemoryStream())
            {
                using (var document = new Document(PageSize.A4, 30, 30, 30, 30))
                {
                    using (var writer = PdfWriter.GetInstance(document, msOutput))
                    {
                        //Open our document for writing
                        document.Open();

                        //Bind a reader to our text
                        using (TextReader reader = new StringReader(html))
                        {
                            //Parse the HTML and write it to the document
                            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, reader);
                        }

                        //Close the main document
                        document.Close();
                    }

                    //Return our raw bytes
                    return msOutput.ToArray();
                }
            }
        }

    }
}
