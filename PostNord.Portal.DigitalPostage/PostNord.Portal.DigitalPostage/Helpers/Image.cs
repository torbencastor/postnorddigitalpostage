﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Web;

namespace PostNord.Portal.DigitalPostage.Helpers
{
    /// <summary>
    /// Image Helper
    /// </summary>
    public class Image
    {
        /// <summary>
        /// Shows the image.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="image">The image.</param>
        public static void ShowImage(string filename, byte[] image)
        {
            HttpResponse httpResponse = HttpContext.Current.Response;

            System.Drawing.Image img = ByteArrayToImage(image);

            string contentType = string.Empty;
            string ext = GetImageExtension(img, out contentType);

            httpResponse.ClearHeaders();
            httpResponse.Clear();

            httpResponse.ContentType = contentType;
            httpResponse.AddHeader("Content-Transfer-Encoding", "binary");
            httpResponse.AddHeader("Content-Disposition", "image;filename=\"" + filename + ext + "\"");
            httpResponse.AddHeader("Content-Length", image.Length.ToString());

            httpResponse.BinaryWrite(image);

            httpResponse.Flush();
            httpResponse.End();
        }


        /// <summary>
        /// Convert array of bytes into image.
        /// </summary>
        /// <param name="byteArrayIn">The byte array</param>
        /// <returns></returns>
        public static System.Drawing.Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        /// <summary>
        /// Gets the image extension.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="contentType">Type of the content.</param>
        /// <returns></returns>
        public static string GetImageExtension(System.Drawing.Image image, out string contentType)
        {
            string ext = string.Empty;
            contentType = string.Empty;

            if (System.Drawing.Imaging.ImageFormat.Jpeg.Equals(image.RawFormat))
            {
                ext = ".jpg";
                contentType = "image/jpeg";
            }
            else if (System.Drawing.Imaging.ImageFormat.Gif.Equals(image.RawFormat))
            {
                ext = ".gif";
                contentType = "image/gif";
            }
            else if (System.Drawing.Imaging.ImageFormat.Png.Equals(image.RawFormat))
            {
                ext = ".png";
                contentType = "image/png";
            }
            return ext;
        }

        /// <summary>
        /// Gets the image extension.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns></returns>
        public static string GetImageExtension(System.Drawing.Image image)
        {
            string contentType = string.Empty;
            return GetImageExtension(image, out contentType);
        }

    }
}
