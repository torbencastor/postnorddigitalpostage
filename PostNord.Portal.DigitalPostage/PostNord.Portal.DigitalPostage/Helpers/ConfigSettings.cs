﻿using Microsoft.SharePoint;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Helpers
{
    /// <summary>
    /// Config Settings helper
    /// </summary>
    public class ConfigSettings : TextSettings
    {

        /// <summary>
        /// Gets the property bag key.
        /// </summary>
        /// <param name="web">The web.</param>
        /// <returns>Property bag key for current web language.</returns>
        public static string GetPropertyBagKey(SPWeb web)
        {
            return string.Format("AdminSiteUrl_{0}", web.Language);
        }

        /// <summary>
        /// Date when list was last modified or deleted from
        /// </summary>
        /// <returns></returns>
        public static new bool IsCacheDirty()
        {
            return IsCacheDirty(ListName);
        }

        /// <summary>
        /// Sets the last modified date.
        /// </summary>
        public static new void SetLastModifiedDate()
        {
            SetLastModifiedDate(ListName);
        }

        /// <summary>
        /// Sets the Admin web url in property bag.
        /// </summary>
        /// <param name="web">The web.</param>
        public static void SetAdminWebUrlInPropertyBag(SPWeb web)
        {

            string propertyKey = GetPropertyBagKey(web);

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite siteElevated = new SPSite(web.Site.ID))
                {
                    using (SPWeb webElevated = siteElevated.OpenWeb(siteElevated.RootWeb.ID))
                    {
                        // If we are at rootsite and property has already been set drop out since we are deploying on a development machine
                        // and we dont want to overwrite /admin setting
                        if (webElevated.IsRootWeb && (string.IsNullOrEmpty(webElevated.Properties[propertyKey])) == false)
                        {
                            return;
                        }
                        HelperClass.AddProperty(webElevated, propertyKey, web.ServerRelativeUrl);
                    }
                }
            });                
        }

        /// <summary>
        /// Gets the admin web URL.
        /// </summary>
        /// <param name="web">The web.</param>
        /// <returns>If web is rootweb. RootWeb Url is returned. If propertybag information is found it is returned, otherwise we expect /admin to be the correct position for the admin lists.</returns>
        public static string GetAdminWebUrl(SPWeb web)
        {
            string propertyKey = GetPropertyBagKey(web);
            string url = string.Empty;

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite siteElevated = new SPSite(web.Site.ID))
                {
                    if (siteElevated.RootWeb.Properties.ContainsKey(propertyKey))
                    {
                        url = siteElevated.RootWeb.Properties[propertyKey];
                    }
                }
            });
            return url;
        }

        /// <summary>
        /// Default Name of List used to look up translations
        /// </summary>
        protected static new string ListName
        {
            get
            {
                return "ConfigSettings";
            }
        }

        /// <summary>
        /// Clears the cache.
        /// </summary>
        /// <param name="language">The language.</param>
        public static new void ClearCache(uint language)
        {
            ClearCache(ListName, language);
        }

        /// <summary>
        /// Gets the values.
        /// </summary>
        /// <returns></returns>
        public static new Hashtable GetValues()
        {
            return GetValues(ListName);
        }

        /// <summary>
        /// Gets translated text from Translation List
        /// </summary>
        /// <param _name="Key">The key.</param>
        /// <param _name="DefaultValue">The default value.</param>
        /// <returns>
        /// Translated text if found or else Default text
        /// </returns>
        public static new string GetText(string key, string defaultValue)
        {
            return GetText(key, defaultValue, ListName);
        }

        /// <summary>
        /// Gets translated text from Translation List
        /// </summary>
        /// <param _name="Key">The key.</param>
        /// <returns>
        /// Translated text if found or else Empty string
        /// </returns>
        public static new string GetText(string key)
        {
            return GetText(key, string.Empty, ListName);
        }
    }
}
