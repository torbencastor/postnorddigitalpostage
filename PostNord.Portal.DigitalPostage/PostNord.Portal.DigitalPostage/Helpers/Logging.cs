﻿using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.Helpers
{
    public class Logging
    {
        /// <summary>
        /// Tracing Severity levels
        /// </summary>
        public enum TraceSeverityLevel
        {
            High = 20,
            Medium = 50,
            Monitorable = 15,
            None = 0,
            Unexpected = 10,
            Verbose = 100,
            VerboseEx = 200
        };

        /// <summary>
        /// Event Severity Level
        /// </summary>
        public enum EventSeverityLevel
        {
            Error = 40,
            ErrorCritical = 30,
            ErrorSecurityBreach = 20,
            ErrorServiceUnavailable = 10,
            FailureAudit = 60,
            Information = 80,
            None = 0,
            Success = 90,
            SuccessAudit = 70,
            Verbose = 100,
            Warning = 50
        };

        public enum ErrorCodes
        {
            UNKNOWN_ERROR,
            VERSION_NOT_SUPPORTED,
            PLATFORM_NOT_SUPPORTED,
            EMAIL_NOT_VALID,
            WEB_SHOP_IS_CLOSED,
            PRODUCT_NOT_FOUND,
            PRIORITY_OUT_OF_RANGE,
            ORDER_NOT_FOUND,
            TRANSACTION_ID_MISMATCH,
            ORDER_IS_CLOSED,
            ORDER_ALREADY_PAID,
            EMAIL_DOES_NOT_MATCH,
            PAYMENTMETHOD_INVALID,
            HASHCODE_MISSING,
            NO_CONNECTION_TO_NETS,
            NETS_TIME_OUT,
            CINT_TIME_OUT,
            CINT_ERROR,
            NETS_ERROR,
            MOBILE_PAY_SIGNATURE_INVALID,
            MOBILE_PAY_CAPTURE_ERROR,
            WRONG_WEB_SERVICE,
            ACCESS_DENIED,
            UNABLE_TO_SEND_MAIL,
            NETS_AUTH_FAILURE,
            ORDER_CANCELLED,
            ORDER_REFUNDED
        }


        /// <summary>
        /// Logs the event.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="traceSeverity">The trace severity.</param>
        /// <param name="evenSeverity">The even severity.</param>
        public static void LogEvent(string errorMessage, TraceSeverityLevel traceSeverity, EventSeverityLevel evenSeverity){
            LogEvent(0, errorMessage, traceSeverity, evenSeverity, null);
        }

        /// <summary>
        /// Logs the event.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="traceSeverity">The trace severity.</param>
        /// <param name="evenSeverity">The even severity.</param>
        public static void LogEvent(uint id, string errorMessage, TraceSeverityLevel traceSeverity, EventSeverityLevel evenSeverity){
             LogEvent(id, errorMessage, traceSeverity, evenSeverity, null);
        }

        /// <summary>
        /// Logs the event.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="traceSeverity">The trace severity.</param>
        /// <param name="evenSeverity">The even severity.</param>
        /// <param name="data">The data.</param>
        public static void LogEvent(uint id, string errorMessage, TraceSeverityLevel traceSeverity, EventSeverityLevel evenSeverity, params object[] data)
        {
            SPDiagnosticsService diagSvc = SPDiagnosticsService.Local;
            diagSvc.WriteTrace(id,
                new SPDiagnosticsCategory("Innofactor", (TraceSeverity)traceSeverity, (EventSeverity)evenSeverity),
                (TraceSeverity)traceSeverity,
                errorMessage,
                data);
        }

    }
}
