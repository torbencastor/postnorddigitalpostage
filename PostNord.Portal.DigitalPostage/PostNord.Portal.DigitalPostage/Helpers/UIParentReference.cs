﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PostNord.Portal.DigitalPostage.Helpers
{
    public class UIParentReference
    {
        private Dictionary<string, string> _urlParameters;
        private HttpRequest _httpRequest = null;
        /// <summary>
        /// URL parameters of parent URL
        /// Can be used to retrive the URL parameters of the parent page in case of an iFraming
        /// </summary>
        public Dictionary<string, string> UrlParameters
        {
            get
            {
                if (this._urlParameters == null)
                {
                    this._urlParameters = GetUrlParameters();
                }
                return this._urlParameters;
            }
        }
        public UIParentReference(HttpRequest httpRequest)
        {
            this._httpRequest = httpRequest;
        }


        /// <summary>
        /// Extracts URL parameters from the url of the parent that embeds the calling page
        /// An example is when the calling page needs URL parematers from a parent where it is embedde in an iFreame.
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetUrlParameters()
        {
            var urlParammeters = new Dictionary<string, string>();
            if (this._httpRequest.UrlReferrer != null)
            {
                var urlQuiry = this._httpRequest.UrlReferrer.Query;
                if (urlQuiry != null && !string.IsNullOrEmpty(urlQuiry))
                {
                    string token = string.Empty;
                    string responsecode = string.Empty;

                    if (!string.IsNullOrEmpty(urlQuiry))
                    {
                        char[] arr = { '?', '&' };
                        var urlParams = urlQuiry.Split(arr, StringSplitOptions.RemoveEmptyEntries);
                        char[] arr1 = { '=' };

                        foreach (var paramset in urlParams)
                        {
                            var keyValuePair = paramset.Split(arr1);
                            //is it a key value pair?
                            if (keyValuePair.Count() == 2)
                            {//is the key new?
                                var key = keyValuePair[0].ToLower(); //getting the parameter name as lower case
                                if (!urlParammeters.Keys.Contains(key))
                                {
                                    urlParammeters.Add(key, keyValuePair[1]);
                                }
                            }
                        }
                    }
                }
            }
            //always returned as initialised because the code needs to know that the Request.UrlReferrer.Query has been processed at least once
            return urlParammeters;
        }

    }
}
