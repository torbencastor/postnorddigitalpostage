﻿<%@ Assembly Name="PostNord.Portal.DigitalPostage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=fa72fcbf499cafdf" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DebugConfigSettings.aspx.cs" Inherits="PostNord.Portal.DigitalPostage.Layouts.DigitalPostage.DebugConfigSettings" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    Config Settings
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <h1>Debug Config Settings</h1>
    <h3>Lookup</h3>
    <b>Type:</b>
    <div>
        <asp:RadioButtonList ID="blType" runat="server" OnSelectedIndexChanged="blType_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="ConfigSettings" Selected="True"></asp:ListItem>
            <asp:ListItem Text="TextSettings"></asp:ListItem>
            <asp:ListItem Text="Terms"></asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div>
        <asp:TextBox ID="txtLookup" runat="server"></asp:TextBox>&nbsp;<asp:Button ID="btnOk" runat="server" Text="Lookup" OnClick="btnOk_Click1" />&nbsp;<asp:Button ID="btnClearCache" runat="server" Text="Clear cache" OnClick="btnClearCache_Click"/>
    </div>
     <div>
        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>
    <h2>Data</h2>
    <div>
        <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
    </div>
    <div>
        <asp:GridView ID="gvData" runat="server"></asp:GridView>
    </div>
   
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Config Settings
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
Config Settings</asp:Content>
