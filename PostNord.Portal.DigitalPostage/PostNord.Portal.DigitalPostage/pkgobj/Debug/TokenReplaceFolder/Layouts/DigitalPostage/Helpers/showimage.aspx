﻿<%@ Assembly Name="PostNord.Portal.DigitalPostage, Version=1.0.0.0, Culture=neutral, PublicKeyToken=fa72fcbf499cafdf" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="showimage.aspx.cs" Inherits="PostNord.Portal.DigitalPostage.Layouts.DigitalPostage.Helpers.ShowImage" %>
<html>
    <body style="padding:0; margin:0; background-color:#808080">
        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
        <asp:Image ID="imgImage" runat="server" />
    </body>
</html>
