﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="yes"/>
    <xsl:decimal-format name="danish" decimal-separator="," grouping-separator="." />

    <xsl:template match="Orders">

        <html lang="dk" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"  />
                <style type="text/css">
                    body,p,b,td,li,span {
                    font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif;
                    font-size:14px;
                    }
                </style>
            </head>
            <body style="font-family:Calibri,'CalibriLiteBold',Arial,Sans-Serif; font-size:14px;">
                <p>
                    <table style="width:100%;">
                        <tr>
                            <td >
                                <h1 style="color:#0099bb; font-size:18px; margin-bottom:0px;padding-bottom:2px;font-weight:normal;">*[TEMPLATE_INVOICE_HEADLINE]*</h1>
                            </td>
                            <td align="right">
                                <img width="180px" src="*[TEMPLATE_INVOICE_LOGO]*" alt="" title=""/>
                            </td>
                        </tr>
                    </table>
                </p>
                <p>
                    <table style="width:100%;border:solid 2px #0099bb;background-color: #0099bb;border-collapse:collapse;border-bottom-width:0">
                        <tr>
                            <td style="width:100%; padding:0;border: none;border-width:0">
                                <table style="width:100%;border-collapse:collapse;border: none;border-width:0">
                                    <tr style="background-color:#0099bb;color:white;border: none;border-width:0">
                                        <td style="background-color:#0099bb;width:100%;padding:2px 8px 2px 8px;font-weight:bold;color:white">
                                            *[TEMPLATE_INVOICE_NO]*
                                            <xsl:value-of select="ReceiptNumber"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px 10px 8px 10px;background-color: white;">
                                            *[TEMPLATE_INVOICE_GREETING]*
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:100%; padding:0px;">
                                <table style="width:100%;border:solid 0px green;border-collapse:collapse;padding:0">
                                    <tr style="background-color:#eee;color:#666;padding:0">
                                        <td style="width:50%;padding:2px 40px 2px 8px;font-weight:normal;">*[TEMPLATE_INVOICE_HEADER_PRODUCT]*</td>
                                        <td style="width:16%;text-align:right;font-weight:normal;padding:2px 0px 2px 8px;">*[TEMPLATE_INVOICE_HEADER_COUNT]*</td>
                                        <td style="width:16%;text-align:right;font-weight:normal;padding:2px 0px 2px 8px;">*[TEMPLATE_INVOICE_HEADER_ITEM_PRICE]*</td>
                                        <td style="width:16%;text-align:right;font-weight:normal;padding:2px 0px 2px 8px;">Pris ialt ekskl. moms</td>
                                        <td style="width:16%;text-align:right;font-weight:normal;padding:2px 0px 2px 8px;">Moms</td>
                                        <td style="width:16%;text-align:right;padding:2px 40px 2px 8px; margin:0px;font-weight:normal;">*[TEMPLATE_INVOICE_HEADER_PRICE]*</td>
                                    </tr>

                                    <xsl:apply-templates select="SubOrders" mode="SubOrders" />

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:100%;padding:0px;padding-top: 20px;background-color: white">
                                <table style="width:100%;border:solid 2px #0099bb;background-color: white;border-collapse:collapse;">

                                    <xsl:apply-templates select="SubOrders" mode="mySubOrders"/>

                                </table>
                            </td>
                        </tr>
                    </table>

                    <table style="width:100%;padding-top: 5px; border:2px solid #0099bb;border-top:none;background-color:#eee;border-collapse:collapse">
                        <tr style="background-color:#eee">
                            <td style="width:650px;padding:8px 40px 4px 10px;font-weight:bold">
                            </td>
                            <td style="width:70%;text-align:left;color:#0099bb;font-weight:bold">*[TEMPLATE_INVOICE_TOTAL_AMOUNT_DUE]* :</td>
                            <td style="width:30%;text-align:right;color:#0099bb;font-weight:bold;padding:5px 40px 2px 0px; margin:0px">
                                <xsl:call-template name="FormatPrice">
                                    <xsl:with-param name="Price" select="OrderSum" />
                                </xsl:call-template>
                            </td>
                        </tr>
                    </table>
                </p>
                <p>
                    <table style="width:100%;border:solid 2px #0099bb;background-color: #0099bb;border-collapse:collapse;">
                        <tr>
                            <td style="width:50%;width:130px; padding:2px 40px 2px 10px;font-weight:bold; background-color:#0099bb;color:white">*[TEMPLATE_INVOICE_PURCHASE_HEADER]*</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;padding-top:5px;background-color: white;">
                                <table style="width:100%;">
                                    <tr style="border-bottom:solid 2px #999;">
                                        <td style="padding-top:5px;">*[TEMPLATE_INVOICE_PURCHASE_DATE]*</td>
                                        <td style="padding-top:5px;">
                                            <xsl:call-template name="FormatDate">
                                                <xsl:with-param name="Date" select="SalesDate" />
                                            </xsl:call-template>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:5px;">*[TEMPLATE_INVOICE_PURCHASE_EMAIL]*</td>
                                        <td style="padding-top:5px;">
                                            <xsl:value-of select="Email"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:5px;padding-bottom:5px;">*[TEMPLATE_INVOICE_PURCHASE_TRANSACTIONID]*</td>
                                        <td style="padding-top:5px;padding-bottom:5px;">
                                            <xsl:value-of select="CaptureId"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:8px;padding-bottom:5px;" colspan="2">*[TEMPLATE_INVOICE_EXPIRATION]*</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </p>
                <p>
                    <table style="width:100%;border:solid 2px #0099bb;background-color: #0099bb;border-collapse:collapse;">
                        <tr style="border:solid 0px #0099bb">
                            <td style="padding:2px 40px 2px 10px;font-weight:bold; background-color:#0099bb;color:white">*[TEMPLATE_INVOICE_HOWTO_HEADER]*</td>
                        </tr>
                        <tr>
                            <td style="padding:5px 40px 2px 10px;background-color:white">
                                <span style="color:#0099bb;">*[TEMPLATE_INVOICE_HOWTO_LABELS_HEADER]*</span>
                                <ol style="margin-top:5px; margin-bottom:5px">
                                    <li>*[TEMPLATE_INVOICE_HOWTO_LABELS_STEP1]*</li>
                                    <li>*[TEMPLATE_INVOICE_HOWTO_LABELS_STEP2]*</li>
                                    <li>*[TEMPLATE_INVOICE_HOWTO_LABELS_STEP3]*</li>
                                </ol>
                                <span style="color:#0099bb;">*[TEMPLATE_INVOICE_HOWTO_HANDWRITTEN_HEADER]*</span>
                                <ol style="margin-top:5px; margin-bottom:15px">
                                    <li>*[TEMPLATE_INVOICE_HOWTO_HANDWRITTEN_STEP1]*</li>
                                </ol>
                                <p style="margin-top:3px">*[TEMPLATE_INVOICE_HOWTO_EPILOG]*</p>
                                <p style="margin-top:3px">*[TEMPLATE_INVOICE_CONTACT]*</p>
                            </td>
                        </tr>
                    </table>
                </p>
                <p style="padding:2px">
                    <span style="font-size:14px;">
                        *[TEMPLATE_INVOICE_BESTREGARDS]*<br/>
                    </span>
                    <table style="width:100%;">
                        <tr>
                            <td>
                                *[TEMPLATE_INVOICE_SENDER_LINE1]*<br/>
                                *[TEMPLATE_INVOICE_SENDER_LINE2]*<br/>
                                *[TEMPLATE_INVOICE_SENDER_LINE3]*<br/>
                                *[TEMPLATE_INVOICE_SENDER_LINE4]*
                            </td>
                        </tr>
                    </table>
                </p>
            </body>
        </html>
    </xsl:template>



    <xsl:template match="SubOrders" mode="SubOrders">
        <xsl:for-each select="SubOrders">
            <tr style="background-color:#FFFFFF;">
                <td style="padding:5px 1px 5px 10px;" width="25%">
                    <xsl:value-of select="DestinationInfo" />
                </td>
                <td style="text-align:right;padding:5px 1px 5px 10px;" width="10%">
                    <xsl:value-of select="Amount"/>
                </td>
                <td style="text-align:right;padding:5px 1px 5px 10px;" width="15%">
                    <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="Price"/>
                    </xsl:call-template>
                </td>
                <!--Pris ialt eksl. moms-->
                <td style="text-align:right;padding:5px 1px 5px 10px;" width="15%">
                    <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="SubOrderSumWithoutVAT"/>
                    </xsl:call-template>
                </td>
                <!-- Moms -->
                <td style="text-align:right;padding:5px 1px 5px 10px;" width="15%">
                    <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="SubOrderSumVAT"/>
                    </xsl:call-template>
                </td>
                <td style="text-align:right;padding:5px 40px 5px 5px;" width="20%">
                    <xsl:call-template name="FormatPrice">
                        <xsl:with-param name="Price" select="SubOrderSum" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:for-each>
    </xsl:template>


    <xsl:key name="TotalSubOrders" match="SubOrders" use="Priority"/>
    <xsl:template match="SubOrders" mode="mySubOrders">
        <xsl:for-each select="SubOrders[generate-id()=generate-id(key('TotalSubOrders',Priority)[1])]">
            <xsl:sort select="Priority" order="descending" />

            <xsl:if test="Priority = 'A'">
                <tr style="background-color:white;color:#444">
                    <td style="width:650px;padding:5px 40px 2px 10px;font-weight:bold;"></td>
                    <td style="width:70%;text-align:left;">Total Momspligtig - Quickbreve :</td>
                    <td style="width:30%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                        <xsl:call-template name="FormatPrice">
                            <xsl:with-param name="Price" select="sum(key('TotalSubOrders',Priority)/SubOrderSum)" />
                        </xsl:call-template>
                    </td>

                </tr>
                <tr style="background-color:white;color:#999">
                    <td style="width:650px;padding:5px 40px 2px 10px;font-weight:bold;"></td>
                    <td style="width:70%;text-align:left;">Total ekskl. moms :</td>
                    <td style="width:30%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                        <xsl:call-template name="FormatPrice">
                            <xsl:with-param name="Price" select="sum(key('TotalSubOrders',Priority)/SubOrderSumWithoutVAT)" />
                        </xsl:call-template>
                    </td>

                </tr>
                <tr style="background-color:white;color:#999">
                    <td style="width:650px;padding:5px 40px 2px 10px;font-weight:bold;"></td>
                    <td style="width:70%;text-align:left;">
                        *[TEMPLATE_INVOICE_TOTAL_AMOUNT_VAT]*
                        (<xsl:call-template name="FormatPct">
                            <xsl:with-param name="pct" select="key('TotalSubOrders',Priority)/VAT" />
                        </xsl:call-template>)
                        :
                    </td>
                    <td style="width:30%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                        <xsl:call-template name="FormatPrice">
                            <xsl:with-param name="Price" select="sum(key('TotalSubOrders',Priority)/SubOrderSumVAT)" />
                        </xsl:call-template>
                    </td>

                </tr>
            </xsl:if>

            <xsl:if test="Priority != 'A'">
                <tr style="background-color:white;color:#444">
                    <td style="width:650px;padding:5px 40px 2px 10px;font-weight:bold;"></td>
                    <td style="width:70%;text-align:left;">Total Momsfri - Breve :</td>
                    <td style="width:30%;text-align:right;padding:5px 40px 2px 0px; margin:0px;">
                        <xsl:call-template name="FormatPrice">
                            <xsl:with-param name="Price" select="sum(key('TotalSubOrders',Priority)/SubOrderSum)" />
                        </xsl:call-template>
                    </td>

                </tr>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>




    <xsl:template name="FormatDate">
        <xsl:param name="Date"/>

        <xsl:value-of select="concat(
                      substring($Date, 9, 2),
                      '-',
                      substring($Date, 6, 2),
                      '-',
                      substring($Date, 1, 4)
                      )"/>
    </xsl:template>

    <xsl:template name="FormatPrice">
        <xsl:param name="Price" />
        <xsl:if test="$Price != 0">
            DKK <xsl:value-of select='format-number($Price, "#,00", "danish")'/>
        </xsl:if>
        <xsl:if test="$Price = 0">
            DKK 0,00
        </xsl:if>
    </xsl:template>


    <xsl:template name="FormatPct">
        <xsl:param name="pct" /><xsl:value-of select='$pct - 100'/>%
    </xsl:template>

</xsl:stylesheet>
