﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Publishing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace PostNord.Portal.DigitalPostage.Modules.PublishingPages
{
    /// <summary>
    /// Publishing Page where Menus are suppressed
    /// </summary>
    public class NoMenusPage : PublishingLayoutPage
    {

        Label lblError;

        /// <summary>
        /// master page location directory
        /// </summary>
        private const string MINIMAL_MASTER = "/_catalogs/masterpage/PostNord.master";


        /// <summary>
        /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
        /// </summary>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            lblError = new Label();
            Controls.Add(lblError);
        }

        /// <summary>
        /// Initializes the master page that should be used for the current request.
        /// If we are published, change masterpage to minimal mas
        /// </summary>
        /// <param name="e">Event argument.</param>
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            try
            {

                if (SPContext.Current != null)
                {
                    // Change to minimal master i we have a published version
                    if (SPContext.Current.ListItem.File.Level == SPFileLevel.Published)
                    {
                        this.MasterPageFile = MINIMAL_MASTER;
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error switching masterpage: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Initializes the <see cref="T:System.Web.UI.HtmlTextWriter" /> object and calls on the child controls of the <see cref="T:System.Web.UI.Page" /> to render.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> that receives the page content.</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            try
            {
                base.Render(writer);
                if (this.MasterPageFile != MINIMAL_MASTER)
                {
                    writer.Write("Masterpage could not be switched");
                }
            }
            catch(Exception ex)
            {
                writer.Write("Error rendering: {0}<hr/>{1}", ex.Message, ex.StackTrace);
            }
        }

    }
}
