﻿using Microsoft.SharePoint;
using PostNord.Portal.DigitalPostage.Bll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;

namespace PostNord.Portal.DigitalPostage.WebParts.ValidateCode
{
    /// <summary>
    /// Validate Code WebPart
    /// </summary>
    [ToolboxItemAttribute(false)]
    public partial class ValidateCode : WebPart
    {

        int _humanErrorPeriod = 300;
        bool _updateValidationResults = true;

        /// <summary>
        /// Gets or sets the human error period.
        /// </summary>
        /// <value>
        /// The human error period in seconds
        /// </value>
        [WebBrowsable(true),
        WebDisplayName("Human Error period in seconds"),
        Personalizable(PersonalizationScope.Shared),
        Category("Settings")]
        public int HumanErrorPeriod
        {
            get { return _humanErrorPeriod; }
            set { _humanErrorPeriod = value; }
        }

        /// <summary>
        /// Should Validation Results be updated when a request is sent
        /// </summary>
        /// <value>
        /// <c>true</c> if validation result should be updated (default) otherwise, <c>false</c>.
        /// </value>
        [WebBrowsable(true),
        WebDisplayName("Update validations results"),
        Personalizable(PersonalizationScope.Shared),
        Category("Settings")]
        public bool UpdateValidationResults
        {
            get { return _updateValidationResults; }
            set { _updateValidationResults = value; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ValidateCode"/> class.
        /// </summary>
        public ValidateCode()
        {
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            txtCode.Focus();
        }

        /// <summary>
        /// Verifies this instance.
        /// </summary>
        protected void Verify()
        {
            lblError.Text = string.Empty;
            lblIssueDate.Text = string.Empty;
            lblResult.Text = string.Empty;
            lblStatus.Text = string.Empty;
            lblValidUntil.Text = string.Empty;
            lblValue.Text = string.Empty;
            lblProduct.Text = string.Empty;
            lblOrderId.Text = string.Empty;
            lblVerifyCount.Text = string.Empty;
            lblCodeDetails.Text = string.Empty;
            lblCodeId.Text = string.Empty;

            string codeText = txtCode.Text.Trim().Replace("-", " ").Replace("O","0");

            int day = 0;
            int value = 0;
            int codeId = 0;
            float amount = 0;
            string errorMessage;

            Codes codes = new Codes();
            bool error = !codes.GetCodeDetails(codeText, out day, out value, out amount, out codeId, out errorMessage);

            //int result =  Portal.CodeWrapper.GetCodeDetails(codeText, out day, out value, out amount, out number);

            if (error || day == 0 || value == 0)
            {
                lblCodeDetails.Text = string.Format("Error: {0}", errorMessage);
            }
            else
            {
                lblCodeDetails.Text = string.Format("Week: {0} Value: {1} Number: {2}", day, value, codeId);
            }

            if (string.IsNullOrEmpty(codeText))
            {
                lblError.Text = "Search pattern is empty";
                txtCode.Focus();
                return;
            }

            int numberOfDaysValid = Orders.PostageValidInDays;
            //int.TryParse(Helpers.ConfigSettings.GetText("PostageValidInInDays"), out numberOfDaysValid);

            List<Orders> orders = Orders.GetOrdersFromCode(DateTime.Today.Date.AddDays(-1 * numberOfDaysValid), Helpers.HelperClass.Today(), codeText);

            if (orders.Count == 0)
            {
                if (_updateValidationResults)
                {
                    ValidationResults vr = new ValidationResults();
                    vr.CodeText = codeText;
                    vr.Employee = SPContext.Current.Web.CurrentUser.LoginName;
                    vr.ErrorCode = ValidationResults.ValidationValues.DoesNotExist;
                    vr.Timestamp = DateTime.Now;
                    vr.Save();
                }
                lblResult.Text = string.Format("Code: {0} not found! {1}", codeText, Codes.ScrambleCode(codeText));
                tcResult.BackColor = System.Drawing.Color.Red;
                tcResult.ForeColor = System.Drawing.Color.White;
                pnlVerifyNoError.Visible = false;
                pnlResult.Visible = true;
                return;
            }

            if (orders.Count == 1)
            {
                Codes code = new Codes(codeText);

                lblIssueDate.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", code.IssueDate);
                lblValidUntil.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", code.ExpireDate);
                lblStatus.Text = code.Status.ToString();
                lblValue.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", code.Price);
                lblCodeId.Text = code.CodeId.ToString();
                SubOrders subOrder = code.SubOrder;
                lblOrderId.Text = subOrder.OrderId.ToString();
                lblProduct.Text = subOrder.Product;

                List<ValidationResults> vr = ValidationResults.GetValidationResults(DateTime.Now.AddSeconds(this.HumanErrorPeriod * -1), Helpers.HelperClass.Today(), code.Id);
                lblVerifyCount.Text = vr.Count.ToString();

                if (vr.Count > 0 && pnlVerifyNoError.Visible == false)
                {
                    lblVerify.Text = string.Format("The code: {0} has been validated {1} times within the last: {2} seconds.<br/>Please verify that this is NOT a human error. <br/>", codeText, vr.Count, this.HumanErrorPeriod);
                    pnlVerifyNoError.Visible = true;
                    pnlResult.Visible = false;
                    pnlMultipleCodes.Visible = false;
                    return;
                }
                else
                {
                    pnlResult.Visible = true;
                    pnlVerifyNoError.Visible = false;
                    pnlMultipleCodes.Visible = false;
                }

                ValidationResults.ValidationValues valResult = code.Validate(SPContext.Current.Web.CurrentUser.LoginName, _updateValidationResults);

                switch (valResult)
                {
                    case ValidationResults.ValidationValues.OK:
                        lblResult.Text = string.Format("Code: {0} is valid!", codeText);
                        tcResult.BackColor = System.Drawing.Color.Green;
                        tcResult.ForeColor = System.Drawing.Color.White;
                        break;
                    case ValidationResults.ValidationValues.Disabled:
                        lblResult.Text = string.Format("Code: {0}  disabled!", codeText);
                        tcResult.BackColor = System.Drawing.Color.Red;
                        tcResult.ForeColor = System.Drawing.Color.Black;
                        break;
                    case ValidationResults.ValidationValues.Refunded:
                        lblResult.Text = string.Format("Code: {0} Code refunded!", codeText);
                        tcResult.BackColor = System.Drawing.Color.Red;
                        tcResult.ForeColor = System.Drawing.Color.Black;
                        break;
                    case ValidationResults.ValidationValues.AlreadyUsed:
                        lblResult.Text = string.Format("Code: {0} Code already used!", codeText);
                        tcResult.BackColor = System.Drawing.Color.Red;
                        tcResult.ForeColor = System.Drawing.Color.Black;
                        break;
                    case ValidationResults.ValidationValues.Expired:
                        lblResult.Text = string.Format("Code: {0} Code is expired!", codeText);
                        tcResult.BackColor = System.Drawing.Color.Red;
                        tcResult.ForeColor = System.Drawing.Color.Black;
                        break;
                }
            }
            else
            {
                
                pnlResult.Visible = false;
                pnlVerifyNoError.Visible = false;
                pnlMultipleCodes.Visible = true;
                lblResult.Text = string.Format("{0} Codes found matching order!", orders.Count);
                tcResult.BackColor = System.Drawing.Color.Red;
                tcResult.ForeColor = System.Drawing.Color.Black;
                rptMultipleCodes.DataSource = Bll.Codes.GetCodesFromCode(DateTime.Today.Date.AddDays(-1 * numberOfDaysValid), Helpers.HelperClass.Today(),  codeText);
                rptMultipleCodes.DataBind();
            }
            txtCode.Text = "";
            txtCode.Focus();
        }

        /// <summary>
        /// Handles the Click event of the btnValidate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnValidate_Click(object sender, EventArgs e)
        {
            try
            {
                Verify();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnVerify control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnVerify_Click(object sender, EventArgs e)
        {
            try
            {
                Verify();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
    }
}
