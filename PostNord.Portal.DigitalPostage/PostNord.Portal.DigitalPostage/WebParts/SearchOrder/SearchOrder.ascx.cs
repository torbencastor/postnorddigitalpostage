﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebPartPages;
using PostNord.Portal.DigitalPostage.Bll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Linq;
using System.Web.UI;

namespace PostNord.Portal.DigitalPostage.WebParts.SearchOrder
{
    /// <summary>
    /// Search Orders
    /// </summary>
    [ToolboxItemAttribute(false)]
    public partial class SearchOrder : System.Web.UI.WebControls.WebParts.WebPart
    {

        int _pageLength = 25;
        string _viewOrderUrl = "/KundeCenter/pages/vieworder.aspx";


        /// <summary>
        /// Gets or sets the view order URL.
        /// </summary>
        /// <value>
        /// The view order URL.
        /// </value>
        [WebBrowsable(true),
        WebDisplayName("View order URL"),
        Personalizable(PersonalizationScope.Shared),
        Category("Settings")]        
        public string ViewOrderUrl
        {
            get { return _viewOrderUrl; }
            set { _viewOrderUrl = value; }
        }

        /// <summary>
        /// Gets or sets the length of the page.
        /// </summary>
        /// <value>
        /// The length of the page.
        /// </value>
        [WebBrowsable(true),
        WebDisplayName("Page Length"),
        Personalizable(PersonalizationScope.Shared),
        Category("Settings")]
        public int PageLength
        {
            get { return _pageLength; }
            set { _pageLength = value; }
        }

        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        /// <value>
        /// The page number.
        /// </value>
        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                {
                    return Convert.ToInt16(ViewState["PageNumber"]);
                } 
                else
                {
                    return 0;
                }
            }

            set 
            {
                if (value >= 0)
                { 
                    ViewState["PageNumber"] = value;
                }
            }
        }

        private List<DigitalPostage.Bll.Orders> foundOrders = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchOrder"/> class.
        /// </summary>
        public SearchOrder()
        {
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        /// <summary>
        /// Renders the control to the specified HTML writer.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            Page.ClientScript.RegisterForEventValidation(btnSearch.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(btnNext.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(btnNextBottom.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(btnPrev.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(btnPrevBottom.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(drpPlatform.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(drpStatus.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(dtcFromDate.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(dtcFromDate.UniqueID + "Hours", "4");
            Page.ClientScript.RegisterForEventValidation(dtcFromDate.UniqueID + "Minutes", "4");
            Page.ClientScript.RegisterForEventValidation(dtcToDate.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(dtcToDate.UniqueID + "Hours", "4");
            Page.ClientScript.RegisterForEventValidation(dtcToDate.UniqueID + "Minutes", "4");

            base.Render(writer);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SPContext.Current.Web.CurrentUser == null)
            {
                pnlSearchForm.Visible = false;
                pnlSearchResult.Visible = false;
                pnlLookUpOrder.Visible = false;
                lblError.Text = "User is not logged in";
                lblError.Visible = true;
                return;
            }

            if (!Page.IsPostBack)
            {
                dtcFromDate.SelectedDate = DateTime.Now;
                dtcToDate.SelectedDate = DateTime.Now;
                pnlSearchResult.Visible = false;
                PageNumber = 0;
                pnlLookUpOrder.Visible = true;
                pnlSearchForm.Visible = false;
                txtOrderNumber.Focus();
            }
        }

        /// <summary>
        /// Shows the search result.
        /// </summary>
        protected void ShowSearchResult()
        {
            string email = string.Empty;
            DateTime fromDate = DateTime.MinValue;
            DateTime toDate = DateTime.Today;
            string transactionId = string.Empty;

            try
            {
                lblError.Text = "";

                if (!dtcFromDate.IsDateEmpty)
                {
                    fromDate = dtcFromDate.SelectedDate;
                }

                if (!dtcToDate.IsDateEmpty)
                {
                    toDate = dtcToDate.SelectedDate;
                }
                else
                {
                    toDate = DateTime.Today;
                }

                // If transaction Id set Search!
                if (pnlLookUpOrder.Visible && txtOrderNumber.Text != string.Empty)
                {
                    long orderNr = 0;

                    long.TryParse(txtOrderNumber.Text.Trim(), out orderNr);
                    if (orderNr > 0)
                    {
                        foundOrders = new List<Bll.Orders>();
                        DigitalPostage.Bll.Orders order = new Bll.Orders(orderNr);
                        if (order.Id > 0)
                        {
                            string parameter = String.Format("orderid={0}", order.Id);
                            SPUtility.Redirect(ViewOrderUrl, SPRedirectFlags.DoNotEncodeUrl, HttpContext.Current, parameter);
                            //foundOrders.Add(order);
                        }
                    }
                    else
                    {
                        lblError.Text = string.Format("{0} is not a valid receipt number", txtOrderNumber.Text);
                    }
                }
                else if (txtEmail.Text.Trim() != string.Empty)
                {
                    foundOrders = DigitalPostage.Bll.Orders.GetOrders(fromDate, toDate, txtEmail.Text.Trim());
                }
                else if (txtTransactionId.Text.Trim() != string.Empty)
                {
                    foundOrders = DigitalPostage.Bll.Orders.GetOrders(txtTransactionId.Text.Trim());
                } 
                else if (txtCode.Text.Trim() != string.Empty)
                {
                    string code = txtCode.Text.Replace(" ", "").Replace("-","");
                    if (code.Length != 12)
                    {
                        lblError.Text = "Code length is not correct";
                        return;
                    }
                    foundOrders = DigitalPostage.Bll.Orders.GetOrdersFromCode(fromDate, toDate, code);
                }
                else
                {
                    foundOrders = DigitalPostage.Bll.Orders.GetOrders(fromDate, toDate);
                }

                if (txtAmount.Text != string.Empty)
                {
                    float amount = 0;

                    try
                    {
                        amount = float.Parse(txtAmount.Text.Replace('.', ','), new System.Globalization.CultureInfo("da-DK"));
                        foundOrders = foundOrders.FindAll(
                            delegate(Orders order)
                            {
                                return (order.Saldo == amount);
                            }
                        );
                    }
                    catch 
                    {
                        lblError.Text = string.Format("'{0}' is not a valid amount.", txtAmount.Text);
                    }
                }

                if (drpPaymentSearch.SelectedValue != string.Empty)
                {
                    foundOrders = foundOrders.FindAll(
                        delegate(Orders order)
                        {
                            if (drpPaymentSearch.SelectedValue == "Nets")
                            {
                                return (order.PaymentMethod == Orders.PaymentMethods.Nets || order.PaymentMethod == Orders.PaymentMethods.NetsRecurring);
                            }
                            else
                            {
                                return order.PaymentMethod == Orders.PaymentMethods.MobilePay;
                            }
                        }
                    );
                }

                // If platform filter select matching orders
                if (drpPlatform.SelectedValue != string.Empty)
                {
                    foundOrders = foundOrders.FindAll(
                        delegate(Orders order)
                        {
                            return order.AppPlatform == drpPlatform.SelectedValue;
                        }
                    );
                }

                // If status filter select matching orders
                if (drpStatus.SelectedValue != string.Empty)
                {
                    foundOrders = foundOrders.FindAll(
                        delegate(Orders order)
                        {
                            return order.Status.ToString() == drpStatus.SelectedValue;
                        }
                    );
                }

                // If Images filter select matching orders
                if (chkImagesOnly.Checked)
                {
                    foundOrders = foundOrders.FindAll(
                        delegate(Orders order)
                        {
                            return order.ImageSize > 0;
                        }
                    );
                }

                if (foundOrders == null)
                {
                    lblError.Text = "No orders found.";
                    return;
                }
                if (foundOrders.Count == 0)
                {
                    lblError.Text = "Number of orders found: 0";
                    //return;
                }

                string status = drpStatus.Text;
                string platform = drpPlatform.Text;
                bool imagesOnly = chkImagesOnly.Checked;

                drpStatus.Items.Clear();
                drpStatus.Items.Add(new ListItem("All", ""));
                chkImagesOnly.Enabled = false;
                chkImagesOnly.Checked = false;

                foreach(Orders order in foundOrders)
                {
                    if (drpStatus.Items.FindByValue(order.Status.ToString()) == null)
                    {
                        drpStatus.Items.Add(new ListItem(order.Status.ToString(), order.Status.ToString()));
                    }
                    if (order.ImageSize > 0)
                    {
                        chkImagesOnly.Enabled = true;
                    }
                }

                // Reset CheckBox
                if (chkImagesOnly.Enabled)
                {
                    chkImagesOnly.Checked = imagesOnly;
                }

                // Reset Platforms
                ListItem selected = drpPlatform.Items.FindByText(platform);
                if (selected != null)
                {
                    selected.Selected = true;
                }
                else
                {
                    drpPlatform.Items[0].Selected = true;
                }

                // Reset Status
                selected = drpStatus.Items.FindByText(status);
                if (selected != null)
                {
                    selected.Selected = true;
                }
                else
                {
                    drpStatus.Items[0].Selected = true;
                }


                pnlSearchResult.Visible = true;

                lblResultCount.Text = string.Format("Number of orders found: {0}", foundOrders.Count);

                PagedDataSource pds = new PagedDataSource();
                pds.DataSource = foundOrders.OrderByDescending(o => o.Id).ToList(); ;
                pds.AllowPaging = true;
                pds.PageSize = _pageLength;
                pds.CurrentPageIndex = PageNumber;
                rptSearchResult.DataSource = pds;
                rptSearchResult.DataBind();

                int pageCount = Convert.ToInt32( Math.Ceiling((decimal)foundOrders.Count / (decimal)pds.PageSize));

                pnlPagingTop.Visible = pnlPagingButton.Visible = (pageCount > 1);
                lblPaging.Text = lblPagingBottom.Text = string.Format("Showing Page: {0} of {1} pages.", (PageNumber+1), pageCount);

                btnPrev.Enabled = btnPrevBottom.Enabled = PageNumber > 0;
                btnNext.Enabled = btnNextBottom.Enabled = (PageNumber + 1 < pageCount);

            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Search Exception: {0}", ex.Message);
            }
            //drpPlatform.Attributes.Add("onchange", "showWait()");
            //drpStatus.Attributes.Add("onchange", "showWait()");
            //chkImagesOnly.Attributes.Add("onclick", "showWait()");
        }

        /// <summary>
        /// Handles the Click event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
            PageNumber = 0;
            pnlSearchResult.Visible = false;

            if (pnlLookUpOrder.Visible)
            {
                if (string.IsNullOrEmpty(txtOrderNumber.Text))
                {
                    lblError.Text = "Please enter receipt number";
                    pnlSearchForm.Visible = true;
                    return;
                }
                long num = 0;
                long.TryParse(txtOrderNumber.Text, out num);
                if (num <= 0)
                {
                    lblError.Text = "Receipt number is not a valid number";
                    pnlSearchForm.Visible = true;
                    return;
                }
            }
            else
            {
                if (dtcFromDate.IsDateEmpty || dtcToDate.IsDateEmpty)
                {
                    dtcFromDate.SelectedDate = DateTime.Now.Date;
                    dtcFromDate.SelectedDate = DateTime.Now.Date.AddHours(23);
                }

                if (dtcFromDate.SelectedDate > dtcToDate.SelectedDate)
                {
                    DateTime tmp = dtcFromDate.SelectedDate;
                    dtcFromDate.SelectedDate = dtcToDate.SelectedDate;
                    dtcToDate.SelectedDate = tmp;
                }
            }

            ShowSearchResult();
        }

        /// <summary>
        /// Handles the Click event of the btnPrev control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnPrev_Click(object sender, EventArgs e)
        {
            PageNumber--;
            ShowSearchResult();
        }

        /// <summary>
        /// Handles the Click event of the btnNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void btnNext_Click(object sender, EventArgs e)
        {
            PageNumber++;
            ShowSearchResult();
        }

        /// <summary>
        /// Handles the Click event of the btnAdvanced control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnAdvanced_Click(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
            pnlLookUpOrder.Visible = false;
            pnlSearchForm.Visible = true;
            pnlSearchResult.Visible = true;

            if (dtcFromDate.IsDateEmpty)
            {
                dtcFromDate.SelectedDate = DateTime.Now.Date;
            }

            if (dtcToDate.IsDateEmpty)
            {
                dtcToDate.SelectedDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
            }

            if (dtcFromDate.SelectedDate.Date > dtcToDate.SelectedDate.Date)
            {
                dtcToDate.SelectedDate = dtcFromDate.SelectedDate;
            }

            txtEmail.Focus();
        }

        /// <summary>
        /// Handles the Click event of the btnSimple control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnSimple_Click(object sender, EventArgs e)
        {
            pnlLookUpOrder.Visible = true;
            pnlSearchForm.Visible = false;
            pnlSearchResult.Visible = false;
            txtOrderNumber.Focus();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the drpStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void drpStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageNumber = 0;
            ShowSearchResult();
        }

        /// <summary>
        /// Handles the CheckedChanged event of the chkImagesOnly control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void chkImagesOnly_CheckedChanged(object sender, EventArgs e)
        {
            PageNumber = 0;
            ShowSearchResult();
        }



    }
}
