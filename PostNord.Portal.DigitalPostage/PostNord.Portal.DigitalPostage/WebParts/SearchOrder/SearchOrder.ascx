﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchOrder.ascx.cs" Inherits="PostNord.Portal.DigitalPostage.WebParts.SearchOrder.SearchOrder" %>

<style type="text/css">


     .header {
         background-color:#e3e3e3;
     }

     .odd {
         background-color:#f3f3f3;
     }

     .table {
         border : 1px solid #d3d3d3;
     }


    #tblSearchResult th,#tblSearchResult td, #refinements th, #refinements td {
        padding:5px 10px 5px 5px;
    }

    .dateBox {

    }

    .boxWidth {
        width:141px;
    }

     #tblSearchResult .col5,
     #tblSearchResult .col6,
     #tblSearchResult .col7
     {
        text-align:right;
     }

    #searchForm {
        width:480px;
    }

    #advancedForm {
        width:550px;
    }

    #searchForm th, #advancedForm th {
        text-align:left;
    }

    #tblSearchResult th, #refinements th {
        text-align:left;
    }

    #searchForm .error {
        color:red;
    }

    #resultCount {
        padding:5px 5px 15px 5px;
    }

    .pagingPanel{
        margin-top:15px;
        margin-bottom:15px;
    }

    .error {
        color:red;
    }

    .wideDrop {
        width: 175px;
    }

</style>

<script src="/Style%20Library/DigitalPostage/js/jquery-1.9.1.js" type="text/javascript"></script>

<script type="text/javascript">

    String.prototype.insertAt = function (index, string) {
        return this.substr(0, index) + string + this.substr(index);
    }

    function keypressed(textbox) {
        code = textbox.value.replace(/-/g, '').replace(/ /g, '').toUpperCase();

        if (code.length > 4) {
            code = code.insertAt(4, '-');
        }

        if (code.length > 9) {
            code = code.insertAt(9, '-');
        }
        textbox.value = code;
    }

    function showDialog(ordernr) {
        var options = SP.UI.$create_DialogOptions();
        options.title = "Order details";
        options.width = 950;
        options.height = 750;
        options.url = '<%=this.ViewOrderUrl %>?isDlg=1&orderid=' + ordernr;
        SP.UI.ModalDialog.showModalDialog(options);
    }


</script>

<h1>Search order</h1>

<asp:Panel ID="pnlLookUpOrder" runat="server">
   <table id="searchForm">
        <tr>
            <th>Receipt number:</th>
            <td>
                <asp:TextBox ID="txtOrderNumber" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnOpenOrder" runat="server" Text="Open" OnClick="btnSearch_Click"  />&nbsp;<asp:Button ID="btnAdvanced" runat="server" Text="Advanced" OnClick="btnAdvanced_Click"  />
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:UpdatePanel runat="server" ID="wtf">
  <ContentTemplate>

    <asp:UpdatePanel ID="pnlSearchResult" runat="server" RenderMode="Block" UpdateMode="Always">

        <ContentTemplate>
        <asp:Panel ID="pnlSearchForm" runat="server">
            <table id="advancedForm">
                <tr>
                    <th>From date:</th>
                    <td>
                        <SharePoint:DateTimeControl ID="dtcFromDate" runat="server" Calendar="Gregorian" DateOnly="false" LocaleId="1030" CssClassTextBox="dateBox" />
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="Lookup" OnClick="btnSearch_Click" />&nbsp;<input value="Simple" onclick="location.href = location.href" type="button" />
                    </td>
                </tr>
                <tr>
                    <th>To date:</th>
                    <td>
                        <SharePoint:DateTimeControl ID="dtcToDate" runat="server" Calendar="Gregorian" DateOnly="false" LocaleId="1030" CssClassTextBox="dateBox" />
                    </td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td style="text-wrap:none">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="boxWidth"></asp:TextBox> <i>Use * as wildcard</i>
                    </td>
                </tr>
                <tr>
                    <th>Transaction Id:</th>
                    <td>
                        <asp:TextBox ID="txtTransactionId" runat="server" CssClass="boxWidth"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <th>Amount:</th>
                    <td>
                        <asp:TextBox ID="txtAmount" runat="server" CssClass="boxWidth"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <th>Payment method:</th>
                    <td>
                        <asp:DropDownList ID="drpPaymentSearch" runat="server" CssClass="boxWidth">
                            <asp:ListItem Value="" Selected="True">All</asp:ListItem>
                            <asp:ListItem Value="MobilePay">MobilePay</asp:ListItem>
                            <asp:ListItem Value="Nets">NETS</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <th>Platform</th>
                    <td>
                        <asp:DropDownList ID="drpPlatform" runat="server" CssClass="boxWidth">
                            <asp:ListItem Value="" Selected="True">All</asp:ListItem>
                            <asp:ListItem Value="Android">Android</asp:ListItem>
                            <asp:ListItem Value="IOS">IOS</asp:ListItem>
                            <asp:ListItem Value="WebShop">WebShop</asp:ListItem>
                            <asp:ListItem Value="WP">Windows Phone</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <th>Code:</th>
                    <td>
                        <asp:TextBox ID="txtCode" runat="server" onkeyup="keypressed(this)" CssClass="boxWidth"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </asp:Panel>

            <div style="padding-top:10px">
                <asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>
            </div>

            <h2>Refinements</h2>

            <table>
                <tr>
                    <td style="vertical-align:top; width:360px;">
                        <table style="width:200px" id="refinements" class="table">
                            <tr class="header">
                                <th>Status</th>
                                <th>Image only</th>
                                </tr>
                                <tr>
                                <td><asp:DropDownList ID="drpStatus" runat="server" CssClass="wideDrop" OnSelectedIndexChanged="drpStatus_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                                <td><asp:CheckBox ID="chkImagesOnly" runat="server" AutoPostBack="true" OnCheckedChanged="chkImagesOnly_CheckedChanged" /></td>
                            </tr>
                        </table>
                    </td>
                    <td style="vertical-align:middle; width:150px; padding:15px">
                        <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="pnlSearchResult" runat="server">
                            <ProgressTemplate>            
                                <img src="/_layouts/images/gears_anv4.gif" alt="Progress" style="margin-right:10px;" />
                                Searching...            
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>

        <div id="resultCount">
            <asp:Label ID="lblResultCount" runat="server" Text="No search results found."></asp:Label>
        </div>

        <asp:Panel ID="pnlPagingTop" runat="server" CssClass="pagingPanel" Visible="false">
            <asp:Button ID="btnPrev" runat="server" Text="<<" OnClick="btnPrev_Click" ToolTip="Show previous page" />
            <asp:Label ID="lblPaging" runat="server" Text=""></asp:Label>
            <asp:Button ID="btnNext" runat="server" Text=">>" OnClick="btnNext_Click" ToolTip="Show next page" />
        </asp:Panel>

        <asp:Repeater ID="rptSearchResult" runat="server">
            <HeaderTemplate>
                <table id="tblSearchResult" class="table">
                    <tr class="header">
                        <th>
                            Receipt No.
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            SalesDate
                        </th>
                        <th>
                            ExpireDate
                        </th>
                        <th>
                           ReceiptNumber
                        </th>
                        <th>
                            Price
                        </th>
                        <th>
                            Credit
                        </th>
                        <th>
                            Saldo
                        </th>
                        <th>
                            Platform
                        </th>
                        <th>
                            Payment
                        </th>
                        <th>
                            ImageSize
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="col1">
                        <%#  String.Format("<a href='#' onclick=\"showDialog('{0}');return false;\">{0}<a>", DataBinder.Eval(Container.DataItem, "Id")) %>
                    </td>
                    <td class="col2">
                        <%#  Eval("Email") %>
                    </td>
                    <td class="col2">
                        <%#  Eval("Status") %>
                    </td>
                    <td class="col3">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:dd-MM-yyyy HH:mm}", DataBinder.Eval(Container.DataItem, "SalesDate")) %>
                    </td>
                    <td class="col4">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem, "ExpireDate")) %>
                    </td>
                    <td class="col5">
                        <%#  Eval("ReceiptNumber") %>
                    </td>
                    <td class="col6">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "OrderSum"))  %>
                    </td>
                    <td class="col6">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "CreditSum"))  %>
                    </td>
                    <td class="col6">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Saldo"))  %>
                    </td>
                    <td class="col7">
                        <%#  Eval("AppPlatform") %>
                    </td>
                    <td class="col7">
                        <%#  Eval("PaymentMethod") %>
                    </td>
                    <td class="col7">
                        <%# DataBinder.Eval(Container.DataItem, "ImageSize")  %>
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="odd">
                    <td class="col1">
                        <%#  String.Format("<a href='#' onclick=\"showDialog('{0}');return false;\">{0}<a>", DataBinder.Eval(Container.DataItem, "Id")) %>
                    </td>
                    <td class="col2">
                        <%#  Eval("Email") %>
                    </td>
                    <td class="col2">
                        <%#  Eval("Status") %>
                    </td>
                    <td class="col3">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:dd-MM-yyyy HH:mm}", DataBinder.Eval(Container.DataItem, "SalesDate")) %>
                    </td>
                    <td class="col4">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem, "ExpireDate")) %>
                    </td>
                    <td class="col5">
                        <%#  Eval("ReceiptNumber") %>
                    </td>
                    <td class="col6">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "OrderSum"))  %>
                    </td>
                    <td class="col6">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "CreditSum"))  %>
                    </td>
                    <td class="col6">
                        <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Saldo"))  %>
                    </td>
                    <td class="col7">
                        <%#  Eval("AppPlatform") %>
                    </td>
                    <td class="col7">
                        <%#  Eval("PaymentMethod") %>
                    </td>
                    <td class="col7">
                        <%# DataBinder.Eval(Container.DataItem, "ImageSize")  %>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Panel ID="pnlPagingButton" runat="server" CssClass="pagingPanel" Visible="false">
            <asp:Button ID="btnPrevBottom" runat="server" Text="<<" OnClick="btnPrev_Click" ToolTip="Show previous page" />
            <asp:Label ID="lblPagingBottom" runat="server" Text=""></asp:Label>
            <asp:Button ID="btnNextBottom" runat="server" Text=">>" OnClick="btnNext_Click" ToolTip="Show next page" />
        </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>

  </ContentTemplate>

</asp:UpdatePanel>
