﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ValidateCodeViaUrl.ascx.cs" Inherits="PostNord.Portal.DigitalPostage.WebParts.ValidateCodeViaUrl.ValidateCodeViaUrl" %>


<style type="text/css">

     .header {
         background-color:#e3e3e3;
     }

     .odd {
         background-color:#f3f3f3;
     }

     .table {
         border : 1px solid #d3d3d3;
         width:400px;
         margin-top:15px;
     }

     .wide {
         width:600px;
     }

     .table th, .table td {
        text-align:left;
        padding:3px;
     }
     
    .pagingPanel{
        margin-top:15px;
        margin-bottom:15px;
    }

    .error {
        color:red;
    }

</style>

<script type="text/javascript">

    String.prototype.insertAt = function (index, string) {
        return this.substr(0, index) + string + this.substr(index);
    }

   

</script>


    <h1>Validate Code</h1>

    <asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>

    <asp:Panel ID="pnlVerifyNoError" runat="server" Visible="false">
        <h2>Verify Validation</h2>
        <asp:Label runat="server" Text="" ID="lblVerify"></asp:Label>
        <asp:Button runat="server" Text="Verify" ID="btnVerify" OnClick="btnVerify_Click"></asp:Button>
    </asp:Panel>

    <asp:Panel ID="pnlResult" runat="server" Visible="false">
        <asp:Table ID="tblResult" runat="server" CssClass="table">
            <asp:TableRow CssClass="odd">
                <asp:TableCell ID="tcResult" ColumnSpan="2">
                    <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell>Status:</asp:TableHeaderCell>
                <asp:TableCell><asp:Label ID="lblStatus" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow CssClass="odd">
                <asp:TableHeaderCell>Issue date:</asp:TableHeaderCell>
                <asp:TableCell><asp:Label ID="lblIssueDate" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell>Expire date:</asp:TableHeaderCell>
                <asp:TableCell><asp:Label ID="lblValidUntil" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow CssClass="odd">
                <asp:TableHeaderCell>Value:</asp:TableHeaderCell>
                <asp:TableCell><asp:Label ID="lblValue" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell>Product:</asp:TableHeaderCell>
                <asp:TableCell><asp:Label ID="lblProduct" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow CssClass="odd">
                <asp:TableHeaderCell>Order id:</asp:TableHeaderCell>
                <asp:TableCell><asp:Label ID="lblOrderId" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell>Code id:</asp:TableHeaderCell>
                <asp:TableCell><asp:Label ID="lblCodeId" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow CssClass="odd">
                <asp:TableHeaderCell>Verified count:</asp:TableHeaderCell>
                <asp:TableCell><asp:Label ID="lblVerifyCount" runat="server" Text="-"></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell>Code details:</asp:TableHeaderCell>
                <asp:TableCell><asp:Label ID="lblCodeDetails" runat="server" Text="-"></asp:Label></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <asp:Panel ID="pnlMultipleCodes" runat="server" Visible="false">

        <h2>Multiple codes were found</h2>

        <asp:Repeater ID="rptMultipleCodes" runat="server">
        <HeaderTemplate>
            <table class="table wide">
                <tr class="header">
                    <th>
                        Id
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Issue date
                    </th>
                    <th>
                        Expire Date
                    </th>
                    <th>
                        Used date 
                    </th>
                    <th>
                        Value
                    </th>
                    <th>
                        Product
                    </th>
                    <th>
                       Order id
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="col1">
                    <%#  String.Format("<a href='?codeid={0}' onclick='alert('Not implemented!'); return false;'>{0}<a>", DataBinder.Eval(Container.DataItem, "CodeId")) %>
                </td>
                <td class="col2">
                    <%#  Eval("Status") %>
                </td>
                <td class="col2">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem, "IssueDate")) %>
                </td>
                <td class="col3">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem, "ExpireDate")) %>
                </td>
                <td class="col4">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem, "UsedDate")) %>
                </td>
                <td class="col4">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Price")) %>
                </td>
                <td class="col5">
                    <%#  Eval("SubOrder.Product") %>
                </td>
                <td class="col6">
                    <%#  Eval("SubOrder.Order.Id") %>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>

        </asp:Repeater>


    </asp:Panel>
