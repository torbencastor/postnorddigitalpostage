﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewOrder.ascx.cs" Inherits="PostNord.Portal.DigitalPostage.WebParts.ViewOrder.ViewOrder" %>


<link rel="stylesheet" href="/Style%20Library/DigitalPostage/css/jquery-ui.css" />
<script src="/Style%20Library/DigitalPostage/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="/Style%20Library/DigitalPostage/js/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" href="/Style%20Library/DigitalPostage/css/style.css" />

<script type="text/javascript">

    String.prototype.insertAt = function (index, string) {
        return this.substr(0, index) + string + this.substr(index);
    }

    function keypressed(textbox) {
        code = textbox.value.replace(/-/g, '').replace(/ /g, '').toUpperCase();

        if (code.length > 4) {
            code = code.insertAt(4, '-');
        }

        if (code.length > 9) {
            code = code.insertAt(9, '-');
        }
        textbox.value = code;
    }

    function sortTable(tblId, col, dir) {
        var rows = $(tblId + ' tbody tr:gt(0)').removeClass('odd').get();
        var less = -1;
        var more = 1;
        if (!dir) {
            less = 1;
            more = -1;
        }

        rows.sort(function (a, b) {
            var A = $(a).children('td').eq(col).text().toUpperCase();
            var B = $(b).children('td').eq(col).text().toUpperCase();
            if (A < B) {
                return less;
            }

            if (A > B) {
                return more;
            }

            return 0;
        });
        $.each(rows, function (index, row) {
            $(tblId).children('tbody').append(row);
            if (index % 2 == 1) {
                $(row).addClass('odd');
            }
        });
    }

    function sortCol(tbl, col, linkObj) {
        var tblId = '#' + tbl;

        // if col is already selected reverse order unless we already are reversed
        if ($(linkObj).hasClass("selected") && !$(linkObj).hasClass("desc")) {
            $(linkObj).addClass('desc');
            sortTable(tblId, col, true);
            return false;
        }

        // clear all selected links and add selected class to current link
        $(tblId + ' tr.header .sortlink').removeClass('selected').removeClass('desc');
        $(linkObj).addClass('selected');
        sortTable(tblId, col, false);
        return false;
    }

    var firstClick = true;
    $(function () {
        $("#tabs").tabs();
        $("#s4-ribbonrow").hide();
        $("#RibbonContainer").hide();

        $('#paymentInfo').click(function () {
            if (firstClick) {
                $('.btnUpdate').click();
            }
            firstClick = false;
        });

    });

    function changeCode() {
        var value = $('#srcCode').val();
        $('#srcCode').val(value);
        $("#codes tr").each(function (index) {
            if (index != 0) {

                $row = $(this);

                var id = $row.find(".col4").text();
                if (id.indexOf(value) != 0) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
            }
        });

    }

    function clearCode() {
        if ($('#srcCode').val() != '') {
            $('#srcCode').val('');
            changeCode();
            $('#srcCode').focus();
        }
    }

    function showRefund(refundId) {
        document.URL = document.URL + '&refundId=' + refundId;
    }

    function selAll(ctrl) {
        var checked = ctrl.checked;
        $("#codes tr").each(function (index) {
            if (index != 0) {
                $row = $(this);
                var input = $row.find("input");
                if (!input.prop('disabled')) {
                    input.prop('checked', checked)
                }
            }
        });
    }

    function closeWindow() {
        SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel);
    }

</script>

<style type="text/css">

    .header {
        background-color:#e3e3e3;
    }

    .odd {
        background-color:#f3f3f3;
    }

    .table {
        border : 1px solid #d3d3d3;
    }

    .sortlink {
        text-decoration:none;
    }

   

    a:link   {color:green;}
    a:visited {color:green;}
    a:hover   {color:red;}
    a:active  {color:yellow;}


    a.sortlink:link, a.sortlink:visited, a.sortlink:hover, a.sortlink:active {
        background-color:#e3e3e3;
        color:#0072bc;
        text-decoration: none;
    }

    a.selected:link, a.selected:visited, a.selected:hover, a.selected:active {
        background-color:#e3e3e3;
        text-decoration: underline;
    }

    #orderDetails {
        width: 900px;
        height:50px;
        margin-top: 15px;
        margin-bottom: 20px;
    }

    #refundInfo {
        padding-left: 10px;
        margin-bottom: 5px;
    }

    #refundInfo td, #refundInfo th {
        vertical-align:top;
    }

    #refundInfo th {
        background-color:#e3e3e3;
    }

    #log, #subOrders {
        width: 500px;
    }
     
    #codes {
        width: 700px;
    }

    #refunds th,#refunds td {
        text-align:left;
        padding-left:10px;
    }

    #log th.col2 {
        width:200px;
    }

    #orderDetails th, #log th, #code th {
        text-align:left;
        padding-right:10px;
    }

    #codes th.col1 { width: 50px; text-align:left;  }
    #codes th.col2 { width: 200px; text-align:left; }
    #codes th.col3 { width: 75px; text-align:right; padding-right:10px; padding-left:10px; }
    #codes th.col4 { width: 125px; text-align:left; }
    #codes th.col5 { width: 125px; text-align:left; }
    #codes th.col6 { width: 125px; text-align:left; }
    #codes th.col7 { width: 75px; text-align:left; }

    #codes td.col3, #codes td.col7  {
      text-align:right;
      padding-right:10px;
      padding-left:10px;
    }

    #refundInfo {
        margin-top:10px;
    }

    #refundInfo th {
        text-align:left;
        padding-right:20px;
    }

    .refundText {
        width:250px;
    }

    hr {
        height:2px;
    }

    #subOrders th,#subOrders td {
        text-align:left;
        padding:5px 10px 5px 0px;
    }

    #subOrders td.col1 {
        width:250px;
    }

    .right {
        text-align:right;
    }

     #subOrders th.col2,
     #subOrders th.col3,
     #subOrders th.col4 {
        text-align:right;
     }

     #subOrders td.col2,
     #subOrders td.col3,
     #subOrders td.col4
     {
        text-align:right;
        padding-right:10px;
     }

     #log th.col1 {
         width:120px;
     }
     #log th.col2 {
         width:75px;
     }
     #log th.col3 {
         width:305px;
     }

    .error {
        color:red;
    }

    .nowrap {
        white-space: nowrap;
    }

</style>


<h1>Order details</h1>

<asp:Label ID="lblInfo" runat="server" Text=""></asp:Label>
<asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>

<asp:Panel ID="pnlViewOrder" runat="server">

    <table id="orderDetails">
        <tr>
            <th>Receipt number:</th>
            <td>
                <asp:Label ID="txtOrderNumber" runat="server" Text="" EnableViewState="true"></asp:Label>
            </td>
            <th>Date:</th>
            <td>
                <asp:Label ID="txtDate" runat="server" Text="" EnableViewState="true"></asp:Label>
            </td>
            <th>Expire date:</th>
            <td>
                <asp:Label ID="txtExpireDate" runat="server" Text="" EnableViewState="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <th>Status:</th>
            <td>
                <asp:Label ID="txtStatus" runat="server" Text="" EnableViewState=""></asp:Label>
            </td>
            <th>Transaction id:</th>    
            <td>
                <asp:Label ID="lblTransactionId" runat="server" Text="" EnableViewState="true"></asp:Label>
            </td>
            <th>Email:</th>
            <td>
                <asp:Label ID="txtEmail" runat="server" Text="" EnableViewState="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <th>Updated in CINT</th>
            <td>
                <asp:Label ID="lblCint" runat="server" Text="" EnableViewState=""></asp:Label>
            </td>
            <th>Payment Method:</th>
            <td>
                <asp:Label ID="lblPaymentMethod" runat="server" Text="" EnableViewState=""></asp:Label>
            </td>
            <th>App Type:</th>
            <td>
                <asp:Label ID="lblAppType" runat="server" Text="" EnableViewState=""></asp:Label>
            </td>
        </tr>
    </table>

    <div id="tabs">
      <ul>
        <li><a href="#tabs-1">Codes</a></li>
        <li><a href="#tabs-2" id="paymentInfo">Payment</a></li>
        <li><a href="#tabs-3">Products</a></li>
        <li><a href="#tabs-4">Log</a></li>
        <li><a href="#tabs-5">Refunds</a></li>
        <li><a href="#tabs-6">Image</a></li>
      </ul>


      <div id="tabs-1">
        <asp:Panel ID="pnlCustomerInteraction" runat="server">
            <h2>Customer Interaction</h2>

         <div id="searchCode">
            <b>Search code:</b>
            <input id="srcCode" type="text" onkeyup="keypressed(this); changeCode()" />&nbsp;<input type="button" value="Clear" onclick="clearCode()" />&nbsp;<input type="button" value="Close" onclick="    closeWindow()" />
         </div>
        <asp:Repeater ID="rptCodes" runat="server" EnableViewState="true">
            <HeaderTemplate>
                <table id="codes" class="table">
                    <tr class="header">
                        <th class="col1"><input type="checkbox" id="chkSelAll" onclick="selAll(this)" title="Select all" /></th>
                        <th class="col4"><a href="#" onclick="sortCol('codes', 1, this);" class="sortlink">Code</a></th>
                        <th class="col2"><a href="#" onclick="sortCol('codes', 2, this);" class="sortlink">Product</a></th>
                        <th class="col3">Price</th>
                        <th class="col5"><a href="#" onclick="sortCol('codes', 4, this);" class="sortlink">Status</a></th>
                        <th class="col7"><a href="#" onclick="sortCol('codes', 5, this);" class="sortlink">Refund id</a></th>
                        <th class="col6">Refund date</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td class="col1">
                            <asp:CheckBox ID="chkCode" runat="server" Enabled='<%# Eval("CanRefund") %>' onclick="clearCode()" />
                            <asp:HiddenField ID="hidCodeId" runat="server" Value ='<%# Eval("Id") %>'  />
                        </td>
                        <td class="col4"><%# Eval("PostCode") %></td>
                        <td class="col2"><%# Eval("SubOrder.Product") %></td>
                        <td class="col3 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Price"))  %></td>
                        <td class="col5"><%# Eval("StatusLimited") %></td>
                        <td class="col7"><%# Eval("RefundId") %></td>
                        <td class="col6 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem, "RefundedDate")) %></td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr class="odd">
                        <td class="col1">
                            <asp:CheckBox ID="chkCode" runat="server" Enabled='<%# Eval("CanRefund") %>' onclick="clearCode()" />
                            <asp:HiddenField ID="hidCodeId" runat="server" Value ='<%# Eval("Id") %>'  />
                        </td>
                        <td class="col4"><%# Eval("PostCode") %></td>
                        <td class="col2"><%# Eval("SubOrder.Product") %></td>
                        <td class="col3 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Price"))  %></td>
                        <td class="col5"><%# Eval("StatusLimited") %></td>
                        <td class="col7"><%# Eval("RefundId") %></td>
                        <td class="col6 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem, "RefundedDate")) %></td>
                    </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>  
            <asp:ValidationSummary ID="valSummary" runat="server" EnableClientScript="true" ShowSummary="true" ValidationGroup="Refund" />
            <table id="refundInfo" class="table">
                <tr>
                    <th>Name:</th>
                    <td>
                        <asp:TextBox ID="txtRefundName" runat="server" Text="" CssClass="refundText"></asp:TextBox>
                        <asp:RequiredFieldValidator 
                            ID="reqName" runat="server" ErrorMessage="Name is missing"  ControlToValidate="txtRefundName"
                            ValidationGroup="Refund" Text="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td>
                        <asp:TextBox ID="txtRefundAddress" runat="server" Text="" CssClass="refundText"></asp:TextBox>
                        <asp:RequiredFieldValidator 
                            ID="reqAddress" runat="server" ErrorMessage="Address is missing"  ControlToValidate="txtRefundAddress"
                            ValidationGroup="Refund" Text="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>Post nr.</th>
                    <td>
                        <asp:TextBox ID="txtRefundPostNr" runat="server" Text="" CssClass="refundText"></asp:TextBox>
                        <asp:RequiredFieldValidator 
                            ID="reqPostNr" runat="server" ErrorMessage="Post nr. is missing"  ControlToValidate="txtRefundPostNr"
                            ValidationGroup="Refund" Text="*"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cv" runat="server" ControlToValidate="txtRefundPostNr" Type="Integer"
                            Operator="DataTypeCheck" ErrorMessage="Post nr. must be a number" />
                    </td>
                </tr>
                <tr>
                    <th>City</th>
                    <td>
                        <asp:TextBox ID="txtRefundCity" runat="server" Text="" CssClass="refundText"></asp:TextBox>
                         <asp:RequiredFieldValidator 
                            ID="reqCity" runat="server" ErrorMessage="City is missing"  ControlToValidate="txtRefundCity"
                            ValidationGroup="Refund" Text="*"></asp:RequiredFieldValidator>
                   </td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td>
                        <asp:TextBox ID="txtRefundPhone" runat="server" Text="" CssClass="refundText"></asp:TextBox>
                        <asp:RequiredFieldValidator 
                            ID="reqPhone" runat="server" ErrorMessage="Phone is missing"  ControlToValidate="txtRefundPhone"
                            ValidationGroup="Refund" Text="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>Message</th>
                    <td>
                        <asp:TextBox ID="txtRefundMessage" runat="server" TextMode="MultiLine" 
                            Columns="60" Rows="3" Text="" CssClass="refundText">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnRefund" runat="server" Text="Refund" OnClick="btnRefund_Click" ValidationGroup="Refund" CausesValidation="true" />
                        <asp:Button ID="btnResend" runat="server" Text="Resend mail" OnClick="btnResend_Click" ValidationGroup="Refund" CausesValidation="true" />
                        <input type="button" value="Close" onclick="closeWindow()" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
      </div>
      <div id="tabs-2">
            <h2>Payment information</h2>
            <asp:UpdatePanel ID="updUpdateOrder" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td colspan="2" style="padding-bottom:15px; height:200px; vertical-align:top">
                                <p>
                                    <asp:Label runat="server" ID="lblUpdateResult"></asp:Label>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Panel ID="pnlUpdateOrderStatus" runat="server" Visible="false">
                                    <p>
                                        <asp:Label ID="lblUpdateOrderStatusText" runat="server"></asp:Label>
                                        
                                    </p>
                                    <p>
                                        <asp:RadioButtonList ID="rblOrderChoice" runat="server">
                                            <asp:ListItem Value="UpdateToPaid" Selected="True">
                                                <b>Update status to PAID</b><br />
                                                A mail containing receipt and codes will be sent to the customer.
                                            </asp:ListItem>
                                            <asp:ListItem Value="RefundOrder" Selected="False">
                                                <b>Refund entire order</b><br />
                                                A mail containing refund information will be sent to the customer.
                                            </asp:ListItem>
                                            <asp:ListItem Value="CancelOrder" Selected="False" Enabled="false">
                                                <b>Cancel reservation</b><br />
                                                Customer will not be informed by this operation.
                                            </asp:ListItem>
                                        </asp:RadioButtonList>
                                        <br />
                                    </p>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnUpdateOrderStatus" runat="server" Text="Update order status" OnClick="btnUpdateOrderStatus_Click" Visible="false" />
                                <asp:Button ID="btnUpdate" runat="server" Text="Get Payment Information" OnClick="btnUpdatePayment_Click" ValidationGroup="" CausesValidation="false" CssClass="btnUpdate" />
                                <input type="button" value="Close" onclick="closeWindow()" />
                            </td>
                            <td style="padding-left:20px">
                                <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="updUpdateOrder" runat="server">
                                    <ProgressTemplate>            
                                        <img src="/_layouts/images/gears_anv4.gif" alt="Progress" style="margin-right:10px;" />
                                        Calling payment provider...
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
      </div>
      <div id="tabs-3">
        <asp:Repeater ID="rptSubOrders" runat="server" EnableViewState="true">
            <HeaderTemplate>
                <table id="subOrders" class="table">
                    <tr class="header">
                        <th class="col1">Product</th>
                        <th class="col1">Destination</th>
                        <th class="col2">Amount</th>
                        <th class="col3">Item price</th>
                        <th class="col4">Price</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td class="col1"><%# Eval("Product") %></td>
                        <td class="col1"><%# Eval("Destination") %></td>
                        <td class="col2"><%# Eval("Amount") %></td>
                        <td class="col3 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Price"))  %></td>
                        <td class="col4 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "SubOrderSum"))  %></td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr class="odd">
                        <td class="col1"><%# Eval("Product") %></td>
                        <td class="col1"><%# Eval("Destination") %></td>
                        <td class="col2"><%# Eval("Amount") %></td>
                        <td class="col3 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Price"))  %></td>
                        <td class="col4 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "SubOrderSum"))  %></td>
                    </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:Repeater>  

        <asp:Repeater ID="rptCredit" runat="server" EnableViewState="true">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td class="col1">Credit Nr: <%# Eval("Id") %></td>
                        <td class="col2"></td>
                        <td class="col2"></td>
                        <td class="col3"></td>
                        <td class="col4 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "CreditSum"))  %></td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr class="odd">
                        <td class="col1">Credit Nr: <%# Eval("Id") %></td>
                        <td class="col2"></td>
                        <td class="col2"></td>
                        <td class="col3"></td>
                        <td class="col4 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "CreditSum"))  %></td>
                    </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                    <tr class="header">
                        <th class="col1">Total</th>
                        <th class="col2"></th>
                        <th class="col2"></th>
                        <th class="col3"></th>
                        <th class="col4 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", order.Saldo)  %></t>
                    </tr>
                </table>
            </FooterTemplate>
        </asp:Repeater>  
        <div style="padding:10px;">
            <asp:HyperLink ID="hypShowReceipt" runat="server">Show Receipt</asp:HyperLink>
            &nbsp;
            <asp:HyperLink ID="hypShowCodes" runat="server">Show Codes</asp:HyperLink>
        </div>
      </div>
      <div id="tabs-4">
        <asp:Repeater ID="rptLog" runat="server" EnableViewState="true">
            <HeaderTemplate>
                <table id="log" class="table">
                    <tr class="header">
                        <th class="col1">Timestamp</th>
                        <th class="col2">Severity</th>
                        <th class="col3">Message</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td class="col1 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d} {0:t}", DataBinder.Eval(Container.DataItem, "Timestamp"))  %></td>
                        <td class="col2"><%# Eval("Severity") %></td>
                        <td class="col3"><%# Eval("Text") %></td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr class="odd">
                        <td class="col1 nowrap"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d} {0:t}", DataBinder.Eval(Container.DataItem, "Timestamp"))  %></td>
                        <td class="col2"><%# Eval("Severity") %></td>
                        <td class="col3"><%# Eval("Text") %></td>
                    </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>  
      </div>
      <div id="tabs-5">
        <asp:Repeater ID="rptRefunds" runat="server" EnableViewState="true">
            <HeaderTemplate>
                <table id="refunds" class="table">
                    <tr class="header">
                        <th>Id</th>
                        <th>CINT updated</th>
                        <th>Timestamp</th>
                        <th>Name</th>
                        <th>Credit</th>
                        <th>Address</th>
                        <th>Post nr.</th>
                        <th>City</th>
                        <th>Phone</th>
                        <th>Message</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td><a href='<%# Eval("RefundUrl") %>' title="Show credit note"><%# Eval("Id") %></a></td>
                        <td><%# Eval("SentToCint") %></td>
                        <td><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d} {0:t}", DataBinder.Eval(Container.DataItem, "Timestamp"))  %></td>
                        <td><%# Eval("Name") %></td>
                        <td><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "CreditSum")) %></td>
                        <td><%# Eval("Address") %></td>
                        <td><%# Eval("PostNumber") %></td>
                        <td><%# Eval("City") %></td>
                        <td><%# Eval("Phone") %></td>
                        <td><%# Eval("Message") %></td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr class="odd">
                        <td><a href='<%# Eval("RefundUrl") %>' title="Show credit note"><%# Eval("Id") %></a></td>
                        <td><%# Eval("SentToCint") %></td>
                        <td><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d} {0:t}", DataBinder.Eval(Container.DataItem, "Timestamp"))  %></td>
                        <td><%# Eval("Name") %></td>
                        <td><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "CreditSum")) %></td>
                        <td><%# Eval("Address") %></td>
                        <td><%# Eval("PostNumber") %></td>
                        <td><%# Eval("City") %></td>
                        <td><%# Eval("Phone") %></td>
                        <td><%# Eval("Message") %></td>
                    </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>  
      </div>
      <div id="tabs-6">
          <asp:Image ID="imgLabel" runat="server" CssClass="imageLabel" Visible="false" />
          <div class="imgInfo">
              <asp:Label ID="lblImageInfo" runat="server"></asp:Label>
          </div>
      </div>
    </div>
</asp:Panel>

