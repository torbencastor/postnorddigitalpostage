﻿using iTextSharp.text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using PostNord.Portal.DigitalPostage.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace PostNord.Portal.DigitalPostage.WebParts.ViewOrder
{
    /// <summary>
    /// View Order Webpart
    /// </summary>
    [ToolboxItemAttribute(false)]
    public partial class ViewOrder : WebPart
    {

        string RBL_ORDER_CHOICE_NO_CODES_TEXT = "This order is paid according to the payment provider, but no codes have been generated.<br />You can either refund the entire order or update status on the order to paid. <br />";
        string RBL_ORDER_CHOICE_NO_CODES_ITEM_TEXT = "<b>Generate codes</b><br />A mail containing receipt and codes will be sent to the customer.";

        /// <summary>
        /// Gets the order identifier from URL
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        public long OrderId
        {
            get
            {
                long orderId = 0;
                if (HttpContext.Current.Request["Id"] != null)
                {
                    long.TryParse(HttpContext.Current.Request["Id"], out orderId);
                }
                if (HttpContext.Current.Request["OrderId"] != null)
                {
                    long.TryParse(HttpContext.Current.Request["OrderId"], out orderId);
                }
                return orderId;
            }
        }

        private DigitalPostage.Bll.Orders order = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewOrder"/> class.
        /// </summary>
        public ViewOrder()
        {
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }


        private void ShowRefund()
        {
            order = new Bll.Orders(this.OrderId);
            Bll.Refunds refund = null;
            int refundId = 0;
            int.TryParse(HttpContext.Current.Request["refundid"], out refundId);

            if (refundId <= 0)
            {
                lblError.Text = "Refund Id is not valid";
                lblError.Visible = true;
                return;
            }
            try
            {
                refund = new Bll.Refunds(refundId);
                if (refund.Id == 0)
                {
                    throw new ApplicationException("Refund not found!");
                }
                if (refund.OrderNr != order.Id)
                {
                    throw new ApplicationException("Refund does not belong to order!");
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Refund could not be loaded. {0}", ex.Message);
                lblError.Visible = true;
                return;
            }

            try
            {

                if (HttpContext.Current.Request["debug"] == "xml")
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "text/xml";
                    HttpContext.Current.Response.Write(refund.Xml());
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                    return;
                }

                string receiptTemplateUrl = ConfigSettings.GetText("CreditNotaPDFTemplate", "/Style%20Library/DigitalPostage/templates/CreditNotePDF.xslt");
                string html = refund.GetHtml(receiptTemplateUrl);

                if (HttpContext.Current.Request["debug"] == "html")
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "text/html";
                    if (html == string.Empty)
                    {
                        HttpContext.Current.Response.Write(string.Format("No HTML, using template: {0}", receiptTemplateUrl));
                    }
                    else
                    {
                        HttpContext.Current.Response.Write(html);
                    }
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                    return;
                }

                byte[] pdfReceipt = Portal.DigitalPostage.Helpers.Pdf.CreatePdf(html, PageSize.A4, 10f, 10f, 10f, 0f);
                string PdfFilename = string.Format("Refund{0}_{1}.pdf", order.Id, refund.Id);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", PdfFilename));
                using (MemoryStream ms = new MemoryStream(pdfReceipt))
                {
                    ms.WriteTo(HttpContext.Current.Response.OutputStream);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error showing Receipt. {0}", ex.Message);
                lblError.Visible = true;
            }
        }

        private void ShowCodes()
        {
            order = new Bll.Orders(this.OrderId);
            try
            {
                string labelTemplateUrl = ConfigSettings.GetText("LabelTemplate");

                byte[] pdfLabels = order.GenerateLabels(labelTemplateUrl);

                if (pdfLabels == null)
                {
                    lblError.Text = "All labels are refunded. PDF could not be generated.";
                    lblError.Visible = true;
                    return;
                }

                string PdfFilename = string.Format("Postage_{0}.pdf", order.CaptureId);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", PdfFilename));
                using (MemoryStream ms = new MemoryStream(pdfLabels))
                {
                    ms.WriteTo(HttpContext.Current.Response.OutputStream);
                    ms.Close();
                }

                try
                {
                    //HttpContext.Current.Response.Flush();
                    //HttpContext.Current.Response.End();
                }
                catch { }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error showing labels. {0}", ex.Message);
                lblError.Visible = true;
            }
        }

        private void ShowReceipt()
        {
            order = new Bll.Orders(this.OrderId);

            try
            {

                if (HttpContext.Current.Request["debug"] == "xml")
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "text/xml";
                    HttpContext.Current.Response.Write(order.Xml());
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                    return;
                }

                string receiptTemplateUrl = ConfigSettings.GetText("ReceiptPdfTemplate");
                if (receiptTemplateUrl == string.Empty)
                {
                    throw new ApplicationException("Config setting: 'ReceiptPdfTemplate' missing!");
                }

                if (order.AppPlatform != "WebShop") 
                {
                    receiptTemplateUrl = ConfigSettings.GetText("ReceiptPdfTemplateWS");
                    if (receiptTemplateUrl == string.Empty)
                    {
                        throw new ApplicationException("Config setting: 'ReceiptPdfTemplateWS' missing!");
                    }
                }
                
                string html = order.GetHtml(receiptTemplateUrl);

                if (HttpContext.Current.Request["debug"] == "html")
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "text/html";
                    HttpContext.Current.Response.Write(html);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                    return;
                }

                byte[] pdfReceipt = Portal.DigitalPostage.Helpers.Pdf.CreatePdf(html, PageSize.A4, 10f, 10f, 10f, 0f);
                string PdfFilename = string.Format("Receipt{0}.pdf", order.CaptureId);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", PdfFilename));
                using (MemoryStream ms = new MemoryStream(pdfReceipt))
                {
                    ms.WriteTo(HttpContext.Current.Response.OutputStream);
                    ms.Close();
                }
                try
                {
                    //HttpContext.Current.Response.Flush();
                    //HttpContext.Current.Response.End();
                }
                catch { }

            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error showing Receipt. {0}", ex.Message);
                lblError.Visible = true;
            }
        }


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SPContext.Current.Web.CurrentUser == null)
            {
                pnlViewOrder.Visible = false;
                lblError.Text = "User is not logged in";
                lblError.Visible = true;
                return;
            }

            if (this.OrderId == 0)
            {
                lblError.Text = "Id not set";
                valSummary.Enabled = false;
                valSummary.Visible = false;
                reqAddress.Enabled = false;
                reqCity.Enabled = false;
                reqName.Enabled = false;
                reqPostNr.Enabled = false;  
                reqPhone.Enabled = false;
                return;
            }

            try
            {
                if (!Page.IsPostBack)
                {

                    hypShowCodes.NavigateUrl = string.Format("{0}?orderid={1}&showcodes=1", HttpContext.Current.Request.Url.AbsolutePath,  this.OrderId);
                    hypShowReceipt.NavigateUrl = string.Format("{0}?orderid={1}&showreceipt=1", HttpContext.Current.Request.Url.AbsolutePath, this.OrderId);

                    order = new Bll.Orders(this.OrderId);

                    if (HttpContext.Current.Request["showimg"] != null && order.ImageSize > 0)
                    {
                        PostNord.Portal.DigitalPostage.Helpers.Image.ShowImage(order.CaptureId, order.LabelImage);
                    }

                    if (HttpContext.Current.Request["showreceipt"] != null)
                    {
                        ShowReceipt();
                        return;
                    }

                    if (HttpContext.Current.Request["showcodes"] != null)
                    {
                        ShowCodes();
                        return;
                    }

                    if (HttpContext.Current.Request["refundId"] != null)
                    {
                        ShowRefund();
                        return;
                    }

                    txtDate.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d} {0:t}", order.SalesDate);
                    txtExpireDate.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", order.ExpireDate);
                    txtOrderNumber.Text = string.Format("{0}", order.Id);
                    txtStatus.Text = order.Status.ToString();
                    lblAppType.Text = string.Format("{0}  version: {1}", order.AppPlatform, order.AppVersion);
                    lblPaymentMethod.Text = order.PaymentMethod.ToString();

                    // If Version 1
                    if (order.AppPlatform == "WebShop" && order.AppVersion == 1)
                    {
                        lblCint.Text = "%";
                    }
                    else
                    {
                        if (order.SentToCint)
                        {
                            lblCint.Text = "Yes";
                            lblCint.ForeColor = System.Drawing.Color.Green;
                        }
                        else
                        {
                            lblCint.Text = "No";
                            lblCint.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    if (order.Status != Bll.Orders.OrderStatusValues.Paid)
                    {
                        txtStatus.ForeColor = System.Drawing.Color.Red;
                    } else
                    {
                        txtStatus.ForeColor = System.Drawing.Color.Green;
                    }
                    lblTransactionId.Text = order.CaptureId;
                    //lblReceiptNr.Text = order.ReceiptNumber;
                    txtEmail.Text = order.Email;

                    if (order.ImageSize > 0)
                    {
                        try
                        {
                            System.Drawing.Image img = PostNord.Portal.DigitalPostage.Helpers.Image.ByteArrayToImage(order.LabelImage);
                            string filename = order.CaptureId + PostNord.Portal.DigitalPostage.Helpers.Image.GetImageExtension(img);
                            
                            imgLabel.Visible = true;
                            imgLabel.ImageUrl = string.Format("{0}&showimg=1", HttpContext.Current.Request.Url.AbsoluteUri);
                            imgLabel.Width = img.Width;
                            imgLabel.Height = img.Height;

                            string url = string.Format("<a href='{0}' target='_blank'>{1}</a>", imgLabel.ImageUrl, filename);
                            lblImageInfo.Text = string.Format("<b>Dimensions: </b>{0}x{1}px <br/><b>Filename: </b>{2}<br/>", img.Width, img.Height, url);

                        }
                        catch (Exception ex)
                        {
                            imgLabel.Visible = false;
                            lblImageInfo.Text = string.Format("Error reading Image: {0}", ex.Message);
                        }
                    } 
                    else
                    {
                        lblImageInfo.Text = "No Image";
                    }

                    rptSubOrders.DataSource = order.SubOrders;
                    rptSubOrders.DataBind();

                    rptCredit.DataSource = order.Refunds;
                    rptCredit.DataBind();

                    rptLog.DataSource = order.Logs;
                    rptLog.DataBind();

                    rptRefunds.DataSource = order.Refunds;
                    rptRefunds.DataBind();

                    List<Bll.Codes> codes = new List<Bll.Codes>();
                    
                    // Add codes for all SubOrders
                    foreach (Bll.SubOrders subOrder in order.SubOrders)
                    {
                        codes.AddRange(subOrder.Codes);
                    }
                    
                    // Enable Resend button if no codes or no receipt number
                    btnResend.Visible = btnRefund.Visible = (codes.Count > 0 && !string.IsNullOrEmpty(order.ReceiptNumber) && order.Status == Bll.Orders.OrderStatusValues.Paid);

                    rptCodes.DataSource = codes;
                    rptCodes.DataBind();
                }
            }
            catch(Exception ex)
            {
                lblError.Text = string.Format("Error loading order: {0}", ex.Message);
                lblError.Visible = true;
            }
        }

        /// <summary>
        /// Updates the nets payment.
        /// </summary>
        protected void UpdateNetsPayment(Bll.Orders order)
        {
            lblUpdateResult.ForeColor = System.Drawing.Color.Black;
            lblUpdateResult.Text = "<b>Nets payment status:</b><br/>";
            try
            {
                var cult = new System.Globalization.CultureInfo("da-DK");
                var paymentInfo = order.GetNetsPaymentInfo();

                float amountCaptured = (float.Parse(paymentInfo.Summary.AmountCaptured) / 100);
                float amountCredited = (float.Parse(paymentInfo.Summary.AmountCredited) / 100);

                lblUpdateResult.Text += string.Format("Authorized: {0}<br/>", paymentInfo.Summary.Authorized);
                lblUpdateResult.Text += string.Format(cult, "Amount captured: {0:C}<br/>", amountCaptured);
                lblUpdateResult.Text += string.Format(cult, "Amount credited: {0:C}<br/>", amountCredited);
                lblUpdateResult.Text += string.Format("Payment Method: {0}<br/>", paymentInfo.CardInformation.PaymentMethod);
                lblUpdateResult.Text += "<br/><b>History: </b><br/>";
                foreach (var line in paymentInfo.History)
                {
                    lblUpdateResult.Text += string.Format("{0:dd-MM-yy HH:mm:ss} - {1} {2}<br/>", line.DateTime, line.Operation, (string.IsNullOrEmpty(line.Description)?"":" / " + line.Description));
                }

                if (paymentInfo.Error != null && paymentInfo.ErrorLog != null && paymentInfo.ErrorLog.Length > 0)
                {
                    lblUpdateResult.Text += string.Format("<br/><b>Errors:</b><br/>");
                    foreach (var line in paymentInfo.ErrorLog)
                    {
                        lblUpdateResult.Text += string.Format("{0:dd-MM-yy HH:mm:ss} - {1} | Code: {2} Source: {3} Text: {4}<br/>", line.DateTime, line.Operation, line.ResponseCode, line.ResponseSource, line.ResponseText);
                    }
                }

                if (paymentInfo.Summary.Authorized && amountCaptured == order.OrderSum && amountCredited == 0 && order.Status != Bll.Orders.OrderStatusValues.Paid)
                {
                    btnUpdateOrderStatus.Visible = pnlUpdateOrderStatus.Visible = true;
                    lblUpdateOrderStatusText.Text = string.Format("This order is paid according to the payment provider, but its current status is <i>{0}</i>.<br />You can either refund the entire order or update status on the order to paid. <br />", order.Status);
                }
                else if (paymentInfo.Summary.Authorized && amountCaptured == order.OrderSum && amountCredited == 0 && order.Status == Bll.Orders.OrderStatusValues.Paid && order.GetCodes().Count == 0)
                {
                    btnUpdateOrderStatus.Visible = pnlUpdateOrderStatus.Visible = true;
                    lblUpdateOrderStatusText.Text = RBL_ORDER_CHOICE_NO_CODES_TEXT;
                    rblOrderChoice.Items[0].Text = RBL_ORDER_CHOICE_NO_CODES_ITEM_TEXT;
                }
                else
                {
                    btnUpdateOrderStatus.Visible = pnlUpdateOrderStatus.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblUpdateResult.ForeColor = System.Drawing.Color.Red;
                lblUpdateResult.Text += string.Format("Error: {0}",  ex.Message);
            }
        }

        /// <summary>
        /// Updates the mobile pay payment.
        /// </summary>
        protected void UpdateMobilePayPayment(Bll.Orders order)
        {
            lblUpdateResult.ForeColor = System.Drawing.Color.Black;
            lblUpdateResult.Text = "<b>MobilePay status:</b><br/>";
            try
            {
                var paymentInfo = order.GetMobilePayPaymentInfo();
                var cult = new System.Globalization.CultureInfo("da-DK");
                string price = string.Format(cult, "{0} kr.", paymentInfo.OriginalAmount);
                lblUpdateResult.Text += string.Format("Captured: {0}<br/>", paymentInfo.LatestPaymentStatus == "CAP");
                lblUpdateResult.Text += string.Format("Amount captured: {0}<br/>", price);
                lblUpdateResult.Text += string.Format("TransactionId: {0}<br/>", paymentInfo.OriginalTransactionId);
                lblUpdateResult.Text += string.Format("Refunded: {0}<br/>", paymentInfo.LatestPaymentStatus == "REF");
                lblUpdateResult.Text += string.Format("Latest payment status: {0}<br/>", paymentInfo.LatestPaymentStatus);
                lblUpdateResult.Text += string.Format("Return code: {0} ({1})<br/>", paymentInfo.ReturnCode, Helpers.MobilePay.V2Returncode(paymentInfo.ReturnCode));
                lblUpdateResult.Text += string.Format("Reason code: {0} ({1})<br/>", paymentInfo.ReasonCode, Helpers.MobilePay.V2Reasoncode(paymentInfo.ReasonCode));

                if (!string.IsNullOrEmpty(paymentInfo.OriginalTransactionId) && string.IsNullOrEmpty(order.CaptureId))
                {
                    order.CaptureId = paymentInfo.OriginalTransactionId;
                    order.Save();
                }

                if (paymentInfo.transactions != null && paymentInfo.transactions.Length > 0)
                {
                    lblUpdateResult.Text += "<br/><b>Transactions:</b><br/>";
                    foreach (var transaction in paymentInfo.transactions)
                    {
                        price = string.Format(cult, "{0} kr.", transaction.Amount);
                        DateTime time = SPUtility.CreateDateTimeFromISO8601DateTimeString(transaction.TimeStamp);
                     
                        lblUpdateResult.Text += string.Format("{0:dd-MM-yy HH:mm:ss} Amount: {1} Status: {2} TransactionId: {3}<br/>", time, price, transaction.PaymentStatus, transaction.TransactionId);
                    }
                }

                if (paymentInfo.LatestPaymentStatus == Helpers.MobilePay.PAYMENTSTATUS_CAPTURED && order.Status != Bll.Orders.OrderStatusValues.Paid)
                {
                    btnUpdateOrderStatus.Visible = pnlUpdateOrderStatus.Visible = true;
                }
                else if (paymentInfo.LatestPaymentStatus == Helpers.MobilePay.PAYMENTSTATUS_CAPTURED && order.Status == Bll.Orders.OrderStatusValues.Paid && order.GetCodes().Count == 0)
                {
                    btnUpdateOrderStatus.Visible = pnlUpdateOrderStatus.Visible = true;
                    lblUpdateOrderStatusText.Text = RBL_ORDER_CHOICE_NO_CODES_TEXT;
                    rblOrderChoice.Items[0].Text = RBL_ORDER_CHOICE_NO_CODES_ITEM_TEXT;
                }
                else if (paymentInfo.LatestPaymentStatus == Helpers.MobilePay.PAYMENTSTATUS_RESERVED)
                {
                    btnUpdateOrderStatus.Visible = pnlUpdateOrderStatus.Visible = true;
                    rblOrderChoice.Items.FindByValue("UpdateToPaid").Enabled = false;
                    rblOrderChoice.Items.FindByValue("RefundOrder").Enabled = false;
                    System.Web.UI.WebControls.ListItem cancel = rblOrderChoice.Items.FindByValue("CancelOrder");
                    cancel.Enabled = true;
                    rblOrderChoice.SelectedValue = cancel.Value;
                }
                else
                {
                    btnUpdateOrderStatus.Visible = pnlUpdateOrderStatus.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblUpdateResult.ForeColor = System.Drawing.Color.Red;
                lblUpdateResult.Text += string.Format("Error: {0}", ex.Message);
            }
        }

        private void RefreshPage()
        {
            try
            {
                this.Page.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
            }
            catch { }
        }


        /// <summary>
        /// Handles the Click event of the btnUpdateOrderStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnUpdateOrderStatus_Click(object sender, EventArgs e)
        {
            order = new Bll.Orders(this.OrderId);
            switch (rblOrderChoice.SelectedValue)
            {
                case "RefundOrder":
                    try
                    {
                        order.RefundOrder();
                        Thread.Sleep(1000);
                        RefreshPage();
                    }
                    catch (Exception ex)
                    {
                        lblUpdateResult.ForeColor = System.Drawing.Color.Red;
                        lblUpdateResult.Text = string.Format("Error refunding order: {0}", ex.Message);
                    }
                    break;
                case "UpdateToPaid":
                    try
                    {
                        order.RepairOrder(true);
                        Thread.Sleep(1000);
                        RefreshPage();
                    }
                    catch (Exception ex)
                    {
                        lblUpdateResult.ForeColor = System.Drawing.Color.Red;
                        lblUpdateResult.Text = string.Format("Error repairing order: {0}", ex.Message);
                    }
                    break;

                case "CancelOrder" :
                    try
                    {
                        order.CancelOrder();
                        Thread.Sleep(1000);
                        RefreshPage();
                    }
                    catch (Exception ex)
                    {
                        lblUpdateResult.ForeColor = System.Drawing.Color.Red;
                        lblUpdateResult.Text = string.Format("Error cancelling order: {0}", ex.Message);
                    }
                    break;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnUpdatePayment control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnUpdatePayment_Click(object sender, EventArgs e)
        {
            try
            {
                order = new Bll.Orders(this.OrderId);
                if (order.Status == Bll.Orders.OrderStatusValues.Initialized)
                {
                    lblUpdateResult.Text = "Payment has not been sent to a provider.";
                    return;
                }

                switch (order.PaymentMethod)
                {
                    case Bll.Orders.PaymentMethods.Nets:
                    case Bll.Orders.PaymentMethods.NetsRecurring:
                        UpdateNetsPayment(order);
                        break;
                    case Bll.Orders.PaymentMethods.MobilePay:
                        UpdateMobilePayPayment(order);
                        break;
                    default:
                        lblUpdateResult.Text = string.Format("Payment not determined ({0})", order.PaymentMethod);
                        break;
                }
            }
            catch (Exception ex)
            {
                lblUpdateResult.ForeColor = System.Drawing.Color.Red;
                lblUpdateResult.Text = string.Format("Error: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnResend_Click(object sender, EventArgs e)
        {
            try
            {
                order = new Bll.Orders(this.OrderId);
                if (order.Id > 0 && order.Status == Bll.Orders.OrderStatusValues.Paid)
                {

                    string log = string.Format("<b>Mail resent to:</b> {0}<br/> <b>Name:</b> {1}<br/><b>Address:</b> {2}<br/><b>Post nr.:</b> {3}<br/><b>City:</b> {4}<br/><b>Phone:</b> {5}<br/><b>Message:</b><br/>{6}",
                        Microsoft.SharePoint.Utilities.SPEncode.HtmlEncode(order.Email),
                        Microsoft.SharePoint.Utilities.SPEncode.HtmlEncode(txtRefundName.Text),
                        Microsoft.SharePoint.Utilities.SPEncode.HtmlEncode(txtRefundAddress.Text),
                        Microsoft.SharePoint.Utilities.SPEncode.HtmlEncode(txtRefundPostNr.Text),
                        Microsoft.SharePoint.Utilities.SPEncode.HtmlEncode(txtRefundCity.Text),
                        Microsoft.SharePoint.Utilities.SPEncode.HtmlEncode(txtRefundPhone.Text),
                        Microsoft.SharePoint.Utilities.SPEncode.HtmlEncode(txtRefundMessage.Text));
                    order.AddLog(log);
                    order.SendMail();

                    lblInfo.Text = string.Format("Mail sent to : {0}", order.Email);

                    if (order.Refunds.Count > 0)
                    {
                        foreach (var refund in order.Refunds)
                        {
                            refund.SendMail();
                        }
                        lblInfo.Text += string.Format("<br/>{0} refund mails resent", order.Refunds.Count);
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Mail send error: {0}", ex.Message);
                lblError.Visible = true;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnRefund control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnRefund_Click(object sender, EventArgs e)
        {
            try
            {
                order = new Bll.Orders(this.OrderId);
                Bll.Refunds refund = null;

                List<Bll.Codes> refundCodes = new List<Bll.Codes>();
                foreach (RepeaterItem i in rptCodes.Items)
                {
                    //Retrieve the state of the CheckBox
                    CheckBox cb = (CheckBox)i.FindControl("chkCode");

                    if (cb.Checked)
                    {
                        //Retrieve the value associated with that CheckBox
                        HiddenField codeIdFld = (HiddenField)i.FindControl("hidCodeId");
                        long codeId = long.Parse(codeIdFld.Value);
                        Bll.Codes code = new Bll.Codes(codeId);
                        refundCodes.Add(code);
                    }
                }

                if (refundCodes.Count == 0)
                {
                    lblError.Text = "You have not selected any codes to refund.";
                } else
                {
                    try
                    {
                        // Create Refund object
                        refund = new Bll.Refunds();
                        refund.Address = txtRefundAddress.Text;
                        refund.City = txtRefundCity.Text;
                        refund.Message = txtRefundMessage.Text;
                        refund.Name = txtRefundName.Text;
                        refund.OrderNr = order.Id;
                        refund.Phone = txtRefundPhone.Text;
                        refund.PostNumber = int.Parse(txtRefundPostNr.Text);
                        refund.Timestamp = DateTime.Now;
                        refund.RefundCodes(refundCodes);
                        refund.SendMail();
                        order.AddLog("Refund mail sent");

                        if (order.Saldo == 0)
                        {
                            order.SetStatus(Bll.Orders.OrderStatusValues.Credited);
                        }
                    }
                    catch (Exception ex)
                    {
                        lblError.Text = string.Format("Error refunding: {0}", ex.Message);
                        lblError.Visible = true;
                        order.AddLog(string.Format(new System.Globalization.CultureInfo("da-DK"), "Error Refunding. Error message: {0}", ex.Message), Bll.OrderLogs.SeverityValues.Error);
                        return;
                    }

                    // Reload page
                    try
                    {
                        HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.PathAndQuery);
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Refund error: {0}", ex.Message);
                lblError.Visible = true;
            }
        }
    }
}
