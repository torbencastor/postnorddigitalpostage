﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderStatus.ascx.cs" Inherits="PostNord.Portal.DigitalPostage.WebParts.OrderStatus.OrderStatus" %>

<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}"></script>
<link rel="stylesheet" href="/Style%20Library/DigitalPostage/css/jquery-ui.css" />
<script src="/Style%20Library/DigitalPostage/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="/Style%20Library/DigitalPostage/js/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" href="/Style%20Library/DigitalPostage/css/style.css" />

  
<script type="text/javascript">

    function showDialog(ordernr) {
        var options = SP.UI.$create_DialogOptions();
        options.title = "Order details";
        options.width = 950;
        options.height = 750;
        options.url = '<%=this.ViewOrderUrl %>?isDlg=1&orderid=' + ordernr;
        SP.UI.ModalDialog.showModalDialog(options);
    }


    $(function () {
        $("#tabs").tabs();
        // copy onchange event from dateselect input to hour + min dropdown
        var s = $("input.dateBox").first().attr('onchange');
        $(".ms-dttimeinput select").attr("onchange", s);

        // Update graph when tab is clicked
        $("#tabs a").first().click(function () {
            updateGraph(true);
        });

        var refreshTime = '<%=this.RefreshTime %>';

        if (refreshTime > 0) {
            var refreshId = setInterval(function () {
                btnRefresh = $('#<%=btnUpdate.ClientID %>').click();
            }, refreshTime * 1000);
        }
    });

    function pageLoad() {
        updateGraph(false);
    }

    var lastHourCount = 0;
    var lastDate = "";

    function updateGraph(force) {

        if ($("#tabs div[aria-hidden='false']").index() != 1) {
            return;
        }

        var hourCount = $('#<%=drpTimeSelector.ClientID %>')[0].value;
        var date = $('#<%=startDate.ClientID %>_startDateDate')[0].value;
        var methodUrl = "";
        var xAxisTitle = "";

        if (hourCount <= 24) {
            methodUrl = "/_layouts/DigitalPostage/LogData.aspx/GraphDataDay";
            xAxisTitle = "Hour";
        } else {
            methodUrl = "/_layouts/DigitalPostage/LogData.aspx/GraphDataDays";
            xAxisTitle = "Day";
        }

        if (hourCount != lastHourCount || date != lastDate || force == true) {

            $.ajax({
                type: "POST",
                url: methodUrl,
                data: "{date:'" + date + "', hourCount:" + hourCount + ", status:2}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (msg) {
                    var data = msg.d;

                    var options = {
                        fontSize: '8px',
                        fontName: '"Arial"',
                        title: 'Orders',
                        vAxis: {
                            title: 'Number of orders',
                            viewWindow: {
                                min: 0
                            }
                        },
                        hAxis: { title: xAxisTitle },
                        seriesType: 'line',
                        series: { 0: { type: 'line' } }
                    };

                    data = google.visualization.arrayToDataTable(data);

                    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                    chart.draw(data, options);

                },
                error: function (msg) {
                    alert(msg.responseText);
                }
            });
        }
        lastDate = date;
        lastHourCount = hourCount;
    }


</script>

<style type="text/css">
  .header {
        background-color:#e3e3e3;
    }

    .odd {
        background-color:#f3f3f3;
    }

    .table {
        border : 1px solid #d3d3d3;
    }


    .table th.col1 { width: 120px; text-align:left;  }
    .table th.col2 { width: 85px; text-align:right; }
    .table th.col3 { width: 75px; text-align:right; }
    .table th.col4 { width: 75px; text-align:right; }
    .table th.col5 { width: 100px; text-align:right; }
    .table th.col6 { width: 75px; text-align:right; }
    .table th.col7 { width: 75px; text-align:right; }
    .table th.col8 { width: 75px; text-align:right; }
    .table th.col9 { width: 75px; text-align:right; }

    .table td  { text-align:right; padding-right:10px; }
    .table td.platform { text-align:left; }

    #tabs {
        width:925px;
    }

    .time {
        height: 20px;
        margin-top:10px;
        margin-bottom:10px;
    }

    .timeInfo {
        padding-top:7px;
        padding-bottom:7px;
    }

    .timeSelector {
        float : right;
    }

    .ok {
        background-color:lightgreen;
    }

    .notok {
        background-color:orangered;
    }

    .warning {
        background-color:orange;
    }

    .timeSelector div {
        float:left;
        margin-right:5px;
    }

    .buttons img {
        height:15px;
        margin-right:5px;
        width: 15px;
    }

    .error {
        color:red;
    }

    #chart_div {
        width: 900px; 
        height: 500px;
    }

    #errorLog th.col1 {
        width:60px;
    }

    #errorLog th.col2 {
        width:150px;
    }

    #errorLog th.col4 {
        width:600px;
        text-align:left;
    }

    #errorLog td.left {
        text-align:left;
    }

    #errorMenu {
        float:right;
    }

    #errorButtons, #errorDateSelector {
        float:left;
        margin-left:8px;
    }

</style>

<asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>
<asp:Label ID="lblInfo" runat="server" Text=""></asp:Label>

<div id="debug"></div>

<div id="tabs">
    <ul>
        <li><a href="#tabs-1">Order Status</a></li>
        <li><a href="#tabs-2">Errors</a></li>
        <li><a href="#tabs-3">Log Search</a></li>
    </ul>

      <div id="tabs-1">

        <asp:DataGrid runat="server" ID="dgData" AutoGenerateColumns="true"></asp:DataGrid>

        <asp:UpdatePanel ID="upOrderStatus" runat="server" RenderMode="Block" UpdateMode="Conditional">

          <ContentTemplate>
            <div class="time">
                <div class="timeSelector">
                    <div>
                        <asp:DropDownList ID="drpTimeSelector" runat="server" OnSelectedIndexChanged="drpTimeSelector_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="Hour" Value="1" ></asp:ListItem>
                            <asp:ListItem Text="Day" Value="24" ></asp:ListItem>
                            <asp:ListItem Text="Week" Value="168" ></asp:ListItem>
                            <asp:ListItem Text="Month (30 days)" Value="720" ></asp:ListItem>
                            <asp:ListItem Text="Year" Value="8760" ></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="dateSelector">
                    <SharePoint:DateTimeControl ID="startDate" runat="server" Calendar="Gregorian" DateOnly="false" LocaleId="1030" CssClassTextBox="dateBox" OnDateChanged="startDate_DateChanged" AutoPostBack="true" />
                    </div>
                    <div class="buttons">
                        <asp:ImageButton  ID="btnPrevious" runat="server" Text="Go to previous" OnClick="btnPrevious_Click" ImageUrl="/_layouts/images/arrowendleft_light.gif" CssClass="imgButton" />
                        <asp:ImageButton ID="btnNext" runat="server" Text="Go to next" OnClick="btnNext_Click" ImageUrl="/_layouts/images/arrowendright_light.gif" CssClass="imgButton" />
                        <asp:ImageButton ID="btnUpdate" runat="server" Text="Go to next" OnClick="btnUpdate_Click" ImageUrl="/_layouts/images/Kpi_UpdateValues.gif" CssClass="imgButton" OnClientClick="updateGraph(true)" />
                    </div>
                </div>
            </div>

            <div id="timeInfo">
                <asp:Label runat="server" ID="lblTimeInfo" ></asp:Label>
            </div>
            <asp:Repeater ID="rptStatus" runat="server" EnableViewState="true">
                <HeaderTemplate>
                    <table id="statusValues" class="table">
                        <tr class="header">
                            <th class="col1">Platform</th>
                            <th class="col2">Initialized</th>
                            <th class="col3">NETS</th>
                            <th class="col4">NETS Save</th>
                            <th class="col5">NETS Reused</th>
                            <th class="col6">MobilePay</th>
                            <th class="col7">Paid</th>
                            <th class="col8">Credited</th>
                            <th class="col9">MonitorLevel</th>
                            <th class="col9">Total</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <%# Eval("StatusTr") %>
                            <td class="platform"><%# Eval("Platform") %></td>
                            <td><%# Eval("StatInit") %></td>
                            <td><%# Eval("StatNets") %></td>
                            <td><%# Eval("StatNetsSave") %></td>
                            <td><%# Eval("StatNetsReused") %></td>
                            <td><%# Eval("StatMobilePay") %></td>
                            <td><%# Eval("StatPaid") %></td>
                            <td><%# Eval("StatCredited") %></td>
                            <td><%# Eval("MonitorLevel") %></td>
                            <td><%# Eval("StatTotal") %></td>
                        </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                        <%# Eval("StatusTrOdd") %>
                            <td class="platform"><%# Eval("Platform") %></td>
                            <td><%# Eval("StatInit") %></td>
                            <td><%# Eval("StatNets") %></td>
                            <td><%# Eval("StatNetsSave") %></td>
                            <td><%# Eval("StatNetsReused") %></td>
                            <td><%# Eval("StatMobilePay") %></td>
                            <td><%# Eval("StatPaid") %></td>
                            <td><%# Eval("StatCredited") %></td>
                            <td><%# Eval("MonitorLevel") %></td>
                            <td><%# Eval("StatTotal") %></td>
                        </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                        <tr class="header">
                            <td class="platform">
                                <b>Total:</b>
                            </td>
                            <td class="right">
                                <asp:Label ID="lblInitSum" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="right">
                                <asp:Label ID="lblNetsSum" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="right">
                                <asp:Label ID="lblNetsSaveSum" runat="server" Text=""></asp:Label>
                                </td>
                            <td class="right">
                                <asp:Label ID="lblNetsReused" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="right">
                                <asp:Label ID="lblMobilePaySum" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="right">
                                <asp:Label ID="lblPaidSum" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="right">
                                <asp:Label ID="lblCreditedSum" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="right">
                            </td>
                            <td class="right">
                                <asp:Label ID="lblTotalSum" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </FooterTemplate>
            </asp:Repeater>  
          </ContentTemplate>
          <Triggers>
              <asp:AsyncPostBackTrigger ControlID="drpTimeSelector" />
              <asp:AsyncPostBackTrigger ControlID="startDate" />
              <asp:AsyncPostBackTrigger ControlID="btnPrevious" />
              <asp:AsyncPostBackTrigger ControlID="btnNext" />
              <asp:AsyncPostBackTrigger ControlID="btnUpdate" />
          </Triggers>
        </asp:UpdatePanel>

        <div id="chart_div"></div>

    </div>
    <div id="tabs-2">
        <asp:UpdatePanel ID="upErrorLog" runat="server" RenderMode="Block" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="errorMenu">
                    <div id="errorDateSelector">
                        <SharePoint:DateTimeControl ID="dcErrorLog" runat="server" Calendar="Gregorian" DateOnly="false" LocaleId="1030" CssClassTextBox="dateBox" OnDateChanged="dcErrorLog_DateChanged" AutoPostBack="true" />
                    </div>
                    <div id="errorButtons">
                        <asp:ImageButton  ID="btnErrorPrevious" runat="server" Text="Go to previous" OnClick="btnErrorPrevious_Click" ImageUrl="/_layouts/images/arrowendleft_light.gif" CssClass="imgButton" />
                        <asp:ImageButton ID="btnErrorNext" runat="server" Text="Go to next" OnClick="btnErrorNext_Click" ImageUrl="/_layouts/images/arrowendright_light.gif" CssClass="imgButton" />
                        <asp:ImageButton ID="btnErrorUpdate" runat="server" Text="Go to next" OnClick="btnErrorUpdate_Click" ImageUrl="/_layouts/images/Kpi_UpdateValues.gif" CssClass="imgButton" OnClientClick="updateGraph(true)" />
                    </div>
                </div>
                <div style="clear:both"></div>
                <asp:Repeater ID="repErrorLog" runat="server" EnableViewState="true">
                    <HeaderTemplate>
                        <table id="errorLog">
                            <tr class="header">
                                <th class="col1">Order</th>
                                <th class="col2">Timestamp</th>
                                <th class="col3">Severity</th>
                                <th class="col4">Text</th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
                                <td class="platform"><%#  String.Format("<a href='#' onclick=\"showDialog('{0}');return false;\">{0}<a>", DataBinder.Eval(Container.DataItem, "OrderId")) %> </td>
                                <td><%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:dd-MM-yyyy HH:mm:ss}") %></td>
                                <td><%# Eval("Severity") %></td>
                                <td class="left"><%# Eval("Text") %></td>
                            </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                            <tr class="odd">
                                <td class="platform"><%#  String.Format("<a href='#' onclick=\"showDialog('{0}');return false;\">{0}<a>", DataBinder.Eval(Container.DataItem, "OrderId")) %> </td>
                                <td><%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:dd-MM-yyyy HH:mm:ss}") %></td>
                                <td><%# Eval("Severity") %></td>
                                <td class="left"><%# Eval("Text") %></td>
                            </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>  

                <asp:Panel ID="pnlErrorPagingButtons" runat="server" CssClass="pagingPanel">
                    <asp:Button ID="btnErrorPagingPrev" runat="server" Text="<<" OnClick="btnErrorPagingPrev_Click" ToolTip="Show previous page" />
                    <asp:Label ID="lblErrorPagingBottom" runat="server" Text=""></asp:Label>
                    <asp:Button ID="btnErrorPagingNext" runat="server" Text=">>" OnClick="btnErrorPagingNext_Click" ToolTip="Show next page" />
                </asp:Panel>

            </ContentTemplate>
            <Triggers>

            </Triggers>


        </asp:UpdatePanel>


    </div>
    <div id="tabs-3">
        <asp:UpdatePanel ID="upLogSearch" runat="server" RenderMode="Block" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <div style="float:left">
                        <asp:TextBox ID="txtErrorSearch" runat="server"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                    </div>
                    <div class="" style="float:right">
                        <div class="timeSelector">
                            <div class="dateSelector">
                            <SharePoint:DateTimeControl ID="dtcErrorLogSearch" runat="server" Calendar="Gregorian" DateOnly="false" LocaleId="1030" CssClassTextBox="dateBox" OnDateChanged="dtcErrorLogSearch_DateChanged" AutoPostBack="true" />
                            </div>
                            <div class="buttons">
                                <asp:ImageButton ID="lsPrev" runat="server" Text="Go to previous" ImageUrl="/_layouts/images/arrowendleft_light.gif" CssClass="imgButton" OnClick="lsPrev_Click" />
                                <asp:ImageButton ID="lsNext" runat="server" Text="Go to next" ImageUrl="/_layouts/images/arrowendright_light.gif" CssClass="imgButton" OnClick="lsNext_Click" />
                                <asp:ImageButton ID="lsUpdate" runat="server" Text="Go to next" ImageUrl="/_layouts/images/Kpi_UpdateValues.gif" CssClass="imgButton" OnClick="lsUpdate_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <div style="vertical-align:middle; width:150px; padding:15px">
                    <asp:UpdateProgress ID="upLogProgress" AssociatedUpdatePanelID="upLogSearch" runat="server">
                        <ProgressTemplate>            
                            <img src="/_layouts/images/gears_anv4.gif" alt="Progress" style="margin-right:10px;" />
                            Searching...            
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>

                <asp:Label ID="lblErrorLogSearchError" runat="server" ForeColor="Red" />

                <asp:Repeater ID="repErrorSearch" runat="server" EnableViewState="true">
                    <HeaderTemplate>
                        <table id="errorLog">
                            <tr class="header">
                                <th class="col1">Order</th>
                                <th class="col2">Timestamp</th>
                                <th class="col3">Severity</th>
                                <th class="col4">Text</th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
                                <td class="platform"><%#  String.Format("<a href='#' onclick=\"showDialog('{0}');return false;\">{0}<a>", DataBinder.Eval(Container.DataItem, "OrderId")) %> </td>
                                <td><%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:dd-MM-yyyy HH:mm:ss}") %></td>
                                <td><%# Eval("Severity") %></td>
                                <td class="left"><%# Eval("Text") %></td>
                            </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                            <tr class="odd">
                                <td class="platform"><%#  String.Format("<a href='#' onclick=\"showDialog('{0}');return false;\">{0}<a>", DataBinder.Eval(Container.DataItem, "OrderId")) %> </td>
                                <td><%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:dd-MM-yyyy HH:mm:ss}") %></td>
                                <td><%# Eval("Severity") %></td>
                                <td class="left"><%# Eval("Text") %></td>
                            </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>  

                <asp:Panel ID="pnlErrorSearch" runat="server" CssClass="pagingPanel" Visible="false">
                    <asp:Button ID="btnErrorSearchPagingPrev" runat="server" Text="<<" OnClick="btnErrorSearchPagingPrev_Click" ToolTip="Show previous page" />
                    <asp:Label ID="lblErrorSearch" runat="server" Text=""></asp:Label>
                    <asp:Button ID="btnErrorSearchPagingNext" runat="server" Text=">>" OnClick="btnErrorSearchPagingNext_Click" ToolTip="Show next page" />
                </asp:Panel>

            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" />
                <asp:AsyncPostBackTrigger ControlID="lsPrev" />
                <asp:AsyncPostBackTrigger ControlID="lsNext" />
                <asp:AsyncPostBackTrigger ControlID="lsUpdate" />
                <asp:AsyncPostBackTrigger ControlID="dtcErrorLogSearch" />
            </Triggers>

        </asp:UpdatePanel>
      
    </div>




</div>
