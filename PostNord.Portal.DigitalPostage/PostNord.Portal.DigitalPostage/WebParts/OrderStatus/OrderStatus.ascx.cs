﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using PostNord.Portal.DigitalPostage.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace PostNord.Portal.DigitalPostage.WebParts.OrderStatus
{

    /// <summary>
    /// Order Status WebPart
    /// </summary>
    [ToolboxItemAttribute(false)]
    public partial class OrderStatus : WebPart
    {
        #region Private Vars
        private int _pageLength = 3;
        private int _refreshTime = 60;
        string _viewOrderUrl = "/KundeCenter/pages/vieworder.aspx";

        private class StatusData : IEquatable<StatusData>
        {

            public string Platform { get; set; }
            public int StatInit { get; set; }
            public int StatNets { get; set; }
            public int StatNetsSave { get; set; }
            public int StatNetsReused { get; set; }
            public int StatMobilePay { get; set; }
            public int StatPaid { get; set; }
            public int StatCredited { get; set; }
            public int MonitorLevel { get; set; }
            public int ErrorPercent { get; set; }
            public int StatTotal
            {
                get
                {
                    return (this.StatCredited + this.StatInit + this.StatMobilePay + this.StatNets + this.StatNetsReused + this.StatNetsSave + this.StatPaid);
                }
            }

            public string StatusTr
            {
                get
                {
                    return string.Format("<tr class='{0}'>", this.StatusClass);
                }
            }
            public string StatusTrOdd
            {
                get
                {
                    return string.Format("<tr class='{0} odd'>", this.StatusClass);
                }
            }

            public bool Equals(StatusData obj)
            {
                if (obj == null)
                    return false;
                else
                    return (this.Platform == obj.Platform);
            }

            public int CompareTo(StatusData compare)
            {
                if (this.Platform == compare.Platform)
                {
                    return 0;
                }
                return 1;
            }

            /// <summary>
            /// Gets the status class calculated by comparing MonitorLevel to StatTotal
            /// </summary>
            /// <value>
            /// The status class.
            /// Values: [empty] / unset / ok / notok
            /// </value>
            public string StatusClass
            {
                get
                {
                    string status = "ok";

                    // If not hourly return nothing
                    if (this.MonitorLevel == -1)
                    {
                    }

                    // If not hourly return nothing
                    if (this.MonitorLevel == 0)
                    {
                    }

                    // If nothing sold and total is not 0 we have an issue
                    if (this.StatPaid == 0 && this.StatTotal > 0)
                    {
                        status = "warning";
                    }

                    // If ErrorPercent is exceeded show warning
                    if ((this.StatPaid + ((this.StatPaid * ErrorPercent) / 100)) > this.StatTotal)
                    {
                        status = "warning";
                    }

                    // If monitorlevel is exceeded show error
                    if (this.MonitorLevel > this.StatPaid)
                    {
                        status = "notok";
                    }


                    return status;
                }
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="StatusData"/> class.
            /// </summary>
            /// <param name="platform">The platform.</param>
            public StatusData(string platform)
            {
                this.Platform = platform;
                this.StatInit = 0;
                this.StatNets = 0;
                this.StatNetsSave = 0;
                this.StatNetsReused = 0;
                this.StatMobilePay = 0;
                this.StatPaid = 0;
                this.StatCredited = 0;
                this.MonitorLevel = 0;
                this.ErrorPercent = 10;
            }
        }

        SPList _monitorLevelList = null;

        #endregion

        #region Methods
        /// <summary>
        /// Gets or sets the view order URL.
        /// </summary>
        /// <value>
        /// The view order URL.
        /// </value>
        /// <remarks>Not auto refresh in edit mode</remarks>
        [WebBrowsable(true),
        WebDisplayName("Refresh time in seconds (0 to disable)"),
        Personalizable(PersonalizationScope.Shared),
        Category("Settings")]
        public int RefreshTimeEdit
        {
            get { return _refreshTime;}
            set { _refreshTime = value; }
        }

        /// <summary>
        /// Gets or sets the view order URL.
        /// </summary>
        /// <value>
        /// The view order URL.
        /// </value>
        /// <remarks>Not auto refresh in edit mode</remarks>
        [WebBrowsable(true),
        WebDisplayName("Error log Page Length"),
        Personalizable(PersonalizationScope.Shared),
        Category("Settings")]
        public int PageLength
        {
            get { return _pageLength; }
            set { _pageLength = value; }
        }

        /// <summary>
        /// Gets or sets the view order URL.
        /// </summary>
        /// <value>
        /// The view order URL.
        /// </value>
        [WebBrowsable(true),
        WebDisplayName("View order URL"),
        Personalizable(PersonalizationScope.Shared),
        Category("Settings")]
        public string ViewOrderUrl
        {
            get { return _viewOrderUrl; }
            set { _viewOrderUrl = value; }
        }

        #endregion

        #region Public Properties


        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        /// <value>
        /// The page number.
        /// </value>
        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                {
                    return Convert.ToInt16(ViewState["PageNumber"]);
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                if (value >= 0)
                {
                    ViewState["PageNumber"] = value;
                }
            }
        }


        /// <summary>
        /// Gets or sets the error page number.
        /// </summary>
        /// <value>
        /// The page number.
        /// </value>
        public int ErrorPageNumber
        {
            get
            {
                if (ViewState["ErrorPageNumber"] != null)
                {
                    return Convert.ToInt16(ViewState["ErrorPageNumber"]);
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                if (value >= 0)
                {
                    ViewState["ErrorPageNumber"] = value;
                }
            }
        }


        /// <summary>
        /// Gets or sets the view order URL.
        /// </summary>
        /// <value>
        /// The view order URL.
        /// </value>
        /// <remarks>Not auto refresh in edit mode</remarks>
        public int RefreshTime
        {
            get
            {
                if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
                {
                    return _refreshTime;
                }
                return 0;
            }
        }

        /// <summary>
        /// Encapsulates getting the selected value for the timeselector
        /// </summary>
        /// <value>
        /// Number of hours to add when pressing forward / backward button
        /// </value>
        protected int HourCount
        {
            get
            {
                return int.Parse(drpTimeSelector.SelectedValue);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dtcErrorLogSearch.SelectedDate = DateTime.Now.Date;
                startDate.SelectedDate = DateTime.Now.AddHours(-1 * HourCount);
                startDate.SelectedDate.AddMinutes(-1 * startDate.SelectedDate.Minute);
                startDate.MaxDate = DateTime.Now;
                UpdateData();

                dcErrorLog.SelectedDate = DateTime.Now;
                UpdateErrorLog();

            }
            lblError.Text = string.Empty;
            lblInfo.Text = string.Empty;
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the drpTimeSelector control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void drpTimeSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.HourCount > 24 && startDate.SelectedDate.AddHours(this.HourCount) > DateTime.Now)
            {
                // Adjust date to scale
                startDate.SelectedDate = startDate.SelectedDate.AddHours(-1 * this.HourCount);
            }

            UpdateData();
        }

        /// <summary>
        /// Handles the Click event of the btnNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (startDate.SelectedDate.AddHours(this.HourCount).AddMinutes(-10) < DateTime.Now)
            {
                startDate.SelectedDate = startDate.SelectedDate.AddHours(this.HourCount);
            }
            UpdateData();
        }

        /// <summary>
        /// Handles the Click event of the btnPrevious control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            startDate.SelectedDate = startDate.SelectedDate.AddHours(-1 * this.HourCount);
            UpdateData();
        }

        /// <summary>
        /// Handles the DateChanged event of the startDate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void startDate_DateChanged(object sender, EventArgs e)
        {
            UpdateData();
        }

        /// <summary>
        /// Renders the control to the specified HTML writer.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            Page.ClientScript.RegisterForEventValidation(btnNext.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(btnPrevious.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(btnUpdate.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(drpTimeSelector.UniqueID, "4");
            Page.ClientScript.RegisterForEventValidation(startDate.UniqueID, "4");
            base.Render(writer);
        }

        /// <summary>
        /// Handles the Click event of the btnUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void btnUpdate_Click(object sender, ImageClickEventArgs e)
        {
            // Keep showing the last hour
            if (this.HourCount == 1 && (int)(DateTime.Now.AddHours(-1) - startDate.SelectedDate).TotalMinutes < 10)
            {
                startDate.SelectedDate = DateTime.Now.AddHours(-1);
            }

            UpdateData();
        }


        /// <summary>
        /// Updates the error log.
        /// </summary>
        protected void UpdateErrorLog()
        {
            try
            {
                DateTime endDate = dcErrorLog.SelectedDate;
                DateTime startDate = endDate.AddDays(-1);

                DataSet ds = Dal.OrderLogs.GetOrderLogStatus(startDate, endDate, 4);

                if (ds.Tables[0].DefaultView.Count == 0)
                {
                    lblErrorPagingBottom.Text = "Nothing found.";
                    repErrorLog.Visible = false;
                    btnErrorPagingNext.Visible = false;
                    btnErrorPagingPrev.Visible = false;
                    pnlErrorPagingButtons.Visible = true;
                    return;
                }
                else
                {
                    repErrorLog.Visible = true;
                    btnErrorNext.Visible = true;
                    btnErrorPrevious.Visible = true;
                }

                PagedDataSource pds = new PagedDataSource();
                pds.DataSource = ds.Tables[0].DefaultView;
                pds.AllowPaging = true;
                pds.PageSize = _pageLength;
                pds.CurrentPageIndex = PageNumber;

                int pageCount = Convert.ToInt32(Math.Ceiling((decimal)ds.Tables[0].DefaultView.Count / (decimal)pds.PageSize));

                pnlErrorPagingButtons.Visible = (pageCount > 1);
                lblErrorPagingBottom.Text = string.Format("Showing Page: {0} of {1} pages.", (PageNumber + 1), pageCount);

                btnErrorPagingPrev.Enabled = PageNumber > 0;
                btnErrorPagingNext.Enabled = (PageNumber + 1 < pageCount);

                repErrorLog.DataSource = pds;
                repErrorLog.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error loading Log: {0}", ex.Message);
            }

        }

        /// <summary>
        /// Handles the Click event of the btnErrorPrevious control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void btnErrorPrevious_Click(object sender, ImageClickEventArgs e)
        {
            dcErrorLog.SelectedDate = dcErrorLog.SelectedDate.AddDays(-1);
            PageNumber = 0;
            UpdateErrorLog();
        }

        /// <summary>
        /// Handles the Click event of the btnErrorNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void btnErrorNext_Click(object sender, ImageClickEventArgs e)
        {
            if (dcErrorLog.SelectedDate.Date < DateTime.Today)
            {
                dcErrorLog.SelectedDate = dcErrorLog.SelectedDate.AddDays(1);
            }
            PageNumber = 0;
            UpdateErrorLog();
        }

        /// <summary>
        /// Handles the Click event of the btnErrorUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void btnErrorUpdate_Click(object sender, ImageClickEventArgs e)
        {
            UpdateErrorLog();
        }

        /// <summary>
        /// Handles the DateChanged event of the dcErrorLog control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void dcErrorLog_DateChanged(object sender, EventArgs e)
        {
            if (dcErrorLog.SelectedDate > DateTime.Now)
            {
                dcErrorLog.SelectedDate = DateTime.Today;
            }
            UpdateErrorLog();
        }

        /// <summary>
        /// Handles the Click event of the btnErrorPagingPrev control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnErrorPagingPrev_Click(object sender, EventArgs e)
        {
            PageNumber--;
            UpdateErrorLog();
        }

        /// <summary>
        /// Handles the Click event of the btnErrorPagingNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnErrorPagingNext_Click(object sender, EventArgs e)
        {
            PageNumber++;
            UpdateErrorLog();
        }


        /// <summary>
        /// Handles the Click event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ErrorPageNumber = 0;
            UpdateErrorSearchResult();
        }

        /// <summary>
        /// Handles the Click event of the btnErrorSearchPagingPrev control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnErrorSearchPagingPrev_Click(object sender, EventArgs e)
        {
            ErrorPageNumber--;
            UpdateErrorSearchResult();
        }

        /// <summary>
        /// Handles the Click event of the btnErrorSearchPagingNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnErrorSearchPagingNext_Click(object sender, EventArgs e)
        {
            ErrorPageNumber++;
            UpdateErrorSearchResult();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Updates the error log.
        /// </summary>
        protected void UpdateErrorSearchResult()
        {
            if (txtErrorSearch.Text.Trim() != string.Empty)
            {
                try
                {
                    DateTime searchDate = DateTime.MinValue;

                    if (dtcErrorLogSearch.IsDateEmpty)
                    {
                        searchDate = DateTime.Now.AddDays(-1);
                    }
                    else
                    {
                        searchDate = dtcErrorLogSearch.SelectedDate;
                    }

                    DataSet ds = Dal.OrderLogs.GetOrderLogSearch(searchDate, searchDate.AddDays(1), "%" + txtErrorSearch.Text.Trim().Replace("*", "").Replace("%", "") + "%");

                    if (ds.Tables[0].DefaultView.Count == 0)
                    {
                        lblErrorSearch.Text = "Nothing found.";
                        repErrorSearch.Visible = false;
                        btnErrorSearchPagingNext.Visible = false;
                        btnErrorSearchPagingPrev.Visible = false;
                        pnlErrorSearch.Visible = true;
                        return;
                    }
                    else
                    {
                        repErrorSearch.Visible = true;
                        btnErrorSearchPagingNext.Visible = true;
                        btnErrorSearchPagingPrev.Visible = true;
                    }

                    PagedDataSource pds = new PagedDataSource();
                    pds.DataSource = ds.Tables[0].DefaultView;
                    pds.AllowPaging = true;
                    pds.PageSize = _pageLength;
                    pds.CurrentPageIndex = ErrorPageNumber;

                    int pageCount = Convert.ToInt32(Math.Ceiling((decimal)ds.Tables[0].DefaultView.Count / (decimal)pds.PageSize));

                    pnlErrorSearch.Visible = (pageCount > 1);
                    lblErrorSearch.Text = string.Format("Showing Page: {0} of {1} pages.", (ErrorPageNumber + 1), pageCount);

                    btnErrorSearchPagingPrev.Enabled = ErrorPageNumber > 0;
                    btnErrorSearchPagingNext.Enabled = (ErrorPageNumber + 1 < pageCount);

                    repErrorSearch.DataSource = pds;
                    repErrorSearch.DataBind();
                }
                catch (Exception ex)
                {
                    lblErrorLogSearchError.Text = string.Format("Error loading Log: {0}", ex.Message);
                }
            }
        }

        /// <summary>
        /// Gets the monitor level.
        /// </summary>
        /// <param name="day">The day.</param>
        /// <param name="hour">The hour.</param>
        /// <param name="platform">The platform.</param>
        /// <returns></returns>
        protected int GetMonitorLevel(int day, int hour, string platform)
        {
            if (_monitorLevelList == null)
            {
                try
                {
                    string adminUrl = ConfigSettings.GetAdminWebUrl(SPContext.Current.Web);
                    using (SPWeb web = SPContext.Current.Site.OpenWeb(adminUrl))
                    {
                        _monitorLevelList = web.Lists["MonitorLevels"];
                    }

                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Error loading Monitor levels: {0}", ex.Message));
                }
            }

            SPQuery query = new SPQuery();
            query.Query = string.Format(@"<Where>
                                            <And>
                                              <And>
                                                <Eq>
                                                  <FieldRef Name='Hour' />
                                                  <Value Type='Number'>{0}</Value>
                                                </Eq>
                                                <Eq>
                                                  <FieldRef Name='Platform' />
                                                  <Value Type='Text'>{1}</Value>
                                                </Eq>
                                              </And>
                                              <Or>
                                                <Eq>
                                                  <FieldRef Name='Day' />
                                                  <Value Type='Number'>{2}</Value>
                                                </Eq>
                                                <Eq>
                                                  <FieldRef Name='Day' />
                                                  <Value Type='Number'>0</Value>
                                                </Eq>
                                              </Or>
                                            </And>
                                          </Where>", hour, platform, day);
            query.ViewFields = "<FieldRef Name='Total' />";
            SPListItemCollection items = _monitorLevelList.GetItems(query);

            switch (items.Count)
            {
                case 0:
                    return 0;
                case 1:
                    return int.Parse(items[0]["Total"].ToString());
                default:
                    throw new ApplicationException(string.Format("Multiple monitorlevels found for: Day:{0} Hour: {1} Platform:{2}", day, hour, platform));
            }
        }


        /// <summary>
        /// Gets the monitor data.
        /// </summary>
        /// <param name="day">The day.</param>
        /// <param name="hour">The hour.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException"></exception>
        private List<StatusData> GetMonitorData(int day, int hour)
        {

            if (_monitorLevelList == null)
            {
                try
                {
                    string adminUrl = ConfigSettings.GetAdminWebUrl(SPContext.Current.Web);
                    using (SPWeb web = SPContext.Current.Site.OpenWeb(adminUrl))
                    {
                        _monitorLevelList = web.Lists["MonitorLevels"];
                    }

                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Error loading Monitor levels: {0}", ex.Message));
                }
            }

            SPQuery query = new SPQuery();
            query.Query = string.Format(@"<Where>
                                            <And>
                                              <Eq>
                                                <FieldRef Name='Hour' />
                                                <Value Type='Number'>{0}</Value>
                                              </Eq>
                                              <Or>
                                                <Eq>
                                                  <FieldRef Name='Day' />
                                                  <Value Type='Number'>{1}</Value>
                                                </Eq>
                                                <Eq>
                                                  <FieldRef Name='Day' />
                                                  <Value Type='Number'>0</Value>
                                                </Eq>
                                              </Or> 
                                            </And>
                                        </Where>", hour, day);
            query.ViewFields = "<FieldRef Name='Platform' /><FieldRef Name='Total' />";

            SPListItemCollection items = _monitorLevelList.GetItems(query);

            List<StatusData> statDataList = new List<StatusData>();

            foreach (SPListItem item in items)
            {
                StatusData statData = new StatusData((string)item["Platform"]);
                statData.MonitorLevel = int.Parse(item["Total"].ToString());
                statDataList.Add(statData);
            }

            return statDataList;
        }


        protected int GetMonitorLevel(int day, string platform)
        {

            if (_monitorLevelList == null)
            {
                try
                {
                    string adminUrl = ConfigSettings.GetAdminWebUrl(SPContext.Current.Web);
                    using (SPWeb web = SPContext.Current.Site.OpenWeb(adminUrl))
                    {
                        _monitorLevelList = web.Lists["MonitorLevels"];
                    }

                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Error loading Monitor levels: {0}", ex.Message));
                }
            }

            SPQuery query = new SPQuery();
            query.Query = string.Format(@"<Where>
                                            <And>
                                                <Eq>
                                                    <FieldRef Name='Platform' />
                                                    <Value Type='Text'>{0}</Value>
                                                </Eq>
                                                <Or>
                                                    <Eq>
                                                        <FieldRef Name='Day' />
                                                        <Value Type='Number'>{1}</Value>
                                                    </Eq>
                                                    <Eq>
                                                        <FieldRef Name='Day' />
                                                        <Value Type='Number'>0</Value>
                                                    </Eq>
                                                </Or>
                                            </And>
                                          </Where>", platform, day);
            query.ViewFields = "<FieldRef Name='Total' />";

            SPListItemCollection items = _monitorLevelList.GetItems(query);

            int sum = 0;
            foreach (SPListItem item in items)
            {
                sum += int.Parse(item["Total"].ToString());
            }

            return sum;
        }

        /// <summary>
        /// Updates the presented data 
        /// </summary>
        protected void UpdateData()
        {
            try
            {
                DateTime startTime = startDate.SelectedDate;
                DateTime endTime = startTime.AddHours(this.HourCount);

                if (this.HourCount == 1)
                {
                    startDate.DateOnly = false;
                    lblTimeInfo.Text = string.Format("<b>TimeSpan: </b> {0} => {1}", startTime.ToString("dd-MM-yyyy HH:mm"), endTime.ToString("dd-MM-yyyy HH:mm"));
                }
                else
                {
                    startDate.DateOnly = true;
                    startDate.SelectedDate = startDate.SelectedDate.AddHours(startDate.SelectedDate.Hour * -1);
                    lblTimeInfo.Text = string.Format("<b>TimeSpan: </b> {0} => {1}", startTime.ToString("dd-MM-yyyy"), endTime.ToString("dd-MM-yyyy"));
                }
                DataSet ds = Dal.Orders.GetOrderStatus(startTime, endTime);
                DataTable data = ds.Tables[0];

                if (Page.Request["debug"] != null)
                {
                    dgData.DataSource = data;
                    dgData.DataBind();
                }
                else
                {
                    dgData.Enabled = false;
                }


                StatusData currentStat = null;
                List<StatusData> statusList = new List<StatusData>();
                int day = GetDayNum(startDate.SelectedDate);
                int hour = startDate.SelectedDate.Hour;

                List<StatusData> monitorList = GetMonitorData(day, hour);

                // Loop rows from Table. Reformat info to presentation
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string platform = (string)row["Platform"];
                    int count = (int)row["OrderCount"];
                    short status = (short)row["OrderStatus"];
                    if (currentStat == null || platform != currentStat.Platform)
                    {
                        if (currentStat != null)
                        {
                            statusList.Add(currentStat);
                        }
                        currentStat = new StatusData(platform);
                        if (this.HourCount == 1)
                        {
                            currentStat.MonitorLevel = GetMonitorLevel(day, hour, platform);
                        }
                        else
                        {
                            currentStat.MonitorLevel = GetMonitorLevel(day, platform) * this.HourCount / 24;
                        }
                    }

                    switch (status)
                    {
                        case 0:
                            currentStat.StatInit = count;
                            break;
                        case 1:
                            currentStat.StatNets = count;
                            break;
                        case 2:
                            currentStat.StatPaid = count;
                            break;
                        case 3:
                            currentStat.StatCredited = count;
                            break;
                        case 4:
                            currentStat.StatNetsSave = count;
                            break;
                        case 5:
                            currentStat.StatNetsReused = count;
                            break;
                        case 6:
                            currentStat.StatMobilePay = count;
                            break;
                    }
                }
                if (currentStat != null)
                {
                    statusList.Add(currentStat);
                }

                int initSum = 0;
                int netsSum = 0;
                int netsSaveSum = 0;
                int netsReuseSum = 0;
                int mobilePaySum = 0;
                int creditedSum = 0;
                int paidSum = 0;
                int totalSum = 0;

                // Calc sums
                foreach (StatusData d in statusList)
                {
                    initSum += d.StatInit;
                    netsSum += d.StatNets;
                    netsSaveSum += d.StatNetsSave;
                    netsReuseSum += d.StatNetsReused;
                    mobilePaySum += d.StatMobilePay;
                    creditedSum += d.StatCredited;
                    paidSum += d.StatPaid;
                    totalSum += d.StatTotal;
                }


                // Add Monitor Items not found in monitorList
                foreach (StatusData monitorItem in monitorList)
                {
                    if (!statusList.Contains(monitorItem))
                    {
                        if (this.HourCount > 1)
                        {
                            monitorItem.MonitorLevel = GetMonitorLevel(day, monitorItem.Platform) * this.HourCount / 24;
                        }

                        statusList.Add(monitorItem);
                    }
                }

                // If we have no data and no monitor items we give up.
                if (statusList.Count == 0)
                {
                    lblError.Text += string.Format("<br/>No orders or monitor info found for the given period");
                    rptStatus.Visible = false;
                    return;
                }
                else
                {
                    rptStatus.Visible = true;
                }

                rptStatus.DataSource = statusList;
                rptStatus.DataBind();

                SetLabel("lblInitSum", initSum);
                SetLabel("lblNetsSum", netsSum);
                SetLabel("lblNetsSaveSum", netsSaveSum);
                SetLabel("lblNetsReused", netsReuseSum);
                SetLabel("lblMobilePaySum", mobilePaySum);
                SetLabel("lblCreditedSum", creditedSum);
                SetLabel("lblPaidSum", paidSum);
                SetLabel("lblTotalSum", totalSum);
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Update Data Exception: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Sets text on label in Repeater Control
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="val">The value.</param>
        protected void SetLabel(string id, int val)
        {
            Label tmpLabel = FindControlInRepeater.FindControl(rptStatus, id) as Label;
            tmpLabel.Text = val.ToString();

        }

        /// <summary>
        /// Gets the day number.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>
        /// Monday: 1
        /// ...
        /// Sunday:7
        /// </returns>
        protected int GetDayNum(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return 1;
                case DayOfWeek.Tuesday:
                    return 2;
                case DayOfWeek.Wednesday:
                    return 3;
                case DayOfWeek.Thursday:
                    return 4;
                case DayOfWeek.Friday:
                    return 5;
                case DayOfWeek.Saturday:
                    return 6;
                default:
                    return 7;
            }
        }

        #endregion

        /// <summary>
        /// Handles the Click event of the lsPrev control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void lsPrev_Click(object sender, ImageClickEventArgs e)
        {
            if (dtcErrorLogSearch.IsDateEmpty)
            {
                dtcErrorLogSearch.SelectedDate = DateTime.Now.Date;
            }
            dtcErrorLogSearch.SelectedDate = dtcErrorLogSearch.SelectedDate.AddDays(-1);
            UpdateErrorSearchResult();
        }

        /// <summary>
        /// Handles the Click event of the lsNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void lsNext_Click(object sender, ImageClickEventArgs e)
        {
            if (dtcErrorLogSearch.IsDateEmpty)
            {
                dtcErrorLogSearch.SelectedDate = DateTime.Now.Date;
            }
            if (dtcErrorLogSearch.SelectedDate < DateTime.Now.Date)
            {
                dtcErrorLogSearch.SelectedDate = dtcErrorLogSearch.SelectedDate.AddDays(1);
            }
            UpdateErrorSearchResult();
        }

        /// <summary>
        /// Handles the Click event of the lsUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
        protected void lsUpdate_Click(object sender, ImageClickEventArgs e)
        {
            UpdateErrorSearchResult();
        }

        /// <summary>
        /// Handles the DateChanged event of the dtcErrorLogSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void dtcErrorLogSearch_DateChanged(object sender, EventArgs e)
        {
            UpdateErrorSearchResult();
        }

    }
}
