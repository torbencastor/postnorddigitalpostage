﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="pnc" Namespace="PostNord.Portal.DigitalPostage.WebControls" Assembly="$SharePoint.Project.AssemblyFullName$" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuyPostageWP.ascx.cs" Inherits="PostNord.Portal.DigitalPostage.WebParts.BuyPostage.BuyPostageWP" %>


<script type="text/javascript">
    var deleteConfirmMessage = '<pnc:GetText runat="server" Key="BUYPOSTAGE_DELETE_CONFIRM" Default="Er du sikker på at du vil slette?" />';
    var maxNumberOfItems = Number('<pnc:GetText runat="server" Key="BUYPOSTAGE_MAX_ITEMS" Default="1000" />');
    var errorTooManyItems = '<pnc:GetText runat="server" Key="BUYPOSTAGE_ERROR_TOO_MANY_ITEMS" Default="Max antal er {0}." />';
    var errorNotANumber = '<pnc:GetText runat="server" Key="BUYPOSTAGE_ERROR_NOT_A_NUMBER" Default="Du skal angive et antal" />';

    var errorSelectImageFile = '<pnc:GetText runat="server" Key="BUYPOSTAGE_SELECT_IMAGE" Default="Vælg venligst billedfil!" />';
    var errorSelectNumber = '<pnc:GetText runat="server" Key="BUYPOSTAGE_ERROR_SELECT_NUMBER" Default="Vælg venligst antal." />';
    var errorSelectEmail = '<pnc:GetText runat="server" Key="BUYPOSTAGE_ERROR_SELECT_EMAIL" Default="E-mail skal udfyldes." />';
    var errorEmailNotValid = '<pnc:GetText runat="server" Key="BUYPOSTAGE_ERROR_EMAIL_NOT_VALID" Default="E-mail er ikke gyldig." />';
    var errorEmailsDontCompare = '<pnc:GetText runat="server" Key="BUYPOSTAGE_ERROR_EMAILS_DONT_COMPARE" Default="De indtastede emails er ikke ens. Kontrollér venligst." />';
    var errorAcceptTerms = '<pnc:GetText runat="server" Key="BUYPOSTAGE_ERROR_ACCEPT_TERMS" Default="Accepter venligst handelsbetingelserne" />';
    var valutaCode = '<pnc:GetText runat="server" Key="BUYPOSTAGE_VALUTA_CODE" Default="DKK" />';
</script>

<div class="body">
    <input type="hidden" id="wizMaxStep" />
    <div class="nav">
        <ol>
            <li class="active"><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_1" Default="" /></span></li>
            <li><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_2" Default="" /></span></li>
            <li><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_3" Default="" /></span></li>
            <li><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_4" Default="" /></span></li>
            <li class="last"><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_5" Default="" /></span></li>
        </ol>
    </div>
    <%--<div id="log" style="margin:20px; border:1px solid #eee;background:#fefefe"></div>--%>
    <div id="steps">
        <div id="form1" class="stepsForm">
            <h1><pnc:GetText runat="server" Key="BUYPOSTAGE_STEP_1_HEADLINE" Default="" /></h1>
            <div class="error" id="form1error" style="display:none"></div>

            <%--Destination--%>
            <div class="infoBlock radios">
                <span class="headerText">Vælg destination</span><br />
                <div id="divRadioDestinationDenmark">
                    <input type="radio" name="destination" title="Danmark" id="destinationdenmark" checked="checked" />
                    <label class="radioBtnLabel" id="radioBtnTextDestinationDenmark" for="destinationdenmark">Danmark</label><br />
                </div>
                <div id="divRadioDestinationUdland">
                    <input type="radio" name="destination" title="Udland" id="destinationudland" />
                    <label class="radioBtnLabel" id="radioBtnTextDestinationUdland" for="destinationudland">Udland</label><br />
                </div>
            </div>

            <%--Leveringstype--%>
            <div class="infoBlock radios">
                <span class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_CHOOSE_PRIORITY" Default="" /></span>&nbsp;<a class="info" href="#" tooltip="priorityHelp"><img src="/_layouts/DigitalPostage/imgs/question.png" border="0" class="questionimg" /></a><br />
                <div id="divRadioB">
                    <input type="radio" name="post" title="B Post" id="bpost" checked="checked" />
                    <label class="radioBtnLabel" id="radioBtnTextB" for="bpost"><pnc:GetText runat="server" Key="BUYPOSTAGE_RADIO_B_DENMARK" Default="BUYPOSTAGE_RADIO_B not defined" /></label><br />
                </div>
                <div id="divRadioA">
                    <input type="radio" name="post" title="A Post" id="apost" />
                    <label class="radioBtnLabel" id="radioBtnTextA" for="apost"><pnc:GetText runat="server" Key="BUYPOSTAGE_RADIO_A_DENMARK" Default="BUYPOSTAGE_RADIO_A not defined" /></label><br />
                </div>

                <div class="ui-tooltip ui-widget ui-corner-all ui-widget-content ui-helper-hidden" id="priorityHelp" style='<pnc:GetText runat="server" Key="BUYPOSTAGE_HELP_PRIORITY_POS" Default="left: 270px; top: 190px; width:400px" />' tooltip-box="">
                    <pnc:GetText runat="server" Key="BUYPOSTAGE_HELP_PRIORITY_TEXT" Default="Pris og leveringshastighed afhænger af, om du sender dit brev som A- eller B-post. Leveringstiden afhænger af destinationen. Du kan altid finde den præcise leveringstid på Post Danmarks hjemmeside." />
                </div>
            </div>

            <div class="infoBlock">
                <span class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_CHOOSE_WEIGHT" Default="" /></span><br />
                <asp:DropDownList runat="server" ID="weightSelector"></asp:DropDownList>
            </div>


            <%--<div class="infoBlock">
                <span class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_CHOOSE_COUNTRY" Default="" /></span><br />
                <asp:DropDownList runat="server" ID="countrySelector"></asp:DropDownList>
            </div>--%>

            <h2><pnc:GetText runat="server" Key="BUYPOSTAGE_CHOOSE_AMOUNT" Default="" /></h2>
            <table id="shoppingBasket">
                <tr class="productTitleBar">
                    <th id="productTitle" class="productListingCol1 productTitle"><pnc:GetText runat="server" Key="BUYPOSTAGE_AMOUNT" Default="" /></th>
                    <th id="productAmount" class="productListingCol2 productTitle productAmount"><pnc:GetText runat="server" Key="BUYPOSTAGE_UNIT_PRICE" Default="" /></th>
                    <th id="productPrice" class="productListingCol3 productTitle productAmount"><pnc:GetText runat="server" Key="BUYPOSTAGE_PRICE" Default="" /></th>
                </tr>
                <tr id="productListingRow">
                    <td colspan="3" id="productListingBlock">
                        <table id="productListing">
                            <tr class="productTitleBar">
                                <td class="productListingCol1">
                                    <input type="text" value="1" id="newPostage" />
                                </td>
                                <td class="productListingCol2 productAmount">
                                    <input type="text" value="0" id="unitPrize" readonly="readonly" />
                                    <input type="hidden" value="" id="productId" />
                                </td>
                                <td class="productListingCol3 productAmount">
                                    <input type="text" value="0" id="unitTotal" readonly="readonly" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2" id="productsButtons">
                        <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_1_BTN" Default="" />' id="form1Proceed" class="buttonClass primaryBtn" title='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_1_BTN" Default="" />' />
                    </td>
                </tr>
            </table>
        </div>

        <div id="form2" class="stepsForm" style="display:none">
            <h1><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_2_HEADLINE" Default="" /></h1>
            <div class="error" id="form2error">
            </div>
            <table id="orderView">
                <tr id="orderHeaderRow">
                    <th class="item dest"><pnc:GetText runat="server" Key="BUYPOSTAGE_DESTINATION" Default="" /></th>
                    <th class="item weight"><pnc:GetText runat="server" Key="BUYPOSTAGE_WEIGHT" Default="" /></th>
                    <th class="item speed"><pnc:GetText runat="server" Key="BUYPOSTAGE_SPEED" Default="" /></th>
                    <th class="item price"><pnc:GetText runat="server" Key="BUYPOSTAGE_UNIT_PRICE" Default="" /></th>
                    <th class="item amount"><pnc:GetText runat="server" Key="BUYPOSTAGE_AMOUNT" Default="" /></th>
                    <th class="item totalprice"><pnc:GetText runat="server" Key="BUYPOSTAGE_PRICE" Default="" /></th>
                    <th></th>
                </tr>
                <tr id="orderTotalRow">
                    <td class="total"><pnc:GetText runat="server" Key="BUYPOSTAGE_TOTAL" Default="" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="item total" id="totalOrderAmount"></td>
                    <td></td>
                </tr>
            </table>
            <table id="orderViewButtons">
                <tr id="orderButtonsRow">
                    <td colspan="7" id="orderButtons" style="text-align: right;">
                        <input type="button" class="buttonClass secondaryBtn" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_2_BTN_MORE" Default="" />' id="BuyMore" />
                        <input type="button" class="buttonClass secondaryBtn" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_2_BTN_LABEL" Default="" />' id="MakeLabel" />
                        <input type="button" class="buttonClass primaryBtn" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_2_BTN_PAY" Default="" />' id="Go2Payment" />
                    </td>
                </tr>
            </table>

        </div>

        <div id="form3" class="stepsForm" style="display:none">
            <h1><pnc:GetText runat="server" Key="BUYPOSTAGE_HEADLINE_LABEL" Default="" />&nbsp;<a class="info" href="#" tooltip="labelHelp"><img src="/_layouts/DigitalPostage/imgs/question.png" border="0" class="questionimg" /></a></h1>
            <div class="error" id="form3error" style="display:none"></div>
            <div class="ui-tooltip ui-widget ui-corner-all ui-widget-content ui-helper-hidden" id="labelHelp" style='<pnc:GetText runat="server" Key="BUYPOSTAGE_HELP_LABEL_POS" Default="left: 190px; top: 65px; width:400px" />' tooltip-box="">
                <pnc:GetText runat="server" Key="BUYPOSTAGE_HELP_LABEL_TEXT" Default="Hvis du printer dine portokoder på labels har du mulighed for at upload dit eget billede.Du kan også vælge at skrive koderne med en kuglepen direkte på kuverten, så kan du i stedet gå direkte til betaling." />   
            </div>
            <table>
                <tr>
                    <td id="labelCell">
                        <table id="labelDesign" style="border: solid 1px black;">
                            <tr>
                                <td>
                                    <img src='<pnc:GetText runat="server" Key="DefaultLabelImage" UseConfigSetting="true" />' id="img" />
                                </td>
                                <td id="dummyImg">
                                    <img src='<pnc:GetText runat="server" Key="BUYPOSTAGE_HEADLINE_LABEL_IMG" Default="/Style%20Library/DigitalPostage/imgs/LabelText.png" />' alt="" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td id="textCell">
                        <div class="headline"><pnc:GetText runat="server" Key="BUYPOSTAGE_LAYOUT" Default="" /></div>
                        <br />
                        <span id="defaultLayoutText"><pnc:GetText runat="server" Key="BUYPOSTAGE_LAYOUT_DESC" Default="" /></span>
                        <span id="selectedLayoutText" style="display:none"><pnc:GetText runat="server" Key="BUYPOSTAGE_LAYOUT_DESC_SELECTED" Default="Sådan kommer dit label til at se ud." /></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="width:361px;" id="fileDiv">
                            <input type="file" id="BrowserHiddenFileField" name="fileProp" class="uploadClass" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_BTN_BROWSE" Default="" />' />
                        </div>
                        <div style="padding-top: 10px;">
                            <input type="submit" name="Upload" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_BTN_UPLOAD" Default="" />' id="uploadBtn" class="buttonClass secondaryBtn" />
                            <input type="button" name="ResetImg" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_BTN_RESET" Default="" />' id="resetImg" class="buttonClass secondaryBtn" />
                            <img id="uploadBtn-loading" src="/_layouts/digitalpostage/imgs/loading.gif" style="vertical-align: middle;"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="form3Buttons">
                        <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_3_BTN_BACK" Default="" />' class="buttonClass secondaryBtn" id="labelPrevious" />
                    </td>
                    <td style="text-align: right;" class="form3Buttons">
                        <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_3_BTN_CHOOSE" Default="" />' class="buttonClass primaryBtn" id="labelSelect" />
                    </td>
                </tr>
            </table>
        </div>

        <div id="form4" class="stepsForm" style="display:none">
            <h1><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_HEADLINE" Default="" /></h1>
            <div class="error" id="form4error">
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" CssClass="errorMessage"></asp:Label>
            </div>
            <table>
                <tr>
                    <td id="emails">
                        <div id="email1Text" class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_EMAIL_TOP" Default="" /></div>
                        <input type="text" class="email" id="email1" value="" />
                        <div id="email2Text" class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_EMAIL_BOTTOM" Default="" /></div>
                        <input type="text" class="email" id="email2" title="E-mail adresse" value="" />
                        <div id="errorBox"></div>
                    </td>
                    <td>
                        <div class="headerText"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_INFO_HEADLINE" Default="" /></div>
                        <pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_INFO" Default="" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" id="terms">
                        <input type="checkbox" id="acceptTerms" /><label id="termsText" for="acceptTerms"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_ACCEPT" Default="" /></label>
                        <a href='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_ACCEPT_LINK_URL" Default="" />' title='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_ACCEPT_LINK_TITLE" Default="" />' target="_blank"><pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_ACCEPT_LINK_TEXT" Default="" /></a>
                        <span id="termsError"> </span>
                    </td>
                </tr>
            </table>
            <table id="buttons">
                <tr>
                    <td>
                        <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_BTN_VIEW_ORDER" Default="" />' class="buttonClass secondaryBtn" id="seeOrderBtn" />
                        <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_BTN_LABEL" Default="" />' class="buttonClass secondaryBtn" id="makeLabelBtn" />
                    </td>
                    <td style="text-align: right;">
                        <img id="go2PaymentBtn-loading" src="/_layouts/digitalpostage/imgs/loading.gif" style="vertical-align: middle;"/>
                        <input type="button" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_FORM_4_BTN_PAY" Default="" />' class="buttonClass primaryBtn" id="go2PaymentBtn" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<pnc:GetMessage runat="server" />   