﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using iTextSharp.text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.WebControls;
using PostNord.Portal.DigitalPostage.Bll;
using PostNord.Portal.DigitalPostage.Helpers;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;

namespace PostNord.Portal.DigitalPostage.WebParts.BuyPostage
{
    [ToolboxItemAttribute(false)]
    public partial class BuyPostageWP : WebPart
    {
        private const string TextListName = "TextSettingsV5";      
        /// <summary>
        /// The platform definition
        /// </summary>
        public static string PLATFORM = "WebShop";

        /// <summary>
        /// Initializes a new instance of the <see cref="BuyPostageWP"/> class.
        /// </summary>
        public BuyPostageWP()
        {
            TextSettings.ListName = TextListName;
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        /// <summary>
        /// Constant script names
        /// </summary>
        private enum ScriptNames
        {
            /// <summary>
            /// Prices script
            /// </summary>
            PostagePrices,
            /// <summary>
            /// Weight script
            /// </summary>
            PostageWeight,
            /// <summary>
            /// Delivery script
            /// </summary>
            PostageDelivery,
            /// <summary>
            /// Country script
            /// </summary>
            PostageCountry
        }

        /// <summary>
        /// Holds the type of the object
        /// </summary>
        private Type csType;


        /// <summary>
        /// Creates the order.
        /// </summary>
        /// <param name="transactionID">The transaction identifier.</param>
        /// <param name="debug">The debug.</param>
        private void CreateOrder(string transactionID, string debug)
        {

            bool forceCaptureError = false;
            bool.TryParse(ConfigSettings.GetText("ForceCaptureError", "false"), out forceCaptureError);

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite siteElevated = new SPSite(SPContext.Current.Site.ID))
                {
                    using (SPWeb web = siteElevated.OpenWeb(siteElevated.RootWeb.ID))
                    {
                        Bll.Orders order = new Orders(transactionID);
                        try
                        {
                            order.AddLogDebug(string.Format("Capture: {0} ({1})", HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.UserAgent));

                            var paymentInfo = order.GetNetsPaymentInfo();
                            float amountCaptured = (int.Parse(paymentInfo.Summary.AmountCaptured) / 100);
                            float amountCredited = (int.Parse(paymentInfo.Summary.AmountCredited) / 100);

                            if (amountCredited == 0)
                            {
                                var info = order.Capture();

                                if (forceCaptureError)
                                {
                                    info.ErrorCode = "FORCED_ERROR";
                                    info.ErrorMessage = "Transaction (Process ID 133) was deadlocked on lock | communication buffer resources with another process and has been chosen as the deadlock victim. Rerun the transaction.";
                                }

                                if (!string.IsNullOrEmpty(info.ErrorCode))
                                {
                                    if (info.ErrorMessage.Contains("was deadlocked"))
                                    {
                                        order.AddLogDebug(string.Format("Deadlock detected. Message: {0}", info.ErrorMessage));
                                        paymentInfo = order.GetNetsPaymentInfo();
                                        amountCaptured = (int.Parse(paymentInfo.Summary.AmountCaptured) / 100);
                                        amountCredited = (int.Parse(paymentInfo.Summary.AmountCredited) / 100);
                                        if (amountCaptured != order.OrderSum || amountCredited > 0)
                                        {
                                            var errMsg = string.Format("Der opstod en fejl hos betalingsleverandøren! Kvitteringsnr: {0}. Prøv igen senere eller kontakt kundeservice.", order.Id);
                                            order.AddLogError(string.Format("Capture deadlock error: {0}. Description:{1} Captured: {2} Credited: {3}", info.ErrorCode, info.ErrorMessage, paymentInfo.Summary.AmountCaptured, paymentInfo.Summary.AmountCredited));
                                            lblError.Text = errMsg;
                                            lblError.Visible = true;
                                            return;
                                        }
                                        else
                                        {
                                            order.AddLogDebug(string.Format("Captured amount: {0} Credited amount: {1} - continue proces ", paymentInfo.Summary.AmountCaptured, paymentInfo.Summary.AmountCredited));
                                        }
                                    }
                                    else
                                    {

                                        var errMsg = string.Format("Der opstod en fejl! Kvitteringsnr: {0}. Prøv igen senere eller kontakt kundeservice.", order.Id);
                                        if (info.ErrorCode == PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.NETS_AUTH_FAILURE.ToString())
                                        {
                                            errMsg = string.Format("Betalingsleverandøren afviste betalingen! Kvitteringsnr: {0}. Prøv igen senere eller kontakt kundeservice.", order.Id);
                                        }
                                        //MMI: Punkt 1 i fejl-dokumentet med 7 punkter: Below is an example of a logges error text. This should be caught in the if statement and not in this else statement?
                                        //Capture error: NETS_ERROR. Description:NETS Capture Error, Unknown Exception: Transaction (Process ID 88) was deadlocked on lock | communication buffer resources with another process and has been chosen as the deadlock victim. Rerun the transaction.
                                        //Capture error: NETS_ERROR. Description:NETS Capture Error, Unknown Exception: Transaction (Process ID 120) was deadlocked on lock | communication buffer resources with another process and has been chosen as the deadlock victim. Rerun the transaction.
                                        //Possible SQL statements failing due to deadlock:
                                        //OrdersInsertRow, OrdersUpdateRow, 
                                        //
                                        order.AddLogError(string.Format("Capture error: {0}. Description:{1}", info.ErrorCode, info.ErrorMessage));
                                        lblError.Text = errMsg;
                                        lblError.Visible = true;
                                        return;
                                    }
                                }
                            }
                            else
                            {
                                order.AddLogDebug(string.Format("Order already Credited: {0}", amountCredited));
                            }

                        }
                        catch (Exception ex)
                        {
                            var errMsg = string.Format("Der opstod en fejl ved betalingen. {0}. Kvitteringsnr.: {1}. Prøv igen senere eller kontakt kundeservice.", ex.Message, order.Id);
                            order.AddLogError(string.Format("Capture Exception: {0}.", ex.Message));
                            Logging.LogEvent(string.Format("Capture Exception: {0} OrderId: {1}", errMsg, order.Id), Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                            lblError.Text = errMsg;
                            lblError.Visible = true;
                            return;
                        }

                        List<Codes> codes = null;

                        try
                        {
                            codes = order.GetCodes();
                        }
                        catch (Exception ex)
                        {
                            var errMsg = string.Format("Postkoder kunne ikke genereres. Kvitteringsnr.: {0} Kontakt venligst kundeservice.", order.Id);
                            order.AddLogError(errMsg);
                            Logging.LogEvent(string.Format("{0}. Error:{1} OrderId: {2}", errMsg, ex.Message, order.Id), Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                            lblError.Text = errMsg;
                            lblError.Visible = true;
                            return;
                        }

                        if (codes.Count == 0)
                        {
                            lblError.Text = string.Format("Der blev ikke fundet nogen koder. Kvitteringsnr.: {0} Kontakt venligst kundeservice.", order.Id);
                            order.AddLogError("No codes were found. Email not sent!");
                            lblError.Visible = true;
                            return;
                        }

                        if (codes.Count > 0)
                        {
                            try
                            {
                                order.SendMail();
                                order.AddLogDebug(string.Format("Mail sent to: {0}", order.Email));
                            }
                            catch (Exception ex)
                            {
                                lblError.Text = string.Format("Fejl ved afsendelse af mail. Kvitteringsnr: {0} Kontakt venligst kundeservice.", order.Id); ;
                                lblError.Visible = true;
                                string errMsg = string.Format("Error sending mail: {0}", ex.Message);
                                order.AddLogError(errMsg);
                                Logging.LogEvent(string.Format("{0} OrderId: {1}", errMsg, order.Id), Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                                return;
                            }

                            string resultUrl = ConfigSettings.GetText("FinalReceiptPage") + transactionID;
                            try
                            {
                                this.Page.Response.Redirect(resultUrl, false);
                            }
                            catch (Exception ex)
                            {
                                lblError.Text = string.Format("Fejl ved visning af kvittering: Kvitteringsnr.: {0}. Kontakt venligst kundeservice.", order.Id);
                                lblError.Visible = true;
                                order.AddLogError(lblError.Text + " Url: " + resultUrl + " Exception: " + ex.Message);
                                Logging.LogEvent(lblError.Text + " Url: " + resultUrl + " Exception: " + ex.Message, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
                            }
                        }
                    }
                }
            });
        }


        /// <summary>
        /// Page being loaded event handler
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event argument</param>
        protected void Page_Load(object sender, EventArgs e)
        {

            string transactionID = HttpContext.Current.Request["transactionid"];
            string responseCode = HttpContext.Current.Request["responsecode"];
            string showImageParam = HttpContext.Current.Request["showimg"];
            string debug = HttpContext.Current.Request["debug"];
            string clearcache = HttpContext.Current.Request["clearcache"];

            //are there any of the parameters in the URL?

            if (string.IsNullOrEmpty(transactionID) & string.IsNullOrEmpty(responseCode) &
                string.IsNullOrEmpty(showImageParam) & string.IsNullOrEmpty(debug))
            {
                //trying to retrieve the values from tha parent URL (in case the code is running in an iFrame)
                var uiParentReference = new UIParentReference(HttpContext.Current.Request);
                uiParentReference.UrlParameters.TryGetValue("transactionid", out transactionID);
                uiParentReference.UrlParameters.TryGetValue("responsecode", out responseCode);
                uiParentReference.UrlParameters.TryGetValue("showimg", out showImageParam);
                uiParentReference.UrlParameters.TryGetValue("debug", out debug);
                uiParentReference.UrlParameters.TryGetValue("clearcache", out clearcache);

            }
            bool showImg = (showImageParam == "true");

            try
            {

                if (!Page.IsPostBack)
                {
                    if (TextSettings.IsCacheDirty() || !string.IsNullOrEmpty(clearcache))
                    {
                        TextSettings.ClearCache(SPContext.Current.Web.Language);
                    }

                    if (ConfigSettings.IsCacheDirty() || !string.IsNullOrEmpty(clearcache))
                    {
                        ConfigSettings.ClearCache(SPContext.Current.Web.Language);
                    }

                    string message = string.Empty;
                    string errorMessage = CheckClient(PLATFORM, 234, out message);
                    if (errorMessage != string.Empty)
                    {
                        string header = " <link rel='stylesheet' href='/_layouts/DigitalPostage/js/wizstepWP.css' />";
                        try
                        {
                            HttpContext.Current.Response.ContentType = "text/html";
                            HttpContext.Current.Response.Clear();
                            HttpContext.Current.Response.Write(string.Format("{0}<div>{1}</div><!-- Platform: {2} - Error message: {3} -->", header, message, PLATFORM, errorMessage));
                            HttpContext.Current.Response.End();
                            HttpContext.Current.Response.Close();
                        }
                        catch { }
                        return;
                    }
                }

                this.csType = this.GetType();
                LoadData();

                string rawUrl = this.Page.Request.RawUrl;

                if (this.Page.Request.UrlReferrer != null)
                {
                    rawUrl = this.Page.Request.UrlReferrer.AbsoluteUri;
                }

                if (!string.IsNullOrEmpty(transactionID) && !string.IsNullOrEmpty(responseCode))
                {
                    if (responseCode.ToLower() == "ok")
                    {
                        CreateOrder(transactionID, debug);
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error: {0}", ex.Message);
                lblError.Visible = true;
                Logging.LogEvent("Error in BuyPostage Page_Load: " + ex.Message, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
            }
        }


        /// <summary>
        /// Gets or sets the type of border that frames a Web Parts control.
        /// </summary>
        /// <returns>One of the <see cref="T:System.Web.UI.WebControls.WebParts.PartChromeType" /> values. The default is <see cref="F:System.Web.UI.WebControls.WebParts.PartChromeType.Default" />.</returns>
        public override PartChromeType ChromeType
        {
            get
            {
                return PartChromeType.None;
            }
            set
            {
                base.ChromeType = value;
            }
        }

        /// <summary>
        /// Loads data product lists 
        /// </summary>
        private void LoadData()
        {
            List<ProductInfo> products = null;
            List<PriceCategory> priceCategories = null;
            List<Prices> priceList = null;

            // Load product and price data for lists
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.ID))
                {
                    string adminWebUrl = Portal.DigitalPostage.Helpers.ConfigSettings.GetAdminWebUrl(site.RootWeb);
                    using (SPWeb web = site.OpenWeb(adminWebUrl))
                    {
                        //ProductInfo info = new ProductInfo();
                        products = ProductInfo.GetProductInfos(web);

                        PriceCategory pcat = new PriceCategory();
                        priceCategories = pcat.GetProductCategories(web);

                        Prices prices = new Prices();
                        priceList = prices.GetPrices(web);
                    }
                }
            });

            SortedList<string, string> countries = new SortedList<string, string>();
            List<int> weights = new List<int>();

            // this.countrySelector.Width = new Unit(200, UnitType.Pixel);
            this.weightSelector.Width = new Unit(200, UnitType.Pixel);

            StringBuilder scriptBuilder = new StringBuilder();
            scriptBuilder.AppendLine("<script type='text/javascript'>");
            scriptBuilder.AppendLine("  var priceCategories = new Array();");
            scriptBuilder.AppendLine("  var prices = new Array();");
            scriptBuilder.AppendLine("  var texts = new Array();");
            scriptBuilder.AppendLine(string.Format("  var defaultImage = '{0}'", ConfigSettings.GetText("DefaultLabelImage")));
            scriptBuilder.AppendLine();

            // Create pricecategory JS array
            foreach (PriceCategory pc in priceCategories)
            {
                scriptBuilder.AppendFormat("priceCategories[{0}]=['{1}','{2}','{3}']; {4}", pc.Id, pc.AText, pc.BText, pc.Title, Environment.NewLine);
            }
            scriptBuilder.AppendLine();

            // create prices JS array
            foreach (Prices priceObj in priceList)
            {
                scriptBuilder.AppendFormat("prices.push(['{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}']); {8}",
                                            priceObj.Id,
                                            priceObj.Weight,
                                            priceObj.APrice * 100,
                                            priceObj.BPrice * 100,
                                            priceObj.CategoryId,
                                            priceObj.Category,
                                            priceObj.Title,
                                            priceObj.AVAT,
                                            Environment.NewLine);
            }
            scriptBuilder.AppendLine();

            // Add countries to drop down
            foreach (ProductInfo info in products)
            {
                string country = info.Country;
                int weight = info.Weight;

                // change country selector
                //if (!countries.ContainsKey(country))
                //{
                //    countries.Add(country, country);
                //    this.countrySelector.Items.Add(new System.Web.UI.WebControls.ListItem(country, info.PriceCategoryId.ToString()));
                //}

                if (!weights.Contains(weight))
                {
                    weights.Add(weight);
                    this.weightSelector.Items.Add(new System.Web.UI.WebControls.ListItem(string.Format(TextSettings.GetText("BUY_POSTAGE_STEP_1_WEIGHT_SELECT", "maks. {0} g"), weight), weight.ToString()));
                }

                double aprice = info.APrice;
                double bprice = info.BPrice;

                if (aprice < 100)
                {
                    aprice *= 100;
                }
                if (bprice < 100)
                {
                    bprice *= 100;
                }
            }

            // sort countries and select DK as default
            // HelperClass.SortListItems(this.countrySelector.Items, false);
            HelperClass.SortListValues(this.weightSelector.Items, false);

            //for (int i = 0; i < this.countrySelector.Items.Count; i++)
            //{
            //    if (this.countrySelector.Items[i].Text == "Danmark")
            //    {
            //        countrySelector.SelectedIndex = i;
            //        break;
            //    }
            //}


            weightSelector.SelectedIndex = 0;

            string transactionID = HttpContext.Current.Request["transactionid"];
            string responseCode = HttpContext.Current.Request["responsecode"];

            //are the values in the URL?
            if (string.IsNullOrEmpty(transactionID) & string.IsNullOrEmpty(responseCode))
            {//trying to retreive the values from the parent URL
                var uiParentReference = new UIParentReference(HttpContext.Current.Request);
                uiParentReference.UrlParameters.TryGetValue("transactionid", out transactionID); //the URL parameter might not be there
                uiParentReference.UrlParameters.TryGetValue("responsecode", out responseCode); //the URL parameter might not be there
            }


            // Create JS for loading existing order
            scriptBuilder.AppendLine("function LoadOrder() {");

            // Load order if we have transaction code and response is nok OK
            if (!string.IsNullOrEmpty(transactionID) && !string.IsNullOrEmpty(responseCode))
            {
                var order = new Orders(transactionID);
                if (order.Id > 0 && order.Status != Orders.OrderStatusValues.Paid)
                {
                    int prodCount = 0;
                    foreach (SubOrders subOrder in order.SubOrders)
                    {
                        prodCount++;
                        scriptBuilder.AppendFormat("    AddNewProduct({0}, '{1}', 0, {2}, '{3}', {4}, {5}, {6}, {7});{8}", subOrder.Amount, subOrder.Destination, subOrder.Weight, subOrder.Priority, subOrder.Price * 100, subOrder.ProductListId, subOrder.Price * 100, subOrder.VAT, Environment.NewLine);
                    }
                    if (prodCount > 0)
                    {
                        scriptBuilder.AppendLine("  DisplayForm(4);");
                        scriptBuilder.AppendLine("  DisplayStep(4);");

                        scriptBuilder.AppendFormat("  $('#email1').val('{0}');{1}", order.Email, Environment.NewLine);
                        scriptBuilder.AppendFormat("  $('#email2').val('{0}');{1}", order.Email, Environment.NewLine);
                        scriptBuilder.AppendLine("  $('#acceptTerms').prop('checked', true);");
                    }

                    if (order.ImageSize > 0)
                    {
                        scriptBuilder.AppendFormat("   $('#img').attr('src', '/_layouts/digitalpostage/helpers/showorderimg.aspx?id={0}'); {1}", transactionID, Environment.NewLine);
                    }
                }
            }
            scriptBuilder.AppendLine("}");
            scriptBuilder.AppendLine("</script>");

            string random = DateTime.Now.Ticks.ToString();

            scriptBuilder.AppendLine(@"

                <script src='/_layouts/DigitalPostage/js/jquery-1.10.2.js' type='text/javascript'></script>
                <script src='/_layouts/DigitalPostage/js/jquery-ui.js' type='text/javascript'></script>
                <link href='/_layouts/DigitalPostage/js/jquery-ui.css' rel='stylesheet' type='text/css' />
                <script src='/Style%20Library/DigitalPostage/js/jquery.selectBox.js' type='text/javascript'></script>
                <script src='/Style%20Library/DigitalPostage/js/json.js' type='text/javascript'></script>
                <link href='/Style%20Library/DigitalPostage/css/jquery.selectBox.css' rel='stylesheet' type='text/css' />
                <link href='/Style%20Library/DigitalPostage/css/style.css' rel='stylesheet' type='text/css' />

<link rel='stylesheet' href='/_layouts/DigitalPostage/js/wizstepWP.css?" + random + @"' />
                <!--[if IE]>
                    <link rel='stylesheet' href='/Style%20Library/DigitalPostage/css/wizstepie.css' />
                <![endif]-->


<script src='/_layouts/DigitalPostage/js/postageWP.js?rev=" + random + @"' type='text/javascript'></script>
                <script src='/_layouts/DigitalPostage/js/ajaxfileupload.js?rev=" + random + @"' type='text/javascript'></script>
                <script type='text/javascript'>
                    var shoppingCart = new Array();

                    $(document).ready(function () {
                        PostageInit();
                        LoadOrder();
                    });
                </script>");


            if (!this.Page.ClientScript.IsClientScriptBlockRegistered(ScriptNames.PostagePrices.ToString()))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(csType, ScriptNames.PostagePrices.ToString(), scriptBuilder.ToString());
            }
        }



        /// <summary>
        /// Checks the client.
        /// </summary>
        /// <param name="platform">The platform.</param>
        /// <param name="version">The version.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        private string CheckClient(string platform, double version, out string message)
        {
            string webUrl = string.Empty;
            message = string.Empty;
            string listMessage = string.Empty;

            if (string.IsNullOrEmpty(platform))
            {
                return PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.UNKNOWN_ERROR.ToString();
            }
            string returnCode = ""; // "" = ok

            const string APP_VERSIONS_LIST_NAME = "AppVersions";
            Guid siteId = SPContext.Current.Site.ID;

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite siteElevated = new SPSite(siteId))
                {
                    string propertyKey = ConfigSettings.GetPropertyBagKey(siteElevated.RootWeb);
                    if (siteElevated.RootWeb.Properties.ContainsKey(propertyKey))
                    {
                        string url = siteElevated.RootWeb.Properties[propertyKey];
                        using (SPWeb adminWeb = siteElevated.OpenWeb(url))
                        {
                            SPList versions = adminWeb.Lists[APP_VERSIONS_LIST_NAME];

                            SPQuery query = new SPQuery();
                            query.ViewFields = "<FieldRef Name='LinkTitle' /><FieldRef Name='Disabled' /><FieldRef Name='Message' />";
                            query.Query = string.Format(@"<Where><Eq><FieldRef Name='Title' /><Value Type='Text'>{0}</Value></Eq></Where>", platform);

                            SPListItemCollection items = versions.GetItems(query);

                            if (items.Count <= 0) // Platform not supported 
                            {
                                returnCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.PLATFORM_NOT_SUPPORTED.ToString();
                            }
                            else
                            {
                                SPListItem item = items[0];

                                string pform = (string)item[versions.Fields.GetFieldByInternalName("Title").Id];

                                if (versions.Fields.ContainsField("Message"))
                                {
                                    if (item[versions.Fields.GetFieldByInternalName("Message").Id] != null)
                                    {
                                        listMessage = (string)item[versions.Fields.GetFieldByInternalName("Message").Id];
                                    }
                                }

                                bool disabled = false;
                                if (item[versions.Fields.GetFieldByInternalName("Disabled").Id] != null)
                                {
                                    disabled = (bool)item[versions.Fields.GetFieldByInternalName("Disabled").Id];
                                }

                                if (disabled)
                                {
                                    returnCode = PostNord.Portal.DigitalPostage.Helpers.Logging.ErrorCodes.WEB_SHOP_IS_CLOSED.ToString();
                                }
                            }
                        }
                    }
                }
            });

            message = listMessage.Replace("\n", "<br/>");
            return returnCode;
        }

    }
}
