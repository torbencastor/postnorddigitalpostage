﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI.WebControls.WebParts;

namespace PostNord.Portal.DigitalPostage.WebParts.CleanUp
{
    /// <summary>
    /// Clean up database
    /// </summary>
    [ToolboxItemAttribute(false)]
    public partial class CleanUp : WebPart
    {

        int _numberOfFiles = 500;

        /// </summary>
        /// <value>
        /// The human error period in seconds
        /// </value>
        [WebBrowsable(true),
        WebDisplayName("Number of files to process at a time."),
        Personalizable(PersonalizationScope.Shared),
        Category("Settings")]
        public int NumberOfFiles
        {
            get { return _numberOfFiles; }
            set
            {
                if (value >= 0)
                {
                    _numberOfFiles = value;
                }
                else
                {
                    _numberOfFiles = value * -1;
                }
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="CleanUp"/> class.
        /// </summary>
        public CleanUp()
        {
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SPContext.Current.Web.CurrentUser == null)
            {
                pnlCleanUp.Visible = false;
                lblError.Text = "User is not logged in";
                lblError.Visible = true;
                return;
            }
            
            if (!Page.IsPostBack)
            {
                dtcFromDate.SelectedDate = DateTime.Now.AddDays(-30).Date;
                dtcToDate.SelectedDate = DateTime.Now.Date;
            }
        
        }


        /// <summary>
        /// Handles the Click event of the btnDeleteUnfinishedOrders control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnDeleteUnfinishedOrders_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (dtcFromDate.IsDateEmpty || dtcToDate.IsDateEmpty)
            {
                lblError.Text = "Date range is not selected";
                return;
            }

            if (dtcFromDate.SelectedDate > dtcToDate.SelectedDate)
            {
                lblError.Text = "<b>From date</b> can not be before <b>To date</b>.";
                return;
            }

            if (dtcToDate.SelectedDate.Date >= DateTime.Now.Date)
            {
                lblError.Text = "<b>To date</b> should not be Today.";
                return;
            }

            Dal.Orders.DeleteUnUsedOrders(dtcFromDate.SelectedDate, dtcToDate.SelectedDate);
            Search();
        }

        /// <summary>
        /// Handles the Click event of the btnDeleteUnusedImages control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnDeleteUnusedImages_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (dtcFromDate.IsDateEmpty || dtcToDate.IsDateEmpty)
            {
                lblError.Text = "Date range is not selected";
                return;
            }

            if (dtcFromDate.SelectedDate > dtcToDate.SelectedDate)
            {
                lblError.Text = "<b>From date</b> can not be before <b>To date</b>.";
                return;
            }

            if (dtcToDate.SelectedDate.Date >= DateTime.Now.Date)
            {
                lblError.Text = "<b>To date</b> should not be Today.";
                return;
            }

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite siteElevated = new SPSite(SPContext.Current.Site.ID))
                {
                    using (SPWeb web = siteElevated.OpenWeb(siteElevated.RootWeb.ID))
                    {
                        web.AllowUnsafeUpdates = true;

                        SPFolder docLib = web.Folders["Lists"].SubFolders["LabelImages"];

                        DateTime fromDate = dtcFromDate.SelectedDate.Date;
                        DateTime toDate = dtcToDate.SelectedDate.Date;
                        SPQuery query = new SPQuery();

                        query.ViewFields = "<FieldRef Name='LinkFilename' /><FieldRef Name='Title' /><FieldRef Name='Modified' /><FieldRef Name='FileSizeDisplay' /><FieldRef Name='FileType' /><FieldRef Name='VideoHeightInPixels' /><FieldRef Name='VideoWidthInPixels' /><FieldRef Name='ImageSize' />";
                        query.RowLimit = (uint)_numberOfFiles;
                        query.Query = string.Format("<Where><And><Geq><FieldRef Name='Modified' /><Value Type='DateTime' IncludeTimeValue='FALSE'>{0}</Value></Geq><Leq><FieldRef Name='Modified' /><Value Type='DateTime' IncludeTimeValue='FALSE'>{1}</Value></Leq></And></Where>", SPUtility.CreateISO8601DateTimeFromSystemDateTime(fromDate), SPUtility.CreateISO8601DateTimeFromSystemDateTime(toDate));

                        SPListItemCollection files = docLib.DocumentLibrary.GetItems(query);

                        lblError.Visible = true;
                        lblError.Text = string.Format("Deleting: {0} images.", files.Count);

                        int count = 1;

                        if (files.Count > 0)
                        {
                            StringBuilder deletebuilder = new StringBuilder();
                            deletebuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Batch OnError=\"Return\">");
                            string command = "<Method ID=\"{0}\">" + 
                                                "<SetList Scope=\"Request\">" + docLib.ParentListId + "</SetList>" +
                                                "<SetVar Name=\"ID\">{1}</SetVar>" +
                                                "<SetVar Name=\"owsfileref\">{2}</SetVar>" +
                                                "<SetVar Name=\"Cmd\">Delete</SetVar>" +
                                                "</Method>";

                            foreach (SPListItem item in files)
                            {
                                deletebuilder.Append(string.Format(command, count++, item.ID, SPUrlUtility.CombineUrl(SPContext.Current.Site.Url, item.File.Url)));
                            }
                            deletebuilder.Append("</Batch>");

                            lblError.Text = web.ProcessBatchData(deletebuilder.ToString());
                            web.Update();
                        }
                    }
                }
            });

            Search();
        }

        /// <summary>
        /// Searches this instance.
        /// </summary>
        protected void Search()
        {
            lblError.Text = "";
 
            if (dtcFromDate.IsDateEmpty || dtcToDate.IsDateEmpty)
            {
                lblError.Text = "Date range is not selected";
                return;
            }

            if (dtcFromDate.SelectedDate > dtcToDate.SelectedDate)
            {
                lblError.Text = "<b>From date</b> can not be before <b>To date</b>.";
                return;
            }

            try
            {
                int unpaidOrders = 0;
                int paidOrders = 0;
                Bll.Orders.GetStatus(dtcFromDate.SelectedDate, dtcToDate.SelectedDate, out paidOrders, out unpaidOrders);

                lblFinishedOrders.Text = paidOrders.ToString();
                lblUnFinishedOrders.Text = unpaidOrders.ToString();


                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite siteElevated = new SPSite(SPContext.Current.Site.ID))
                    {
                        using (SPWeb web = siteElevated.OpenWeb(siteElevated.RootWeb.ID))
                        {
                            web.AllowUnsafeUpdates = true;

                            SPFolder docLib = web.Folders["Lists"].SubFolders["LabelImages"];

                            DateTime fromDate = dtcFromDate.SelectedDate.Date;
                            DateTime toDate = dtcToDate.SelectedDate.Date;
                            SPQuery query = new SPQuery();

                            query.ViewFields = "<FieldRef Name='LinkFilename' /><FieldRef Name='Title' /><FieldRef Name='Modified' /><FieldRef Name='FileSizeDisplay' /><FieldRef Name='FileType' /><FieldRef Name='VideoHeightInPixels' /><FieldRef Name='VideoWidthInPixels' /><FieldRef Name='ImageSize' />";
                            query.RowLimit = (uint)_numberOfFiles;
                            query.Query = string.Format("<Where><And><Geq><FieldRef Name='Modified' /><Value Type='DateTime' IncludeTimeValue='FALSE'>{0}</Value></Geq><Leq><FieldRef Name='Modified' /><Value Type='DateTime' IncludeTimeValue='FALSE'>{1}</Value></Leq></And></Where>", SPUtility.CreateISO8601DateTimeFromSystemDateTime(fromDate), SPUtility.CreateISO8601DateTimeFromSystemDateTime(toDate));

                            SPListItemCollection files = docLib.DocumentLibrary.GetItems(query);

                            long fileSize = 0;
                            foreach(SPListItem file in files)
                            {
                                if (file["FileSizeDisplay"] != null)
                                {
                                    fileSize += long.Parse(file["FileSizeDisplay"].ToString());
                                }
                            }

                            lblImageSize.Text = GetFileSize(fileSize);
                            lblNumberOfImages.Text = files.Count.ToString();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error: {0}", ex.Message);
            }

        }

        string GetFileSize(long size)
        {
            if (size < 1024)
            {
                return string.Format("{0} bytes", size);
            }
            if (size < (1024 * 1024))
            {
                return string.Format("{0} kb", size / 1024);
            }
            if (size < (1024 * 1024 * 1024))
            {
                return string.Format("{0} Mb", size / (1024 * 1024)); ;
            }

            return string.Format("{0} Gb", size / (1024 * 1024 * 1024)); ;
            
        }

        /// <summary>
        /// Handles the Click event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
        }
    }
}
