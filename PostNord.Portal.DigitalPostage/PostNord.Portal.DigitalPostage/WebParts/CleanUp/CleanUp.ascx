﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CleanUp.ascx.cs" Inherits="PostNord.Portal.DigitalPostage.WebParts.CleanUp.CleanUp" %>
<style type="text/css">

     .header {
         background-color:#e3e3e3;
     }

     .odd {
         background-color:#f3f3f3;
     }

     .table {
         border : 1px solid #d3d3d3;
         width:500px;
         margin-top:15px;
     }

     .wide {
         width:600px;
     }

     .table th, .table td {
        text-align:left;
        padding:3px;
     }
     
    .pagingPanel{
        margin-top:15px;
        margin-bottom:15px;
    }

    .error {
        color:red;
    }

    .number {
        text-align:right;
    }

</style>

<h1>Clean Up</h1>
<asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>


<asp:Panel ID="pnlCleanUp" runat="server">
    <table class="table">
        <tr>
            <th>From date:</th>
        
            <td>
                <SharePoint:DateTimeControl ID="dtcFromDate" runat="server" Calendar="Gregorian" DateOnly="true" LocaleId="1030" CssClassTextBox="dateBox" />
            </td>
            <td>
                <asp:Button ID="btnSearch" runat="server" Text="Lookup" OnClick="btnSearch_Click" />
            </td>
        </tr>
        <tr>
            <th>To date:</th>
            <td>
                <SharePoint:DateTimeControl ID="dtcToDate" runat="server" Calendar="Gregorian" DateOnly="true" LocaleId="1030" CssClassTextBox="dateBox" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>Finished orders:</th>
            <td>
                <asp:Label ID="lblFinishedOrders" runat="server" Text="" CssClass="number"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>Unfinished orders:</th>
            <td>
                <asp:Label ID="lblUnFinishedOrders" runat="server" Text="" CssClass="number"></asp:Label>
            </td>
            <td><asp:Button ID="btnDeleteUnfinishedOrders" runat="server" Text="Delete" OnClick="btnDeleteUnfinishedOrders_Click" /></td>
        </tr>
        <tr>
            <th>Number of images:</th>
            <td>
                <asp:Label ID="lblNumberOfImages" runat="server" Text="" CssClass="number"></asp:Label>
            </td>
            <td><asp:Button ID="btnDeleteUnusedImages" runat="server" Text="Delete" OnClick="btnDeleteUnusedImages_Click" /></td>
        </tr>
        <tr>
            <th>Image consumption:</th>
            <td>
                <asp:Label ID="lblImageSize" runat="server" Text="" CssClass="number"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Panel>