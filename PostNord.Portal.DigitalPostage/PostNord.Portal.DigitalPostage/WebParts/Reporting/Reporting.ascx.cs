﻿using Microsoft.SharePoint;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace PostNord.Portal.DigitalPostage.WebParts.Reporting
{
    /// <summary>
    /// Reporting WebPart
    /// </summary>
    [ToolboxItemAttribute(false)]
    public partial class Reporting : WebPart
    {

        private double OrderSum = 0;
        private double CreditSum = 0;
        private double SaldoSum = 0;
        private int AmountSum = 0;
        private int OrderCount = 0;
        private int TotalOrderCount = 0;
        private int ReimbursedCount = 0;


        /// <summary>
        /// Initializes a new instance of the <see cref="Reporting"/> class.
        /// </summary>
        public Reporting()
        {
        }


        private bool ValidateInput()
        {
            lblError.Text = string.Empty;

            if (dtcFromDate.IsDateEmpty || dtcToDate.IsDateEmpty)
            {
                lblError.Text = "Please fill out Date Fields";
                return false;
            }

            if (dtcFromDate.SelectedDate > dtcToDate.SelectedDate)
            {
                lblError.Text = "From date is larger than to date!";
                return false;
            }

            return true;
        }


        #region Page Events
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // set the client click handler. If this is not done Post backs stop working.
                btnExport.OnClientClick = "window.setTimeout(function() { _spFormOnSubmitCalled = false; }, 10);";

                dtcFromDate.SelectedDate = DateTime.Today.AddMonths(-1);
                dtcToDate.SelectedDate = DateTime.Today;
                HideReports();
            }
        }

        #endregion

        #region Button Events

        /// <summary>
        /// Handles the Click event of the btnShowReport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnShowReport_Click(object sender, EventArgs e)
        {
            if (!ValidateInput())
            {
                return;
            }

            this.HideReports();

            switch (drpReportType.SelectedValue)
            {
                case "SalePerProduct":
                    this.ShowReportSalePerProductWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "SalePerProductWithoutVat":
                    this.ShowReportSalePerProductWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "SalePerCustomer":
                    this.ShowReportSalePerCustomerWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "SalePerCustomerWithoutVat":
                    this.ShowReportSalePerCustomerWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "CustomerGroups":
                case "CustomerGroupsOnlinePorto":
                case "CustomerGroupsMobilPorto":
                case "CustomerGroupsWithoutVat":
                case "CustomerGroupsOnlinePortoWithoutVat":
                case "CustomerGroupsMobilPortoWithoutVat":
                    this.ShowReportCustomerGroups(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1), drpReportType.SelectedValue);
                    break;
                case "Reimbursements":
                    this.ShowReportReimbursementsWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "ReimbursementsWithoutVat":
                    this.ShowReportReimbursementsWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "SalesChannels":
                    this.ShowReportChannelSalesWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "SalesChannelsWithoutVat":
                    this.ShowReportChannelSalesWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "CodeValidation":
                    this.ShowReportCodeValidationSum(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    this.ShowReportCodeValidationAndOrder(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "RefundedOrderWithVat":
                    this.ShowReportRefundedOrderWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "RefundedOrderWithoutVat":
                    this.ShowReportRefundedOrderWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                default:
                    lblError.Text = string.Format("Unknown Report selection: {0}", drpReportType.SelectedValue);
                    lblError.Visible = true;
                    break;

            }
        }



        /// <summary>
        /// Handles the Click event of the btnExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (!ValidateInput())
            {
                return;
            }
            switch (drpReportType.SelectedValue)
            {
                case "SalePerProduct":
                    this.ExportReportSalePerProductWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddMilliseconds(-1));
                    break;
                case "SalePerProductWithoutVat":
                    this.ExportReportSalePerProductWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddMilliseconds(-1));
                    break;
                case "SalePerCustomer":
                    this.ExportReportSalePerCustomerWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "SalePerCustomerWithoutVat":
                    this.ExportReportSalePerCustomerWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "CustomerGroups":
                case "CustomerGroupsOnlinePorto":
                case "CustomerGroupsMobilPorto":
                case "CustomerGroupsWithoutVat":
                case "CustomerGroupsOnlinePortoWithoutVat":
                case "CustomerGroupsMobilPortoWithoutVat":
                    this.ExportReportCustomerGroups(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddMilliseconds(-1), drpReportType.SelectedValue);
                    break;
                case "Reimbursements":
                    this.ExportReportReimbursementsWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "ReimbursementsWithoutVat":
                    this.ExportReportReimbursementsWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "SalesChannels":
                    this.ExportReportChannelSalesWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "SalesChannelsWithoutVat":
                    this.ExportReportChannelSalesWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "CodeValidation":
                    //this is called first, because the export uses data from the UI table
                    this.ShowReportCodeValidationSum(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    this.ExportReportCodeValidationSum(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "RefundedOrderWithVat":
                    this.ExportReportRefundedOrderWithVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                case "RefundedOrderWithoutVat":
                    this.ExportReportRefundedOrderWithoutVat(dtcFromDate.SelectedDate, dtcToDate.SelectedDate.AddDays(1).AddSeconds(-1));
                    break;
                default:
                    lblError.Text = string.Format("Unknown Export Selection: {0}", drpReportType.SelectedValue);
                    lblError.Visible = true;
                    break;
            }
        }



        #endregion

        #region Reports

        private void HideReports()
        {
            pnlSalePerCustomer.Visible = false;
            lblSalePerCustomerWithVat.Visible = false;
            lblSalePerCustomerWithoutVat.Visible = false;
            pnlSalePerProduct.Visible = false;
            lblSalePerProduct.Visible = false;
            lblSalePerProductWithoutVat.Visible = false;
            pnlCustomerGroups.Visible = false;
            pnlReimbursements.Visible = false;
            lblReimbursements.Visible = false;
            lblReimbursementsWithoutVat.Visible = false;
            pnlCodeValidation.Visible = false;
            pnlChannelSales.Visible = false;
            lblChannelSalesWithVat.Visible = false;
            lblChannelSalesWithoutVat.Visible = false;
            pnlRefundOrderWithVat.Visible = false;
            lblReportRefundWithVat.Visible = false;
            lblReportRefundWithoutVat.Visible = false;
            pnlReportCodeValidationAndOrder.Visible = false;

            //clear data
            rptChannelSales.DataSource = null;
            rptRefundOrderWithVat.DataSource = null;
            rptReimbursements.DataSource = null;
            rptSalePerCustomer.DataSource = null;
            rptSalePerProduct.DataSource = null;
            rptReportCodeValidationAndOrder.DataSource = null;

            rptChannelSales.DataBind();
            rptRefundOrderWithVat.DataBind();
            rptReimbursements.DataBind();
            rptSalePerCustomer.DataBind();
            rptSalePerProduct.DataBind();
            rptReportCodeValidationAndOrder.DataBind();


        }

        /// <summary>
        /// Gets the payment method.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <returns></returns>
        protected string GetPaymentMethod(object method)
        {
            Bll.Orders.PaymentMethods result = (Bll.Orders.PaymentMethods)Enum.Parse(typeof(Bll.Orders.PaymentMethods), method.ToString());
            return result.ToString();
        }

        /// <summary>
        /// Shows the report sale per product.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ShowReportCodeValidationSum(DateTime fromDate, DateTime toDate)
        {
            pnlCodeValidation.Visible = true;
            try
            {
                using (SqlDataReader row = Dal.ValidationResults.GetReportValidationResultSums(fromDate, toDate))
                {
                    List<int> errorCounts = new List<int>();
                    int errorSum = 0;
                    while (row.Read())
                    {
                        TableRow tr = new TableRow();
                        TableHeaderCell th = new TableHeaderCell();
                        th.CssClass = "left";
                        th.Text = row["Status"].ToString();
                        tr.Cells.Add(th);

                        TableCell td = new TableCell();
                        int errorCount = (int)row["ErrorCount"];
                        errorSum += errorCount;
                        errorCounts.Add(errorCount);
                        td.Text = errorCount.ToString();
                        td.CssClass = "right";
                        tr.Cells.Add(td);

                        // Add empty cell for percentage
                        td = new TableCell();
                        td.CssClass = "right";
                        tr.Cells.Add(td);

                        tblCodeValidation.Rows.Add(tr);
                    }

                    float sum = 0;
                    for (int i = 0; i < errorCounts.Count; i++)
                    {
                        float percentage = (float)errorCounts[i] / errorSum;
                        sum += percentage;
                        tblCodeValidation.Rows[i + 1].Cells[2].Text = string.Format(new System.Globalization.CultureInfo("da-DK"), "{0:P}", percentage);
                    }

                    TableRow footerRow = new TableRow();
                    TableHeaderCell footerTh = new TableHeaderCell();
                    footerTh.CssClass = "left";
                    footerTh.Text = "Total:";
                    footerRow.Cells.Add(footerTh);

                    footerTh = new TableHeaderCell();
                    footerTh.CssClass = "right";
                    footerTh.Text = errorSum.ToString();
                    footerRow.Cells.Add(footerTh);

                    footerTh = new TableHeaderCell();
                    footerTh.CssClass = "right";
                    footerTh.Text = string.Format(new System.Globalization.CultureInfo("da-DK"), "{0:P}", sum);
                    footerRow.Cells.Add(footerTh);

                    tblCodeValidation.Rows.Add(footerRow);
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: CodeValidationSum, Message: {0}", ex.Message);
            }
        }

        protected DataTable GetReportSalePerCustomerWithVat(DateTime fromDate, DateTime toDate)
        {
            DataSet dsSaleGroupedOnCustomers = Dal.Orders.GetReportGroupedOnCustomersDataSetWithVat(fromDate, toDate);
            DataSet dsReimbursementsPerCustomer = Dal.Orders.GetReportViewReimbursementsPerCustomerWithVat(fromDate, toDate);
            var result = GetReportSalePerCustomer(dsSaleGroupedOnCustomers, dsReimbursementsPerCustomer);
            return result;
        }

        protected DataTable GetReportSalePerCustomerWithoutVat(DateTime fromDate, DateTime toDate)
        {
            DataSet dsSaleGroupedOnCustomers = Dal.Orders.GetReportGroupedOnCustomersDataSetWithoutVat(fromDate, toDate);
            DataSet dsReimbursementsPerCustomer = Dal.Orders.GetReportViewReimbursementsPerCustomerWithoutVat(fromDate, toDate);
            var result = GetReportSalePerCustomer(dsSaleGroupedOnCustomers, dsReimbursementsPerCustomer);
            return result;
        }

        /// <summary>
        /// Gets the report sale per customer.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException"></exception>
        protected DataTable GetReportSalePerCustomer(DataSet dsSaleGroupedOnCustomers, DataSet dsReimbursementsPerCustomer)
        {

            DataTable dtSaleGroupedOnCustomers = dsSaleGroupedOnCustomers.Tables[0];
            DataTable dtReimbursementsPerCustomer = dsReimbursementsPerCustomer.Tables[0];

            // Loop all customers
            foreach (DataRow row in dtSaleGroupedOnCustomers.Rows)
            {
                // Find matching reimbursements
                DataRow[] matchingRows = dtReimbursementsPerCustomer.Select(string.Format("Email = '{0}'", row["Email"]));
                if (matchingRows.Length > 1)
                {
                    throw new ApplicationException(string.Format("Too many matching rows: {0}, Email: {1}", matchingRows.Length, row["Email"]));
                }

                // Add Reimbursement info to sale row
                if (matchingRows.Length > 0)
                {
                    DataRow matchingRow = matchingRows[0];
                    row["Amount"] = (int)row["Amount"] - (int)matchingRow["Amount"];
                    row["CreditSum"] = (double)matchingRow["CreditSum"];
                    row["Saldo"] = (double)row["OrderSum"] - (double)matchingRow["CreditSum"];
                }
                else
                {
                    row["CreditSum"] = 0;
                }
            }

            // Loop Reimbursements and make sure we dont have row without a match in Reimbursements
            foreach (DataRow row in dtReimbursementsPerCustomer.Rows)
            {
                DataRow[] matchingRows = dtSaleGroupedOnCustomers.Select(string.Format("Email = '{0}'", row["Email"]));
                if (matchingRows.Length == 0)
                {
                    DataRow newRow = dtSaleGroupedOnCustomers.NewRow();
                    newRow["Email"] = (string)row["Email"];
                    newRow["OrderSum"] = newRow["Saldo"] = newRow["CreditSum"] = 0 - (double)row["CreditSum"];
                    newRow["Amount"] = 0 - (int)row["Amount"];
                    dtSaleGroupedOnCustomers.Rows.Add(newRow);
                }
            }

            dtSaleGroupedOnCustomers.DefaultView.Sort = "Saldo desc";

            return dtSaleGroupedOnCustomers.DefaultView.Table;
        }


        /// <summary>
        /// Gets the report sale per product data.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Datatable containing relevant data</returns>
        protected DataTable GetReportSalePerProductDataWithVat(DateTime fromDate, DateTime toDate)
        {
            DataSet dsSale = Dal.Orders.GetReportSalePerProductWithVat(fromDate, toDate);
            DataSet dsReimbursements = Dal.Orders.GetReportReimbursedSalePerProductWithVat(fromDate, toDate);
            var result = GetReportSalePerProductData(dsSale, dsReimbursements);
            return result;
        }

        /// <summary>
        /// Gets the report sale per product data without vat.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Datatable containing relevant data</returns>
        protected DataTable GetReportSalePerProductDataWithoutVat(DateTime fromDate, DateTime toDate)
        {
            DataSet dsSale = Dal.Orders.GetReportSalePerProductWithoutVat(fromDate, toDate);
            DataSet dsReimbursements = Dal.Orders.GetReportReimbursedSalePerProductWithoutVat(fromDate, toDate);
            var result = GetReportSalePerProductData(dsSale, dsReimbursements);
            return result;
        }
        /// <summary>
        /// Gets the report sale per product data.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Datatable containing relevant data</returns>
        protected DataTable GetReportSalePerProductData(DataSet dsSale, DataSet dsReimbursements)
        {

            DataTable dtReimbusements = dsReimbursements.Tables[0];
            DataTable dtSale = dsSale.Tables[0];

            dtReimbusements.Columns.Add("Product");
            dtReimbusements.Columns.Add("Reimbursed", System.Type.GetType("System.Int32"));
            dtReimbusements.Columns.Add("Total", System.Type.GetType("System.Double"));
            foreach (DataRow row in dtReimbusements.Rows)
            {
                row["Product"] = string.Format("{0} {1}g {2}", row["Destination"], row["Weight"], row["Priority"]);
            }

            dtSale.Columns.Add("Product");
            dtSale.Columns.Add("Reimbursed", System.Type.GetType("System.Int32"));
            dtSale.Columns.Add("Total", System.Type.GetType("System.Double"));
            foreach (DataRow row in dtSale.Rows)
            {
                row["Product"] = string.Format("{0} {1}g {2}", row["Destination"], row["Weight"], row["Priority"]);

                // Find matching rows
                DataRow[] matchingRows = dtReimbusements.Select(string.Format("Product = '{0}' AND Price='{1}'", row["Product"], row["Price"]));
                if (matchingRows.Length > 1)
                {
                    throw new ApplicationException(string.Format("Too many matching rows: {0}, Product: {1}", matchingRows.Length, row["Product"]));
                }

                // Add Reimbursement info to sale row
                if (matchingRows.Length > 0)
                {
                    DataRow matchingRow = matchingRows[0];
                    row["Reimbursed"] = (int)matchingRow["SubAmount"];
                    row["Total"] = ((int)row["SubAmount"] - (int)matchingRow["SubAmount"]) * (double)row["Price"];
                }
                else
                {
                    row["Reimbursed"] = (int)0;
                    row["Total"] = (int)row["SubAmount"] * (double)row["Price"];
                }
            }

            // Loop Reimbursements and make sure we dont have row without a match in Reimbursements
            foreach (DataRow row in dtReimbusements.Rows)
            {
                DataRow[] matchingRows = dtSale.Select(string.Format("Product = '{0}'", row["Product"]));
                if (matchingRows.Length == 0)
                {
                    DataRow newRow = dtSale.NewRow();
                    newRow["Destination"] = (string)row["Destination"];
                    newRow["Product"] = (string)row["Product"];
                    newRow["Price"] = (double)row["Price"];
                    newRow["SubAmount"] = (int)0;
                    newRow["SubSum"] = (double)0;
                    newRow["Reimbursed"] = (int)row["SubAmount"];
                    newRow["Total"] = (0 - (int)newRow["Reimbursed"]) * (double)newRow["Price"];
                    dtSale.Rows.Add(newRow);
                }
            }

            dtSale.DefaultView.Sort = "Destination asc, Weight asc, Priority asc";

            return dtSale.DefaultView.Table;

        }

        /// <summary>
        /// Shows the report sale per product.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ShowReportSalePerProductWithVat(DateTime fromDate, DateTime toDate)
        {
            pnlSalePerProduct.Visible = true;
            lblSalePerProduct.Visible = true;

            try
            {
                DataTable report = this.GetReportSalePerProductDataWithVat(fromDate, toDate);
                rptSalePerProduct.DataSource = report;
                rptSalePerProduct.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportSalePerProduct, Message: {0}", ex.Message);
            }
        }
        /// <summary>
        /// Shows the report sale per product without vat.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ShowReportSalePerProductWithoutVat(DateTime fromDate, DateTime toDate)
        {
            pnlSalePerProduct.Visible = true;
            lblSalePerProductWithoutVat.Visible = true;
            try
            {
                DataTable report = this.GetReportSalePerProductDataWithoutVat(fromDate, toDate);
                rptSalePerProduct.DataSource = report;
                rptSalePerProduct.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportSalePerProduct, Message: {0}", ex.Message);
            }
        }


        /// <summary>
        /// Shows the report amount interval per customer.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ShowReportCustomerGroups(DateTime fromDate, DateTime toDate, string reportType)
        {
            pnlCustomerGroups.Visible = true;
            string text = string.Empty;
            switch (reportType)
            {
                case "CustomerGroupsWithoutVat":
                case "CustomerGroups":
                    text = " - Total";
                    break;
                case "CustomerGroupsOnlinePortoWithoutVat":
                case "CustomerGroupsOnlinePorto":
                    text = " - WebShop";
                    break;
                case "CustomerGroupsMobilPortoWithoutVat":
                case "CustomerGroupsMobilPorto":
                    text = " - App";
                    break;
            }

            text += reportType.EndsWith("WithoutVat") ? " without vat" : " with vat";

            lblCustomerGroupType.Text = text;

            try
            {
                // Get interval for number of codes
                Hashtable amountIntervalsTable = Bll.Orders.GetOrdersIntervalsAmount(fromDate, toDate, reportType);

                lblIntAmount1.Text = amountIntervalsTable["1"].ToString();
                lblIntAmount2_5.Text = amountIntervalsTable["2"].ToString();
                lblIntAmount6_10.Text = amountIntervalsTable["6"].ToString();
                lblIntAmount11_100.Text = amountIntervalsTable["11"].ToString();
                lblIntAmount101.Text = amountIntervalsTable["101"].ToString();

                int sum = 0;
                foreach (DictionaryEntry entry in amountIntervalsTable)
                {
                    sum += (int)entry.Value;
                }

                lblIntAmountSum.Text = sum.ToString();


                // Get interval for sales
                amountIntervalsTable = Bll.Orders.GetOrdersIntervalsSales(fromDate, toDate, reportType);

                lblSale0.Text = amountIntervalsTable["0"].ToString();
                lblSale51.Text = amountIntervalsTable["51"].ToString();
                lblSale101.Text = amountIntervalsTable["101"].ToString();
                lblSale501.Text = amountIntervalsTable["501"].ToString();
                lblSale1001.Text = amountIntervalsTable["1001"].ToString();

                sum = 0;
                foreach (DictionaryEntry entry in amountIntervalsTable)
                {
                    sum += (int)entry.Value;
                }

                lblSaleSum.Text = sum.ToString();
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportAmountIntervalPerCustomer, Message: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Shows the report sale per product.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ShowReportSalePerCustomerWithVat(DateTime fromDate, DateTime toDate)
        {
            pnlSalePerCustomer.Visible = true;
            lblSalePerCustomerWithVat.Visible = true;

            try
            {
                rptSalePerCustomer.DataSource = this.GetReportSalePerCustomerWithVat(fromDate, toDate);
                rptSalePerCustomer.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportSalePerCustomerWithVat, Message: {0}", ex.Message);
            }
        }
        /// <summary>
        /// Shows the report sale per product.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ShowReportSalePerCustomerWithoutVat(DateTime fromDate, DateTime toDate)
        {
            pnlSalePerCustomer.Visible = true;
            lblSalePerCustomerWithoutVat.Visible = true;
            try
            {
                rptSalePerCustomer.DataSource = this.GetReportSalePerCustomerWithoutVat(fromDate, toDate);
                rptSalePerCustomer.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportSalePerCustomerWithoutVat, Message: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Shows the report sale per product width vat.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ShowReportReimbursementsWithVat(DateTime fromDate, DateTime toDate)
        {
            pnlReimbursements.Visible = true;
            lblReimbursements.Visible = true;

            try
            {
                using (SqlDataReader MyReader = Dal.Orders.GetReportReimbursementsWithVat(fromDate, toDate))
                {
                    rptReimbursements.DataSource = MyReader;
                    rptReimbursements.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportReimbursements, Message: {0}", ex.Message);
            }
        }
        /// <summary>
        /// Shows the report sale per product without vat.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ShowReportReimbursementsWithoutVat(DateTime fromDate, DateTime toDate)
        {
            pnlReimbursements.Visible = true;
            lblReimbursementsWithoutVat.Visible = true;

            try
            {
                using (SqlDataReader MyReader = Dal.Orders.GetReportReimbursementsWithoutVat(fromDate, toDate))
                {
                    rptReimbursements.DataSource = MyReader;
                    rptReimbursements.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportReimbursements, Message: {0}", ex.Message);
            }
        }


        /// <summary>
        /// Shows the report refund per product with vat.
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        private void ShowReportRefundedOrderWithVat(DateTime fromDate, DateTime toDate)
        {
            pnlRefundOrderWithVat.Visible = true;
            lblReportRefundWithVat.Visible = true;

            try
            {
                using (SqlDataReader MyReader = Dal.Orders.GetReportRefundedOrderWithVat(fromDate, toDate))
                {
                    rptRefundOrderWithVat.DataSource = MyReader;
                    rptRefundOrderWithVat.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportRefundsWithVat, Message: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Shows the report refund per product without vat.
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        private void ShowReportCodeValidationAndOrder(DateTime fromDate, DateTime toDate)
        {
            pnlReportCodeValidationAndOrder.Visible = true;
            try
            {
                using (SqlDataReader MyReader = Dal.ValidationResults.GetReportCodeValidationAndOrder(fromDate, toDate))
                {
                    rptReportCodeValidationAndOrder.DataSource = MyReader;
                    rptReportCodeValidationAndOrder.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportCodeValidationAndOrder, Message: {0}", ex.Message);
            }
        }
        /// <summary>
        /// Shows the report refund per product without vat.
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        private void ShowReportRefundedOrderWithoutVat(DateTime fromDate, DateTime toDate)
        {
            pnlRefundOrderWithVat.Visible = true;
            lblReportRefundWithoutVat.Visible = true;

            try
            {
                using (SqlDataReader MyReader = Dal.Orders.GetReportRefundedOrderWithoutVat(fromDate, toDate))
                {
                    rptRefundOrderWithVat.DataSource = MyReader;
                    rptRefundOrderWithVat.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportRefundsWithoutVat, Message: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Shows the report channel sales.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ShowReportChannelSalesWithVat(DateTime fromDate, DateTime toDate)
        {
            pnlChannelSales.Visible = true;
            lblChannelSalesWithVat.Visible = true;

            try
            {
                using (SqlDataReader MyReader = Dal.Orders.GetReportReportChannelsWithVat(fromDate, toDate))
                {
                    rptChannelSales.DataSource = MyReader;
                    rptChannelSales.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportChannelSalesWithVat, Message: {0}", ex.Message);
            }
        }

        protected void ShowReportChannelSalesWithoutVat(DateTime fromDate, DateTime toDate)
        {
            pnlChannelSales.Visible = true;
            lblChannelSalesWithoutVat.Visible = true;

            try
            {
                using (SqlDataReader MyReader = Dal.Orders.GetReportReportChannelsWithoutVat(fromDate, toDate))
                {
                    rptChannelSales.DataSource = MyReader;
                    rptChannelSales.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportChannelSalesWithoutVat, Message: {0}", ex.Message);
            }
        }

        #endregion

        #region Excel Export

        private static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName)
        {
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[1];
            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet
            return ws;
        }

        private string ToDate(object data)
        {
            DateTime t = (DateTime)data;
            return t.ToString("dd-MM-yyyy");
        }

        private string ToDateTime(object data)
        {
            DateTime t = (DateTime)data;
            return t.ToString("dd-MM-yyyy HH:mm:ss");
        }

        private string ToString(object data)
        {
            if (data == DBNull.Value || data == null)
            {
                return string.Empty;
            }

            return data.ToString();
        }
        /// <summary>
        /// Export the report sale per product.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportSalePerProductWithoutVat(DateTime fromDate, DateTime toDate)
        {
            DataTable report = this.GetReportSalePerProductDataWithoutVat(fromDate, toDate);
            ExportReportSalePerProduct(fromDate, toDate, report, true);
        }

        /// <summary>
        /// Export the report sale per product.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportSalePerProductWithVat(DateTime fromDate, DateTime toDate)
        {
            DataTable report = this.GetReportSalePerProductDataWithVat(fromDate, toDate);
            ExportReportSalePerProduct(fromDate, toDate, report, false);
        }

        protected void ExportReportSalePerProduct(DateTime fromDate, DateTime toDate, DataTable report, bool withoutVat)
        {
            try
            {

                using (ExcelPackage package = new ExcelPackage())
                {
                    var name = withoutVat ? "SalePerProductWithoutVat" : "SalePerProductWithVat";
                    ExcelWorksheet sheet = CreateSheet(package, name);

                    sheet.Column(1).Width = 35;
                    sheet.Column(2).Width = 15;
                    sheet.Column(5).Width = 15;

                    //Merging cells and create a center heading for out table
                    var title = withoutVat ? "Digital Postage Sale pr. product without vat" : "Digital Postage Sale pr. product width vat";
                    sheet.Cells[1, 1].Value = title; // Heading Name
                    sheet.Cells[1, 1, 1, 1].Style.Font.Bold = true; //Font should be bold

                    sheet.Cells[3, 1].Value = "From date:";
                    sheet.Cells[3, 2].Value = fromDate.ToString("dd-MM-yyyy");

                    sheet.Cells[4, 1].Value = "To date:";
                    sheet.Cells[4, 2].Value = toDate.ToString("dd-MM-yyyy");

                    //sheet.Cells[5, 1].Value = "Product count:";

                    sheet.Cells[3, 1, 5, 1].Style.Font.Bold = true; //Font should be bold

                    int startrow = 7;
                    int rownum = startrow;

                    sheet.Cells[rownum, 1].Value = "Product";
                    sheet.Cells[rownum, 2].Value = "Price";
                    sheet.Cells[rownum, 3].Value = "Amount";
                    sheet.Cells[rownum, 4].Value = "Sale";
                    sheet.Cells[rownum, 5].Value = "Reimbursed";
                    sheet.Cells[rownum, 6].Value = "Total";
                    sheet.Cells[rownum, 1, rownum, 6].Style.Font.Bold = true; //Font should be bold



                    // Create DataView
                    DataView view = new DataView(report);
                    // Sort by State and ZipCode column in descending order
                    view.Sort = "Destination asc, Weight asc, Priority asc";


                    int subAmountSum = 0;
                    double subSumSum = 0;
                    int reimbursedSum = 0;
                    double totalSum = 0;


                    foreach (DataRowView row in view)
                    {
                        rownum++;
                        sheet.Cells[rownum, 1].Value = row["Product"];
                        sheet.Cells[rownum, 2].Value = row["Price"];
                        sheet.Cells[rownum, 3].Value = (int)row["SubAmount"];
                        sheet.Cells[rownum, 4].Value = (double)row["SubSum"];
                        sheet.Cells[rownum, 5].Value = (int)row["Reimbursed"];
                        sheet.Cells[rownum, 6].Value = (double)row["Total"];


                        subAmountSum += (int)row["SubAmount"];
                        subSumSum += (double)row["SubSum"];
                        reimbursedSum += (int)row["Reimbursed"];
                        totalSum += (double)row["Total"];

                    }

                    rownum++;

                    sheet.Cells[rownum, 1].Value = "Total:";

                    sheet.Cells[rownum, 3].Value = subAmountSum;
                    sheet.Cells[rownum, 4].Value = subSumSum;
                    sheet.Cells[rownum, 5].Value = reimbursedSum;
                    sheet.Cells[rownum, 6].Value = totalSum;


                    sheet.Cells[rownum, 1, rownum, 6].Style.Font.Bold = true; //Font should be bold
                    var fileprefix = withoutVat ? "DigitalPostageSalePerProductWithoutVat" : "DigitalPostageSalePerProductWithVat";
                    string filename = string.Format("{2}_{0}-{1}.xlsx", fromDate.ToString("yyyyMMdd"), toDate.ToString("yyyyMMdd"), fileprefix);

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", filename));
                    using (MemoryStream ms = new MemoryStream())
                    {
                        package.SaveAs(ms);
                        ms.WriteTo(HttpContext.Current.Response.OutputStream);
                        ms.Close();
                    }
                    try
                    {
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                    catch { }
                    return;
                }

            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportSalePerProduct{1}, Message: {0}", ex.Message, withoutVat ? "WithoutVat" : "WithVat");
            }
        }


        /// <summary>
        /// Shows the report amount interval per customer.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportCustomerGroups(DateTime fromDate, DateTime toDate, string reportType)
        {
            try
            {

                string headline = string.Empty;
                switch (reportType)
                {
                    case "CustomerGroups":
                    case "CustomerGroupsWithoutVat":
                        headline = " - Total"; // Heading Name
                        break;
                    case "CustomerGroupsOnlinePorto":
                    case "CustomerGroupsOnlinePortoWithoutVat":
                        headline = " - WebShop"; // Heading Name
                        break;
                    case "CustomerGroupsMobilPorto":
                    case "CustomerGroupsMobilPortoWithoutVat":
                        headline = " - App"; // Heading Name
                        break;
                }
                headline += reportType.EndsWith("WithoutVat") ? " without vat" : " with vat";

                using (ExcelPackage package = new ExcelPackage())
                {


                    ExcelWorksheet sheet = CreateSheet(package, "Customer groups" + headline);

                    sheet.Column(1).Width = 25;
                    sheet.Column(2).Width = 25;
                    sheet.Column(3).Width = 25;
                    sheet.Column(4).Width = 25;

                    sheet.Cells[1, 1].Value = "Digital Postage - Customer groups" + headline; // Heading Name

                    sheet.Cells[1, 1, 1, 1].Style.Font.Bold = true; //Font should be bold

                    sheet.Cells[3, 1].Value = "From date:";
                    sheet.Cells[3, 2].Value = fromDate.ToString("dd-MM-yyyy");

                    sheet.Cells[4, 1].Value = "To date:";
                    sheet.Cells[4, 2].Value = toDate.ToString("dd-MM-yyyy");

                    sheet.Cells[3, 1, 4, 1].Style.Font.Bold = true; //Font should be bold

                    int firstrow = 6;

                    sheet.Cells[firstrow, 1].Value = "Number of codes";
                    sheet.Cells[firstrow, 2].Value = "Number of customers";
                    sheet.Cells[firstrow, 3].Value = "Sale DKK";
                    sheet.Cells[firstrow, 4].Value = "Number of customers";
                    sheet.Cells[firstrow, 1, firstrow, 4].Style.Font.Bold = true; //Font should be bold

                    sheet.Cells[firstrow + 1, 1].Value = "1";
                    sheet.Cells[firstrow + 2, 1].Value = "2-5";
                    sheet.Cells[firstrow + 3, 1].Value = "6-10";
                    sheet.Cells[firstrow + 4, 1].Value = "11-100";
                    sheet.Cells[firstrow + 5, 1].Value = "> 100";
                    sheet.Cells[firstrow + 6, 1].Value = "Sum";
                    sheet.Cells[firstrow + 1, 1, firstrow + 6, 1].Style.Font.Bold = true; //Font should be bold

                    // Get interval for number of codes
                    Hashtable amountIntervalsTable = Bll.Orders.GetOrdersIntervalsAmount(fromDate, toDate, reportType);
                    sheet.Cells[firstrow + 1, 2].Value = (int)amountIntervalsTable["1"];
                    sheet.Cells[firstrow + 2, 2].Value = (int)amountIntervalsTable["2"];
                    sheet.Cells[firstrow + 3, 2].Value = (int)amountIntervalsTable["6"];
                    sheet.Cells[firstrow + 4, 2].Value = (int)amountIntervalsTable["11"];
                    sheet.Cells[firstrow + 5, 2].Value = (int)amountIntervalsTable["101"];
                    sheet.Cells[firstrow + 6, 2].Value = (int)amountIntervalsTable["1"] + (int)amountIntervalsTable["2"] + (int)amountIntervalsTable["6"] + (int)amountIntervalsTable["11"] + (int)amountIntervalsTable["101"];
                    sheet.Cells[firstrow + 6, 2].Style.Font.Bold = true;

                    sheet.Cells[firstrow + 1, 3].Value = "0-50";
                    sheet.Cells[firstrow + 2, 3].Value = "51-100";
                    sheet.Cells[firstrow + 3, 3].Value = "101-500";
                    sheet.Cells[firstrow + 4, 3].Value = "501-1000";
                    sheet.Cells[firstrow + 5, 3].Value = "> 1000";
                    sheet.Cells[firstrow + 1, 3, firstrow + 5, 3].Style.Font.Bold = true; //Font should be bold

                    // Get interval for number of codes
                    amountIntervalsTable = Bll.Orders.GetOrdersIntervalsSales(fromDate, toDate, reportType);
                    sheet.Cells[firstrow + 1, 4].Value = (int)amountIntervalsTable["0"];
                    sheet.Cells[firstrow + 2, 4].Value = (int)amountIntervalsTable["51"];
                    sheet.Cells[firstrow + 3, 4].Value = (int)amountIntervalsTable["101"];
                    sheet.Cells[firstrow + 4, 4].Value = (int)amountIntervalsTable["501"];
                    sheet.Cells[firstrow + 5, 4].Value = (int)amountIntervalsTable["1001"];
                    sheet.Cells[firstrow + 6, 4].Value = (int)amountIntervalsTable["0"] + (int)amountIntervalsTable["51"] + (int)amountIntervalsTable["101"] + (int)amountIntervalsTable["501"] + (int)amountIntervalsTable["1001"];
                    sheet.Cells[firstrow + 6, 4].Style.Font.Bold = true;

                    string filename = string.Format("DigitalPostage{0}_{1}-{2}.xlsx", reportType, fromDate.ToString("yyyyMMdd"), toDate.ToString("yyyyMMdd"));

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", filename));
                    using (MemoryStream ms = new MemoryStream())
                    {
                        package.SaveAs(ms);
                        ms.WriteTo(HttpContext.Current.Response.OutputStream);
                        ms.Close();
                    }
                    try
                    {
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                    catch { }

                    return;
                }

            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ShowReportAmountIntervalPerCustomer, Message: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Shows the report sale per product.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportSalePerCustomerWithVat(DateTime fromDate, DateTime toDate)
        {
            DataTable salePerCustomerTable = GetReportSalePerCustomerWithVat(fromDate, toDate);
            ExportReportSalePerCustomer(fromDate, toDate, false, salePerCustomerTable);
        }

        /// <summary>
        /// Shows the report sale per product.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportSalePerCustomerWithoutVat(DateTime fromDate, DateTime toDate)
        {
            DataTable salePerCustomerTable = GetReportSalePerCustomerWithoutVat(fromDate, toDate);
            ExportReportSalePerCustomer(fromDate, toDate, false, salePerCustomerTable);
        }


        /// <summary>
        /// Shows the report sale per product.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportSalePerCustomer(DateTime fromDate, DateTime toDate, bool withoutVat, DataTable salePerCustomerTable)
        {

            try
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    var name = withoutVat ? "Sale per customer without vat" : "Sale per customer with vat";
                    ExcelWorksheet sheet = CreateSheet(package, name);

                    sheet.Column(1).Width = 40;
                    sheet.Column(2).Width = 15;
                    sheet.Column(3).Width = 15;
                    sheet.Column(4).Width = 15;
                    sheet.Column(5).Width = 15;

                    //Merging cells and create a center heading for out table

                    sheet.Cells[1, 1].Value = withoutVat ? "Digital Postage Sale pr. customer without vat" : "Digital Postage Sale pr. customer with vat"; // Heading Name
                    sheet.Cells[1, 1, 1, 1].Style.Font.Bold = true; //Font should be bold

                    sheet.Cells[3, 1].Value = "From date:";
                    sheet.Cells[3, 2].Value = fromDate.ToString("dd-MM-yyyy");

                    sheet.Cells[4, 1].Value = "To date:";
                    sheet.Cells[4, 2].Value = toDate.ToString("dd-MM-yyyy");

                    //sheet.Cells[5, 1].Value = "Order count:";

                    sheet.Cells[3, 1, 5, 1].Style.Font.Bold = true; //Font should be bold

                    int startrow = 7;
                    int rownum = startrow;

                    sheet.Cells[rownum, 1].Value = "Email";
                    sheet.Cells[rownum, 2].Value = "Amount";
                    sheet.Cells[rownum, 3].Value = "Order sum";
                    sheet.Cells[rownum, 4].Value = "Credit Sum";
                    sheet.Cells[rownum, 5].Value = "Saldo";
                    sheet.Cells[rownum, 1, rownum, 5].Style.Font.Bold = true; //Font should be bold




                    int amountSum = 0;
                    double orderSum = 0;
                    double creditSum = 0;
                    double saldoSum = 0;

                    foreach (DataRow row in salePerCustomerTable.Rows)
                    {
                        rownum++;
                        sheet.Cells[rownum, 1].Value = row["Email"];
                        sheet.Cells[rownum, 2].Value = (int)row["Amount"];
                        sheet.Cells[rownum, 3].Value = (double)row["OrderSum"];
                        sheet.Cells[rownum, 4].Value = (double)row["CreditSum"];
                        sheet.Cells[rownum, 5].Value = (double)row["Saldo"];

                        amountSum += (int)row["Amount"];
                        orderSum += (double)row["OrderSum"];
                        creditSum += (double)row["CreditSum"];
                        saldoSum += (double)row["Saldo"];
                    }

                    rownum++;

                    sheet.Cells[rownum, 1].Value = string.Format("Number of customers: {0}", rownum - startrow - 1);
                    sheet.Cells[rownum, 2].Value = amountSum;
                    sheet.Cells[rownum, 3].Value = orderSum;
                    sheet.Cells[rownum, 4].Value = creditSum;
                    sheet.Cells[rownum, 5].Value = saldoSum;

                    sheet.Cells[rownum, 1, rownum, 6].Style.Font.Bold = true; //Font should be bold
                    var fileprefix = withoutVat ? "DigitalPostageSalePerCustomeWithoutVat" : "DigitalPostageSalePerCustomeWithVat";
                    string filename = string.Format("{2}_{0}-{1}.xlsx", fromDate.ToString("yyyyMMdd"), toDate.ToString("yyyyMMdd"), fileprefix);

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", filename));
                    using (MemoryStream ms = new MemoryStream())
                    {
                        package.SaveAs(ms);
                        ms.WriteTo(HttpContext.Current.Response.OutputStream);
                        ms.Close();
                    }
                    try
                    {
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                    catch { }
                    return;
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ExportReportSalePerCustomer{1}, Message: {0}", ex.Message, withoutVat ? "withoutVat" : "WithVat");
            }
        }

        /// <summary>
        /// Export the report for refunded orders with vat.
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        private void ExportReportRefundedOrderWithVat(DateTime fromDate, DateTime toDate)
        {
            var row = Dal.Orders.GetReportRefundedOrderWithVat(fromDate, toDate);

            ExportReportRefunded(fromDate, toDate, false, row, "Refunds with vat", "RefundsWithVat");
        }
        /// <summary>
        /// Export the report for refunded orders with vat.
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        private void ExportReportRefundedOrderWithoutVat(DateTime fromDate, DateTime toDate)
        {
            var row = Dal.Orders.GetReportRefundedOrderWithoutVat(fromDate, toDate);
            ExportReportRefunded(fromDate, toDate, true, row, "Refunds without vat", "RefundsWithoutVat");
        }

        /// <summary>
        /// Export the report for reimbursements with vat.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportReimbursementsWithVat(DateTime fromDate, DateTime toDate)
        {
            var row = Dal.Orders.GetReportReimbursementsWithVat(fromDate, toDate);
            ExportReportRefunded(fromDate, toDate, false, row, "Reimbursements with vat", "ReimbursementsWithVat");
        }
        /// <summary>
        /// Export the report for reimbursements without vat.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportReimbursementsWithoutVat(DateTime fromDate, DateTime toDate)
        {
            var row = Dal.Orders.GetReportReimbursementsWithoutVat(fromDate, toDate);
            ExportReportRefunded(fromDate, toDate, true, row, "Reimbursements without vat", "ReimbursementsWithoutVat");
        }

        private void ExportReportRefunded(DateTime fromDate, DateTime toDate, bool withoutVat, SqlDataReader row, string name, string titleWithoutWitespace)
        {
            try
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    ExcelWorksheet sheet = CreateSheet(package, name);

                    int col = 1;
                    sheet.Column(col++).Width = 15;
                    sheet.Column(col++).Width = 10;
                    sheet.Column(col++).Width = 10;
                    sheet.Column(col++).Width = 10;
                    sheet.Column(col++).Width = 10;
                    sheet.Column(col++).Width = 15;
                    sheet.Column(col++).Width = 15;
                    sheet.Column(col++).Width = 15;
                    sheet.Column(col++).Width = 15;
                    sheet.Column(col++).Width = 15;
                    sheet.Column(col++).Width = 15;
                    sheet.Column(col++).Width = 15;

                    //Merging cells and create a center heading for out table
                    sheet.Cells[1, 1].Value = name; // Heading Name
                    sheet.Cells[1, 1, 1, 1].Style.Font.Bold = true; //Font should be bold

                    sheet.Cells[3, 1].Value = "From date:";
                    sheet.Cells[3, 2].Value = fromDate.ToString("dd-MM-yyyy");

                    sheet.Cells[4, 1].Value = "To date:";
                    sheet.Cells[4, 2].Value = toDate.ToString("dd-MM-yyyy");

                    sheet.Cells[3, 1, 5, 1].Style.Font.Bold = true; //Font should be bold

                    int startrow = 7;
                    int rownum = startrow;


                    col = 1;
                    sheet.Column(col).Width = 15;
                    sheet.Cells[rownum, col++].Value = string.Format("{0} date", name);

                    sheet.Column(col).Width = 10;
                    sheet.Cells[rownum, col++].Value = "Order nr";

                    sheet.Column(col).Width = 15;
                    sheet.Cells[rownum, col++].Value = "Sales date";

                    sheet.Column(col).Width = 15;
                    sheet.Cells[rownum, col++].Value = "Expire date";

                    sheet.Column(col).Width = 10;
                    sheet.Cells[rownum, col++].Value = "Order sum";

                    sheet.Column(col).Width = 10;
                    sheet.Cells[rownum, col++].Value = "Credit sum";

                    sheet.Column(col).Width = 10;
                    sheet.Cells[rownum, col++].Value = "Saldo";

                    sheet.Column(col).Width = 15;
                    sheet.Cells[rownum, col++].Value = "Channel";

                    sheet.Column(col).Width = 15;
                    sheet.Cells[rownum, col++].Value = "Employee";

                    sheet.Column(col).Width = 25;
                    sheet.Cells[rownum, col++].Value = "Email";

                    sheet.Column(col).Width = 15;
                    sheet.Cells[rownum, col++].Value = "Name";

                    sheet.Column(col).Width = 20;
                    sheet.Cells[rownum, col++].Value = "Address";

                    sheet.Column(col).Width = 8;
                    sheet.Cells[rownum, col++].Value = "Post nr";

                    sheet.Column(col).Width = 15;
                    sheet.Cells[rownum, col++].Value = "City";

                    sheet.Column(col).Width = 10;
                    sheet.Cells[rownum, col++].Value = "Phone";

                    sheet.Column(col).Width = 40;
                    sheet.Cells[rownum, col++].Value = "Message";

                    sheet.Cells[rownum, 1, rownum, col].Style.Font.Bold = true; //Font should be bold

                    double orderSum = 0;
                    double creditSum = 0;
                    double saldoSum = 0;

                    using (row)
                    {
                        while (row.Read())
                        {
                            col = 1;
                            rownum++;
                            sheet.Cells[rownum, col++].Value = ToDate(row["Timestamp"]);
                            sheet.Cells[rownum, col++].Value = (long)row["OrderNr"];
                            sheet.Cells[rownum, col++].Value = ToDate(row["SalesDate"]);
                            sheet.Cells[rownum, col++].Value = ToDate(row["ExpireDate"]);
                            sheet.Cells[rownum, col++].Value = (double)row["OrderSum"];
                            sheet.Cells[rownum, col++].Value = (double)row["CreditSum"];
                            sheet.Cells[rownum, col++].Value = (double)row["Saldo"];
                            sheet.Cells[rownum, col++].Value = row["Platform"];
                            sheet.Cells[rownum, col++].Value = TrimUserName(row["Employee"]);
                            sheet.Cells[rownum, col++].Value = row["Email"];
                            sheet.Cells[rownum, col++].Value = row["Name"];
                            sheet.Cells[rownum, col++].Value = row["Address"];
                            sheet.Cells[rownum, col++].Value = row["PostNumber"];
                            sheet.Cells[rownum, col++].Value = row["City"];
                            sheet.Cells[rownum, col++].Value = row["Phone"];
                            sheet.Cells[rownum, col++].Value = row["Message"];

                            orderSum += (double)row["OrderSum"];
                            creditSum += (double)row["CreditSum"];
                            saldoSum += (double)row["Saldo"];

                        }
                    }

                    rownum++;

                    sheet.Cells[rownum, 1].Value = string.Format("Number of {0}: {1}", name.ToLower(), rownum - startrow - 1);
                    sheet.Cells[rownum, 5].Value = orderSum;
                    sheet.Cells[rownum, 6].Value = creditSum;
                    sheet.Cells[rownum, 7].Value = saldoSum;

                    sheet.Cells[rownum, 1, rownum, 7].Style.Font.Bold = true; //Font should be bold

                    string filename = string.Format("DigitalPostage{2}_{0}-{1}.xlsx", fromDate.ToString("yyyyMMdd"), toDate.ToString("yyyyMMdd"), titleWithoutWitespace);

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", filename));
                    using (MemoryStream ms = new MemoryStream())
                    {
                        package.SaveAs(ms);
                        ms.WriteTo(HttpContext.Current.Response.OutputStream);
                        ms.Close();
                    }
                    try
                    {
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                    catch { }
                    return;
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ExportReport{1}, Message: {0}", ex.Message, titleWithoutWitespace);
            }
        }

        public string RemoveWhitespace(string input)
        {
            int j = 0, inputlen = input.Length;
            char[] newarr = new char[inputlen];

            for (int i = 0; i < inputlen; ++i)
            {
                char tmp = input[i];

                if (!char.IsWhiteSpace(tmp))
                {
                    newarr[j] = tmp;
                    ++j;
                }
            }

            return new String(newarr, 0, j);
        }

        protected void ExportReportChannelSalesWithoutVat(DateTime fromDate, DateTime toDate)
        {
            SqlDataReader row = Dal.Orders.GetReportReportChannelsWithoutVat(fromDate, toDate);
            ExportReportChannelSales(fromDate, toDate, row, true);
        }

        protected void ExportReportChannelSalesWithVat(DateTime fromDate, DateTime toDate)
        {

            SqlDataReader row = Dal.Orders.GetReportReportChannelsWithVat(fromDate, toDate);
            ExportReportChannelSales(fromDate, toDate, row, false);
        }

        /// <summary>
        /// Shows the report for reimbursements.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportChannelSales(DateTime fromDate, DateTime toDate, SqlDataReader row, bool withoutVat)
        {

            try
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    var name = withoutVat ? "ChannelSalesWithoutVat" : "ChannelSalesWithVat";
                    ExcelWorksheet sheet = CreateSheet(package, name);

                    var header = withoutVat ? "Sales channel and Payment method withour vat" : "Sales channel and Payment method with vat";
                    //Merging cells and create a center heading for out table
                    sheet.Cells[1, 1].Value = header; // Heading Name
                    sheet.Cells[1, 1, 1, 1].Style.Font.Bold = true; //Font should be bold

                    sheet.Cells[3, 1].Value = "From date:";
                    sheet.Cells[3, 2].Value = fromDate.ToString("dd-MM-yyyy");

                    sheet.Cells[4, 1].Value = "To date:";
                    sheet.Cells[4, 2].Value = toDate.ToString("dd-MM-yyyy");

                    sheet.Cells[3, 1, 5, 1].Style.Font.Bold = true; //Font should be bold

                    int startrow = 7;
                    int rownum = startrow;


                    int col = 1;
                    sheet.Column(col).Width = 30;
                    sheet.Cells[rownum, col++].Value = "Platform";

                    sheet.Column(col).Width = 20;
                    sheet.Cells[rownum, col++].Value = "Orders";

                    sheet.Column(col).Width = 20;
                    sheet.Cells[rownum, col++].Value = "Amount";

                    sheet.Column(col).Width = 40;
                    sheet.Cells[rownum, col++].Value = "Payment Method";

                    sheet.Column(col).Width = 20;
                    sheet.Cells[rownum, col++].Value = "Sale";

                    sheet.Column(col).Width = 20;
                    sheet.Cells[rownum, col++].Value = "Reimbursed";

                    sheet.Column(col).Width = 20;
                    sheet.Cells[rownum, col++].Value = "Total";

                    sheet.Cells[rownum, 1, rownum, col].Style.Font.Bold = true; //Font should be bold

                    double orderSum = 0;
                    double creditSum = 0;
                    double saldoSum = 0;
                    long totalAmount = 0;
                    int totalOrderCount = 0;

                    using (row)
                    {
                        while (row.Read())
                        {
                            col = 1;
                            rownum++;
                            sheet.Cells[rownum, col++].Value = row["Platform"];
                            sheet.Cells[rownum, col++].Value = (int)row["OrderCount"];
                            sheet.Cells[rownum, col++].Value = (int)row["Amount"];
                            sheet.Cells[rownum, col++].Value = GetPaymentMethod(row["PaymentMethod"]);
                            sheet.Cells[rownum, col++].Value = (double)row["OrderSum"];
                            sheet.Cells[rownum, col++].Value = (double)row["CreditSum"];
                            sheet.Cells[rownum, col++].Value = (double)row["Saldo"];

                            totalAmount += (int)row["Amount"];
                            totalOrderCount += (int)row["OrderCount"];
                            orderSum += (double)row["OrderSum"];
                            creditSum += (double)row["CreditSum"];
                            saldoSum += (double)row["Saldo"];

                        }
                    }

                    rownum++;

                    col = 1;
                    rownum++;
                    sheet.Cells[rownum, col++].Value = "Total";
                    sheet.Cells[rownum, col++].Value = totalOrderCount;
                    sheet.Cells[rownum, col++].Value = totalAmount;
                    sheet.Cells[rownum, col++].Value = "";
                    sheet.Cells[rownum, col++].Value = orderSum;
                    sheet.Cells[rownum, col++].Value = creditSum;
                    sheet.Cells[rownum, col++].Value = saldoSum;

                    sheet.Cells[rownum, 1, rownum, col].Style.Font.Bold = true; //Font should be bold

                    string filename = string.Format("DigitalPostageChannelSales{2}_{0}-{1}.xlsx", fromDate.ToString("yyyyMMdd"), toDate.ToString("yyyyMMdd"), withoutVat ? "WithoutVat" : "WithVat");

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", filename));
                    using (MemoryStream ms = new MemoryStream())
                    {
                        package.SaveAs(ms);
                        ms.WriteTo(HttpContext.Current.Response.OutputStream);
                        ms.Close();
                    }
                    try
                    {
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                    catch { }
                    return;
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: ExportReportChannelSales{1}, Message: {0}", ex.Message, withoutVat ? "WithoutVat" : "WithVat");
            }
        }


        /// <summary>
        /// Shows the report for reimbursements.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        protected void ExportReportCodeValidationSum(DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    ExcelWorksheet summarySheet = package.Workbook.Worksheets.Add("Validation Summary");


                    summarySheet.Cells[1, 1].Value = "Validation Summary"; // Heading Name
                    summarySheet.Cells[1, 1, 1, 1].Style.Font.Bold = true; //Font should be bold

                    summarySheet.Cells[3, 1].Value = "From date:";
                    summarySheet.Cells[3, 2].Value = fromDate.ToString("dd-MM-yyyy");

                    summarySheet.Cells[4, 1].Value = "To date:";
                    summarySheet.Cells[4, 2].Value = toDate.ToString("dd-MM-yyyy");

                    summarySheet.Cells[3, 1, 5, 1].Style.Font.Bold = true; //Font should be bold

                    int row = 7;
                    int col = 0;

                    foreach (TableRow tr in tblCodeValidation.Rows)
                    {
                        col = 0;
                        row++;
                        foreach (TableCell td in tr.Cells)
                        {
                            col++;
                            if (row == 8)
                            {
                                summarySheet.Cells[row, col].Value = td.Text;
                            }
                            else
                            {
                                switch (col)
                                {
                                    case 1:
                                        summarySheet.Cells[row, col].Value = td.Text;
                                        break;
                                    case 2:
                                        summarySheet.Cells[row, col].Value = int.Parse(td.Text);
                                        break;
                                    case 3:
                                        double percent = float.Parse(td.Text.Replace("%", "").Replace(",", "."));
                                        summarySheet.Cells[row, col].Style.Numberformat.Format = "0.00%";
                                        summarySheet.Cells[row, col].Value = percent / 100;
                                        break;
                                }
                            }
                        }
                    }

                    col = 1;
                    summarySheet.Column(col++).Width = 20;
                    summarySheet.Column(col++).Width = 20;
                    summarySheet.Column(col++).Width = 20;
                    summarySheet.Column(col++).Width = 20;

                    // Set bold text
                    summarySheet.Cells[8, 1, 8, 3].Style.Font.Bold = true;
                    summarySheet.Cells[8, 1, row, 1].Style.Font.Bold = true;
                    summarySheet.Cells[row, 1, row, 3].Style.Font.Bold = true;

                    ExcelWorksheet sheet = package.Workbook.Worksheets.Add("Validation details");


                    sheet.Cells[1, 1].Value = "Validation and order details"; // Heading Name
                    sheet.Cells[1, 1, 1, 1].Style.Font.Bold = true; //Font should be bold

                    sheet.Cells[3, 1].Value = "From date:";
                    sheet.Cells[3, 2].Value = fromDate.ToString("dd-MM-yyyy");

                    sheet.Cells[4, 1].Value = "To date:";
                    sheet.Cells[4, 2].Value = toDate.ToString("dd-MM-yyyy");

                    sheet.Cells[3, 1, 5, 1].Style.Font.Bold = true; //Font should be bold

                    col = 0;
                    int rownum = 7;
                    sheet.Column(++col).Width = 20;
                    sheet.Cells[rownum, col].Value = "Code"; // Heading Name

                    sheet.Column(++col).Width = 20;
                    sheet.Cells[rownum, col].Value = "Date"; // Heading Name

                    sheet.Column(++col).Width = 20;
                    sheet.Cells[rownum, col].Value = "Status"; // Heading Name

                    sheet.Column(++col).Width = 20;
                    sheet.Cells[rownum, col].Value = "Employee"; // Heading Name

                    sheet.Column(++col).Width = 20;
                    sheet.Cells[rownum, col].Value = "Order Id"; // Heading Name

                    sheet.Column(++col).Width = 20;
                    sheet.Cells[rownum, col].Value = "Price"; // Heading Name

                    sheet.Column(++col).Width = 30;
                    sheet.Cells[rownum, col].Value = "E-mail"; // Heading Name

                    sheet.Column(++col).Width = 30;
                    sheet.Cells[rownum, col].Value = "Product"; // Heading Name

                    sheet.Cells[rownum, 1, rownum, 8].Style.Font.Bold = true; //Font should be bold


                    using (SqlDataReader res = Dal.ValidationResults.GetReportCodeValidationAndOrder(fromDate, toDate))
                    {
                        while (res.Read())
                        {
                            rownum++;
                            sheet.Cells[rownum, 1].Value = Bll.Codes.FormatCode(ToString(res["CodeText"]));
                            sheet.Cells[rownum, 2].Value = ToDateTime(res["Timestamp"]);
                            sheet.Cells[rownum, 3].Value = ToString(res["Status"]);
                            sheet.Cells[rownum, 4].Value = TrimUserName(res["Employee"]);

                            if (res["OrderId"] != null)
                            {
                                long orderId;
                                long.TryParse(res["OrderId"].ToString(), out orderId);
                                if (orderId > 0) sheet.Cells[rownum, 5].Value = orderId;
                            }
                            if (res["Price"] != null && !string.IsNullOrEmpty(res["Price"].ToString()))
                            {
                                sheet.Cells[rownum, 6].Value = res["Price"].ToString();
                            }
                            if (res["Email"] != null && !string.IsNullOrEmpty(res["Email"].ToString()))
                            {
                                sheet.Cells[rownum, 7].Value = res["Email"].ToString();
                            }
                            //Product
                            var productValue = string.Format("{0} {1}", res["Destination"], res["Weight"]);
                            sheet.Cells[rownum, 8].Value = productValue;
                        }
                    }

                    string filename = string.Format("DigitalPostageCodeValidation_{0}-{1}.xlsx", fromDate.ToString("yyyyMMdd"), toDate.ToString("yyyyMMdd"));

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", filename));
                    using (MemoryStream ms = new MemoryStream())
                    {
                        package.SaveAs(ms);
                        ms.WriteTo(HttpContext.Current.Response.OutputStream);
                        ms.Close();
                    }
                    try
                    {
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                    catch { }
                    return;
                }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error generating report: CodeValidation, Message: {0}", ex.Message);
            }
        }


        #endregion

        #region Item Handlers

        /// <summary>
        /// Handles the ItemDataBound event of the rptSalePerProduct control. Sums are generated by adding items.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RepeaterItemEventArgs"/> instance containing the event data.</param>
        void rptSalePerProduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                this.AmountSum = 0;
                this.OrderSum = 0;
                this.SaldoSum = 0;
                this.ReimbursedCount = 0;
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                this.AmountSum += (int)((DataRowView)e.Item.DataItem)["SubAmount"];
                this.OrderSum += (double)((DataRowView)e.Item.DataItem)["SubSum"];
                this.ReimbursedCount += (int)((DataRowView)e.Item.DataItem)["Reimbursed"];
                this.SaldoSum += (double)((DataRowView)e.Item.DataItem)["Total"];
                this.OrderCount++;
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblAmountSum = e.Item.FindControl("lblAmountSum") as Label;
                lblAmountSum.Text = this.AmountSum.ToString();

                Label lblOrderSum = e.Item.FindControl("lblOrderSum") as Label;
                lblOrderSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.OrderSum);

                Label lblReimbursedSum = e.Item.FindControl("lblReimbursedSum") as Label;
                lblReimbursedSum.Text = String.Format("{0}", this.ReimbursedCount);

                Label lblGrandTotal = e.Item.FindControl("lblGrandTotal") as Label;
                lblGrandTotal.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.SaldoSum);
            }
        }

        /// <summary>
        /// Handles the ItemDataBound event of the rptSalePerCustomer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RepeaterItemEventArgs"/> instance containing the event data.</param>
        void rptSalePerCustomer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                this.OrderCount = 0;
                this.AmountSum = 0;
                this.OrderSum = 0;
                this.CreditSum = 0;
                this.SaldoSum = 0;
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                this.AmountSum += (int)((DataRowView)e.Item.DataItem)["Amount"];
                this.OrderSum += (double)((DataRowView)e.Item.DataItem)["OrderSum"];
                this.CreditSum += (double)((DataRowView)e.Item.DataItem)["CreditSum"];
                this.SaldoSum += (double)((DataRowView)e.Item.DataItem)["Saldo"];
                this.OrderCount++;
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblPcCustomerCount = e.Item.FindControl("lblPcCustomerCount") as Label;
                lblPcCustomerCount.Text = string.Format("Number of customers: {0}", this.OrderCount);

                Label lblAmountSum = e.Item.FindControl("lblPcAmountSum") as Label;
                lblAmountSum.Text = this.AmountSum.ToString();

                Label lblOrderSum = e.Item.FindControl("lblPcOrderSum") as Label;
                lblOrderSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.OrderSum);

                Label lblCreditSum = e.Item.FindControl("lblPcCreditSum") as Label;
                lblCreditSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.CreditSum);

                Label lblSaldoSum = e.Item.FindControl("lblPcSaldoSum") as Label;
                lblSaldoSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.SaldoSum);
            }
        }

        /// <summary>
        /// Handles the ItemDataBound event of the rptReimbursements control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RepeaterItemEventArgs"/> instance containing the event data.</param>
        void rptReimbursements_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                this.OrderCount = 0;
                this.AmountSum = 0;
                this.OrderSum = 0;
                this.CreditSum = 0;
                this.SaldoSum = 0;
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                this.OrderSum += (double)((DbDataRecord)e.Item.DataItem)["OrderSum"];
                this.CreditSum += (double)((DbDataRecord)e.Item.DataItem)["CreditSum"];
                this.SaldoSum += (double)((DbDataRecord)e.Item.DataItem)["Saldo"];
                this.OrderCount++;
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblReimbursementCount = e.Item.FindControl("lblReimbursementCount") as Label;
                lblReimbursementCount.Text = string.Format("Number of reimbursements: {0}", this.OrderCount);

                Label lblReimbursementOrderSum = e.Item.FindControl("lblReimbursementOrderSum") as Label;
                lblReimbursementOrderSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.OrderSum);

                Label lblReimbursementCreditSum = e.Item.FindControl("lblReimbursementCreditSum") as Label;
                lblReimbursementCreditSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.CreditSum);

                Label lblReimbursementSaldoSum = e.Item.FindControl("lblReimbursementSaldoSum") as Label;
                lblReimbursementSaldoSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.SaldoSum);
            }
        }
        /// <summary>
        /// Handles the ItemDataBound event of the rptRefundOrderWithVat control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptRefundOrderWithVat_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                this.OrderCount = 0;
                this.AmountSum = 0;
                this.OrderSum = 0;
                this.CreditSum = 0;
                this.SaldoSum = 0;
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                this.OrderSum += (double)((DbDataRecord)e.Item.DataItem)["OrderSum"];
                this.CreditSum += (double)((DbDataRecord)e.Item.DataItem)["CreditSum"];
                this.SaldoSum += (double)((DbDataRecord)e.Item.DataItem)["Saldo"];
                this.OrderCount++;
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblReimbursementCount = e.Item.FindControl("lblRefundOrderCount") as Label;
                lblReimbursementCount.Text = string.Format("Number of refunds: {0}", this.OrderCount);

                Label lblReimbursementOrderSum = e.Item.FindControl("lblRefundOrderSum") as Label;
                lblReimbursementOrderSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.OrderSum);

                Label lblReimbursementCreditSum = e.Item.FindControl("lblRefundCreditSum") as Label;
                lblReimbursementCreditSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.CreditSum);

                Label lblReimbursementSaldoSum = e.Item.FindControl("lblRefundSaldoSum") as Label;
                lblReimbursementSaldoSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.SaldoSum);
            }
        }

        /// <summary>
        /// Handles the ItemDataBound event of the rptReimbursements control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RepeaterItemEventArgs"/> instance containing the event data.</param>
        void rptChannelSales_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                this.OrderCount = 0;
                this.AmountSum = 0;
                this.OrderSum = 0;
                this.CreditSum = 0;
                this.TotalOrderCount = 0;
                this.SaldoSum = 0;
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                this.TotalOrderCount += (int)((DbDataRecord)e.Item.DataItem)["OrderCount"];
                this.AmountSum += (int)((DbDataRecord)e.Item.DataItem)["Amount"];
                this.OrderSum += (double)((DbDataRecord)e.Item.DataItem)["OrderSum"];
                this.CreditSum += (double)((DbDataRecord)e.Item.DataItem)["CreditSum"];
                this.SaldoSum += (double)((DbDataRecord)e.Item.DataItem)["Saldo"];
                this.OrderCount++;
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblChannelSalesOrderCount = e.Item.FindControl("lblChannelSalesOrderCount") as Label;
                lblChannelSalesOrderCount.Text = string.Format("{0}", this.TotalOrderCount);

                Label lblChannelSalesCount = e.Item.FindControl("lblChannelSalesCount") as Label;
                lblChannelSalesCount.Text = string.Format("{0}", this.AmountSum);

                Label lblChannelSalesOrderSum = e.Item.FindControl("lblChannelSalesOrderSum") as Label;
                lblChannelSalesOrderSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.OrderSum);

                Label lblChannelSalesCreditSum = e.Item.FindControl("lblChannelSalesCreditSum") as Label;
                lblChannelSalesCreditSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.CreditSum);

                Label lblChannelSalesSaldoSum = e.Item.FindControl("lblChannelSalesSaldoSum") as Label;
                lblChannelSalesSaldoSum.Text = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", this.SaldoSum);

            }
        }


        #endregion


        /// <summary>
        /// Trims the name of the user.
        /// </summary>
        /// <param name="longName">The long name.</param>
        /// <returns></returns>
        static public string TrimUserName(object longName)
        {
            if (longName == null)
            {
                return string.Empty;
            }
            if (longName.ToString().Contains(@"\"))
            {
                return longName.ToString().Split('\\')[1];
            }
            return longName.ToString();
        }

        public string ProcessOrderId(object orderId)
        {
            long orderIdVal;
            if (orderId != null && long.TryParse(orderId.ToString(), out orderIdVal) && orderIdVal > 0)
            {
                return orderId.ToString();
            }
            else
            {
                return "&nbsp;";
            }
        }

        public string ProcessPrice(object price)
        {
            if (price != null && !string.IsNullOrEmpty(price.ToString()))
            {
                var prVal = String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", price);
                return prVal;
            }
            else
            {
                return "&nbsp;";
            }
        }
        public string CreateProductTitel(object destination, object weight)
        {
            var productValue = string.Format("{0} {1}", destination, weight);
            return productValue;

        }

    }
}
