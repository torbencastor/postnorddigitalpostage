﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Reporting.ascx.cs" Inherits="PostNord.Portal.DigitalPostage.WebParts.Reporting.Reporting" %>


<style type="text/css">
    .table {
        width: 660px;
        margin-top: 20px;
        border: 1px solid #d3d3d3;
    }

    .wide {
        width: 800px;
    }

    .table th, .table td {
        padding-top: 5px;
        padding-bottom: 5px;
        padding-right: 3px;
        vertical-align: top;
    }

    .header {
        background-color: #e3e3e3;
    }

    .odd {
        background-color: #f3f3f3;
    }

    td .left, th .left {
        text-align: left;
    }

    td .right, th .right {
        text-align: right;
    }

    #intervalValue, #intervalSale {
        width: 400px;
    }

    .narrowTable {
        width: 427px;
    }

    #reimbursements {
        width: 100%;
    }

    #channelsales {
        width: 600px;
    }

    #RefundedOrderWithVat, #RefundedOrdrerWithOutVat {
        width: 100%;
    }
</style>

<h1>Digital Postage Reporting</h1>

<asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>

<asp:Panel ID="pnlSearchForm" runat="server">
    <table id="advancedForm">
        <tr>
            <th class="left">From date:</th>
            <td>
                <SharePoint:DateTimeControl ID="dtcFromDate" runat="server" Calendar="Gregorian" DateOnly="true" LocaleId="1030" CssClassTextBox="dateBox" />
            </td>
        </tr>
        <tr>
            <th class="left">To date:</th>
            <td>
                <SharePoint:DateTimeControl ID="dtcToDate" runat="server" Calendar="Gregorian" DateOnly="true" LocaleId="1030" CssClassTextBox="dateBox" />
            </td>
        </tr>
        <tr>
            <th class="left">Report:</th>
            <td>
                <asp:DropDownList ID="drpReportType" runat="server" Width="330">
                    <asp:ListItem Value="SalePerProduct">Sale per product with vat</asp:ListItem>
                    <asp:ListItem Value="SalePerProductWithoutVat">Sale per product without vat</asp:ListItem>
                    <asp:ListItem Value="CustomerGroupsMobilPorto">Customer groups - App with vat</asp:ListItem>
                    <asp:ListItem Value="CustomerGroupsMobilPortoWithoutVat">Customer groups - App without vat</asp:ListItem>
                    <asp:ListItem Value="CustomerGroupsOnlinePorto">Customer groups - WebShop with vat</asp:ListItem>
                    <asp:ListItem Value="CustomerGroupsOnlinePortoWithoutVat">Customer groups - WebShop without vat</asp:ListItem>
                    <asp:ListItem Value="CustomerGroups">Customer groups - Total with vat</asp:ListItem>
                    <asp:ListItem Value="CustomerGroupsWithoutVat">Customer groups - Total without vat</asp:ListItem>
                    <asp:ListItem Value="SalePerCustomer">Sale per customer with vat</asp:ListItem>
                    <asp:ListItem Value="SalePerCustomerWithoutVat">Sale per customer without vat</asp:ListItem>
                    <asp:ListItem Value="SalesChannels">Sales channel and Payment method with vat</asp:ListItem>
                    <asp:ListItem Value="SalesChannelsWithoutVat">Sales channel and Payment method without vat</asp:ListItem>
                    <asp:ListItem Value="Reimbursements">Reimbursements with vat</asp:ListItem>
                    <asp:ListItem Value="ReimbursementsWithoutVat">Reimbursements without vat</asp:ListItem>
                    <asp:ListItem Value="CodeValidation">Code validation</asp:ListItem>
                    <asp:ListItem Value="RefundedOrderWithVat">Refunded with vat</asp:ListItem>
                    <asp:ListItem Value="RefundedOrderWithoutVat">Refunded without vat</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnShowReport" runat="server" Text="Show" OnClick="btnShowReport_Click" />
                <asp:Button ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlSalePerProduct" runat="server" Visible="false">
    <h2>
        <asp:Label ID="lblSalePerProduct" runat="server" Text="Sale Per product" Visible="false"></asp:Label>
        <asp:Label ID="lblSalePerProductWithoutVat" runat="server" Text="Sale Per product without vat" Visible="false"></asp:Label>
    </h2>


    <asp:Repeater ID="rptSalePerProduct" runat="server" OnItemDataBound="rptSalePerProduct_ItemDataBound">
        <HeaderTemplate>
            <table id="tblSearchResult" class="table">
                <tr class="header">
                    <th class="left">Product
                    </th>
                    <th class="right">Price
                    </th>
                    <th class="right">Amount
                    </th>
                    <th class="right">Sale
                    </th>
                    <th class="right">Reimbursed
                    </th>
                    <th class="right">Total
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="left">
                    <%#  Eval("Product") %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Price"))  %>
                </td>
                <td class="right">
                    <%#  Eval("SubAmount") %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "SubSum"))  %>
                </td>
                <td class="right">
                    <%#  Eval("Reimbursed") %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Total"))  %>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="odd">
                <td class="left">
                    <%#  Eval("Product") %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Price"))  %>
                </td>
                <td class="right">
                    <%#  Eval("SubAmount") %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "SubSum"))  %>
                </td>
                <td class="right">
                    <%#  Eval("Reimbursed") %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Total"))  %>
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            <tr class="header">
                <th class="left" colspan="2">Total:
                </th>
                <th class="right">
                    <asp:Label ID="lblAmountSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblOrderSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblReimbursedSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblGrandTotal" runat="server" Text=""></asp:Label>
                </th>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pnlSalePerCustomer" runat="server" Visible="false">
    <h2>
        <asp:Label ID="lblSalePerCustomerWithVat" runat="server" Text="Sale Per customer with vat"></asp:Label>
        <asp:Label ID="lblSalePerCustomerWithoutVat" runat="server" Text="Sale Per customer without vat"></asp:Label>
    </h2>
    <asp:Repeater ID="rptSalePerCustomer" runat="server" OnItemDataBound="rptSalePerCustomer_ItemDataBound">
        <HeaderTemplate>
            <table id="tblSearchResult" class="table">
                <tr class="header">
                    <th class="left">Email
                    </th>
                    <th class="right">Amount
                    </th>
                    <th class="right">Order&nbsp;sum
                    </th>
                    <th class="right">Credit&nbsp;sum
                    </th>
                    <th class="right">Saldo
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="left">
                    <%#  Eval("Email") %>
                </td>
                <td class="right">
                    <%#  Eval("Amount") %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "OrderSum"))  %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "CreditSum"))  %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Saldo"))  %>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="odd">
                <td class="left">
                    <%#  Eval("Email") %>
                </td>
                <td class="right">
                    <%#  Eval("Amount") %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "OrderSum"))  %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "CreditSum"))  %>
                </td>
                <td class="right">
                    <%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem, "Saldo"))  %>
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            <tr class="header">
                <th class="left">
                    <asp:Label ID="lblPcCustomerCount" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblPcAmountSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblPcOrdersum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblPcCreditSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblPcSaldoSum" runat="server" Text=""></asp:Label>
                </th>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>

</asp:Panel>
<asp:Panel ID="pnlCustomerGroups" runat="server" Visible="false">
    <h2>Number of codes sold per customer group
        <asp:Label ID="lblCustomerGroupType" runat="server"></asp:Label></h2>

    <table class="table">
        <tr class="header">
            <th class="left">Number of codes
            </th>
            <th class="right">Number of customers
            </th>
            <th>&nbsp;&nbsp;&nbsp;</th>
            <th class="left">Sale DKK
            </th>
            <th class="right">Number of customers
            </th>
        </tr>
        <tr>
            <th class="left">1</th>
            <td class="right">
                <asp:Label ID="lblIntAmount1" runat="server" Text=""></asp:Label></td>
            <th></th>
            <th class="left">0 - 50</th>
            <td class="right">
                <asp:Label ID="lblSale0" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr class="odd">
            <th class="left">2 - 5</th>
            <td class="right">
                <asp:Label ID="lblIntAmount2_5" runat="server" Text=""></asp:Label></td>
            <td></td>
            <th class="left">51 - 100</th>
            <td class="right">
                <asp:Label ID="lblSale51" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <th class="left">6 - 10</th>
            <td class="right">
                <asp:Label ID="lblIntAmount6_10" runat="server" Text=""></asp:Label></td>
            <td></td>
            <th class="left">101 - 500</th>
            <td class="right">
                <asp:Label ID="lblSale101" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr class="odd">
            <th class="left">11 - 100</th>
            <td class="right">
                <asp:Label ID="lblIntAmount11_100" runat="server" Text=""></asp:Label></td>
            <td></td>
            <th class="left">500 - 1000</th>
            <td class="right">
                <asp:Label ID="lblSale501" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <th class="left">&gt; 100</th>
            <td class="right">
                <asp:Label ID="lblIntAmount101" runat="server" Text=""></asp:Label></td>
            <td></td>
            <th class="left">&gt; 1000</th>
            <td class="right">
                <asp:Label ID="lblSale1001" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <th class="left">Sum
            </th>
            <th class="right">
                <asp:Label ID="lblIntAmountSum" runat="server" Text=""></asp:Label>
            </th>
            <th></th>
            <th class="left"></th>
            <th class="right">
                <asp:Label ID="lblSaleSum" runat="server" Text=""></asp:Label>
            </th>
        </tr>
    </table>

</asp:Panel>
<asp:Panel ID="pnlReimbursements" runat="server" Visible="false">
    <h2>
        <asp:Label ID="lblReimbursements" runat="server" Text="Reimbursements"></asp:Label>
        <asp:Label ID="lblReimbursementsWithoutVat" runat="server" Text="Reimbursements without vat"></asp:Label>
    </h2>

    <asp:Repeater ID="rptReimbursements" runat="server" OnItemDataBound="rptReimbursements_ItemDataBound">
        <HeaderTemplate>
            <table class="table" id="reimbursements">
                <tr class="header">
                    <th class="left">Reimbursement<br />
                        date</th>
                    <th class="right">Order&nbsp;nr.</th>
                    <th class="left">Sales&nbsp;date</th>
                    <th class="right">Expire&nbsp;date</th>
                    <th class="right">Order&nbsp;sum</th>
                    <th class="right">Credit&nbsp;Sum</th>
                    <th class="right">Saldo</th>
                    <th class="right">Channel</th>
                    <th class="left">Employee</th>
                    <th class="left">Email</th>
                    <th class="left">Customer info</th>
                    <th class="left">Phone</th>
                    <th class="left">Message</th>
                </tr>
        </HeaderTemplate>

        <ItemTemplate>
            <tr>
                <td><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"Timestamp")) %></td>
                <td class="right"><%#  Eval("OrderNr") %></td>
                <td><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"SalesDate")) %></td>
                <td class="right"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"ExpireDate")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"OrderSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"CreditSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"Saldo")) %></td>
                <td><%#  Eval("Platform") %></td>
                <td><%#  TrimUserName(Eval("Employee")) %></td>
                <td><%#  Eval("Email") %></td>
                <td><%#  Eval("Name") %>
                    <br />
                    <%#  Eval("Address") %><br />
                    <%#  Eval("PostNumber") %> <%#  Eval("City") %></td>
                <td><%#  Eval("Phone") %></td>
                <td><%#  Eval("Message") %></td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="odd">
                <td class="left"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"Timestamp")) %></td>
                <td class="right"><%#  Eval("OrderNr") %></td>
                <td class="left"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"SalesDate")) %></td>
                <td class="right"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"ExpireDate")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"OrderSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"CreditSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"Saldo")) %></td>
                <td><%#  Eval("Platform") %></td>
                <td><%#  TrimUserName(Eval("Employee")) %></td>
                <td><%#  Eval("Email") %></td>
                <td><%#  Eval("Name") %>
                    <br />
                    <%#  Eval("Address") %><br />
                    <%#  Eval("PostNumber") %> <%#  Eval("City") %></td>
                <td><%#  Eval("Phone") %></td>
                <td><%#  Eval("Message") %></td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            <tr class="header">
                <th class="left" colspan="4">
                    <asp:Label ID="lblReimbursementCount" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblReimbursementOrderSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblReimbursementCreditSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblReimbursementSaldoSum" runat="server" Text=""></asp:Label>
                </th>
                <th colspan="6">&nbsp;
                </th>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>

</asp:Panel>

<asp:Panel ID="pnlRefundOrderWithVat" runat="server" Visible="false">
    <h2>
        <asp:Label ID="lblReportRefundWithVat" runat="server" Text="Refund with vat" Visible="false"></asp:Label>
        <asp:Label ID="lblReportRefundWithoutVat" runat="server" Text="Refund without vat" Visible="false"></asp:Label>
    </h2>

    <asp:Repeater ID="rptRefundOrderWithVat" runat="server" OnItemDataBound="rptRefundOrderWithVat_ItemDataBound">
        <HeaderTemplate>
            <table class="table" id="reimbursements">
                <tr class="header">
                    <th class="left">Refund order<br />
                        date</th>
                    <th class="right">Order&nbsp;nr.</th>
                    <th class="left">Sales&nbsp;date</th>
                    <th class="right">Expire&nbsp;date</th>
                    <th class="right">Order&nbsp;sum</th>
                    <th class="right">Credit&nbsp;Sum</th>
                    <th class="right">Saldo</th>
                    <th class="right">Channel</th>
                    <th class="left">Employee</th>
                    <th class="left">Email</th>
                    <th class="left">Customer info</th>
                    <th class="left">Phone</th>
                    <th class="left">Message</th>
                </tr>
        </HeaderTemplate>

        <ItemTemplate>
            <tr>
                <td><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"Timestamp")) %></td>
                <td class="right"><%#  Eval("OrderNr") %></td>
                <td><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"SalesDate")) %></td>
                <td class="right"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"ExpireDate")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"OrderSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"CreditSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"Saldo")) %></td>
                <td><%#  Eval("Platform") %></td>
                <td><%#  TrimUserName(Eval("Employee")) %></td>
                <td><%#  Eval("Email") %></td>
                <td><%#  Eval("Name") %>
                    <br />
                    <%#  Eval("Address") %><br />
                    <%#  Eval("PostNumber") %> <%#  Eval("City") %></td>
                <td><%#  Eval("Phone") %></td>
                <td><%#  Eval("Message") %></td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="odd">
                <td class="left"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"Timestamp")) %></td>
                <td class="right"><%#  Eval("OrderNr") %></td>
                <td class="left"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"SalesDate")) %></td>
                <td class="right"><%# String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"ExpireDate")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"OrderSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"CreditSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"Saldo")) %></td>
                <td><%#  Eval("Platform") %></td>
                <td><%#  TrimUserName(Eval("Employee")) %></td>
                <td><%#  Eval("Email") %></td>
                <td><%#  Eval("Name") %>
                    <br />
                    <%#  Eval("Address") %><br />
                    <%#  Eval("PostNumber") %> <%#  Eval("City") %></td>
                <td><%#  Eval("Phone") %></td>
                <td><%#  Eval("Message") %></td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            <tr class="header">
                <th class="left" colspan="4">
                    <asp:Label ID="lblRefundOrderCount" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblRefundOrderSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblRefundCreditSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblRefundSaldoSum" runat="server" Text=""></asp:Label>
                </th>
                <th colspan="6">&nbsp;
                </th>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>

</asp:Panel>

<asp:Panel ID="pnlChannelSales" runat="server" Visible="false">
    <h2>
        <asp:Label ID="lblChannelSalesWithVat" runat="server" Text="Sales channel and Payment method with vat"></asp:Label>
        <asp:Label ID="lblChannelSalesWithoutVat" runat="server" Text="Sales channel and Payment method widthout vat"></asp:Label>
    </h2>

    <asp:Repeater ID="rptChannelSales" runat="server" OnItemDataBound="rptChannelSales_ItemDataBound">
        <HeaderTemplate>
            <table class="table" id="channelsales">
                <tr class="header">
                    <th class="left">Platform</th>
                    <th class="right">Orders</th>
                    <th class="right">Amount</th>
                    <th class="left">Payment method</th>
                    <th class="right">Sale</th>
                    <th class="right">Reimbursed</th>
                    <th class="right">Saldo</th>
                </tr>
        </HeaderTemplate>

        <ItemTemplate>
            <tr>
                <td><%#  Eval("Platform") %></td>
                <td class="right"><%#  DataBinder.Eval(Container.DataItem,"OrderCount") %></td>
                <td class="right"><%#  DataBinder.Eval(Container.DataItem,"Amount") %></td>
                <td class="left"><%#  GetPaymentMethod(DataBinder.Eval(Container.DataItem,"PaymentMethod")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"OrderSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"CreditSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"Saldo")) %></td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="odd">
                <td><%#  Eval("Platform") %></td>
                <td class="right"><%#  DataBinder.Eval(Container.DataItem,"OrderCount") %></td>
                <td class="right"><%#  DataBinder.Eval(Container.DataItem,"Amount") %></td>
                <td class="left"><%#  GetPaymentMethod(DataBinder.Eval(Container.DataItem,"PaymentMethod")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"OrderSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"CreditSum")) %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:C}", DataBinder.Eval(Container.DataItem,"Saldo")) %></td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            <tr class="header">
                <th class="left">Total</th>
                <th class="right">
                    <asp:Label ID="lblChannelSalesOrderCount" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblChannelSalesCount" runat="server" Text=""></asp:Label>
                </th>
                <th class="right"></th>
                <th class="right">
                    <asp:Label ID="lblChannelSalesOrderSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblChannelSalesCreditSum" runat="server" Text=""></asp:Label>
                </th>
                <th class="right">
                    <asp:Label ID="lblChannelSalesSaldoSum" runat="server" Text=""></asp:Label>
                </th>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>

</asp:Panel>

<asp:Panel ID="pnlCodeValidation" runat="server">
    <h2>Code validations</h2>
    <asp:Table ID="tblCodeValidation" runat="server" CssClass="table narrowTable">
        <asp:TableHeaderRow CssClass="header">
            <asp:TableHeaderCell CssClass="left">Status</asp:TableHeaderCell>
            <asp:TableHeaderCell CssClass="right">Count</asp:TableHeaderCell>
            <asp:TableHeaderCell CssClass="right">Percentage</asp:TableHeaderCell>
        </asp:TableHeaderRow>
    </asp:Table>
</asp:Panel>

<asp:Panel ID="pnlReportCodeValidationAndOrder" runat="server" Visible="false">
    <h2>
        <asp:Label ID="lblReportCodeValidationAndOrder" runat="server" Text="Code validation and order details"></asp:Label>
    </h2>
    <asp:Repeater ID="rptReportCodeValidationAndOrder" runat="server">
        <HeaderTemplate>
            <table class="table wide" id="tableReportCodeValidationAndOrder">
                <tr class="header">
                    <th class="left">Code</th>
                    <th class="left">Status</th>
                    <th class="right">Date&nbsp;of&nbsp;validation</th>
                    <th class="right">Order&nbsp;nr.</th>
                    <th class="right">Price</th>
                    <th class="left">E-mail</th>
                    <th class="left">Product</th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%#  Eval("CodeText") %></td>
                <td><%#  Eval("Status") %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"Timestamp")) %></td>
                <td class="right"><%# ProcessOrderId(Eval("OrderId")) %></td>
                <td class="right"><%# ProcessPrice(Eval("Price")) %></td>
                <td><%# Eval("Email") %></td>
                <td><%# CreateProductTitel(Eval("Destination"),Eval("Weight")) %></td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="odd">
                <td><%#  Eval("CodeText") %></td>
                <td><%#  Eval("Status") %></td>
                <td class="right"><%#  String.Format(new System.Globalization.CultureInfo("da-DK"), "{0:d}", DataBinder.Eval(Container.DataItem,"Timestamp")) %></td>
                <td class="right"><%# ProcessOrderId(Eval("OrderId")) %></td>
                <td class="right"><%# ProcessPrice(Eval("Price")) %></td>
                <td><%# Eval("Email") %></td>
                <td><%# CreateProductTitel(Eval("Destination"),Eval("Weight")) %></td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>

    </asp:Repeater>

</asp:Panel>

