﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="pnc" Namespace="PostNord.Portal.DigitalPostage.WebControls" Assembly="$SharePoint.Project.AssemblyFullName$" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReceiptWP.ascx.cs" Inherits="PostNord.Portal.DigitalPostage.WebParts.ReceiptWP.ReceiptWP" %>


<script type="text/javascript">

    function printIt(method) {
        try {
            var url = document.location.href + '&' + method + '=1';
            $("#linkFrame").attr('src', url);
        }
        catch (e) {
            alert('error: ' + e.message);
        }
        return false;   
    }

    function restart() {
        //Redirect on the parent, so that it will open in the browser and nort in the iFrame
        window.parent.location.href = '<pnc:GetText runat="server" Key="ReceiptUrl" Default="/" UseConfigSetting="true" />';
    }

</script>

<!-- Google Code for Online Brevporto Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1066589007;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "kb1kCM3VuQkQz7bL_AM";
    var google_remarketing_only = false;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1066589007/?label=kb1kCM3VuQkQz7bL_AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<div class="body">
    <div class="nav">
        <ol>
            <li><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_1" Default="" /></span></li>
            <li><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_2" Default="" /></span></li>
            <li><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_3" Default="" /></span></li>
            <li><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_4" Default="" /></span></li>
            <li class="active last"><span><pnc:GetText runat="server" Key="BUYPOSTAGE_BREADCRUMB_5" Default="" /></span></li>
        </ol>
    </div>
    <div id="steps">
        <div id="form5" class="stepsForm">
            <h1><pnc:GetText runat="server" Key="BUYPOSTAGE_RECEIPT_HEADLINE" Default="" /></h1>
            <div id="receiptButtons">
                <input type="button" id="btnPrintReceipt" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_RECEIPT_PRINT_RECEIPT" Default="" />' onclick="printIt('printreceipt');" class="buttonClass secondaryBtn" />
                <input type="button" id="btnPrintLabels" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_RECEIPT_PRINT_LABELS" Default="" />' onclick="printIt('printlabels');"  class="buttonClass secondaryBtn" />
                <input type="button" id="btnRestart" value='<pnc:GetText runat="server" Key="BUYPOSTAGE_RECEIPT_BUY_MORE_LABELS" Default="Køb flere portokoder" />' onclick="restart();" class="buttonClass secondaryBtn" />
            </div>
            <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
            <asp:Panel ID="receiptForm" runat="server"></asp:Panel>
            <div style="display:none">
                <iframe id="linkFrame" src=""></iframe>
            </div>
        </div>
    </div>
</div>
