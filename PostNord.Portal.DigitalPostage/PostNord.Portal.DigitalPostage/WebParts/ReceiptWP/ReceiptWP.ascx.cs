﻿using iTextSharp.text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using PostNord.Portal.DigitalPostage.Helpers;
using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using System.Xml;

namespace PostNord.Portal.DigitalPostage.WebParts.ReceiptWP
{
    /// <summary>
    /// Receipt Webpart
    /// </summary>
    [ToolboxItemAttribute(false)]
    public partial class ReceiptWP : WebPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiptWP"/> class.
        /// </summary>
        public ReceiptWP()
        {
        }

        /// <summary>
        /// Gets or sets the type of border that frames a Web Parts control.
        /// </summary>
        /// <returns>One of the <see cref="T:System.Web.UI.WebControls.WebParts.PartChromeType" /> values. The default is <see cref="F:System.Web.UI.WebControls.WebParts.PartChromeType.Default" />.</returns>
        public override PartChromeType ChromeType
        {
            get
            {
                return PartChromeType.None;
            }
            set
            {
                base.ChromeType = value;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        private Bll.Orders _order = null;

        /// <summary>
        /// Gets the transaction identifier.
        /// </summary>
        /// <value>
        /// The transaction identifier.
        /// </value>
        private string TransactionId
        {
            get
            {
                return this.Page.Request["transactionId"];

            }
        }

        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        private Bll.Orders Order
        {
            get
            {
                if (_order == null)
                {
                    _order = new Bll.Orders(this.TransactionId);
                }

                return _order;
            }
        }


        /// <summary>
        /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
        /// </summary>
        protected override void CreateChildControls()
        {
            if (this.Page.Request["printlabels"] != null)
            {
                PrintLabels();
                return;
            }

            if (this.Page.Request["printreceipt"] != null)
            {
                PrintReceipt();
                return;
            }
            base.CreateChildControls();
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.ApplicationException"></exception>
        protected void Page_Load(object sender, EventArgs e)
        {

            StringBuilder scriptBuilder = new StringBuilder();
            string random = DateTime.Now.Ticks.ToString();

            scriptBuilder.AppendLine(@"
                <link rel='stylesheet' href='/_layouts/DigitalPostage/js/wizstepWP.css?" + random + @"' />
                <!--[if IE]>
                    <link rel='stylesheet' href='/Style%20Library/DigitalPostage/css/wizstepie.css' />
                <![endif]-->

                <script src='/_layouts/DigitalPostage/js/jquery-1.10.2.js' type='text/javascript'></script>
                <script src='/_layouts/DigitalPostage/js/jquery-ui.js' type='text/javascript'></script>
                <link href='//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css' rel='stylesheet' type='text/css' />
                <link href='/Style%20Library/DigitalPostage/css/style.css' rel='stylesheet' type='text/css' />");


            string scriptName = "Receipt";
            if (!this.Page.ClientScript.IsClientScriptBlockRegistered(scriptName))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptName, scriptBuilder.ToString());
            }

            if (string.IsNullOrEmpty(this.TransactionId) || this.TransactionId.Length != 32)
            {
                lblError.Text = "Necessary information not found!";
                lblError.Visible = true;
                return;
            }

            try
            {
                if (!Page.IsPostBack)
                {
                    //btnPrintReceipt.Text = Portal.DigitalPostage.Helpers.TextSettings.GetText("BUYPOSTAGE_RECEIPT_PRINT_RECEIPT");
                    //  .Text = Portal.DigitalPostage.Helpers.TextSettings.GetText("BUYPOSTAGE_RECEIPT_PRINT_LABELS");

                    string template = ConfigSettings.GetText("ReceiptMailTemplate");
                    if (template == string.Empty)
                    {
                        lblError.Text = "Template not found!";
                        lblError.Visible = true;
                        return;
                    }

                    if (this.Order.Id == 0)
                    {
                        lblError.Text = "Order not found!";
                        lblError.Visible = true;
                        return;
                    }

                    string receiptTemplateUrl = ConfigSettings.GetText("ReceiptMailTemplate");

                    string html = this.Order.GetHtml(receiptTemplateUrl);

                    // Remove XML + html headers
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(html);

                    // Remove xml namespaces
                    xmlDoc = Portal.DigitalPostage.Helpers.HelperClass.RemoveXmlns(xmlDoc);
                    XmlNodeList bodys = xmlDoc.GetElementsByTagName("body");
                    if (bodys.Count > 0)
                    {
                        html = bodys[0].InnerXml;
                    }
                    else
                    {
                        html = string.Format("<div style='color:red'>Error parsing XML: {0}</div>", receiptTemplateUrl);
                    }
                    this.receiptForm.Controls.Add(new LiteralControl(html));
                }

            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error loading contents: {0}", ex.Message);
                lblError.Visible = true;
                Logging.LogEvent(lblError.Text, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
            }
        }


        /// <summary>
        /// Prints the receipt.
        /// </summary>
        private void PrintReceipt()
        {
            try
            {
                string receiptTemplateUrl = ConfigSettings.GetText("ReceiptPdfTemplate");
                string html = this.Order.GetHtml(receiptTemplateUrl);

                byte[] pdfReceipt = Portal.DigitalPostage.Helpers.Pdf.CreatePdf(html, PageSize.A4, 10f, 10f, 10f, 0f);
                string PdfFilename = string.Format("Receipt_{0}.pdf", this.Order.CaptureId);

                this.Page.Response.Clear();
                this.Page.Response.ContentType = "application/pdf";
                this.Page.Response.AddHeader("Content-Type", "application/force-download");
                this.Page.Response.AddHeader("Content-Type", "application/download");
                this.Page.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", PdfFilename));
                using (MemoryStream ms = new MemoryStream(pdfReceipt))
                {
                    ms.WriteTo(this.Page.Response.OutputStream);
                    ms.Close();
                }
                try
                {
                    this.Page.Response.Flush();
                    this.Page.Response.End();
                }
                catch { }
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error showing Receipt. {0}", ex.Message);
                lblError.Visible = true;
                Logging.LogEvent(lblError.Text, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);
            }
        }


        /// <summary>
        /// Prints the labels.
        /// </summary>
        private void PrintLabels()
        {
            try
            {
                string labelTemplateUrl = ConfigSettings.GetText("LabelTemplate");

                byte[] pdfLabels = this.Order.GenerateLabels(labelTemplateUrl);

                if (pdfLabels == null)
                {
                    lblError.Text = "Portokoder mangler eller er refunderet. Det var ikke muligt at generere PDF";
                    lblError.Visible = true;
                    return;
                }

                string PdfFilename = string.Format("Postage_{0}.pdf", this.Order.CaptureId);

                this.Page.Response.Clear();
                this.Page.Response.ContentType = "application/pdf";
                this.Page.Response.AddHeader("Content-Type", "application/force-download");
                this.Page.Response.AddHeader("Content-Type", "application/download");
                this.Page.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", PdfFilename));
                using (MemoryStream ms = new MemoryStream(pdfLabels))
                {
                    ms.WriteTo(this.Page.Response.OutputStream);
                    ms.Close();
                }

                try
                {
                    this.Page.Response.Flush();
                    this.Page.Response.End();
                }
                catch { }

            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error showing labels. {0}", ex.Message);
                lblError.Visible = true;
                Logging.LogEvent(lblError.Text, Logging.TraceSeverityLevel.Unexpected, Logging.EventSeverityLevel.Error);

            }
        }

    }
}
