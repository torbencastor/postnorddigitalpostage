﻿using Microsoft.SharePoint.Utilities;
using PN.SoapLibrary;
using PN.SoapLibrary.WSSecurity;
using PostNord.Portal.DigitalPostage.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostNord.Portal.DigitalPostage.MobilePay
{
    /// <summary>
    /// MobilePay Proxy Class
    /// </summary>
    public class Proxy
    {
        /// <summary>
        /// The base URL for Danske Bank WebServices
        /// </summary>
        private static string BaseURL = "https://privatemobilepayservicesCert.danskebank.com/";

        /// <summary>
        /// Calls the refund SOAP service.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <returns></returns>
        private static DB.MobilePay.Refund.dacRefund_Output CallRefundSoapService(PnConfig configuration, string clientID, string merchantId, string orderNr)
        {
            SoapConnection< DB.MobilePay.Refund.RefundV01Client, DB.MobilePay.Refund.RefundV01> soapConnection =
                new SoapConnection< DB.MobilePay.Refund.RefundV01Client,  DB.MobilePay.Refund.RefundV01>(
                    configuration, SPUtility.ConcatUrls(BaseURL, "P2M/BackendAPI/RefundV01"));

             DB.MobilePay.Refund.RefundRequest refundRequest =
                new  DB.MobilePay.Refund.RefundRequest
                {
                    dacRefund_Input = new  DB.MobilePay.Refund.dacRefund_Input { MerchantId = merchantId, OrderId = orderNr },
                    Security = new  DB.MobilePay.Refund.SecurityHeaderType(),
                    RequestHeader = new  DB.MobilePay.Refund.RequestHeaderType
                    {
                        Language = "DA",
                        SenderId = clientID,
                        SignerId1 = clientID,
                        SignerId2 = clientID,
                        SignerId3 = clientID,
                        RequestId = "1",
                        DBCryptId = "",
                        Timestamp = DateTime.Now.ToUniversalTime().DBFormat()
                    }
                };

             return soapConnection.Send(client => client.Refund(ref refundRequest.Security, refundRequest.RequestHeader, refundRequest.dacRefund_Input));
        }

        /// <summary>
        /// Calls the get status SOAP service.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <returns></returns>
        public static DB.MobilePay.GetStatus.Output CallGetStatusSoapService(PnConfig configuration, string clientID, string merchantId, string orderNr)
        {
            SoapConnection<DB.MobilePay.GetStatus.GetStatusV01Client,  DB.MobilePay.GetStatus.GetStatusV01> soapConnection =
                new SoapConnection<DB.MobilePay.GetStatus.GetStatusV01Client,  DB.MobilePay.GetStatus.GetStatusV01>(
                    configuration, SPUtility.ConcatUrls(BaseURL, "P2M/BackendAPI/GetStatusV01"));

             DB.MobilePay.GetStatus.GetStatusRequest statusRequest =
                new  DB.MobilePay.GetStatus.GetStatusRequest
                {
                    Input = new  DB.MobilePay.GetStatus.Input { MerchantId = merchantId, OrderId = orderNr },
                    Security = new  DB.MobilePay.GetStatus.SecurityHeaderType(),
                    RequestHeader = new  DB.MobilePay.GetStatus.RequestHeaderType
                    {
                        Language = "DA",
                        SenderId = clientID,
                        SignerId1 = clientID,
                        SignerId2 = clientID,
                        SignerId3 = clientID,
                        RequestId = "1",
                        DBCryptId = "",
                        Timestamp = DateTime.Now.ToUniversalTime().DBFormat()
                    }
                };

            return soapConnection.Send(client => client.GetStatus(ref statusRequest.Security, statusRequest.RequestHeader, statusRequest.Input));
        }

        /// <summary>
        /// Calls the get status using configured setting for testMode
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <returns></returns>
        public static DB.MobilePayV02.GetStatus.dacGetStatusOutput CallGetStatus(PnConfig configuration, string clientID, string merchantId, string orderNr)
        {
            bool testMode = false;
            bool.TryParse(ConfigSettings.GetText("MobilePayGetOrderStatusTest", "false"), out testMode);
            return CallGetStatus(configuration, clientID, merchantId, orderNr, testMode);
        }

        /// <summary>
        /// Calls the get status SOAP service.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <param name="testMode">if set to <c>true</c> [test mode].</param>
        /// <returns></returns>
        public static DB.MobilePayV02.GetStatus.dacGetStatusOutput CallGetStatus(PnConfig configuration, string clientID, string merchantId, string orderNr, bool testMode)
        {
            SoapConnection<DB.MobilePayV02.GetStatus.GetStatusV02Client, DB.MobilePayV02.GetStatus.GetStatusV02> soapConnection =
                new SoapConnection<DB.MobilePayV02.GetStatus.GetStatusV02Client, DB.MobilePayV02.GetStatus.GetStatusV02>(
                    configuration,  SPUtility.ConcatUrls(BaseURL, "P2M/BackendAPI/GetStatusV02"));

            DB.MobilePayV02.GetStatus.GetStatusRequest statusRequest =
               new DB.MobilePayV02.GetStatus.GetStatusRequest
               {
                   dacGetStatusInput = new DB.MobilePayV02.GetStatus.dacGetStatusInput {
                       OrderId = orderNr,
                       MerchantId = (testMode ? "APPDK0000000000" : merchantId),
                       Test = (testMode?"Y":"N"),
                       ActionCode = "E"
                   },
                   Security = new DB.MobilePayV02.GetStatus.SecurityHeaderType(),
                   RequestHeader = new DB.MobilePayV02.GetStatus.RequestHeaderType
                   {
                       Language = "DA",
                       SenderId = clientID,
                       SignerId1 = clientID,
                       SignerId2 = clientID,
                       SignerId3 = clientID,
                       RequestId = "1",
                       DBCryptId = "",
                       Timestamp = DateTime.Now.ToUniversalTime().DBFormat()
                   }
               };

            return soapConnection.Send(client => client.GetStatus(ref statusRequest.Security, statusRequest.RequestHeader, statusRequest.dacGetStatusInput));
        }


        /// <summary>
        /// Calls the capture using configured setting for testMode.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        public static DB.MobilePayV02.Capture.Output CallCapture(PnConfig configuration, string clientID, string merchantId, string orderNr, decimal amount)
        {
            bool testMode = false;
            bool.TryParse(ConfigSettings.GetText("MobilePayGetOrderStatusTest", "false"), out testMode);
            return CallCapture(configuration, clientID, merchantId, orderNr, amount, testMode);
        }


        /// <summary>
        /// Calls MobilePay Capture.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <param name="testMode">if set to <c>true</c> [test mode].</param>
        /// <returns></returns>
        public static DB.MobilePayV02.Cancel.Output CallCancel(PnConfig configuration, string clientID, string merchantId, string orderNr, bool testMode)
        {
            SoapConnection<DB.MobilePayV02.Cancel.CancelV01Client, DB.MobilePayV02.Cancel.CancelV01> soapConnection =
                new SoapConnection<DB.MobilePayV02.Cancel.CancelV01Client, DB.MobilePayV02.Cancel.CancelV01>(
                    configuration, SPUtility.ConcatUrls(BaseURL, "P2M/BackendAPI/CancelV01"));

            DB.MobilePayV02.Cancel.CancelRequest cancelRequest =
               new DB.MobilePayV02.Cancel.CancelRequest
               {
                   Input = new DB.MobilePayV02.Cancel.Input
                   {
                       OrderID = orderNr,
                       MerchantID = (testMode ? "APPDK0000000000" : merchantId),
                       Test = (testMode ? "Y" : "N")
                   },
                   Security = new DB.MobilePayV02.Cancel.SecurityHeaderType(),
                   RequestHeader = new DB.MobilePayV02.Cancel.RequestHeaderType
                   {
                       Language = "DA",
                       SenderId = clientID,
                       SignerId1 = clientID,
                       SignerId2 = clientID,
                       SignerId3 = clientID,
                       RequestId = "1",
                       DBCryptId = "",
                       Timestamp = DateTime.Now.ToUniversalTime().DBFormat()
                   }
               };

            return soapConnection.Send(client => client.Cancel(ref cancelRequest.Security, cancelRequest.RequestHeader, cancelRequest.Input));
        }


        /// <summary>
        /// Calls MobilePay Capture.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="test">The test.</param>
        /// <returns></returns>
        public static DB.MobilePayV02.Capture.Output CallCapture(PnConfig configuration, string clientID, string merchantId, string orderNr, decimal amount, bool testMode)
        {
            SoapConnection<DB.MobilePayV02.Capture.CaptureV01Client, DB.MobilePayV02.Capture.CaptureV01> soapConnection =
                new SoapConnection<DB.MobilePayV02.Capture.CaptureV01Client, DB.MobilePayV02.Capture.CaptureV01>(
                    configuration, SPUtility.ConcatUrls(BaseURL, "P2M/BackendAPI/CaptureV01"));

            DB.MobilePayV02.Capture.CaptureRequest captureRequest =
               new DB.MobilePayV02.Capture.CaptureRequest
               {
                   Input = new DB.MobilePayV02.Capture.Input
                   {
                       OrderId = orderNr,
                       MerchantId = (testMode ? "APPDK0000000000" : merchantId),
                       Test = (testMode?"Y":"N"),
                       Amount = amount,
                   },
                   Security = new DB.MobilePayV02.Capture.SecurityHeaderType(),
                   RequestHeader = new DB.MobilePayV02.Capture.RequestHeaderType
                   {
                       Language = "DA",
                       SenderId = clientID,
                       SignerId1 = clientID,
                       SignerId2 = clientID,
                       SignerId3 = clientID,
                       RequestId = "1",
                       DBCryptId = "",
                       Timestamp = DateTime.Now.ToUniversalTime().DBFormat()
                   }
               };

            return soapConnection.Send(client => client.Capture(ref captureRequest.Security, captureRequest.RequestHeader, captureRequest.Input));
        }


        /// <summary>
        /// Calls the refund using the configured setting for testMode
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        public static DB.MobilePayV02.Refund.dacRefund_Output CallRefund(PnConfig configuration, string clientID, string merchantId, string orderNr, decimal amount)
        {
            bool testMode = false;
            bool.TryParse(ConfigSettings.GetText("MobilePayGetOrderStatusTest", "false"), out testMode);
            return CallRefund(configuration, clientID, merchantId, orderNr, amount, testMode);
        }

        /// <summary>
        /// Calls the refund Service.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="testMode">if set to <c>true</c> [test mode].</param>
        /// <returns></returns>
        public static DB.MobilePayV02.Refund.dacRefund_Output CallRefund(PnConfig configuration, string clientID, string merchantId, string orderNr, decimal amount, bool testMode)
        {

            SoapConnection<DB.MobilePayV02.Refund.RefundV02Client, DB.MobilePayV02.Refund.RefundV02> soapConnection =
                new SoapConnection<DB.MobilePayV02.Refund.RefundV02Client, DB.MobilePayV02.Refund.RefundV02>(
                    configuration, SPUtility.ConcatUrls(BaseURL, "P2M/BackendAPI/RefundV02"));

            DB.MobilePayV02.Refund.RefundRequest refundRequest =
               new DB.MobilePayV02.Refund.RefundRequest
               {
                   dacRefund_Input = new DB.MobilePayV02.Refund.dacRefund_Input {
                       MerchantId = (testMode ? "APPDK0000000000" : merchantId),
                       Test = (testMode ? "Y" : "N"),
                       OrderId = orderNr 
                   },
                   Security = new DB.MobilePayV02.Refund.SecurityHeaderType(),
                   RequestHeader = new DB.MobilePayV02.Refund.RequestHeaderType
                   {
                       Language = "DA",
                       SenderId = clientID,
                       SignerId1 = clientID,
                       SignerId2 = clientID,
                       SignerId3 = clientID,
                       RequestId = "1",
                       DBCryptId = "",
                       Timestamp = DateTime.Now.ToUniversalTime().DBFormat()
                   }
               };

            return soapConnection.Send(client => client.Refund(ref refundRequest.Security, refundRequest.RequestHeader, refundRequest.dacRefund_Input));
        }

        /// <summary>
        /// Calls the Request Payment SOAP service.
        /// Not implemented in solution yes!
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="clientID">The client identifier.</param>
        /// <param name="merchantId">The merchant identifier.</param>
        /// <param name="orderNr">The order nr.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="bulkRef">The bulk reference.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="test">The test.</param>
        /// <returns></returns>
        public static DB.MobilePayV02.RequestPayment.dacRefund_Output CallRequestPayment(PnConfig configuration, string clientID, string merchantId, string orderNr, decimal amount, string bulkRef, string customerId, bool testMode)
        {
            SoapConnection<DB.MobilePayV02.RequestPayment.RequestPaymentV01Client, DB.MobilePayV02.RequestPayment.RequestPaymentV01> soapConnection =
                new SoapConnection<DB.MobilePayV02.RequestPayment.RequestPaymentV01Client, DB.MobilePayV02.RequestPayment.RequestPaymentV01>(
                    configuration, SPUtility.ConcatUrls(BaseURL, "P2M/BackendAPI/RequestPaymentV01"));

            DB.MobilePayV02.RequestPayment.RequestPaymentRequest paymentRequest =
               new DB.MobilePayV02.RequestPayment.RequestPaymentRequest
               {

                   dacRefund_Input = new DB.MobilePayV02.RequestPayment.dacRefund_Input
                   {
                       OrderId = orderNr,
                       MerchantId = (testMode ? "APPDK0000000000" : merchantId),
                       Test = (testMode ? "Y" : "N"),
                       Amount = amount,
                       BulkRef = bulkRef,
                       CustomerId = customerId
                   },
                   Security = new DB.MobilePayV02.RequestPayment.SecurityHeaderType(),
                   RequestHeader = new DB.MobilePayV02.RequestPayment.RequestHeaderType
                   {
                       Language = "DA",
                       SenderId = clientID,
                       SignerId1 = clientID,
                       SignerId2 = clientID,
                       SignerId3 = clientID,
                       RequestId = "1",
                       DBCryptId = "",
                       Timestamp = DateTime.Now.ToUniversalTime().DBFormat()
                   }
               };

            return soapConnection.Send(client => client.RequestPayment(ref paymentRequest.Security, paymentRequest.RequestHeader, paymentRequest.dacRefund_Input));
        }
    }
}
