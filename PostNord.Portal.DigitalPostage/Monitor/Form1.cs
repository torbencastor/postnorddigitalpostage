﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.SharePoint;
using Microsoft.ApplicationBlocks.Data;
using System.Net;

namespace Monitor
{
    public partial class Form1 : Form
    {
        Hashtable configSettings = null;
        string connectionString = string.Empty;

        /// <summary>
        /// Gets translated text from Translation List
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="listname">The listname.</param>
        /// <returns>
        /// Translated text if found or else Default text
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        public string GetConfig(string key)
        {
            string defaultValue = "";

            // something unexpected is wrong!
            if (configSettings == null)
            {
                LoadData();
                if (configSettings == null)
                    throw new ApplicationException(string.Format("Config Settings not loadet"));
            }

            if (configSettings != null && configSettings.ContainsKey(key))
            {
                return configSettings[key].ToString();
            }
            else
            {
                return defaultValue;
            }
        }



        protected void LoadData()
        {
            try
            {
                lblError.Text = string.Empty;
                string url = txtServer.Text;

                if (url == string.Empty)
                {
                    lblError.Text = "Server Url not set!";
                    return;
                }

                string ListItemTranslatedFieldName = "Value";
                using (SPSite site = new SPSite(url))
                {
                    using (SPWeb web = site.OpenWeb("/admin"))
                    {
                        SPList list = web.Lists["ConfigSettings"];

                        SPQuery query = new SPQuery();
                        query.RowLimit = (uint)list.ItemCount;
                        query.ViewFields = "<FieldRef Name='Title' /><FieldRef Name='Value' />";
                        query.Query = "<OrderBy><FieldRef Name='LinkTitle' /></OrderBy>";
                        SPListItemCollection settings = list.GetItems(query);

                        configSettings = new Hashtable();

                        foreach (SPListItem setting in settings)
                        {
                            if (!configSettings.ContainsKey(setting.Title))
                            {
                                if (setting[ListItemTranslatedFieldName] != null)
                                {
                                    configSettings.Add(setting.Title, setting[ListItemTranslatedFieldName].ToString());
                                }
                            }
                        }
                    }
                }

                connectionString = GetConfig("DigitalPostageConnectionString");
                connectionString = connectionString.Substring(0, connectionString.IndexOf("User"));
                connectionString += "Integrated Security = true";
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error loading data: {0}", ex.Message);
            }
            
        }


        /// <summary>
        /// Creates the web service connection to CINT
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Config Error: GenerateAndValidateDigitalPostageServiceWsUrl is empty</exception>
        private PostNord.Portal.DigitalPostage.GenerateAndValidateDigitalPostageServiceWs.GenerateAndValidateDigitalPostageService CreateWebService()
        {
            // Disable Certficate check
            if (System.Net.ServicePointManager.ServerCertificateValidationCallback == null)
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            }

            PostNord.Portal.DigitalPostage.GenerateAndValidateDigitalPostageServiceWs.GenerateAndValidateDigitalPostageService gvWs = new PostNord.Portal.DigitalPostage.GenerateAndValidateDigitalPostageServiceWs.GenerateAndValidateDigitalPostageService();

            string url = GetConfig("GenerateAndValidateDigitalPostageServiceWsUrl");
            if (gvWs.Url == string.Empty)
            {
                throw new ApplicationException("Config Error: GenerateAndValidateDigitalPostageServiceWsUrl is empty");
            }

            gvWs.Url = url;

            string wsUserName = GetConfig("PurchaseDigitalPostageServiceWsUserName");
            string wsPassword = GetConfig("PurchaseDigitalPostageServiceWsPassword");

            if (wsUserName == string.Empty || wsPassword == string.Empty)
            {
                throw new ApplicationException("Config Error: GenerateAndValidateDigitalPostageServiceWs username or password is missing");
            }

            PostNord.Portal.DigitalPostage.GenerateAndValidateDigitalPostageServiceWs.EifHeaders headers = new PostNord.Portal.DigitalPostage.GenerateAndValidateDigitalPostageServiceWs.EifHeaders();
            gvWs.EifHeadersValue = headers;
            System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
            NetworkCredential netCred = new NetworkCredential(wsUserName, wsPassword);
            myCredentials.Add(new Uri(gvWs.Url), "Basic", netCred);
            gvWs.Credentials = myCredentials;

            return gvWs;
        }




        public Form1()
        {
            InitializeComponent();
        }

        
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
        }

        private void btnUpdateLogData_Click(object sender, EventArgs e)
        {
            string sql = @"SELECT TOP 100 [OrderId]
                                  ,[Timestamp]
                                  ,[Severity]
                                  ,[Text]
                              FROM [DigitalPostage].[dbo].[OrderLogs]
                              WHERE Severity=4
                              Order by [Id] DESC";
            DataSet ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sql);
            dgLogData.AutoGenerateColumns = true;
            dgLogData.DataSource = ds.Tables[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnUpdateOrders_Click(object sender, EventArgs e)
        {
            string sql = @"SELECT TOP 100 [Id]
                              ,[Email]
                              ,[SalesDate]
                              ,[ExpireDate]
                              ,[OrderSum]
                              ,[OrderStatus]
                              ,[PaymentMethod]
                              ,[AppType]
                              ,[SentToCint]
                              ,[Version]
                              ,[Platform]
                          FROM [DigitalPostage].[dbo].[OrderView]
                          ORDER BY [ID] DESC";

            DataSet ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sql);

            dgOrders.AutoGenerateColumns = true;
            dgOrders.DataSource = ds.Tables[0];
        }

        /// <summary>
        /// Gets the postage number. 
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>
        /// The postage week number. (1-366)
        /// </returns>
        /// <remarks>
        /// The Postage number is defined as number of weeks since 1-1-2014. Every 366 weeks (7 years) it is recycled.
        /// </remarks>
        static int GetPostageNumber(DateTime date)
        {
            int MAX_WEEK_COUNT = 366;
            DateTime startDate = new DateTime(2014, 1, 1);

            int weekCount = WeekCount(startDate, date);
            while (weekCount > MAX_WEEK_COUNT)
            {
                weekCount = weekCount - MAX_WEEK_COUNT;
            }
            return weekCount;
        }

        /// <summary>
        /// Get number of weeks between 2 dates.
        /// </summary>
        /// <param name="periodStart">The period start.</param>
        /// <param name="periodEnd">The period end.</param>
        /// <returns>Number of calendar weeks between 2 days</returns>
        static int WeekCount(DateTime periodStart, DateTime periodEnd)
        {
            const DayOfWeek FIRST_DAY_OF_WEEK = DayOfWeek.Monday;
            const DayOfWeek LAST_DAY_OF_WEEK = DayOfWeek.Sunday;
            const int DAYS_IN_WEEK = 7;

            DateTime firstDayOfWeekBeforeStartDate;
            int daysBetweenStartDateAndPreviousFirstDayOfWeek = (int)periodStart.DayOfWeek - (int)FIRST_DAY_OF_WEEK;
            if (daysBetweenStartDateAndPreviousFirstDayOfWeek >= 0)
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-daysBetweenStartDateAndPreviousFirstDayOfWeek);
            }
            else
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-(daysBetweenStartDateAndPreviousFirstDayOfWeek + DAYS_IN_WEEK));
            }

            DateTime lastDayOfWeekAfterEndDate;
            int daysBetweenEndDateAndFollowingLastDayOfWeek = (int)LAST_DAY_OF_WEEK - (int)periodEnd.DayOfWeek;
            if (daysBetweenEndDateAndFollowingLastDayOfWeek >= 0)
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek);
            }
            else
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek + DAYS_IN_WEEK);
            }

            int calendarWeeks = 1 + (int)((lastDayOfWeekAfterEndDate - firstDayOfWeekBeforeStartDate).TotalDays / DAYS_IN_WEEK);

            return calendarWeeks;
        }

        /// <summary>
        /// Convert Price to Code Generator Price
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        static int ConvertPrice(double value)
        {
            return (int)Math.Round(value * 2);
        }


        /// <summary>
        /// Generate postal code
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="franking">The franking in 0,50 kr steps</param>
        /// <param name="codeId">CodeId identifier 1..425910.</param>
        /// <returns>
        /// Generated Post Code
        /// </returns>
        /// <exception cref="System.ApplicationException">Exception is thrown if number of codes found is not 1</exception>
        public string GetCode(DateTime date, double franking, int codeId)
        {
            var ws = CreateWebService();
            var resendReq = new PostNord.Portal.DigitalPostage.GenerateAndValidateDigitalPostageServiceWs.ResendCodeRequest();

            resendReq.CodeID = codeId.ToString();

            resendReq.Date = GetPostageNumber(date);
            resendReq.Value = ConvertPrice(franking);

            var resendResult = ws.ResendCode(resendReq);

            if (resendResult.Length != 1)
            {
                throw new ApplicationException(string.Format("GetCode Error. Number of Codes found: {0}", resendResult.Length));
            }

            string code = resendResult[0].Code;

            if (GetConfig("TestConfig") == "1")
            {
                code = code.Substring(0, code.Length - 4) + "TEST";
            }

            return code;
        }


        private void tabCodeGen_Click(object sender, EventArgs e)
        {



        }

        private void btnCodesUpdate_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            try
            {
                string sql = @"SELECT TOP 10 *
                                  FROM [DigitalPostage].[dbo].[Codes]
                                  Order by [Id] DESC";

                DataSet ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sql);

                DataTable data = ds.Tables[0];
                data.Columns.Add(new DataColumn("Code"));
                foreach (DataRow row in data.Rows)
                {
                    IDataReader r = SqlHelper.ExecuteReader(connectionString, "CodesSelectRow", (long)row["Id"]);
                    if (r.Read())
                    {
                        PostNord.Portal.DigitalPostage.Bll.Codes code = new PostNord.Portal.DigitalPostage.Bll.Codes(r);
                        try
                        {
                            row["Code"] = GetCode(code.ExpireDate, code.Price, code.CodeId);
                        }
                        catch (Exception exc)
                        {
                            row["Code"] = exc.Message;
                        }
                    }

                }

                dgCodes.AutoGenerateColumns = true;
                dgCodes.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                lblError.Text = string.Format("Error loading codes: {0}", ex.Message);
            }
        }
    }
}
