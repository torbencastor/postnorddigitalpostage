﻿namespace Monitor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabLog = new System.Windows.Forms.TabPage();
            this.dgLogData = new System.Windows.Forms.DataGridView();
            this.btnUpdateLogData = new System.Windows.Forms.Button();
            this.tabCodeGen = new System.Windows.Forms.TabPage();
            this.dgCodes = new System.Windows.Forms.DataGridView();
            this.btnCodesUpdate = new System.Windows.Forms.Button();
            this.tabNets = new System.Windows.Forms.TabPage();
            this.tabOrders = new System.Windows.Forms.TabPage();
            this.dgOrders = new System.Windows.Forms.DataGridView();
            this.btnUpdateOrders = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLogData)).BeginInit();
            this.tabCodeGen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCodes)).BeginInit();
            this.tabOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabLog);
            this.tabControl1.Controls.Add(this.tabCodeGen);
            this.tabControl1.Controls.Add(this.tabNets);
            this.tabControl1.Controls.Add(this.tabOrders);
            this.tabControl1.Location = new System.Drawing.Point(24, 90);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(610, 435);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabLog
            // 
            this.tabLog.Controls.Add(this.dgLogData);
            this.tabLog.Controls.Add(this.btnUpdateLogData);
            this.tabLog.Location = new System.Drawing.Point(4, 22);
            this.tabLog.Name = "tabLog";
            this.tabLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabLog.Size = new System.Drawing.Size(602, 409);
            this.tabLog.TabIndex = 0;
            this.tabLog.Text = "Log";
            this.tabLog.UseVisualStyleBackColor = true;
            // 
            // dgLogData
            // 
            this.dgLogData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLogData.Location = new System.Drawing.Point(6, 6);
            this.dgLogData.Name = "dgLogData";
            this.dgLogData.Size = new System.Drawing.Size(509, 397);
            this.dgLogData.TabIndex = 1;
            // 
            // btnUpdateLogData
            // 
            this.btnUpdateLogData.Location = new System.Drawing.Point(521, 6);
            this.btnUpdateLogData.Name = "btnUpdateLogData";
            this.btnUpdateLogData.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateLogData.TabIndex = 0;
            this.btnUpdateLogData.Text = "Update";
            this.btnUpdateLogData.UseVisualStyleBackColor = true;
            this.btnUpdateLogData.Click += new System.EventHandler(this.btnUpdateLogData_Click);
            // 
            // tabCodeGen
            // 
            this.tabCodeGen.Controls.Add(this.dgCodes);
            this.tabCodeGen.Controls.Add(this.btnCodesUpdate);
            this.tabCodeGen.Location = new System.Drawing.Point(4, 22);
            this.tabCodeGen.Name = "tabCodeGen";
            this.tabCodeGen.Padding = new System.Windows.Forms.Padding(3);
            this.tabCodeGen.Size = new System.Drawing.Size(602, 409);
            this.tabCodeGen.TabIndex = 1;
            this.tabCodeGen.Text = "Codegenerator";
            this.tabCodeGen.UseVisualStyleBackColor = true;
            this.tabCodeGen.Click += new System.EventHandler(this.tabCodeGen_Click);
            // 
            // dgCodes
            // 
            this.dgCodes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCodes.Location = new System.Drawing.Point(6, 6);
            this.dgCodes.Name = "dgCodes";
            this.dgCodes.Size = new System.Drawing.Size(509, 397);
            this.dgCodes.TabIndex = 3;
            // 
            // btnCodesUpdate
            // 
            this.btnCodesUpdate.Location = new System.Drawing.Point(521, 6);
            this.btnCodesUpdate.Name = "btnCodesUpdate";
            this.btnCodesUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnCodesUpdate.TabIndex = 2;
            this.btnCodesUpdate.Text = "Update";
            this.btnCodesUpdate.UseVisualStyleBackColor = true;
            this.btnCodesUpdate.Click += new System.EventHandler(this.btnCodesUpdate_Click);
            // 
            // tabNets
            // 
            this.tabNets.Location = new System.Drawing.Point(4, 22);
            this.tabNets.Name = "tabNets";
            this.tabNets.Padding = new System.Windows.Forms.Padding(3);
            this.tabNets.Size = new System.Drawing.Size(602, 409);
            this.tabNets.TabIndex = 2;
            this.tabNets.Text = "NETS";
            this.tabNets.UseVisualStyleBackColor = true;
            // 
            // tabOrders
            // 
            this.tabOrders.Controls.Add(this.dgOrders);
            this.tabOrders.Controls.Add(this.btnUpdateOrders);
            this.tabOrders.Location = new System.Drawing.Point(4, 22);
            this.tabOrders.Name = "tabOrders";
            this.tabOrders.Size = new System.Drawing.Size(602, 409);
            this.tabOrders.TabIndex = 3;
            this.tabOrders.Text = "Orders";
            this.tabOrders.UseVisualStyleBackColor = true;
            // 
            // dgOrders
            // 
            this.dgOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOrders.Location = new System.Drawing.Point(6, 6);
            this.dgOrders.Name = "dgOrders";
            this.dgOrders.Size = new System.Drawing.Size(509, 397);
            this.dgOrders.TabIndex = 3;
            // 
            // btnUpdateOrders
            // 
            this.btnUpdateOrders.Location = new System.Drawing.Point(521, 6);
            this.btnUpdateOrders.Name = "btnUpdateOrders";
            this.btnUpdateOrders.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateOrders.TabIndex = 2;
            this.btnUpdateOrders.Text = "Update";
            this.btnUpdateOrders.UseVisualStyleBackColor = true;
            this.btnUpdateOrders.Click += new System.EventHandler(this.btnUpdateOrders_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Server";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(342, 30);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 3;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(25, 64);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(29, 13);
            this.lblError.TabIndex = 4;
            this.lblError.Text = "Error";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(24, 30);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(296, 20);
            this.txtServer.TabIndex = 5;
            this.txtServer.Text = "http://dev";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 570);
            this.Controls.Add(this.txtServer);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabLog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgLogData)).EndInit();
            this.tabCodeGen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCodes)).EndInit();
            this.tabOrders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgOrders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabLog;
        private System.Windows.Forms.TabPage tabCodeGen;
        private System.Windows.Forms.TabPage tabNets;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgLogData;
        private System.Windows.Forms.Button btnUpdateLogData;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.TabPage tabOrders;
        private System.Windows.Forms.DataGridView dgOrders;
        private System.Windows.Forms.Button btnUpdateOrders;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.DataGridView dgCodes;
        private System.Windows.Forms.Button btnCodesUpdate;
    }
}

