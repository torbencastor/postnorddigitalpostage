﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.Entity
{
    public class EmailData
    {
        public long Id { get; set; }
        public DateTime CreationDatetime { get; set; }
        public string MessageBody { get; set; }
        public string RecieptBody { get; set; }
        public long OrderId { get; set; }
        public string RecipientEmail { get; set; }
        public int Status { get; set; }
       // public string MailSubject { get;  set; }
        public string PdfLabels { get;  set; }
        public byte[] LabelPdf { get;  set; }
        public byte[] ReceiptPdf { get;  set; }
        public byte[] LabelImage { get;  set; }
        public int ImageSize { get;  set; }
    }
}
