﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.Entity
{
    public class SubOrder
    {
        public long Id { get;  set; }
        public string Priority { get;  set; }
        public List<Entity.Code> Codes { get; set; }
        
    }
}
