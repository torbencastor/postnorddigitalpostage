﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.Entity
{
    public class Code
    {
        private CodeStatusValues _status;
        private string _postCode;
        private string _pdfLabel;
        /// <summary>
        /// Valid Status values
        /// </summary>
        public enum CodeStatusValues
        {
            /// <summary>
            /// Code is disabled
            /// </summary>
            Disabled = 0,

            /// <summary>
            /// Code has not been used
            /// </summary>
            Unused = 1,

            /// <summary>
            /// Code has been used
            /// </summary>
            Used = 2,

            /// <summary>
            /// Code has been refunded
            /// </summary>
            Refunded = 3,

            /// <summary>
            /// Code has no value
            /// </summary>
            None = 4
        }


        /// <summary>
        /// Gets the limited status. Used and Unused are returned as None
        /// </summary>
        /// <remarks>
        /// Used to disguise usage for Service staff
        /// </remarks>
        /// <value>
        /// The status limitid. 
        /// </value>
        public CodeStatusValues StatusLimited
        {
            get
            {
                switch (this._status)
                {
                    case CodeStatusValues.Used:
                    case CodeStatusValues.Unused:
                        return CodeStatusValues.None;
                    default:
                        return this._status;
                }
            }
        }
        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public CodeStatusValues Status { get { return this._status; } set { this._status = value; } }
        public DateTime ExpireDate { get; set; }
        public double Price { get; set; }
        public string PostCode { get { return _postCode; } set { _postCode = value; } }
        public long Id { get; set; }
        public string PdfLabel { get { return _pdfLabel; } set { _pdfLabel = value; } }
    }
}
