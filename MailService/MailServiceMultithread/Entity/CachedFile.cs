﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.Entity
{
    internal class CachedFile
    {
        internal string URL { get; set; }
        internal Byte[] FileStream { get; set; }
    }
}
