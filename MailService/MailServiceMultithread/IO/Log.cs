﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MailServiceMultithread.IO
{

    public class Log
    {
        /// <summary>
        /// logging the error to a text file
        /// </summary>
        /// <param name="ex"></param>
        public void WriteErrorToLog(Exception ex)
        {
            //is it an action of writing to the event log
            if (BLL.CachedSettings.EventLogObj != null)
            {
                var logMessage = new System.Text.StringBuilder();
                logMessage.AppendLine("Exception: " + ex.Message);
                logMessage.AppendLine("Stacktrace: " + ex.StackTrace);
                if (ex.InnerException != null)
                {
                    if (!string.IsNullOrEmpty(ex.InnerException.Message)) logMessage.AppendLine("InnerException.Message: " + ex.InnerException.Message);
                    if (!string.IsNullOrEmpty(ex.InnerException.StackTrace)) logMessage.AppendLine("InnerException.Stacktrace: " + ex.InnerException.StackTrace);
                }
                this.WriteMessageToLog(logMessage.ToString());
            }
            else
            {

                this.WriteMessageToLog("Exception: " + ex.Message);
                this.WriteMessageToLog("Stacktrace: " + ex.StackTrace);
                if (ex.InnerException != null)
                {
                    if (!string.IsNullOrEmpty(ex.InnerException.Message)) this.WriteMessageToLog("InnerException.Message: " + ex.InnerException.Message);
                    if (!string.IsNullOrEmpty(ex.InnerException.StackTrace)) this.WriteMessageToLog("InnerException.Stacktrace: " + ex.InnerException.StackTrace);
                }
            }
        }

        /// <summary>
        /// Logging message to text file
        /// </summary>
        /// <param name="message">The text to log</param>
        public void WriteMessageToLog(string message)
        {
            WriteMessageToLog(message, DateTime.Now);
        }

        /// <summary>
        /// Logging message to text file
        /// </summary>
        /// <param name="message">The text to log</param>
        /// <param name="timestamp">Timestamp can be preset</param>
        internal void WriteMessageToLog(string message, DateTime timestamp)
        {
            var settings = new BLL.Settings();
            //is it an action of writing to the event log
            if (BLL.CachedSettings.EventLogObj != null)
            {
                BLL.CachedSettings.EventLogObj.WriteEntry(message);
            }
            else
            {
                var textLogPath = string.Format("{0} {1}.txt", settings.TextLogPath, DateTime.Now.ToString("dd-MM-yyyy"));

                if (!File.Exists(textLogPath))
                {
                    File.Create(textLogPath);
                }
                try
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(textLogPath, true))
                    {
                        try
                        {
                            file.WriteLine(timestamp.ToString("dd-MM-yyyy hh.mm.ss"));
                            file.WriteLine(message);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                        finally
                        {
                            file.Close();
                        }
                    }
                }
                catch (Exception)
                {
                    //in case the file is reserved by another program
                }
            }
        }
    }
}
