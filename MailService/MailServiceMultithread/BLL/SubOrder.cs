﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailServiceMultithread.Entity;

namespace MailServiceMultithread.BLL
{
    internal class SubOrder
    {

        internal List<Entity.SubOrder> GetByOrderId(long id)
        {
            var dalSuborder = new DAL.SubOrder();
            List<Entity.SubOrder> list = dalSuborder.GetByOrderId(id);
            var dalCode = new DAL.Code();
            foreach (Entity.SubOrder suborder in list)
            {
                List<Code> listOfCode = dalCode.GetCodesBySuborderId(suborder.Id);
                suborder.Codes = listOfCode;
            }
            return list;
        }
    }
}
