﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.BLL
{
    public static class CachedSettings
    {
        #region private
        private static System.Drawing.Image _defaultImage = null;
        /// <summary>
        /// For locking the inmemory image 
        /// </summary>
        private static readonly object _locker = new object();
        #endregion

        /// <summary>
        /// In memory caching of SharePoint configuration settings.
        /// </summary>
        public static Hashtable ValuesForConfiguration = null;
        /// <summary>
        /// In memory caching of SharePoint text settings
        /// </summary>
        public static Hashtable ValuesForTextsettings = null;

        /// <summary>
        /// In memory caching of SharePoint image
        /// </summary>
        public static System.Drawing.Image DefaultImage
        {
            get
            {
                lock (_locker)
                {
                    if (_defaultImage == null) return null;

                    var cloneOfImage = _defaultImage.Clone();
                    return (System.Drawing.Image)cloneOfImage;
                }
            }
            set { _defaultImage = value; }
        }
        /// <summary>
        /// In memory caching of SharePoint label template
        /// </summary>
        public static LabelTemplate LabelTemplate = null;

        public static System.Diagnostics.EventLog EventLogObj=null;

        public static void RefreshCache()
        {
            var log = new IO.Log();
            try
            {
                log.WriteMessageToLog("Resetting cached SharePoint resources (files, prises and configuration)");
                ValuesForConfiguration = SharePoint.Site.GetConfigValues();

                ValuesForTextsettings = SharePoint.Site.GetTextsettingsValues();

                CacheDefaultImage();

                CacheLabelTemplate();
            }
            catch (Exception ex)
            {
                log.WriteErrorToLog(ex);
            }

        }

        private static void CacheLabelTemplate()
        {
            string labelTemplateUrl = GetConfigurationText("LabelTemplate");
            try
            {
                var label = new BLL.Label();
                LabelTemplate = label.GetLabelTemplate(labelTemplateUrl);
            }
            catch (Exception ex)
            {
                string error = string.Format("Error caching label template, URL: {0}", labelTemplateUrl);
                throw new ApplicationException(error, ex);
            }

        }
        private static void CacheDefaultImage()
        {
            var defaultImageURL = GetConfigurationText("DefaultLabelImage");
            try
            {
                var sharepointSite = new SharePoint.Site();
                DefaultImage = sharepointSite.GetImageByUrl(defaultImageURL);
            }
            catch (Exception ex)
            {
                string error = string.Format("Error caching default image, URL: {0}", defaultImageURL);
                throw new ApplicationException(error, ex);
            }
        }

        /// <summary>
        /// Gets translated text from Translation List
        /// </summary>
        /// <param _name="Key">The key.</param>
        /// <returns>
        /// Translated text if found or else Empty string
        /// </returns>
        public static string GetConfigurationText(string key)
        {
            // something unexpected is wrong!
            if (ValuesForConfiguration == null)
            {
                throw new ApplicationException(string.Format("Error loading '{0}' from configuraion in cache", key));
            }

            if (ValuesForConfiguration != null && ValuesForConfiguration.ContainsKey(key))
            {
                return ValuesForConfiguration[key].ToString().Replace(Environment.NewLine, "<br/>");
            }
            else
            {
                return string.Empty;
            }
        }
        public static string GetConfigurationText(string key, string fallback)
        {
            var text = GetConfigurationText(key);
            //return the fallback if the text is not in the settings collection
            return string.IsNullOrEmpty(text) ? fallback : text;
        }
        /// <summary>
        /// Gets translated text from Translation List
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// Translated text if found or else Default text
        /// </returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        public static string GetSettingsText(string key, string defaultValue)
        {
            Hashtable values = ValuesForTextsettings;
            // something unexpected is wrong!
            if (values == null)
            {
                throw new ApplicationException(string.Format("Error loading '{0}' from settings in cache", key));
            }

            if (values != null && values.ContainsKey(key))
            {
                return values[key].ToString().Replace(Environment.NewLine, "<br/>");
            }
            else
            {
                return defaultValue;
            }
        }
    }
}
