﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.BLL
{
    public class OrderLogs
    {
        /// <summary>
        /// Severity Values
        /// </summary>
        public enum SeverityValues
        {
            /// <summary>
            /// Information
            /// </summary>
            Info = 1,
            /// <summary>
            /// Debugging
            /// </summary>
            Debug = 2,
            /// <summary>
            /// Error
            /// </summary>
            Error = 4
        }
        public void Add(long orderId, DateTime timestamp, SeverityValues severity, string text)
        {
            var dal = new DAL.OrderLog();
            dal.OrderLogsInsertRow(orderId, timestamp, (short)severity, text);
        }
    }
}
