﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.BLL
{
    internal class Settings
    {

        public int WaitBeforeDatabaseCallInSeconds 
        {
            get
            {
                var waitBeforeDatabaseCallInSeconds = ConfigurationManager.AppSettings["WaitBeforeDatabaseCallInSeconds"];
                if (string.IsNullOrEmpty(waitBeforeDatabaseCallInSeconds)) throw GetException("WaitBeforeDatabaseCallInSeconds");
                return int.Parse( waitBeforeDatabaseCallInSeconds);
            }
        }


        public string SharePointAdminURL
        {
            get
            {
                var sharePointUrl = ConfigurationManager.AppSettings["SharePointAdminURL"];
                if (string.IsNullOrEmpty(sharePointUrl)) throw GetException("SharePointAdminURL");
                return sharePointUrl;
            }
        }

        public NetworkCredential SharePointNetworkCredential
        {
            get
            {
                string sharePointUser = ConfigurationManager.AppSettings["SharePointUser"];
                string sharePointPass = ConfigurationManager.AppSettings["SharePointPass"];
                string sharePointDomain = ConfigurationManager.AppSettings["SharePointDomain"];

                if (string.IsNullOrEmpty(sharePointUser)) throw GetException("SharePointUser");
                if (string.IsNullOrEmpty(sharePointPass)) throw GetException("SharePointPass");
                if (string.IsNullOrEmpty(sharePointDomain)) throw GetException("SharePointDomain");

                var crid = new NetworkCredential(sharePointUser, sharePointPass, sharePointDomain);
                return crid;
            }
        }


        public string DatabaseConnectionstring
        {
            get
            {
                string connectionsstring = ConfigurationManager.ConnectionStrings["databaseConnection"].ConnectionString;
                if (string.IsNullOrEmpty(connectionsstring)) throw GetException("databaseConnection");
                return connectionsstring;
            }
        }
        
        internal int HoursBetweenCacheRefresh
        {
            get
            {
                var hoursBetweenCacheRefresh = ConfigurationManager.AppSettings["HoursBetweenCacheRefresh"];
                if (string.IsNullOrEmpty(hoursBetweenCacheRefresh)) throw GetException("HoursBetweenCacheRefresh");
                return int.Parse(hoursBetweenCacheRefresh);
            }

        }
        internal string TextLogPath
        {
            get
            {
                var textLogPath = ConfigurationManager.AppSettings["TextLogPath"];
                if (string.IsNullOrEmpty(textLogPath)) throw GetException("TextLogPath");
                return textLogPath;
            }
        }

        internal NullReferenceException GetException(string configTagName)
        {
            return new NullReferenceException(string.Format("'{0}' definiton missing in configuration file",configTagName));
        }
    }
}
