﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.BLL
{
   public class EmailServiceLog
    {
        public void InsertErrorDetails(Exception exc,long? emailDataId,long? orderId, string referenceInfo)
        {
            if (exc != null)
            {
                var dal = new DAL.EmailServiceLog();
                dal.InsertErrorDetails(exc, emailDataId, orderId, referenceInfo);
            }
        }
    }
}
