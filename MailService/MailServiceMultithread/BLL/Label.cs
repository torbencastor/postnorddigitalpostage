﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MailServiceMultithread.BLL
{
   internal class Label
    {
        internal LabelTemplate GetLabelTemplate(string labelTemplateUrl)
        {
            var log = new IO.Log();
            try
            {  //get the cached template object
                LabelTemplate template = null;// MailServiceMultithread.BLL.CachedSettings.LabelTemplate;
                ////is there a template in the cache?
                //if (template == null)
                //{
                    var sharePointSite = new SharePoint.Site();
                    //retrieve the template file from SharePoint
                    var file = sharePointSite.GetTemplateFileByURL(labelTemplateUrl);
                    //transform the template to an object
                    using (MemoryStream stream = new MemoryStream(file))
                    {
                        using (TextReader reader = new StreamReader(stream))
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(LabelTemplate));
                            template = (LabelTemplate)serializer.Deserialize(reader);
                        }
                    }
                    //update cache
                    //MailServiceMultithread.BLL.CachedSettings.LabelTemplate = template;
                    log.WriteMessageToLog("Reading '" + labelTemplateUrl + "' from SharePoint and caching it in the memory");
               // }
                return template;
            }
            catch (Exception ex)
            {
                string error = string.Format("Error retrieving labels template: {0}. Error:{1}", labelTemplateUrl, ex.Message);
                throw new ApplicationException(error, ex);
            }
        }
    }
}
