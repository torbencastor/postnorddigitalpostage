﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Drawing;
using MailServiceMultithread.Entity;

namespace MailServiceMultithread.BLL
{
    public class EmailAndPdfData
    {
        #region private variables

        private DateTime _lastCacheTime;
        private int _hoursBetweenCacheRefresh = 0;
        private int _waitBeforeDatabaseCallInSeconds = 0;

        /// <summary>
        /// used only for Unit Test
        /// </summary>
        SmtpClient _testMailClient = null;

        #endregion
        /// <summary>
        /// Constructor.
        /// Loads SharePoint files and settings in to memory
        /// Loads configuration settings into the memory
        /// Refreshes the cash according to the configuration setting 'HoursBetweenCacheRefresh'
        /// </summary>
        /// <param name="eventLog1"></param>
        public EmailAndPdfData(System.Diagnostics.EventLog eventLog1)
        {
            CachedSettings.EventLogObj = eventLog1;
     
            var settings = new BLL.Settings();

            //retrieving configuration settings
            if (this._waitBeforeDatabaseCallInSeconds == 0) this._waitBeforeDatabaseCallInSeconds = settings.WaitBeforeDatabaseCallInSeconds;
            if (this._hoursBetweenCacheRefresh == 0) this._hoursBetweenCacheRefresh = settings.HoursBetweenCacheRefresh;
            if (this._lastCacheTime == null) this._lastCacheTime = DateTime.Now;



            //Data and resources used in the parallel execution has to be loaded at this point for the threads to be able to share them, 
            //else each thread will cache individually
            CachedSettings.RefreshCache();
        }

        #region public methods
        /// <summary>
        /// 1. Refresh cache if the data is to old
        /// 2. Retrieve all email data from the database.
        /// 3. Process all data in an multithreaded fasion.
        /// 4. Send an email for each order (one thread).
        /// 5. Wait a bit if there was nothing to process.
        /// </summary>
        /// <param name="testMailClient"></param>
        public void RetrieveAndSendAllEmails()
        {
            var log = new IO.Log();
            //clear all cached settings if they are two old
            if (_lastCacheTime.AddHours(this._hoursBetweenCacheRefresh) < DateTime.Now)
            {

                BLL.CachedSettings.RefreshCache();
                this._lastCacheTime = DateTime.Now;
            }

            //Getting a list of processed email date, ready to email
            var list = ProcessEmailDataMultithreaded();

            //send the emails one by one in a synchronous manner
            foreach (var email in list)
            {
                SendIndividualMail(email);
            }

            //was there any mails to process, if not the wait a bit before requesting again?
            if (list.Count == 0)
            {
                var timespanWait = new TimeSpan(0, 0, this._waitBeforeDatabaseCallInSeconds);
                System.Threading.Thread.Sleep(timespanWait);
            }
        }

        /// <summary>
        /// Process the email data list by converting the reciept and labels data in to PDF documents.
        /// The process uses all availeble cores on the Sever CPU.
        /// </summary>
        /// <returns></returns>
        public List<Entity.EmailData> ProcessEmailDataMultithreaded()
        {
            var dal = new DAL.EmailAndPdfData();
            var list = dal.GetNotSend();
            //get the count of CPU cores
            int countOfCores = Environment.ProcessorCount;
            Parallel.ForEach(list,
                new ParallelOptions { MaxDegreeOfParallelism = countOfCores },
                (currentObject) =>
                {
                    try
                    {
                        AddPdfDocumentsToEmailData(currentObject);
                    }
                    catch (Exception ex)
                    {
                        UpdateEmailErrorStatus(currentObject.Id, currentObject.Status);
                        var bllOrderLogs = new BLL.OrderLogs();
                        bllOrderLogs.Add(currentObject.OrderId, DateTime.Now, BLL.OrderLogs.SeverityValues.Error, string.Format("Error processeing PDF and sending email: {0}", ex.Message));

                        var bllEmailServiceLog = new BLL.EmailServiceLog();
                        bllEmailServiceLog.InsertErrorDetails(ex, currentObject.Id, currentObject.OrderId, "Method: ProcessEmailDataMultithreaded");
                    }
                }
            );
            return list;
        }

        public List<Entity.EmailData> ProcessEmailData()
        {
            var dal = new DAL.EmailAndPdfData();
            var list = dal.GetNotSend();
            //get the count of CPU cores
            foreach (var currentObject in list)
            {
                AddPdfDocumentsToEmailData(currentObject);
            }
            return list;
        }

        public byte[] GenerateLabels(Entity.EmailData emailData)
        {
            System.Drawing.Image labelImage = GetEmailDataLabelImage(emailData);
            var bllJsonHelper = new Helper.JSON();
            //Get list of products by passing the custom JSON string
            var products = bllJsonHelper.GetListFromMyJSON(emailData.PdfLabels);
            //If there are no products, geration of a PDF label sheet is not possible
            if (products == null || products.Count() == 0)
            {
                return null;
            }
            var label = new BLL.Label();
            LabelTemplate template = CachedSettings.LabelTemplate;

            PostNord.Portal.Tools.Labels.LabelCreator labelCreator = new PostNord.Portal.Tools.Labels.LabelCreator(template, labelImage);
            return labelCreator.CreateLabelSheets(products.ToArray());
        }


        /// <summary>
        /// Creates the PDF from xhtml text.
        /// </summary>
        /// <param name="html">The HTML.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="marginLeft">The margin left.</param>
        /// <param name="marginRight">The margin right.</param>
        /// <param name="marginTop">The margin top.</param>
        /// <param name="marginBottom">The margin bottom.</param>
        /// <returns>PDF contents</returns>
        public static byte[] CreatePdf(string html, iTextSharp.text.Rectangle pageSize, float marginLeft, float marginRight, float marginTop, float marginBottom)
        {
            using (var msOutput = new MemoryStream())
            {
                using (var document = new Document(PageSize.A4, 30, 30, 30, 30))
                {
                    using (var writer = PdfWriter.GetInstance(document, msOutput))
                    {
                        //Open our document for writing
                        document.Open();

                        //Bind a reader to our text
                        using (TextReader reader = new StringReader(html))
                        {
                            //Parse the HTML and write it to the document
                            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, reader);
                        }

                        //Close the main document
                        document.Close();
                    }

                    //Return our raw bytes
                    return msOutput.ToArray();
                }
            }
        }

        #endregion

        #region private methods

        private void AddPdfDocumentsToEmailData(Entity.EmailData emailData)
        {
            byte[] pdfReceipt = null;
            byte[] labelPdf = null;
            pdfReceipt = CreatePdf(emailData.RecieptBody, PageSize.A4, 10f, 10f, 10f, 0f);
            emailData.ReceiptPdf = pdfReceipt;
            labelPdf = this.GenerateLabels(emailData);

            //   System.IO.File.WriteAllBytes(String.Format( @"F:\Temp\label_{0}_{1}.pdf", emailData.OrderId,DateTime.Now.Ticks), labelPdf);// TEST !!!!!!!!!!!!!!!!!!!!!!!

            emailData.LabelPdf = labelPdf;

            if (labelPdf == null)
            {
                var bllOrderLogs = new BLL.OrderLogs();
                bllOrderLogs.Add(emailData.OrderId, DateTime.Now, BLL.OrderLogs.SeverityValues.Error, string.Format("Error: Generating PDF for labels failed"));

            }
            if (pdfReceipt == null)
            {
                var bllOrderLogs = new BLL.OrderLogs();
                bllOrderLogs.Add(emailData.OrderId, DateTime.Now, BLL.OrderLogs.SeverityValues.Error, string.Format("Error: Generating PDF for reciept failed"));

            }
        }

        /// <summary>
        /// Send an email with reciept PDF and label PDF 
        /// </summary>
        /// <param name="emailData"></param>
        private void SendIndividualMail(Entity.EmailData emailData)
        {
            SmtpClient mailClient;
            if (_testMailClient != null)
            {
                mailClient = _testMailClient;
            }
            else
            {
                var smtpServerName = CachedSettings.GetConfigurationText("SMTPServer");
                mailClient = new SmtpClient(smtpServerName);
            }
            var bllOrderLogs = new BLL.OrderLogs();
            var bllEmailServiceLog = new BLL.EmailServiceLog();


            if (emailData.LabelPdf != null & emailData.ReceiptPdf != null)
            {
                string receiptPdfFileName = CachedSettings.GetConfigurationText("ReceiptPdfFileName", "Kvittering.pdf");
                string codesPdfFilename = CachedSettings.GetConfigurationText("CodesFileName", "Portokoder.pdf");
                string mailTitle = CachedSettings.GetConfigurationText("LabelMailTitle", "Kvittering");
                string mailSubject = CachedSettings.GetSettingsText("MailSubject", string.Empty);
                var replyMailAddress = CachedSettings.GetConfigurationText("ReplyMailAddress");

                MailMessage message = new MailMessage(replyMailAddress, emailData.RecipientEmail);
                message.IsBodyHtml = true;
                message.Body = emailData.MessageBody;
                message.Subject = mailSubject;

                try
                {
                    using (MemoryStream labelStream = new MemoryStream(emailData.LabelPdf))
                    {
                        message.Attachments.Add(new Attachment(labelStream, codesPdfFilename, "application/pdf"));
                        using (MemoryStream pdfStream = new MemoryStream(emailData.ReceiptPdf))
                        {
                            Attachment att = new Attachment(pdfStream, receiptPdfFileName, "application/pdf");
                            message.Attachments.Add(att);
                            try
                            {
                                mailClient.Send(message);
                            }
                            catch (Exception ex)
                            {
                                bllOrderLogs.Add(emailData.OrderId, DateTime.Now, BLL.OrderLogs.SeverityValues.Error, string.Format("Error sending mail: {0}", ex.Message));
                                bllEmailServiceLog.InsertErrorDetails(ex, emailData.Id, emailData.OrderId, "Method: SendIndividualMail");
                            }
                        }
                    }
                    //The email was send and therefore the email data status is set to 1, indicationg that the email is not be send again
                    UpdateEmailSuccessStatus(emailData.Id);
                    bllOrderLogs.Add(emailData.OrderId, DateTime.Now, BLL.OrderLogs.SeverityValues.Info, "Email send");
                }
                catch (Exception ex)
                {
                    UpdateEmailErrorStatus(emailData.Id, emailData.Status);
                    bllOrderLogs.Add(emailData.OrderId, DateTime.Now, BLL.OrderLogs.SeverityValues.Error, string.Format("Error sending email:", ex.Message));
                    bllEmailServiceLog.InsertErrorDetails(ex, emailData.Id, emailData.OrderId, "Method: SendIndividualMail");
                }
            }
            else
            {
                UpdateEmailErrorStatus(emailData.Id, emailData.Status);
                bllOrderLogs.Add(emailData.OrderId, DateTime.Now, BLL.OrderLogs.SeverityValues.Error, "Error sending email: attachments missing");
            }
        }
        private System.Drawing.Image GetEmailDataLabelImage(Entity.EmailData emailData)
        {
            System.Drawing.Image labelImage = null;
            //is there and custom image provided by the customer for the labels?
            if (emailData.ImageSize > 0 && emailData.LabelImage.Length > 0)
            {
                using (MemoryStream mem = new MemoryStream(emailData.LabelImage))
                {
                    labelImage = System.Drawing.Image.FromStream(mem);
                }
            }
            if (labelImage == null) //else use the default image provided by PostNord
            {
                labelImage = CachedSettings.DefaultImage;
            }
            return labelImage;
        }
        private void UpdateEmailSuccessStatus(long emailId)
        {
            var dal = new DAL.EmailAndPdfData();
            dal.UpdateStatus(emailId, 1);
        }
        private void UpdateEmailErrorStatus(long emailId, int oldStatus)
        {
            var dal = new DAL.EmailAndPdfData();
            int status = oldStatus == 2 ? 3 : 2; //if the old status is 2, the set i to 3. 3 meens that the error has occured twise
            dal.UpdateStatus(emailId, status);
        }
        #endregion

        #region Test methods
        /// <summary>
        /// 1. Retrieve all email data from the database.
        /// 2. Process all data in an multithreaded fasion.
        /// 3. Send an email for each order. 
        /// </summary>
        /// <param name="testMailClient">SmtpClient for test</param>
        public void RetrieveAndSendAllEmailsTest(SmtpClient testMailClient)
        {
            _testMailClient = testMailClient;
            RetrieveAndSendAllEmails();
        }

        public void AddTestData(EmailData emailData)
        {
            var dal = new DAL.EmailAndPdfData();
            dal.AddTestData(emailData);
        }
        public TimeSpan CompareProcessingOfEmailData()
        {

            var startMulti = DateTime.Now;

            ProcessEmailDataMultithreaded();
            var endMulti = DateTime.Now;

            ProcessEmailData();
            var endSingle = DateTime.Now;

            var multiIsFasterWith = (endMulti - startMulti) - (endSingle - endMulti);
            return multiIsFasterWith;
        }

        /// <summary>
        /// Used only for Unit Test
        /// </summary>
        /// <param name="emailData"></param>
        /// <param name="testMailClient"></param>
        public void SendIndividualMail(Entity.EmailData emailData, SmtpClient testMailClient, string replyMailAddress)
        {
            _testMailClient = testMailClient;
            SendIndividualMail(emailData);
        }
        #endregion
    }
}
