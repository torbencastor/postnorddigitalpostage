﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.IO;
using System.Collections;
using Microsoft.SharePoint.Client;
using System.Net;
using ClientOM = Microsoft.SharePoint.Client;


namespace MailServiceMultithread.SharePoint
{
    public class Site
    {
        private static string _sharePointAdminURL;
        private const string ListItemTranslatedFieldName = "Value";

        public static string SharePointAdminURL
        {
            get
            {
                if (string.IsNullOrEmpty(_sharePointAdminURL))
                {
                    var settings = new BLL.Settings();
                    _sharePointAdminURL = settings.SharePointAdminURL;
                }
                return _sharePointAdminURL;

            }
        }


        internal static ClientContext GetClientContext(string siteUrl)
        {
            try
            {
                if (string.IsNullOrEmpty(siteUrl.Trim())) throw new NullReferenceException("siteUrl");
                var settings = new BLL.Settings();

                ClientContext clientContext = new ClientContext(siteUrl);
                clientContext.Credentials = settings.SharePointNetworkCredential;

                return clientContext;
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal Byte[] GetTemplateFileByURL(string templateURL)
        {
            FileInformation fileInformation;
            using (var clientContext = new ClientContext(SharePointAdminURL))
            {
                fileInformation = ClientOM.File.OpenBinaryDirect(clientContext, templateURL);
                clientContext.ExecuteQuery();
            }
            Byte[] fileAsByteArray;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                fileInformation.Stream.CopyTo(memoryStream);
                fileAsByteArray = memoryStream.ToArray();
            }
            return fileAsByteArray;

        }

        internal System.Drawing.Image GetImageByUrl(string imageUrl)
        {
            var log = new IO.Log();
            log.WriteMessageToLog("Reading '" + imageUrl + "' from SharePoint and caching it in the memory");
            FileInformation fileInformation;
            using (var clientContext = new ClientContext(SharePointAdminURL))
            {
                fileInformation = ClientOM.File.OpenBinaryDirect(clientContext, imageUrl);
                clientContext.ExecuteQuery();
            }
            System.Drawing.Image file = null;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                fileInformation.Stream.CopyTo(memoryStream);
                file = System.Drawing.Image.FromStream(memoryStream);
            }
            return file;
        }
        public static Hashtable GetConfigValues()
        {
            try
            {
                var log = new IO.Log();
                // if Values is set we are in a unit test where Values are set in the test
                if (BLL.CachedSettings.ValuesForConfiguration != null)
                {
                    return BLL.CachedSettings.ValuesForConfiguration;
                }

                CamlQuery camlQuery = new CamlQuery();
                var calm1 = new System.Text.StringBuilder();
                calm1.Append("<ViewFields>");
                calm1.Append("<FieldRef Name='Title'></FieldRef>");
                calm1.Append("<FieldRef Name='Value'></FieldRef>");
                calm1.Append("</ViewFields>");

                ClientContext clientContext = GetClientContext(SharePointAdminURL);

                var list = GetListcollection("ConfigSettings", calm1.ToString());
                var values = GetConfigurationFromCollection(list);
                BLL.CachedSettings.ValuesForConfiguration = values;
                log.WriteMessageToLog("Reading 'ConfigSettings' from SharePoint and caching it in the memory");
                return values;
            }
            catch (Exception ex)
            {
                string error = string.Format("Error retrieving configuration from SharePoint, URL: {0} ", SharePointAdminURL);
                throw new ApplicationException(error, ex);
            }

        }

        public static Hashtable GetTextsettingsValues()
        {
            var log = new IO.Log();
            CamlQuery camlQuery = new CamlQuery();
            var calm1 = new System.Text.StringBuilder();
            calm1.Append("<ViewFields>");
            calm1.Append("<FieldRef Name='Title'></FieldRef>");
            calm1.Append("<FieldRef Name='Value'></FieldRef>");
            calm1.Append("<FieldRef Name='FromDate'></FieldRef>");
            calm1.Append("</ViewFields>");

            var list = GetListcollection("TextSettingsV5", calm1.ToString());
            var values = GetTextsettingsFromCollection(list);

            log.WriteMessageToLog("Reading 'TextSettingsV5' from SharePoint and caching it in the memory");
            return values;
        }

        private static ListItemCollection GetListcollection(string listname, string cQuery)
        {
            try
            {
                using (ClientContext clientContext = GetClientContext(SharePointAdminURL))
                {
                    ClientOM.List oList = clientContext.Web.Lists.GetByTitle(listname);
                    CamlQuery camlQuery = new CamlQuery();
                    camlQuery.ViewXml = cQuery.ToString();
                    ListItemCollection collListItem = oList.GetItems(camlQuery);
                    clientContext.Load(collListItem);
                    clientContext.ExecuteQuery();
                    return collListItem;
                }
            }
            catch
            {
                throw;
            }
        }

        private static Hashtable GetConfigurationFromCollection(ListItemCollection settings)
        {
            Hashtable settingsTable = new Hashtable();
            foreach (ListItem setting in settings)
            {

                if (!settingsTable.ContainsKey(setting["Title"]))
                {
                    if (setting[ListItemTranslatedFieldName] != null)
                    {
                        settingsTable.Add(setting["Title"], setting[ListItemTranslatedFieldName].ToString());
                    }
                }
                else
                {
                    settingsTable[setting["Title"]] = setting[ListItemTranslatedFieldName].ToString();
                }
            }
            return settingsTable;
        }

        private static Hashtable GetTextsettingsFromCollection(ListItemCollection settings)
        {
            Hashtable settingsTable = new Hashtable();
            foreach (ListItem setting in settings)
            {
                if (setting["FromDate"] != null)
                {
                    DateTime fromDate = (DateTime)setting["FromDate"];
                    if (fromDate > DateTime.Today)
                    {
                        continue;
                    }
                }

                if (!settingsTable.ContainsKey(setting["Title"]))
                {
                    if (setting[ListItemTranslatedFieldName] != null)
                    {
                        settingsTable.Add(setting["Title"], setting[ListItemTranslatedFieldName].ToString());
                    }
                }
                else
                {
                    settingsTable[setting["Title"]] = setting[ListItemTranslatedFieldName].ToString();
                }
            }
            return settingsTable;
        }


    }
}
