﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

using System.Xml;
using System.Xml.Linq;

namespace MailServiceMultithread.SharePoint
{

    


    /// <summary>
    /// Various Helper Methods
    /// </summary>
    public class HelperClass
    {

        /// <summary>
        /// Get Date end value.
        /// </summary>
        /// <returns>
        /// Today 23:59:59
        /// </returns>
        public static DateTime Today()
        {
            return DateTime.Today.Date.AddDays(1).AddSeconds(-1);
        }

        ///// <summary>
        ///// Gets the connection string.
        ///// </summary>
        ///// <returns>
        ///// Get Connection string from Configuration list
        ///// </returns>
        //public static string GetConnectionString()
        //{
        //    return PostNord.Portal.DigitalPostage.Helpers.ConfigSettings.GetText("DigitalPostageConnectionString");
        //}

        /// <summary>
        /// Gets the hash.
        /// </summary>
        /// <param name="inputString">The input string.</param>
        /// <returns>SHA1 hashed array of bytes</returns>
        private static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();  // SHA1.Create()
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        /// <summary>
        /// Gets the hash string.
        /// </summary>
        /// <param name="inputString">The input string.</param>
        /// <returns>
        /// SHA1 hashed string
        /// </returns>
        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        ///// <summary>
        ///// Converts the payment amount from Dkk to øre.
        ///// </summary>
        ///// <param name="amountInDkk">The amount in DKK.</param>
        ///// <returns></returns>
        //public static string ConvertPaymentAmount(double amountInDkk)
        //{
        //    int amount = (int)Math.Round(amountInDkk * 100);
        //    return amount.ToString();
        //}

        /// <summary>
        /// Adds the property to Property bag.
        /// </summary>
        /// <param name="web">The web.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public static void AddProperty(SPWeb web, string key, string value)
        {
            web.AllowUnsafeUpdates = true;
            web.Properties[key] = value;
            web.AllProperties[key] = value;
            web.Update();
            web.Properties.Update();
        }

        /// <summary>
        /// Removes the property from Property bag.
        /// </summary>
        /// <param name="web">The web.</param>
        /// <param name="key">The key.</param>
        public static void RemoveProperty(SPWeb web, string key)
        {
            web.AllowUnsafeUpdates = true;
            web.AllProperties.Remove(key);
            web.Properties[key] = null;
            web.Update();
            web.Properties.Update();
        }

        ///// <summary>
        ///// Removes XML Namespaces from XmlDocument.
        ///// </summary>
        ///// <param name="doc">The document.</param>
        ///// <returns>
        ///// XmlDocument without Xml namespaces
        ///// </returns>
        //public static XmlDocument RemoveXmlns(XmlDocument doc)
        //{
        //    XDocument d;
        //    using (var nodeReader = new XmlNodeReader(doc))
        //    {
        //        d = XDocument.Load(nodeReader);
        //    }

        //    d.Root.Descendants().Attributes().Where(x => x.IsNamespaceDeclaration).Remove();

        //    foreach (var elem in d.Descendants())
        //    {
        //        elem.Name = elem.Name.LocalName;
        //    }

        //    var xmlDocument = new XmlDocument();
        //    using (var xmlReader = d.CreateReader())
        //    {
        //        xmlDocument.Load(xmlReader);
        //    }
        //    return xmlDocument;
        //}

        ///// <summary>
        ///// Sorts the list items.
        ///// </summary>
        ///// <param name="items">The items.</param>
        ///// <param name="Descending">if set to <c>true</c> [descending].</param>
        //public static void SortListItems(ListItemCollection items, bool Descending)
        //{
        //    // Sets the CurrentCulture to Danish in Denmark.
        //    Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");
        //    List<ListItem> list = new List<ListItem>();
        //    foreach (ListItem i in items)
        //    {
        //        list.Add(i);
        //    }
        //    if (Descending)
        //    {
        //        list.Sort(delegate (ListItem x, ListItem y) { return y.Text.CompareTo(x.Text); });
        //    }
        //    else
        //    {
        //        list.Sort(delegate (ListItem x, ListItem y) { return x.Text.CompareTo(y.Text); });
        //    }
        //    items.Clear();
        //    items.AddRange(list.ToArray());
        //}

    //    /// <summary>
    //    /// Sorts the list items.
    //    /// </summary>
    //    /// <param name="items">The items.</param>
    //    /// <param name="Descending">if set to <c>true</c> [descending].</param>
    //    public static void SortListValues(ListItemCollection items, bool Descending)
    //    {
    //        // Sets the CurrentCulture to Danish in Denmark.
    //        Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");
    //        List<ListItem> list = new List<ListItem>();
    //        foreach (ListItem i in items)
    //        {
    //            list.Add(i);
    //        }
    //        if (Descending)
    //        {
    //            list.Sort(delegate (ListItem x, ListItem y) { return int.Parse(y.Value).CompareTo(int.Parse(x.Value)); });
    //        }
    //        else
    //        {
    //            list.Sort(delegate (ListItem x, ListItem y) { return int.Parse(x.Value).CompareTo(int.Parse(y.Value)); });
    //        }
    //        items.Clear();
    //        items.AddRange(list.ToArray());
    //    }



    //}

    ///// <summary>
    ///// MobilePay Helper functions
    ///// </summary>
    //public class MobilePay
    //{

    //    public const string RETURNCODE_OK = "00";
    //    public const string RETURNCODE_GENERAL_ERROR = "12";

    //    public const string REASONCODE_OK = "00";
    //    public const string REASONCODE_TIMEOUT = "98";

    //    public const string PAYMENTSTATUS_RESERVED = "RES";
    //    public const string PAYMENTSTATUS_CAPTURED = "CAP";
    //    public const string PAYMENTSTATUS_REQUESTED = "REQ";
    //    public const string PAYMENTSTATUS_CANCELLED = "CAN";
    //    public const string PAYMENTSTATUS_REFUNDED = "REF";
    //    public const string PAYMENTSTATUS_PARTIAL_REFUNDED = "PAR";
    //    public const string PAYMENTSTATUS_EXPIRED = "EXP";
    //    public const string PAYMENTSTATUS_DECLINED = "DEC";
    //    public const string PAYMENTSTATUS_REJECTED = "REF";


    //    /// <summary>
    //    /// MobilePay v2 status translation
    //    /// </summary>
    //    /// <param name="code">The code.</param>
    //    /// <returns></returns>
    //    public static string V2Statuscode(string code)
    //    {
    //        switch (code)
    //        {
    //            case "RES": return "Payment is reserved / authorized by customer initiated from MobilePay app";
    //            case "REQ": return "Payment is requested by the merchant (not app switch functionality)";
    //            case "CAN": return "Reservation / authorization or request is cancelled by the merchant";
    //            case "CAP": return "Payment is captured";
    //            case "REF": return "Total payment is refunded by merchant";
    //            case "PAR": return "Payment is partial refunded by merchant";
    //            case "EXP": return "The payment request is expired (not app switch functionality)";
    //            case "DEC": return "The payment request is declined by the customer (not app switch functionality)";
    //            case "REJ": return "The capture, authorization, refund or cancel is rejected (declined)";
    //        }
    //        return "Unknown code";
    //    }


        ///// <summary>
        ///// MobilePay v2 ReasonCode translation
        ///// </summary>
        ///// <param name="code">The code.</param>
        ///// <returns>
        ///// Interpreted code
        ///// </returns>
        //public static string V2Reasoncode(string code)
        //{
        //    switch (code)
        //    {
        //        case "00": return "OK";
        //        case "01": return "Invalid OrderId - Order id is not specified or has an invalid value";
        //        case "02": return "Invalid MerchantId - Merchant id is not specified or has an invalid value.";
        //        case "04": return "Invalid Test flag - Test has an invalid value";
        //        case "05": return "Invalid Amount - Amount is not specified or has an invalid value";
        //        case "06": return "Invalid TransactionId - TransactionId has an invalid value";
        //        case "07": return "Invalid TimeOut - TimeOut has an invalid value";
        //        case "08": return "Invalid ActionCode - ActionCode is not specified or has an invalid value.";
        //        case "20": return "Merchant not found - The specified MerchantId could not be confirmed as an active Danske Bank customer";
        //        case "21": return "Order not found - The specified OrderId for an existing order, was not found in the MobilePay backend";
        //        case "22": return "Order already found - The specified OrderId, for a new order, was already registered as an OrderId in the MobilePay backend for this Merchant";
        //        case "23": return "Customer not found - The specified CustomerId was not found in the MobilePay backend";
        //        case "40": return "Transaction is already refunded - It is not possible to refund, if the amount is already refunded";
        //        case "41": return "Partial refund is not possible - Specified amount for partiel refund exceeds remaining amount, which can be refunded";
        //        case "42": return "OrderId and TransactionId in input does not match - The specified TransactionId does not exist for the specified OrderId";
        //        case "43": return "OrderId and CustomerId in input does not match - The specified CustomerId does not exist for the specified OrderId";
        //        case "44": return "Call fails for non-technical reasons - The call has failed for a reason which can’t be disclosed. Stating the specific reason may disclose customer sensitive information like reaching daily MobilePay limit or having credit card revoked.";
        //        case "45": return "Transaction is already captured - It is not possible to capture the transaction";
        //        case "46": return "Transaction is already cancelled - It is not possible to cancel the transaction";
        //        case "47": return "Reservation not found - It is not possible to capture the transaction";
        //        case "48": return "Amount not available - It is not possible to refund a payment, because the merchants account has insufficient funds.";
        //        case "98": return "Deadlock or timeout - The call did not succeed because of a deadlock or a timeout in the backend. Wait and retry.";
        //        case "99": return "Technical error - The call did not succeed because of a technical error in the backend. The technical error should be examined by Danske Bank.";
        //    }
        //    return "Unknown code";
        //}

        ///// <summary>
        ///// MobilePay v2 ReturnCode translation
        ///// </summary>
        ///// <param name="code">The code.</param>
        ///// <returns>
        ///// Interpreted code
        ///// </returns>
        //public static string V2Returncode(string code)
        //{
        //    switch (code)
        //    {
        //        case "00":
        //            return "OK";
        //        case "04":
        //            return "Warning - Service completed with input validation errors – please change input data and retry.";
        //        case "08":
        //            return "Error - Service completed with errors. It is not possible to complete this request.";
        //        case "12":
        //            return "General error - Deadlock or timeout issues. The caller should try again later.";
        //        case "24":
        //            return "Severe error -This error should be examined by Danske Bank.";
        //    }
        //    return "Unknown code";
        //}


    }

}
