﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailServiceMultithread.Entity;
using System.Data.SqlClient;

namespace MailServiceMultithread.DAL
{
    internal class SubOrder
    {
        private string _connectionstring;

        internal SubOrder()
        {

            if (string.IsNullOrEmpty(this._connectionstring))
            {
                var settings = new BLL.Settings();
                this._connectionstring = settings.DatabaseConnectionstring;
            }
        }

        internal List<Entity.SubOrder> GetByOrderId(long orderId)
        {
            var list = new List< Entity.SubOrder>();

            using (var sqlConnection = new SqlConnection(this._connectionstring))
            {
                try
                {
                    sqlConnection.Open();
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("orderId", orderId);
                        command.CommandText = "SubOrder_GetByOrderId";
                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var data =new Entity.SubOrder();
                            if (reader["Id"] != DBNull.Value) data.Id = (long)reader["Id"];
                            if (reader["Priority"] != DBNull.Value) data.Priority = (string)reader["Priority"];
                            list.Add(data);
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }
        
       
    }
}
