﻿using System;
using System.Data.SqlClient;

namespace MailServiceMultithread.DAL
{
    public class EmailServiceLog : Connection
    {
        public void InsertErrorDetails(Exception exc,long? emailDataId, long? orderId, string referenceInfo)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "EmailToSendErrorsInsertRow";

                        if (emailDataId.HasValue) command.Parameters.AddWithValue("@EmailToSendId", emailDataId);
                        else command.Parameters.AddWithValue("@EmailToSendId", DBNull.Value);

                        if (orderId.HasValue) command.Parameters.AddWithValue("@OrderId", orderId);
                        else command.Parameters.AddWithValue("@OrderId", DBNull.Value);

                        if (!string.IsNullOrEmpty(referenceInfo)) command.Parameters.AddWithValue("@ReferenceInfo", referenceInfo);
                        else command.Parameters.AddWithValue("@ReferenceInfo", DBNull.Value);

                        command.Parameters.AddWithValue("@Messeage", exc.Message);
                        command.Parameters.AddWithValue("@Stacktrace", exc.StackTrace);
                       
                        if (exc.InnerException!=null && !string.IsNullOrEmpty(exc.InnerException.Message))
                        {
                            command.Parameters.AddWithValue("@InnerMessage", exc.InnerException.Message);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@InnerMessage", DBNull.Value);
                        }
                        if (exc.InnerException != null && !string.IsNullOrEmpty(exc.InnerException.StackTrace)){
                            command.Parameters.AddWithValue("@InnerStacktrace", exc.InnerException.StackTrace);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@InnerStacktrace", DBNull.Value);
                        }
                        command.Parameters.AddWithValue("@TimeOfError", DateTime.Now);
  
                        var reader = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

        }
    }
}
