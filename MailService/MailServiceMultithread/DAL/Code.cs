﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailServiceMultithread.Entity;
using System.Data.SqlClient;

namespace MailServiceMultithread.DAL
{
    internal class Code
    {
        private string _connectionstring;
        private string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionstring))
                {
                    var settings = new BLL.Settings();
                    _connectionstring = settings.DatabaseConnectionstring;
                }
                return _connectionstring;
            }
        }


        internal List<Entity.Code> GetCodesBySuborderId(long SuborderId)
        {
            var list = new List<Entity.Code>();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("suborderId", SuborderId);
                        command.CommandText = "Codes_GetBySuborderId";
                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var data = new Entity.Code();
                            if (reader["Id"] != DBNull.Value) data.Id = (long)reader["Id"];
                            if (reader["ExpireDate"] != DBNull.Value) data.ExpireDate = (DateTime)reader["ExpireDate"];
                            if (reader["PostCode"] != DBNull.Value) data.PostCode = (string)reader["PostCode"];
                            if (reader["Price"] != DBNull.Value) data.Price = (double)reader["Price"];
                            if (reader["Status"] != DBNull.Value) data.Status = (Entity.Code.CodeStatusValues)(Int16)reader["Status"];
                            if (reader["PdfLabel"] != DBNull.Value) data.PdfLabel = (string)reader["PdfLabel"];
                            list.Add(data);
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }
       
        
    }
}
