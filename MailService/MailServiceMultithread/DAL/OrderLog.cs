﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.DAL
{
    public class OrderLog : Connection
    {
        /// <summary>
        /// Insert OrderLogs row.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="text">The text.</param>
        /// <returns>Generated identifier</returns>
        public void OrderLogsInsertRow(long orderId, DateTime timestamp, short severity, string text)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "OrderLogsInsertRow";
                        command.Parameters.AddWithValue("@OrderId", orderId);
                        command.Parameters.AddWithValue("@Timestamp", timestamp);
                        command.Parameters.AddWithValue("@Severity", severity);
                        command.Parameters.AddWithValue("@Text", text);
                        var reader = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}
