﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.DAL
{
  public  class Connection
    {
        //static, so that it is cached in memory (not read form the config every time)
        private static string _connectionstring;
        protected static string ConnectionString
        {
            get
            {
                var log = new IO.Log();
                if (string.IsNullOrEmpty(_connectionstring))
                {
                    var settings = new BLL.Settings();
                    _connectionstring = settings.DatabaseConnectionstring;
                   log.WriteMessageToLog("Reading connectionstring and caching it in the memory");

                }
                return _connectionstring;
            }
        }
    }
}
