﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailServiceMultithread.Entity;

namespace MailServiceMultithread.DAL
{
    public class EmailAndPdfData : Connection
    {
        public void UpdateStatus(long Id, int status)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "EmailToSendUpdateStatus";
                        command.Parameters.AddWithValue("@Id", Id);
                        command.Parameters.AddWithValue("@Status", status);
                        var reader = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }


        public List<Entity.EmailData> GetNotSend()
        {
            var list = new List<Entity.EmailData>();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "EmailToSendGetNotSend";
                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var data = new Entity.EmailData();
                            if (reader["Id"] != DBNull.Value) data.Id = (int)reader["Id"];
                            if (reader["CreationDatetime"] != DBNull.Value) data.CreationDatetime = (DateTime)reader["CreationDatetime"];
                            if (reader["MessageBody"] != DBNull.Value) data.MessageBody = (string)reader["MessageBody"];
                            if (reader["RecieptBody"] != DBNull.Value) data.RecieptBody = (string)reader["RecieptBody"];
                            if (reader["OrderId"] != DBNull.Value) data.OrderId = (long)reader["OrderId"];
                            if (reader["RecipientEmail"] != DBNull.Value) data.RecipientEmail = (string)reader["RecipientEmail"];
                            if (reader["Status"] != DBNull.Value) data.Status = (int)reader["Status"];
                            if (reader["PdfLabels"] != DBNull.Value) data.PdfLabels = (string)reader["PdfLabels"];
                            if (reader["LabelImage"] != DBNull.Value) data.LabelImage = (byte[])reader["LabelImage"];
                            if (reader["ImageSize"] != DBNull.Value) data.ImageSize = (int)reader["ImageSize"];
                            list.Add(data);
                        }
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            return list;
        }

        internal void AddTestData(EmailData emailData)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "EmailToSendInsertRow";
                        command.Parameters.AddWithValue("@CreationDatetime", emailData.CreationDatetime);
                        command.Parameters.AddWithValue("@MessageBody", emailData.MessageBody);
                        command.Parameters.AddWithValue("@RecieptBody", emailData.RecieptBody);
                        command.Parameters.AddWithValue("@OrderId", emailData.OrderId);
                        command.Parameters.AddWithValue("@RecipientEmail", emailData.RecipientEmail);
                        command.Parameters.AddWithValue("@Status", emailData.Status);
                   //     command.Parameters.AddWithValue("@MailSubject", emailData.MailSubject);
                        command.Parameters.AddWithValue("@PdfLabels", emailData.PdfLabels);
                        command.Parameters.AddWithValue("@LabelImage", emailData.LabelImage);
                        command.Parameters.AddWithValue("@ImageSize", emailData.ImageSize);
                        var reader = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}