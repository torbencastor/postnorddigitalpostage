﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailServiceMultithread.Helper
{
    public class JSON
    {


        public string GetAsString(List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo> list)
        {
            try
            {
                var ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo>));
                string JSONstring = string.Empty;
                using (
                var stream1 = new System.IO.MemoryStream())
                {
                    ser.WriteObject(stream1, list);
                    stream1.Position = 0;

                    using (var sr = new System.IO.StreamReader(stream1))
                    {
                        JSONstring = sr.ReadToEnd();
                    }
                }
                return JSONstring;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// The SharePoint runs on framework 3.5 that does not have JSON.
        /// So a custom version us used
        /// </summary>
        /// <param name="myJSON">Custom JSON string</param>
        /// <returns></returns>
        public List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo> GetListFromMyJSON(string myJSON)
        {
            var list = new List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo>();
            myJSON = myJSON.TrimEnd(']').TrimStart('[');

            string[] arrayOfProductinfo = myJSON.Split(new char[] { '}' },StringSplitOptions.RemoveEmptyEntries);
            foreach (var pInfo in arrayOfProductinfo)
            {
                var indexStartOfCode = pInfo.IndexOf("Code:")+5;

                var indexStartOfExpiredate = pInfo.IndexOf("ExpiryDate:")+11;
                var indexStartOfOverlayText = pInfo.IndexOf("OverlayText:")+12;
                var indexStartOfPrice = pInfo.IndexOf("Price:")+6;

                var code = pInfo.Substring(indexStartOfCode, pInfo.IndexOf('|', indexStartOfCode)- indexStartOfCode);
                var expiredateTicks = long.Parse(pInfo.Substring(indexStartOfExpiredate, pInfo.IndexOf('|', indexStartOfExpiredate)- indexStartOfExpiredate));
                var overlayText = pInfo.Substring(indexStartOfOverlayText, pInfo.IndexOf('|', indexStartOfOverlayText)- indexStartOfOverlayText);
                var price = decimal.Parse(pInfo.Substring(indexStartOfPrice, pInfo.Length- indexStartOfPrice));

                list.Add(new PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo()
                {
                    Code = FormatCode(code),
                    ExpiryDate = new DateTime(expiredateTicks),
                    OverlayText = overlayText == "A" ? "Q" : string.Empty,
                    Price = price
                });
            }
            return list;
        }

        public List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo> GetAsList(string jsonString)
        {
            try
            {
                using (var ms = new System.IO.MemoryStream(Encoding.Unicode.GetBytes(jsonString)))
                {

                    var serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo>));
                    var list = (List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo>)serializer.ReadObject(ms);

                    var returnList = new List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo>();
                    //format the label code
                    foreach (var label in list)
                    {
                        returnList.Add(new PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo()
                        {
                            Code = FormatCode(label.Code),
                            ExpiryDate = label.ExpiryDate,
                            OverlayText = label.OverlayText == "A" ? "Q" : string.Empty, //return Q if A, else return empty
                            Price = label.Price,
                            ProductName = label.ProductName
                        });

                    }


                    return returnList;
                }
            }
            catch (Exception)
            {
                return null;
            }

        }
        /// <summary>
        /// Formats the code
        /// </summary>
        /// <param name="code">Code to be formatted</param>
        /// <returns>Formatted code</returns>
        private string FormatCode(string code)
        {
            string[] parts = code.Split(new char[] { '-' });

            StringBuilder result = new StringBuilder();

            foreach (string part in parts)
            {
                result.AppendLine((part.Aggregate(string.Empty, (c, i) => c + i + ' ')).Trim());
            }

            return result.ToString();
        }

    }
}
