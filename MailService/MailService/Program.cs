﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailServiceMultithread;
using System.Configuration;

namespace MailService
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press enter to exit");
            var log = new MailServiceMultithread.IO.Log();
            try
            {

                MailServiceMultithread.BLL.EmailAndPdfData bll = new MailServiceMultithread.BLL.EmailAndPdfData(null);
                while (!Console.KeyAvailable)
                {
                    try
                    {
                        //Run:
                        // 1. Refresh cache if the data is to old
                        // 2. Retrieve all email data from the database.
                        // 3. Process all data in an multithreaded fasion.
                        // 4. Send an email for each order.  
                        bll.RetrieveAndSendAllEmails();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception.Message: " + ex.Message);
                        log.WriteErrorToLog(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception.Message: " + ex.Message);
                log.WriteErrorToLog(ex);
            }
            Console.ReadLine();
        }
    }
}
