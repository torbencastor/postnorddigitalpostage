﻿CREATE TABLE [dbo].[EmailPDF]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [CreationDatetime] DATETIME NOT NULL, 
    [MessageBody] NVARCHAR(MAX) NOT NULL, 
    [RecieptBody] NVARCHAR(MAX) NOT NULL, 
    [OrderId] BIGINT NOT NULL, 
    [RecipientEmail] NVARCHAR(300) NOT NULL, 
    [Status] INT NOT NULL
)
