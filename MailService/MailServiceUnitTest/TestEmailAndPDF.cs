﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Collections.Generic;

namespace MailServiceUnitTest
{
    [TestClass]
    public class TestEmailAndPDF
    {
        private List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo> GetList(int amountOfCodes)
        {
            var newList = new List<PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo>();
            for (int i = 0; i < amountOfCodes; i++)
            {
                newList.Add(new PostNord.Portal.Tools.Labels.LabelCreator.ProductInfo()
                {
                    Code = "THF8-8H5F-E6E8",
                    ExpiryDate = DateTime.Now.AddDays(20),
                    Price = (decimal)8,
                    OverlayText = "B"
                });
            }
            return newList;

        }


        [TestMethod]
        public void TestAddTestData()
        {
            var bllEmailData = new MailServiceMultithread.BLL.EmailAndPdfData(null);
            string emialBody = System.IO.File.ReadAllText(@"C:\Source\PostNord\Postnord Digital Postage\Source\MailService\MailServiceUnitTest\Testfiles\EmilaBodyHtml.html");
            string recieptBody = System.IO.File.ReadAllText(@"C:\Source\PostNord\Postnord Digital Postage\Source\MailService\MailServiceUnitTest\Testfiles\RecieptHtml.html");

            var emailAndPdfData = new MailServiceMultithread.BLL.EmailAndPdfData(null);

            //var jsonHelper = new MailServiceMultithread.Helper.JSON();
            //var ProductList = GetList(6);
            //var jsonString = jsonHelper.GetAsString(ProductList);

            //var list = jsonHelper.GetAsList(jsonString);

            //Assert.AreEqual(list.Count, ProductList.Count);
            //Assert.AreEqual(list[0].Code, ProductList[0].Code);
            //Assert.AreEqual(list[1].Code, ProductList[1].Code);


            string pdfLabels = "[{Code:7EFA-87HF-2T8T|ExpiryDate:636246144000000000|OverlayText:B|Price:42}"
                + "{Code:MXF0-CYTF-NTV2|ExpiryDate:636246144000000000|OverlayText:B|Price:42,78}"
                + "{Code:N5FT-ML2F-0T05|ExpiryDate:636246144000000000|OverlayText:B|Price:42,00}"
                + "{Code:3KFV-M08F-0TY5|ExpiryDate:636246144000000000|OverlayText:B|Price:42,12}"
                + "{Code:HLF9-03MF-2TK3|ExpiryDate:636246144000000000|OverlayText:B|Price:42,56}]";

            var emailData = new MailServiceMultithread.Entity.EmailData()
            {
                OrderId = 2867,
                RecipientEmail = "torben.naess@innofactor.com",
                MessageBody = emialBody,
                RecieptBody = recieptBody,
                PdfLabels = pdfLabels,
                CreationDatetime = DateTime.Now,
                Status = 0,
                ImageSize = 0,
                LabelImage = null

            };


            bllEmailData.AddTestData(emailData);
        }


        [TestMethod]
        public void TestLabelPDFGenerationOnly()
        {
            var emailAndPdfData = new MailServiceMultithread.BLL.EmailAndPdfData(null);
            var dal = new MailServiceMultithread.DAL.EmailAndPdfData();
            var list = dal.GetNotSend();

            //string labelTemplateUrl = "/Style Library/DigitalPostage/templates/lyrecoTemplate.xml";//
            var label1 = emailAndPdfData.GenerateLabels(list[0]);//, labelTemplateUrl);
            Assert.AreEqual(null, label1);

            var label2 = emailAndPdfData.GenerateLabels(list[1]);//, labelTemplateUrl);
            System.IO.File.WriteAllBytes(@"c:\temp\label.pdf", label2);
            Assert.AreEqual(true, label2.Length > 0);

        }

        [TestMethod]
        public void TestLabelPDFGeneration()
        {
            var emailAndPdfData = new MailServiceMultithread.BLL.EmailAndPdfData(null);
            var dal = new MailServiceMultithread.DAL.EmailAndPdfData();
            var list = dal.GetNotSend();

            //string labelTemplateUrl = "/Style Library/DigitalPostage/templates/lyrecoTemplate.xml";//
            var label1 = emailAndPdfData.GenerateLabels(list[0]);//, labelTemplateUrl);
            Assert.AreEqual(null, label1);

            var label2 = emailAndPdfData.GenerateLabels(list[1]);//, labelTemplateUrl);
            System.IO.File.WriteAllBytes(@"c:\temp\label.pdf", label2);
            Assert.AreEqual(true, label2.Length > 0);

        }


        [TestMethod]
        public void TestRecieptPDFGeneration()
        {
            string recieptBody = System.IO.File.ReadAllText(@"C:\Source\PostNord\Postnord Digital Postage\Source\MailService\MailServiceUnitTest\Testfiles\RecieptHtml.txt", System.Text.Encoding.ASCII);
            var pdfReceipt = MailServiceMultithread.BLL.EmailAndPdfData.CreatePdf(recieptBody, PageSize.A4, 10f, 10f, 10f, 0f);
            Assert.AreEqual(true, pdfReceipt.Length > 0);
        }

        [TestMethod]
        public void TestEmailBody()
        {
            string emialBody = System.IO.File.ReadAllText(@"C:\Source\PostNord\Postnord Digital Postage\Source\MailService\MailServiceUnitTest\Testfiles\EmilaBodyHtml.html");
            TestUsingHotmail(emialBody);
        }

        [TestMethod]
        public void TestEmail()
        {
            string emialBody = System.IO.File.ReadAllText(@"C:\Source\PostNord\Postnord Digital Postage\Source\MailService\MailServiceUnitTest\Testfiles\EmilaBodyHtml.html");
            string recieptBody = System.IO.File.ReadAllText(@"C:\Source\PostNord\Postnord Digital Postage\Source\MailService\MailServiceUnitTest\Testfiles\RecieptHtml.html");

            var emailAndPdfData = new MailServiceMultithread.BLL.EmailAndPdfData(null);
            var emailData = new MailServiceMultithread.Entity.EmailData()
            {
                OrderId = 1,
                RecipientEmail = "torben.naess@innofactor.com",
                //   MailSubject = "test",
                MessageBody = emialBody,
                RecieptBody = recieptBody,
                //CodesPdfFilename = "labels.pdf",
                //RecieptPdfFileName = "reciept.pdf"
            };
            var hotmialClient = GetHotmailClient();
            emailAndPdfData.SendIndividualMail(emailData, hotmialClient, "xx@hotmail.com");
        }

        [TestMethod]
        public void TestCompareProcessingOfEmailData()
        {
            var emailAndPdfData = new MailServiceMultithread.BLL.EmailAndPdfData(null);
            var result = emailAndPdfData.CompareProcessingOfEmailData();
            var secondsFaster = result.Seconds;
            Assert.AreEqual(true, result.Seconds > 0);

        }

        [TestMethod]
        public void TestSendAllEmails()
        {
            var emailAndPdfData = new MailServiceMultithread.BLL.EmailAndPdfData(null);
            emailAndPdfData.RetrieveAndSendAllEmailsTest(GetHotmailClient());
        }

        [TestMethod]
        public void TestFakeNOrmalRun()
        {
            MailServiceMultithread.BLL.EmailAndPdfData bll;

            while (true)
            {
                //call the constructor every time to maintane the cached settings and cached data
                bll = new MailServiceMultithread.BLL.EmailAndPdfData(null);
                var client = GetHotmailClient();
                //Run:
                //1.Retrieve all email data from the database.
                //2.Process all data in an multithreaded fasion.
                //3.Send an email for each order. 
                bll.RetrieveAndSendAllEmailsTest(client);
            }
        }



        private System.Net.Mail.SmtpClient GetHotmailClient()
        {
            System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("smtp.live.com");
            SmtpServer.Port = 25;// 587;
            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.Credentials = GetNetworkCredential();
            SmtpServer.EnableSsl = true;
            return SmtpServer;

        }
        private void TestUsingHotmail(string htmlBody)
        {

            ///"smtp.live.com";
            using (System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("smtp.live.com"))
            {
                var mail = new System.Net.Mail.MailMessage();
                mail.From = new System.Net.Mail.MailAddress("xx@hotmail.com");
                mail.To.Add("torben.naess@innofactor.com");
                mail.Subject = "Test 1";
                mail.IsBodyHtml = true;
                mail.Body = htmlBody;//"this is a test email sent from c# project with hotmail.";
                SmtpServer.Port = 25;// 587;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = GetNetworkCredential();
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
            }
        }



        /// <summary>
        /// Read credential from a text file: first line is the email accoung, second line is the password
        /// </summary>
        /// <returns></returns>
        private System.Net.NetworkCredential GetNetworkCredential()
        {
            System.Net.NetworkCredential cre = null;
            // Read the file and display it line by line.
            using (System.IO.StreamReader file = new System.IO.StreamReader("c:\\Temp\\hotmail.txt"))
            {
                var hotmailAccount = file.ReadLine();
                var hotmailPassword = file.ReadLine();
                file.Close();
                cre = new System.Net.NetworkCredential(hotmailAccount, hotmailPassword);
            }
            return cre;
        }
    }
}
