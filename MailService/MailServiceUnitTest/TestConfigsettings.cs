﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MailServiceUnitTest
{
    [TestClass]
    public class TestConfigsettings
    {
        [TestMethod]
        public void TestLabelURLFromSharePoint()
        {
            string labelTemplateUrl = MailServiceMultithread.BLL.CachedSettings.GetConfigurationText("LabelTemplate");
            string expected = "/Style Library/DigitalPostage/templates/lyrecoTemplate.xml";
            Assert.AreEqual(expected, labelTemplateUrl); //this test can fail if the url is changed in sharepoint
        }
    }
}
