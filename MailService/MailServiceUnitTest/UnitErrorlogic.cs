﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace MailServiceUnitTest
{
    /// <summary>
    /// Summary description for UnitErrorlogic
    /// </summary>
    [TestClass]
    public class UnitErrorlogic
    {
        public UnitErrorlogic()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private string _testErrorHandlingArray = "a,b,c,d,e,f,1,1,a,b,1,1,a,b,c,d,e,f,1,1";
        private string _errorHandlingWaitPeriodsInMinutes = "1,2,3,4,5";

        [TestMethod]
        public void TestMethod1()
        {
            var arrayOfWaitperiods = this._errorHandlingWaitPeriodsInMinutes.Split(new char[] { ',' });
            var errorCount = 0;
            int testCount = -1;

            while (true)
            {
                try
                {
                    //this is only to test the error handling logic
                    if (!string.IsNullOrEmpty(this._testErrorHandlingArray))
                    {
                        System.Threading.Thread.Sleep(new TimeSpan(0, 0, 1));
                        string[] testValueArray = this._testErrorHandlingArray.Split(new char[] { ',' });
                        //reset the count if the test array is at its end
                        if (testCount == testValueArray.Length - 1)
                        {
                            testCount = -1;
                        }
                        testCount++;//always increment, else it will be stuck

                        //will throw an error when the array contains character that canont be cast 
                        var someValue = int.Parse(testValueArray[testCount]);
                        errorCount = 0;//reset the errorcount, because there ar no errors at this point
                    }

                }
                catch (Exception ex)
                {
                    //construct message for eventlog and e-mail
                    var message = string.Format("Error in processing e-mail queue, error {0}/{1}, {2} minute(s) to next attempt ", errorCount + 1, arrayOfWaitperiods.Length, arrayOfWaitperiods[errorCount]);
                    //eventLog1.WriteEntry(message, EventLogEntryType.Error);
                    SendEmail(message, ex);
                    System.Threading.Thread.Sleep(new TimeSpan(0, 0, int.Parse(arrayOfWaitperiods[errorCount])));
                    //reset errorcount if the array has reached the end
                    if (errorCount == arrayOfWaitperiods.Length - 1) errorCount = -1;
                    errorCount++;
                    //sleep for x minuts 
                }
            }
        }

        private void SendEmail(string textMessage, Exception ex)
        {
            try
            {
                string body = ConstructHtmlBody(textMessage, ex);
                Debug.WriteLine(body);
            }
            catch (Exception exx)
            {
                var errrormessage = string.Format("Sending error e-mail fialed with the message: {0}", exx.Message);
            }
        }
        private string ConstructHtmlBody(string errorMessage, Exception ex)
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendLine("<H2>Message from Digital Porto e-mail service</H2>");
            sb.AppendLine(string.Format("<p>{0}</p>", errorMessage));

            if (ex != null)
            {
                sb.AppendLine("<H2>System exception message</H2>");
                sb.AppendLine(string.Format("<p>{0}</p>", ex.Message));
                sb.AppendLine("<H2>System exception stacktrace</H2>");
                sb.AppendLine(string.Format("<p>{0}</p>", ex.StackTrace));

                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    sb.AppendLine("<H3>System innerException message</H3>");
                    sb.AppendLine(string.Format("<p>{0}</p>", ex.InnerException.Message));
                    sb.AppendLine("<H3>System innerException stacktrace</H3>");
                    sb.AppendLine(string.Format("<p>{0}</p>", ex.InnerException.StackTrace));
                }
            }
            return sb.ToString();
        }


    }
}
