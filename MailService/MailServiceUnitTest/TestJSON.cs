﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MailServiceUnitTest
{
    [TestClass]
    public class TestJSON
    {
        [TestMethod]
        public void TestMyJSON()
        {
            var myJSON =new MailServiceMultithread.Helper.JSON();
         var list=   myJSON.GetListFromMyJSON("[{Code:7EFA-87HF-2T8T|ExpiryDate:636246144000000000|OverlayText:B|Price:42}"
                +"{Code:MXF0-CYTF-NTV2|ExpiryDate:636246144000000000|OverlayText:B|Price:42,78}"
                +"{Code:N5FT-ML2F-0T05|ExpiryDate:636246144000000000|OverlayText:B|Price:42,00}"
                +"{Code:3KFV-M08F-0TY5|ExpiryDate:636246144000000000|OverlayText:B|Price:42,12}"
                +"{Code:HLF9-03MF-2TK3|ExpiryDate:636246144000000000|OverlayText:B|Price:42,56}]");

            Assert.AreEqual(42M, list[0].Price);
            Assert.AreEqual(42.78M, list[1].Price);
            Assert.AreEqual(42.00M, list[2].Price);
            Assert.AreEqual(42.12M, list[3].Price);
            Assert.AreEqual(42.56M, list[4].Price);

        }

    }
}
                                                            