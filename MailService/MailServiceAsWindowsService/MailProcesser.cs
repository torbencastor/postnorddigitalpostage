﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace MailServiceAsWindowsService
{
    public partial class MailService : ServiceBase
    {
        private string _testErrorHandlingArray;
        private string _errorHandlingWaitPeriodsInMinutes;
        private string _errorEmailSubject;
        private ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        private Thread _backgroundThread;

        internal void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }

        private string _errorEmailRecipient;

        public MailService()
        {
            InitializeComponent();
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("MailService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MailService", "MailServiceLog");
            }
            eventLog1.Source = "MailService";
            //  eventLog1.Log = "MailServiceLog";
        }

        /// <summary>
        /// The services is build to entisipate sybsystem failure and inform the system owner about it.
        /// When an error occurs the system will write the error to the event log, send a notification to the product owner
        /// and pause for a period. 
        /// The pauses are defined in a comma seperated array in the configuration file ErrorHandlingWaitPeriodsInMinutes.
        /// The service has a build in test feature that cna be given an array of values in the configuration file TestErrorHandlingArray. 
        /// If the TestErrorHandlingArray is empty the service will run normally, if it has an commaseperated array, then the test will run and the mail processing will be ignored.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("Starting service", EventLogEntryType.Information);
            this._testErrorHandlingArray = ConfigurationManager.AppSettings["TestErrorHandlingArray"];
            this._errorHandlingWaitPeriodsInMinutes = ConfigurationManager.AppSettings["ErrorHandlingWaitPeriodsInMinutes"];
            this._errorEmailSubject = ConfigurationManager.AppSettings["ErrorEmailSubject"];
            this._errorEmailRecipient = ConfigurationManager.AppSettings["ErrorEmailRecipient"];

            if (string.IsNullOrEmpty(this._testErrorHandlingArray)) eventLog1.WriteEntry("TestErrorHandlingArray has no value in configuration file, e-mails are being processed", EventLogEntryType.Information);//this is not an error, just an information that it is not running test mode
            else
            {
                eventLog1.WriteEntry("TestErrorHandlingArray has value in configuration file, test mode is enabled", EventLogEntryType.Information);
            }
            if (string.IsNullOrEmpty(this._errorHandlingWaitPeriodsInMinutes))
            {
                eventLog1.WriteEntry("ErrorHandlingWaitPeriodsInMinutes has no value in configuration file", EventLogEntryType.Error);
                this.Stop();
            };
            if (string.IsNullOrEmpty(this._errorEmailSubject))
            {
                eventLog1.WriteEntry("ErrorEmailSubject has no value in configuration file", EventLogEntryType.Error);
                this.Stop();
            }
            if (string.IsNullOrEmpty(this._errorEmailRecipient))
            {
                eventLog1.WriteEntry("ErrorEmailRecipient has no value in configuration file", EventLogEntryType.Error);
                this.Stop();
            }
            eventLog1.WriteEntry("Configuration settings loaded from the configuration file", EventLogEntryType.Information);
            //init EmailAndPdfData to cache settings from SharePoint (get settings for sending e-mail)
            MailServiceMultithread.BLL.EmailAndPdfData emailAndPdfData = new MailServiceMultithread.BLL.EmailAndPdfData(eventLog1);

            SendEmail("Configuration settings successfully cached. Starting service as a background process", null);

            _backgroundThread = new Thread(() => ProcessMails(emailAndPdfData));
            _backgroundThread.Name = "E-mail service background process";
            _backgroundThread.IsBackground = true;
            _backgroundThread.Start();
        }

        private void ProcessMails(MailServiceMultithread.BLL.EmailAndPdfData emailAndPdfData)
        {
            try
            {
                var arrayOfWaitperiods = this._errorHandlingWaitPeriodsInMinutes.Split(new char[] { ',' });
                var errorCount = 0;
                int testCount = -1;

                while (!_shutdownEvent.WaitOne(0))
                {
                    try
                    {
                        //this is only to test the error handling logic
                        if (!string.IsNullOrEmpty(this._testErrorHandlingArray))
                        {
                            System.Threading.Thread.Sleep(new TimeSpan(0, 0, 30));
                            string[] testValueArray = this._testErrorHandlingArray.Split(new char[] { ',' });
                            //reset the count if the test array is at its end
                            if (testCount == testValueArray.Length - 1) testCount = -1;
                            testCount++;//always increment, else it will be stuck

                            //will throw an error when the array contains character that canont be cast 
                            var someValue = int.Parse(testValueArray[testCount]);
                            errorCount = 0;//reset the errorcount, because there ar no errors at this point
                        }
                        else
                        {
                            //Run:
                            // 1. Refresh cache if the data is to old
                            // 2. Retrieve all email data from the database.
                            // 3. Process all data in an multithreaded fasion.
                            // 4. Send an email for each order.  
                            emailAndPdfData.RetrieveAndSendAllEmails();
                            errorCount = 0;//reset the errorcount, because the previos method call did not result in an error
                        }
                    }
                    catch (Exception ex)
                    {
                        //construct message for eventlog and e-mail
                        var message = string.Format("Error in processing e-mail queue, error {0}/{1}, {2} minute(s) to next attempt ", errorCount + 1, arrayOfWaitperiods.Length, arrayOfWaitperiods[errorCount]);
                        var messageForLog = GetErrorMessageForLog(ex, message);
                        eventLog1.WriteEntry(messageForLog, EventLogEntryType.Error);
                        SendEmail(message, ex);
                        //sleep for x minuts 
                        System.Threading.Thread.Sleep(new TimeSpan(0, int.Parse(arrayOfWaitperiods[errorCount]), 0));

                        //reset errorcount if the array has reached the end
                        if (errorCount == arrayOfWaitperiods.Length - 1) errorCount = -1;
                        errorCount++;
                      
                    }
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry(string.Format("Error occured when running service, error: ", ex.Message), EventLogEntryType.Error);
                SendEmail("Error occured when running service", ex);
            }
        }
        private string GetErrorMessageForLog(Exception ex, string message)
        {
            var messageForLog = new System.Text.StringBuilder();
            messageForLog.AppendLine(message);
            messageForLog.AppendLine("Error:");
            messageForLog.AppendLine(ex.Message);
            messageForLog.AppendLine(ex.StackTrace);
            if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
            {
                messageForLog.AppendLine(ex.InnerException.Message);
                messageForLog.AppendLine(ex.InnerException.StackTrace);
            }
            return messageForLog.ToString();
        }

        protected override void OnStop()
        {
            _shutdownEvent.Set();
            if (!_backgroundThread.Join(3000))
            { // give the thread 3 seconds to stop
                _backgroundThread.Abort();
            }
            SendEmail("Stopping service", null);
            eventLog1.WriteEntry("Stopping service", EventLogEntryType.Information);
        }

        private void eventLog1_EntryWritten(object sender, EntryWrittenEventArgs e)
        {

        }

        private void SendEmail(string textMessage, Exception ex)
        {
            try
            {
                var smtpServerName = MailServiceMultithread.BLL.CachedSettings.GetConfigurationText("SMTPServer");
                using (SmtpClient mailClient = new SmtpClient(smtpServerName))
                {
                    var replyMailAddress = MailServiceMultithread.BLL.CachedSettings.GetConfigurationText("ReplyMailAddress");
                    MailMessage message = new MailMessage(replyMailAddress, this._errorEmailRecipient);
                    message.IsBodyHtml = true;
                    message.Body = ConstructHtmlBody(textMessage, ex);
                    message.Subject = this._errorEmailSubject;
                    mailClient.Send(message);
                }

            }
            catch (Exception exx)
            {
                var errrormessage = string.Format("Sending error e-mail fialed with the message: {0}", exx.Message);
                eventLog1.WriteEntry(errrormessage, EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Constructs the HTML body for the error e-mail
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        private string ConstructHtmlBody(string errorMessage, Exception ex)
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendLine("<H2>Message from Digital Porto e-mail service</H2>");
            sb.AppendLine(string.Format("<p>{0}</p>", errorMessage));

            if (ex != null)
            {
                sb.AppendLine("<H2>System exception message</H2>");
                sb.AppendLine(string.Format("<p>{0}</p>", ex.Message));
                sb.AppendLine("<H2>System exception stacktrace</H2>");
                sb.AppendLine(string.Format("<p>{0}</p>", ex.StackTrace));

                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    sb.AppendLine("<H3>System innerException message</H3>");
                    sb.AppendLine(string.Format("<p>{0}</p>", ex.InnerException.Message));
                    sb.AppendLine("<H3>System innerException stacktrace</H3>");
                    sb.AppendLine(string.Format("<p>{0}</p>", ex.InnerException.StackTrace));
                }
            }
            return sb.ToString();
        }
    }
}
