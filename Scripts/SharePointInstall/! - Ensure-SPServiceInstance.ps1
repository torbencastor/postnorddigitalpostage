function global:Ensure-SPServiceInstance {
 [CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact='Low')]
 param (
  [Parameter(Mandatory=$false)]$ServiceName = $null
 )

 if (-not ($ServiceInstance = Get-SPServiceInstance | Where-Object {$_.TypeName -EQ $ServiceName})) {
  Write-Warning ('Use Start-SPServiceInstance -ServiceName <Service Instance Name>')
  Get-SPServiceInstance | Select-Object -ExpandProperty TypeName -Unique | Sort-Object
 }
 else {
  $ServiceInstance | Start-SPServiceInstance
 }
}
