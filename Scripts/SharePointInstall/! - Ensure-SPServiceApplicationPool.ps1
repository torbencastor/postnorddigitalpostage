function global:Ensure-SPServiceApplicationPool {
 Param (
  [Parameter(Mandatory=$true)][Alias('Identity')]$Name,
  [Parameter(Mandatory=$false)][AllowNull()]$Account
 )

 try {
  Get-SPServiceApplicationPool -Identity $Name -ErrorAction 'Stop'
 }
 catch {
  New-SPServiceApplicationPool -Name $Name -Account $Account
 }
}
