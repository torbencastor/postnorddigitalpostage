 
$EnvironmentName = 'Test'
$username = 'PNAZURE\TSP_Farm'
$password = '4ck0=j99K4K2nd8'
$SQLServer = 'SQL1'
$passphrase = '4ck0=j99K4K2nd8'
$superreader = 'PNAZURE\TSP_SuperRead' 
$superuser = 'PNAZURE\TSP_SuperUser'
$SMTPServer = ''
$FromAddress = 'noreply@postdanmark.dk'

write-host('- Provisioning SharePoint farm')
. '.\1 - Init-SPFarm.ps1'
Init-SPFarm -EnvironmentName $EnvironmentName -FarmAccount $username -FarmAccountPassword $password -DBServer $SQLServer -FarmPassphrase $passphrase -CAPort 9000

write-host('- Provisioning super accounts')
. '.\9 - Configure-SPSuperAccounts.ps1'
Configure-SPSuperAccounts -SuperReaderAccount $superreader -SuperUserAccount $superuser

write-host('- Provisioning outgoing email')
. '.\21 - Configure-SPMailOutgoing.ps1'
Configure-SPMailOutgoing -SMTPServ  -FromAddr