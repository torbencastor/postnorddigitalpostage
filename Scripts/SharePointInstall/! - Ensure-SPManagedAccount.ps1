function global:Ensure-SPManagedAccount {
 Param (
  [Parameter(Mandatory=$false)][AllowNull()]$Identity = $null,
  [Parameter(Mandatory=$false)][AllowNull()]$Password = $null
 )

 $ErrorActionPreference = "Stop"
 try {
  Get-SPManagedAccount $Identity
 }
 catch {
  if ($Password) {
   New-SPManagedAccount (New-Object System.Management.Automation.PSCredential $Identity, (ConvertTo-SecureString $Password -AsPlainText -Force))
  }
  else {
   New-SPManagedAccount (Get-Credential $Identity)
  }
 }
 $ErrorActionPreference = "Continue"
}
