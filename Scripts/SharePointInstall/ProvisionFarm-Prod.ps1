 
$EnvironmentName = 'Prod'
$username = 'PNAZURE\PSP_Farm'
$password = 'b1$e>."~7:g#M~+'
$SQLServer = 'SQL1'
$passphrase = 'b1$e>."~7:g#M~+'
$superreader = 'PNAZURE\PSP_SuperRead' 
$superuser = 'PNAZURE\PSP_SuperUser'
$SMTPServer = ''
$FromAddress = 'noreply@postdanmark.dk'

write-host('- Provisioning SharePoint farm')
. '.\1 - Init-SPFarm.ps1'
Init-SPFarm -EnvironmentName $EnvironmentName -FarmAccount $username -FarmAccountPassword $password -DBServer $SQLServer -FarmPassphrase $passphrase -CAPort 9000

write-host('- Provisioning super accounts')
. '.\9 - Configure-SPSuperAccounts.ps1'
Configure-SPSuperAccounts -SuperReaderAccount $superreader -SuperUserAccount $superuser

write-host('- Provisioning outgoing email')
. '.\21 - Configure-SPMailOutgoing.ps1'
Configure-SPMailOutgoing -SMTPServ  -FromAddr