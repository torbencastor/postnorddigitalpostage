function global:New-SPManagedAccountPlain {
 [CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact='Low')]
 param (
  [Parameter(Mandatory=$true)]$Account,
  [Parameter(Mandatory=$true)]$AccountPassword
 )

 New-SPManagedAccount (New-Object System.Management.Automation.PSCredential ($Account, (ConvertTo-SecureString $AccountPassword -AsPlainText -Force)))
}
