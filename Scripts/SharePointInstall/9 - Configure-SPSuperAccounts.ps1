function global:Configure-SPSuperAccounts {
 [CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact='Low')]
 param (
  [Parameter(Mandatory=$true)]$SuperReaderAccount,
  [Parameter(Mandatory=$true)]$SuperUserAccount,
  [Parameter(Mandatory=$false)][Switch]$iisreset = $true
 )

 $superreaderdisplayname = "Portal Super Reader"
 $superuserdisplayname = "Portal Super User"


 $wa = Get-SPWebApplication

 $wa | ForEach-Object {
  $wap = $_.Policies.Add($(if ($_.UseClaimsAuthentication) {"i:0#.w|"})+$SuperReaderAccount, $superreaderdisplayname)
  $wap.PolicyRoleBindings.Add(($_.PolicyRoles | Where-Object {$_.Name -EQ "Full Read"}))
  $_.Properties["portalsuperreaderaccount"] = $SuperReaderAccount
 }

 $wa | ForEach-Object {
  $wap = $_.Policies.Add($(if ($_.UseClaimsAuthentication) {"i:0#.w|"})+$SuperUserAccount, $superuserdisplayname)
  $wap.PolicyRoleBindings.Add(($_.PolicyRoles | Where-Object {$_.Name -EQ "Full Control"}))
  $_.Properties["portalsuperuseraccount"] = $SuperUserAccount
 }

 $wa | ForEach-Object {$_.Update()}

 if ($iisreset) {
  iisreset -noforce
 }
}
