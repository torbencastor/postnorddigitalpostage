function Grant-SPAccessToProcessIdentity {
 Param (
  $ApplicationPoolAccount
 )

 foreach ($identity in $ApplicationPoolAccount) {
  Get-SPWebApplication | ForEach-Object {$_.GrantAccessToProcessIdentity($identity)}
 }
}


function Grant-SPAccessToProcessIdentityCA {
 Param (
  $ApplicationPoolAccount
 )

 foreach ($identity in $ApplicationPoolAccount) {
  (Get-SPWebApplication -IncludeCentralAdministration | Where-Object {$_.IsAdministrationWebApplication}).GrantAccessToProcessIdentity($identity)
 }
}
