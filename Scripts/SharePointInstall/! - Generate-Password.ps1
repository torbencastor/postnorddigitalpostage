# Rune Mariboe
function global:Generate-Password {
 param (
  $BaseType = 'Service', #Personal, Service, (not) Custom, TrueRandom
  $PasswordLength = $null,
  $Passwords = 1,
  [AllowNull()][AllowEmptyString()]$Hide = $false #SecureString, EncryptedString, $false
#  $Key = $false #Make this!
 )

 switch ($BaseType) {
  'Personal' {$Personal = $true}
  'Service' {$Service = $true}
#  'Custom' {$Custom = $true}
  'TrueRandom' {$TrueRandom = $true}
  default {return 'BaseType must be Personal, Service, Custom or TrueRandom'}
 }

 $pws_input = $Passwords

 $ASCII_control_start = 1; #0 = EOF
 $ASCII_control_end = 30; #31 = linebreak
 $ASCII_control_chars = $null;
 for ($i=$ASCII_control_start; $i -le $ASCII_control_end; $i++) {
  $ASCII_control_chars += [char]($i);
 }

 $ASCII_lo_start = 32; #Space
 $ASCII_lo_end = 126;
 $ASCII_lo_chars = $null;
 for ($i=$ASCII_lo_start; $i -le $ASCII_lo_end; $i++) {
  $ASCII_lo_chars += [char]($i);
 }

 #127-159 control?

 $ASCII_hi_start = 160; #No-break space
 $ASCII_hi_end = 255;
 $ASCII_hi_chars = $null;
 for ($i=$ASCII_hi_start; $i -le $ASCII_hi_end; $i++) {
  $ASCII_hi_chars += [char]($i);
 }

 ### No Unicode in PowerShell! ###
 $Unicode_1_start = 256;
 $Unicode_1_end = 687; #Modifiers after this
 $Unicode_1_chars = $null;
 for ($i=$Unicode_1_start; $i -le $Unicode_1_end; $i++) {
  $Unicode_1_chars += [char]($i);
 }

 $Unicode_2_start = 688;
 $Unicode_2_end = 879; #Greek etc. after this
 $Unicode_2_chars = $null;
 for ($i=$Unicode_2_start; $i -le $Unicode_2_end; $i++) {
  $Unicode_2_chars += [char]($i);
 }

 <#
 ### Maybe later... ###
 function typeCheck() {
  document.all("acttype_personal").checked = document.all("pwlength_input").value==8 && !document.all("spec_enabled_box").checked && document.all("shift_enabled_box").checked && document.all("altside_box").checked && !document.all("acttype_trueRandom").checked;
  document.all("acttype_service").checked = document.all("pwlength_input").value -gt 11 && document.all("altgr_enabled_box").checked && document.all("shift_enabled_box").checked && !document.all("altside_box").checked && !document.all("acttype_trueRandom").checked;
  document.all("acttype_custom").checked = !document.all("acttype_personal").checked && !document.all("acttype_service").checked && !document.all("acttype_trueRandom").checked

 /* pwlength_truerandom = document.all("pwlength_input").value
  pws_truerandom = document.all("pws_input").value
  spec_truerandom = document.all("spec_enabled_box").checked
  altgr_truerandom = document.all("altgr_enabled_box").checked
  shift_truerandom = document.all("shift_enabled_box").checked
  altside_truerandom = document.all("altside_box").checked
  asciilo_truerandom = document.all("asciilo_enabled_box").checked
  asciihi_truerandom = document.all("asciihi_enabled_box").checked
  unicode1_truerandom = document.all("unicode1_enabled_box").checked*/

  document.all("pwgenerator_button").disabled = !(document.all("acttype_personal").checked || document.all("acttype_service").checked || document.all("acttype_custom").checked || (document.all("acttype_truerandom").checked && (document.all("asciilo_enabled_box").checked || document.all("asciihi_enabled_box").checked || document.all("unicode1_enabled_box").checked)))
 }
 #>

 if ($Personal) {
  $pwlength_input = if($PasswordLength) {$PasswordLength} else {8}
  $shift_enabled_box = $true
  $spec_enabled_box = $false
  $altgr_enabled_box = $false
  $altside_box = $true

  $asciilo_enabled_box = $false
  $asciihi_enabled_box = $false
  $unicode1_enabled_box = $flase

  $pwlength_custom = $pwlength_input
  $pws_custom = $pws_input
  $spec_custom = $spec_enabled_box
  $altgr_custom = $altgr_enabled_box
  $shift_custom = $shift_enabled_box
  $altside_custom = $altside_box

 # typeCheck()
 }

 if ($Service) {
  $pwlength_input = if($PasswordLength) {$PasswordLength} else {12}
  $shift_enabled_box = $true
  $spec_enabled_box = $true
  $altgr_enabled_box = $true
  $altside_box = $false

  $asciilo_enabled_box = $false
  $asciihi_enabled_box = $false
  $unicode1_enabled_box = $false

  $pwlength_custom = $pwlength_input
  $pws_custom = $pws_input
  $spec_custom = $spec_enabled_box
  $altgr_custom = $altgr_enabled_box
  $shift_custom = $shift_enabled_box
  $altside_custom = $altside_box

 # typeCheck()
 }

 if ($Custom) {
  $pwlength_input = $pwlength_custom
 # $pws_input = $pws_custom
  $spec_enabled_box = $spec_custom
  $altgr_enabled_box = $altgr_custom
  $shift_enabled_box = $shift_custom
  $altside_box = $altside_custom

  $asciilo_enabled_box = $false
  $asciihi_enabled_box = $false
  $unicode1_enabled_box = $false

 # typeCheck()
 }

 if ($TrueRandom) {
  $pwlength_input = if($PasswordLength) {$PasswordLength} else {64}
  $spec_enabled_box = $false
  $altgr_enabled_box = $false
  $shift_enabled_box = $false
  $altside_box = $false
  $asciilo_enabled_box = $true
  $asciihi_enabled_box = $true
  $unicode1_enabled_box = $true

  $pwlength_custom = $pwlength_input
  $pws_custom = $pws_input
  $spec_custom = $spec_enabled_box
  $altgr_custom = $altgr_enabled_box
  $shift_custom = $shift_enabled_box
  $altside_custom = $altside_box

 # typeCheck()
 }

 $left0 = "123456"
 $left0spec = "�"
 $left0specshift = "�!\"#�%&"
 $left0specaltgr = "@�\$�"
 $left1 = "qwert"
 $left1shift = "QWERT"
 $left1specaltgr = "�"
 $left2 = "asdfg"
 $left2shift = "ASDFG"
 $left3 = "zxcvb"
 $left3shift = "ZXCVB"
 $left3spec = "<"
 $left3specshift = ">"
 $left3specaltgr = "\\"
 $right0 = "7890"
 $right0spec = "+"
 $right0specshift = "/()=?"
 $right0specaltgr = "{[]}|"
 $right1 = "yuiop"
 $right1shift = "YUOP"; #"YUIOP"
 $right1spec = "�"
 $right1specshift = "�"
 $right2 = "hjk"; #"hjkl"
 $right2shift = "HJKL"
 $right2spec = "��'"
 $right2specshift = "��*"
 $right3 = "bnm"
 $right3shift = "BNM"
 $right3spec = ",.-"
 $right3specshift = ";:_"
 $right3specaltgr = "�"

 <#
 $space = " "

 $left1comb = "��"
 $left1combshift = "������"
 $left2comb = "��"
 $left2combshift = "������"
 $left2combaltgr = "�"
 $left2combaltgrshift = "�"

 $right1comb = "��������"
 $right1combshift = "�������������������"
 $right1combaltgr = "�"
 $right1combaltgrshift = "�"
 $right3combaltgr = "�"
 $right3combaltgrshift = "�"

 $spacecomb = "��"
 $spacecombshift = "`^"
 $spacecombaltgr = "~"
 #>

 $rows = 4

 $charSpace = $null

 $space_enabled = $false;
 $allow_doubles = $false;
 $allow_softHyphen = $false;
 $softHyphen = '�';

 $pwlength = $pwlength_input;
 $pws = $pws_input;
 $spec_enabled = $spec_enabled_box;
 $shift_enabled = $shift_enabled_box;
 $altgr_enabled = $altgr_enabled_box;
 $altside = $altside_box;
# $trueRandom = $acttype_trueRandom;
 $ASCIIlo = $asciilo_enabled_box;
 $ASCIIhi = $asciihi_enabled_box;
 $unicode1 = $unicode1_enabled_box;

 if ($TrueRandom) {
  $charSpace += if($ASCIIlo) {$ASCII_lo_chars}
  $charSpace += if($ASCIIhi) {$ASCII_hi_chars}
#  $charSpace += if($unicode1) {$Unicode_1_chars} #Unicode not supported by PowerShell
 }

 $left = $false,$true | Get-Random;

 $trapfaults = 0;

 $pwlist = $null;
 for ($j=0; $j -lt $pws; $j++) {
  $errcnt = 0;

  $captrap = -not $shift_enabled;
  $notcaptrap = -not $shift_enabled;
  $spectrap = -not $spec_enabled;
  $notspectrap = -not $spec_enabled;
  $numtrap = $false;
  $notnumtrap = $false;

  $pw = $null;
  for ($i=0; $i -lt $pwlength; $i++) {
   $ln = 0;

   if ($TrueRandom) {
    $randChar = $charSpace.ToCharArray() | Get-Random;
    if (((($randChar -eq '�' -or $randChar -eq ' ') -and (-not $space_enabled -or $i -eq 0 -or $i -eq $pwlength-1)) -or ($i -gt 0 -and $pw[$i-1] -eq $randChar -and -not $allow_doubles)) -or ($randChar -eq $softHyphen -and -not $allow_softHyphen)) {
     $i--;
     continue;
    }
    $pw += @($randChar);
    continue;
   }

   try {
    $ln++; $shift = $shift_enabled -and ($false,$true | Get-Random);
    $ln++; $spec = $spec_enabled -and ($false,$true | Get-Random);
    $ln++; $altgr = $altgr_enabled -and ($false,$true | Get-Random) -and -not $shift -and $spec;

    $ln++; $rownum = Get-Random -Maximum ($rows);
    $ln++; $rowid = if($left) {'left'} else {'right'}
    $ln++; $rowid += $rownum
    $ln++; $rowid += $(if($spec) {'spec'})
    $ln++; $rowid += $(if($shift) {'shift'})
    $ln++; $rowid += $(if($altgr) {'altgr'})
    $ln++; $row = Invoke-Expression ('$'+$rowid)
    #Write-Verbose ("row $rowid : $row")
    $ln++; $colnum = Get-Random -Maximum $row.Length;

    if ((($row[$colnum] -eq 'b' -or $row[$colnum] -eq 'B') -and ($false,$true | Get-Random)) -or ($i -gt 0 -and $pw[$i-1] -eq $row[$colnum] -and -not $allow_doubles)) {
     $i--;
     continue;
     Write-Verbose ("Skipped: $rowid[$colnum] : $row[$colnum] = " + $row[$colnum])
    }
    else {
     Write-Verbose ("$rowid[$colnum] : $row[$colnum] = " + $row[$colnum])
     $ln++; $char = $row[$colnum];
    }
   }
   catch {
    Write-Verbose ("Error: $rowid[$colnum] : $row[$colnum]")
					#$pw += @("ERROR AT $ln - $rowid[$rownum]: " + $_.Exception)
    $i--;
    $errcnt++;
    continue;
   }
   $pw += @($char);

   $left = if($altside) {-not $left} else {$false,$true | Get-Random};

   $captrap = $shift -and -not $spec -or $captrap;
   $notcaptrap = -not $shift -and -not $spec -and $rownum -gt 0 -or $notcaptrap;
   $spectrap = $spec -or $spectrap;
   $notspectrap = -not $spec -or $notspectrap;
   $numtrap = $rownum -eq 0 -and -not $spec -or $numtrap;
   $notnumtrap = $rownum -gt 0 -or $numtrap;
  }
<#
  if($captrap -and $notcaptrap -and $spectrap -and $notspectrap -and $numtrap -and $notnumtrap) {
   switch ($Encrypt) {
    'EncryptedString' {$pwlist += @(ConvertFrom-SecureString -SecureString (ConvertTo-SecureString -String ($pw -join '') -AsPlainText -Force))}
    'SecureString'    {$pwlist +=                                         @(ConvertTo-SecureString -String ($pw -join '') -AsPlainText -Force) }
    default           {$pwlist +=                                                                         @($pw -join '')                      }
   }
  }
  else {
   $pwlist += @($pw -join '' + ' - DISCARDED!`n');
   $pws++;
   $trapfaults++;
   #Write-Verbose ($trapfaults + ' : ' + ($pws-$trapfaults));
   continue;
  }
#>
  if($captrap -and $notcaptrap -and $spectrap -and $notspectrap -and $numtrap -and $notnumtrap -or $trueRandom) {
   switch ($Hide) {
    'EncryptedString' {$pwlist += @(ConvertFrom-SecureString -SecureString (ConvertTo-SecureString -String ($pw -join '') -AsPlainText -Force))}
    'SecureString'    {$pwlist +=                                         @(ConvertTo-SecureString -String ($pw -join '') -AsPlainText -Force) }
    default           {$pwlist +=                                                                         @($pw -join '')                      }
   }
  }
  else {
   $j--
  }
 }

 $pwlist
}


function global:Generate-Key {
 param (
  $FromASCII = $false, # 'abcdefghijklmnopqrstuvwx'
  $RandomKeyLength = 256
 )
 $AcceptedKeyLengths = 128,192,256
 $KeyLength = $FromASCII.Length * 8

 $Key = $null

 if ($FromASCII) {
  if ($AcceptedKeyLengths.Contains($KeyLength)) {
   foreach ($char in $FromASCII.ToCharArray()) {
    $Key += @([char]::ConvertToUtf32("$char",0))
   }
  }
  else {
   Write-Error "Key length must be 128, 192 or 256 bits - input key is $KeyLength bits long."
  }
 }
 elseif ($AcceptedKeyLengths.Contains($RandomKeyLength)) {
  for($i=0; $i -lt $RandomKeyLength/8; $i++) {
   $Key += @(Get-Random -Minimum ([int][byte]::MinValue) -Maximum ([int][byte]::MaxValue+1))
#   $Key += @([char]::ConvertFromUtf32((Get-Random -Minimum ([int][byte]::MinValue) -Maximum ([int][byte]::MaxValue+1))))
  }
 }
 else {
  Write-Error "Key length must be 128, 192 or 256 bits."
 }
 $Key
}

#$Key = Generate-Key

function global:Encrypt-String {
 param (
  [parameter(Mandatory=$true, ValueFromPipeline=$true)]$String,
  [parameter(Mandatory=$false, ValueFromPipeline=$false)]$Key = $false
 )

 if ($String.GetType() -ne [securestring]) {
  $String = ConvertTo-SecureString -String $String -AsPlainText -Force
 }
 if($Key) {
  if($Key.GetType() -ne [securestring]) {
   ConvertFrom-SecureString -SecureString $String -Key $Key
  }
  else {
   ConvertFrom-SecureString -SecureString $String -SecureKey $Key
  }
 }
 else {
  ConvertFrom-SecureString -SecureString $String 
 }
}

#$String = 'Test!Hest'
#$EncryptedString = Encrypt-String -String $String -Key $Key

function global:Decrypt-String {
 param (
  [parameter(Mandatory=$true, ValueFromPipeline=$true)]$EncryptedString,
  [parameter(Mandatory=$false, ValueFromPipeline=$false)]$Key = $false
 )

 if ($String.GetType() -ne [securestring]) {
  $String = ConvertTo-SecureString -String $String -AsPlainText -Force
 }
 if($Key) {
  if($Key.GetType() -ne [securestring]) {
   [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((ConvertTo-SecureString -String $EncryptedString -Key $Key)))
#   (New-Object System.Management.Automation.PSCredential 'TempUsr', (ConvertTo-SecureString -String $EncryptedString -Key $Key)).GetNetworkCredential().Password
  }
  else {
   [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((ConvertTo-SecureString -String $EncryptedString -SecureKey $Key)))
#   (New-Object System.Management.Automation.PSCredential 'TempUsr', (ConvertTo-SecureString -String $EncryptedString -SecureKey $Key)).GetNetworkCredential().Password
  }
 }
 else {
  [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((ConvertTo-SecureString -String $EncryptedString)))
#  (New-Object System.Management.Automation.PSCredential 'TempUsr', (ConvertTo-SecureString -String $EncryptedString)).GetNetworkCredential().Password
 }
}

#Decrypt-String -EncryptedString $EncryptedString -Key $Key


<#
$Key = Generate-Key -FromASCII 'asdfasdfasdfasdf'
$KeyString = $null
foreach ($byte in $Key) {
 $KeyString += [char]::ConvertFromUtf32($byte)
}
$SecKey = ConvertTo-SecureString -String $KeyString -AsPlainText -Force
ConvertFrom-SecureString -SecureString $String $(if($SecKey) {$SecKey})
#>


<#
/*
123456	7890
qwert	yuiop
 asdfg	 hjkl
 zxcvb	 bnm
*/
</script>
</head>

<body onLoad="typeCheck()">
<table width="100%"><tr valign="top"><td width="33%">
PW length: <input type="text" id="pwlength_input" value="8" onKeyUp="typeCheck()"><br>
Number of PWs: <input type="text" id="pws_input" value="50"><br>
Special characters <input type="checkbox" id="spec_enabled_box" onClick="if(!this.checked) {document.all('altgr_enabled_box').checked=false};typeCheck()"><br>
Enable Shift <input type="checkbox" id="shift_enabled_box" onClick="typeCheck()" checked><br>
Enable AltGr <input type="checkbox" id="altgr_enabled_box" onClick="if(this.checked) {document.all('spec_enabled_box').checked=true};typeCheck()"><br>
Alternating left/right <input type="checkbox" id="altside_box" onClick="typeCheck()" checked><br>
Account type:<br>
Personal <input type="radio" name="acttype" id="acttype_personal" onClick="personalSettings()" checked><br>
Service <input type="radio" name="acttype" id="acttype_service" onClick="serviceSettings()"><br>
Custom <input type="radio" name="acttype" id="acttype_custom" onClick="customSettings()"><br><br>
True random <input type="radio" name="acttype" id="acttype_truerandom" onClick="trueRandomSettings()"><br>
&nbsp;ASCII low (032-126) <input type="checkbox" id="asciilo_enabled_box" onClick="typeCheck()" checked disabled><br>
&nbsp;ASCII high (160-255) <input type="checkbox" id="asciihi_enabled_box" onClick="typeCheck()" checked disabled><br>
&nbsp;Unicode Latin (256-687) <input type="checkbox" id="unicode1_enabled_box" onClick="typeCheck()" disabled><br>
<br><input type="button" value="Generate PWs" id="pwgenerator_button" onClick="PWgenerator()"><br><br>
</td>
<td align="center" width="34%"><code id="pwout" style="position: relative; top: 90px; font-family: courier new; font-size: 13"></code></center></td>
<td width="33%"></td>
</tr></table>
</body>
</html>
#>

