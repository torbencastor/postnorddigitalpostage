function global:Init-SPFarm {
 [CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact='Low')]
 param (
  [Parameter(Mandatory=$false)]$EnvironmentName = $env:ComputerName,
  [Parameter(Mandatory=$true)]$FarmAccount,
  [Parameter(Mandatory=$true)]$FarmAccountPassword,
  [Parameter(Mandatory=$true)]$DBServer,
  [Parameter(Mandatory=$false)]$FarmPassphrase = 'FarmPassphrase!123',
  [Parameter(Mandatory=$false)]$CAPort = 8000,
  [Parameter(Mandatory=$false)]$CAKerberos = $false
 )

 $ConfigDB = $EnvironmentName + '_SharePoint_config'
 $ContentDB = $EnvironmentName + '_SharePoint_content_CentralAdministration'
 $FarmPassphrase = ConvertTo-SecureString $FarmPassphrase -asplaintext -force
 $FarmAdminCredentials = New-Object System.Management.Automation.PSCredential $FarmAccount, (ConvertTo-SecureString $FarmAccountPassword -asplaintext -force)

 New-SPConfigurationDatabase -DatabaseServer $DBServer -DatabaseName $ConfigDB -Passphrase $FarmPassphrase -FarmCredentials $FarmAdminCredentials -AdministrationContentDatabaseName $ContentDB
 Install-SPHelpCollection -All
 Initialize-SPResourceSecurity
 Install-SPService
 Install-SPFeature -AllExistingFeatures
 New-SPCentralAdministration -Port $CAPort -WindowsAuthProvider $(if ($CAKerberos) {'Kerberos'} else {'NTLM'})
 Install-SPApplicationContent

 &'C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\15\BIN\psconfigui.exe'
}