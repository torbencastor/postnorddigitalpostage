function global:Configure-SPMailOutgoing {
 [CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact='Low')]
 param (
  [Parameter(Mandatory=$true)]$SMTPSrv,
  [Parameter(Mandatory=$true)]$FromAddr,
  [Parameter(Mandatory=$false)]$ReplyAddr = $false,
  [Parameter(Mandatory=$false)]$Charset = 65001
 )

 if (-not $ReplyAddr) {
  $ReplyAddr = $FromAddr
 }

 $CAWebApp = Get-SPWebApplication -IncludeCentralAdministration | Where-Object {$_.IsAdministrationWebApplication}
 $CAWebApp.UpdateMailSettings($SMTPSrv, $FromAddr, $ReplyAddr, $Charset)

 $s = [Microsoft.SharePoint.Administration.SPServer]::Local
 $si = $s.ServiceInstances | Where-Object {$_.TypeName -eq "Microsoft SharePoint Foundation Incoming E-Mail"}
 $si.ServerDropFolder
}
