﻿function EnsureStorageAccount
{
    param([string]$storageAccountName)

    if($storageAccountName -eq "")
    {
        while($true)
        {
            $storageAccountName = "storage" + (randomString)
            if((Test-AzureName -Storage $storageAccountName) -eq $true)
            {
                Write-Host "Dynamically generated $storageAccountName is in use. Looking for another."
            }
            else
            {
                Write-Host "Creating new storage account $storageAccountName in $location"
                try
                {
                    New-AzureStorageAccount -StorageAccountName $storageAccountName -Location $location
                }
                catch
                {
                    return
                }
                break
            }
        }
    }
    else
    {
        if((Test-AzureName -Storage $storageAccountName) -eq $true)
        {
            Write-Host "Using Storage Account $storageAccountName."
        }
        else
        {
            Write-Host "Creating new storage account $storageAccountName in $location"
            try
            {
                New-AzureStorageAccount -StorageAccountName $storageAccountName -Location $location
            }
            catch
            {
                return
            }
            Write-Host "Using Storage Account $storageAccountName."
        }
    }
    return $storageAccountName
}

function EnsureSubscription
{
    param ($subscriptionName)

    if($subscriptionName -ne "")
    {
        $subscription = Get-AzureSubscription -SubscriptionName $subscriptionName
    }
    else
    {
        $subscription = Get-AzureSubscription -Current
    }

    if($subscription -eq $null)
    {
        Write-Host "Windows Azure Subscription is not configured or the specified subscription name is invalid." -f Red
        Write-Host "Use Get-AzurePublishSettingsFile and Import-AzurePublishSettingsFile first" -f Red
        return
    }

    return $subscription
}

function EnsurePassword
{
    param($adminPassword)

    if($adminPassword -eq "")
    {
        $adminPassword = (randomString -length 10) + "0!"
        Write-Host "Generated Service Password"
    }

    return $adminPassword
}

function EnsureVMName
{
    Param($VMName)

    if($VMName -eq '')
    {
        $VMName = 'VM'
    }

    if((Get-AzureVM | where{$_.Name.tolower() -eq $VMName.ToLower()}) -ne $null)
    {
        $OriVMName = $VMName
        $i = 0
        while($true)
        {
            $i++
            $VMName = $OriVMName + $i.ToString()
            if((Get-AzureVM | where{$_.Name.tolower() -eq $VMName.ToLower()}) -eq $null)
            {
                break
            }
        }
    }
     Write-Host('Using VMName '+$VMName)
    return $VMName
}

function EnsureVNetName
{
    param($VNetName)

    if($VNetName -eq '')
    {
        $VNetName = 'VN' + (randomString).toString()
    }

    return $VNetName
}

function EnsureAffinityGroup
{
    param($AffinityGrp)

    if($AffinityGrp -eq '')
    {
        $AffinityGrp = 'AffGrp' + (randomString).toString()
    }

    return $AffinityGrp
}

function randomString
{
    param($length = 6)

    $digits = 48..57
    $letters = 65..90 + 97..122
    $rstring = get-random -count $length `
            -input ($digits + $letters) |
                    % -begin { $aa = $null } `
                    -process {$aa += [char]$_} `
                    -end {$aa}
    return $rstring.ToString().ToLower()
}