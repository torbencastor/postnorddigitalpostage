<#
 * Copyright Microsoft Corporation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
#>

param(
	$subscriptionFilePath,
	$subscriptionName,
	$storageAccount,
	$dcLocation,
	$affinityGroupName,	
	$vmName, 
	$serviceName, 
	$availabilitySet,
	$size,
	$imageName,	
	$subnetNames,
	$password,
	$adminUserName,	
	$dataDisks,			
	$vnetName,	
    $netBiosDomainName,	
	$dcInstallMode, 	
	$dnsDomain,
    $createVNET
)


# Include script file for shared functions
$scriptFolder = Split-Path -parent $MyInvocation.MyCommand.Definition
. "$scriptFolder\..\SharedComponents\SharedFunctions.ps1"


################## Functions ##############################

function CreateVNet()
{
	#Get the NetworkConfig.xml path
	$vnetConfigPath = (Join-Path -Path $scriptFolder -ChildPath "..\Config\AD-VNET\NetworkConfig.xml")
    $vnetConfigPath = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($vnetConfigPath)
	
	#Get the CreateVnet.ps1 script path
	$vnetScriptPath = (Join-Path -Path $scriptFolder -ChildPath "CreateVNet.ps1")
    $vnetScriptPath = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($vnetScriptPath)
	
	#Populate the argument list
	$createVNetArgumentList = @()
	$createVNetArgumentList += ("-DCLocation", '$dcLocation')
	$createVNetArgumentList += ("-AffinityGroupName", $affinityGroupName)
    $createVNetArgumentList += ("-VNetConfigPath",'$vnetConfigPath')
	
	Write-Host $vnetConfigPath
    
	#Invoke the CreateVNet script with arguments
    Invoke-Expression ".'$vnetScriptPath' $createVNetArgumentList"    
}

function CreateRemotePSEnabledVM
{
	#Get the CreateRPSEnabledVM.ps1 script path
	$RPSVmScriptPath = (Join-Path -Path $scriptFolder -ChildPath "CreateRPSEnabledVM.ps1")
    $RPSVmScriptPath = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($RPSVmScriptPath)
	
	
	#fill in the argument list
	$createVMArgumentList = @()
	$createVMArgumentList += ("-VMName", $vmName)
	$createVMArgumentList += ("-ImageName", '$imageName')
    $createVMArgumentList += ("-Size",$size)
	$createVMArgumentList += ("-SubnetNames",$subnetNames)
	$createVMArgumentList += ("-AdminUserName",$adminUserName)
	$createVMArgumentList += ("-Password",'$password')
	$createVMArgumentList += ("-ServiceName",$serviceName)	
	$createVMArgumentList += ("-AvailabilitySet",$availabilitySet)
	$createVMArgumentList += ("-VNetName",$vnetName)
	$createVMArgumentList += ("-DiskLabelPrefix",$diskLabelPrefix)
	$createVMArgumentList += ("-AffinityGroup",$affinityGroupName)
	$createVMArgumentList += ("-DomainJoin",$dnsDomain)
	$createVMArgumentList += ("-Domain",$netBiosDomainName)
	$createVMArgumentList += ("-DataDisks",'(,$dataDisks)')
	
	#Invoke the CreateRPSEnabledVM script with arguments
    Invoke-Expression ". '$RPSVmScriptPath' $createVMArgumentList"
}

function FormatDisk
{
	#Get the FormatDisk.ps1 script path
	$FormatDiskScriptPath = (Join-Path -Path $scriptFolder -ChildPath "FormatDisk.ps1")
    $FormatDiskScriptPath = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($FormatDiskScriptPath)
	
	#fill in the argument list
	$formatDiskargumentList = @()
	$formatDiskargumentList += ("-VMName",$vmName)
	$formatDiskargumentList += ("-ServiceName",$serviceName)
	$formatDiskargumentList += ("-AdminUserName",$adminUserName)
	$formatDiskargumentList += ("-Password",'$password')
	
	#Invoke Format disk with arguments
	Invoke-Expression ". '$FormatDiskScriptPath' $formatDiskargumentList"
}

################## Functions ##############################

################## Script execution begin ###########

Import-Module "C:\Program Files (x86)\Microsoft SDKs\Windows Azure\PowerShell\Azure\Azure.psd1" -Verbose:$false
Select-AzureSubscription -SubscriptionName $subscriptionName -verbose
Set-AzureSubscription -SubscriptionName $subscriptionName -CurrentStorageAccount $storageAccount -verbose

$scriptFolder = Split-Path -parent $MyInvocation.MyCommand.Definition
$secPassword = ConvertTo-SecureString $password -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential($adminUserName, $secPassword)
$domainCredential = New-Object System.Management.Automation.PSCredential("$netBiosDomainName\$adminUserName", $secPassword)

if(($createVNET -eq $true) -and ($dcInstallMode -eq "NewForest"))
{
	CreateVNet
}

CreateRemotePSEnabledVM

FormatDisk

################## Script execution end ##############