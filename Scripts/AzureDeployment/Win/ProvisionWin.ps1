<#
 * Copyright Microsoft Corporation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
#>

param([parameter(Mandatory=$true)][string]$configFilePath)

Import-Module "C:\Program Files (x86)\Microsoft SDKs\Windows Azure\PowerShell\Azure\Azure.psd1" -Verbose:$false

$scriptFolder = Split-Path -Parent (Split-Path -parent $MyInvocation.MyCommand.Definition)
. "$scriptFolder\SharedComponents\SharedFunctions.ps1"


Use-RunAs
Write-Host "Installing Windows Server using Configuration Template: $configFilePath"

$config = [xml](gc $configFilePath)

$winScriptPath = (Join-Path -Path $scriptFolder -ChildPath 'Win\Win-Deploy.ps1')


# Provision VMs in each VM Group
foreach($VMRole in $config.Azure.AzureVMGroups.VMRole)
{
	$dataDisks = @()
	foreach($dataDiskEntry in $VMRole.DataDiskSizesInGB.Split(';'))
	{
		$dataDisks += @($dataDiskEntry)
	}
				
	if($VMRole.Name -eq 'WindowsServer')
	{		
		foreach($azureVm in $VMRole.AzureVM)
		{			
			
						
		$password = GetPasswordByUserName $VMRole.ServiceAccountName $config.Azure.ServiceAccounts.ServiceAccount
	
			$createWinArgumentList = @()
			$createWinArgumentList += ("-SubscriptionName", '$config.Azure.SubscriptionName')
			$createWinArgumentList += ("-StorageAccount", $config.Azure.StorageAccount)
			$createWinArgumentList += ("-DCLocation", '$config.Azure.Location')
			$createWinArgumentList += ("-AffinityGroupName", '$config.Azure.AffinityGroup')
			$createWinArgumentList += ("-VMName", '$azureVm.Name')
			$createWinArgumentList += ("-ServiceName", '$config.Azure.ServiceName')
			$createWinArgumentList += ("-AvailabilitySet", '$VMRole.AvailabilitySet')
			$createWinArgumentList += ("-Size", '$VMRole.VMSize')
			$createWinArgumentList += ("-ImageName", '$VMRole.StartingImageName ')
			$createWinArgumentList += ("-SubnetNames", '$VMRole.SubnetNames')
			$createWinArgumentList += ("-Password", '$password')
			$createWinArgumentList += ("-AdminUserName", '$VMRole.ServiceAccountName')			
			$createWinArgumentList += ("-VNetName", '$config.Azure.VNetName')
			$createWinArgumentList += ("-NetBiosDomainName", '$config.Azure.ActiveDirectory.Domain')	
			$createWinArgumentList += ("-DCInstallMode", '$azureVm.DCType')						
			$createWinArgumentList += ("-DnsDomain", '$config.Azure.ActiveDirectory.DnsDomain')
			$createWinArgumentList += ("-DataDisks", '(,$dataDisks)')
            $createWinArgumentList += ("-createVNET", '$config.Azure.AzureVNET.CreateVNET')
			
			Invoke-Expression ".'$winScriptPath' $createWinArgumentList"    		             
		}
	}	
}

