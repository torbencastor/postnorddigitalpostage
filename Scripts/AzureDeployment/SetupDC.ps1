﻿
Function AutoConfigureDC
{
    # If serviceName, storageAccount or servicePassword is specified do not automatically create
    param([parameter(Mandatory=$true)][string]$TemplateName, 
          [parameter(Mandatory=$true)][string]$Location, 
          [parameter(Mandatory=$true)][string]$ScriptFolder, 
          [parameter(Mandatory=$false)][string]$subscriptionName="", 
          [parameter(Mandatory=$false)][string]$serviceName="",   
          [parameter(Mandatory=$false)][string]$storageAccountName ="",
          [parameter(Mandatory=$false)][string]$adminAccount="spadmin",   
          [parameter(Mandatory=$false)][string]$adminPassword="",     
          [parameter(Mandatory=$false)][string]$Domain="AzurePNtest",
          [parameter(Mandatory=$false)][string]$DnsDomain="azurepntest.postnord.dk",
          [parameter(Mandatory=$false)][string]$VMName="",
          [parameter(Mandatory=$false)][string]$VNetName="",
          [parameter(Mandatory=$false)][string]$AffinityGroup="",
          [parameter(Mandatory=$false)][string]$configOnly=$false,
          [parameter(Mandatory=$false)][string]$doNotShowCreds=$false
          )
    . .\EnsureFunctions.ps1

    $storageAccountName = EnsureStorageAccount $storageAccountName
    $subscription = EnsureSubscription $subscriptionName
    Select-AzureSubscription $subscription.SubscriptionName

    $adminPassword = EnsurePassword $adminPassword
    $VMName = EnsureVMName $VMName
    $VNetName = EnsureVNetName $VNetName
    $AffinityGroup = EnsureAffinityGroup $AffinityGroup

    $ad = "$scriptFolder\AD\ProvisionAD-PN.ps1"
    $adConfig = "$scriptFolder\Config\$templateName\AD-Sample-PN.xml"

    Write-Host "Setting AD Configuration File"
    $autoAdConfig = SetADConfiguration -configPath $adConfig -serviceName $serviceName -storageAccount $storageAccountName -subscription $subscription.SubscriptionName -adminAccount $adminAccount -password $adminPassword -domain $domain -dnsDomain $dnsDomain -VMName $VMName -VNetName $VNetName -AffinityGroup $AffinityGroup
    
    Write-Host "Installing Active Directory" 
    & $ad -configFilePath $autoAdConfig
    
}


function SetADConfiguration 
{
    param($configPath,$serviceName,$storageAccount,$subscription, $adminAccount, $password, $domain, $dnsDomain, $VMName, $VNetName, $AffinityGroup)

    $w2k12img = (GetLatestImage "Windows Server 2012 Datacenter")
    $configPathAutoGen = $configPath.Replace(".xml", "-AutoGen.xml")

    [xml] $config = gc $configPath
    $config.Azure.SubscriptionName = $subscription 
    $config.Azure.ServiceName = $serviceName    
    $config.Azure.StorageAccount = $storageAccount
    $config.Azure.Location = $location 
    $config.Azure.VNetName = $VNetName
    $config.Azure.AffinityGroup = $AffinityGroup
    $config.Azure.AzureVMGroups.VMRole.StartingImageName = $w2k12img
    $config.Azure.AzureVMGroups.VMRole.ServiceAccountName = $adminAccount
    $config.Azure.AzureVMGroups.VMRole.AzureVM.Name = $VMName
    $config.Azure.ActiveDirectory.Domain = $domain
    $config.Azure.ActiveDirectory.DnsDomain = $dnsDomain
    
    foreach($serviceAccount in $config.Azure.ServiceAccounts.ServiceAccount)
    {
        $serviceAccount.UserName = $adminAccount
        $serviceAccount.Password = $password
    }
    $config.Save($configPathAutoGen)
    return $configPathAutoGen
}

function GetLatestImage
{
   param($imageFamily)
   $images = Get-AzureVMImage | where { $_.ImageFamily -eq $imageFamily } | Sort-Object -Descending -Property PublishedDate;
   return ($images[0].ImageName)
}



$scriptFolder = Split-Path -Parent $MyInvocation.MyCommand.Definition
. "$scriptFolder\SharedComponents\sharedfunctions.ps1"
#. "$scriptFolder\SharedComponents\autoconfigure.ps1"

if((IsAdmin) -eq $false)
{
    Write-Host "Must run PowerShell elevated." -F Red
    return
}

$d = get-date
Write-Host "Starting DC Deployment $d"

AutoConfigureDC -TemplateName "SingleVMs" -Location "North Europe" -ScriptFolder $scriptFolder -storageAccountName "pnstoragetest" -serviceName 'pn-spservice-test' -adminPassword 'P@ssword' -adminAccount 'PNAdmin' -VMName 'PN-DC' -VNetName 'PN-VirtualNet' -AffinityGroup 'PN-AffinityGroup'

$d = get-date
Write-Host "End DC Deployment $d"
