﻿function AutoConfigureSQL
{
    param([parameter(Mandatory=$true)][string]$TemplateName, 
          [parameter(Mandatory=$true)][string]$Location, 
          [parameter(Mandatory=$true)][string]$ScriptFolder, 
          [parameter(Mandatory=$true)][string]$DomainController,
          [parameter(Mandatory=$false)][string]$subscriptionName="", 
          [parameter(Mandatory=$false)][string]$serviceName="",   
          [parameter(Mandatory=$false)][string]$storageAccountName ="",
          [parameter(Mandatory=$false)][string]$adminAccount="spadmin",   
          [parameter(Mandatory=$false)][string]$adminPassword="",     
          [parameter(Mandatory=$false)][string]$Domain="AzurePNtest",
          [parameter(Mandatory=$false)][string]$DnsDomain="azurepntest.postnord.dk",
          [parameter(Mandatory=$false)][string]$VMName="",
          [parameter(Mandatory=$false)][string]$VNetName="",
          [parameter(Mandatory=$false)][string]$AffinityGroup="",
          [parameter(Mandatory=$false)][string]$configOnly=$false,
          [parameter(Mandatory=$false)][string]$doNotShowCreds=$false

          )
    . .\EnsureFunctions.ps1
    
    $storageAccountName = EnsureStorageAccount $storageAccountName
    $subscription = EnsureSubscription $subscriptionName
    $adminPassword = EnsurePassword $adminPassword
    $VMName = EnsureVMName $VMName
    $VNetName = EnsureVNetName $VNetName
    $AffinityGroup = EnsureAffinityGroup $AffinityGroup

    $sql = "$scriptFolder\SQL\ProvisionSQL-PN.ps1"
    $sqlConfig = "$scriptFolder\Config\$templateName\SQL-Sample-PN.xml"

    Write-Host "Setting SQL Configuration File"
    $autoSqlConfig = SetSqlConfiguration -configPath $sqlConfig -serviceName $serviceName -storageAccount $storageAccountName -subscription $subscription.SubscriptionName -adminAccount $adminAccount -password $adminPassword -domain $domain -dnsDomain $dnsDomain -VNetName $VNetName -AffinityGroup $AffinityGroup -DomainController $DomainController -VMName $VMName
    
    Write-Host "Installing SQL Server 2012"
    & $sql -configFilePath $autoSqlConfig
}

function SetSQLConfiguration
{
    param($configPath,$serviceName,$storageAccount,$subscription, $adminAccount, $password, $domain, $dnsDomain, $VNetName, $AffinityGroup, $DomainController, $VMName)

    $sql2k12img = (GetLatestImage "SQL Server 2012 SP1 Enterprise on Windows Server 2008 R2")
    $configPathAutoGen = $configPath.Replace(".xml", "-AutoGen.xml")
    [xml] $config = gc $configPath
    $config.Azure.SubscriptionName = $subscription 
    $config.Azure.ServiceName = $serviceName    
    $config.Azure.StorageAccount = $storageAccount
    $config.Azure.VNetName = $VNetName
    $config.Azure.AffinityGroup = $AffinityGroup
    $config.Azure.Connections.ActiveDirectory.ServiceName = $serviceName
    $config.Azure.Connections.ActiveDirectory.Domain = $domain
    $config.Azure.Connections.ActiveDirectory.DnsDomain = $dnsDomain
    $config.Azure.Connections.ActiveDirectory.DomainControllerVM = $DomainController
    $config.Azure.Connections.ActiveDirectory.ServiceAccountName = "$domain\$adminAccount"
    $config.Azure.AzureVMGroups.VMRole.ServiceAccountName = "$adminAccount"
    $config.Azure.AzureVMGroups.VMRole.StartingImageName = $sql2k12img
    $config.Azure.AzureVMGroups.VMRole.AzureVM.Name = $VMName

    if($config.Azure.AzureVMGroups.VMRole.QuorumStartingImageName -ne $null)
    {
        $config.Azure.AzureVMGroups.VMRole.QuorumStartingImageName = (GetLatestImage "Windows Server 2008 R2 SP1")
    } 
    foreach($serviceAccount in $config.Azure.ServiceAccounts.ServiceAccount)
    {
        if(($serviceAccount.Type -eq "WindowsLocal") -or ($serviceAccount.Type -eq "SQL"))
        {
           $serviceAccount.UserName = $adminAccount
        }
        else #domain account
        {
          $serviceAccount.UserName = "$domain\$adminAccount"
        }
        $serviceAccount.Password = $password
    }

    $config.Azure.SQLCluster.InstallerDomainUsername = "$domain\$adminAccount"
    $config.Azure.SQLCluster.InstallerDatabaseUserName = $adminAccount

    if($config.Azure.AzureVMGroups.VMRole.HighAvailabilityType -ne $null)
    {
        $config.Azure.SQLCluster.PrimaryServiceAccountName = "$domain\$adminAccount"
        $config.Azure.SQLCluster.SecondaryServiceAccountName = "$domain\$adminAccount"
    }
    $config.Save($configPathAutoGen)
    return $configPathAutoGen
}

function GetLatestImage
{
   param($imageFamily)
   $images = Get-AzureVMImage | where { $_.ImageFamily -eq $imageFamily } | Sort-Object -Descending -Property PublishedDate;
   return ($images[0].ImageName)
}

$scriptFolder = Split-Path -Parent $MyInvocation.MyCommand.Definition
. "$scriptFolder\SharedComponents\sharedfunctions.ps1"
#. "$scriptFolder\SharedComponents\autoconfigure.ps1"

if((IsAdmin) -eq $false)
{
    Write-Host "Must run PowerShell elevated." -F Red
    return
}

$d = get-date
Write-Host "Starting SQL Deployment $d"

AutoConfigureSQL -TemplateName "SingleVMs" -Location "North Europe" -ScriptFolder $scriptFolder -storageAccountName "pnstoragetest" -serviceName 'pn-spservice-test' -adminPassword 'P@ssword' -adminAccount 'PNAdmin' -VMName 'PN-SQL' -VNetName 'PN-VirtualNet' -AffinityGroup 'PN-AffinityGroup' -DomainController 'PN-DC1'

$d = get-date
Write-Host "End SQL Deployment $d"