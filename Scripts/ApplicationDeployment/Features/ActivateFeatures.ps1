# Ensuring the existense of SharePoint snapin
If ((Get-PsSnapin |?{$_.Name -eq "Microsoft.SharePoint.PowerShell"})-eq $null)
{
   	Write-Host(" - Loading SharePoint Powershell Snapin")
  	$PSSnapin = Add-PsSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue | Out-Null
}
				
function ActivateFeature
{
	param($name,$featureGuid,$url)
	
	Write-Host(" feature : "+ $name + ", at URL: "+$url)
	Enable-SPFeature -id $featureGuid -URL $url
}
try{
pushd (Split-Path -Parent $MyInvocation.MyCommand.Definition)
. ..\00_Settings.ps1

Write-Host("## Activating features ##")

#$webUrl = 'http://url'
#$name = "FeatureName"
#$featureGuid = 'FeatureGuid'
#ActivateFeature $name $featureGuid $webUrl

$webUrl = $fullWebAppUrl
$name = "Digital Postage - WebParts"
$featureGuid = '63e6a15e-726d-41b7-b84f-d9c34f916905'
ActivateFeature $name $featureGuid $webUrl

$webUrl =  $fullWebAppUrl
$name = "Digital Postage - Create publishingPages"
$featureGuid = '86ff8b42-f662-4e64-b6be-23a08473eff5'
ActivateFeature $name $featureGuid $webUrl


$webUrl =  $fullWebAppUrl + '/admin'
$name = "Digital Postage - Create Lists"
$featureGuid = '15211e5a-8cee-460c-b797-bc7fbd6a6c01'
ActivateFeature $name $featureGuid $webUrl
}
Finally{
popd
}
Write-Host("")
Write-Host("## Done activating features ##")