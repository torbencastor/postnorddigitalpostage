# Web application settings
$environment = 'Prod'
$serviceName = 'pn-digitalpostage-service.cloudapp.net'
$AppPoolAccount = "pnazure\PSP_DPAppPool"
$Port = 80
$webTemplate= 'CMSPUBLISHING#0'
$WebAppName = $environment+'_'+$serviceName
$AppPoolName = $WebAppName+"_AppPool"
$HostHeader = $serviceName
$Url = 'http://'+$serviceName
$fullWebAppUrl = $webAppUrl +':'+$Port
$DBName = $environment+"_SP2010_ Content_"+$WebAppName

# Site collection and web settings
$sites = @(@{isSiteCollection=$true;SiteName='Digital Porto';Owner='pnazure\testsiteadmin';Url=$fullWebAppUrl;Template=$webTemplate;Language=1033},
           @{SiteName='Administration';Url=$fullWebAppUrl+'/admin';Template=$webTemplate;Language=1033},
           @{SiteName='Raportering';Url=$fullWebAppUrl+'/raportering';Template=$webTemplate;Language=1033},
           @{SiteName='Produktion';Url=$fullWebAppUrl+'/produktion';Template=$webTemplate;Language=1033},
           @{SiteName='Kunde Center';Url=$fullWebAppUrl+'/kundecenter';Template=$webTemplate;Language=1033})