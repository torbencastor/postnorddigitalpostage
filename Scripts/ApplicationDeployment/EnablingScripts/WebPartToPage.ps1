﻿param (
    [string]$webUrl = $toolsUrl,
    [string]$configXmlPath = '.\WebPartToPageConfig.xml'
 )

If ((Get-PsSnapin |?{$_.Name -eq "Microsoft.SharePoint.PowerShell"})-eq $null)
{
   	Write-Host(" - Loading SharePoint Powershell Snapin")
  	$PSSnapin = Add-PsSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue | Out-Null
}
 
function AddWebPartToPage([string]$siteUrl,[string]$pageRelativeUrl,[string]$localWebpartPath,[string]$ZoneName,[int]$ZoneIndex, [string]$wpTitle)
{
    try
    {
        #this reference is required here
        $clientContext= [Microsoft.SharePoint.Client.ClientContext,Microsoft.SharePoint.Client, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c]
        $context=New-Object Microsoft.SharePoint.Client.ClientContext($siteUrl)
        write-host "  - Reading file " $pageRelativeUrl
        $oFile = $context.Web.GetFileByServerRelativeUrl($pageRelativeUrl);
        $limitedWebPartManager = $oFile.GetLimitedWebPartManager([Microsoft.Sharepoint.Client.WebParts.PersonalizationScope]::Shared);
        
        $xtr = New-Object System.Xml.XmlTextReader($localWebpartPath)
         [void] [Reflection.Assembly]::LoadWithPartialName("System.Text")
        $sb = new-object System.Text.StringBuilder
 
        while ($xtr.Read())
        {
            $tmpObj = $sb.AppendLine($xtr.ReadOuterXml());
        }
        $newXml =  $sb.ToString()
 
        if ($xtr -ne $null)
        {
            $xtr.Close()
        }
 
        #Add Web Part to catalogs folder
        $oWebPartDefinition = $limitedWebPartManager.ImportWebPart($newXml);
        write-host("  - Adding Webpart "+$wpTitle+" ...") 
        $limitedWebPartManager.AddWebPart($oWebPartDefinition.WebPart, $ZoneName, $ZoneIndex) | Out-Null;
        $context.ExecuteQuery() | Out-Null
        write-host "   - Adding Web Part Done"
    }
    catch
    {
        write-host("   - Error while 'AddWebPartToPage': "+$_.exception) -ForegroundColor red
    }
 
}
 
#Checks out the page
function CheckOutPage ($SPFile)
{
    $x=$SPFile.ServerRelativeUrl
    if($SPFile.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
    {
        write-host " -  File already checked-out...doing undo checkout"
 
        $SPFile.UndoCheckOut()
    }
 
    if ($SPFile.Level -ne [Microsoft.SharePoint.SPFileLevel]::Checkout)
    {
        write-host " - Checking-out page" $x
 
        $SPFile.CheckOut()
    }
    else
    {
        write-host " - No Check-out needed page" $x
 
    }
} 
 
#check in the page
function CheckInPage ($SPFile)
{
    $url=$SPFile.ServerRelativeUrl
    
    if ($SPFile.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
    {
        write-host " - Checking-in page" $url
        $oldValue = $SPFile.Web.AllowUnsafeUpdates

        $SPFile.Web.AllowUnsafeUpdates = $true

        $SPFile.CheckIn("Checkin", [Microsoft.SharePoint.SPCheckInType]::MajorCheckin)

        $SPFile.Web.AllowUnsafeUpdates = $oldValue
    }
    else
    {
        write-host " - No Check-in needed page" $url
    }
 
} 
 
#Approve the page
function ApprovePage ($PageListItem)
{
    if($PageListItem.ListItems.List.EnableModeration)
    {
        #Check to ensure page requires approval, and if so, approve it
        try{
            write-host " - Approving page" $PageListItem.File.ServerRelativeUrl
 
            $PageListItem.File.Publish("")
            $PageListItem.File.Approve("Page approved automatically by PowerShell script")
        }
        catch
        {
            write-host $_ -ForegroundColor Red
        }
    }
    else
    {
        write-host " - No approval on list"
    }
}
 
#Add Web Part to the page
function AddWebPart($wpTitle, $wpDestinationPageFullUrl, $wpLocalPath, $wpZoneName, $wpZoneIndex, $IsReplace, $IsNewPage)
{
    [Microsoft.SharePoint.SPFile] $spFile = $Web.GetFile($wpDestinationPageFullUrl)

    write-host(' - Adding web part '+$wpTitle+' to :'+$spFile.url+'...')
    try
    {
        $Web.AllowUnsafeUpdates = $true
		#if ($IsNewPage)
		#{
	        if ($SPFile.CheckOutStatus -ne "None")
	        {
	            write-host "  - Doing undocheckout"
	            $SPFile.UndoCheckOut()
	        }
	        #else
	        #{
	            CheckOutPage -SPFile $spFile
	        #}
		#}
 
        ####
        [Microsoft.SharePoint.WebPartPages.SPLimitedWebPartManager]$wpManager = $Web.GetLimitedWebPartManager($FullUrl,[System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared) 
 
        if ($null -eq [System.Web.HttpContext]::Current)
        {
            $sw = New-Object System.IO.StringWriter
            $resp = New-Object System.Web.HttpResponse $sw
            $req = New-Object System.Web.HttpRequest "", $Web.Url, ""
            $htc = New-Object System.Web.HttpContext $req, $resp
            #explicitly cast $web to spweb object else sharepoint will
            #see it as a PSObject, and AddWebpart wil fail
            $htc.Items["HttpHandlerSPWeb"] = $web  -as [Microsoft.SharePoint.SPweb]
            [System.Web.HttpContext]::Current = $htc
            if ($sw -ne $null)
            {
                $sw.Dispose()
            }
        }
        #$Web.AllowUnsafeUpdates = $true
 
        if ($IsReplace -eq $true)
        {
            $wpToDelete = $wpManager.WebParts | % { $_ }
            foreach($WebPart in $wpToDelete)
            {
                if($WebPart -ne $null)
                {
                    if($WebPart.Title -eq $WebPartTitle)
                    {
                        write-host "  - Deleting existing Web Part" $WebPart.Title -ForegroundColor Green
                        $wpManager.DeleteWebPart($WebPart)
                    }
 
                }
            }
        }
        ####
 
        $pageRelativeUrl = $SPFile.ServerRelativeUrl
        AddWebPartToPage $siteUrl $pageRelativeUrl $wpLocalPath $wpZoneName $wpZoneIndex $wpTitle
    }
    catch
    {
        write-host $_.exception -ForegroundColor Red
    }
    finally
    {
        if($wpManager -ne $null)
        {
            $wpManager.Dispose()
        }
        if($Web -ne $null)
        {
                $Web.AllowUnsafeUpdates = $false
                $Web.Dispose()

                CheckInPage -SPFile $spFile
                ApprovePage -SPFile $spFile.Item
        }
    }    
}
 
## Main Programm ##
try
{
    $runningDir = resolve-Path .\
    if($configXmlPath -eq $null -or $configXmlPath.Length -eq 0)
    {
        $configXmlPath = join-path -path $runningDir -childpath "WebPartToPageConfig.xml"
    }
    write-host(' - Reading config file from: '+$configXmlPath)
    [xml]$SiteConfig = get-content $configXmlPath
    $siteUrl = $webUrl
    $Web = Get-SPWeb -identity $siteUrl
 
    $isNewPage = $true
	$previousPageUrl = $null
    foreach($WebPart in $SiteConfig.Config.WebParts.WebPart)
    {
        #if attributes are present to the node, use innerText as $WebPart.Title will not return value
        #$wpTitle = $WebPart.Title.InnerText
        $wpTitle = $WebPart.Title
        $FullUrl = $web.url + $WebPart.DestinationPagePath
        $LocalWebPartPath = join-path -path $runningDir -childpath $WebPart.LocalSrc
        $ZoneName = $WebPart.ZoneName
        $ZoneIndex = $WebPart.ZoneIndex
        $Replace = $WebPart.Replace
 
 		if($previousPageUrl -ne $null -and $previousPageUrl -eq $FullUrl)
		{
			$isNewPage = $false
		}
		else
		{
			$isNewPage = $true
		}
		
        AddWebPart $wpTitle $FullUrl $LocalWebPartPath $ZoneName $ZoneIndex $Replace $isNewPage
		
		$previousPageUrl = $FullUrl
 
    }
	
	# action for the last file 
	#check in
    #CheckInPage -SPFile $spFile
    # Approve & Publish
    #ApprovePage -PageListItem $spFile.Item
}
catch
{
    write-host $error[0] -ForegroundColor Red
}
finally
{
    if($Web -ne $null)
    {
        $Web.Dispose()
    }
 
}
 
Pop-Location