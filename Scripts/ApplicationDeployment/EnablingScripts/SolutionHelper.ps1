﻿# Ensuring the existense of SharePoint snapin
If ((Get-PsSnapin |?{$_.Name -eq "Microsoft.SharePoint.PowerShell"})-eq $null)
{
   	Write-Host(" - Loading SharePoint Powershell Snapin")
  	$PSSnapin = Add-PsSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue | Out-Null
}

# Example: IsSolutionDeployToWebApp 'SolutionNameIncl.WSP' 'http://WebAppURL'
function IsSolutionDeployToWebApp
{
    [CmdletBinding()]
    param($solutionName, $webAppUrl)
    
    #ensures the web app url is valid
    if((Get-SPWebApplication $webAppUrl -ErrorAction SilentlyContinue) -ne $null)
    {
        $solution = Get-SPSolution $solutionName -ErrorAction SilentlyContinue
        if($solution -ne $null)
        {
            if($solution.Deployed -eq $false)
            {
                #The solution is not deployed to any web applications at all
                return $false
            }

            if($solution.DeploymentState -eq 'GlobalDeployed')
            {
                #The solution is deployed gloabally and all web applications is therefor affected
                return $true
            }
            
            if($solution.DeployedWebApplications -ne $null)
            {
                foreach($DeployedWebApp in $solution.DeployedWebApplications)
                {
                    if(($DeployedWebApp.Url | where{$_.tolower().contains($webAppUrl.tolower())}) -ne $null)
                    {
                        #The requested web application url is among the urls to which the solution is deployed
                        return $true
                    }
                }
            }
        }
    }
    
    return $false
}

# Example: IsAllSolutionsDeployToWebApp @('SolutionName01Incl.WSP','SolutionName02Incl.WSP','SolutionName03Incl.WSP') 'http://WebAppURL'
function IsAllSolutionsDeployToWebApp
{
    [CmdletBinding()]
    param($solutionNames, $webAppUrl)

    foreach($solutionName in $solutionNames)
    {
        if((IsSolutionDeployToWebApp $solutionName $webAppUrl ) -eq $false)
        {
            return $false
        }
    }

    return $true
}

function EnsureSolutionProvisioningOnWebApplication
{
    [CmdletBinding()]
    param([string]$SolutionName, 
        [string]$WebAppIdentity,
        [switch]$GacDeployment,
        [switch]$CASPolicies,
        [switch]$Force)
    
    $webApp = Get-SPWebApplication $WebAppIdentity -ErrorAction SilentlyContinue
    if($webApp -ne $null)
    {
        $solution = Get-SPSolution $solutionName -ErrorAction SilentlyContinue
        if($solution -ne $null)
        {
            write-host(' - Provisioning solution '+$SolutionName+' to web application '+$WebAppIdentity+'...') -NoNewline
            if(!(IsSolutionDeployToWebApp $SolutionName $WebAppIdentity))
            {
                try
                {
                    $cmd = '$solution = Install-SPSolution -Identity $SolutionName -WebApplication $WebAppIdentity' 
                    if($GacDeployment)
                    {
                        $cmd = $cmd +' -GACDeployment'
                    }
                    if($Force)
                    {
                        $cmd = $cmd +' -Force'
                    }
                    if($CASPolicies)
                    {
                        $cmd = $cmd +' -CASPolicies'
                    }

                    Write-Debug('Executing '+ $cmd)
                    iex $cmd
                    WaitForJobToFinish $SolutionName
                    write-host('Done') -F Green
                }
                catch
                {
                    Write-Host
                    Write-Host('  - Error provisioning solution '+$SolutionName+' to web app '+$WebAppIdentity+': '+$_.Exception.Message) -F Red
                }
            }
            else
            {
                write-host;
                write-host('  - Solution '+$SolutionName+' is already provisioned to '+$WebAppIdentity)   -F Yellow
            }
        }
        Else
        {
            write-host(' - Error: No solution found from '+$SolutionName+'. Add the solution to the farm and try again') -F Red
        }
    }
    else
    {
        write-host(' - Error: No web application found from '+$WebAppIdentity) -F Red
    }
}

function EnsureAddSolution
{
    [CmdletBinding()]
    param([string]$SolutionName, 
        [string]$SolutionFilePath,
        [switch]$GacDeployment,
        [switch]$CASPolicies,
        [switch]$Force)

    $solution = Get-SPSolution $solutionName -ErrorAction SilentlyContinue
    if($solution -eq $null)
    {
        Write-Host(' - Adding solution '+$SolutionName+'...') -NoNewline
        try
        {
            $cmd = 'Add-SPSolution -LiteralPath "'+ $SolutionFilePath+'"'
       
            Write-Debug('Executing '+ $cmd)
            iex $cmd | Out-Null

            write-host('Done') -F Green
        }
        catch
        {
            Write-Host
            Write-Host('  - Error adding solution '+$SolutionName+': '+$_.Exception.Message) -F Red
        }
    }
    else
    {
        Write-Host(' - Updating solution '+$SolutionName+'...') -NoNewline
        try
        {
            $cmd = 'Update-SPSolution -Identity "'+ $SolutionName +'" -LiteralPath "'+$SolutionFilePath+'"'

            if($GACDeployment)
            {
                $cmd = $cmd +' -GACDeployment'
            }
            if($Force)
            {
                $cmd = $cmd +' -Force'
            }
            if($CASPolicies)
            {
                $cmd = $cmd +' -CASPolicies'
            }

            Write-Debug('Executing '+ $cmd)
            iex $cmd
            WaitForJobToFinish $SolutionName
            write-host('Done') -F Green
        }
        catch
        {
            Write-Host
            Write-Host('  - Error updating solution '+$SolutionName+': '+$_.Exception.Message) -F Red
        }

    }
}

function EnsureInstallSolution
{
 [CmdletBinding()]
    param([Parameter(Mandatory=$true)][string]$SolutionFile, 
        [Parameter(Mandatory=$true)][string]$webAppId,
        [switch]$GacDeployment,
        [switch]$CASPolicies,
        [switch]$Force)

    $file = Get-ChildItem $solutionFile
    $literalPath = $file.FullName
    $solutionName = $file.Name

    #Build EnsureSolution command
    $cmd = 'EnsureAddSolution -SolutionName "'+$solutionName+'" -SolutionFilePath "'+$literalPath+'"'

    if($GACDeployment)
    {
        $cmd = $cmd +' -GACDeployment'
    }
    if($Force)
    {
        $cmd = $cmd +' -Force'
    }
    if($CASPolicies)
    {
        $cmd = $cmd +' -CASPolicies'
    }

    Write-Debug('Executing '+ $cmd)
    iex $cmd
     
    #Build EnsureSolutionOnWebApp command
    $cmd = 'EnsureSolutionProvisioningOnWebApplication -SolutionName "'+$solutionName+'" -WebAppIdentity "'+$webAppId+'"'

    if($GACDeployment)
    {
        $cmd = $cmd +' -GACDeployment'
    }
    if($Force)
    {
        $cmd = $cmd +' -Force'
    }
    if($CASPolicies)
    {
        $cmd = $cmd +' -CASPolicies'
    }

    Write-Debug('Executing '+ $cmd)
    iex $cmd
}


function WaitForJobToFinish([string]$SolutionFileName)
{ 
    $JobName = "*solution-deployment*$SolutionFileName*"
    $job = Get-SPTimerJob | ?{ $_.Name -like $JobName }
    if ($job -eq $null) 
    {
        Write-Host('  - Timer job not found') -f Red
    }
    else
    {
        $JobFullName = $job.Name
        #Write-Host('  - Waiting to finish job '+$JobFullName+' ') -NoNewLine
        
        while ((Get-SPTimerJob $JobFullName) -ne $null) 
        {
            Write-Host -NoNewLine .
            Start-Sleep -Seconds 2
        }
        #Write-Host
        #Write-Host('   - Finished waiting for job.')
    }
}
