try
{
    $currentFolder = Split-Path -Parent $MyInvocation.MyCommand.Definition
    Push-Location $currentFolder

    . ..\EnablingScripts\SolutionHelper.ps1
    . ..\00_Settings.ps1

    $solutionFile = '.\PostNord.Portal.DigitalPostage.wsp'
    $webAppId = $fullWebAppUrl
    EnsureInstallSolution -SolutionFile $solutionFile -webAppId $webAppId -GacDeployment
}
finally
{
    Pop-Location
}