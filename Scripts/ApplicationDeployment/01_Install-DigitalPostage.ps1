
write-host('## Provisioning the Digital Postage solution ##')
write-host('## Started: '+ (Get-Date) +' ##')
write-host

if((Get-ChildItem 00_Settings.ps1 -ErrorAction SilentlyContinue) -eq $null)
{
    Write-Host(' - Missing settings file: 00_Settings.ps1') -F Red
}
else
{
    #Setup Web applications
    #.\SetupSiteStructure\ProvisioningWebApplications.ps1

    #Setup Sites
    .\SetupSiteStructure\ProvisioningSites.ps1

    #Provisioning Solutions
    #.\SolutionPackages\ProvisionSolutions.ps1

    #Activate Features
    .\Features\ActivateFeatures.ps1
}

write-host
write-host('## Finished: '+ (Get-Date) +' ##')