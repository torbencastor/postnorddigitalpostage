If ((Get-PsSnapin |?{$_.Name -eq "Microsoft.SharePoint.PowerShell"})-eq $null)
{
   	Write-Host(" - Loading SharePoint Powershell Snapin")
  	$PSSnapin = Add-PsSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue | Out-Null
}


try
{
    $currentFolder = Split-Path -Parent $MyInvocation.MyCommand.Definition
    Push-Location $currentFolder

    . ..\00_Settings.ps1

    write-host(' - Provisioning web application '+$WebAppName+'...') -NoNewline
    $ap = New-SPAuthenticationProvider
    New-SPWebApplication -Name $WebAppName -ApplicationPool $AppPoolName -ApplicationPoolAccount (Get-SPManagedAccount $AppPoolAccount) -HostHeader $HostHeader -Port $Port -Url $Url -AuthenticationProvider $ap -DatabaseName $DBName | out-null
    Write-Host('Done') -F Green
}
catch
{    
    write-host
    write-host('  - Error: '+$_.Exception.Message) -F Red
}
finally
{
    Pop-Location
}
