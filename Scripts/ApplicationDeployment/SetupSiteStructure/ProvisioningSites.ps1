If ((Get-PsSnapin |?{$_.Name -eq "Microsoft.SharePoint.PowerShell"})-eq $null)
{
   	Write-Host(" - Loading SharePoint Powershell Snapin")
  	$PSSnapin = Add-PsSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue | Out-Null
}

# Creates a site collection
function CreateSiteCollection()
{
    param($name,$owner,$url,$template, $language = 1033)
    
    
        write-host(' - Creating site collection named ['+$name+'] on ['+$url+'] ...') -NoNewline
        $site =   New-SPSite -Url $url -Name $name -OwnerAlias $owner -Template $template -Language $language -EA SilentlyContinue -EV err| Out-Null
        if($site -ne $null)
        {
            Write-Host('Done') -F Green
        }
        else
        {
            Write-Host
            Write-Host("  - Error: "+$err) -F Red
        }
}


function CreateSite()
{
    param($name, $url, $template = 'BLANKINTERNET#0', $language = 1033)

    try
    {
        write-host(' - Creating site named ['+$name+'] on ['+$url+'] ...') -NoNewline
        $web = New-SPWeb -Url ($url) -Name ($name) -Template $template -Language $language -EA SilentlyContinue -EV err
        if($web -ne $null)
        {
            Write-Host('Done') -F Green
        }
        else
        {
            Write-Host
            Write-Host("  - Error: "+$err) -F Red
        }
    }
    catch
    {
        Write-Host
        Write-Host("  - Error: "+$_.Exception.Message) -F Red
    }
}

function EnsureManagedPath
{
    param([string]$WebWebAppUrl,
    [string]$WebManagedPath)

    try
    {
        $managedPath = Get-SPManagedPath -Identity $WebManagedPath -WebApplication $WebWebAppUrl -ErrorAction SilentlyContinue
        if($managedPath -eq $null)
        {           
            write-host(' - Creating managed path ['+$WebWebAppUrl+'/'+$WebManagedPath+']...') -NoNewline
            New-SPManagedPath -RelativeURL $WebManagedPath -WebApplication $WebWebAppUrl
            write-host('Done') -F Green
        }
        else
        {
            write-host(' - Managed path already exist, no action required')
        }
    }
    catch{
        write-host
        write-host(' - An error occured during ensuring of manged path') -F Red
    }     # Ensuring the existence of the managed path

}

# Creates the structure
function CreateSiteStructure()
{
    param($sites)

    Write-Host('### Creating Site Structure ###')


    $errorOccurred = $false
    foreach($site in $sites)
    {
        # Configuring all the site collections
        try
        {
            if($site.isSiteCollection -eq $true)
            {
                $siteName = $site.SiteName
                $siteOwner = $site.Owner
                $siteUrl = $site.Url
                $siteTemplate = $site.Template
                $siteLanguage =  $site.Language

                CreateSiteCollection -name $siteName -Owner $siteOwner -url $siteUrl -template $siteTemplate -language $siteLanguage
            }
            else
            {
                $siteName = $site.SiteName
                $siteUrl = $site.Url
                $siteTemplate = $site.Template
                $siteLanguage =  $site.Language
                
                CreateSite -name $siteName -url $siteUrl -template $siteTemplate -language $siteLanguage
            }

        }
        catch{
            write-host('  - An error occured when while reading properties of site: '+$site) -F Red
        }
    }

    Write-Host('### Create Site Structure is complete ###')
}

# Calling the create structure function

pushd (Split-Path -Parent $MyInvocation.MyCommand.Definition)
. ..\00_Settings.ps1

CreateSiteStructure $sites

popd