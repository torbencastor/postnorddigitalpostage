
# Web application settings
$environment = 'Test'
$serviceName = 'pn-digitalpostage-service.cloudapp.net'
$AppPoolAccount = "pnazure\TSP_DPAppPool"
$Port = 81
$webTemplate= 'CMSPUBLISHING#0'
$WebAppName = $environment+'_'+$serviceName
$AppPoolName = $WebAppName+"_AppPool"
$HostHeader = $serviceName
$webAppUrl = 'http://'+$serviceName
$fullWebAppUrl = $webAppUrl +':'+$Port
$DBName = $environment+"_SP2010_ Content_"+$WebAppName

# Site collection and web settings
$sites = @(@{isSiteCollection=$true;SiteName='Digital Porto';Owner='pnazure\testsiteadmin';Url=$fullWebAppUrl;Template=$webTemplate;Language=1033},
           @{SiteName='Administration';Url=$fullWebAppUrl+'/admin';Template=$webTemplate;Language=1033},
           @{SiteName='Raportering';Url=$fullWebAppUrl+'/raportering';Template=$webTemplate;Language=1033},
           @{SiteName='Produktion';Url=$fullWebAppUrl+'/produktion';Template=$webTemplate;Language=1033},
           @{SiteName='Kunde Center';Url=$fullWebAppUrl+'/kundecenter';Template=$webTemplate;Language=1033})


